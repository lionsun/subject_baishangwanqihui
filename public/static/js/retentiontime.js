/**
 *获取网页来源的地址
 */
function getReferrer() {
    var referrer = "";
    try {
        referrer = window.top.document.referrer;
    } catch(e) {
        if (window.parent) {
            try {
                referrer = window.parent.document.referrer;
            } catch(e2) {
                referrer = "";
            }
        }
    }
    if (referrer === "") {
        referrer = document.referrer;
    }
    return referrer;
}

//统计用户访问网站页面时间
var tjSecond = 0;
var tjRandom = 0;
//定时增加时间
window.setInterval(function() {
    tjSecond++;
},1000);
//获取随机的时间
tjRandom = (new Date()).valueOf();
//加载
window.onload = function() {
    var goods_sku_id;
};
//onbeforeunload 事件在即将离开当前页面（刷新或关闭）时触发
//该事件可用于弹出对话框，提示用户是继续浏览页面还是离开当前页面
window.onbeforeunload = function() {
    //定义空的容器
    var params = {};
    params.tjRd = tjRandom;
    //获取当前的url地址
    params.url = location.href;
    params.time = tjSecond;
    params.time_in = Date.parse(new Date()) - (tjSecond * 1000);
    params.time_out = Date.parse(new Date());
    //获取页面的title标题
    params.title = document.title;
    //获取域
    params.domain = document.domain;
    //获取屏幕的高度
    params.client_height = window.screen.height;
    //获取屏幕的宽度
    params.client_width = window.screen.width;
    //获取语言
    params.client_language = navigator.language;
    //获取来源地址
    params.refer = getReferrer();
    params.goods_sku_id = goods_sku_id;

    if(params.time > 0){
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: "/api/pageretention/addretention",
            data: {params: params},
            success: function(res){

            }
        });
    }


};
