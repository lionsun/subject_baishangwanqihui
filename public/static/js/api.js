
function HttpApi() {

}

HttpApi.prototype.contextPath = "http://dt.bswqh.com/yunzserver"

HttpApi.prototype.post= function(url, body, onSuccess, onError) {
    return jQuery.ajax({
        "url": url,//请求的url
        "data": JSON.stringify(body),//请求的参数
        "type": "post",//请求的类型
        "contentType": "application/json;charset=UTF-8",
        "dataType": "json",//接收数据类型
        "async": true,//异步请求
        "cache": false,//浏览器缓存
        "success": onSuccess?onSuccess:function(){},//请求成功后执行的函数
        "error": onError?onError:function(){},//请求失败后执行的函数
        // "jsonpCallback": "jsonp" + (new Date()).valueOf().toString().substr(-4),//通过jsonp跨域请求的回调函数名
        // "beforeSend": function () {}，//请求前要处理的函数
    });
};

HttpApi.prototype.exescript = function(scriptName, body, onSuccess, onError) {
    return this.post(HttpApi.prototype.contextPath+"/flow/conf/feature/"+scriptName, body, onSuccess, onError);
};
