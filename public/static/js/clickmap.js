var heatmap;
var heatmap_circles = [];
var heatmap_canvas;
var heatmap_context;
var sign = window.location.hash;
var height = $(document).height();
var width = $(document).width();
var time = GetRequest();
var start_time = time.start_time;
var end_time = time.end_time;
var mark_sign = false;

//收集用户点击数据
$(document).mousedown('on', function (event) {
    var x = event.pageX;
    var y = event.pageY;

    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        url: "/api/pageclickmap/addclickmap",
        data: {x: x, y: y},
        success: function (res) {

        }
    });
});
//显示用户点击数据
$(document).mousemove('on', function (event) {
    var x = event.pageX;
    var y = event.pageY;

    var val = heatmap.getValueAt({x: x, y: y});
    myalert('点击次数：' + val, event.clientY, (event.clientX+18));

});



if(sign == "#clickmap"){
    $("body").css({"position":"absolute", "background-color": "blanchedalmond", "width": width, "height": height, "opacity": "0.39", "z-index": "9"});
    $("body").attr("id", "heatmapArea");
    var url = window.location.origin + window.location.pathname;
    heatmap = h337.create({
        container: document.getElementById("heatmapArea"), //容器
    });
    var clickmapOption = [];
    var clickmapMax = 0;


    // 数据获取要换接口Url
    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        url: "/api/pageclickmap/getclickmapbyurl",
        data: {url_: url.toString(), start_time: start_time, end_time: end_time},
        dataType: 'json',
        success: function(res){
            res = JSON.parse(res);
            $.each(res.data, function(k,v){
                var val = v.click_sum;
                clickmapMax = Math.max(clickmapMax, val);
                var d = {
                    x: v.xxx,
                    y: v.yyy,
                    value: val
                };
                clickmapOption.push(d);
            });
            var clickmapData = {
                max: clickmapMax,
                data: clickmapOption
            };
            heatmap.setData(clickmapData);
            $(".heatmap-canvas").css({"z-index": "10"});
        }
    });
}

// 点击数量提示
function myalert(str, top, left) {
    var div = '<div class="mark" style="display: none; position: fixed;  top: ' + top + 'px; right: 0; bottom: 0; left: ' + left + 'px; height: 40px; line-height: 40px; width: 80px; text-align: center; font-size: 0.28rem; color: #fff; background: rgba(0, 0, 0, 0.6); border-radius: 0.1rem; padding: 3px 6px; z-index: 9999;"></div>';
    if(!mark_sign){
        $('body').append(div);
        mark_sign = true;
    }
    $('.mark').css("top", top+'px');
    $('.mark').css("left", left+'px');
    $('.mark').html(str);
    $('.mark').show();
}

function GetRequest() {
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {
        var str = url.substr(1);
        strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
        }
    }
    return theRequest;
}
