;(function($, window, document,undefined) {
    //定义的构造函数
    var Drag = function(ele, opt) {
        this.$ele = ele,
				this.x = 0,
				this.y = 0,
        this.defaults = {
        			root:'root',
					parent:'parent',
					direction:'all',
					handler:false,
					dragStart:function(x,y){},
					dragEnd:function(x,y){},
					dragMove:function(x,y){}
				},
				this.options = $.extend({}, this.defaults, opt)
    }
    //定义方法
    Drag.prototype = {
        run: function() {
					var $this = this;
					var root = this.options.root;//拖动事件的作用范围 要加以限制
					var element = this.$ele;
					var direction = this.options.direction; //方向
					var handler = this.options.handler;
					var parent = this.options.parent;
					var isDown = false; //记录鼠标是否按下
					var fun = this.options; //使用外部函数
					var X = 0,
							Y = 0,
							moveX,
							moveY;
					// 阻止冒泡
					element.find('*').not('img').mousedown(function(e) {
						e.stopPropagation();
					});
					if(root == 'root'){
						root = document;
					}
					//初始化判断
					if(parent == 'parent'){
						parent = element.parent();
					}else{
						parent = element.parents(parent);
					}
					if(!handler){
						handler = element; 
					}else{
						handler = element.find(handler);
					}
					//初始化
					parent.css({position:'relative'});
					element.css({position:'absolute'});
					var boxWidth=0,boxHeight=0,sonWidth=0,sonHeight=0;
					//盒子 和 元素大小初始化
					initSize();
					$(window).resize(function(){
						initSize();

					});
					//盒子 和 元素大小初始化函数
					function initSize(){
						boxWidth = parent.outerWidth();
						boxHeight = parent.outerHeight();
						sonWidth = element.outerWidth();
						sonHeight = element.outerHeight();
					}
					handler.css({cursor:'move'}).mousedown(function(e){
						isDown = true;
						X = e.pageX;
						Y = e.pageY;
						$this.x = element.position().left;
						$this.y = element.position().top;
						element.addClass('on');
						fun.dragStart(parseInt(element.css('left')),parseInt(element.css('top')));
						return false;
					});
					$(root).mouseup(function(e){fun.dragEnd(parseInt(element.css('left')),parseInt(element.css('top')));element.removeClass('on');isDown = false;});
					$(root).mousemove(function(e){
						moveX = $this.x+e.pageX-X;
						moveY = $this.y+e.pageY-Y;
						function thisXMove(){ //x轴移动
							if(isDown == true){
								element.css({left:moveX});
							}else{
								return;
							}
							if(moveX < 0){
								element.css({left:0});
							}
							if(moveX > (boxWidth-sonWidth)){
								element.css({left:boxWidth-sonWidth});
							}
							return moveX;
						}
						function thisYMove(){ //y轴移动
							if(isDown == true){
								element.css({top:moveY});
							}else{
								return;
							}
							if(moveY < 0){
								element.css({top:0});
							}
							if(moveY > (boxHeight-sonHeight)){
								element.css({top:boxHeight-sonHeight});
							}
							return moveY;
						}
						function thisAllMove(){ //全部移动
							if(isDown == true){
								element.css({left:moveX,top:moveY});
							}else{
								return;
							}
							if(moveX < 0){
								element.css({left:0});
							}
							if(moveX > (boxWidth-sonWidth)){
								element.css({left:boxWidth-sonWidth});
							}
							if(moveY < 0){
								element.css({top:0});
							}
							if(moveY > (boxHeight-sonHeight)){
								element.css({top:boxHeight-sonHeight});
							}
						}
						if(isDown){
					  	fun.dragMove(parseInt(element.css('left')),parseInt(element.css('top')));
						}else{
							return false;
						}
						if(direction.toLowerCase() == "x"){
							thisXMove();
						}else if(direction.toLowerCase() == "y"){
							thisYMove();
						}else{
							thisAllMove();
						}
					});
        }
    }

    //插件
    $.fn.myDrag = function(options) {
        //创建实体
        var drag = new Drag(this, options);
        //调用方法
        drag.run();
		return this;
    }
})(jQuery, window, document);