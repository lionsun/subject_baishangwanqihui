<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\api\controller;

use addon\platformpintuan\model\PintuanGroup as PintuanGroupModel;
use addon\platformpintuan\model\PintuanOrder as PintuanOrderModel;
use app\api\controller\BaseApi;

/**
 * 拼团组
 */
class Pintuangroup extends BaseApi
{
	/**
	 * 列表信息
	 */
	public function lists()
	{
		$goods_id = isset($this->params['goods_id']) ? $this->params['goods_id'] : 0;
		$pintuan_id = $this->params['pintuan_id'] ?? 0;
		if (empty($goods_id)) {
			return $this->response($this->error('', 'REQUEST_GOODS_ID'));
		}
		
		$pintuan_group_model = new PintuanGroupModel();
		$condition = [
			[ 'ppg.goods_id', '=', $goods_id ],
			[ 'ppg.status', '=', 2 ],// 当前状态:0未支付 1拼团失败 2.组团中3.拼团成功
		];
		if(!empty($pintuan_id)){
            $condition[] = [ 'ppg.pintuan_id', '=', $pintuan_id];
        }
		$list = $pintuan_group_model->getPintuanGoodsGroupList($condition);
		return $this->response($list);
	}

    /**
     * 成团列表
     */
	public function page()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $pintuan_id = $this->params['pintuan_id'] ?? 0;
        $page = $this->params['page'] ?? 1;
        $page_size = $this->params['page_size'] ?? PAGE_LIST_ROWS;
        $status = $this->params['status'] ?? PintuanGroupModel::GROUP_STATUS_IN_PROCESS . ',' . PintuanGroupModel::GROUP_STATUS_SUCCESS;

        $condition = [];
        $condition[] = ['pg.pintuan_id', '=', $pintuan_id];
        $condition[] = ['pg.pintuan_trader_id', '=', $this->member_id];
        if($status !== ''){
            $condition[] = ['status', 'in', $status];
        }

        $pintuan_group_model = new PintuanGroupModel();
        $res = $pintuan_group_model->getPintuanGroupPageList($condition, $page, $page_size);
        $pintuan_order_model = new PintuanOrderModel();
        foreach($res['data']['list'] as $key=>$val){
            $res['data']['list'][$key]['buyer_list'] = $pintuan_order_model->getPintuanOrderList([['group_id', '=', $val['group_id']]], '*', 'order_id asc')['data'];
        }

        return $this->response($res);
    }

    /**
     * 一键成团
     */
    public function virtualComplete()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);
        //$this->member_id = 80;

        $group_id = $this->params['group_id'] ?? 0;

        $pintuan_group_model = new PintuanGroupModel();
        $res = $pintuan_group_model->virtualComplete($group_id, $this->member_id);
        return $this->response($res);
    }
	
}