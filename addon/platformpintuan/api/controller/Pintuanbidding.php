<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\api\controller;

use addon\platformpintuan\model\PintuanOrder as PintuanOrderModel;
use addon\platformpintuan\model\PintuanTrader\Config as ConfigModel;
use addon\platformpintuan\model\PintuanTrader\PintuanBidding as PintuanBiddingModel;
use app\api\controller\BaseApi;
use app\model\goods\Goods as GoodsModel;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use app\model\order\OrderCommon as OrderCommonModel;

/**
 * 团长
 */
class Pintuanbidding extends BaseApi
{
    /**
     * 竞标分页
     */
    public function biddingPage()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $page = $this->params['page'] ?? 1;
        $page_size = $this->params['page_size'] ?? PAGE_LIST_ROWS;
        $bidding_status = $this->params['bidding_status'] ?? PintuanBiddingModel::BIDDING_STATUS_IN_PROCESS;
        $keywords = $this->params['keywords'] ?? '';

        $bidding_model = new PintuanBiddingModel();
        $alias = 'bidding';
        $join = [
            ['goods_sku sku', 'bidding.sku_id = sku.sku_id', 'left'],
        ];
        $field = [
            "bidding.bidding_id, bidding.pintuan_name, bidding.site_id, bidding.site_name, bidding.bidding_start_time, bidding.bidding_end_time, bidding.bidding_status, bidding.bidding_level, bidding.start_time, bidding.end_time",
            "sku.goods_id,sku.sku_id,sku.sku_name,sku.price,sku.stock,sku.sale_num,sku.sku_image,sku.spec_name,sku.source",
        ];
        $order = 'bidding.create_time desc';
        $condition = [];
        if($bidding_status !== ''){
            $condition[] = ['bidding.bidding_status', '=', $bidding_status];
        }
        if(trim($keywords) !== ''){
            $condition[] = ['sku.sku_name', 'like', "%{$keywords}%"];
        }

        $res = $bidding_model->getPintuanBiddingPageList($condition, $field, $order, $page, $page_size, $alias, $join);
        //查询参与信息
        foreach($res['data']['list'] as $key=>$val){
            $enter_info = $bidding_model->getPintuanBiddingEnterInfo([
                ['bidding_id', '=', $val['bidding_id']],
                ['pintuan_trader_id', '=', $this->member_id]], '*');
            $res['data']['list'][$key]['enter_id'] = !empty($enter_info['data']) ? $enter_info['data']['enter_id'] : 0;
            $res['data']['list'][$key]['enter_info'] = $enter_info;
            $res['data']['list'][$key]['enter_condition'] = [['bidding_id', '=', $val['bidding_id']], ['pintuan_trader_id', '=', $this->member_id]];
        }

        return $this->response($res);
    }

    /**
     * 竞标详情
     */
    public function biddingDetail()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $bidding_id = $this->params['bidding_id'] ?? 0;

        $bidding_model = new PintuanBiddingModel();
        $bidding_info = $bidding_model->getPintuanBiddingInfo([['bidding_id', '=', $bidding_id]])['data'];
        $bidding_info['bidding_level_rule'] = json_decode($bidding_info['bidding_level_rule'], 'true');

        //竞标金额
        $config_model = new ConfigModel();
        $config_info = $config_model->getPintuanTraderConfig()['data']['value'];
        foreach($bidding_info['bidding_level_rule'] as $key=>$val){
            $bidding_info['bidding_level_rule'][$key]['bidding_money'] = $val['bidding_num'] * $val['cost_price'] * $config_info['bidding_deposit_rate'] / 100;
        }

        if(!empty($bidding_info)){
            $goods_model = new GoodsModel();
            $sku_info = $goods_model->getGoodsSkuInfo([['sku_id', '=', $bidding_info['sku_id']]], 'goods_id,sku_id,sku_name,price,stock,sale_num,sku_image,spec_name,source,sku_spec_format')['data'];
            $sku_info['sku_spec_format'] = empty($sku_info['sku_spec_format']) ? '{}' : $sku_info['sku_spec_format'];
            $bidding_info['sku_info'] = $sku_info;
        }

        return $this->response($this->success($bidding_info));
    }

    /**
     * 参与竞标
     */
    public function enter()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $bidding_id = $this->params['bidding_id'] ?? 0;
        $bidding_level = $this->params['bidding_level'] ?? 0;
        $pay_password = $this->params['pay_password'] ?? '';

        $bidding_model = new PintuanBiddingModel();
        $res = $bidding_model->pintuanBiddingEnter([
            'bidding_id' => $bidding_id,
            'bidding_level' => $bidding_level,
            'pintuan_trader_id' => $this->member_id,
            'pay_password' => $pay_password,
        ]);
        return $this->response($res);
    }

    /**
     * 参与分页
     */
    public function enterPage()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $keywords = $this->params['keywords'] ?? '';

        $page = $this->params['page'] ?? 1;
        $page_size = $this->params['page_size'] ?? PAGE_LIST_ROWS;
        $enter_status = $this->params['enter_status'] ?? '';

        $bidding_model = new PintuanBiddingModel();
        $alias = 'enter';
        $join = [
            ['promotion_platform_pintuan_bidding bidding', 'bidding.bidding_id = enter.bidding_id', 'left'],
        ];
        $field = [
            "enter.enter_id, enter.enter_time, enter.enter_status, enter.pintuan_trader_id, enter.pintuan_trader_name, enter.bidding_level, enter.bidding_money",
            "bidding.bidding_id, bidding.pintuan_id, bidding.pintuan_name, bidding.site_id, bidding.site_name, bidding.bidding_start_time, bidding.bidding_end_time, bidding.bidding_status, bidding.start_time, bidding.end_time",
            "bidding.goods_id, bidding.sku_id, bidding.sku_name, bidding.sku_image, bidding.price, bidding.bidding_level_rule, bidding.enter_num, bidding.bidding_success_num, bidding.pintuan_num",
        ];
        $order = 'enter.enter_time desc';
        $condition = [];
        $condition[] = ['enter.pintuan_trader_id', '=', $this->member_id];
        if($enter_status !== '') {
            $condition[] = ['enter.enter_status', '=', $enter_status];
        }
        if(trim($keywords) !== ''){
            $condition[] = ['bidding.sku_name', 'like', "%{$keywords}%"];
        }

        $res = $bidding_model->getPintuanBiddingEnterPageList($condition, $field, $order, $page, $page_size, $alias, $join);
        foreach($res['data']['list'] as $key=>$val){
            $res['data']['list'][$key]['bidding_level_rule'] = json_decode($val['bidding_level_rule'], true);
        }
        return $this->response($res);
    }

    /**
     * 批发管理
     */
    public function pintuanPage()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $keywords = $this->params['keywords'] ?? '';

        $page = $this->params['page'] ?? 1;
        $page_size = $this->params['page_size'] ?? PAGE_LIST_ROWS;
        $status = $this->params['status'] ?? '';

        $bidding_model = new PintuanBiddingModel();
        $alias = 'enter';
        $join = [
            ['promotion_platform_pintuan_bidding bidding', 'bidding.bidding_id = enter.bidding_id', 'left'],
            ['promotion_platform_pintuan pintuan', 'pintuan.pintuan_id = bidding.pintuan_id', 'inner'],
            ['promotion_platform_pintuan_goods goods', 'goods.pintuan_id = bidding.pintuan_id', 'inner'],
        ];
        $field = [
            "enter.enter_id, enter.enter_time, enter.enter_status, enter.pintuan_trader_id, enter.pintuan_trader_name, enter.bidding_level, enter.bidding_money",
            "bidding.bidding_id, bidding.pintuan_id, bidding.pintuan_name, bidding.site_id, bidding.site_name, bidding.bidding_start_time, bidding.bidding_end_time, bidding.bidding_status, bidding.start_time, bidding.end_time",
            "bidding.goods_id, bidding.sku_id, bidding.sku_name, bidding.sku_image, bidding.price ,bidding.bidding_level_rule",
            "pintuan.status, pintuan.pintuan_num",
            "goods.pintuan_price, goods.id"
        ];
        $order = 'enter.enter_time desc';
        $condition = [];
        $condition[] = ['enter.pintuan_trader_id', '=', $this->member_id];
        $condition[] = ['enter.enter_status', '=', PintuanBiddingModel::ENTER_STATUS_SUCCESS];

        if($status !== ''){
            //结束的包含已失效的
            if($status == PintuanModel::PINTUAN_STATUS_CLOSED){
                $status = PintuanModel::PINTUAN_STATUS_ENDED . ',' . PintuanModel::PINTUAN_STATUS_CLOSED;
            }
            $condition[] = ['pintuan.status', 'in', $status];
        }
        if(trim($keywords) !== ''){
            $condition[] = ['bidding.sku_name', 'like', "%{$keywords}%"];
        }

        $res = $bidding_model->getPintuanBiddingEnterPageList($condition, $field, $order, $page, $page_size, $alias, $join);
        foreach($res['data']['list'] as $key=>$val){
            $res['data']['list'][$key]['status_name'] = PintuanModel::getPintuanStatus($val['status']);
            $res['data']['list'][$key]['bidding_level_rule'] = json_decode($val['bidding_level_rule'], true);
            $res['data']['list'][$key]['brokerage'] = sprintf('%.2f',$res['data']['list'][$key]['pintuan_price']-$res['data']['list'][$key]['bidding_level_rule'][0]['cost_price']);
        }
        return $this->response($res);
    }

    /**
     * 参与详情
     */
    public function enterDetail()
    {
        $res = $this->checkToken();
        if($res['code'] < 0) return $this->response($res);

        $enter_id = $this->params['enter_id'] ?? 0;

        $bidding_model = new PintuanBiddingModel();
        $enter_info = $bidding_model->getPintuanBiddingEnterInfo([['enter_id', '=', $enter_id], ['pintuan_trader_id', '=', $this->member_id]])['data'];
        if(!empty($enter_info)){
            $bidding_info = $bidding_model->getPintuanBiddingInfo([['bidding_id', '=', $enter_info['bidding_id']]])['data'];
            $bidding_info['bidding_level_rule'] = json_decode($bidding_info['bidding_level_rule'], 'true');
            //竞标金额
            $config_model = new ConfigModel();
            $config_info = $config_model->getPintuanTraderConfig()['data']['value'];
            foreach($bidding_info['bidding_level_rule'] as $key=>$val){
                $bidding_info['bidding_level_rule'][$key]['bidding_money'] = $val['bidding_num'] * $val['cost_price'] * $config_info['bidding_deposit_rate'] / 100;
            }
            if(!empty($bidding_info)){
                $goods_model = new GoodsModel();
                $sku_info = $goods_model->getGoodsSkuInfo([['sku_id', '=', $bidding_info['sku_id']]], 'goods_id,sku_id,sku_name,price,stock,sale_num,sku_image,spec_name,source,sku_spec_format')['data'];
                $sku_info['sku_spec_format'] = empty($sku_info['sku_spec_format']) ? '{}' : $sku_info['sku_spec_format'];
                $bidding_info['sku_info'] = $sku_info;
            }
        }else{
            $bidding_info = null;
        }

        $res = [
            'enter_info' => $enter_info,
            'bidding_info' => $bidding_info,
        ];

        return $this->response($this->success($res));
    }

    /**
     * 订单列表
     * @return false|string
     */
    public function orderPage()
    {
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);

        $alias = 'o';
        $join = [
            ['promotion_platform_pintuan_order ppo', 'ppo.order_id = o.order_id', 'inner']
        ];

        $condition = array(
            ["ppo.pintuan_trader_id", "=", $this->member_id],
        );
        $field = "o.*, ppo.pintuan_trader_commission_money";

        $order_status = isset($this->params[ 'order_status' ]) ? $this->params[ 'order_status' ] : 'all';
        switch ($order_status) {
            case "waitpay"://待付款
                $condition[] = ["o.order_status", "=", 0];
                break;
            case "waitsend"://待发货
                $condition[] = ["o.order_status", "=", 1];
                break;
            case "waitconfirm"://待收货
                $condition[] = ["o.order_status", "=", 3];
                break;
            case "waitrate"://待评价
                $condition[] = ["o.order_status", "in", [4, 10]];
                $condition[] = ["o.is_evaluate", "=", 1];
                break;
        }

        $page_index = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;

        $order_common_model = new OrderCommonModel();
        $res = $order_common_model->getMemberOrderPageList($condition, $page_index, $page_size, "o.create_time desc", $field, $alias, $join);
        $res['condition'] = $condition;
        return $this->response($res);
    }
}