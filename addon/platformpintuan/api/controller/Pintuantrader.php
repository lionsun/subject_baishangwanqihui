<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\api\controller;

use addon\platformpintuan\model\PintuanTrader\PintuanTrader as PintuanTraderModel;
use addon\platformpintuan\model\PintuanTrader\Config as ConfigModel;
use app\model\member\Member as MemberModel;
use app\api\controller\BaseApi;
use app\model\system\Qrcode as QrcodeModel;
use addon\platformpintuan\model\PintuanTrader\Poster as PosterModel;

/**
 * 团长
 */
class Pintuantrader extends BaseApi
{

    /**
     * 申请成为团长
     */
    public function apply()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $pintuan_trader_level_data = PintuanTraderModel::getPintuanTraderLevel();
        $pintuan_trader_level = $pintuan_trader_level_data[0]['pintuan_trader_level'];
        $pintuan_trader_level_name = $pintuan_trader_level_data[0]['pintuan_trader_level_name'];
        $data = [
            'member_id'                   => $this->member_id,
            'apply_pintuan_trader_level'         => $pintuan_trader_level,
            'apply_pintuan_trader_level_name'    => $pintuan_trader_level_name,
        ];
        $pintuan_trader_model = new PintuanTraderModel();
        $result = $pintuan_trader_model->apply($data);
        return $this->response($result);
    }

    /**
     * 申请成为团长
     */
    public function agentApply(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $pintuan_trader_level_data = PintuanTraderModel::getPintuanTraderLevel();
        $pintuan_trader_level = $pintuan_trader_level_data[1]['pintuan_trader_level'];
        $pintuan_trader_level_name = $pintuan_trader_level_data[1]['pintuan_trader_level_name'];
        $data = [
            'member_id'                   => $this->member_id,
            'apply_pintuan_trader_level'         => $pintuan_trader_level,
            'apply_pintuan_trader_level_name'    => $pintuan_trader_level_name,
        ];
        $pintuan_trader_model = new PintuanTraderModel();
        $result = $pintuan_trader_model->agentApply($data);
        return $this->response($result);
    }


    //团长类型
    public function trader_type_list()
    {
        $level = PintuanTraderModel::getPintuanTraderLevel();
        return $this->response($this->success($level));
    }

    //代理商下级列表
    public function agentChildList()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $page = isset($this->params['page']) ? $this->params['page'] : 0;
        $page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;

        $condition = [];
        $condition[] = ['superior','=',$this->member_id];
        $field = '*';
        $order = 'create_time desc';

        $pintuan_trader_model = new PintuanTraderModel();
        $list = $pintuan_trader_model->getPintuanTraderPageList($condition,  $field, $order, $page, $page_size);

        $member_model = new MemberModel();
        foreach($list['data']['list'] as $key=>$val){
            $member_info = $member_model->getMemberInfo([['member_id', '=', $val['member_id']]], 'username, nickname, headimg');
            $list['data']['list'][$key] = array_merge($val, $member_info['data'] ?? []);
        }
        return $this->response($list);
    }


    //代理商下级审核列表
    public function checkList()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $status = isset($this->params['status']) ? $this->params['status'] : '';
        $page = isset($this->params['page']) ? $this->params['page'] : 1;
        $page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
        if($status != 'all'){
            //待审核
            if($status==0){
                $condition[] = ['p.check_status','=',0];
            }elseif($status==2){//已通过
                $condition[] = ['p.check_status','>=',2];
            }elseif($status==1){//已拒绝
                $condition[] = ['p.check_status','=',1];
            }
        }
        $condition[] = ['p.agent_id','=',$this->member_id];
        $alias = 'p';
        $join = [
            [
                'member m',
                'p.member_id = m.member_id',
                'left'
            ]
        ];
        $field = 'p.*,m.member_id,m.member_type,m.username,m.nickname,m.mobile,m.member_level,m.member_level_name,m.reg_time';

        $pintuan_trader_model = new PintuanTraderModel();
        $result = $pintuan_trader_model->pintuanTraderApplyPageList($condition, $page, $page_size, 'p.apply_time desc', $field, $alias, $join);
        foreach($result['data']['list'] as $key=>$val){
            if($result['data']['list'][$key]['check_status']==0){
                $result['data']['list'][$key]['agent_status_name'] = '待审核';
                $result['data']['list'][$key]['admin_status_name'] = '';
            }elseif($result['data']['list'][$key]['check_status']==1){
                $result['data']['list'][$key]['agent_status_name'] = '已拒绝';
                $result['data']['list'][$key]['admin_status_name'] = '';
            }elseif($result['data']['list'][$key]['check_status']==2){
                $result['data']['list'][$key]['agent_status_name'] = '已通过';
                $result['data']['list'][$key]['admin_status_name'] = '待平台审核';
            }elseif($result['data']['list'][$key]['check_status']==3){
                $result['data']['list'][$key]['agent_status_name'] = '已通过';
                $result['data']['list'][$key]['admin_status_name'] = '平台拒绝';
            }elseif($result['data']['list'][$key]['check_status']==4){
                $result['data']['list'][$key]['agent_status_name'] = '已通过';
                $result['data']['list'][$key]['admin_status_name'] = '平台通过';
            }elseif($result['data']['list'][$key]['check_status']==5){
                $result['data']['list'][$key]['agent_status_name'] = '已通过';
                $result['data']['list'][$key]['admin_status_name'] = '平台拒绝';
            }
        }
        $result['condition'] = $condition;
        return $this->response($result);
    }


    //代理商审核下级申请
    public function agentCheck()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $id = isset($this->params['id']) ? $this->params['id'] : 0;
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;
        $reason = isset($this->params['reason']) ? $this->params['reason'] : '';
        $option = isset($this->params['option']) ? $this->params['option'] : 1;
        if($option==1){
            $option = 'pass';
        }else{
            $option = 'refuse';
        }
        $data = [
            'member_id' => $member_id,
            'reason' => $reason
        ];
        $condition = [];
        $condition[] = ['id','=',$id];
        $condition[] = ['member_id','=',$member_id];
        $condition[] = ['check_status','=',0];
        $pintuan_trader_model = new PintuanTraderModel();
        $result = $pintuan_trader_model->pintuanTraderCheck(0,$data,$condition,$option);
        return $this->response($result);
    }

    /**
     * 团长信息
     */
    public function detail()
    {
        $token = $this->checkToken();
        if($token['code'] < 0){
            return $this->response($token);
        }
        $pintuan_trader_model = new PintuanTraderModel();
        $pintuan_trader_info = $pintuan_trader_model->getPintuanTraderInfo([['member_id','=',$this->member_id]]);
        return $this->response($pintuan_trader_info);
    }

    /**
     * 团长申请信息
     */
    public function applyDetail()
    {
        $res = $this->checkToken();
        if($res['code'] < 0){
            return $this->response($res);
        }
        $pintuan_trader_model = new PintuanTraderModel();
        $condition = [];
        $condition[] = ['member_id','=',$this->member_id];
        $condition[] = ['apply_pintuan_trader_level','=','common'];
        $field = 'member_id, apply_pintuan_trader_level, apply_pintuan_trader_level_name, apply_time, check_status, admin_check_time, admin_reject_reason';
        $apply_info = $pintuan_trader_model->getApplyInfo($condition,$field);

        $config_model = new ConfigModel();
        $config_info = $config_model->getPintuanTraderCheckConfig();

        $res = $this->success([
            'apply_info' => $apply_info['data'],
            'config_info' => $config_info['data']['value'],
        ]);

        return $this->response($res);
    }

    /**
     * 代理商申请信息
     */
    public function agentApplyDetail()
    {
        $res = $this->checkToken();
        if($res['code'] < 0){
            return $this->response($res);
        }
        $pintuan_trader_model = new PintuanTraderModel();
        $condition = [];
        $condition[] = ['member_id','=',$this->member_id];
        $condition[] = ['apply_pintuan_trader_level','=', 'partner'];
        $apply_info = $pintuan_trader_model->getApplyInfo($condition,'*');

        $config_model = new ConfigModel();
        $config_info = $config_model->getPintuanTraderCheckConfig();

        $res = $this->success([
            'apply_info' => $apply_info['data'],
            'config_info' => $config_info['data']['value'],
        ]);

        return $this->response($res);
    }

    /**
     * 获取代理商二维码
     * @return false|string
     */
    public function getAgentQrcode()
    {
        $res = $this->checkToken();
        if($res['code'] < 0){
            return $this->response($res);
        }
        $path = $this->params['path'] ?? '';
        $qrcode_model = new QrcodeModel();
        $res = $qrcode_model->getQrcode(['path' => $path, 'member_id' => $this->member_id]);
        return $this->response($res);
    }

    /**
     * 获取代理商海报
     * @return false|string
     */
    public function getAgentPoster()
    {
        $res = $this->checkToken();
        if($res['code'] < 0){
            return $this->response($res);
        }

        $path = $this->params['path'] ?? '';
        $poster_model = new PosterModel();
        $res = $poster_model->getPoster(['path' => $path, 'member_id' => $this->member_id]);
        return $this->response($res);
    }

    /**
     * 获取绑定代理商信息
     */
    public function getBindAgentInfo()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;
        $pintuan_trader_model = new PintuanTraderModel();
        $agent_info = $pintuan_trader_model->getPintuanTraderInfo([['member_id', '=', $member_id]]);
        if(!empty($agent_info['data'])){
            $member_model = new MemberModel();
            $agent_member_info = $member_model->getMemberInfo([['member_id', '=', $member_id]], 'username, headimg, nickname, realname');
            $agent_info['data'] = array_merge($agent_info['data'], $agent_member_info['data']);
        }
        $self_info = $pintuan_trader_model->getPintuanTraderInfo([['member_id', '=', $this->member_id]]);
        return $this->response($this->success([
            'agent_info' => $agent_info['data'],
            'self_info' => $self_info['data'],
        ]));
    }

    //绑定上级代理商
    public function bindAgent()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;

        $pintuan_trader_model = new PintuanTraderModel();
        $result = $pintuan_trader_model->bindAgentPintuanTrader($member_id,$this->member_id);
        return $this->response($result);
    }
}