<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace addon\platformpintuan\api\controller;

use addon\platformpintuan\model\PintuanOrder as PintuanOrderModel;
use app\api\controller\BaseApi;

/**
 * 批发订单
 * @author Administrator
 *
 */
class Order extends BaseApi
{
	
	/**
	 * 批发订单详情
	 * @return string
	 */
	public function detail()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;//批发订单主键id
		if (empty($id)) {
			return $this->response($this->error('', 'REQUEST_ID'));
		}
		$pintuan_order_model = new PintuanOrderModel();
		$res = $pintuan_order_model->getPintuanOrderDetail($id, $this->member_id);
		return $this->response($res);
	}
	
	/**
	 * 批发列表
	 * @return string
	 */
	public function page()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);

        $page_index = isset($this->params['page']) ? $this->params['page'] : 1;
        $page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
        $pintuan_status = isset($this->params['pintuan_status']) ? $this->params['pintuan_status'] : 'all';

		$condition = array();
        $condition[] = [ "ppo.member_id", "=", $this->member_id ];

		if ($pintuan_status != "all") {
			$condition[] = [ "ppo.pintuan_status", "=", $pintuan_status ];
		} else {
			$condition[] = [ "ppo.pintuan_status", "<>", "0" ];//不查询未支付的批发
		}

        $pintuan_order_model = new PintuanOrderModel();
		$res = $pintuan_order_model->getPintuanOrderPageList($condition, $page_index, $page_size, "o.pay_time desc");
		if (!empty($res['data']['list'])) {
			foreach ($res['data']['list'] as $k => $v) {
				$member_list = $pintuan_order_model->getPintuanOrderList([ [ "group_id", "=", $v["group_id"] ] ], "member_img,nickname");
				$res['data']['list'][ $k ]['member_list'] = $member_list['data'];
			}
		}
		return $this->response($res);
	}
}