<?php
// +----------------------------------------------------------------------
// | 店铺端菜单设置
// +----------------------------------------------------------------------
return [

    [
        'name' => 'PROMOTION_PLATFORM_PINTUAN',
        'title' => '团长批发',
        'url' => 'platformpintuan://shop/pintuan/lists',
        'parent' => 'PROMOTION_CENTER',
        'is_show' => 0,
        'sort' => 100,
        'child_list' => [
            [
                'name' => 'PROMOTION_PLATFORM_PINTUAN_LIST',
                'title' => '批发列表',
                'url' => 'platformpintuan://shop/pintuan/lists',
                'is_show' => 1,
                'child_list' => [
                    [
                        'name' => 'PROMOTION_PLATFORM_PINTUAN_DETAIL',
                        'title' => '活动详情',
                        'url' => 'platformpintuan://shop/pintuan/detail',
                        'sort'    => 1,
                        'is_show' => 0
                    ],
                    [
                        'name' => 'PROMOTION_PLATFORM_PINTUAN_GROUP_ORDER',
                        'title' => '批发组订单列表',
                        'url' => 'platformpintuan://shop/pintuan/groupOrder',
                        'sort'    => 1,
                        'is_show' => 0
                    ],
                    [
                        'name' => 'PROMOTION_PLATFORM_PINTUAN_GROUP',
                        'title' => '开团团队',
                        'url' => 'platformpintuan://shop/pintuan/group',
                        'parent' => 'PROMOTION_PLATFORM_PINTUAN',
                        'is_show' => 0,
                        'child_list' => [

                        ]
                    ],
                ]
            ],

        ]
    ],


];