<?php
// 事件定义文件
return [
	'bind' => [

	],

	'listen' => [
		//展示活动
		'ShowPromotion' => [
			'addon\platformpintuan\event\ShowPromotion',
		],
		//订单支付事件
		'OrderPay' => [
			'addon\platformpintuan\event\OrderPay',
		],
		'PromotionType' => [
			'addon\platformpintuan\event\PromotionType',
		],
		//开启批发活动
		'OpenPlatformPintuan' => [
			'addon\platformpintuan\event\OpenPlatformPintuan',
		],
		//关闭批发活动
		'ClosePlatformPintuan' => [
			'addon\platformpintuan\event\ClosePlatformPintuan',
		],
		//关闭批发组
		'ClosePlatformPintuanGroup' => [
			'addon\platformpintuan\event\ClosePlatformPintuanGroup',
		],

		//默认广告位
		'InitAdv' => [
			'addon\platformpintuan\event\InitAdv',
		],
        // 商品列表
        'GoodsListPromotion' => [
            'addon\platformpintuan\event\GoodsListPromotion',
        ],
        //用户账户来源类型
        'MemberAccountFromType' => [
            'addon\platformpintuan\event\MemberAccountFromType',
        ],
        //开启批发竞选
        'StartPlatformPintuanBidding' => [
            'addon\platformpintuan\event\StartPlatformPintuanBidding',
        ],
        //结束批发竞选
        'EndPlatformPintuanBidding' => [
            'addon\platformpintuan\event\EndPlatformPintuanBidding',
        ],
        //商家订单结算完以后的事件
        'ShopOrderCalculate' => [
            'addon\platformpintuan\event\PintuanOrderCommissionCalculate',
        ],
        //订单完成后时间
        'OrderComplete' => [
            'addon\platformpintuan\event\PintuanOrderCommissionSettlement'
        ],
        //批发活动失效事件
        'InvalidPintuan' => [
            'addon\platformpintuan\event\PintuanEndRefundBiddingMoney'
        ],
        //pc导航链接
        'PcNavLink' => [
            'addon\platformpintuan\event\PcNavLink'
        ],
        //营销订单详情
        'PromotionOrderInfo' => [
            'addon\platformpintuan\event\PromotionOrderInfo'
        ],
        //检测相同时间段营销活动
        'CheckSameTimePromotion' => [
            'addon\platformpintuan\event\CheckSameTimePromotion',
        ],
        //修改用户头像
        'ModifyMemberHeadimg' => [
            'addon\platformpintuan\event\DeleteMemberQrcode'
        ],
	],

	'subscribe' => [
	],
];
