<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\platformpintuan\model;

use app\model\BaseModel;
use app\model\system\Cron;
use think\facade\Db;

/**
 * 批发活动
 */
class Pintuan extends BaseModel
{
    //批发类型
    const PINTUAN_TYPE_SHOP = 1;
    const PINTUAN_TYPE_PLATFORM = 2;

    //批发状态
    const PINTUAN_STATUS_NOT_START = 0;
    const PINTUAN_STATUS_IN_PROCESS = 1;
    const PINTUAN_STATUS_ENDED = 2;
    const PINTUAN_STATUS_CLOSED = 3;

    /**
     * 类型
     */
    public static function getPintuanType($id = null)
    {
        $type = [
            self::PINTUAN_TYPE_SHOP  => '普通批发',
            self::PINTUAN_TYPE_PLATFORM => '团长批发',
        ];
        if (is_null($id)) {
            return $type;
        }

        if (isset($type[$id])) {
            $type_name = $type[$id];
        } else {
            $type_name = '';
        }
        return $type_name;
    }

    /**
     * 状态
     */
    public static function getPintuanStatus($id = null)
    {
        $status = [
            self::PINTUAN_STATUS_NOT_START  => '未开始',
            self::PINTUAN_STATUS_IN_PROCESS => '进行中',
            self::PINTUAN_STATUS_ENDED      => '已结束',
            self::PINTUAN_STATUS_CLOSED     => '已关闭（手动）',
        ];
        if (is_null($id)) {
            return $status;
        }

        if (isset($status[$id])) {
            $status_name = $status[$id];
        } else {
            $status_name = '';
        }
        return $status_name;
    }

	/**
	 * 添加批发
	 * @param array $pintuan_data
	 * @param array $sku_list
	 */
	public function addPintuan($pintuan_data, $sku_list)
	{
		$pintuan_data['create_time'] = time();
		
		if ($pintuan_data['pintuan_time'] == 0) {
			return $this->error('', "批发有效期时长不能为0");
		}

		//查询该商品 普通
        $pintuan_type = $pintuan_data['pintuan_type'] ?? self::PINTUAN_TYPE_SHOP;
		if($pintuan_type == self::PINTUAN_TYPE_SHOP){
            $promotion_info = model('promotion_platform_pintuan')->getInfo([
                [ 'site_id', '=', $pintuan_data['site_id'] ],
                [ 'status', 'in', '0,1' ],
                [ 'goods_id', '=', $pintuan_data['goods_id'] ],
                ['', 'exp', Db::raw('not ( (`start_time` > '.$pintuan_data['end_time'].' and `start_time` > '.$pintuan_data['start_time'].' )  or (`end_time` < '.$pintuan_data['start_time'].' and `end_time` < '.$pintuan_data['end_time'].'))')]//todo  修正  所有的优惠都要一样
            ], 'pintuan_id,status');
            if (!empty($promotion_info)) {
                return $this->error('', "当前商品在当前时间段内已经存在批发活动");
            }
		}

		if ($pintuan_data['start_time'] <= time()) {
			$pintuan_data['status'] = 1;
		} else {
			$pintuan_data['status'] = 0;
		}
		
		model("promotion_platform_pintuan")->startTrans();
		
		try {
			
			$pintuan_id = model("promotion_platform_pintuan")->add($pintuan_data);
			
			foreach ($sku_list as $k => $v) {
				
				$data = [
					'pintuan_id' => $pintuan_id,
					'goods_id' => $pintuan_data['goods_id'],
					'sku_id' => $v['sku_id'],
                    'cost_price' => $v['cost_price'] ?? 0.00,
					'pintuan_price' => $v['pintuan_price'],
					'promotion_price' => $v['promotion_price'] == '' ? $v['pintuan_price'] : $v['promotion_price'],
				];
				model("promotion_platform_pintuan_goods")->add($data);
			}
			
			$cron = new Cron();
			if ($pintuan_data['status'] == 1) {
				$cron->addCron(1, 0, "批发活动关闭", "ClosePlatformPintuan", $pintuan_data['end_time'], $pintuan_id);
			} else {
				$cron->addCron(1, 0, "批发活动开启", "OpenPlatformPintuan", $pintuan_data['start_time'], $pintuan_id);
				$cron->addCron(1, 0, "批发活动关闭", "ClosePlatformPintuan", $pintuan_data['end_time'], $pintuan_id);
			}
			
			model('promotion_platform_pintuan')->commit();
			return $this->success($pintuan_id);
			
		} catch (\Exception $e) {
			model('promotion_platform_pintuan')->rollback();
			return $this->error('', $e->getMessage());
		}
	}
	
	/**
	 * 编辑批发
	 * @param unknown $pintuan_id
	 * @param unknown $site_id
	 * @param unknown $pintuan_data
	 * @param unknown $sku_list
	 */
	public function editPintuan($pintuan_id, $site_id, $pintuan_data, $sku_list)
	{
		//查询该商品是否存在批发
		$promotion_info = model('promotion_platform_pintuan')->getInfo([
			[ 'site_id', '=', $site_id ],
			[ 'status', 'in', '0,1' ],
			[ 'goods_id', '=', $pintuan_data['goods_id'] ],
            [ 'pintuan_id', '<>', $pintuan_id],
            [ '', 'exp', Db::raw('not ( (`start_time` > '.$pintuan_data['end_time'].' and `start_time` > '.$pintuan_data['start_time'].' )  or (`end_time` < '.$pintuan_data['start_time'].' and `end_time` < '.$pintuan_data['end_time'].'))')]//todo  修正  所有的优惠都要一样
		], 'pintuan_id,status');
		if (!empty($promotion_info)) {
			if ($promotion_info['pintuan_id'] != $pintuan_id) {
				return $this->error('', "当前商品在当前时间段内已经存在批发活动");
			}
		}
		
		$pintuan_info = model("promotion_platform_pintuan")->getInfo([ [ 'pintuan_id', '=', $pintuan_id ], [ 'site_id', '=', $site_id ] ], 'status');
		if ($pintuan_info['status'] == 1) {
			return $this->error('', "当前活动再进行中，不能修改");
		}
		
		$cron = new Cron();

		if ($pintuan_data['start_time'] <= time()) {
			
			$pintuan_data['status'] = 1;
		} else {
			$pintuan_data['status'] = 0;
		}
		
		$pintuan_data['modify_time'] = time();
		
		$res = model("promotion_platform_pintuan")->update($pintuan_data, [ [ 'pintuan_id', '=', $pintuan_id ], [ 'site_id', '=', $site_id ] ]);
		if ($res) {
			model("promotion_platform_pintuan_goods")->delete([ [ 'pintuan_id', '=', $pintuan_id ] ]);
			foreach ($sku_list as $k => $v) {
				$data = [
					'pintuan_id' => $pintuan_id,
					'goods_id' => $pintuan_data['goods_id'],
					'sku_id' => $v['sku_id'],
					'pintuan_price' => $v['pintuan_price'],
					'promotion_price' => $v['promotion_price'] == '' ? $v['pintuan_price'] : $v['promotion_price']
				];
				model("promotion_platform_pintuan_goods")->add($data);
			}
		}
		if ($pintuan_data['start_time'] <= time()) {
			//活动商品启动
			$this->cronOpenPlatformPintuan($pintuan_id);
			$cron->deleteCron([ [ 'event', '=', 'OpenPlatformPintuan' ], [ 'relate_id', '=', $pintuan_id ] ]);
			$cron->deleteCron([ [ 'event', '=', 'ClosePlatformPintuan' ], [ 'relate_id', '=', $pintuan_id ] ]);
			
			$cron->addCron(1, 0, "批发活动关闭", "ClosePlatformPintuan", $pintuan_data['end_time'], $pintuan_id);
		} else {
			$cron->deleteCron([ [ 'event', '=', 'OpenPlatformPintuan' ], [ 'relate_id', '=', $pintuan_id ] ]);
			$cron->deleteCron([ [ 'event', '=', 'ClosePlatformPintuan' ], [ 'relate_id', '=', $pintuan_id ] ]);
			
			$cron->addCron(1, 0, "批发活动开启", "OpenPlatformPintuan", $pintuan_data['start_time'], $pintuan_id);
			$cron->addCron(1, 0, "批发活动关闭", "ClosePlatformPintuan", $pintuan_data['end_time'], $pintuan_id);
		}
		
		return $this->success($res);
	}
	
	/**
	 * 增加批发组人数及购买人数
	 * @param array $data
	 * @param array $condition
	 * @return array
	 */
	public function editPintuanNum($data = [], $condition = [])
	{
		$res = model('promotion_platform_pintuan')->update($data, $condition);
		return $this->success($res);
	}
	
	/**
	 * 删除批发
	 * @param unknown $pintuan_id
	 * @param unknown $site_id
	 */
	public function deletePintuan($pintuan_id, $site_id)
	{
		$pintuan_info = model("promotion_platform_pintuan")->getInfo([ [ 'pintuan_id', '=', $pintuan_id ], [ 'site_id', '=', $site_id ] ]);
		if ($pintuan_info['status'] == 1) {
			return $this->error('', "当前活动再进行中，不能删除");
		}
		$res = model("promotion_platform_pintuan")->delete([ [ 'pintuan_id', '=', $pintuan_id ], [ 'site_id', '=', $site_id ] ]);
		if ($res) {
			model("promotion_platform_pintuan_goods")->delete([ [ 'pintuan_id', '=', $pintuan_id ] ]);
			$cron = new Cron();
			$cron->deleteCron([ [ 'event', '=', 'OpenPlatformPintuan' ], [ 'relate_id', '=', $pintuan_id ] ]);
			$cron->deleteCron([ [ 'event', '=', 'ClosePlatformPintuan' ], [ 'relate_id', '=', $pintuan_id ] ]);
		}
		return $this->success($res);
	}
	
	/**
	 * 批发失效
	 * @param unknown $pintuan_id
	 * @param unknown $site_id
	 */
	public function invalidPintuan($pintuan_id, $site_id)
	{
		model('promotion_platform_pintuan')->startTrans();
		try {
			$pintuan_info = model("promotion_platform_pintuan")->getInfo([ [ 'pintuan_id', '=', $pintuan_id ], [ 'site_id', '=', $site_id ] ]);
			
			$res = model("promotion_platform_pintuan")->update(
				[ 'status' => 3, 'modify_time' => time() ],
				[ [ 'pintuan_id', '=', $pintuan_id ], [ 'site_id', '=', $site_id ] ]
			);
			
			if ($pintuan_info['group_num'] > 0) {//有人批发
				//查询所有批发组
				$group_model = new PintuanGroup();
				$group_info = $group_model->getPintuanGroupList([ [ 'pintuan_id', '=', $pintuan_id ] ], 'group_id');
				$group = $group_info['data'];
				
				if (!empty($group)) {
					foreach ($group as $v) {
						
						$result = $group_model->cronClosePlatformPintuanGroup($v['group_id']);
						if ($result['code'] < 0) {
							model('promotion_platform_pintuan')->rollback();
							return $result;
						}
					}
				}
			}

			//批发失效
            event('InvalidPintuan', ['pintuan_id' => $pintuan_id]);
			model('promotion_platform_pintuan')->commit();
			return $this->success($res);
			
		} catch (\Exception $e) {
			
			model('promotion_platform_pintuan')->rollback();
			return $this->error($e->getMessage() . $e->getFile() . $e->getLine());
		}
		
	}
	
	/**
	 * 获取批发信息
	 * @param array $condition
	 * @param string $field
	 * @return array
	 */
	public function getPintuanInfo($condition = [], $field = '*')
	{
		//批发信息
		$pintuan_info = model("promotion_platform_pintuan")->getInfo($condition, $field);
		
		return $this->success($pintuan_info);
	}
	
	/**
	 * 获取批发详细信息
	 * @param $pintuan_id
	 * @return array
	 */
	public function getPintuanDetail($pintuan_id)
	{
		//批发信息
		$pintuan_info = model("promotion_platform_pintuan")->getInfo([ [ 'pintuan_id', '=', $pintuan_id ] ]);
		//商品sku信息
		
		$field = 'ps.*,gs.sku_name,gs.price,gs.stock';
		$alias = 'ps';
		$join = [
			[
				'goods_sku gs',
				'ps.sku_id = gs.sku_id',
				'inner'
			]
		];
		$condition[] = [ 'ps.pintuan_id', '=', $pintuan_id ];
		$sku_list = model("promotion_platform_pintuan_goods")->getList($condition, $field, '', $alias, $join);
		$pintuan_info['sku_list'] = $sku_list;
		
		return $this->success($pintuan_info);
	}
	
	/**
	 * 批发商品详情
	 * @param array $condition
	 * @return array
	 */
	public function getPintuanGoodsDetail($condition = [])
	{
		$field = 'ppg.id,ppg.pintuan_id,ppg.goods_id,ppg.sku_id,ppg.pintuan_price,ppg.promotion_price,
		pp.pintuan_name,pp.pintuan_num,pp.start_time,pp.status,pp.end_time,pp.buy_num,pp.is_single_buy,pp.is_promotion,pp.group_num,pp.order_num,pp.pintuan_type,
		sku.site_id,sku.sku_name,sku.sku_spec_format,sku.price,sku.promotion_type,sku.stock,sku.click_num,sku.sale_num,sku.collect_num,sku.sku_image,sku.sku_images,sku.goods_id,sku.site_id,sku.goods_content,sku.goods_state,sku.verify_state,sku.is_virtual,sku.is_free_shipping,sku.goods_spec_format,sku.goods_attr_format,sku.introduction,sku.unit,sku.video_url,sku.evaluate,sku.category_id,sku.category_id_1,sku.category_id_2,sku.category_id_3,sku.category_name';
		$alias = 'ppg';
		$join = [
			[ 'goods_sku sku', 'ppg.sku_id = sku.sku_id', 'inner' ],
			[ 'promotion_platform_pintuan pp', 'ppg.pintuan_id = pp.pintuan_id', 'inner' ],
		];
		$pintuan_goods_info = model('promotion_platform_pintuan_goods')->getInfo($condition, $field, $alias, $join);
		return $this->success($pintuan_goods_info);
	}
	
	/**
	 * 获取批发列表
	 * @param array $condition
	 * @param string $field
	 * @param string $order
	 * @param string $limit
	 */
	public function getPintuanList($condition = [], $field = '*', $order = '', $limit = null)
	{
		
		$list = model('promotion_platform_pintuan')->getList($condition, $field, $order, '', '', '', $limit);
		return $this->success($list);
	}
	
	/**
	 * 获取批发分页列表
	 * @param array $condition
	 * @param number $page
	 * @param string $page_size
	 * @param string $order
	 * @param string $field
	 */
	public function getPintuanPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
	{
		$field = 'p.*,gs.sku_name, gs.sku_image, g.goods_name,g.goods_image';
		$alias = 'p';
		$join = [
            [
                'promotion_platform_pintuan_goods ppg',
                'p.pintuan_id = ppg.pintuan_id',
                'inner'
            ],
            [
                'goods_sku gs',
                'ppg.sku_id = gs.sku_id',
                'inner'
            ],
			[
				'goods g',
				'p.goods_id = g.goods_id',
				'inner'
			]
		];
		
		$list = model('promotion_platform_pintuan')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
		return $this->success($list);
	}
	
	/**
	 * 获取批发商品分页列表
	 * @param array $condition
	 * @param number $page
	 * @param string $page_size
	 * @param string $order
	 * @param string $field
	 */
	public function getPintuanGoodsPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'pp.is_recommend desc,pp.start_time desc', $field = 'pp.site_id,pp.pintuan_name,pp.is_virtual_goods,pp.pintuan_num,pp.pintuan_time,pp.is_recommend,pp.status,pp.group_num,pp.order_num,ppg.id,ppg.pintuan_id,ppg.pintuan_price,ppg.promotion_price,sku.sku_id,sku.sku_name,sku.price,sku.sku_image,g.goods_id,g.goods_name', $alias = '', $join = '')
	{
		$list = model('promotion_platform_pintuan')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
		return $this->success($list);
	}
	
	/**
	 * 开启批发活动
	 * @param $pintuan_id
	 * @return array|\multitype
	 */
	public function cronOpenPlatformPintuan($pintuan_id)
	{
		$pintuan_info = model('promotion_platform_pintuan')->getInfo([
				[ 'pintuan_id', '=', $pintuan_id ] ]
			, 'sku_id,goods_id,start_time,status,pintuan_type'
		);
		if (!empty($pintuan_info)) {
			if ($pintuan_info['start_time'] <= time() && $pintuan_info['status'] == 0) {
				$res = model('promotion_platform_pintuan')->update([ 'status' => 1 ], [ [ 'pintuan_id', '=', $pintuan_id ] ]);
				if($pintuan_info["pintuan_type"] == self::PINTUAN_TYPE_SHOP){
				    //商品和所有规格都标识为批发 关联同一个活动id
				    //model('goods')->update(['promotion_type' => 'platformpintuan', 'promotion_id' => $pintuan_id], [['goods_id', '=', $pintuan_info['goods_id']]]);
				    //model('goods_sku')->update(['promotion_type' => 'platformpintuan', 'promotion_id' => $pintuan_id], [['goods_id', '=', $pintuan_info['goods_id']]]);
                }else{
				    //商品和所有规格都标识为批发 对应的sku关联活动id
                    model('goods')->update(['promotion_type' => 'pintuan_platform', 'promotion_id' => $pintuan_id], [['sku_id', '=', $pintuan_info['sku_id']]]);
                    model('goods_sku')->update(['promotion_type' => 'pintuan_platform'], [['goods_id', '=', $pintuan_info['goods_id']], ['promotion_type', '<>', 'pintuan_platform']]);
                    model('goods_sku')->update(['promotion_id' => $pintuan_id], [['sku_id', '=', $pintuan_info['sku_id']]]);
                }
				return $this->success($res);
			} else {
				return $this->error("", "批发活动已开启或者关闭");
			}
		} else {
			return $this->error("", "批发活动不存在");
		}
		
	}
	
	/**
	 * 关闭批发活动
	 * @param $pintuan_id
	 * @return array|\multitype
	 */
	public function cronClosePlatformPintuan($pintuan_id)
	{
		$pintuan_info = model('promotion_platform_pintuan')->getInfo([
				[ 'pintuan_id', '=', $pintuan_id ] ]
			, 'site_id,start_time,status,pintuan_type,goods_id,sku_id'
		);
		if (!empty($pintuan_info)) {
			if($pintuan_info['pintuan_type'] == self::PINTUAN_TYPE_SHOP){
			    //将活动类型和活动id全部清空
                //model('goods')->update(['promotion_type' => '', 'promotion_id' => 0], [['goods_id', '=', $pintuan_info['goods_id']]]);
                //model('goods_sku')->update(['promotion_type' => '', 'promotion_id' => 0], [['goods_id', '=', $pintuan_info['goods_id']]]);
            }else{
			    //先清空规格的关联关系，如果商品的所有规格都已没有活动 则商品取消取消关联
                model('goods_sku')->update(['promotion_id' => 0], [['goods_id', '=', $pintuan_info['goods_id']]]);
                $count = model('goods_sku')->getCount([['promotion_type', '=', 'pintuan_platform'], ['promotion_id', '=', $pintuan_id]]);
                if($count == 0){
                    model('goods')->update(['promotion_type' => '', 'promotion_id' => 0], [['goods_id', '=', $pintuan_info['goods_id']]]);
                }
			}
			return $this->invalidPintuan($pintuan_id, $pintuan_info['site_id']);
		} else {
			return $this->error("", "批发活动不存在");
		}
	}
}