<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\platformpintuan\model\PintuanTrader;

use app\model\BaseModel;
use app\model\system\Cron;
use app\model\system\Promotion as PromotionModel;
use think\facade\Db;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use app\model\member\Member as MemberModel;
use app\model\member\MemberAccount;

/**
 * 批发竞标
 */
class PintuanBidding extends BaseModel
{
    //竞标状态
    const BIDDING_STATUS_NOT_START = 0;
    const BIDDING_STATUS_IN_PROCESS = 1;
    const BIDDING_STATUS_ENDED = 2;
    const BIDDING_STATUS_CLOSED = 3;

    //参与状态
    const ENTER_STATUS_AUDIT = 0;
    const ENTER_STATUS_SUCCESS = 1;
    const ENTER_STATUS_FAIL = 2;

    /**
     * 竞标状态
     */
    public static function getBiddingStatus($id = null)
    {
        $status = [
            self::BIDDING_STATUS_NOT_START  => '未开始',
            self::BIDDING_STATUS_IN_PROCESS => '进行中',
            self::BIDDING_STATUS_ENDED      => '已结束',
            self::BIDDING_STATUS_CLOSED     => '已关闭（手动）',
        ];
        if (is_null($id)) {
            return $status;
        }

        if (isset($status[$id])) {
            $status_name = $status[$id];
        } else {
            $status_name = '';
        }
        return $status_name;
    }

    /**
     *  参与状态
     */
    public static function getEnterStatus($id = null)
    {
        $status = [
            self::ENTER_STATUS_AUDIT   => '审核中',
            self::ENTER_STATUS_SUCCESS => '竞标成功',
            self::ENTER_STATUS_FAIL    => '竞标失败',
        ];
        if (is_null($id)) {
            return $status;
        }

        if (isset($status[$id])) {
            $status_name = $status[$id];
        } else {
            $status_name = '';
        }
        return $status_name;
    }

    /**
     * 添加拼团竞标
     */
    public function addPintuanBidding($param)
    {
        if (time() > $param['bidding_end_time']) return $this->error('', '竞标结束时间不可早于当前时间');
        if ($param['bidding_start_time'] > $param['bidding_end_time']) return $this->error('', '竞标开始时间不可晚于结束时间');
        if ($param['start_time'] < $param['bidding_end_time']) return $this->error('', '拼团开始时间不可早于竞标结束时间');
        if ($param['start_time'] > $param['end_time']) return $this->error('', '拼团开始时间不可晚于拼团结束时间');

        if (time() > $param['bidding_start_time']) {
            $bidding_status = self::BIDDING_STATUS_IN_PROCESS;
        } else {
            $bidding_status = self::BIDDING_STATUS_NOT_START;
        }

        //检测相同时间短营销活动
        $promotion_model = new PromotionModel();
        $same_time_promotion = $promotion_model->checkSameTimePromotion([
            'mode' => 'sku',
            'id' => $param['sku_id'],
            'time_info' => [
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
            ],
        ]);
        if(!empty($same_time_promotion)){
            return $this->error('', '该商品在相同时间已存在营销活动');
        }

        $sku_info = model('goods_sku')->getInfo([['sku_id', '=', $param['sku_id']]], 'site_id, site_name, goods_id, sku_id, sku_name, sku_image, price, cost_price, goods_class');
        if (empty($sku_info)) return $this->error('商品信息有误');
        $is_virtual_goods = $sku_info['goods_class'] == 2 ? 1 : 0;

        //拼团配置
        $config_model = new Config();
        $config_info = $config_model->getPintuanTraderConfig();

        $bidding_data = [
            'site_id'            => $sku_info['site_id'],
            'site_name'          => $sku_info['site_name'],
            'pintuan_name'       => $param['pintuan_name'],
            'goods_id'           => $sku_info['goods_id'],
            'sku_id'             => $param['sku_id'],
            'sku_name'           => $sku_info['sku_name'],
            'sku_image'          => $sku_info['sku_image'],
            'price'              => $sku_info['price'],
            'cost_price'         => $sku_info['cost_price'],
            'is_virtual_goods'   => $is_virtual_goods,
            'remark'             => $param['remark'],
            'create_time'        => time(),
            'is_recommend'       => $param['is_recommend'],
            'bidding_start_time' => $param['bidding_start_time'],
            'bidding_end_time'   => $param['bidding_end_time'],
            'start_time'         => $param['start_time'],
            'end_time'           => $param['end_time'],
            'is_single_buy'      => $param['is_single_buy'],
            'is_virtual_buy'     => $param['is_virtual_buy'],
            'bidding_level_rule' => $param['bidding_level_rule'],
            'bidding_status'     => $bidding_status,
            'deposit_rate'       => $config_info['data']['value']['bidding_deposit_rate'],
        ];
        $bidding_id = model('promotion_platform_pintuan_bidding')->add($bidding_data);

        //添加自动开始和结束竞标事件
        $cron = new Cron();
        if ($bidding_status == self::BIDDING_STATUS_NOT_START) {
            $cron->addCron(1, 0, "拼团竞标开始", "StartPlatformPintuanBidding", $param['bidding_start_time'], $bidding_id);
        }
        $cron->addCron(1, 0, "拼团竞标结束", "EndPlatformPintuanBidding", $param['bidding_end_time'], $bidding_id);

        return $this->success($bidding_id);
    }

    /**
     * 编辑拼团竞标
     */
    public function editPintuanBidding($bidding_id, $param)
    {
        $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['bidding_id', '=', $bidding_id]], 'bidding_start_time, bidding_end_time');
        if (empty($bidding_info)) return $this->error('', '竞标信息有误');
        if ($bidding_info['bidding_end_time'] < time()) return $this->error('', '竞标已结束');
        if ($bidding_info['bidding_start_time'] < time()) return $this->error('', '竞标中不可修改');

        if (time() > $param['bidding_end_time']) return $this->error('', '竞标结束时间不可早于当前时间');
        if ($param['bidding_start_time'] > $param['bidding_end_time']) return $this->error('', '竞标开始时间不可早于结束时间');

        if (time() > $param['bidding_start_time']) {
            $bidding_status = self::BIDDING_STATUS_IN_PROCESS;
        } else {
            $bidding_status = self::BIDDING_STATUS_NOT_START;
        }

        //检测相同时间短营销活动
        $promotion_model = new PromotionModel();
        $same_time_promotion = $promotion_model->checkSameTimePromotion([
            'mode' => 'sku',
            'id' => $param['sku_id'],
            'time_info' => [
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
            ],
            'uncheck_condition' => json_encode([
                'promotion_type' => 'platformpintuan',
                'promotion_id' => $param['bidding_id'],
                'mode' => 'sku',
                'id' => $param['sku_id'],
            ]),
        ]);
        if(!empty($same_time_promotion)){
            return $this->error('', '该商品在相同时间已存在营销活动');
        }

        $sku_info = model('goods_sku')->getInfo([['sku_id', '=', $param['sku_id']]], 'site_id, site_name, goods_id, sku_id, sku_name, sku_image, price, cost_price, goods_class');
        if (empty($sku_info)) return $this->error('商品信息有误');
        $is_virtual_goods = $sku_info['goods_class'] == 2 ? 1 : 0;

        //拼团配置
        $config_model = new Config();
        $config_info = $config_model->getPintuanTraderConfig();

        $bidding_data = [
            'site_id'            => $sku_info['site_id'],
            'site_name'          => $sku_info['site_name'],
            'pintuan_name'       => $param['pintuan_name'],
            'goods_id'           => $sku_info['goods_id'],
            'sku_id'             => $param['sku_id'],
            'sku_name'           => $sku_info['sku_name'],
            'sku_image'          => $sku_info['sku_image'],
            'price'              => $sku_info['price'],
            'cost_price'         => $sku_info['cost_price'],
            'is_virtual_goods'   => $is_virtual_goods,
            'remark'             => $param['remark'],
            'modify_time'        => time(),
            'is_recommend'       => $param['is_recommend'],
            'bidding_start_time' => $param['bidding_start_time'],
            'bidding_end_time'   => $param['bidding_end_time'],
            'start_time'         => $param['start_time'],
            'end_time'           => $param['end_time'],
            'is_single_buy'      => $param['is_single_buy'],
            'is_virtual_buy'     => $param['is_virtual_buy'],
            'bidding_level_rule' => $param['bidding_level_rule'],
            'bidding_status'     => $bidding_status,
            'deposit_rate'       => $config_info['data']['value']['bidding_deposit_rate'],
        ];

        //竞标状态
        $res = model('promotion_platform_pintuan_bidding')->update($bidding_data, [['bidding_id', '=', $bidding_id]]);

        //更新自动开始和结束竞标事件
        $cron = new Cron();
        $cron->deleteCron([['event', '=', 'StartPlatformPintuanBidding'], ['relate_id', '=', $bidding_id]]);
        $cron->deleteCron([['event', '=', 'EndPlatformPintuanBidding'], ['relate_id', '=', $bidding_id]]);
        if ($bidding_status == self::BIDDING_STATUS_NOT_START) {
            $cron->addCron(1, 0, "拼团竞标开始", "StartPlatformPintuanBidding", $param['bidding_start_time'], $bidding_id);
        }
        $cron->addCron(1, 0, "拼团竞标结束", "EndPlatformPintuanBidding", $param['bidding_end_time'], $bidding_id);

        return $this->success($res);
    }

    /**
     * 删除拼团竞标
     */
    public function deletePintuanBidding($bidding_id)
    {
        $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['bidding_id', '=', $bidding_id]], 'pintuan_trader_id, bidding_status');
        if (empty($bidding_info)) return $this->error('', '竞标信息有误');
        if (!($bidding_info['bidding_status'] == self::BIDDING_STATUS_NOT_START || ($bidding_info['bidding_status'] == self::BIDDING_STATUS_ENDED && $bidding_info['pintuan_trader_id'] == 0))) {
            return $this->error('', '只有未开始的和结束未中标的可以删除');
        }

        $res = model('promotion_platform_pintuan_bidding')->delete([['bidding_id', '=', $bidding_id]]);
        return $this->success($res);
    }

    /**
     * 参与拼团竞标
     */
    public function pintuanBiddingEnter($params)
    {
        $bidding_id = $params['bidding_id'] ?? 0;
        $pintuan_trader_id = $params['pintuan_trader_id'] ?? 0;
        $bidding_level = $params['bidding_level'] ?? 0;
        $pay_password = $params['pay_password'] ?? '';

        $enter_count = model('promotion_platform_pintuan_bidding_enter')->getCount([['bidding_id', '=', $bidding_id], ['pintuan_trader_id', '=', $pintuan_trader_id]]);
        if (!empty($enter_count)) return $this->error('', '已参与竞选，请勿重复提交');

        $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['bidding_id', '=', $bidding_id]],
            'bidding_id, pintuan_name, bidding_start_time, bidding_end_time, bidding_level_rule, goods_id, start_time, end_time');
        if (empty($bidding_info)) return $this->error('', '竞标信息有误');
        if (time() < $bidding_info['bidding_start_time']) return $this->error('', '竞标未开始');
        if (time() > $bidding_info['bidding_end_time']) return $this->error('', '竞标已结束');

        $bidding_level_rule = json_decode($bidding_info['bidding_level_rule'], true);
        if (!isset($bidding_level_rule[$bidding_level])) return $this->error('', '等级信息有误');

        $pintuan_trader_info = model('pintuan_trader')->getInfo([['member_id', '=', $pintuan_trader_id]], 'member_id');
        if(empty($pintuan_trader_info)) return $this->error('', '团长信息有误');

        $member_info = model('member')->getInfo([['member_id', '=', $pintuan_trader_id]], 'member_id, username, balance, balance_money');
        if (empty($member_info)) return $this->error('', '用户信息有误');

        //检测支付密码
        $member_model = new MemberModel();
        $check_res    = $member_model->checkPayPassword($pintuan_trader_id, $pay_password);
        if ($check_res['code'] < 0) return $check_res;

        $config_model = new Config;
        $config_info = $config_model->getPintuanTraderConfig()['data']['value'];
        $deposit_rate = $config_info['bidding_deposit_rate'];
        if($deposit_rate < 0 || $deposit_rate > 100){
            return $this->error('', '保证金设置有误');
        }

        //检测账户余额
        $bidding_level_content     = $bidding_level_rule[$bidding_level];
        $bidding_money = $bidding_level_content['bidding_num'] * $bidding_level_content['cost_price'] * $config_info['bidding_deposit_rate'] / 100;

        if ($member_info['balance'] + $member_info['balance_money'] < $bidding_money) {
            return $this->error('', '账户余额不足');
        }

        model('promotion_platform_pintuan_bidding_enter')->startTrans();
        try {
            $enter_data = [
                'bidding_id'          => $bidding_info['bidding_id'],
                'pintuan_trader_id'   => $pintuan_trader_id,
                'pintuan_trader_name' => $member_info['username'],
                'enter_time'          => time(),
                'bidding_level'       => $bidding_level,
                'enter_status'        => self::ENTER_STATUS_AUDIT,
                'bidding_money'       => $bidding_money,
            ];
            $enter_id   = model('promotion_platform_pintuan_bidding_enter')->add($enter_data);

            model('promotion_platform_pintuan_bidding')->setInc([['bidding_id', '=', $bidding_id]], 'enter_num');

            //使用余额
            $member_account = new MemberAccount();
            if ($member_info['balance'] >= $bidding_money) {
                $use_balance       = $bidding_money;
                $use_balance_money = 0;
            } else {
                $use_balance       = $member_info['balance'];
                $use_balance_money = $bidding_money - $member_info['balance'];
            }
            if ($use_balance > 0) {
                $res = $member_account->addMemberAccount($pintuan_trader_id, 'balance', -1 * $use_balance, 'pintuan_bidding', $bidding_id, "竞标【{$bidding_info['pintuan_name']}】支付余额");
                if ($res['code'] < 0) {
                    model('promotion_platform_pintuan_bidding_enter')->rollback();
                    return $res;
                }
            }
            if ($use_balance_money > 0) {
                $res = $member_account->addMemberAccount($pintuan_trader_id, 'balance_money', -1 * $use_balance_money, 'pintuan_bidding', $bidding_id, "竞标【{$bidding_info['pintuan_name']}】支付余额");
                if ($res['code'] < 0) {
                    model('promotion_platform_pintuan_bidding_enter')->rollback();
                    return $res;
                }
            }

            model('promotion_platform_pintuan_bidding_enter')->commit();
            return $this->success($enter_id);
        } catch (\Exception $e) {
            model('promotion_platform_pintuan_bidding_enter')->rollback();
            return $this->error('', $e->getMessage() . $e->getFile() . $e->getLine());
        }
    }

    /**
     * 选择团长
     */
    public function selectPintuanBiddingEnter($enter_ids)
    {
        try{
            //linux环境对in条件检测比较严格 必须转换为字符串类型
            $enter_ids = (string)$enter_ids;
            $enter_list = model('promotion_platform_pintuan_bidding_enter')->getList([['enter_id', 'in', $enter_ids]]);
            if (empty($enter_list)) return $this->error('', '参与信息有误');

            $bidding_id = 0;
            $bidding_level = null;
            $pintuan_trader_id_arr = [];
            foreach($enter_list as $key=>$val){
                if($bidding_id == 0){
                    $bidding_id = $val['bidding_id'];
                }else{
                    if($val['bidding_id'] != $bidding_id){
                        return $this->error('', '竞标活动不一致');
                    }
                }
                if(is_null($bidding_level)){
                    $bidding_level = $val['bidding_level'];
                }else{
                    if($bidding_level != $val['bidding_level']){
                        return $this->error('', '只能选择同一等级的竞标团长');
                    }
                }
                $pintuan_trader_id_arr[] = $val['pintuan_trader_id'];
            }

            $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['bidding_id', '=', $bidding_id]]);
            if (empty($bidding_info)) return $this->error('', '竞标信息有误');
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }

        model('promotion_platform_pintuan_bidding_enter')->startTrans();
        try {
            model('promotion_platform_pintuan_bidding_enter')->update(['enter_status' => self::ENTER_STATUS_SUCCESS], [['enter_id', 'in', $enter_ids]]);
            model('promotion_platform_pintuan_bidding_enter')->update(['enter_status' => self::ENTER_STATUS_FAIL], [['bidding_id', '=', $bidding_id], ['enter_id', 'not in', $enter_ids]]);
            model('promotion_platform_pintuan_bidding')->update([
                'bidding_level'       => $bidding_level,
                'bidding_status'      => self::BIDDING_STATUS_ENDED,
                'bidding_success_num' => count($enter_list),
            ], [['bidding_id', '=', $bidding_id]]);

            //退回竞标失败团长的支付余额
            $member_account = new MemberAccount();
            $account_list   = model('member_account')->getList([['type_tag', '=', $bidding_id], ['account_type', 'in', 'balance,balance_money'], ['from_type', '=', 'pintuan_bidding'], ['member_id', 'not in', $pintuan_trader_id_arr]]);
            foreach ($account_list as $val) {
                $res = $member_account->addMemberAccount($val['member_id'], $val['account_type'], -1 * $val['account_data'], 'pintuan_bidding_fail_refund', $bidding_id, "竞标【{$bidding_info['pintuan_name']}】失败退回支付余额");
                if ($res['code'] < 0) {
                    model('promotion_platform_pintuan_bidding_enter')->rollback();
                    return $res;
                }
            }

            $bidding_level_rule    = json_decode($bidding_info['bidding_level_rule'], true);
            $bidding_level_content = $bidding_level_rule[$bidding_level];
            $pintuan_time_data = $bidding_level_content['pintuan_time'];
            $pintuan_time = 60 * 24 * $pintuan_time_data['day'] + 60 * $pintuan_time_data['hour'] + $pintuan_time_data['minute'];

            $pintuan_data  = [
                'site_id'          => $bidding_info['site_id'],
                'site_name'        => $bidding_info['site_name'],
                'pintuan_name'     => $bidding_info['pintuan_name'],
                'goods_id'         => $bidding_info['goods_id'],
                'sku_id'           => $bidding_info['sku_id'],
                'is_virtual_goods' => $bidding_info['is_virtual_goods'],
                'pintuan_num'      => $bidding_level_content['pintuan_num'],//几人团
                'pintuan_time'     => $pintuan_time,//拼团有效期 分钟
                'remark'           => $bidding_info['remark'],
                'is_recommend'     => $bidding_info['is_recommend'],
                'start_time'       => $bidding_info['start_time'],
                'end_time'         => $bidding_info['end_time'],
                'buy_num'          => $bidding_level_content['buy_num'],//限购数量
                'is_single_buy'    => $bidding_info['is_single_buy'],
                'is_virtual_buy'   => $bidding_info['is_virtual_buy'],
                'is_promotion'     => 0,
                'pintuan_type'     => PintuanModel::PINTUAN_TYPE_PLATFORM,
            ];
            $pintuan_model = new PintuanModel();

            $sku_info = [
                'goods_id' => $bidding_info['goods_id'],
                'sku_id' => $bidding_info['sku_id'],
                'cost_price' => $bidding_level_content['cost_price'],
                'pintuan_price' => $bidding_level_content['pintuan_price'],
                'promotion_price' => $bidding_level_content['pintuan_price'],
            ];
            $res = $pintuan_model->addPintuan($pintuan_data, [$sku_info]);
            if ($res['code'] < 0) {
                model('promotion_platform_pintuan_bidding_enter')->rollback();
                return $res;
            }

            //关联拼团
            $pintuan_id = $res['data'];
            model('promotion_platform_pintuan_bidding')->update(['pintuan_id' => $pintuan_id,], [['bidding_id', '=', $bidding_id]]);
            model('promotion_platform_pintuan_bidding_enter')->update(['pintuan_id' => $pintuan_id], [['enter_id', 'in', $enter_ids]]);

            model('promotion_platform_pintuan_bidding_enter')->commit();
            return $this->success();
        } catch (\Exception $e) {
            model('promotion_platform_pintuan_bidding_enter')->rollback();
            trace('竞标结束报错---' . $e->getMessage() . $e->getFile() . $e->getLine());
            return $this->error('', $e->getMessage() . $e->getFile() . $e->getLine());
        }
    }

    /**
     * 自动执行 开启拼团竞标
     */
    public function cronStartPlatformPintuanBidding($bidding_id)
    {
        $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['bidding_id', '=', $bidding_id]]);
        if (empty($bidding_info)) return $this->error('', '竞标信息有误');
        //if(time() < $bidding_info['bidding_start_time']) return $this->error('', '竞标未开始');

        //更改状态
        model('promotion_platform_pintuan_bidding')->update(['bidding_status' => self::BIDDING_STATUS_IN_PROCESS], [['bidding_id', '=', $bidding_id]]);
        return $this->success();
    }

    /**
     * 自动执行 结束拼团竞标
     */
    public function cronEndPlatformPintuanBidding($bidding_id)
    {
        trace('进入竞标自动结束事件');
        $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['bidding_id', '=', $bidding_id], ['bidding_status', '=', self::BIDDING_STATUS_IN_PROCESS]]);
        if (empty($bidding_info)) return $this->error('', '竞标信息有误');
        //if(time() < $bidding_info['bidding_end_time']) return $this->error('', '竞标未结束');

        //更改状态
        model('promotion_platform_pintuan_bidding')->update(['bidding_status' => self::BIDDING_STATUS_ENDED], [['bidding_id', '=', $bidding_id]]);


        //已经自动选择团长，不再执行选择团长过程 直接返回成功 by wangds 20210424
        /*
        $pintuan_trader = model('promotion_platform_pintuan_bidding_enter')->getFirstData([['bidding_id', '=', $bidding_id]], 'enter_id', 'bidding_level desc, enter_time asc');
        trace('被选中的团长');
        trace($pintuan_trader);
        if (empty($pintuan_trader)) {
            return $this->error('', '没有团长参与竞选');
        }

        return $this->selectPintuanBiddingEnter($pintuan_trader['enter_id']);
        */
        return $this->success();
    }

    /**
     * 拼团结束退还团长竞标金额
     * @param $pintuan_id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function pintuanEndRefundBiddingMoney($pintuan_id)
    {
        $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['pintuan_id', '=', $pintuan_id]]);
        if(empty($bidding_info)) return $this->error('', '竞标信息有误');





































































































        $member_account = new MemberAccount();
        //竞标人有多个
        $enter_id_arr = model('promotion_platform_pintuan_bidding_enter')->getColumn([['bidding_id', '=', $bidding_info['bidding_id']], ['enter_status', '=', self::ENTER_STATUS_SUCCESS]], 'pintuan_trader_id');
        if(!empty($enter_id_arr)){
            $account_list   = model('member_account')->getList([['type_tag', '=', $bidding_info['bidding_id']], ['account_type', 'in', 'balance,balance_money'], ['from_type', '=', 'pintuan_bidding'], ['member_id', 'in', $enter_id_arr]]);
            foreach ($account_list as $val) {
                $res = $member_account->addMemberAccount($val['member_id'], $val['account_type'], -1 * $val['account_data'], 'pintuan_end_refund_bidding_money', $bidding_info['bidding_id'], "拼团活动【{$bidding_info['pintuan_name']}】结束退回竞标余额");
                if ($res['code'] < 0) {
                    model('promotion_platform_pintuan_bidding_enter')->rollback();
                    return $res;
                }
            }
        }

        return $this->success();
    }

    /**
     * 获取拼团竞标列表
     */
    public function getPintuanBiddingList($condition = [], $field = true, $order = '', $alias = 'a', $join = [])
    {
        $list = model('promotion_platform_pintuan_bidding')->getList($condition, $field, $order, $alias, $join);
        return $this->success($list);
    }

    /**
     * 获取拼团竞标分页列表
     */
    public function getPintuanBiddingPageList($condition = [], $field = true, $order = '', $page = 1, $list_rows = PAGE_LIST_ROWS, $alias = 'a', $join = [])
    {
        $res = model('promotion_platform_pintuan_bidding')->pageList($condition, $field, $order, $page, $list_rows, $alias, $join);
        foreach ($res['list'] as $key => $val) {
            if (isset($val['bidding_status'])) $res['list'][$key]['bidding_status_name'] = self::getBiddingStatus($val['bidding_status']);
        }
        return $this->success($res);
    }

    /**
     * 获取拼团竞标信息
     */
    public function getPintuanBiddingInfo($condition, $field = '*')
    {
        $info = model('promotion_platform_pintuan_bidding')->getInfo($condition, $field);
        if (isset($info['bidding_status'])) $info['bidding_status_name'] = self::getBiddingStatus($info['bidding_status']);
        return $this->success($info);
    }

    /**
     * 获取参与者列表
     */
    public function getPintuanBiddingEnterList($condition = [], $field = true, $order = '', $alias = 'a', $join = [])
    {
        $list = model('promotion_platform_pintuan_bidding_enter')->getList($condition, $field, $order, $alias, $join);
        return $this->success($list);
    }

    /**
     * 获取参与者分页列表
     */
    public function getPintuanBiddingEnterPageList($condition = [], $field = true, $order = '', $page = 1, $list_rows = PAGE_LIST_ROWS, $alias = 'a', $join = [])
    {
        $res = model('promotion_platform_pintuan_bidding_enter')->pageList($condition, $field, $order, $page, $list_rows, $alias, $join);
        foreach ($res['list'] as $key => $val) {
            if (isset($val['enter_status'])) {
                $res['list'][$key]['enter_status_name'] = self::getEnterStatus($val['enter_status']);
            }
        }
        return $this->success($res);
    }

    /**
     * 获取参与者信息
     */
    public function getPintuanBiddingEnterInfo($condition, $field = '*')
    {
        $info = model('promotion_platform_pintuan_bidding_enter')->getInfo($condition, $field);
        return $this->success($info);
    }
}