<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\platformpintuan\model\PintuanTrader;

use app\model\BaseModel;
use extend\QRcode as QRcodeExtend;

/**
 * 二维码生成类
 */
class Poster extends BaseModel
{
    /**
     * 获取海报目录
     * @return string
     */
    public function getPosterDir()
    {
        $dir = "upload/poster/platformpintuan/";
        return $dir;
    }

    /**
     * 获取海报
     * @param $param
     * @return array
     */
    public function getPoster($param)
    {
        $path = $param['path'];
        $member_id = $param['member_id'];

        if(empty($path)) return $this->error('', 'path参数不可为空');
        if(empty($member_id)) return $this->error('', 'member_id参数不可为空');

        $poster_dir = $this->getPosterDir();
        //检测并创建目录 递归创建
        if(!is_dir($poster_dir)){
            mkdir($poster_dir, '0777', true);
        }
        $poster_path = "{$poster_dir}/member_{$member_id}.png";
        $config_model = new Config();
        $config_info = $config_model->getPosterConfig()['data']['value'];
        if(!is_file($poster_path)){
            try {
                //生成带头像的二维码
                $member_info = model('member')->getInfo([['member_id', '=', $member_id]], 'headimg');
                if(empty($member_info)){
                    return $this->error('', '用户信息有误');
                }
                $logo_path = $member_info['headimg'];
                $qrcode_path = "upload/qrcode/platformpintuan/member_{$member_id}.png";
                $res = getQrcode(['code_text' => $path, 'code_out_file' => $qrcode_path, 'logo_path' => $logo_path]);
                if($res['code'] < 0) return $res;

                //生成海报
                if(empty($config_info['bg_url'])){
                    return $this->error('', '平台未设置海报背景，请联系平台管理员');
                }
                $bg_url = $config_info['bg_url'];
                $poster = file_get_contents($bg_url);
                if(empty($poster)){
                    return $this->error('', '海报背景无法读取，请联系平台管理员');
                }
                $qrcode = file_get_contents($qrcode_path);
                if(empty($poster)){
                    return $this->error('', '二维码生成有误');
                }

                //将海报和二维码合并
                $poster = imagecreatefromstring($poster);
                $qrcode = imagecreatefromstring($qrcode);
                $poster_real_width = imagesx($poster);
                $qrcode_real_width = imagesx($qrcode);
                $qrcode_real_height = imagesy($qrcode);

                //计算绘制海报上二维码的实际位置和大小
                $scale = $poster_real_width / $config_info['bg_width'];
                $qrcode_width = $scale * $config_info['code_size'];
                $qrcode_height = $qrcode_width;
                $qrcode_pos_x = $scale * $config_info['code_pos_x'];
                $qrcode_pos_y = $scale * $config_info['code_pos_y'];

                imagecopyresampled($poster, $qrcode, $qrcode_pos_x, $qrcode_pos_y, 0, 0, $qrcode_width, $qrcode_height, $qrcode_real_width, $qrcode_real_height);
                header('Content-type: image/png');
                imagepng($poster, $poster_path);
                imagedestroy($poster);

            } catch (\Exception $e) {
                return error(-1, $e->getMessage() . $e->getLine());
            }
        }
        return success(0, '操作成功', ['poster_path' => img($poster_path), 'config_info' => $config_info]);
    }

    /**
     * 删除海报
     * @param $param
     */
    public function deletePoster($param)
    {
        //检测设置值是否有改变 没有则不删除
        $old_value = $param['old_value'] ?? [];
        $new_value = $param['new_value'] ?? [];
        $is_change = false;
        foreach($old_value as $key=>$val){
            if($val != $new_value[$key]){
                $is_change = true;
                break;
            }
        }
        if($is_change == false){
            return $this->error('', '设置没有更改，无需删除海报图片');
        }

        //执行删除
        $poster_dir = $this->getPosterDir();
        $list = glob("{$poster_dir}/*");
        if(!empty($list)){
            foreach($list as $file_path){
                if(is_file($file_path)){
                    unlink($file_path);
                }
            }
        }
        return $this->success();
    }

    /**
     * 删除用户二维码
     * @param $param
     */
    public function deleteMemberPoster($param)
    {
        $member_id = $param['member_id'];
        $poster_dir = $this->getPosterDir();
        $poster_path = "{$poster_dir}/member_{$member_id}.png";
        if(file_exists($poster_path)){
            unlink($poster_path);
        }
        return $this->success();
    }
}