<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\platformpintuan\model\PintuanTrader;

use app\model\BaseModel;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use addon\platformpintuan\model\PintuanTrader\Config as ConfigModel;
use app\model\member\MemberAccount;
use think\facade\Db;

/**
 * 批发佣金
 */
class PintuanCommission extends BaseModel
{
    /**
     * 订单计算
     * @param $order_data
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function calculate($param)
    {
        try{
            $order_id = $param['order_id'];
            $order_info = model('order')->getInfo([['order_id', '=', $order_id]]);
            $promotion_type = $order_info['promotion_type'];
            if($promotion_type != 'platformpintuan'){
                return $this->error('', '非批发订单');
            }
            $pintuan_order_info = model('promotion_platform_pintuan_order')->getInfo([['order_id', '=', $order_id]]);
            if(empty($pintuan_order_info)){
                return $this->error('', '订单信息有误');
            }
            if($pintuan_order_info['pintuan_type'] != PintuanModel::PINTUAN_TYPE_PLATFORM){
                return $this->error('', '非团长批发类型订单');
            }
            $pintuan_id = $pintuan_order_info['pintuan_id'];

            $bidding_info = model('promotion_platform_pintuan_bidding')->getInfo([['pintuan_id', '=', $pintuan_id]], '*');
            if(empty($bidding_info)){
                return $this->error([], '竞标信息有误');
            }
            $bidding_level_rule = json_decode($bidding_info['bidding_level_rule'], true);

            //初始化信息
            $agent_commission_money = 0;
            $profit = 0;
            $data = [];
            $data['pintuan_trader_id'] = 0;
            $data['pintuan_trader_name'] = '';
            $data['pintuan_trader_commission_rate'] = 0;
            //获取基本信息
            $data['goods_id'] = $bidding_info['goods_id'];
            $data['sku_id'] = $bidding_info['sku_id'];
            $data['sku_name'] = $bidding_info['sku_name'];
            $data['sku_image'] = $bidding_info['sku_image'];
            $data['real_cost_price'] = $bidding_info['cost_price'];
            $data['cost_price'] = $bidding_level_rule[$bidding_info['bidding_level']]['cost_price'];

            //获取购买价格和数量
            $order_goods_info = model('order_goods')->getInfo([['order_id', '=', $order_id]], 'price, num');
            $data['price'] = $order_goods_info['price'];
            $data['num'] = $order_goods_info['num'];
            $trader_profit = ($data['price'] - $data['cost_price']) * $order_goods_info['num'];
            $platform_profit = ($data['cost_price'] - $data['real_cost_price']) * $order_goods_info['num'];

            $pintuan_trader_id = $pintuan_order_info['pintuan_trader_id'];
            $enter_info = model('promotion_platform_pintuan_bidding_enter')->getInfo([['bidding_id', '=', $bidding_info['bidding_id']], ['pintuan_trader_id', '=', $pintuan_trader_id], ['enter_status', '=', PintuanBidding::ENTER_STATUS_SUCCESS]]);
            if(!empty($enter_info)){
                //批发和代理佣金设置
                $config_model = new ConfigModel();
                $config_info = $config_model->getPintuanTraderConfig()['data']['value'];

                //初始化信息
                $data['pintuan_trader_id'] = $pintuan_trader_id;
                $data['pintuan_trader_name'] = $enter_info['pintuan_trader_name'];
                $data['pintuan_trader_commission_rate'] = 100;

                $trader_info = model('pintuan_trader')->getInfo([['member_id', '=', $pintuan_trader_id]], 'superior');
                if(!empty($trader_info['superior'])){
                    //上级代理
                    $source_one_info = model('pintuan_trader')->getInfo([['member_id', '=', $trader_info['superior']]], '*');
                    if(!empty($source_one_info)){
                        $pintuan_trader_level = $source_one_info['pintuan_trader_level'];
                        $source_one_commission_rate = $config_info["{$pintuan_trader_level}_agent_rate"];
                        $data['source_agent_one_id'] = $source_one_info['member_id'];
                        $data['source_agent_one_name'] = $source_one_info['username'];
                        $data['source_agent_one_level'] = $source_one_info['pintuan_trader_level'];
                        $data['source_agent_one_commission_rate'] = $source_one_commission_rate;
                        $data['source_agent_one_commission_money'] = $platform_profit * $source_one_commission_rate / 100;
                        $agent_commission_money += $data['source_agent_one_commission_money'];

                        //上上级代理
                        $source_two_info = model('pintuan_trader')->getInfo([['member_id', '=', $source_one_info['superior']]], '*');
                        if(!empty($source_two_info)){
                            $pintuan_trader_level = $source_two_info['pintuan_trader_level'];
                            $source_two_commission_rate = $config_info["{$pintuan_trader_level}_agent_rate"] - $source_one_commission_rate;
                            $data['source_agent_two_id'] = $source_two_info['member_id'];
                            $data['source_agent_two_name'] = $source_two_info['username'];
                            $data['source_agent_two_level'] = $source_two_info['pintuan_trader_level'];
                            $data['source_agent_two_commission_rate'] = $source_two_commission_rate;
                            $data['source_agent_two_commission_money'] = $platform_profit * $source_two_commission_rate / 100;
                            $agent_commission_money += $data['source_agent_two_commission_money'];
                        }
                    }
                }
            }

            //平台
            $data['platform_commission_money'] = $platform_profit - $agent_commission_money;

            //团长
            $data['pintuan_trader_commission_money'] = $trader_profit;

            $res = model('promotion_platform_pintuan_order')->update($data, [['order_id', '=', $order_id]]);

            //更新原本订单已计算好的商家和平台金额
            //商家的钱只有真实的成本价
            //平台的钱包括两部分，一部分是显示成本与真实成本之间的差价，另一部分是利润的抽成
            $order_commission_data = [
                'shop_money' => $data['real_cost_price'] * $data['num'],
                'platform_money' => $data['platform_commission_money'],
            ];
            model('order')->update($order_commission_data, [['order_id', '=', $order_id]]);
            model('order_goods')->update($order_commission_data, [['order_id', '=', $order_id]]);

            return $this->success($res);
        }catch(\Exception $e){
            return $this->error('', $e->getMessage() . $e->getFile() . $e->getLine());
        }

    }

    /**
     * 订单结算
     * 商家和平台的钱是按照账单定期结算 所以这里不需要考虑
     * @param $order_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function settlement($order_id)
    {
        $order_info = model('promotion_platform_pintuan_order')->getInfo([['order_id', '=', $order_id, 'pintuan_type' => PintuanModel::PINTUAN_TYPE_PLATFORM], ['is_settlement', '=', 0]]);
        if(empty($order_info)) return $this->error('', '订单信息有误');

        $member_account = new MemberAccount();
        $res = [];
        if((float)$order_info['pintuan_trader_commission_money'] > 0){
            $res['trader'] = $member_account->addMemberAccount($order_info['pintuan_trader_id'], 'balance_money', $order_info['pintuan_trader_commission_money'], 'pintuan_trader_commission', $order_id, "订单编号：{$order_info['order_no']}，订单商品：{$order_info['sku_name']},团长佣金");
        }
        if((float)$order_info['source_agent_one_commission_money'] > 0){
            $res['source_agent_one'] = $member_account->addMemberAccount($order_info['source_agent_one_id'], 'balance_money', $order_info['source_agent_one_commission_money'], 'pintuan_source_agent_one_commission', $order_id, "订单编号：{$order_info['order_no']}，订单商品：{$order_info['sku_name']},团长上级佣金");
        }
        if((float)$order_info['source_agent_two_commission_money'] > 0){
            $res['source_agent_two'] = $member_account->addMemberAccount($order_info['source_agent_two_id'], 'balance_money', $order_info['source_agent_two_commission_money'], 'pintuan_source_agent_two_commission', $order_id, "订单编号：{$order_info['order_no']}，订单商品：{$order_info['sku_name']},团长上上级佣金");
        }
        $res['update_order'] = model('promotion_platform_pintuan_order')->update(['is_settlement' => 1], [['order_id', '=', $order_id]]);

        //更新团长相关数据
        $res['update'] = model('pintuan_trader')->update([
            'order_num' => Db::raw("order_num + 1"),
            'commission_money' => Db::raw("commission_money + {$order_info['pintuan_trader_commission_money']}"),
            'superior_commission_money' => Db::raw("superior_commission_money + {$order_info['source_agent_one_commission_money']}"),
        ], [['member_id', '=', $order_info['pintuan_trader_id']]]);

        trace('订单结算结果');
        trace($res);

        return $this->success($res);
    }
}