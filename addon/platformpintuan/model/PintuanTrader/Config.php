<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\platformpintuan\model\PintuanTrader;

use app\model\BaseModel;
use app\model\system\Config as ConfigModel;

/**
 * 设置
 */
class Config extends BaseModel
{
    /**
     * 团长设置
     * @return array
     */
    public function setPintuanTraderConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '团长设置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_TRADER_CONFIG' ] ]);
        return $res;
    }

    /**
     * 团长设置
     * @return
     */
    public function getPintuanTraderConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_TRADER_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'partner_agent_rate'  => 0,
                'district_agent_rate' => 0,
                'city_agent_rate'     => 0,
                'bidding_deposit_rate' => 0,//竞标抽成比例
            ];
        }
        return $res;
    }

    /**
     * 团长审核设置
     * @return array
     */
    public function setPintuanTraderCheckConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '团长审核设置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_TRADER_CHECK_CONFIG' ] ]);
        return $res;
    }

    /**
     * 团长审核设置
     * @return
     */
    public function getPintuanTraderCheckConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_TRADER_CHECK_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'admin_correct_agent_count' => 0,
                'admin_reject_agent_day'    => 0,
                'admin_reject_user_day'     => 0,
                'superior_reject_agent_day' => 0
            ];
        }
        return $res;
    }

    /**
     * 团长申请协议配置
     * @return array
     */
    public function setAgreementConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '团长申请协议配置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_TRADER_APPLY_CONFIG' ] ]);
        return $res;
    }

    /**
     * 获取团长申请协议配置
     * @return
     */
    public function getAgreementConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_TRADER_APPLY_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'title' => '团长申请协议',
                'content'=> '申请内容',
            ];
        }
        return $res;
    }

    /**
     * 团长申请协议配置
     * @return array
     */
    public function setAgentAgreementConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '团长申请协议配置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_AGENT_APPLY_CONFIG' ] ]);
        return $res;
    }

    /**
     * 获取团长申请协议配置
     * @return
     */
    public function getAgentAgreementConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PINTUAN_AGENT_APPLY_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'title' => '代理商申请协议',
                'content'=> '申请内容',
            ];
        }
        return $res;
    }

    /**
     * 海报配置
     * array $data
     */
    public function setPosterConfig($data)
    {
        $old_value = $this->getPosterConfig()['data']['value'];
        $config = new ConfigModel();
        $res = $config->setConfig($data, '海报配置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'POSTER_CONFIG' ] ]);
        //删除海报
        $poster_model = new Poster();
        $poster_model->deletePoster(['old_value' => $old_value, 'new_value' => $data]);
        return $res;
    }

    /**
     * 海报配置
     */
    public function getPosterConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'POSTER_CONFIG' ] ]);
        if(empty($res['data']['value']))
        {
            $res['data']['value'] = [
                //背景相关
                'bg_width'  => 300,
                'bg_height' => 450,
                'bg_url'    => '',
                //二维码相关
                'code_size' => 50,
                'code_pos_x' => 10,
                'code_pos_y' => 10,
            ];
        }
        return $res;
    }
}