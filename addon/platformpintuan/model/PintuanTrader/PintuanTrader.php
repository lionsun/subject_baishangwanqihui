<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\platformpintuan\model\PintuanTrader;

use app\model\BaseModel;
use addon\platformpintuan\model\PintuanTrader\Config as ConfigModel;


/**
 * 团长
 */
class PintuanTrader extends BaseModel
{
    /**
     * 获取团长等级
     */
    public static function getPintuanTraderLevel()
    {
        $pintuan_trader_level = [
            [
                'pintuan_trader_level_id' => 0,
                'pintuan_trader_level' => 'common',
                'pintuan_trader_level_name' => '普通拼团商'
            ],
            [
                'pintuan_trader_level_id' => 1,
                'pintuan_trader_level' => 'partner',
                'pintuan_trader_level_name' => '合伙人代理商'
            ],
            [
                'pintuan_trader_level_id' => 2,
                'pintuan_trader_level' => 'district',
                'pintuan_trader_level_name' => '区县级代理商'
            ],
            [
                'pintuan_trader_level_id' => 3,
                'pintuan_trader_level' => 'city',
                'pintuan_trader_level_name' => '地市级代理商'
            ]
        ];
        return $pintuan_trader_level;
    }

    /**
     * 获取团长等级名称
     */
    public static function getPintuanTraderLevelName($level)
    {
        $pintuan_trader_level = self::getPintuanTraderLevel();
        $level_name = '';
        foreach($pintuan_trader_level as $key=>$val){
            if($val['pintuan_trader_level'] == $level){
                $level_name = $val['pintuan_trader_level_name'];
            }
        }
        return $level_name;
    }

     //申请成为团长
    public function apply($data)
    {
        $config_model = new ConfigModel();
        $check_info = $config_model->getPintuanTraderCheckConfig()['data']['value'];
        //是否是第一次提交
        $is_apply = model('pintuan_trader_apply')->getInfo($data);
        if($is_apply){
            //审核状态
            $check_status = $is_apply['check_status'];
            switch($check_status){
                case 2:
                    return $this->error('','等待后台审核');
                    break;
                case 3:
                    //判断审核次数与审核时间
                    $update_check_data = [
                        'check_status' => 2,
                        'apply_time'   => time()
                    ];
                    //后台审核代理商次数
                    $last_admin_check_time = $is_apply['admin_check_time'];
                    $time_between = time()-$last_admin_check_time;
                    //判断审核次数与审核时间
                    if($time_between <= (int)$check_info['admin_reject_user_day']*24*60*60){
                        $diff_str = timediff($last_admin_check_time+$check_info['admin_reject_user_day']*24*60*60);
                        return $this->error('',$diff_str.'后再申请');
                    }
                    $update_check = model('pintuan_trader_apply')->update($update_check_data,$data);
                    if($update_check){
                        return $this->success('申请成功');
                    }else{
                        return $this->error('','申请失败');
                    }
                    break;
                case 4:
                    return $this->error('','请勿重复申请');
                    break;
            }

        }else{
            $add_data = [
                'check_status' => 2,
                'apply_time'   => time(),
            ];
            $last_add_data = array_merge($data, $add_data);
            $add = model('pintuan_trader_apply')->add($last_add_data);
            if($add){
                return $this->success('申请成功');
            }else{
                return $this->error('','申请失败');
            }
        }
    }

    //代理商申请
    public function agentApply($data){
        $config_model = new ConfigModel();
        $check_info = $config_model->getPintuanTraderCheckConfig()['data']['value'];
        //是否是第一次提交
        $is_apply = model('pintuan_trader_apply')->getInfo($data);
        if($is_apply){
            //审核状态
            $check_status = $is_apply['check_status'];
            switch($check_status){
                case 1:
                    $superior_check_time = $is_apply['superior_check_time'];
                    $time_between = time()-$superior_check_time;
                    //判断审核次数与审核时间
                    if($time_between <= (int)$check_info['superior_reject_agent_day']*24*60*60){
                        $diff_str = timediff($superior_check_time+$check_info['superior_reject_agent_day']*24*60*60);
                        return $this->error('',$diff_str.'后再申请');
                    }
                    $update_check_data = [
                        'check_status' => 0,
                        'apply_time'   => time()
                    ];
                    $update_check = model('pintuan_trader_apply')->update($update_check_data,$data);
                    if($update_check){
                        return $this->success('申请成功');
                    }else{
                        return $this->error('','申请失败');
                    }
                    break;
                case 2:
                    return $this->error('','等待后台审核');
                    break;
                case 3:
                    //后台审核代理商次数
                    $admin_check_time = $is_apply['admin_check_time'];
                    $time_between = time()-$admin_check_time;
                    //判断审核次数与审核时间
                    if($time_between <= (int)$check_info['admin_reject_agent_day']*24*60*60){
                        $diff_str = timediff($admin_check_time+$check_info['admin_reject_agent_day']*24*60*60);
                        return $this->error('',$diff_str.'后再申请');
                    }
                    //判断审核次数与审核时间
                    $update_check_data = [
                        'check_status' => 0,
                        'apply_time'   => time()
                    ];
                    $update_check = model('pintuan_trader_apply')->update($update_check_data,$data);
                    if($update_check){
                        return $this->success('申请成功');
                    }else{
                        return $this->error('','申请失败');
                    }
                    break;
                case 5:
                    $update_check_data = [
                        'check_status' => 0,
                        'apply_time'   => time()
                    ];
                    $update_check = model('pintuan_trader_apply')->update($update_check_data,$data);
                    if($update_check){
                        return $this->success('申请成功');
                    }else{
                        return $this->error('','申请失败');
                    }
                    break;
                case 4:
                    return $this->error('','请勿重复申请');
                    break;
            }
        }else{
            $member_info = model('pintuan_trader')->getInfo([['member_id','=',$data['member_id']]]);
            $superior_info = model('pintuan_trader')->getInfo([['member_id','=',$member_info['superior']]]);
            //必须是普通拼团商有上级且是合伙人以上级别才可以申请
            if(!($member_info['pintuan_trader_level'] == 'common' && $member_info['superior'] != 0 && in_array($superior_info['pintuan_trader_level'],['district', 'city']))){
                return $this->error('','您不能申请代理商');
            }
            $add_data = [
                'agent_id'     => $member_info['superior'],
                'check_status' => 0,
                'apply_time'   => time()
            ];

            $last_add_data = array_merge($data, $add_data);
            $add = model('pintuan_trader_apply')->add($last_add_data);
            if($add){
                return $this->success('申请成功');
            }else{
                return $this->error('','申请失败');
            }
        }
    }

    //团长信息
    public function getPintuanTraderInfo($condition){
         $info = model('pintuan_trader')->getInfo($condition);
         if(isset($info['superior'])){
             if($info['superior']!=0){
                 $superior_info = model('pintuan_trader')->getInfo([['member_id','=',$info['superior']]],'pintuan_trader_level');
                 $info['superior_pintuan_trader_level'] = $superior_info['pintuan_trader_level'];
             }
         }
         return $this->success($info);
    }

    //团长信息
    public function getPintuanTraderPageList($condition, $field, $order, $page, $page_size, $alias='', $join=''){
        $list = model('pintuan_trader')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return $this->success($list);
    }

    //团长审核分页列表
    public function pintuanTraderApplyPageList($condition, $page, $page_size, $order = '', $field = '*',$alias,$join)
    {
        $list = model('pintuan_trader_apply')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, '');
        return $this->success($list);
    }


     /**
     * 团长审核
     * @param  $is_admin
     * @param  $data
     * @param  $condition
     */
    public function pintuanTraderCheck($is_admin, $data, $condition, $option = 'pass')
    {
        //后台审核
        $check_info = model('pintuan_trader_apply')->getInfo($condition);
        if($is_admin==1){
            switch($option){
                case 'pass':
                    if($check_info['apply_pintuan_trader_level'] == 'common'){
                        $member_info = model('member')->getInfo([['member_id', '=', $check_info['member_id']]], 'username');
                        model('pintuan_trader')->add([
                            'member_id' => $check_info['member_id'],
                            'username' => $member_info['username'] ?? '',
                            'create_time' => time(),
                            'pintuan_trader_level' => 'common',
                            'pintuan_trader_level_name' => '普通团长',
                        ]);
                    }else{
                        model('pintuan_trader')->update(['pintuan_trader_level' => 'partner', 'pintuan_trader_level_name' => '合伙人代理商'], [['member_id', '=', $data['member_id']]]);
                    }
                    $upd_apply_data = [
                        'check_status' => 4,
                        'admin_check_time'   => time(),
                    ];
                    $res = model('pintuan_trader_apply')->update($upd_apply_data,$condition);
                    break;
                case 'refuse':
                    $upd_apply_data = [
                        'check_status'              => 3,
                        'admin_check_time'     => time(),
                        'admin_reject_reason'  => $data['reason']
                    ];
                    model('pintuan_trader_apply')->setInc($condition,'admin_reject_count',1);
                    $res = model('pintuan_trader_apply')->update($upd_apply_data,$condition);
                    break;
                case 'correct':
                    $upd_apply_data = [
                        'check_status'              => 5,
                        'admin_check_time'     => time(),
                        'admin_reject_reason'  => $data['reason']
                    ];
                    $config_model = new ConfigModel();
                    $check_config_info = $config_model->getPintuanTraderCheckConfig()['data']['value'];
                    if($check_info['admin_correct_count']==(int)$check_config_info['admin_correct_agent_count']-1){
                        model('pintuan_trader_apply')->setInc($condition,'admin_reject_count',1);
                        $upd_apply_data['admin_correct_count'] = 0;
                    }else{
                        model('pintuan_trader_apply')->setInc($condition,'admin_correct_count',1);
                    }
                    $res = model('pintuan_trader_apply')->update($upd_apply_data,$condition);
                    break;
            }
        }else{
            switch($option){
                case 'pass':
                    $upd_apply_data = [
                        'check_status'     => 2,
                        'superior_check_time' => time(),
                    ];
                    $res = model('pintuan_trader_apply')->update($upd_apply_data,$condition);
                    break;
                case 'refuse':
                    $upd_apply_data = [
                        'check_status'              => 1,
                        'superior_check_time'          => time(),
                        'superior_reject_reason'  => $data['reason']
                    ];
                    model('pintuan_trader_apply')->setInc($condition,'agent_check_count',1);
                    $res = model('pintuan_trader_apply')->update($upd_apply_data,$condition);
                    break;
            }
        }
        if($res){
            return $this->success($res);
        }else{
            return $this->error('审核失败');
        }
    }

    //团长详情


    //绑定代理商成为团长
    public function bindAgentPintuanTrader($member_id,$self_member_id)
    {
        $agent_info = model('pintuan_trader')->getInfo([['member_id','=',$member_id],['pintuan_trader_level','in','partner,district,city']]);
        if($agent_info){
            $data = [
                'member_id'                   => $self_member_id,
                'superior'                    => $member_id,
                'pintuan_trader_level'        => 'common',
                'pintuan_trader_level_name'   => '普通拼团商',
                'create_time'                 => time()
            ];
            $res = model('pintuan_trader')->add($data);
            if($res){
                model('member')->update(['is_pintuan_trader'=>1],[['member_id','=',$self_member_id]]);
                model('pintuan_trader_apply')->update(['check_status' => 4, 'admin_check_time' => time()], [['member_id', '=', $self_member_id], ['apply_pintuan_trader_level', '=', 'common']]);
            }
            return $this->success($res);
        }else{
            return $this->error('不存在此代理商');
        }
    }

    /**
     *
     * @param $condition
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getApplyInfo($condition,$field='*')
    {
        $res = model('pintuan_trader_apply')->getInfo($condition,$field);
        return $this->success($res);
    }

    /**
     * 添加团长
     * @param $member_data
     * @param $pintuan_trader_level
     * @param $pintuan_trader_level_name
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function addPintuanTrader($member_id,$pintuan_trader_level)
    {
        $count = model('pintuan_trader')->getCount([['member_id', '=', $member_id]]);
        if($count > 0){
            return $this->error('', '该用户已经是团长');
        }

        $member_info = model('member')->getInfo([['member_id', '=', $member_id]], 'username');
        if(empty($member_info)){
            return $this->error('', '用户信息有误');
        }

        //添加数据
        model('pintuan_trader')->add([
            'member_id' => $member_id,
            'username'  => $member_info['username'],
            'pintuan_trader_level' => $pintuan_trader_level,
            'pintuan_trader_level_name' => self::getPintuanTraderLevelName($pintuan_trader_level),
            'create_time' => time()
        ]);
        //更改审核状态
        model('pintuan_trader_apply')->update(['check_status' => 4, 'admin_check_time' => time()], [['member_id', '=', $member_id], ['apply_pintuan_trader_level', '=', $pintuan_trader_level]]);
        //更改会员的状态
        model('member')->update(['is_pintuan_trader'=> 1 ],[['member_id','=',$member_id]]);

        return $this->success();
    }

    /**
     * @param $member_id
     * @param $pintuan_trader_level
     */
    public function setPintuanTraderLevel($member_id,$pintuan_trader_level)
    {
        $trader_info = model('pintuan_trader')->getInfo([['member_id', '=', $member_id]]);
        if(empty($trader_info)){
            return $this->error('', '团长信息有误');
        }

        $level_arr = ['common', 'partner', 'district', 'city'];
        $level_arr = array_flip($level_arr);

        if($trader_info['superior']){
            $superior_info = model('pintuan_trader')->getInfo([['member_id', '=', $trader_info['superior']]]);
            if($level_arr[$superior_info['pintuan_trader_level']] <= $level_arr[$pintuan_trader_level]){
                return $this->error('', '等级不可与上级相同或高于上级');
            }
        }
        $lower_trader_list = model('pintuan_trader')->getList([['superior', '=', $member_id]]);
        if(!empty($lower_trader_list)){
            foreach($lower_trader_list as $val){
                if($level_arr[$val['pintuan_trader_level']] >= $level_arr[$pintuan_trader_level]){
                    return $this->error('', '等级不可与下级相同或低于下级');
                }
            }
        }

        model('pintuan_trader')->update([
            'pintuan_trader_level' => $pintuan_trader_level,
            'pintuan_trader_level_name' => self::getPintuanTraderLevelName($pintuan_trader_level),
        ], [['member_id', '=', $member_id]]);

        return $this->success();
    }
}