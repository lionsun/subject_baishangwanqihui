<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\shop\controller;

use addon\platformpintuan\model\PintuanOrder;
use app\model\shop\Shop as ShopModel;
use app\shop\controller\BaseShop;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use addon\platformpintuan\model\PintuanGroup as PintuanGroupModel;

/**
 * 批发控制器
 */
class Pintuan extends BaseShop
{
	
	/*
	 *  批发活动列表
	 */
	public function lists()
	{
		$model = new PintuanModel();
		$condition[] = [ 'p.site_id', '=', $this->site_id ];
		//获取续签信息
		if (request()->isAjax()) {
			$status = input('status', '');//批发状态
			if ($status) {
				if ($status == 6) {
					$condition[] = [ 'p.status', '=', 0 ];
				} else {
					$condition[] = [ 'p.status', '=', $status ];
				}
				
			}
			$pintuan_name = input('pintuan_name', '');
			if ($pintuan_name) {
				$condition[] = [ 'p.pintuan_name', 'like', '%' . $pintuan_name . '%' ];
			}
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getPintuanPageList($condition, $page, $page_size, 'p.pintuan_id desc');
            foreach($list['data']['list'] as $key=>$val){
                $list['data']['list'][$key]['pintuan_type_name'] = PintuanModel::getPintuanType($val['pintuan_type']);
            }
			return $list;
		} else {
			$this->forthMenu();
            return $this->fetch("pintuan/lists");
		}

	}
	
	/*
	 *  批发详情
	 */
	public function detail()
	{
		$pintuan_model = new PintuanModel();
		
		$pintuan_id = input('pintuan_id', '');
		//获取批发信息
		$pintuan_info = $pintuan_model->getPintuanDetail($pintuan_id);
		$this->assign('pintuan_info', $pintuan_info);
		return $this->fetch("pintuan/detail");
	}
	
	/**********************************  开团团队    ******************************************************/
	
	/*
	 *  开团团队列表
	 */
	public function group()
	{
        $model = new PintuanGroupModel();
        $pintuan_id = input('pintuan_id', '');
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $status = input('status', '');//批发状态

            $condition = [['pg.pintuan_id', '=', $pintuan_id]];
            if ($status) {
                if ($status == 6) {
                    $condition[] = [ 'pg.status', '=', 0 ];
                } else {
                    $condition[] = [ 'pg.status', '=', $status ];
                }
            }

            $list = $model->getPintuanGroupPageList($condition, $page, $page_size, 'pg.group_id desc');
            return $list;
        } else {
            $pintuan_info = $model->getPintuanInfo($pintuan_id);
            $this->assign('pintuan_info', $pintuan_info['data']);

            $this->assign('pintuan_id', $pintuan_id);
            $this->forthMenu();
            return $this->fetch("pintuan/group");
        }
	}
	
	/*
	 *  批发组成员订单列表
	 */
	public function groupOrder()
	{
		$model = new PintuanOrder();
		
		$condition = [];
		$condition[] = [ 'pintuan_status', 'in', '2,3' ];
		$group_id = input('group_id', '');
		if ($group_id) {
			$condition[] = [ 'ppo.group_id', '=', $group_id ];
		}
		//获取续签信息
		if (request()->isAjax()) {
			
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getPintuanOrderPageList($condition, $page, $page_size, 'ppo.id desc');
			return $list;
			
		} else {
            $this->assign('group_id', $group_id);
			
			return $this->fetch("pintuan/group_order");
		}
	}
	
}