
//竞标等级
function BiddingLevel(param){
    let that = this;
    that.form = param.form;
    that.laytpl = param.laytpl;
    that.laydate = param.laydate;
    that.instance = param.instance;//对象实例 用于单独处理回调等事件
    that.data = {
        sku_info : param.sku_info || null,
        old_sku_id : param.sku_info ? param.sku_info.sku_id : 0,
        bidding_id : param.bidding_id || 0,
        level_rule : param.level_rule || [],
        pintuan_time : param.pintuan_time || '',
    }

    //添加等级
    that.addLevel = function(){
        that.data.level_rule.push({
            pintuan_num : 1,
            buy_num : 1,
            pintuan_time : {
                day: 0,
                hour:0,
                minute:0,
            },
            cost_price : 0.00,
            pintuan_price : 0.00,
            bidding_num : 0,
        });
    }

    //删除等级
    that.delLevel = function(index){
        that.data.level_rule.splice(index, 1);
    }

    //选择等级
    that.selectLevel = function(index){
        that.data.selected_index = index;
    }

    //渲染内容
    that.renderRule = function(){
        that.laytpl($("#rule_tpl").html()).render(that.data.level_rule, function(html) {
            $("#rule_box").html(html);
            that.form.render();
        });
    }

    //选择商品
    that.selectGoods = function(){
        let pintuan_time = $("input[name='pintuan_time']").val();
        if(pintuan_time == ''){
            layer.msg('请先选择拼团活动时间');
            return;
        }
        let start_time = pintuan_time.split(' ~ ')[0];
        let end_time = pintuan_time.split(' ~ ')[1];

        layer_index_select_goods = layer.open({
            title: '选择商品',
            skin: 'layer-tips-class',
            type: 2,
            area: ['80%', '80%'],
            content: ns.url("platformpintuan://admin/pintuanbidding/goodsselect", {
                callback : that.instance + "." + "selectGoodsCallback",
                promotion_start_time : start_time,
                promotion_end_time : end_time,
                //不检测条件
                uncheck_condition:JSON.stringify({
                    promotion_type : 'platformpintuan',
                    promotion_id : that.data.bidding_id,
                    mode : 'sku',
                    id : that.data.old_sku_id,
                })
            }),
        });
    }

    //选择商品回调
    that.selectGoodsCallback = function(sku_info){
        layer.close(layer_index_select_goods);
        that.data.sku_info = sku_info;
        that.renderSkuInfo();
    }

    //渲染sku信息
    that.renderSkuInfo = function(){
        $("input[name='goods_id']").val(that.data.sku_info.goods_id);
        $("input[name='sku_id']").val(that.data.sku_info.sku_id);
        that.laytpl($("#sku_tpl").html()).render(that.data.sku_info, function(html) {
            $("#sku_list").html(html).parent().parent().removeClass('layui-hide');
        });
    }

    /**
     * 检测商品
     */
    that.checkGoods = function(new_value){
        let old_value = that.data.pintuan_time;
        let layer_index;
        if(old_value && old_value != new_value && that.data.sku_info){
            layer_index = layer.confirm('为防止活动时间冲突，更改时间后需重新选择商品，确定要更改吗？', {
                title:'操作提示',
                btn: ['确定', '取消'],
                yes: function(){
                    that.data.pintuan_time = new_value;
                    that.clearGoods();
                    layer.close(layer_index);
                },
                btn2: function() {
                    $("#pintuan_time").val(old_value);
                    layer.close(layer_index);
                }
            });
        }else{
            that.data.pintuan_time = new_value;
        }
    }

    //清空商品
    that.clearGoods = function(){
        that.data.sku_info = null;
        $("input[name='goods_id']").val(0);
        $("input[name='sku_id']").val(0);
        $("#sku_list").html('').parent().parent().addClass('layui-hide');
    }

    //检测数据
    that.checkData = function(){

        if(!that.data.sku_info){
            layer.msg('请先选择竞标商品');
            return false;
        }
        if(that.data.level_rule.length <= 0){
            layer.msg('至少要有一个竞标等级');
            return false;
        }

        let sku_info = that.data.sku_info;
        let pintuan_time_arr = $("input[name='pintuan_time']").val().split(' ~ ');

        for(let i in that.data.level_rule){
            let item = that.data.level_rule[i];
            if(parseInt(item.pintuan_num) < 1){
                $("tr[data-index='"+ i +"'] [name='pintuan_num']").focus();
                layer.msg('成团人数不可小于1');
                return false;
            }
            if(item.buy_num == 0){
                $("tr[data-index='"+ i +"'] [name='buy_num']").focus();
                layer.msg('限制购买不可为0');
                return false;
            }
            var pintuan_time = Number(item.pintuan_time.day) * 24 * 60 + Number(item.pintuan_time.hour) * 60 + Number(item.pintuan_time.minute);
            var time = new Date(pintuan_time_arr[1]).getTime() - new Date(pintuan_time_arr[0]).getTime();
            if (pintuan_time == 0) {
                $("tr[data-index='"+ i +"'] [name='pintuan_time.day']").focus();
                layer.msg("拼团有效期不能为0！", {icon: 5, anim: 6});
                return false;
            }
            if (time < (pintuan_time * 60 * 1000)) {
                $("tr[data-index='"+ i +"'] [name='pintuan_time.day']").focus();
                layer.msg("拼团有效期不能大于活动时长！", {icon: 5, anim: 6});
                return false;
            }
            if(item.pintuan_price == 0){
                $("tr[data-index='"+ i +"'] [name='pintuan_price']").focus();
                layer.msg('请输入拼团价');
                return false;
            }
            if(item.cost_price == 0){
                $("tr[data-index='"+ i +"'] [name='cost_price']").focus();
                layer.msg('请输入成本价');
                return false;
            }
            if(item.bidding_num == 0){
                $("tr[data-index='"+ i +"'] [name='bidding_num']").focus();
                layer.msg('请输入商品数量');
                return false;
            }
            if(parseFloat(item.pintuan_price) > parseFloat(sku_info.price)){
                $("tr[data-index='"+ i +"'] [name='pintuan_price']").focus();
                layer.msg('拼团价不可高于售价');
                return false;
            }
            if(parseFloat(item.cost_price) > parseFloat(item.pintuan_price)){
                $("tr[data-index='"+ i +"'] [name='cost_price']").focus();
                layer.msg('设定成本价不可高于拼团价');
                return false;
            }
            if(parseFloat(item.cost_price) < parseFloat(sku_info.cost_price)){
                $("tr[data-index='"+ i +"'] [name='cost_price']").focus();
                layer.msg('设定成本价不可低于真实成本价');
                return false;
            }
            if(parseFloat(item.cost_price) * 2 < parseFloat(item.pintuan_price) + parseFloat(sku_info.cost_price)){
                $("tr[data-index='"+ i +"'] [name='cost_price']").focus();
                layer.msg('设定成本价与真实成本价的差价必须不能低于拼团价和设定成本价的差价');
                return false;
            }
        }

        return true;
    }

    that.getData = function(){
        return that.data;
    }

    //初始化
    that.init = function(){
        $("#rule_box").on('click', "button[data-action='add_level']", function(){
            that.addLevel();
            that.renderRule();
        })
        $("#rule_box").on('click', "a[data-action='delete_level']", function(){
            let index = $(this).parents('tr').data('index');
            that.delLevel(index);
            that.renderRule()
        })
        $("#rule_box").on('input', "td input", function(){
            let field_type = $(this).attr('name');
            let index = $(this).parents('tr').data('index');
            let field_type_arr = field_type.split('.');
            if(field_type_arr.length > 1){
                that.data.level_rule[index][field_type_arr[0]][field_type_arr[1]] = $(this).val();
            }else{
                that.data.level_rule[index][field_type] = $(this).val();
            }
        })
        $("#select_goods").on({
            click : function(){
                that.selectGoods();
            }
        });
    }

    that.init();

    if(that.data.sku_info){
        that.renderSkuInfo();
    }
    that.renderRule();
}