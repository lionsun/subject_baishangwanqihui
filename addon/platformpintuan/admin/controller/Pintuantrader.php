<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\admin\controller;

use addon\platformpintuan\model\PintuanTrader\Config;
use addon\platformpintuan\model\PintuanTrader\PintuanTrader as PintuanTraderModel;
use addon\platformpintuan\model\PintuanTrader\Config as ConfigModel;
use app\model\member\Member as MemberModel;
use app\model\member\MemberLabel as MemberLabelModel;
use app\model\member\MemberLevel as MemberLevelModel;

/**
 * 团长
 */
class Pintuantrader extends BaseAdmin
{
    /**
     * 团长列表
     */
    public function lists()
    {
        if (request()->isAjax()) {
            $member_model = new MemberModel();

            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'username');//可以传username mobile email
            $pintuan_trader_level = input('pintuan_trader_level', '');
            $superior_trader_name = input('superior_trader_name', '');//团长上级
            $member_type = input('member_type', '');
            $condition = [];
            //下拉选择
            $condition[] = [ 'm.'.$search_text_type, 'like', "%" . $search_text . "%" ];
            //团长等级
            if($pintuan_trader_level != ''){
                $condition[] = [ 'pt.pintuan_trader_level', '=', $pintuan_trader_level ];
            }
            if($member_type != ''){
                $condition[] = [ 'm.member_type', '=', $member_type ];
            }
            if(!empty($superior_trader_name)){
                $superior_trader_info = $member_model->getMemberInfo([['username', '=', $superior_trader_name]], 'member_id')['data'];
                if(!empty($superior_trader_info)){
                    $superior_trader_id = $superior_trader_info['member_id'];
                }else{
                    $superior_trader_id = -1;
                }
                $condition[] = ['pt.superior', '=', $superior_trader_id];
            }

            $alias = 'pt';
            $field = 'pt.*,m.nickname,m.headimg,m.member_type,m.username,m.mobile,m.email';
            $join = [
                [
                    'member m',
                    'pt.member_id = m.member_id',
                    'inner'
                ]
            ];
            $order = 'pt.create_time desc';
            $pintuan_trader_model = new PintuanTraderModel();
            $list = $pintuan_trader_model->getPintuanTraderPageList($condition, $field, $order, $page, $page_size, $alias, $join);
            foreach($list['data']['list'] as $key=>$val){
                //查询上级团长
                if($val['superior'] != 0){
                    $list['data']['list'][$key]['superior_trader_info'] = $superior_trader_info = $member_model->getMemberInfo([['member_id', '=', $val['superior']]], 'member_id, username, nickname, mobile')['data'];
                }
            }
            return $list;
        } else {

            //团长等级
            $this->assign('pintuan_trader_level', PintuanTraderModel::getPintuanTraderLevel());

            //四级菜单
            $this->forthMenu([]);

            return $this->fetch('pintuan_trader/lists');
        }
    }

    /**
     * 添加团长
     */
    public function addPintuanTrader()
    {
        if (request()->isAjax()) {
            $data = [
                'username' => input('username', ''),
                'mobile' => input('mobile', ''),
                'email' => input('email', ''),
                'password' => data_md5(input('password', '')),
                'status' => input('status', 1),
                'headimg' => input('headimg', ''),
                'member_type' => input('member_type', ''),
                'member_level' => input('member_level', ''),
                'member_level_name' => input('member_level_name', ''),
                'nickname' => input('nickname', ''),
                'sex' => input('sex', 0),
                'birthday' => input('birthday', '') ? strtotime(input('birthday', '')) : 0,
                'realname' => input('realname', ''),
                'reg_time' => time(),
            ];

            $pintuan_trader_data = [
                'create_time' => time(),
                'username' => input('username', ''),
                'pintuan_trader_level' => input('pintuan_trader_level', ''),
                'pintuan_trader_level_name' => input('pintuan_trader_level_name', ''),
            ];

            $apply_data = [
                'company_name' => input('company_name', ''),
                'province_id' => input('province_id', 0),
                'city_id' => input('city_id', 0),
                'district_id' => input('district_id', 0),
                'province_name' => input('province_name', ''),
                'city_name' => input('city_name', ''),
                'district_name' => input('district_name', ''),
                'address' => input('address', ''),
                'contacts_name' => input('contacts_name', ''),
                'contacts_mobile' => input('contacts_mobile', ''),
                'legal_person_name' => input('legal_person_name', ''),
                'legal_person_mobile' => input('legal_person_mobile', ''),
                'card_no' => input('card_no', ''),
                'card_electronic_front' => input('card_electronic_front', ''),
                'card_electronic_back' => input('card_electronic_back', ''),
                'business_licence_number' => input('business_licence_number', ''),
                'business_licence_number_electronic' => input('business_licence_number_electronic', ''),
                'company_scale' => input('company_scale', 1),
            ];

            if($data['member_type']==2){
                $data['invite_code'] = transferCodeOrId(rand(1000,9999));
            }

            if($data['member_type']==1){
                $data['code'] = input('invite_code', '');
            }
            $member_model = new MemberModel();
            $this->addLog("添加会员" . $data['username'] . $data['mobile']);
            return $member_model->addMember($data,$apply_data,$pintuan_trader_data);
        } else {
            //会员等级
            $member_level_model = new MemberLevelModel();
            $member_level_type = $member_level_model->getMemberLevelType();
            foreach($member_level_type as $key=>$val){
                $member_level_list[$key] = $member_level_model->getMemberLevelList([['level_type','=',$val['type_id']]], 'level_id, level_name', 'growth asc')['data'];
            }
            //企业规模
            $member_model = new MemberModel();
            $team_scale = $member_model->getCompanyScaleSet();
            $this->assign('member_level_list', $member_level_list);
            $this->assign('team_scale', $team_scale);
            //代理类型
            $this->assign('pintuan_trader_list',PintuanTraderModel::getPintuanTraderLevel());
            $this->forthMenu([]);
            return $this->fetch('pintuan_trader/add_pintuantrader');
        }
    }


    /**
     * 团长审核列表
     */
    public function checkLists()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'username');//可以传username mobile email
            $pintuan_trader_level = input('pintuan_trader_level', '');
            $member_type = input('member_type', '');
            $check_status = input('check_status', '');
            $apply_pintuan_trader_level = input('apply_pintuan_trader_level', 'common');

            $condition = [];
            //下拉选择
            $condition[] = [ 'm.'.$search_text_type, 'like', "%" . $search_text . "%" ];
            //团长等级
            if($pintuan_trader_level != ''){
                $condition[] = [ 'pt.pintuan_trader_level', '=', $pintuan_trader_level ];
            }
            if($member_type != ''){
                $condition[] = [ 'm.member_type', '=', $member_type ];
            }
            if($check_status != ''){
                $condition[] = [ 'pt.check_status', '=', $check_status ];
            }
            $condition[] = [ 'pt.apply_pintuan_trader_level', '=', $apply_pintuan_trader_level ];
            $alias = 'pt';
            $join = [
                [
                    'member m',
                    'pt.member_id = m.member_id',
                    'inner'
                ]
            ];
            $condition[] = ['pt.check_status', '>=', 2];
            $field = 'pt.*,m.member_id,m.member_type,m.status,m.username,m.nickname,m.mobile,m.member_level,m.member_level_name';
            $order = 'apply_time desc';

            $pintuan_trader_model = new PintuanTraderModel();
            $result = $pintuan_trader_model->pintuanTraderApplyPageList($condition, $page, $page_size, $order, $field, $alias, $join);
            return $result;
        }else{
            //四级菜单
            $this->forthMenu([]);
            return $this->fetch("pintuan_trader/check_lists");
        }
    }

    /**
     * 团长审核
     */
    public function check()
    {
        if(request()->isAjax()){
            $id = input('id', 0);
            $member_id = input('member_id', 0);
            $reason = input('reason', '');
            $option = input('option', 0);

            $data = [
                'member_id' => $member_id,
                'reason' => $reason
            ];

            $condition[] = ['id','=',$id];
            $condition[] = ['member_id','=',$member_id];

            $pintuan_trader_model = new PintuanTraderModel();
            $result = $pintuan_trader_model->pintuanTradercheck(1,$data,$condition,$option);
            return $result;
        }
    }

    /**
     * 团长相关设置
     * @return array|mixed
     */
    public function config()
    {
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            //设置代理商抽成比例
            $partner_agent_rate = input('partner_agent_rate', '');
            $district_agent_rate = input('district_agent_rate', '');
            $city_agent_rate = input('city_agent_rate', '');
            $bidding_deposit_rate = input('bidding_deposit_rate', '');

            $data = [
                'partner_agent_rate'  => $partner_agent_rate,
                'district_agent_rate' => $district_agent_rate,
                'city_agent_rate'     => $city_agent_rate,
                'bidding_deposit_rate' => $bidding_deposit_rate,
            ];
            return $config_model->setPintuanTraderConfig($data);
        }else{
            //获取代理商抽成比例
            $config_info = $config_model->getPintuanTraderConfig()['data']['value'];
            $this->assign('config_info', $config_info);

            $agent_check_info = $config_model->getPintuanTraderCheckConfig()['data']['value'];
            $this->assign('agent_check_info', $agent_check_info);

            //四级菜单
            $this->forthMenu([]);

            return $this->fetch('pintuan_trader/config');
        }
    }

    /**
     * 审核设置
     */
    public function checkConfig()
    {
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            //设置代理商审核设置
            $admin_correct_agent_count = input('admin_correct_agent_count', ''); //后台整改代理次数
            $admin_reject_agent_day = input('admin_reject_agent_day', ''); //后台驳回代理再次提交时间间隔
            $admin_reject_user_day = input('admin_reject_user_day', ''); //后台驳回团长再次提交时间间隔
            $superior_reject_agent_day = input('superior_reject_agent_day', ''); //后台驳回团长再次提交时间间隔
            $data = [
                'admin_correct_agent_count'  => $admin_correct_agent_count,
                'admin_reject_agent_day'     => $admin_reject_agent_day,
                'admin_reject_user_day'      => $admin_reject_user_day,
                'superior_reject_agent_day'  => $superior_reject_agent_day
            ];
            return $config_model->setPintuanTraderCheckConfig($data);
        }
    }

    /**
     * 团长申请协议
     */
    public function agreement()
    {
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            $data = [
                'title'        => input('title', ''),
                'content'    => input('content', ''),
                'time' => time(),
            ];

            $type = input('type', '');
            if($type == 'common'){
                return $config_model->setAgreementConfig($data);
            }else if($type == 'partner'){
                return $config_model->setAgentAgreementConfig($data);
            }
        }else{
            $config_info    = $config_model->getAgreementConfig();
            $this->assign('config_info', $config_info['data']['value']);

            $agent_config_info    = $config_model->getAgentAgreementConfig();
            $this->assign('agent_config_info', $agent_config_info['data']['value']);

            //四级菜单
            $this->forthMenu([]);
            return $this->fetch("pintuan_trader/agreement");
        }

    }

    /*
     *  会员选择
     */
    public function memberSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = 8;
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'username');//可以传username mobile email

            $condition = [];
            //下拉选择
            $condition[] = [ $search_text_type, 'like', "%" . $search_text . "%" ];
            $condition[] = [ 'is_pintuan_trader', '=', 0 ];

            $order = 'member_id desc';
            $field = 'member_id,mobile,headimg,username,reg_time,nickname';

            $member_model = new MemberModel();
            $list = $member_model->getMemberPageList($condition, $page, $page_size, $order, $field);
            return $list;
        } else {
            //回调函数
            $callback = input('callback', '');
            $this->assign('callback', $callback);
            return $this->fetch('pintuan_trader/member_select');
        }
    }

    /**
     * @return array|mixed
     */
    public function addPintuanTraderFromSelect()
    {
        if (request()->isAjax()) {
            $member_id = input('member_id', '');
            $pintuan_trader_level = input('pintuan_trader_level', '');

            $pintuan_trader_model = new PintuanTraderModel();
            $res = $pintuan_trader_model->addPintuanTrader($member_id,$pintuan_trader_level);
            return $res;
        }
    }

    /**
     * 设置团长等级
     * @return array
     */
    public function setPintuanTraderLevel()
    {
        if (request()->isAjax()) {
            $member_id = input('member_id', '');
            $pintuan_trader_level = input('pintuan_trader_level', '');

            $pintuan_trader_model = new PintuanTraderModel();
            $res = $pintuan_trader_model->setPintuanTraderLevel($member_id,$pintuan_trader_level);
            return $res;
        }
    }

    /**
     * 海报设置
     */
    public function posterConfig()
    {
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            $data = array(
                'bg_width'   => (int)input('bg_width', 300),
                'bg_height'  => (int)input('bg_height', 450),
                'bg_url'     => input('bg_url', ''),
                'code_size'  => (int)input('code_size', 50),
                'code_pos_x' => (int)input('code_pos_x', 0),
                'code_pos_y' => (int)input('code_pos_y', 0),
            );
            $res = $config_model->setPosterConfig($data);
            return $res;
        } else {
            $result = $config_model->getPosterConfig();
            $this->assign("config_info", $result[ 'data' ][ 'value' ]);
            $this->forthMenu();
            return $this->fetch('pintuan_trader/poster_config');
        }
    }
}