<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\admin\controller;

use addon\platformpintuan\model\PintuanTrader\Config as ConfigModel;
use addon\platformpintuan\model\PintuanTrader\PintuanBidding as PintuanBiddingModel;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use app\model\goods\Goods;
use app\model\goods\Goods as GoodsModel;
use app\model\system\Promotion as PromotionModel;

/**
 * 批发竞标控制器
 */
class Pintuanbidding extends BaseAdmin
{
    /**
     * 添加竞标
     */
    public function add()
    {
        if(request()->isAjax()){
            $param = [
                'pintuan_name'       => input('pintuan_name', ''),//活动名称
                'goods_id'           => input('goods_id', 0),//商品ID
                'sku_id'             => input('sku_id', 0),//skuID
                'remark'             => input('remark', ''),//备注
                'is_recommend'       => input('is_recommend', ''),//是否推荐
                'bidding_start_time' => date_to_time(input('bidding_start_time', '')),//竞标开始时间
                'bidding_end_time'   => date_to_time(input('bidding_end_time', '')),//竞标结束时间
                'start_time'         => date_to_time(input('start_time', '')),//开始时间
                'end_time'           => date_to_time(input('end_time', '')),//结束时间
                'is_single_buy'      => input('is_single_buy', 0),//是否单独购买
                'is_virtual_buy'     => input('is_virtual_buy', 0),//是否虚拟成团
                'bidding_level_rule' => input('bidding_level_rule', ''),//是否团长优惠
            ];
            $bidding_model = new PintuanBiddingModel();
            return $bidding_model->addPintuanBidding($param);
        }else{
            return $this->fetch("pintuan_bidding/add");
        }
    }

    /**
     * 编辑竞标
     */
    public function edit()
    {
        $bidding_model = new PintuanBiddingModel();
        $bidding_id = input('bidding_id', 0);
        if(request()->isAjax()) {
            $param = [
                'pintuan_name'       => input('pintuan_name', ''),//活动名称
                'goods_id'           => input('goods_id', 0),//商品ID
                'sku_id'             => input('sku_id', 0),//skuID
                'remark'             => input('remark', ''),//备注
                'is_recommend'       => input('is_recommend', ''),//是否推荐
                'bidding_start_time' => date_to_time(input('bidding_start_time', '')),//竞标开始时间
                'bidding_end_time'   => date_to_time(input('bidding_end_time', '')),//竞标结束时间
                'start_time'         => date_to_time(input('start_time', '')),//开始时间
                'end_time'           => date_to_time(input('end_time', '')),//结束时间
                'is_single_buy'      => input('is_single_buy', 0),//是否单独购买
                'is_virtual_buy'     => input('is_virtual_buy', 0),//是否虚拟成团
                'bidding_level_rule' => input('bidding_level_rule', ''),//是否团长优惠
            ];
            return $bidding_model->editPintuanBidding($bidding_id, $param);
        }else{
            $bidding_info = $bidding_model->getPintuanBiddingInfo([['bidding_id', '=', $bidding_id]])['data'];
            $goods_model = new GoodsModel();
            $sku_info = $goods_model->getGoodsSkuInfo([['sku_id', '=', $bidding_info['sku_id']]], 'goods_id,sku_id,sku_name,price,cost_price,stock,sale_num,sku_image,spec_name,source')['data'];
            $bidding_info['sku_info'] = $sku_info;
            $this->assign('bidding_info', $bidding_info);

            return $this->fetch("pintuan_bidding/edit");
        }
    }

    /**
     * 删除
     */
    public function delete()
    {
        if(request()->isAjax()){
            $bidding_model = new PintuanBiddingModel();
            $bidding_id = input('bidding_id', 0);
            return $bidding_model->deletePintuanBidding($bidding_id);
        }
    }

    /**
     * 竞标详情
     */
    public function detail()
    {
        $bidding_model = new PintuanBiddingModel();
        
        $bidding_id = input('bidding_id', 0);
        $bidding_info = $bidding_model->getPintuanBiddingInfo([['bidding_id', '=', $bidding_id]])['data'];
        $goods_model = new GoodsModel();
        $sku_info = $goods_model->getGoodsSkuInfo([['sku_id', '=', $bidding_info['sku_id']]], 'goods_id,sku_id,sku_name,price,cost_price,stock,sale_num,sku_image,spec_name,source')['data'];
        $bidding_info['sku_info'] = $sku_info;
        $this->assign('bidding_info', $bidding_info);

        return $this->fetch("pintuan_bidding/detail");
    }

    /**
     * 竞标列表
     */
	public function lists()
	{
	    $search_type_arr = [
	        'b.site_name' => '商家名称',
	        'b.pintuan_name' => '活动名称',
	        's.sku_name' => '商品名称',
        ];
        $bidding_model = new PintuanBiddingModel();
        if(request()->isAjax()){
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_type = input('search_type', '');
            $search_text = input('search_text', '');
            $bidding_status = input('bidding_status', '');

            $alias = 'b';
            $join = [
                ['goods_sku s', 'b.sku_id = s.sku_id', 'left'],
                ['promotion_platform_pintuan p', 'p.pintuan_id = b.pintuan_id', 'left']
            ];
            $field = [
                "b.bidding_id, b.pintuan_name, b.site_id, b.site_name, b.bidding_start_time, b.bidding_end_time, b.bidding_status, b.bidding_level, b.bidding_success_num, b.start_time, b.end_time, b.enter_num",
                "s.goods_id,s.sku_id,s.sku_name,s.price,s.stock,s.sale_num,s.sku_image,s.spec_name,s.source",
                "p.status"
            ];
            $order = 'b.create_time desc';
            $condition = [];
            if(in_array($search_type, array_keys($search_type_arr)) && !empty($search_text)){
                $condition[] = [$search_type, 'like', "%{$search_text}%"];
            }
            if($bidding_status !== ''){
                $condition[] = ['b.bidding_status', '=', $bidding_status];
            }

            $res = $bidding_model->getPintuanBiddingPageList($condition, $field, $order, $page, $page_size, $alias, $join);
            foreach($res['data']['list'] as $key=>$val){
                if(is_null($val['status'])){
                    $status_name = '';
                }else{
                    $status_name = PintuanModel::getPintuanStatus($val['status']);
                }
                $res['data']['list'][$key]['status_name'] = $status_name;
            }
            return $res;
        }else{
            $bidding_status = PintuanBiddingModel::getBiddingStatus();
            $this->assign('bidding_status', $bidding_status);

            $this->assign('search_type_arr', $search_type_arr);

            //四级菜单
            $this->forthMenu([]);

            return $this->fetch("pintuan_bidding/lists");
        }
	}

    /**
     * 团长列表
     */
	public function enterLists()
    {
        $bidding_model = new PintuanBiddingModel();
        $bidding_id = input('bidding_id', 0);
        if(request()->isAjax()){
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $pintuan_trader_name = input('pintuan_trader_name', '');
            $enter_status = input('enter_status', '');
            $bidding_level = input('bidding_level', '');
            $search_type = input('search_type', 'all');
            $selected_enter_ids = input('selected_enter_ids', '');

            $condition = [];
            $condition[] = ['bidding_id', '=', $bidding_id];
            if(!empty($pintuan_trader_name)){
                $condition[] = ['pintuan_trader_name', 'like', "%{$pintuan_trader_name}%"];
            }
            if($enter_status !== ''){
                $condition[] = ['enter_status', '=', $enter_status];
            }
            if($bidding_level !== ''){
                $condition[] = ['bidding_level', '=', $bidding_level];
            }
            if($search_type == 'selected'){
                $condition[] = ['enter_id', 'in', $selected_enter_ids];
            }

            $field = '*';
            $order = 'enter_time desc';
            $bidding_info = $bidding_model->getPintuanBiddingInfo([['bidding_id', '=', $bidding_id]])['data'];
            $bidding_level_rule = json_decode($bidding_info['bidding_level_rule'], true);

            $res = $bidding_model->getPintuanBiddingEnterPageList($condition, $field, $order, $page, $page_size);
            foreach($res['data']['list'] as $key=>$val){
                $res['data']['list'][$key]['bidding_level_content'] = $bidding_level_rule[$val['bidding_level']] ?? null;
            }
            return $res;
        }else{
            $bidding_info = $bidding_model->getPintuanBiddingInfo([['bidding_id', '=', $bidding_id]])['data'];
            $goods_model = new GoodsModel();
            $sku_info = $goods_model->getGoodsSkuInfo([['sku_id', '=', $bidding_info['sku_id']]], 'goods_id,sku_id,sku_name,price,stock,sale_num,sku_image,spec_name,source')['data'];
            $bidding_info['sku_info'] = $sku_info;
            $this->assign('bidding_info', $bidding_info);

            $enter_status_arr = PintuanBiddingModel::getEnterStatus();
            $this->assign('enter_status_arr', $enter_status_arr);

            return $this->fetch("pintuan_bidding/enter_lists");
        }
    }

    /**
     * 选择团长
     */
    public function selectEnter()
    {
        if(request()->isAjax()){
            $enter_ids = input('enter_ids', '');
            $bidding_model = new PintuanBiddingModel();
            return $bidding_model->selectPintuanBiddingEnter($enter_ids);
        }
    }

    /**
     * 商品选择
     * @return array|mixed
     */
    public function goodsSelect()
    {
        $goods_model = new GoodsModel();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', "");
            $site_name = input('site_name', "");
            $category_id = input('category_id', "");
            $brand_id = input('goods_brand', '');
            $goods_attr_class = input("goods_attr_class", "");
            $site_id = input("site_id", "");
            $goods_class = input('goods_class', "");
            $promotion_start_time = input('promotion_start_time', '');
            $promotion_end_time = input('promotion_end_time', '');
            $promotion_start_time = strtotime($promotion_start_time);
            $promotion_end_time = strtotime($promotion_end_time);

            $alias = 'g';
            $join = [
                ['shop s', 'g.site_id = s.site_id', 'left'],
            ];

            $condition = [];
            $condition[] = [ 'g.goods_state', '=', 1 ];
            $condition[] = [ 'g.verify_state', '=', 1 ];
            $condition[] = [ 's.shop_status', '=', 1 ];

            if (!empty($goods_name)) {
                $condition[] = [ 'g.goods_name', 'like', '%' . $goods_name . '%' ];
            }
            if (!empty($site_name)) {
                $condition[] = [ 'g.site_name', 'like', '%' . $site_name . '%' ];
            }

            if ($goods_class !== "") {
                $condition[] = [ 'g.goods_class', '=', $goods_class ];
            }
            if (!empty($category_id)) {
                $condition[] = [ 'g.category_id|category_id_1|category_id_2|category_id_3', '=', $category_id ];
            }
            if ($brand_id) {
                $condition[] = [ 'g.brand_id', '=', $brand_id ];
            }
            if ($goods_attr_class) {
                $condition[] = [ 'g.goods_attr_class', '=', $goods_attr_class ];
            }
            if (!empty($site_id)) {
                $condition[] = [ 'g.site_id', '=', $site_id ];
            }

            $field = 'g.goods_id,g.goods_name,g.site_id,g.site_name,g.goods_image,g.is_own,g.goods_state,g.verify_state,g.price,g.goods_stock,g.create_time,g.sale_num,g.is_virtual,g.goods_class,source,g.cost_price,g.brand_name,g.goods_stock_alarm,g.category_name';
            $goods_list = $goods_model->getGoodsPageList($condition, $page_index, $page_size, 'g.create_time desc', $field, $alias, $join);
            $promotion_model = new PromotionModel();
            foreach ($goods_list[ 'data' ][ 'list' ] as $k => $v) {
                //相同时间的营销活动
                $goods_list[ 'data' ][ 'list' ][ $k ][ 'same_time_promotion'] = $promotion_model->checkSameTimePromotion([
                    'mode' => 'spu',
                    'id' => $v['goods_id'],
                    'time_info' => ['start_time' => $promotion_start_time, 'end_time' => $promotion_end_time]
                ]);
            }

            return $goods_list;
        } else {
            $verify_state = $goods_model->getVerifyState();
            $arr = [];
            foreach ($verify_state as $k => $v) {
                // 过滤已审核状态
                if ($k != 1) {
                    $total = $goods_model->getGoodsTotalCount([ [ 'verify_state', '=', $k ] ]);
                    $total = $total[ 'data' ];
                    $arr[] = [
                        'state' => $k,
                        'value' => $v,
                        'count' => $total
                    ];
                }
            }
            $verify_state = $arr;
            $this->assign("verify_state", $verify_state);

            $callback = input('callback', '');
            $this->assign('callback', $callback);

            //活动时间
            $promotion_start_time = input('promotion_start_time', '');
            $promotion_end_time = input('promotion_end_time', '');
            $this->assign('promotion_start_time', $promotion_start_time);
            $this->assign('promotion_end_time', $promotion_end_time);

            //不检测条件
            $uncheck_condition = input('uncheck_condition', '');
            $this->assign('uncheck_condition', $uncheck_condition);

            return $this->fetch('pintuan_bidding/goods_select');
        }
    }

    /**
     * 获取SKU商品列表
     */
    public function getGoodsSkuList()
    {
        if (request()->isAjax()) {
            $goods_id = input("goods_id", 0);
            $promotion_start_time = input('promotion_start_time', '');
            $promotion_end_time = input('promotion_end_time', '');
            $uncheck_condition = input('uncheck_condition', '');

            $goods_model = new GoodsModel();
            $sku_list = $goods_model->getGoodsSkuList([ [ 'goods_id', '=', $goods_id ] ], 'goods_id,sku_id,sku_name,price,cost_price,stock,sale_num,sku_image,spec_name,source,sku_no');
            $promotion_model = new PromotionModel();
            foreach($sku_list['data'] as $key=>$val){
                //相同时间的营销活动
                if(!empty($promotion_start_time) && !empty($promotion_end_time)){
                    $sku_list[ 'data' ][ $key ][ 'same_time_promotion'] = $promotion_model->checkSameTimePromotion([
                        'mode' => 'sku',
                        'id' => $val['sku_id'],
                        'time_info' => [
                            'start_time' => strtotime($promotion_start_time),
                            'end_time' => strtotime($promotion_end_time),
                        ],
                        'uncheck_condition' => $uncheck_condition,
                    ]);
                }else{
                    $sku_list[ 'data' ][ $key ][ 'same_time_promotion'] = [];
                }
            }
            return $sku_list;
        }
    }

    public function test()
    {
        /*$model = new \addon\platformpintuan\model\PintuanTrader\PintuanCommission();
        $res = $model->calculate(['order_id' => 375]);
        dump($res);*/

        /*$model = new \addon\platformpintuan\model\Pintuan();
        $res = $model->cronOpenPlatformPintuan(45);
        dump($res);*/

        /*$model = new \addon\platformpintuan\model\PintuanTrader\PintuanBidding();
        $res = $model->cronEndPlatformPintuanBidding(71);
        dump($res);*/

        /*$model = new \addon\platformpintuan\model\PintuanTrader\PintuanCommission();
        $res = $model->settlement(183);
        dump($res);*/

        /*$res = event('CronOrderComplete', ['relate_id' => 186]);
        dump($res);*/

        /*$res = event('OpenPlatformPintuan', ['relate_id' => 46]);
        dump($res);*/

        /*$res = event('EndPlatformPintuanBidding', ['relate_id' => 5]);
        dump($res);*/
        
        $res = event('ClosePlatformPintuan', ['relate_id' => 18]);
        dump($res);
    }
}