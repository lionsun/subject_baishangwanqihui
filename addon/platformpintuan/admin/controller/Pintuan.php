<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformpintuan\admin\controller;

use addon\platformpintuan\model\PintuanOrder;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use addon\platformpintuan\model\PintuanGroup as PintuanGroupModel;

/**
 * 限时折扣控制器
 */
class Pintuan extends BaseAdmin
{
	
	/*
	 *  批发活动列表
	 */
	public function lists()
	{
		$model = new PintuanModel();
        $condition = [];
		//获取续签信息
		if (request()->isAjax()) {
			$status = input('status', '');//批发状态
            $pintuan_name = input('pintuan_name', '');
			$pintuan_type = input('pintuan_type', '');//批发状态
            $goods_name = input('goods_name', '');
            $start_time = input('start_time', '');
            $end_time = input('end_time', '');
            $site_name = input('site_name', '');

			if ($status) {
				if ($status == 6) {
					$condition[] = [ 'p.status', '=', 0 ];
				} else {
					$condition[] = [ 'p.status', '=', $status ];
				}
			}
			if ($pintuan_name) {
				$condition[] = [ 'p.pintuan_name', 'like', '%' . $pintuan_name . '%' ];
			}
			if($pintuan_type !== ''){
			    $condition[] = ['p.pintuan_type', '=', $pintuan_type];
            }

			if ($goods_name) {
				$condition[] = [ 'g.goods_name', 'like', '%' . $goods_name . '%' ];
			}
			if (!empty($start_time) && empty($end_time)) {
				$condition[] = [ "p.start_time", ">=", date_to_time($start_time) ];
			} elseif (empty($start_time) && !empty($end_time)) {
				$condition[] = [ "p.end_time", "<=", date_to_time($end_time) ];
			} elseif (!empty($start_time) && !empty($end_time)) {
				$condition[] = [ "p.start_time", ">=", date_to_time($start_time) ];
				$condition[] = [ "p.end_time", "<=", date_to_time($end_time) ];
			}
			if ($site_name) {
				$condition[] = [ 'p.site_name', 'like', '%' . $site_name . '%' ];
			}
			
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getPintuanPageList($condition, $page, $page_size, 'p.pintuan_id desc');
			return $list;
		} else {
            //四级菜单
            $this->forthMenu([]);
            return $this->fetch("pintuan/lists");
		}

	}
	
	/*
	 *  开团团队列表
	 */
	public function group()
	{
		$model = new PintuanGroupModel();
		$pintuan_id = input('pintuan_id', '');
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
            $status = input('status', '');//批发状态

            $condition = [['pg.pintuan_id', '=', $pintuan_id]];
            if ($status) {
                if ($status == 6) {
                    $condition[] = [ 'pg.status', '=', 0 ];
                } else {
                    $condition[] = [ 'pg.status', '=', $status ];
                }
            }

			$list = $model->getPintuanGroupPageList($condition, $page, $page_size, 'pg.group_id desc');
			return $list;
		} else {
		    $pintuan_info = $model->getPintuanInfo($pintuan_id);
			$this->assign('pintuan_info', $pintuan_info['data']);

			$this->assign('pintuan_id', $pintuan_id);
            return $this->fetch("pintuan/group");
		}
	}
	
	/*
	*  批发组成员订单列表
	*/
	public function groupOrder()
	{
		$model = new PintuanOrder();
		
		$condition = [];
		$group_id = input('group_id', '');
		if ($group_id) {
			$condition[] = [ 'ppo.group_id', '=', $group_id ];
		}
		//获取续签信息
		if (request()->isAjax()) {
			
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getPintuanOrderPageList($condition, $page, $page_size, 'ppo.id desc');
			return $list;
			
		} else {
			$list = $model->getPintuanOrderPageList($condition, 1, PAGE_LIST_ROWS, 'ppo.id desc');
			$this->assign('list', $list);
			
			return $this->fetch("pintuan/group_order");
		}
	}

	public function test()
    {

            $res = event('ClosePlatformPintuan', ['relate_id' => input('id', 0)]);
            dump($res);

    }
	
}