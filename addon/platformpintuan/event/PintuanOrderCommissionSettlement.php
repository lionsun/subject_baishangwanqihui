<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformpintuan\event;

use addon\platformpintuan\model\PintuanTrader\PintuanCommission;
use config\Service2;

/**
 * 拼团佣金计算
 */
class PintuanOrderCommissionSettlement
{

    public function handle($params)
    {
        //$pintuan_commission = new PintuanCommission();
        //$res= $pintuan_commission->settlement($params['order_id']);
        //return $res;
        $service = new Service2();

        $req_body = [];
        $req_body['orderId'] = $params['order_id'];

        $resp = $service->post("/flow/conf/feature/SCRIPT_TZPF_CLOSE_ORDER", json_encode($req_body));

        if( $resp->code==0){
            // trace('pass');
            trace('拼团订单结算完成, orderId:'.$params['order_id']);
        }else{
            trace('拼团订单结算失败, orderId:'.$params['order_id']);
        }

        return this.success();
    }
}