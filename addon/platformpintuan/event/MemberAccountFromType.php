<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformpintuan\event;

/**
 * 会员账户变化来源类型
 */
class MemberAccountFromType
{
    
	public function handle($data)
	{
    	$from_type = [
    		'balance' => [
    			'pintuan_bidding' => [
    				'type_name' => '批发竞标',
    				'type_url' => '',
    			],
                'pintuan_bidding_fail_refund' => [
                    'type_name' => '批发竞标失败退回',
                    'type_url' => '',
                ],
                'pintuan_end_refund_bidding_money' => [
                    'type_name' => '批发结束竞标金额退回',
                    'type_url' => '',
                ],
    		],
    		'balance_money' => [
    			'pintuan_bidding' => [
    				'type_name' => '批发竞标',
    				'type_url' => '',
    			],
                'pintuan_bidding_fail_refund' => [
                    'type_name' => '批发竞标失败金额退回',
                    'type_url' => '',
                ],
                'pintuan_end_refund_bidding_money' => [
                    'type_name' => '批发结束竞标金额退回',
                    'type_url' => '',
                ],
                'pintuan_trader_commission' => [
                    'type_name' => '团长佣金',
                    'type_url' => '',
                ],
                'pintuan_source_agent_one_commission' => [
                    'type_name' => '团长上级佣金',
                    'type_url' => '',
                ],
                'pintuan_source_agent_two_commission' => [
                    'type_name' => '团长上上级佣金',
                    'type_url' => '',
                ],
    		],
    	];
        if($data == ''){
            return $from_type;
        }else{
            return $from_type[$data];
        }
	    
	}
}