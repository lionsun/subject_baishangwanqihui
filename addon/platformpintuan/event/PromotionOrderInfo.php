<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformpintuan\event;

/**
 * 活动类型
 */
class PromotionOrderInfo
{
    /**
     * 营销订单详情
     * @param $param
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
	public function handle($param)
	{
	    if($param['promotion_type'] == 'platformpintuan'){
	        $order_info = model('promotion_platform_pintuan_order')->getInfo([['order_id', '=', $param['order_id']]]);
	        return $order_info;
        }
	}
}