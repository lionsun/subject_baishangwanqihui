<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformpintuan\event;

use addon\platformpintuan\model\PintuanTrader\PintuanBidding as PintuanBiddingModel;
use addon\platformpintuan\model\Pintuan as PintuanModel;
use think\facade\Db;

/**
 * 团长批发活动时间检测
 */
class CheckSameTimePromotion
{
	public function handle($param)
	{
        $mode = $param['mode'];
        $id = $param['id'];
        $time_info = $param['time_info'];
        //不检测条件
        $uncheck_condition = !empty($param['uncheck_condition']) ? json_decode($param['uncheck_condition'], true) : null;

        if($mode == 'spu'){
            $condition = [['goods_id', '=', $id]];
        }else{
            $condition = [['sku_id', '=', $id]];
        }
        $show_info = [];
        $bidding_list = model('promotion_platform_pintuan_bidding')->getList($condition, 'bidding_id, pintuan_name, sku_id, start_time, end_time, bidding_status, bidding_success_num');
        foreach($bidding_list as $val){
            if($val['bidding_status'] == PintuanBiddingModel::BIDDING_STATUS_NOT_START || $val['bidding_status'] == PintuanBiddingModel::BIDDING_STATUS_IN_PROCESS || ($val['bidding_status'] == PintuanBiddingModel::BIDDING_STATUS_ENDED && $val['bidding_success_num'] > 0)){
                if(!($uncheck_condition && $uncheck_condition['promotion_type'] == 'platformpintuan' && $val['bidding_id'] == $uncheck_condition['promotion_id'] && $mode == $uncheck_condition['mode'] && $uncheck_condition['id'] == $val['sku_id'])){
                    if(checkSameTime($time_info, $val)){
                        $show_info[] = [
                            'name' => $val['pintuan_name'],
                            'start_time' => $val['start_time'],
                            'end_time' => $val['end_time'],
                        ];
                    }
                }
            }
        }
        if(!empty($show_info)){
            $res = [
                'promotion_list' => $show_info,
                'type' => 'platformpintuan',
                'type_name' => '团长批发',
            ];
            return $res;
        }
    }

}