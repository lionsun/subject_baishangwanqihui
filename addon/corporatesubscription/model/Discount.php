<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\corporatesubscription\model;

use app\model\BaseModel;
use app\model\system\Cron;
use think\facade\Db;

/**
 * 企业集采
 */
class Discount extends BaseModel
{
    const PROMOTION_TYPE = 'corporatesubscription';
    const PROMOTION_TYPE_NAME = '企业集采';

    /**
     * 添加企业集采商品
     * @param  $goods_ids
     */
    public function addDiscountGoods($add_corporate_subscription_data,$condition,$corporate_subscription_data)
    {
        $goods_id = $add_corporate_subscription_data['goods_id'];
        if(model('promotion_corporate_subscription_goods')->getInfo([['goods_id','=',$goods_id]])){
            return $this->error("", "不能重复添加");
        }
        //添加折扣商品
        $res = model('promotion_corporate_subscription_goods')->add($add_corporate_subscription_data);
        model('goods_sku')->update($corporate_subscription_data,$condition);
        model('goods')->update($corporate_subscription_data,$condition);
        if($res){
            return $this->success($res);
        }else{
            return $this->error("", "添加失败");
        }
    }

    /**
     * 编辑企业集采商品
     * @param  $goods_ids
     */
    public function editDiscountGoods($edit_corporate_subscription_data,$condition,$corporate_subscription_data)
    {
        //编辑采购商品
        $res = model('promotion_corporate_subscription_goods')->update($edit_corporate_subscription_data,$condition);
        model('goods_sku')->update($corporate_subscription_data,$condition);
        model('goods')->update($corporate_subscription_data,$condition);
        if($res){
            return $this->success($res);
        }else{
            return $this->error("", "编辑失败");
        }
    }

    /**
     * 删除企业集采商品
     * @param  $id
     * @param  $goods_id
     * @param  $site_id
     */
    public function deleteDiscountGoods($goods_id, $site_id)
    {
        $upd_data = [
            'subscription_open_state' => 0,
            'corporate_subscription'  => '',
            'subscription_status'     => 0,
            'discount_state'     => 0,
            'discount_content'     => [],
        ];
        $res = model('goods_sku')->update($upd_data,[['goods_id', '=', $goods_id],['site_id', '=', $site_id]]);
        $upd_data['subscription_check_reason'] = '';
        $res = model('goods')->update($upd_data,[['goods_id', '=', $goods_id],['site_id', '=', $site_id]]);
        $res = model('promotion_corporate_subscription_goods')->delete([['goods_id', '=', $goods_id],['site_id', '=', $site_id]]);
        return $this->success($res);
    }

    /**
     * 获取企业集采基础信息
     * @param $site_id
     */
    public function getDiscountInfo($condition)
    {
        $info = model('promotion_corporate_subscription_goods')->getInfo($condition, '*');
        return $this->success($info);
    }

    public function getDiscountGoods($condition)
    {
        $discount_list  = model('promotion_corporate_subscription_goods')->getList($condition, 'goods_id');
        $discount_goods = '';
        if(!empty($discount_list)){
            foreach($discount_list as $key=>$val){
                $discount_goods .= $discount_list[$key]['goods_id'].',';
            }
        }
        $discount_goods = !empty($discount_goods) ? $discount_goods : '';
        if($discount_goods!=''){
            $discount_goods = substr($discount_goods,0,-1);
        }
        $order          = $discount_goods ? Db::raw("field(goods_id, {$discount_goods})") : '';
        $list           = model('goods')->getList([['goods_id', 'in', $discount_goods]], 'goods_id, goods_name, goods_image, price, cost_price, subscription_open_state,corporate_subscription,subscription_status,subscription_check_reason,discount_state,discount_content', $order);

        return $this->success($list);
    }

    /**
     * 企业认证设置
     * @param $goods_sku_array
     * @return array|\multitype
     */
    public function corporateSubscription($condition,$data)
    {
        $res = model('goods')->update($data, $condition);
        $res1 = model('goods_sku')->update($data, $condition);
        $res2 = model('promotion_corporate_subscription_goods')->update($data, $condition);
        if($res && $res1 && $res2){
            return $this->success($res);
        }else{
            return $this->error('操作失败');
        }
    }

    public function checkCorporateSubscription($data, $condition)
    {
        $res = model('goods')->update($data, $condition);
        $sku_data = [
            'subscription_status' => $data['subscription_status']
        ];
        $res1 = model('goods_sku')->update($sku_data, $condition);
        $res2 = model('promotion_corporate_subscription_goods')->update($data, $condition);
        if($res && $res1 && $res2){
            return $this->success($res);
        }else{
            return $this->error('','请勿重复操作');
        }
    }

    /**
     * 获取企业集采列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getDiscountList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $list = model('promotion_corporate_subscription_goods')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

    /**
     * 获取企业集采分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getGoodsPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = 'site_id,goods_id,sku_id,sku_name,price,sku_image,create_time,stock,goods_state,goods_class,goods_class_name,max_buy,min_buy,ladder_discount_id,nthmfold_discount_id,subscription_open_state,corporate_subscription,subscription_status,cost_price,manjian_discount_id', $alias = '', $join = '', $group = null, $limit = 100)
    {
        $list = model('promotion_corporate_subscription_goods')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, $group, $limit);
        return $this->success($list);
    }
}