<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\corporatesubscription\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\corporatesubscription\model\Discount as DiscountModel;
use app\model\goods\Goods as GoodsModel;
use app\model\member\MemberLevel as MemberLevelModel;

/**
 * 企业集采控制器
 */
class Discount extends BaseAdmin
{
	
	/**
	 * 企业集采列表
	 */
    public function lists()
    {
        $discount_model = new DiscountModel();
        if (request()->isAjax()) {
            $page= (int)input('page', '');
            $page_size= (int)input('page_size', '');
            $condition[] = ['p.subscription_status','in','1,2,3'];
            $field = 'p.*,gs.goods_id,gs.sku_id,gs.sku_name as goods_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image as goods_image,gs.goods_name,gs.site_id,gs.website_id,gs.is_own,gs.is_free_shipping,gs.introduction,gs.promotion_type,s.site_name,gs.goods_spec_format,gs.is_virtual,gs.ladder_discount_id,gs.source,gs.nthmfold_discount_id,gs.subscription_open_state,gs.corporate_subscription,gs.cost_price,gs.discount_state,gs.discount_content';
            $alias = 'p';
            $join = [
                ['goods_sku gs', 'p.goods_id = gs.goods_id', 'left']
            ];
            $join[] = [ 'shop s', 's.site_id = gs.site_id', 'inner'];
            // $join[] = [ 'goods g', 'gs.sku_id = g.sku_id', 'inner' ];
            $order_by = 'p.id desc';
            //只查看处于开启状态的店铺
            $condition[] = ['s.shop_status', '=', 1];
            $list = $discount_model->getGoodsPageList($condition, $page, $page_size, $order_by, $field, $alias, $join);
            return $list;
        } else {
            $member_level_model = new MemberLevelModel();
            $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',2]])['data'];
            $this->assign("member_level_list", $member_level_list);
            return $this->fetch("discount/lists");
        }
    }

    public function checkCorporateSubscription(){
        $goods_id = input('goods_id', '');
        $subscription_status = (int)input('subscription_status', 0);
        $subscription_check_reason= input('subscription_check_reason', '');
        $condition= [ ['goods_id','in',$goods_id ]];
        $data = [
            'subscription_status' => $subscription_status,
            'subscription_check_reason' => $subscription_check_reason,
        ];
        $discount_model = new DiscountModel();
        return $discount_model->checkCorporateSubscription($data,$condition);
    }

    public function closeCorporateSubscription(){
        $goods_id = input("goods_id", 0);
        $data = [
            'subscription_open_state' => 0,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->corporateSubscription($condition,$data);
        return $res;
    }

    public function openCorporateSubscription(){
        $goods_id = input("goods_id", 0);
        $data = [
            'subscription_open_state' => 1,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->corporateSubscription($condition,$data);
        return $res;
    }

}