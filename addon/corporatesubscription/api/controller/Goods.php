<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\corporatesubscription\api\controller;

use app\api\controller\BaseApi;
use addon\corporatesubscription\model\Discount as DiscountModel;
use app\model\goods\Goods as GoodsModel;
use app\model\member\Member as MemberModel;
use app\model\shop\Shop as ShopModel;

/**
 * 企业集采
 */
class Goods extends BaseApi
{
	/**
	 * 基础信息
	 */
	public function info()
	{
		$sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
		if (empty($sku_id)) {
			return $this->response($this->error('', 'REQUEST_SKU_ID'));
		}
		$goods = new GoodsModel();
		$field = "sku_id,sku_name,sku_spec_format,price,cost_price,market_price,discount_price,promotion_type,start_time,end_time,stock,click_num,sale_num,collect_num,sku_image,sku_images,goods_id,site_id,goods_content,goods_state,verify_state,is_virtual,is_free_shipping,goods_spec_format,goods_attr_format,introduction,unit,video_url,evaluate,category_id,category_id_1,category_id_2,category_id_3,category_name,max_buy,min_buy,source,ladder_discount_id,nthmfold_discount_id,subscription_open_state,corporate_subscription,subscription_status,cost_price,manjian_discount_id,discount_content,discount_state";
		$info = $goods->getGoodsSkuInfo([ [ 'sku_id', '=', $sku_id ] ], $field);
	    if(!empty($info)){
	        $info['data'] = $goods->getGoodsPromotionInfo($info['data']);
	    }
		$info['data'] = $this->dealWithPrice($info['data']);
	
		return $this->response($info);
	}
	
    /**
     * 企业集采列表
     */
    public function page(){
        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
        $condition[] = ['p.subscription_open_state','=',1];
        $condition[] = ['p.subscription_status','=', 2];
        $discount_model = new DiscountModel();
        $field = 'p.*,gs.goods_id,gs.sku_id,gs.sku_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.goods_name,gs.site_id,gs.website_id,gs.is_own,gs.is_free_shipping,gs.introduction,gs.promotion_type,g.goods_image,g.site_name,gs.goods_spec_format,gs.is_virtual,gs.ladder_discount_id,gs.source,gs.nthmfold_discount_id,gs.subscription_open_state,gs.corporate_subscription,gs.cost_price,gs.discount_state,gs.discount_content';
        $alias = 'p';
        $join = [
            ['goods_sku gs', 'p.goods_id = gs.goods_id', 'left']
        ];
        $join[] = [ 'shop s', 's.site_id = gs.site_id', 'inner'];
        $join[] = [ 'goods g', 'gs.sku_id = g.sku_id', 'inner' ];
        $order_by = 'p.id desc';
        //只查看处于开启状态的店铺
        $condition[] = ['s.shop_status', '=', 1];
        $list = $discount_model->getGoodsPageList($condition, $page, $page_size, $order_by, $field, $alias, $join);
        foreach($list['data']['list'] as $key=>$val){
            $list['data']['list'][$key] = $this->dealWithPrice($val);
        }
        return $this->response($list);
    }

    /**
     * 企业集采详情
     */
    public function detail(){
        $sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
        if (empty($sku_id)) {
            return $this->response($this->error('', 'REQUEST_SKU_ID'));
        }
		
		$res = [];
		
        $goods_model = new GoodsModel();
        $goods_sku_detail = $goods_model->getGoodsSkuDetail($sku_id);
        $goods_sku_detail = $goods_sku_detail[ 'data' ];
        $goods_sku_detail['goods_content'] = dealWithEditorContent($goods_sku_detail['goods_content']);
        $goods_sku_detail = $this->dealWithPrice($goods_sku_detail);
		$res[ 'goods_sku_detail' ] = $goods_sku_detail;
		
		if (empty($goods_sku_detail)) return $this->response($this->error($res));
		$goods_info = $goods_model->getGoodsInfo([['goods_id', '=', $goods_sku_detail['goods_id']]])['data'] ?? [];
		if (empty($goods_info)) return $this->response($this->error([], '找不到商品'));
		$res[ 'goods_info' ] = $goods_info;
		
		if ($goods_sku_detail['max_buy'] > 0){
		    if($this->member_id > 0){
		        $res[ 'goods_sku_detail' ]['purchased_num'] = $goods_model->getGoodsPurchasedNum($goods_sku_detail['goods_id'], $this->member_id);
		    }
		}
		//店铺信息
		$shop_model = new ShopModel();
		$shop_info = $shop_model->getShopInfo([ [ 'site_id', '=', $goods_sku_detail[ 'site_id' ] ] ], 'site_id,site_name,is_own,logo,avatar,banner,seo_description,qq,ww,telephone,shop_desccredit,shop_servicecredit,shop_deliverycredit,shop_baozh,shop_baozhopen,shop_baozhrmb,shop_qtian,shop_zhping,shop_erxiaoshi,shop_tuihuo,shop_shiyong,shop_shiti,shop_xiaoxie,shop_sales,sub_num');
		
		$shop_info = $shop_info[ 'data' ];
		$res[ 'shop_info' ] = $shop_info;
		
		return $this->response($this->success($res));
    }
	
	/**
	 * 处理价格
	 * @param {Object} $goods_sku_detail
	 */
	protected function dealWithPrice($goods_sku_detail)
	{
		$token = $this->checkToken();
		if($token['code'] >= 0){
		    $field = 'member_id,member_type,member_level,member_level_name';
		    //登录状态显示登录后的价格
		    $member_model = new MemberModel();
		    $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$field)['data'];
		    if($goods_sku_detail['subscription_open_state']==1 && $goods_sku_detail['subscription_status']==2){
		        $corporate_subscription_goods_arr = json_decode($goods_sku_detail['corporate_subscription'],true);
		        $subscription_arr = array();
		        foreach($corporate_subscription_goods_arr as $k => $corporate_subscription_goods){
		            if($memberInfo['member_type']!=0) {
		                if ($corporate_subscription_goods['level_id'] == $memberInfo['member_level']) {
		                    $subscription_rate = $corporate_subscription_goods['level_rate'];
		                } elseif ($corporate_subscription_goods['level_id'] < $memberInfo['member_level']){
		                    $subscription_arr[$k] = $corporate_subscription_goods;
		                    $subscription_arr[$k]['corporate_subscription_goods_price'] = round($goods_sku_detail['cost_price']*(1+$corporate_subscription_goods['level_rate']/100),2);
		                }
		            }else{
		                if ($corporate_subscription_goods['level_id'] > 0){
		                    $subscription_arr[$k] = $corporate_subscription_goods;
		                    $subscription_arr[$k]['corporate_subscription_goods_price'] = round($goods_sku_detail['cost_price']*(1+$corporate_subscription_goods['level_rate']/100),2);
		                }
		                $subscription_rate = $corporate_subscription_goods_arr[0]['level_rate'];
		            }
		        }
		        $goods_sku_detail['corporate_subscription_price_arr'] = $subscription_arr;
		        $goods_sku_detail['corporate_subscription_price'] = round($goods_sku_detail['cost_price']*(1+$subscription_rate/100),2);
		    }
		}else{
		    $corporate_subscription_goods_arr = json_decode($goods_sku_detail['corporate_subscription'],true);
		    $subscription_rate = $corporate_subscription_goods_arr[0]['level_rate'];
		    $goods_sku_detail['corporate_subscription_price'] = round($goods_sku_detail['cost_price']*(1+$subscription_rate/100),2);
		}
		return $goods_sku_detail;
	}
}