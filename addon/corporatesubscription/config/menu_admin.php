<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT',
        'title' => '企业集采',
        'url' => 'corporatesubscription://admin/discount/lists',
        'parent' => 'PROMOTION_SHOP',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_CLOSE',
                'title' => '关闭',
                'url' => 'corporatesubscription://admin/discount/close',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_DELETE',
                'title' => '删除',
                'url' => 'corporatesubscription://admin/discount/delete',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_CHECK',
                'title' => '认证审核',
                'url' => 'corporatesubscription://admin/discount/check',
                'sort'    => 1,
                'is_show' => 0
            ],
        
        ]
    ],
];
