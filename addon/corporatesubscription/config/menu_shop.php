<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT',
        'title' => '企业集采',
        'url' => 'corporatesubscription://shop/discount/lists',
        'parent' => 'PROMOTION_CENTER',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_GOODS_ADD',
                'title' => '添加商品',
                'url' => 'corporatesubscription://shop/discount/add',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_GOODS_EDIT',
                'title' => '编辑商品',
                'url' => 'corporatesubscription://shop/discount/edit',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_GOODS_SELECT',
                'title' => '商品选择',
                'url' => 'corporatesubscription://shop/discount/selectgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_CORPORATE_SUBSCRIPTION_DISCOUNT_GOODS_DELETE',
                'title' => '商品删除',
                'url' => 'corporatesubscription://shop/discount/deletegoods',
                'sort'    => 1,
                'is_show' => 0
            ],
        ]
    ],
];
