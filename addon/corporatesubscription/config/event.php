<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //企业集采开启
        'Opencorporatesubscription'  => [
            'addon\corporatesubscription\event\OpenDiscount',
        ],
        //企业集采关闭
        'Closecorporatesubscription'  => [
            'addon\corporatesubscription\event\CloseDiscount',
        ],
        //展示活动
        'ShowPromotion' => [
            'addon\corporatesubscription\event\ShowPromotion',
        ],
        //检测相同时间段营销活动
        'CheckSameTimePromotion' => [
            'addon\corporatesubscription\event\CheckSameTimePromotion',
        ],
        //活动详情
        'PromotionInfo' => [
            'addon\corporatesubscription\event\PromotionInfo',
        ],
        //活动详情
        'PromotionPrice' => [
            'addon\corporatesubscription\event\PromotionPrice',
        ],
        //活动页面
        'PcNavLink' => [
            'addon\corporatesubscription\event\PcNavLink',

        ],
    ],

    'subscribe' => [
    ],
];
