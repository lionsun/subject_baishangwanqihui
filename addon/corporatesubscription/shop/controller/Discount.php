<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\corporatesubscription\shop\controller;


use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\member\MemberLevel as MemberLevelModel;
use app\model\shop\Shop as ShopModel;
use app\shop\controller\BaseShop;
use addon\corporatesubscription\model\Discount as DiscountModel;

/**
 * 企业集采控制器
 */
class Discount extends BaseShop
{
	
	/**
	 * 企业集采商品列表
	 */
	public function lists()
	{
        $discount_model = new DiscountModel();
		if (request()->isAjax()) {
            $page= input('page', '');
            $page_size= input('page_size', '');
            $field = 'p.*,gs.goods_id,gs.sku_id,gs.sku_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.goods_name,gs.site_id,gs.website_id,gs.is_own,gs.is_free_shipping,gs.introduction,gs.promotion_type,g.goods_image,g.site_name,gs.goods_spec_format,gs.is_virtual,gs.ladder_discount_id,gs.source,gs.nthmfold_discount_id,gs.subscription_open_state,gs.corporate_subscription,gs.cost_price,gs.discount_state,gs.discount_content';
            $alias = 'p';
            $join = [
                ['goods_sku gs', 'p.goods_id = gs.goods_id', 'left']
            ];
            $join[] = [ 'shop s', 's.site_id = gs.site_id', 'inner'];
            $join[] = [ 'goods g', 'gs.sku_id = g.sku_id', 'inner' ];
            $order_by = 'p.id desc';
            //只查看处于开启状态的店铺
            $condition[] = ['p.subscription_status','in','1,2,3'];
            $condition[] = ['s.shop_status', '=', 1];
            $list = $discount_model->getGoodsPageList($condition, $page, $page_size, $order_by, $field, $alias, $join);
            return $list;
		} else {
            $member_level_model = new MemberLevelModel();
            $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',2]])['data'];
            $this->assign("member_level_list", $member_level_list);
			return $this->fetch("discount/lists");
		}
	}

    /**
     * 添加商品企业集采
     */
    public function add()
    {
        if (request()->isAjax()) {
            $site_model = new ShopModel();
            $site_info = $site_model->getShopInfo([['site_id', '=', $this->site_id]], 'site_name');
            //获取商品信息
            $goods_id = input('goods_id', 0);
            $goods_name = input('goods_name', '');
            $discount_state = input('discount_state', 0);
            $discount_content = input('discount_content', '');
            $subscription_open_state = input('subscription_open_state', 0);
            $corporate_subscription = input('corporate_subscription', '');
            $add_corporate_subscription_data = [
                'site_id'                 => $this->site_id,
                'site_name'               => $site_info[ 'data' ][ 'site_name' ],
                'goods_id'                => $goods_id,
                'goods_name'              => $goods_name,
                'discount_state'          => $discount_state,
                'discount_content'        => $discount_content,
                'subscription_status'     => 1,
                'subscription_open_state' => $subscription_open_state,
                'corporate_subscription'  => $corporate_subscription,
            ];
            $condition = [
                'site_id'  => $this->site_id,
                'goods_id' => $goods_id
            ];
            $corporate_subscription_data = [
                'subscription_status'     => 1,
                'subscription_open_state' => $subscription_open_state,
                'corporate_subscription'  => $corporate_subscription,
                'discount_state'          => $discount_state,
                'discount_content'        => $discount_content,
            ];
            $discount_model = new DiscountModel();
            return $discount_model->addDiscountGoods($add_corporate_subscription_data,$condition,$corporate_subscription_data);
        } else {
            $member_level_model = new MemberLevelModel();
            $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',2]])['data'];
            $this->assign("member_level_list", $member_level_list);
            return $this->fetch("discount/add");
        }
    }

    /**
     * 编辑商品企业集采
     */
    public function edit(){
        if (request()->isAjax()) {
            $site_model = new ShopModel();
            $site_info = $site_model->getShopInfo([['site_id', '=', $this->site_id]], 'site_name');
            //获取商品信息
            $goods_id = input('goods_id', 0);
            $discount_state = input('discount_state', 0);
            $discount_content = input('discount_content', 0);
            $subscription_open_state = input('subscription_open_state', 0);
            $corporate_subscription = input('corporate_subscription', 0);
            $edit_corporate_subscription_data = [
                'site_name'               => $site_info[ 'data' ][ 'site_name' ],
                'discount_state'          => $discount_state,
                'discount_content'        => $discount_content,
                'subscription_status'     => 1,
                'subscription_open_state' => $subscription_open_state,
                'corporate_subscription'  => $corporate_subscription,
            ];
            $condition = [
                'site_id' => $this->site_id,
                'goods_id' => $goods_id
            ];
            $corporate_subscription_data = [
                'subscription_status'     => 1,
                'subscription_open_state' => $subscription_open_state,
                'corporate_subscription'  => $corporate_subscription,
                'discount_state'          => $discount_state,
                'discount_content'        => $discount_content,
            ];
            $discount_model = new DiscountModel();
            return $discount_model->editDiscountGoods($edit_corporate_subscription_data,$condition,$corporate_subscription_data);
        } else {
            $goods_id = input('goods_id', 0);
            $discount_model = new DiscountModel();
            $corporate_subscription = $discount_model->getDiscountInfo([['goods_id','=',$goods_id]])['data'];
            if(!empty($corporate_subscription)){
                $goods_model = new GoodsModel();
                $corporate_subscription['corporate_subscription'] = json_decode($corporate_subscription['corporate_subscription'],true);
                $corporate_subscription['goods_image'] = $goods_model->getGoodsDetail($goods_id)['data']['goods_image'];
                $corporate_subscription['sku_list'] = $goods_model->getGoodsSkuList([['goods_id','=',$goods_id]])['data'];
            }
            $this->assign("detail", $corporate_subscription);
            return $this->fetch("discount/edit");
        }
    }

    /**
     * 关闭企业集采
     */
    public function closeCorporateSubscription(){
        $goods_id = input("goods_id", 0);
        $data = [
            'subscription_open_state' => 0,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->corporateSubscription($condition,$data);
        return $res;
    }
    
    /**
     * 开启企业集采
     */
    public function openCorporateSubscription(){
        $goods_id = input("goods_id", 0);
        $data = [
            'subscription_open_state' => 1,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->corporateSubscription($condition,$data);
        return $res;
    }

	
	/**
	 * 删除商品
	 */
	public function deleteGoods()
	{
		if (request()->isAjax()) {
			$goods_id = input('goods_id', '');
			$discount_model = new DiscountModel();
			return $discount_model->deleteDiscountGoods($goods_id, $this->site_id);
		}
	}

    /**
     * 商品选择组件
     * @return \multitype
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $goods_id = input('goods_id', 0);
            $is_virtual = input('is_virtual', '');// 是否虚拟类商品（0实物1.虚拟）
            $min_price = input('min_price', 0);
            $max_price = input('max_price', 0);
            $goods_class = input('goods_class', "");// 商品类型，实物、虚拟
            $category_id = input('category_id', "");// 商品分类id
            $promotion_type = input('promotion_type', "");

            $condition = [
                ['is_delete', '=', 0],
                ['goods_state', '=', 1],
                ['site_id', '=', $this->site_id]
            ];


            if (!empty($goods_name)) {
                $condition[] = ['goods_name', 'like', '%' . $goods_name . '%'];
            }
            if ($is_virtual !== "") {
                $condition[] = ['is_virtual', '=', $is_virtual];
            }
            if (!empty($goods_id)) {
                $condition[] = ['goods_id', '=', $goods_id];
            }
            if (!empty($category_id)) {
                $condition[] = ['category_id', 'like', [$category_id, '%' . $category_id . ',%', '%' . $category_id, '%,' . $category_id . ',%'], 'or'];
            }

            if (!empty($promotion_type)) {
                $condition[] = ['promotion_addon', 'like', "%{$promotion_type}%"];
            }


            if ($goods_class !== "") {
                $condition[] = ['goods_class', '=', $goods_class];
            }

            if ($min_price != "" && $max_price != "") {
                $condition[] = ['price', 'between', [$min_price, $max_price]];
            } elseif ($min_price != "") {
                $condition[] = ['price', '<=', $min_price];
            } elseif ($max_price != "") {
                $condition[] = ['price', '>=', $max_price];
            }

            $order = 'create_time desc';
            $goods_model = new GoodsModel();
            $field = 'goods_id,goods_name,goods_class_name,goods_image,price,goods_stock,create_time,is_virtual,cost_price';
            $goods_list = $goods_model->getGoodsPageList($condition, $page, $page_size, $order, $field);
            return $goods_list;
        } else {

            //已经选择的商品sku数据
            $select_id = input('select_id', '');
            $mode = input('mode', 'spu');
            $max_num = input('max_num', 0);
            $min_num = input('min_num', 0);
            $is_virtual = input('is_virtual', '');
            $disabled = input('disabled', 0);
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao
            $discount_id = input('discount_id', 0);//企业集采id

            $this->assign('select_id', $select_id);
            $this->assign('mode', $mode);
            $this->assign('max_num', $max_num);
            $this->assign('min_num', $min_num);
            $this->assign('is_virtual', $is_virtual);
            $this->assign('disabled', $disabled);
            $this->assign('promotion', $promotion);
            $this->assign('discount_id', $discount_id);

            // 营销活动
            $goods_promotion_type = event('GoodsPromotionType');
            $this->assign('promotion_type', $goods_promotion_type);

            //商品分类
            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList([], 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);
            return $this->fetch("discount/goods_select");
        }
    }
}