<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\pintuan\event;

use addon\pintuan\model\PintuanTrader\PintuanBidding as PintuanBiddingModel;
use addon\pintuan\model\Pintuan as PintuanModel;
use think\facade\Db;

/**
 * 普通拼团活动时间检测
 */
class CheckSameTimePromotion
{
	public function handle($param)
	{
        $mode = $param['mode'];
        $id = $param['id'];
        $time_info = $param['time_info'];

        $show_info = [];
        if($mode == 'spu'){
            $pintuan_list = model('promotion_pintuan')->getList([['goods_id', '=', $id], ['pintuan_type', '=', PintuanModel::PINTUAN_TYPE_SHOP], ['status', 'in', [PintuanModel::PINTUAN_STATUS_NOT_START, PintuanModel::PINTUAN_STATUS_IN_PROCESS]]], 'pintuan_name,start_time,end_time');
            foreach($pintuan_list as $val){
                if(checkSameTime($time_info, $val)){
                    $show_info[] = [
                        'name' => $val['pintuan_name'],
                        'start_time' => $val['start_time'],
                        'end_time' => $val['end_time'],
                    ];
                }
            }
            if(!empty($show_info)){
                $res = [
                    'promotion_list' => $show_info,
                    'type' => 'pintuan',
                    'type_name' => '拼团',
                ];
                return $res;
            }
        }
    }

}