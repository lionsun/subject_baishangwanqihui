<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\pintuan\admin\controller;

use app\admin\controller\BaseAdmin as BaseController;
/**
 * 拼团商
 */
class BaseAdmin extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->assign('extend_base', 'app/admin/view/base.html');
    }

    /**
     * 加载模板输出
     * @param string $template
     * @param array $vars
     * @param array $config
     * @return mixed
     */
    protected function fetch($template = '', $vars = [], $config = [])
    {
        $config = array_merge([
            'PINTUAN_JS' => __ROOT__ . '/'. ADDON_DIR_NAME .'/pintuan/admin/view/public/js',
            'PINTUAN_IMG' => __ROOT__ . '/'. ADDON_DIR_NAME .'/pintuan/admin/view/public/img',
            'PINTUAN_CSS' => __ROOT__ . '/'. ADDON_DIR_NAME .'/pintuan/admin/view/public/css',
        ], $config);
        return parent::fetch($template, $vars, $config);
    }
}