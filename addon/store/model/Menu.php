<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\store\model;

use app\model\BaseModel;

/**
 * 菜单管理
 * @author Administrator
 *
 */
class Menu extends BaseModel
{
    public function uninstall()
    {
        $res = model("menu")->delete([['app_module', '=', 'store']]);
        return $this->success($res);
    }
}