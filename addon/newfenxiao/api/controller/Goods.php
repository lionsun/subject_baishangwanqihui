<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\api\controller;

use addon\newfenxiao\model\FenxiaoGoods;
use addon\newfenxiao\model\FenxiaoGoodsSku;
use app\api\controller\BaseApi;
use addon\newfenxiao\model\Fenxiao as FenxiaoModel;
use addon\newfenxiao\model\FenxiaoGoodsSku as FenxiaoGoodsSkuModel;
use addon\newfenxiao\model\Config as ConfigModel;

/**
 * 分销商品
 */
class Goods extends BaseApi
{

	/**
	 * 分销商品详情
	 * @return false|string
	 */
	public function detail()
	{
		$token = $this->checkToken();
		if ($token[ 'code' ] < 0) return $this->response($token);

		$sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
		if (empty($sku_id)) {
			return $this->response($this->error('', 'REQUEST_SKU_ID'));
		}

		$fenxiao_model = new FenxiaoModel();
		$fenxiao_info = $fenxiao_model->getFenxiaoInfo([ [ 'member_id', '=', $this->member_id ] ], "fenxiao_id,level_id");
		$fenxiao_info = $fenxiao_info[ 'data' ];

		$fenxiao_goods_sku_model = new FenxiaoGoodsSkuModel();

		$condition = [ [ 'fgs.sku_id', '=', $sku_id ], [ 'fgs.level_id', '=', $fenxiao_info[ 'level_id' ] ] ];
		$res = $fenxiao_goods_sku_model->getFenxiaoGoodsSkuDetail($condition);

		$config = new ConfigModel();
		$basics_config = $config->getFenxiaoBasicsConfig();
		$basics_config = $basics_config[ 'data' ][ 'value' ];

		$words_config = $config->getFenxiaoWordsConfig();
		$words_config = $words_config[ 'data' ][ 'value' ];

		if ($res[ 'data' ]) {
			$discount_price = $res[ 'data' ][ 'discount_price' ];

			// 一级佣金比例/金额
			$money = 0;
			if ($res['data']['one_rate'] > 0) {
				$money = number_format($discount_price * $res[ 'data' ][ 'one_rate' ] / 100, 2);
			}elseif ($res['data']['one_money'] > 0){
                $money = $res['data']['one_money'];
            }

			$res[ 'data' ][ 'commission_money' ] = $money;
			$res[ 'data' ][ 'words_account' ] = $words_config[ 'account' ];

		}
		return $this->response($res);
	}

	/**
	 * 分销商品分页列表
	 */
	public function page()
	{
		$page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
		$page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;

        $fenxiao_goods = new FenxiaoGoods();
        $list = $fenxiao_goods->getApiFenxiaoGoodsPage([], $page, $page_size );
		return $this->response($list);
	}

    /**
     * 分销市场分页列表
     */
    public function memberGoodsPage()
    {
        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $member_id = isset($this->params[ 'member_id' ]) ? $this->params[ 'member_id' ] : 0;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;

        $condition[] = array('nmg.member_id', '=', $member_id);
        $fenxiao_goods = new FenxiaoGoods();
        $list = $fenxiao_goods->getApiMemberFenxiaoGoodsPage($condition, $page, $page_size );
        return $this->response($list);
    }

    /**
     * 分销商品分页列表
     */
    public function getMemberGoodsPage2()
    {
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);

        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;

        $condition[] = array('nmg.member_id', '=', $this->member_id);
        $fenxiao_goods = new FenxiaoGoods();
        $list = $fenxiao_goods->getApiMemberFenxiaoGoodsPage($condition, $page, $page_size );
        return $this->response($list);
    }

    /**
     * 加入会员分销
     */
	public function addMemberGoods(){

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $goods_id = isset($this->params[ 'goods_id' ]) ? $this->params[ 'goods_id' ] : 0;
        $fenxiao_goods = new FenxiaoGoods();
        $res = $fenxiao_goods->addMemberGoods($goods_id, $this->member_id);
        return $this->response($res);
    }

    /**
     * 修改销售价
     */
    public function updateMemberGoodsPrice(){

//        $sku_data = array(
//            array('sku_id' => 1, 'price'=> 2),
//            array('sku_id' => 2, 'price'=> 3)
//        );
//        dump(json_encode($sku_data));exit();
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $goods_id = isset($this->params[ 'goods_id' ]) ? $this->params[ 'goods_id' ] : 0;
        $sku_data = isset($this->params[ 'sku_data' ]) ? $this->params[ 'sku_data' ] : 0;
        $fenxiao_goods = new FenxiaoGoods();
        $res = $fenxiao_goods->updateMemberGoods(json_decode($sku_data, true), $goods_id, $this->member_id);
        return $this->response($res);
    }

    /**
     * 获取会员分销商品详情
     */
    public function getMemberGoodsInfo(){

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $goods_id = isset($this->params[ 'goods_id' ]) ? $this->params[ 'goods_id' ] : 0;

        $fenxiao_goods_sku = new FenxiaoGoodsSku();
        $goods_info = $fenxiao_goods_sku->getMemberGoodsDetail($goods_id, $this->member_id);
        return $this->response($goods_info);
    }

    /**
     * 会员分销市场中移除商品
     */
    public function deleteMemberGoods(){

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $goods_id = isset($this->params[ 'goods_id' ]) ? $this->params[ 'goods_id' ] : 0;
        $fenxiao_goods_sku = new FenxiaoGoodsSku();
        $res = $fenxiao_goods_sku->deleteMemberGoods($goods_id, $this->member_id);
        return $this->response($res);;
    }

}