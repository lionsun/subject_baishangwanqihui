<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\api\controller;

use addon\newfenxiao\model\Fenxiao as FenxiaoModel;
use addon\newfenxiao\model\FenxiaoLevel;
use addon\newfenxiao\model\FenxiaoOrder;
use addon\newfenxiao\model\Poster;
use app\api\controller\BaseApi;
use app\model\member\Member;
use Carbon\Carbon;

/**
 * 分销相关信息
 */
class Fenxiao extends BaseApi
{
	/**
	 * 获取分销商信息
	 */
    public function detail()
    {
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);


        $model = new FenxiaoModel();
        $info = $model->getFenxiaoDetailInfo($this->member_id );
        return $this->response($info);
    }
	
	/**
	 * 获取推荐人分销商信息
	 */
	public function sourceInfo()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$member = new Member();
		$member_info = $member->getMemberInfo([ ['member_id', '=', $this->member_id] ], 'fenxiao_id');
		$fenxiao_id = $member_info['data']['fenxiao_id'] ?? 0; 
		
		if (empty($fenxiao_id)) {
			return $this->response($this->error('', 'REQUEST_SOURCE_MEMBER'));
		}
		$condition = [
			[ 'fenxiao_id', '=', $fenxiao_id ]
		];
		
		$model = new FenxiaoModel();
		$info = $model->getFenxiaoInfo($condition, 'fenxiao_name');
		
		return $this->response($info);
	}
	
	/**
	 * 分销海报
	 * @return \app\api\controller\false|string
	 */
	public function poster()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$qrcode_param = isset($this->params['qrcode_param']) ? $this->params['qrcode_param'] : '';//二维码
		if (empty($qrcode_param)) {
			return $this->response($this->error('', 'REQUEST_QRCODE_PARAM'));
		}
		
		$qrcode_param = json_decode($qrcode_param, true);
		$qrcode_param['source_member'] = $this->member_id;
		
		$poster = new Poster();
		$res = $poster->distribution($this->params['app_type'], $this->params['page'], $qrcode_param);
		
		return $this->response($res);
	}
	
	/**
	 * 分销商等级信息
	 */
	public function level()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$level = $this->params['level'] ?? 0;
		
		$condition = [
			[ 'level_id', '=', $level ]
		];
		$model = new FenxiaoLevel();
		$info = $model->getLevelInfo($condition);
		
		return $this->response($info);
	}
	

	/**
	 * 查询我的团队的数量
	 */
	public function teamNum(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $fenxiao = new \addon\newfenxiao\model\Fenxiao();
        $res = $fenxiao->teamNum($this->member_id);
		return $this->response($res);
	}

    /**
     * @return false|string
     * 会员进过的分销市场
     */
	public function getMemberTuanzList(){

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $fenxiao = new \addon\newfenxiao\model\Fenxiao();
        $list = $fenxiao->getMemberTuanzList($this->member_id);
        return $this->response($list);
    }

    /**
     * 添加会员进过的分销市场
     */
    public function addMemberTuanz(){

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $fenxiao_member = $this->params['fenxiao_member'] ?? 0;

        $fenxiao = new \addon\newfenxiao\model\Fenxiao();
        $res = $fenxiao->addMemberTuanz($this->member_id, $fenxiao_member);
        return $this->response($res);
    }

    /**
     * 获取团队
     */
    public function getTearm(){

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $page = $this->params['page'] ?? 1;
        $page_size = $this->params['page_size'] ?? PAGE_LIST_ROWS;

        $fenxiao = new \addon\newfenxiao\model\Fenxiao();
        $list = $fenxiao->getTearm($this->member_id, $page, $page_size);
        return $this->response($list);
    }

    /**
     * 会员分销订单
     */
    public function getMemberOrder(){

        $page = $this->params['page'] ?? 1;
        $state = $this->params['state'] ?? 0;
        $page_size = $this->params['page_size'] ?? PAGE_LIST_ROWS;

        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $fenxiao_order = new FenxiaoOrder();
        $list = $fenxiao_order->getMemberOrder($this->member_id, $state, $page, $page_size);
        return $this->response($list);
    }

    /**
     * 业绩考核
     */
    public function yejiAssessment(){

        $fenxiao_order = new FenxiaoOrder();
        $fenxiao_order->memberTaskIssue();
    }
}