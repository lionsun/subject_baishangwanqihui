<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\shop\controller;

use addon\newfenxiao\model\FenxiaoGoodsSku;
use addon\fenxiao\model\FenxiaoOrder as FenxiaoOrderModel;
use addon\newfenxiao\model\FenxiaoGoods;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsShopCategory as GoodsShopCategoryModel;
use app\shop\controller\BaseShop;
use think\facade\Db;

/**
 * 分销控制器
 */
class Fenxiao extends BaseShop
{

    /**
     * 分销概况
     */
    public function index()
    {
        //获取分销的总金额
        $order_model = new FenxiaoOrderModel();
        $commission = $order_model->getFenxiaoOrderDetail([ [ 'site_id', '=', $this->site_id ], [ 'is_settlement', '=', 1 ], [ 'is_refund', '=', 0 ] ], 'sum(real_goods_money) as real_goods_money,sum(commission) as commission');
        if ($commission['data']['real_goods_money'] == null) {
            $commission['data']['real_goods_money'] = '0.00';
        }
        if ($commission['data']['commission'] == null) {
            $commission['data']['commission'] = '0.00';
        }
        $this->assign('shop_commission', $commission['data']);
        $goods_model = new GoodsModel();
        $fenxiao_goods_num = $goods_model->getGoodsInfo([ [ 'site_id', '=', $this->site_id ], [ 'is_fenxiao', '=', 1 ] ], 'count(goods_id) as fenxiao_goods_num');
        $this->assign('fenxiao_goods_num', $fenxiao_goods_num['data']['fenxiao_goods_num']);
        return $this->fetch("fenxiao/index");
    }

    /**
     *  商品列表
     */
    public function lists()
    {
        $goods_model = new FenxiaoGoods();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', "");
            $start_sale = input('start_sale', 0);
            $end_sale = input('end_sale', 0);
            $is_fenxiao = input('is_fenxiao', '');
            $goods_shop_category_ids = input('goods_shop_category_ids', '');
            $condition = [ [ 'is_delete', '=', 0 ], [ 'g.site_id', '=', $this->site_id ], [ 'verify_state', '=', 1 ] ];
            if (!empty($search_text)) {
                $condition[] = [ 'goods_name', 'like', '%' . $search_text . '%' ];
            }
            if ($is_fenxiao !== "") {
                $condition[] = [ 'nfg.state', '=', $is_fenxiao ];
            }
            if (!empty($start_sale)) $condition[] = [ 'sale_num', '>=', $start_sale ];
            if (!empty($end_sale)) $condition[] = [ 'sale_num', '<=', $end_sale ];
            if (!empty($goods_shop_category_ids)) $condition[] = [ 'goods_shop_category_ids', 'like', [ $goods_shop_category_ids, '%' . $goods_shop_category_ids . ',%', '%' . $goods_shop_category_ids, '%,' . $goods_shop_category_ids . ',%' ], 'or' ];
            $res = $goods_model->getGoodsPageList($condition, $page_index, $page_size);
            return $res;
        } else {
            //获取店内分类
            $goods_shop_category_model = new GoodsShopCategoryModel();
            $goods_shop_category_list = $goods_shop_category_model->getShopCategoryTree([ [ 'site_id', "=", $this->site_id ] ], 'category_id,category_name,pid,level');
            $goods_shop_category_list = $goods_shop_category_list['data'];
            $this->assign("goods_shop_category_list", $goods_shop_category_list);
            return $this->fetch("fenxiao/lists");
        }
    }

    /**
     * 添加活动
     */
    public function config()
    {
        $goods_id = input('goods_id');

        $fenxiao_goods_model = new FenxiaoGoods();
        $fenxiao_sku_model = new FenxiaoGoodsSku();
        $goods_info = $fenxiao_sku_model->getGoodsDetail($goods_id);
        if (request()->isAjax()) {
            Db::startTrans();
            try {
                $fenxiao_type = input('fenxiao_type', 1);
                $fenxiao_skus = input('fenxiao', []);
                $is_fenxiao = input('is_fenxiao', 0);

                if ($fenxiao_type == 2) {
                    $fenxiao_goods_sku_data = [];

                    foreach ($fenxiao_skus['sku_id'] as $key => $sku_id) {

                        $fenxiao_sku = [
                            'goods_id' => $goods_id,
                            'sku_id' => $sku_id,
                            'shop_cost' => $fenxiao_skus['shop_cost'][$key],
                            'site_id' => $this->site_id,
                        ];
                        $fenxiao_goods_sku_data[] = $fenxiao_sku;
                    }

                    $fenxiao_sku_model->deleteSku([ 'goods_id' => $goods_id ]);
                    $fenxiao_sku_model->addSkuList($fenxiao_goods_sku_data);
                }

                $fenxiao_goods_model = new FenxiaoGoods();
                $goods_data = array(
                    'state' => $is_fenxiao,
                );

                $re = $fenxiao_goods_model->editGoodsFenxiao($goods_data, [ [ 'goods_id', '=', $goods_id ], [ 'site_id', '=', $this->site_id ] ]);
                Db::commit();
                return $re;
            } catch (Exception $e) {
                Db::rollback();
                return error(-1, $e->getMessage());
            }
        }
        $fenxiao_skus = $fenxiao_goods_model->getSkuList([ 'goods_id' => $goods_id ]);

        $goods_info['data']['fenxiao_skus'] = $fenxiao_skus['data'];
        $goods_info['data']['goods_image'] =
            !empty($goods_info['data']['goods_image'])?explode(',', $goods_info['data']['goods_image'])[0]:'';


        $this->assign('goods_info', $goods_info['data']);
        return $this->fetch("fenxiao/config");
    }

    /**
     * 修改分销状态
     */
    public function modify()
    {
        if (request()->isAjax()) {
            $fenxiao_goods_model = new FenxiaoGoods();
            $goods_id = input('goods_id');
            $fenxiao_state = input('fenxiao_state', 0);

            $fenxiao_goods_info = $fenxiao_goods_model->getFenxiaoGoods([['goods_id', '=', $goods_id]]);
            if(empty($fenxiao_goods_info['data'])){

                $data = array(
                    'goods_id' => $goods_id,
                    'state' => 1,
                    'site_id' => $this->site_id
                );
                $res = $fenxiao_goods_model->addFenxiaoGoods($data);
            }else{
                $res = $fenxiao_goods_model->modifyFenxiaoGoods(['state' => $fenxiao_state], [['goods_id', '=', $goods_id]]);
            }

            return $res;
        }
    }
}