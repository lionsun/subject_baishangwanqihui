<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\shop\controller;

use addon\newfenxiao\model\FenxiaoOrder as FenxiaoOrderModel;
use addon\newfenxiao\model\FenxiaoOrder;
use app\model\order\Order as OrderModel;
use app\shop\controller\BaseShop;


/**
 * 分销订单控制器
 */
class Order extends BaseShop
{

    /*
     *  分销订单列表
     */
    public function lists()
    {
        if(request()->isAjax()) {
            $order_model = new FenxiaoOrderModel();
            $page_index = input('page_index', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);

            $order_label = input('order_label', 'goods');
            $search_text = input('search_text', '');
            $order_status = input('order_status', '');

            $start_time = input('start_time','');
            $end_time = input('end_time','');
            $condition = [['o.site_id', '=', $this->site_id]];
            if (!empty($search_text)) {
                $condition[] = ['og.sku_name|og.order_no', 'LIKE', "%{$search_text}%"];
                if ($order_label == 'goods') {
                    $condition[] = ['og.sku_name', 'LIKE', "%{$search_text}%"];
                }
                if ($order_label == 'order') {
                    $condition[] = ['og.order_no', 'LIKE', "%{$search_text}%"];
                }
            }
            if (!empty($order_status)) {
                $condition[] = ['mo.state', '=', $order_status];
            }

            if(!empty($start_time) && empty($end_time)){
                $condition[] = ['o.create_time','>=',date_to_time($start_time)];
            }elseif(empty($start_time) && !empty($end_time)){
                $condition[] = ['o.create_time','<=',date_to_time($end_time)];
            }elseif(!empty($start_time) && !empty(date_to_time($end_time))){
                $condition[] = ['o.create_time','between',[date_to_time($start_time),date_to_time($end_time)]];
            }

            $list = $order_model->getFenxiaoOrderPageList($condition, $page_index, $page_size, 'fenxiao_order_id DESC');
            return $list;
        }
        return $this->fetch('order/lists');
    }

    public function detail() {
        $fenxiao_order_model = new FenxiaoOrder();
        $fenxiao_order_id = input('fenxiao_order_id', '');
        $order_info = $fenxiao_order_model->getFenxiaoOrderDetail([['fenxiao_order_id', '=', $fenxiao_order_id]]);
        $this->assign('order_info', $order_info['data']);
        return $this->fetch('order/detail');
    }

}