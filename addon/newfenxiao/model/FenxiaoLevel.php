<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newfenxiao\model;

use app\model\BaseModel;


/**
 * 分销
 */
class FenxiaoLevel extends BaseModel
{

    /**
     * 获取分销商等级分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getLevelPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('new_fenxiao_level')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }

    /**
     * 获取分销等级信息
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLevelInfo($condition = [], $field = '*')
    {
        $res = model('new_fenxiao_level')->getInfo($condition, $field);
        return $this->success($res);
    }

    /**
     * 获取分销商等级列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getLevelList($condition = [], $field = '*', $order = '', $limit = null)
    {

        $list = model('new_fenxiao_level')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

}