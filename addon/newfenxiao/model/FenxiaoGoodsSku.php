<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newfenxiao\model;

use app\model\BaseModel;


/**
 * 分销
 */
class FenxiaoGoodsSku extends BaseModel
{

    /**
     * 批量添加分销商品
     * @param $data
     */
    public function addSkuList($data)
    {
        $re = model('new_fenxiao_goods_sku')->addList($data);
        return $this->success($re);
    }

    /**
     * @param $data
     * @return array
     * 修改分销商品
     */
    public function editSkuList($data, $condition)
    {
        $re = model('new_fenxiao_goods_sku')->update($data, $condition);
        return $this->success($re);
    }

    /**
     * 删除分销商品
     * @param array $condition
     * @return array
     */
    public function deleteSku($condition = [])
    {
        $res = model('new_fenxiao_goods_sku')->delete($condition);
        return $this->success($res);
    }

    /**
     * @param $sku_id
     * 获取sku详情
     */
    public function getSkuInfo($sku_id){

        $res = model('new_fenxiao_goods_sku')->getInfo([['sku_id', '=', $sku_id]], '*');
        return $this->success($res);
    }

    /**
     * 获取分销sku列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getSkuList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $list = model('new_fenxiao_goods_sku')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

    /**
     * 获取商品详情
     * @param $goods_id
     * @return \multitype
     */
    public function getGoodsDetail($goods_id)
    {
        $info = model('goods')->getInfo([['goods_id', '=', $goods_id]], "*");
        $info[ 'fenxiao_goods' ] = model('new_fenxiao_goods')->getInfo([['goods_id', '=', $goods_id]], "*");
        $info[ 'sku_data' ] = model('goods_sku')->getList([['goods_id', '=', $goods_id]], 'sku_id, sku_name, sku_no, sku_spec_format, price, market_price, cost_price, discount_price, stock, weight, volume,  sku_image, sku_images, sort,max_buy,min_buy,subscription_open_state,corporate_subscription,subscription_status');
        return $this->success($info);
    }

    /**
     * 获取商品详情
     * @param $goods_id
     * @return \multitype
     */
    public function getMemberGoodsDetail($goods_id, $member_id)
    {
        $info = model('goods')->getInfo([['goods_id', '=', $goods_id]], "*");

        $field = 'gs.sku_id, gs.sku_name,nfgs.admin_cost, nfgs.price, nmgs.sale_price,nmgs.member_id';
        $alias = 'gs';
        $join = [
            [
                'new_fenxiao_goods_sku nfgs',
                'nfgs.sku_id = gs.sku_id',
                'inner'
            ],
            [
                'member_goods_sku nmgs',
                'nmgs.sku_id = gs.sku_id',
                'inner'
            ]
        ];

        $info[ 'sku_data' ] = model('goods_sku')->getList([['gs.goods_id', '=', $goods_id], ['nmgs.member_id', '=', $member_id]], $field, 'gs.sku_id asc', $alias, $join);
        return $this->success($info);
    }

    /**
     * @param $goods_id
     * @param $member_id
     * 移除会员分销商品
     */
    public function deleteMemberGoods($goods_id, $member_id){

        $res = model('member_goods')->delete([['goods_id', '=', $goods_id], ['member_id', '=', $member_id]]);
        $res = model('member_goods_sku')->delete([['goods_id', '=', $goods_id], ['member_id', '=', $member_id]]);
        return $this->success($res);
    }
}