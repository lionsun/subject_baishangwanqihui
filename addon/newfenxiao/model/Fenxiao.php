<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newfenxiao\model;

use app\model\BaseModel;


/**
 * 分销
 */
class Fenxiao extends BaseModel
{
	
	public $fenxiao_status_zh = [
		1 => '正常',
		-1 => '冻结',
	];


    /**
     * 会员注册之后
     * @param unknown $member_id
     */
    public function memberRegister($member_id)
    {
        $config = new Config();

        // 分销商基础设置
        $basics_config = $config->getFenxiaoBasicsConfig();
        $basics_config = $basics_config['data']['value'];
    }


    /**
     * 获取会员分页列表
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getMemberPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '')
    {
        $field = 'm.*,nfl.level_name as fenxiao_name';
        $alias = 'm';
        $join = [
            [
                'new_fenxiao_level nfl',
                'nfl.level_id = m.fenxiao_level',
                'left'
            ]
        ];
        $list = model('member')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, '');
        foreach($list['list'] as $key=>$item){
            if($item['source_fenxiao'] > 0) {
                $source_info = model('member')->getInfo([['member_id', '=', $item['source_fenxiao']]], 'username,nickname,headimg,fenxiao_level');

                $source_info['source_level_name'] = '普通会员';
                if ($source_info['fenxiao_level'] > 0) {
                    $source_info['source_level_name'] = model('new_fenxiao_level')->getInfo([['level_id', '=', $source_info['fenxiao_level']]], 'level_name')['level_name'];
                }
                $list['list'][$key]['source_info'] = $source_info;
            }

        }
        return $this->success($list);
    }

    /**
     * 修改会员(注意标签与等级名称)
     * @param $data
     * @param $condition
     * @return array
     */
    public function editMember($data, $condition)
    {
        $res = model('member')->update($data, $condition);
        if ($res === false) {
            return $this->error('', 'RESULT_ERROR');
        }
        return $this->success($res);
    }

    /**
     * @param array $condition
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取分销详情
     */
    public function getFenxiaoDetailInfo($member_id){


        $condition = [
            [ 'm.member_id', '=', $member_id ]
        ];

        $field = 'm.*,nfl.level_name as fenxiao_name';
        $alias = 'm';
        $join = [
            [
                'new_fenxiao_level nfl',
                'nfl.level_id = m.fenxiao_level',
                'left'
            ]
        ];
        $info = model('member')->getInfo($condition, $field, $alias, $join);

        $info['child_source'] = model('member')->getCount([['source_fenxiao', '=', $member_id]]); //团队
        return $this->success($info);;
    }

    /**
     * 获取会员进入的团市场
     */
    public function getMemberTuanzList($member_id){

        $list = model('member_tuanz')->getList([['member_id', '=', $member_id]], '*');
        foreach($list as $key => $fenxiao_member){

            $list[$key]['info'] = model('member')->getInfo([['member_id', '=', $fenxiao_member['fenxiao_member']]], 'username,nickname,mobile,headimg');
        }
        return $this->success($list);;
    }

    /**
     * @param $member_id
     * @param $fenxiao_member
     * 添加会员进过的分销市场
     */
    public function addMemberTuanz($member_id, $fenxiao_member){

        $fenxiao_level = model('member')->getInfo([['member_id', '=', $member_id]], 'fenxiao_level')['fenxiao_level'];
        if(empty($fenxiao_level)){
            return $this->error('当前进入的非团长市场');;
        }

        $info = model('member_tuanz')->getInfo([['member_id', '=', $member_id], ['fenxiao_member', '=', $fenxiao_member]], '*');
        $res = 1;
        if(empty($info)){
            $data = array(
                'member_id' => $member_id,
                'fenxiao_member' => $fenxiao_member,
                'create_time' => time(),
            );

            $res = model('member_tuanz')->add($data);
        }

        return $this->success($res);;
    }

    /**
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * 我的团队
     */
    public function getTearm($member_id, $page = 1, $page_size = PAGE_LIST_ROWS, $order = ''){

        $fenxiao_level = model('member')->getInfo([['member_id', '=', $member_id]], 'fenxiao_level')['fenxiao_level'];
        if(empty($fenxiao_level)){
            return $this->error('当前会员不是团长');
        }

        if($fenxiao_level == 1){

            $field = 'mt.*,m.username,m.nickname,m.mobile,m.headimg,m.member_level_name,m.member_label_name';
            $alias = 'mt';
            $join = [
                [
                    'member m',
                    'm.member_id = mt.member_id',
                    'inner'
                ]
            ];
            $list = model('member_tuanz')->pageList([['fenxiao_member', '=', $member_id]], $field, 'create_time desc', $page, $page_size, $alias, $join);
        }else if($fenxiao_level == 2 || $fenxiao_level == 3){

            $list = model('member')->pageList([['source_fenxiao', '=', $member_id]], '*', 'reg_time desc', $page, $page_size);
        }

        return $this->success($list);
    }

    /**
     * @param $member_id
     * 团队人数
     */
    public function teamNum($member_id)
    {

        $fenxiao_level = model('member')->getInfo([['member_id', '=', $member_id]], 'fenxiao_level')['fenxiao_level'];
        if (empty($fenxiao_level)) {
            return $this->error('当前会员不是团长');;
        }

        $sum = 0;
        if ($fenxiao_level == 1) {

            $sum = model('member_tuanz')->getCount([['fenxiao_member', '=', $member_id]]);
        } else if ($fenxiao_level == 2 || $fenxiao_level == 3) {

            $sum = model('member')->getCount([['source_fenxiao', '=', $member_id]]);
        }

        return $this->success($sum);
    }

    /**
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * 获取佣金池明细
     */
    public function getCommissionPoolRecordList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = ''){

        $list = model('commission_pool_record')->pageList($condition, '*', $order, $page, $page_size);
        return $this->success($list);
    }

    /**
     * 佣金池总金额
     */
    public function getPoolSum(){

        $money = model('commission_pool_record')->getSum([], 'money');
        return $money;
    }
}