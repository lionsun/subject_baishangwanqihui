<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newfenxiao\model;

use app\model\BaseModel;
use app\model\member\MemberAccount;


/**
 * 分销商提现
 */
class FenxiaoWithdraw extends BaseModel
{
	//提现类型
	public $withdraw_type = [
		'balance' => '余额',
		'weixin' => '微信',
		'alipay' => '支付宝',
		'bank' => '银行卡',
	];
	

	
}