<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newfenxiao\model;

use app\model\system\Config as ConfigModel;
use app\model\BaseModel;
use app\model\system\Document;

/**
 * 微信小程序配置
 */
class Config extends BaseModel
{
	/******************************************************************** 分销结算配置 start ****************************************************************************/
	
	/**
	 * 设置分销结算配置
	 * @return multitype:string mixed
	 */
	public function setFenxiaoSettlementConfig($data, $is_use)
	{
		
		$config = new ConfigModel();
		//分销商结算配置
		$settlement_data = [
			'account_type' => $data['account_type'],//佣金计算方式
		];
		
		$config->setConfig($settlement_data, '分销结算配置', $is_use, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'FENXIAO_SETTLEMENT_CONFIG' ] ]);
		//分销商提现配置
		$withdraw_data = [
			'withdraw' => $data['withdraw'],//最低提现额度
			'withdraw_rate' => $data['withdraw_rate'],//佣金提现手续费
			'min_no_fee' => $data['min_no_fee'],//最低免手续费区间
			'max_no_fee' => $data['max_no_fee'],//最高免手续费区间
			'withdraw_status' => $data['withdraw_status'],//提现审核
			'settlement_day' => $data['settlement_day'],//天数
			'withdraw_type' => $data['withdraw_type'],//提现方式
		];
		$res = $config->setConfig($withdraw_data, '分销提现配置', $is_use, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_WITHDRAW_CONFIG' ] ]);
		return $res;
	}
	
	/**
	 * 分销商结算配置
	 * @return multitype:string mixed
	 */
	public function getFenxiaoSettlementConfig()
	{
		$config = new ConfigModel();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_SETTLEMENT_CONFIG' ] ]);
		if (empty($res['data']['value'])) {
			$res['data']['value'] = [
				'account_type' => 0
			];
		}
		return $res;
	}
	
	/**
	 * 分销商提现配置
	 * @return multitype:string mixed
	 */
	public function getFenxiaoWithdrawConfig()
	{
		$config = new ConfigModel();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_WITHDRAW_CONFIG' ] ]);
		if (empty($res['data']['value'])) {
			$res['data']['value'] = [
				'withdraw' => 0,//最低提现额度
				'withdraw_rate' => 0,//佣金提现手续费
				'min_no_fee' => 0,//最低免手续费区间
				'max_no_fee' => 0,//最高免手续费区间
				'withdraw_status' => 1,//提现审核
				'withdraw_type' => 0,//提现方式
			];
		}
		return $res;
	}
	/******************************************************************** 分销结算配置 end ****************************************************************************/
	
	/******************************************************************** 分销文字配置 start ****************************************************************************/
	
	/**
	 * 设置分销文字配置
	 * @return multitype:string mixed
	 */
	public function setFenxiaoWordsConfig($data, $is_use)
	{
		$config = new ConfigModel();
		$res = $config->setConfig($data, '分销文字配置', $is_use, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_WORDS_CONFIG' ] ]);
		return $res;
	}
	
	/**
	 * 获取分销文字配置
	 * @return multitype:string mixed
	 */
	public function getFenxiaoWordsConfig()
	{
		$config = new ConfigModel();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_WORDS_CONFIG' ] ]);
		if (empty($res['data']['value'])) {
			$res['data']['value'] = [
				'concept' => '分销',// 分销概念
				'fenxiao_name' => '分销商',// 分销商名称
				'withdraw' => '提现',// 提现名称
				'account' => '佣金',// 佣金
				'my_team' => '团队',// 我的团队
				'child' => '下线',// 下线
			];
		}
		return $res;
	}
	/******************************************************************** 分销文字配置 end ****************************************************************************/

    /**
     * 设置分销任务配置
     */
    public function setFenxiaoTaskConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '分销任务配置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_TASK_CONFIG' ] ]);
        return $res;
    }

    /**
     * 分销商任务配置
     */
    public function getFenxiaoTaskConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_TASK_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                //新用户消费奖励
                'new_user_consume_reward' => 0,
                //绩效奖励
                'performance_reward' => [
                    //['num' => 1000, 'reward' => 5],
                ],
                //绩效周期
                'performance_cycle_type' => 'month',//month 按月 quarter 按季度 year 按年 diy 自定义
                'performance_cycle_diy_days' => 30,//自定义天数
            ];
        }
        return $res;
    }

    /**
     * 绩效周期类型
     */
    public static function getPerformanceCycleType()
    {
        $arr = [
//            ['name' => 'month', 'title' => '按月'],
//            ['name' => 'quarter', 'title' => '按季度'],
//            ['name' => 'year', 'title' => '按年'],
            ['name' => 'diy', 'title' => '自定义'],
        ];
        return $arr;
    }

    /**
     * 设置分销奖金池配置
     */
    public function setFenxiaoBonusPoolConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '分销任务配置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_BONUS_POOL_CONFIG' ] ]);
        return $res;
    }

    /**
     * 分销商奖金池配置
     */
    public function getFenxiaoBonusPoolConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_BONUS_POOL_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                //输入比率 商品对外成本和设定成本之间的部分为基数
                'input_rate' => 0,
            ];
        }
        return $res;
    }

    /**
     * 获取分销基本设置
     * @return multitype:string mixed
     */
    public function getFenxiaoBasicsConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_BASICS_CONFIG' ] ]);

        if (empty($res['data']['value'])) {

            $res['data']['value'] = [
                'level' => 0,//分销层级
                'internal_buy' => 0,//分销内购
                'is_examine' => 1,//是否需要审核
            ];
        }
        return $res;
    }
}