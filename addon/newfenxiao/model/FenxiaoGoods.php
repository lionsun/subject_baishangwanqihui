<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\model;

use app\model\BaseModel;


/**
 * 分销商品
 */
class FenxiaoGoods extends BaseModel
{


    /**
     * 获取商品分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getGoodsPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'create_time desc',$group = null)
    {

        $field = 'g.*,nfg.fenxiao_goods_id,nfg.state as fenxiao_state, nfg.goods_state as fenxiao_goods_state';
        $alias = 'g';
        $join = [
            [
                'new_fenxiao_goods nfg',
                'nfg.goods_id = g.goods_id',
                'left'
            ]
        ];
        $list = model('goods')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, $group);
        return $this->success($list);
    }

    /**
     * @param $data
     * @param $condition
     * @return array
     * @throws \think\db\exception\DbException
     * 修改分销商品
     */
    public function modifyFenxiaoGoods($data, $condition){

        $res = model('new_fenxiao_goods')->update($data, $condition);
        return $this->success($res);
    }

    /**
     * @param $condition
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 查询分销商品详情
     */
    public function getFenxiaoGoods($condition){

        $info = model('new_fenxiao_goods')->getInfo($condition, '*');
        return $this->success($info);
    }

    /**
     * @param $data
     * 添加分销商品
     */
    public function addFenxiaoGoods($data){

        $res = model('new_fenxiao_goods')->add($data);
        return $this->success($res);
    }

    /**
     * @return array
     */
    public function editGoodsFenxiao($data, $condition)
    {
        $re = model('new_fenxiao_goods')->update($data, $condition);
        return $this->success($re);
    }


    /**
     * 获取分销sku列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getSkuList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $list = model('new_fenxiao_goods_sku')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

    /**
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param null $group
     * 接口请求的分销商品列表
     */
    public function getApiFenxiaoGoodsPage($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'g.create_time desc',$group = null){

        $condition[] = ['nfg.state', '=', 1]; //参与分销
        $condition[] = ['nfg.goods_state', '=', 1]; //已经上架
        $field = 'g.*,nfg.fenxiao_goods_id,nfg.state as fenxiao_state,  nfg.price as sale_price';
        $alias = 'nfg';
        $join = [
            [
                'goods g',
                'g.goods_id = nfg.goods_id ',
                'inner'
            ],
//            [
//                 'member_goods nmg',
//                'nmg.goods_id = nfg.goods_id',
//                'left'
//            ]
        ];
        $list = model('new_fenxiao_goods')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, $group);
        return $this->success($list);
    }

    /**
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param null $group
     * 接口请求的分销商品列表
     */
    public function getApiMemberFenxiaoGoodsPage($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'g.create_time desc',$group = null){

        $condition[] = ['nfg.state', '=', 1]; //参与分销
        $condition[] = ['nfg.goods_state', '=', 1]; //已经上架
        $field = 'g.*,nfg.fenxiao_goods_id,nfg.state as fenxiao_state, nmg.member_id, nmg.price as sale_price';
        $alias = 'g';
        $join = [
            [
                'new_fenxiao_goods nfg',
                'nfg.goods_id = g.goods_id',
                'inner'
            ],
            [
                'member_goods nmg',
                'nmg.goods_id = g.goods_id',
                'inner'
            ]
        ];
        $list = model('goods')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, $group);
        return $this->success($list);
    }
    /**
     * @param $goods_id
     * @param $member_id
     * 添加会员商品
     */
    public function addMemberGoods($goods_id, $member_id){

        $fenxiao_goods = model('new_fenxiao_goods')->getInfo([['goods_id', '=', $goods_id],['goods_state', '=', 1]],'*');
        if(empty($fenxiao_goods)){
            return $this->error('该商品未参加分销！');
        }

        $member_goods = model('member_goods')->getInfo([['goods_id', '=', $goods_id],['member_id', '=', $member_id]],'*');
        if(!empty($member_goods)){
            return $this->error('该商品已经加入您的分销市场！');
        }

        $price = 0;

        $sku_list = model('new_fenxiao_goods_sku')->getList([['goods_id', '=', $goods_id]], '*');
        $sku_data = array();
        foreach($sku_list as $key=>$sku){
            if(empty($price)){
                $price = $sku['price'];
            }else{
                $price = $sku['price']> $price ? $price : $sku['price'];
            }

            $sku_data[] = array(
                'goods_id' => $goods_id,
                'sku_id' => $sku['sku_id'],
                'member_id' => $member_id,
                'sale_price' => $sku['price'],
                'create_time' => time()
            );
        }
        model('member_goods_sku')->addList($sku_data);

        $data = array(
            'goods_id' => $goods_id,
            'member_id' => $member_id,
            'price' => $price,
            'create_time' => time()
        );
        $res = model('member_goods')->add($data);
        return $this->success($res);
    }

    /**
     * @param $sku_data
     * @param $goods_id
     * @param $member_id
     * 分销价更改
     */
    public function updateMemberGoods($sku_data, $goods_id, $member_id){

        $price = 0.00;
        $error = '';
        foreach ($sku_data as $item){

            $jianyi_price = model('new_fenxiao_goods_sku')->getInfo([['sku_id', '=', $item['sku_id']]], 'admin_cost')['admin_cost'];
            if($jianyi_price > $item['price']){
                $error = '不可低于成本价！';
                break;
            }
            if(empty($price)){
                $price = $item['price'];
            }else{
                $price = $item['price']> $price ? $price : $item['price'];
            }

            $condition = array(
                ['goods_id', '=', $goods_id],
                ['member_id', '=', $member_id],
                ['sku_id', '=', $item['sku_id']]
            );
            $data = array(
                'sale_price' => $item['price']
            );

            model('member_goods_sku')->update($data, $condition);
        }

        if(!empty($error)){
            return $this->error($error);
        }

        $res = model('member_goods')->update(['price' => $price], [['goods_id', '=', $goods_id], ['member_id', '=', $member_id]]);
        return $this->success($res);
    }

    /**
     * @param $goods_id
     * 会员商品详情
     */
    public function getMemberGoods($goods_id){

        $res = model('member_goods')->getInfo([['goods_id', '=', $goods_id]], '*');
        return $this->success($res);;
    }

    /**
     * @param $sku_id
     * 获取会员商品项详情
     */
    public function getMemberGoodsSku($sku_id, $fenxiao_member){

        $info = model('member_goods_sku')->getInfo([['sku_id', '=', $sku_id], ['member_id', '=', $fenxiao_member]], '*');
        return $this->success($info);;
    }

}