<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newfenxiao\model;

use app\model\BaseModel;
use app\model\member\MemberAccount;
use app\model\system\Config as ConfigModel;

/**
 * 分销商品
 */
class FenxiaoOrder extends BaseModel
{

    /**
     * @param $order
     * 团长订单计算
     */
    public function calculate($order){

        $order_info = model('order')->getInfo([['order_id', '=', $order['order_id']]], '*');
        if(empty($order_info['fenxiao_member']))return $this->error('非团长订单！');

        $order_goods_list = model('order_goods')->getList([['order_id', '=', $order['order_id']]], '*');


        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'NEW_FENXIAO_BONUS_POOL_CONFIG' ] ]);
        $level_2 = model('new_fenxiao_level')->getInfo([['level_id','=', 2]], '*');
        $level_3 = model('new_fenxiao_level')->getInfo([['level_id','=', 3]], '*');
        foreach ($order_goods_list as $order_goods){

            //团长费用
            $goods_sku = model('new_fenxiao_goods_sku')->getInfo([['sku_id', '=', $order_goods['sku_id']]], '*');
            $commission1 = $order_goods['goods_money'] - $goods_sku['admin_cost'] * $order_goods['num'];

            $money = $goods_sku['admin_cost'] - $goods_sku['shop_cost'];
            //站长费用
            $commission2 = $money * $level_2['rate'] / 100;

            //总站长
            $commission3 = $money * $level_3['rate'] / 100;

            //奖金池
            $commission4 = $money * $res['data']['value']['input_rate'] / 100;


            $member_id2 = model('member')->getInfo([['member_id', '=', $order_info['fenxiao_member']]], 'source_fenxiao')['source_fenxiao'];//站长获取
            $member_id3 = 0;
            if(!empty($member_id2)){
                $member_id3 = model('member')->getInfo([['member_id', '=', $member_id2]], 'source_fenxiao')['source_fenxiao']; //总站长获取
            }

            $data = array(
                'order_id' => $order['order_id'],
                'order_goods_id' => $order_goods['order_goods_id'],
                'member_id' => $order_info['member_id'],
                'member_id1' => $order_info['fenxiao_member'],
                'member_id2' => empty($member_id2) ? 0 : $member_id2,
                'member_id3' => empty($member_id3) ? 0 : $member_id3,
                'sale_price' => $order_goods['price'],
                'admin_price' => $goods_sku['admin_cost'],
                'shop_price' => $goods_sku['shop_cost'],
                'commission_1' => $commission1,
                'commission_2' => $commission2,
                'commission_3' => $commission3,
                'commission_4' => $commission4,
                'admin_profit' => $goods_sku['admin_cost'] - $commission2 - $commission3 - $commission4 - $goods_sku['shop_cost'],
                'state' => 0,
                'create_time' => time(),
                'state_time' => time(),
                'goods_money' => $order_goods['goods_money'],
                'num' => $order_goods['num'],
            );
            $res = model('member_order')->add($data);
        }

        //拉新奖励
        $this->laxinReward($order['order_id']);

        //业绩奖励开启
        $this->addMemberTask($order_info['fenxiao_member']);
    }

    /**
     * 订单退款
     * @param $order_goods_id
     * @return array
     */
    public function refund($order_goods_id)
    {
        $res = model("member_order")->update(['state' => -1, 'state_time' => time()], [['order_goods_id', '=', $order_goods_id]]);
        return $this->success($res);
    }

    /**
     * @param $order_id
     * 发放佣金
     */
    public function settlement($order_id){

        $info = model('member_order')->getInfo([['order_id', '=', $order_id]], '*');

        //团长佣金
        $member_account = new MemberAccount();

        $member_account->addMemberAccount($info['member_id1'], 'balance_money', $info['commission_1'], 'commission', $info['id'], '顾客下单佣金发放');
        //站长佣金
        if(!empty($info['commission_2'])) {
            $member_account->addMemberAccount($info['member_id2'], 'balance_money', $info['commission_2'], 'commission', $info['id'], '顾客下单佣金发放');
        }
        //总站长佣金
        if(!empty($info['commission_2'])) {
            $member_account->addMemberAccount($info['member_id3'], 'balance_money', $info['commission_3'], 'commission', $info['id'], '顾客下单佣金发放');
        }
        //佣金池发放
        $data = array(
            'relation_id' => $info['id'],
            'relation_type' => 1,
            'money' => $info['commission_4'],
            'create_time' => time(),
            'remarks' => '顾客下单'
        );
        model('commission_pool_record')->add($data);

        model('member_order')->update(['state' => 1],[['order_id', '=', $order_id]]);
    }

    /**
     * 团订单列表
     */
    public function getMemberOrder($member_id, $state, $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'o.create_time desc'){

        $fenxiao_level = model('member')->getInfo([['member_id', '=', $member_id]], 'fenxiao_level')['fenxiao_level'];
        if(empty($fenxiao_level)){
            return $this->error('当前会员不是团长');;
        }
        $condition = [];
        $condition[] = array('state', '=', $state);
        $field = '*';
        if($fenxiao_level == 1){
            $condition[] = array('member_id1', '=', $member_id);
            $field = 'mo.id, mo.order_id, mo.order_goods_id, mo.member_id,member_id1 as commssion_member,mo.sale_price,mo.admin_price,mo.shop_price,mo.commission_1 as commission,mo.state';
        }elseif($fenxiao_level == 2){
            $condition[] = array('member_id2', '=', $member_id);
            $field = 'mo.id, mo.order_id, mo.order_goods_id, mo.member_id,member_id2 as commssion_member,mo.sale_price,mo.admin_price,mo.shop_price,mo.commission_2 as commission,state';
        }elseif($fenxiao_level == 3){
            $condition[] = array('member_id3', '=', $member_id);
            $field = 'mo.id, mo.order_id, mo.order_goods_id, mo.member_id,mo.member_id3 as commssion_member,mo.sale_price,mo.admin_price,mo.shop_price,mo.commission_3 as commission,state';
        }

        $field .=' ,og.order_no, og.site_name, o.order_status, o.order_status_name, o.member_id,o.create_time
         ,og.goods_id, og.sku_id, og.sku_name, og.sku_image, og.price, og.cost_price,og.num, og.goods_money as real_goods_money';
        $alias = 'mo';
        $join = [
            [
                'order o',
                'mo.order_id = o.order_id',
                'inner'
            ],
            [
                'order_goods og',
                'og.order_goods_id = mo.order_goods_id',
                'inner'
            ]
        ];

        $list = model('member_order')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return $this->success($list);
    }

    /**
     * @param $order_id
     * 查询是否满足拉新条件
     */
    public function laxinReward($order_id){

        $order_info = model('order')->getInfo([['order_id', '=', $order_id]], 'order_id, fenxiao_member,member_id');
        $member_info = model('member')->getInfo([['member_id', '=', $order_info['member_id']]], 'source_member');
        if($member_info['source_member'] == $order_info['fenxiao_member']){
            dump(123);

            $count = model('order')->getCount([['member_id', '=', $order_info['member_id']], ['fenxiao_member', '=', $order_info['fenxiao_member']], ['pay_status', '=', 1]]);
            if($count == 1){

                //拉新奖励
                $config = new Config();
                $config_info = $config->getFenxiaoTaskConfig();
                $reward =$config_info['data']['value']['new_user_consume_reward'];
                if($reward > 0){

                    $member_account = new MemberAccount();
                    $member_account->addMemberAccount($order_info['fenxiao_member'], 'balance_money', $reward, 'commission1', $order_info['member_id'], '拉新下单奖励');

                    //从佣金池扣除
                    $data = array(
                        'relation_id' => $order_id,
                        'relation_type' => 2,
                        'money' => $reward *-1,
                        'create_time' => time(),
                        'remarks' => '拉新奖励扣除'
                    );
                    model('commission_pool_record')->add($data);
                }
            }
        }
    }

    /*
     * @param $member_id
     * 添加任务时间
     */
    public function addMemberTask($member_id){

        $info = model('member_task')->getCount([['member_id', '=', $member_id], ['state', '=', 0]]);
        if(!empty($info)){
            return;
        }

        $config = new Config();
        $config_info = $config->getFenxiaoTaskConfig();
        $day =$config_info['data']['value']['performance_cycle_diy_days'];
        $start_time = time();
        $end_time = $start_time + $day*60*60*24;
        $data = array(
            'member_id' => $member_id,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'create_time' => time(),
            'state' => 0,
        );

        $res = model('member_task')->add($data);
        return $this->success($res);
    }

    /**
     * 业绩发放
     */
    public function memberTaskIssue(){


        $list = model('member_task')->getList([['state', '=', 0], ['end_time', '<=', time()]], '*');
        if(!empty($list)) {

            $config = new Config();
            $config_info = $config->getFenxiaoTaskConfig();
            $performance_reward = $config_info['data']['value']['performance_reward'];
            if(empty($performance_reward))return;

            foreach ($list as $key => $item) {

                $yeji = model('member_order')->getSum([['member_id1', '=', $item['member_id']], ['state', '=', 1], ['state_time', 'between', array($item['start_time'], $item['end_time'])]], 'goods_money');
                $reward = ['num' =>0, 'reward' => 0];
                foreach ($performance_reward as $where_money){

                    if($where_money['num'] < $yeji){
                        $reward = $where_money;
                    }
                }

                //更改原有的任务信息
                model('member_task')->update(['task_money' => $reward['num'],
                    'commission' => $reward['reward'],
                    'state' => 1,
                    'current_money' => $yeji
                ],[['id', '=', $item['id']]]);

                if($reward['reward'] > 0) {
                    //发放佣金
                    $member_account = new MemberAccount();
                    $member_account->addMemberAccount($item['member_id'], 'balance_money', $reward['reward'], 'commission2', $item['id'], '达标业绩' . $reward['num'] . '奖励');

                    //从佣金池扣除
                    $data = array(
                        'relation_id' => $item['id'],
                        'relation_type' => 3,
                        'money' => $reward['reward'] * -1,
                        'create_time' => time(),
                        'remarks' => '团长业绩达标奖励'
                    );
                    model('commission_pool_record')->add($data);
                }

                //添加新的任务
                $this->addMemberTask($item['member_id']);
            }
        }
    }

    /**
     * @param array $condition
     * @param int $page
     * @param $page_size
     * 分销订单列表
     */
    public function getFenxiaoOrderPageList($condition =[], $page = 1, $page_size){

        $field = 'mo.order_id, mo.order_goods_id, mo.member_id1, mo.member_id2, mo.member_id3, mo.sale_price, mo.admin_price, mo.shop_price, mo.commission_1, mo.commission_2, mo.commission_3, mo.commission_4, mo.admin_profit, mo.state
         ,og.order_no, og.site_name, o.order_status, o.order_status_name, o.member_id,o.create_time,o.site_id
         ,og.goods_id, og.sku_id, og.sku_name, og.sku_image, og.price, og.cost_price,og.num, og.goods_money';
        $alias = 'mo';
        $join = [
            [
                'order o',
                'mo.order_id = o.order_id',
                'inner'
            ],
            [
                'order_goods og',
                'og.order_goods_id = mo.order_goods_id',
                'inner'
            ]
        ];
        $list = model('member_order')->pageList($condition, $field,'o.create_time desc',$page, $page_size, $alias, $join);

        foreach ($list['list'] as $key=>$order_goods) {
            //团长信息
            $list['list'][$key]['member_info1'] = model('member')->getInfo([['member_id', '=', $order_goods['member_id1']]], 'member_id,username,nickname,mobile,email,headimg');
            //站长
            $list['list'][$key]['member_info2'] = model('member')->getInfo([['member_id', '=', $order_goods['member_id2']]], 'member_id,username,nickname,mobile,email,headimg');
            //总站长
            $list['list'][$key]['member_info3'] = model('member')->getInfo([['member_id', '=', $order_goods['member_id3']]], 'member_id,username,nickname,mobile,email,headimg');
        }

        return $this->success($list);

    }
}