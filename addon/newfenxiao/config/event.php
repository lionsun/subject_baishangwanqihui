<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //展示活动
        'ShowPromotion' => [
            'addon\newfenxiao\event\ShowPromotion',
        ],
        'OrderComplete' => [
            'addon\newfenxiao\event\OrderSettlement',
        ],
        'OrderRefundFinish' => [
            'addon\newfenxiao\event\OrderGoodsRefund',
        ],
        'OrderPay' => [
            'addon\newfenxiao\event\OrderPay',
        ],

        'MemberAccountFromType' => [
            'addon\newfenxiao\event\MemberAccountFromType',
        ],
    		
    	'MemberRegister' => [
    		'addon\newfenxiao\event\MemberRegister',
    	],

        // 商品列表
        'GoodsListPromotion' => [
            'addon\newfenxiao\event\GoodsListPromotion',
        ],
    ],

    'subscribe' => [
    ],
];
