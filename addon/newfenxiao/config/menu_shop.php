<?php
// +----------------------------------------------------------------------
// | 店铺端菜单设置
// +----------------------------------------------------------------------
return [
	[

		'name' => 'PROMOTION_NEW_FENXIAO_SHOP',
		'title' => '分销管理',
		'url' => 'newfenxiao://shop/fenxiao/lists',
		'parent' => 'TOOL_ROOT',
		'picture' => 'addon/newfenxiao/shop/view/public/img/distribution.png',
		'is_show' => 1,
		'sort' => 1,
		'child_list' => [
			[
				'name' => 'PROMOTION_NEW_FENXIAO_LIST',
				'title' => '分销商品',
				'url' => 'newfenxiao://shop/fenxiao/lists',
				'is_show' => 1,
				'child_list' => [
					[
                    'name' => 'PROMOTION_NEW_FENXIAO_LIST_SHOP',
                    'title' => '分销商品',
                    'url' => 'newfenxiao://shop/fenxiao/lists',
                    'is_show' => 1,
                    'child_list' => [
                        [
                            'name' => 'PROMOTION_NEW_FENXIAO_CONFIG_SHOP',
                            'title' => '商品设置',
                            'url' => 'newfenxiao://shop/fenxiao/config',
                            'sort' => 1,
                            'is_show' => 0
                        ],
                        [

                            'name' => 'PROMOTION_NEW_FENXIAO_MODIFY',
                            'title' => '状态设置',
                            'url' => 'fenxiao://shop/fenxiao/modify',
                            'sort' => 1,
                            'is_show' => 0
                        ],
				    ]
			        ],
                ],
            ],
            [
				'name' => 'PROMOTION_NEW_FENXIAO_ORDER_SHOP',
				'title' => '分销订单',
				'url' => 'newfenxiao://shop/order/lists',
				'is_show' => 1,
				'child_list' => [
					[
						'name' => 'PROMOTION_NEW_FENXIAO_ORDER_DETAIL_SHOP',
						'title' => '订单详情',
						'url' => 'newfenxiao://shop/order/detail',
						'sort' => 1,
						'is_show' => 0
					],
				]
			],
		]
	],
];