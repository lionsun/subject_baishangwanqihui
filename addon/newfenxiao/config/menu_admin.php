<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
	[
		'name' => 'PROMOTION_NEW_FENXIAO_ROOT',
		'title' => '管理中心',
		'url' => 'newfenxiao://admin/fenxiao/lists',
		'parent' => 'TOOL_ROOT',
		'is_show' => 1,
		'is_control' => 1,
		'is_icon' => 0,
		'sort' => 1,
		'child_list' => [
			[
				'name' => 'PROMOTION_NEW_FENXIAO',
				'title' => '分销商',
				'url' => 'newfenxiao://admin/fenxiao/lists',
				'is_show' => 1,
				'is_control' => 1,
				'is_icon' => 0,
				'sort' => 1,
				'child_list' => [
					[
						'name' => 'PROMOTION_NEW_FENXIAO_LISTS',
						'title' => '分销商',
						'url' => 'newfenxiao://admin/fenxiao/lists',
						'is_show' => 1,
						'child_list' => [
							[
								'name' => 'PROMOTION_NEW_FENXIAO_DETAIL',
								'title' => '分销商信息',
								'url' => 'newfenxiao://admin/fenxiao/detail',
								'is_show' => 1,
							],
                            [
                                'name' => 'PROMOTION_NEW_FENXIAO_TEAM',
                                'title' => '分销商团队',
                                'url' => 'newfenxiao://admin/fenxiao/team',
                                'is_show' => 1,
                            ],
							[
								'name' => 'PROMOTION_NEW_FENXIAO_ACCOUNT',
								'title' => '账户明细',
								'url' => 'newfenxiao://admin/fenxiao/account',
								'is_show' => 1,
							],
							[
								'name' => 'PROMOTION_NEW_FENXIAO_ORDERMANAGE',
								'title' => '订单管理',
								'url' => 'newfenxiao://admin/fenxiao/order',
								'is_show' => 1,
							],
							[
								'name' => 'PROMOTION_NEW_FENXIAO_ORDERMANAGEDETAIL',
								'title' => '订单详情',
								'url' => 'newfenxiao://admin/fenxiao/orderdetail',
								'is_show' => 0,
							],
						
						]
					],
				],
			
			],
			[
				'name' => 'PROMOTION_NEW_FENXIAO_GOODS',
				'title' => '分销商品',
				'url' => 'newfenxiao://admin/goods/lists',
				'is_show' => 1,
				'is_control' => 1,
				'is_icon' => 0,
				'sort' => 2,
				'child_list' => [
					[
						'name' => 'PROMOTION_NEW_FENXIAO_GOODS_DETAIL',
						'title' => '商品详情',
						'url' => 'newfenxiao://admin/goods/detail',
						'is_show' => 0,
					],
				
				]
			],
			[
				'name' => 'PROMOTION_NEW_FENXIAO_ORDER',
				'title' => '分销订单',
				'url' => 'newfenxiao://admin/order/lists',
				'is_show' => 1,
				'is_control' => 1,
				'is_icon' => 0,
				'sort' => 3,
				'child_list' => [
					[
						'name' => 'PROMOTION_NEW_FENXIAO_ORDER_DETAIL',
						'title' => '订单详情',
						'url' => 'newfenxiao://admin/order/detail',
						'is_show' => 0,
					],
				
				]
			],

			[
				'name' => 'PROMOTION_NEW_FENXIAO_LEVEL',
				'title' => '分销等级',
				'url' => 'newfenxiao://admin/level/lists',
				'is_show' => 1,
				'is_control' => 1,
				'is_icon' => 0,
				'sort' => 5,
				'child_list' => [
					[
						'name' => 'PROMOTION_NEW_FENXIAO_LEVEL_LISTS',
						'title' => '等级列表',
						'url' => 'newfenxiao://admin/level/lists',
						'is_show' => 0,
					],
					[
						'name' => 'PROMOTION_NEW_FENXIAO_LEVEL_ADD',
						'title' => '添加等级',
						'url' => 'newfenxiao://admin/level/add',
						'is_show' => 0,
					],
					[
						'name' => 'PROMOTION_NEW_FENXIAO_LEVEL_EDIT',
						'title' => '编辑等级',
						'url' => 'newfenxiao://admin/level/edit',
						'is_show' => 0,
					],
					[
						'name' => 'PROMOTION_NEW_FENXIAO_LEVEL_STATUS',
						'title' => '等级状态设置',
						'url' => 'newfenxiao://admin/level/status',
						'is_show' => 0,
					],
					[
						'name' => 'PROMOTION_NEW_FENXIAO_LEVEL_DELETE',
						'title' => '删除等级',
						'url' => 'newfenxiao://admin/level/delete',
						'is_show' => 0,
					]
				]
			],
			[
				'name' => 'PROMOTION_NEW_FENXIAO_CONFIG',
				'title' => '分销设置',
				'url' => 'newfenxiao://admin/config/task',
				'is_show' => 1,
				'is_control' => 1,
				'is_icon' => 0,
				'sort' => 6,
				'child_list' => [

                    [
                        'name' => 'PROMOTION_NEW_FENXIAO_TASK',
                        'title' => '任务设置',
                        'url' => 'newfenxiao://admin/config/task',
                        'is_show' => 1,
                        'sort' => 1,
                    ],
                    [
                        'name' => 'PROMOTION_NEW_FENXIAO_BONUS_POOL',
                        'title' => '奖金池设置',
                        'url' => 'newfenxiao://admin/config/bonuspool',
                        'is_show' => 1,
                        'sort' => 2,
                    ]
				]
			],
            [
                'name' => 'PROMOTION_NEW_FENXIAO_POOL',
                'title' => '奖金池',
                'url' => 'newfenxiao://admin/fenxiao/bonuspoolList',
                'is_show' => 1,
                'is_control' => 1,
                'is_icon' => 0,
                'sort' => 6,
            ]
		]
	],

];