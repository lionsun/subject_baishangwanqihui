<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\admin\controller;

use addon\newfenxiao\model\FenxiaoAccount;
use addon\newfenxiao\model\FenxiaoApply;
use addon\newfenxiao\model\FenxiaoGoods;
use addon\newfenxiao\model\FenxiaoGoodsSku;
use app\model\member\Member as MemberModel;
use addon\newfenxiao\model\FenxiaoData;
use addon\newfenxiao\model\FenxiaoLevel;
use addon\newfenxiao\model\FenxiaoLevel as FenxiaoLevelModel;
use addon\newfenxiao\model\FenxiaoOrder;
use app\admin\controller\BaseAdmin;
use addon\newfenxiao\model\Fenxiao as FenxiaoModel;
use app\model\system\Menu as MenuModel;



/**
 *  分销设置
 */
class Fenxiao extends BaseAdmin
{
	/**
	 * 分销概况
	 */
	public function index()
	{
		//获取分销商账户统计
		$fenxiao_data_model = new FenxiaoData();
		$account_data = $fenxiao_data_model->getFenxiaoAccountData();
		$this->assign('account_data', $account_data);
		//累计佣金
		$fenxiao_account = number_format($account_data['account'] + $account_data['account_withdraw'], 2, '.', '');
		$this->assign('fenxiao_account', $fenxiao_account);
		//获取申请人数
		$fenxiao_apply_num = $fenxiao_data_model->getFenxiaoApplyCount();
		$this->assign('fenxiao_apply_num', $fenxiao_apply_num);
		//分销商人数
		$fenxiao_num = $fenxiao_data_model->getFenxiaoCount();
		$this->assign('fenxiao_num', $fenxiao_num);
		return $this->fetch('fenxiao/index');
	}
	
	/**
	 * 分销商列表
	 */
	public function lists()
	{
		$model = new FenxiaoModel();
		if (request()->isAjax()) {

            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'username');//可以传username mobile email

            $condition = [];
            //下拉选择
            $condition[] = [ $search_text_type, 'like', "%" . $search_text . "%" ];
            $order = 'reg_time desc';
            $field = 'member_id, source_member, username, mobile, email, status, headimg, member_level, member_level_name, member_label, member_label_name, qq, qq_openid, wx_openid, wx_unionid, ali_openid, baidu_openid, toutiao_openid, douyin_openid, login_ip, login_type, login_time, last_login_ip, last_login_type, last_login_time, login_num, nickname, realname, sex, location, birthday, reg_time, point, balance, balance_money, growth, account5,member_type,invite_code,invite_id,check_status,code,business_id,invite_id,fenxiao_level';

            $member_model = new \addon\newfenxiao\model\Fenxiao();
            $list = $member_model->getMemberPageList($condition, $page, $page_size, $order, $field);

            return $list;
        } else {

			$level_model = new FenxiaoLevel();
			$level_list = $level_model->getLevelList([], 'level_id,level_name');
			$this->assign('level_list', $level_list['data']);



			return $this->fetch('fenxiao/lists');
		}
	}

    /**
     * 修改分享商等级
     * @return array
     */
    public function updateFenxiaoLevel()
    {
        if(request()->isAjax()){
            $member_id = input('member_id');
            $fenxiao_level = input('fenxiao_level');
            $member_model = new \addon\newfenxiao\model\Fenxiao();

            $condition = [['member_id','=',$member_id]];

            $res = $member_model->editMember(['fenxiao_level' => $fenxiao_level],$condition);
            return $res;
        }
    }

    /**
     * 设置上级
     * @return array
     */
    public function updateSourceFenxiao()
    {
        if(request()->isAjax()){
            $source_fenxiao = input('source_fenxiao', 0);
            $member_id = input('member_id', 0);

            $member_model = new \addon\newfenxiao\model\Fenxiao();
            $condition = [['member_id','=',$member_id]];

            $res = $member_model->editMember(['source_fenxiao' => $source_fenxiao],$condition);
            return $res;
        }
    }

    /**
     * 修改分销商品
     */
    public function updateFenxiaoGoods(){

        if(request()->isAjax()){

            $fenxiao_skus = input('fenxiao', []);
            $goods_id = input('goods_id');
            $goods_state = input('goods_state', 0);

            $price = 0.00;
            foreach ($fenxiao_skus['sku_id'] as $key => $sku_id) {

                $fenxiao_sku = [
                    'admin_cost' => $fenxiao_skus['admin_cost'][$key],
                    'price' => $fenxiao_skus['price'][$key],
                ];

                $fenxiao_goods_sku = new FenxiaoGoodsSku();
                $fenxiao_goods_sku->editSkuList($fenxiao_sku, [['sku_id', '=', $sku_id]]);

                $price = empty($price) ? $fenxiao_skus['price'][$key] : $price;
                if($price > $fenxiao_skus['price'][$key]){
                    $price = $fenxiao_skus['price'][$key];
                }
            }

            $fenxiao_goods = new FenxiaoGoods();
            $res = $fenxiao_goods->editGoodsFenxiao(['goods_state' => $goods_state, 'price' => $price], [['goods_id', '=', $goods_id]]);
            return  $res;
        }
    }


    /**
	 * 详情
	 */
	public function detail()
	{
		$fenxiao_id = input('fenxiao_id', '');
		$model = new FenxiaoModel();
		$fenxiao_leve_model = new FenxiaoLevelModel();
		$condition[] = [ 'f.fenxiao_id', '=', $fenxiao_id ];
		$info = $model->getFenxiaoDetailInfo($condition);
		$fenxiao_level = $fenxiao_leve_model->getLevelInfo([ [ 'level_id', '=', $info['data']['level_id'] ] ]);
		$this->assign('status', $model->fenxiao_status_zh);
		$this->assign('level', $fenxiao_level['data']);
		$this->assign('info', $info['data']);
		
		$this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
		return $this->fetch('fenxiao/fenxiao_detail');
	}
	
	/**
	 * 分销账户信息
	 */
	public function account()
	{
		$model = new FenxiaoModel();
		$fenxiao_id = input('fenxiao_id', '');
		
		$condition[] = [ 'f.fenxiao_id', '=', $fenxiao_id ];
		$info = $model->getFenxiaoDetailInfo($condition);
		$account = $info['data']['account'] - $info['data']['account_withdraw_apply'];
		$info['data']['account'] = number_format($account, 2, '.', '');
		$this->assign('fenxiao_info', $info['data']);
		
		if (request()->isAjax()) {
			
			$account_model = new FenxiaoAccount();
			$page = input('page', 1);
			$status = input('status', '');
			
			$fenxiao_id = input('fenxiao_id', '');
			$list_condition[] = [ 'fenxiao_id', '=', $fenxiao_id ];
			if ($status) {
				if ($status == 1) {
					$list_condition[] = [ 'money', '>', 0 ];
				} else {
					$list_condition[] = [ 'money', '<', 0 ];
				}
			}
			
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && $end_time) {
				$list_condition[] = [ 'create_time', 'between', [ $start_time, $end_time ] ];
			} elseif (!$start_time && $end_time) {
				$list_condition[] = [ 'create_time', '<=', $end_time ];
				
			} elseif ($start_time && !$end_time) {
				$list_condition[] = [ 'create_time', '>=', $start_time ];
			}
			
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $account_model->getFenxiaoAccountPageList($list_condition, $page, $page_size);
			return $list;
		}
		$this->assign('fenxiao_id', $fenxiao_id);
		
		$this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
		return $this->fetch('fenxiao/fenxiao_account');
	}
	
	/**
	 * 订单管理
	 */
	public function order()
	{
		$model = new FenxiaoOrder();
		
		$fenxiao_id = input('fenxiao_id', '');
		if (request()->isAjax()) {
			
			$page_index = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$fenxiao_id = input('fenxiao_id', '');
			
			$condition[] = [ 'one_fenxiao_id|two_fenxiao_id|three_fenxiao_id', '=', $fenxiao_id ];
			
			$search_text_type = input('search_text_type', "goods_name");//订单编号/店铺名称/商品名称
			$search_text = input('search_text', "");
			if (!empty($search_text)) {
				$condition[] = [ 'fo.' . $search_text_type, 'like', '%' . $search_text . '%' ];
			}
			
			//下单时间
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if (!empty($start_time) && empty($end_time)) {
				$condition[] = [ 'o.create_time', '>=', date_to_time($start_time) ];
			} elseif (empty($start_time) && !empty($end_time)) {
				$condition[] = [ 'o.create_time', '<=', date_to_time($end_time) ];
			} elseif (!empty($start_time) && !empty(date_to_time($end_time))) {
				$condition[] = [ 'o.create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
			}
			
			$list = $model->getFenxiaoOrderPageList($condition, $page_index, $page_size);
			return $list;
			
		} else {
			//订单状态
			$this->assign('fenxiao_id', $fenxiao_id);
			$this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
			return $this->fetch('fenxiao/order_lists');
		}
	}
	
	/**
	 * 订单详情
	 */
	public function orderDetail()
	{
		$fenxiao_order_model = new FenxiaoOrder();
		$fenxiao_order_id = input('fenxiao_order_id', '');
		$order_info = $fenxiao_order_model->getFenxiaoOrderDetail([ [ 'fenxiao_order_id', '=', $fenxiao_order_id ] ]);
		$this->assign('order_info', $order_info['data']);
		return $this->fetch('fenxiao/order_detail');
	}
	
	/**
	 * 冻结
	 */
	public function frozen()
	{
		$fenxiao_id = input('fenxiao_id', '');
		
		$model = new FenxiaoModel();
		
		return $model->frozen($fenxiao_id);
	}
	
	/**
	 * 恢复正常
	 */
	public function unfrozen()
	{
		$fenxiao_id = input('fenxiao_id', '');
		
		$model = new FenxiaoModel();
		
		return $model->unfrozen($fenxiao_id);
	}
	
	
	/**
	 * 分销商申请列表
	 */
	public function apply()
	{
		$model = new FenxiaoApply();
		if (request()->isAjax()) {
			
			$condition[] = [ 'status', '=', 1 ];
			
			$fenxiao_name = input('fenxiao_name', '');
			if ($fenxiao_name) {
				$condition[] = [ 'fenxiao_name', 'like', '%' . $fenxiao_name . '%' ];
			}
			$mobile = input('mobile', '');
			if ($mobile) {
				$condition[] = [ 'mobile', 'like', '%' . $mobile . '%' ];
			}
			$level_id = input('level_id', '');
			if ($level_id) {
				$condition[] = [ 'level_id', '=', $level_id ];
			}
			$create_start_time = input('create_start_time', '');
			$create_end_time = input('create_end_time', '');
			if ($create_start_time && $create_end_time) {
				$condition[] = [ 'create_time', 'between', [ strtotime($create_start_time), strtotime($create_end_time) ] ];
			} elseif (!$create_start_time && $create_end_time) {
				$condition[] = [ 'create_time', '<=', strtotime($create_end_time) ];
				
			} elseif ($create_start_time && !$create_end_time) {
				$condition[] = [ 'create_time', '>=', strtotime($create_start_time) ];
			}
			
			$rg_start_time = input('rg_start_time', '');
			$rg_end_time = input('rg_end_time', '');
			if ($rg_start_time && $rg_end_time) {
				$condition[] = [ 'reg_time', 'between', [ strtotime($rg_start_time), strtotime($rg_end_time) ] ];
			} elseif (!$rg_start_time && $rg_end_time) {
				$condition[] = [ 'reg_time', '<=', strtotime($rg_end_time) ];
				
			} elseif ($rg_start_time && !$rg_end_time) {
				$condition[] = [ 'reg_time', '>=', strtotime($rg_start_time) ];
			}
			
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getFenxiaoApplyPageList($condition, $page, $page_size, 'create_time desc', '*');
			return $list;
			
		} else {
			
			$level_model = new FenxiaoLevel();
			$level_list = $level_model->getLevelList([ [ 'status', '=', 1 ] ], 'level_id,level_name');
			$this->assign('level_list', $level_list['data']);
			
			$this->forthMenu();
			return $this->fetch('fenxiao/apply');
		}
	}
	
	/**
	 * 分销商申请通过
	 */
	public function applyPass()
	{
		$apply_id = input('apply_id');
		
		$model = new FenxiaoApply();
		$res = $model->pass($apply_id);
		return $res;
	}
	
	/**
	 * 分销商申请通过
	 */
	public function applyRefuse()
	{
		$apply_id = input('apply_id');
		
		$model = new FenxiaoApply();
		$res = $model->refuse($apply_id);
		return $res;
	}
	
	
	/**
	 * 四级菜单
	 * @param unknown $params
	 */
	protected function fiveMenu($params = [])
	{
		$url = strtolower($this->url);
		$menu_model = new MenuModel();
		$menu_info = $menu_model->getMenuInfo([ [ 'url', "=", $url ], [ 'level', '=', 5 ] ], 'parent');
		
		if (!empty($menu_info['data'])) {
			$menus = $menu_model->getMenuList([ [ 'app_module', "=", $this->app_module ], [ 'is_show', "=", 1 ], [ 'parent', '=', $menu_info['data']['parent'] ] ], '*', 'sort asc');
			foreach ($menus['data'] as $k => $v) {
				$menus['data'][ $k ]['parse_url'] = addon_url($menus['data'][ $k ]['url'], $params);
				if ($menus['data'][ $k ]['url'] == $url) {
					$menus['data'][ $k ]['selected'] = 1;
				} else {
					$menus['data'][ $k ]['selected'] = 0;
				}
			}
			$this->assign('forth_menu', $menus['data']);
		}
	}

    /**
     * 分销商下级团队
     */
    public function team()
    {
        $fenxiao_id = input('fenxiao_id', 0);
        $fenxiao_model = new FenxiaoModel();
        if (request()->isAjax()) {

            $condition[] = [ 'status', '=', 1 ];

            $level = input('level', 0);


            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $list = $fenxiao_model->getFenxiaoTeam($level, $fenxiao_id, $page, $page_size);
            return $list;
        } else {
            $fenxiao_info = $fenxiao_model->getFenxiaoInfo([["fenxiao_id", "=", $fenxiao_id]],"level_id");
            $level_id = $fenxiao_info['data']["level_id"];
            $fenxiao_level_model = new FenxiaoLevelModel();
            $level_info = $fenxiao_level_model->getLevelInfo([["level_id", "=", $level_id]],"level_num");
            $this->assign('level_num', $level_info["data"]["level_num"]);

            $this->assign('fenxiao_id', $fenxiao_id);
            $this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
            return $this->fetch('fenxiao/team');
        }
    }

    /*
     *  会员选择
     */
    public function memberSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = 8;
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'username');//可以传username mobile email

            $condition = [];
            //下拉选择
            $condition[] = [ $search_text_type, 'like', "%" . $search_text . "%" ];
            $condition[] = [ 'is_pintuan_trader', '=', 0 ];

            $order = 'member_id desc';
            $field = 'member_id,mobile,headimg,username,reg_time,nickname';

            $member_model = new MemberModel();
            $list = $member_model->getMemberPageList($condition, $page, $page_size, $order, $field);
            return $list;
        } else {
            //回调函数
            $callback = input('callback', '');
            $this->assign('callback', $callback);
            return $this->fetch('fenxiao/member_select');
        }
    }

    /*
     *  分销商选择
     */
    public function fenxiaoSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = 8;
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'fenxiao_name');
            $current_fenxiao_id = input('current_fenxiao_id', 0);
            $condition = [];
            //下拉选择
            $condition[] = [ "f.".$search_text_type, 'like', "%" . $search_text . "%" ];

            $order = 'f.fenxiao_id desc';
            $field = 'fenxiao_id,mobile,fenxiao_no,fenxiao_name,level_name';

            $fenxiao_model = new FenxiaoModel();
            $current_fenxiao_info = $fenxiao_model->getFenxiaoInfo([["fenxiao_id", "=", $current_fenxiao_id]],"level_id");
            $current_level_id = $current_fenxiao_info["data"]["level_id"];

            $fenxiao_level_model = new FenxiaoLevelModel();
            $current_level_info = $fenxiao_level_model->getLevelInfo([["level_id", "=", $current_level_id ]],"level_num");
            $current_level_num = $current_level_info["data"]["level_num"];
            $level_list = $fenxiao_level_model ->getLevelList([["level_num", ">", $current_level_num]],"level_id,level_num");
            $level_id_array = array();
            if(!empty($level_list["data"])){
                foreach($level_list["data"] as $k=>$v){
                    if(!in_array($v["level_id"],$level_id_array)){
                        array_push($level_id_array,$v["level_id"]);
                    }
                }
                $condition[] = [ "f.level_id", 'in', $level_id_array  ];
            }
            $list = $fenxiao_model->getFenxiaoPageList($condition, $page, $page_size, $order, $field);
            return $list;
        } else {
            $current_fenxiao_id = input('current_fenxiao_id', '');
            $this->assign('current_fenxiao_id', $current_fenxiao_id);
            return $this->fetch('fenxiao/fenxiao_select');
        }
    }

    /**
     * 更新上级分销商
     * @return array|mixed
     */
    public function updateFenxiaoParent()
    {
        if (request()->isAjax()) {
            $parent = input('parent', '');
            $current_fenxiao_id = input('current_fenxiao_id', '');

            $fenxiao_model = new FenxiaoModel();
            $res = $fenxiao_model->updateFenxiaoParent($current_fenxiao_id, $parent);
            return $res;
        }
    }

    /**
     * 奖金池列表
     */
    public function bonusPoolList(){

        $fenxiao = new \addon\newfenxiao\model\Fenxiao();
        if (request()->isAjax()) {

            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);

            $account_type = input('account_type', 0);
            $start_date = input('start_date', 0);
            $end_date = input('end_date', 0);

            $condition = [];
            if(!empty($account_type)){

                $condition[] = array('relation_type', '=', $account_type);
            }

            if ($start_date != '' && $end_date != '') {
                $condition[] = [ 'create_time', 'between', [ strtotime($start_date), strtotime($end_date) ] ];
            } else if ($start_date != '' && $end_date == '') {
                $condition[] = [ 'create_time', '>=', strtotime($start_date) ];
            } else if ($start_date == '' && $end_date != '') {
                $condition[] = [ 'create_time', '<=', strtotime($end_date) ];
            }

            $list = $fenxiao->getCommissionPoolRecordList($condition, $page_index, $page_size);
            return $list;
        }

        $money = $fenxiao->getPoolSum();
        $this->assign('money', $money);
        return $this->fetch('fenxiao/pool_list');
    }

    public function test(){

        $fenxiao = new FenxiaoOrder();
        $res = $fenxiao->laxinReward(569);
        dump($res);
    }

}