<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\admin\controller;

use addon\newfenxiao\model\Config as ConfigModel;
use app\admin\controller\BaseAdmin;
use app\model\system\Document;

/**
 *  分销设置
 */
class Config extends BaseAdmin
{
	/**
	 *  分销结算设置
	 */
	public function settlement()
	{
		$model = new ConfigModel();
		if (request()->isAjax()) {
			$data = [
				'account_type' => input('account_type', ''),//佣金计算方式
				'settlement_day' => input('settlement_day', ''),//天数
				'withdraw' => input('withdraw', ''),//最低提现额度
				'withdraw_rate' => input('withdraw_rate', ''),//佣金提现手续费
				'min_no_fee' => input('min_no_fee', ''),//最低免手续费区间
				'max_no_fee' => input('max_no_fee', ''),//最高免手续费区间
				'withdraw_status' => input('withdraw_status', ''),//提现审核
				'withdraw_type' => input('withdraw_type', ''),//提现方式
			];
			$res = $model->setFenxiaoSettlementConfig($data, 1);
			return $res;
		} else {
			$settlement = $model->getFenxiaoSettlementConfig();
			$this->assign("settlement_info", $settlement['data']['value']);
			$withdraw = $model->getFenxiaoWithdrawConfig();
			$this->assign("withdraw_info", $withdraw['data']['value']);
			
			$this->forthMenu();
			return $this->fetch('config/settlement');
		}
		
	}
	
	/**
	 *  分销文字设置
	 */
	public function words()
	{
		$model = new ConfigModel();
		if (request()->isAjax()) {
			$data = [
				'concept' => input('concept', ''),//分销概念
				'fenxiao_name' => input('fenxiao_name', ''),//分销商名称
				'withdraw' => input('withdraw', ''),//提现名称
				'account' => input('account', ''),//佣金
				'my_team' => input('my_team', ''),//我的团队
				'child' => input('child', ''),//下线
			];
			
			$res = $model->setFenxiaoWordsConfig($data, 1);
			return $res;
		} else {
			$fenxiao_config_result = $model->getFenxiaoWordsConfig();
			$config_info = $fenxiao_config_result['data']["value"];
			$this->assign("config_info", $config_info);
			
			$this->forthMenu();
			return $this->fetch('config/words');
		}
	}

    /**
     * 任务设置
     */
	public function task()
    {
        $model = new ConfigModel();
        if (request()->isAjax()) {
            $data = [
                'new_user_consume_reward' => input('new_user_consume_reward', ''),//新用户消费奖励
                'performance_reward' => json_decode(input('performance_reward', ''), true),//绩效奖励 json格式
                'performance_cycle_type' => input('performance_cycle_type', 'month'),//绩效周期类型
                'performance_cycle_diy_days' => input('performance_cycle_diy_days', 30),//自定义天数
            ];
            $res = $model->setFenxiaoTaskConfig($data);
            return $res;
        } else {
            $config_info = $model->getFenxiaoTaskConfig();
            $this->assign("config_info", $config_info['data']['value']);

            //绩效周期类型
            $performance_cycle_type = ConfigModel::getPerformanceCycleType();
            $this->assign('performance_cycle_type', $performance_cycle_type);

            $this->forthMenu();
            return $this->fetch('config/task');
        }
    }

    /**
     * 奖金池设置
     */
    public function bonusPool()
    {
        $model = new ConfigModel();
        if (request()->isAjax()) {
            $data = [
                'input_rate' => input('input_rate', 10),//输入比率 商品对外成本和设定成本之间的部分为基数
            ];
            $res = $model->setFenxiaoBonusPoolConfig($data, 1);
            return $res;
        } else {
            $config_info = $model->getFenxiaoBonusPoolConfig();
            $this->assign("config_info", $config_info['data']['value']);

            $this->forthMenu();
            return $this->fetch('config/bonus_pool');
        }
    }
}