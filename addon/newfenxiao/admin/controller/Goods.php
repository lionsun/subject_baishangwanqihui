<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\admin\controller;

use addon\newfenxiao\model\FenxiaoGoods;
use addon\newfenxiao\model\FenxiaoGoodsSku as FenxiaoGoodsSkuModel;
use addon\newfenxiao\model\FenxiaoLevel as FenxiaoLevelModel;
use app\model\goods\Goods as GoodsModel;
use app\admin\controller\BaseAdmin;
use app\model\goods\GoodsBrand;

/**
 *  分销商品
 */
class Goods extends BaseAdmin
{
	
	/**
	 * 分销等级列表
	 */
	public function lists()
	{
		$model = new FenxiaoGoods();
		
		if (request()->isAjax()) {
			
			$page_index = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);

			$search_text_type = input('search_text_type', "goods_name");//店铺名称或者商品名称
			$search_text = input('search_text', "");
			if (!empty($search_text)) {
				$condition[] = [ $search_text_type, 'like', '%' . $search_text . '%' ];
			}
			
			$goods_class = input('goods_class', "");//商品种类
			if ($goods_class !== "") {
				$condition[] = [ 'goods_class', '=', $goods_class ];
			}
			
			$goods_state = input('goods_state', "");//商品状态
			if ($goods_state !== '') {
				$condition[] = [ 'goods_state', '=', $goods_state ];
			}
			
			$category_id = input('category_id', "");//分类ID
			if (!empty($category_id)) {
				$condition[] = [ 'category_id|category_id_1|category_id_2|category_id_3', '=', $category_id ];
			}
			
			$brand_id = input('goods_brand', '');//商品品牌
			if (!empty($brand_id)) {
				$condition[] = [ 'brand_id', '=', $brand_id ];
			}
            $condition[] = ['nfg.state', '=', 1];
			$list = $model->getGoodsPageList($condition, $page_index, $page_size);
			return $list;
		} else {
			//商品品牌
			$goods_brand_model = new GoodsBrand();
			$list = $goods_brand_model->getBrandList('', 'brand_id,brand_name', 'sort asc');
			$this->assign('goods_brand', $list['data']);
			return $this->fetch('goods/lists');
		}
	}
	
	public function detail()
	{
		$goods_id = input('goods_id');

		$fenxiao_sku_model = new FenxiaoGoodsSkuModel();

		$goods_info = $fenxiao_sku_model->getGoodsDetail($goods_id);

		$fenxiao_skus = $fenxiao_sku_model->getSkuList([ 'goods_id' => $goods_id ]);


		$goods_info['data']['fenxiao_skus'] = $fenxiao_skus['data'];

		$this->assign('goods_info', $goods_info['data']);
		return $this->fetch('goods/detail');
	}
}