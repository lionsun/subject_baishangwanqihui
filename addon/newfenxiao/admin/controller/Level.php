<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newfenxiao\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\newfenxiao\model\Config as ConfigModel;
use addon\newfenxiao\model\FenxiaoLevel as FenxiaoLevelModel;

/**
 *  分销等级管理
 */
class Level extends BaseAdmin
{
	
	/**
	 * 分销等级列表
	 */
	public function lists()
	{
		$model = new FenxiaoLevelModel();
		$field = 'level_id,level_name,rate,create_time';
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getLevelPageList([], $page, $page_size, 'level_id asc', $field);
			return $list;
		} else {
			return $this->fetch('level/lists');
		}
		
	}

	
	/**
	 * 编辑分销等级
	 */
	public function edit()
	{
		$model = new FenxiaoLevelModel();
		if (request()->isAjax()) {
			$data = [
				'level_name' => input('level_name', ''),
                'level_num' => input('level_num', 0),
				'rate' => input('rate', ''),
			];
			$level_id = input('level_id', '');
			
			$res = $model->editLevel($data, [ [ 'level_id', '=', $level_id ] ]);
			return $res;
		} else {
            $level_id = input('level_id', '');
		    $level_info = $model->getLevelInfo([['level_id', '=', $level_id]]);
		    $this->assign('level_info', $level_info['data']);
            return $this->fetch('level/edit');
		}
	}

	public function delete()
	{
		$model = new FenxiaoLevelModel();
		
		$level_id = input('level_id', '');
		$res = $model->deleteLevel($level_id);
		return $res;
	}
}