<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\alisms\event;
use addon\alisms\model\Config as ConfigModel;

/**
 * 短信模板  (后台调用)
 */
class DoEditSmsMessage
{
	/**
	 * 短信发送方式方式及配置
	 */
	public function handle()
	{
	    $config_model = new ConfigModel();
        $config_result = $config_model->getSmsConfig();
        $config = $config_result["data"];
        if($config["is_use"] == 1){
            return ["edit_url" => "alisms://admin/message/edit"];
        }

	}
}