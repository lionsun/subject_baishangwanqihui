<?php


/*
* Copyright (c) 2008-2016 vip.com, All Rights Reserved.
*
* Powered by com.vip.osp.osp-idlc-2.5.11.
*
*/

namespace com\vip\fcs\ap\service;

class BaseBillAndDisCountExtSource {
	
	static $_TSPEC;
	public $dbNo = null;
	public $itemSize = null;
	public $vendorGoodsNo = null;
	public $poDeliveryType = null;
	public $priceTime = null;
	public $extOrderNum = null;
	public $deliveryWarehouseCode = null;
	public $deliveryWarehouseName = null;
	
	public function __construct($vals=null){
		
		if (!isset(self::$_TSPEC)){
			
			self::$_TSPEC = array(
			501 => array(
			'var' => 'dbNo'
			),
			502 => array(
			'var' => 'itemSize'
			),
			503 => array(
			'var' => 'vendorGoodsNo'
			),
			504 => array(
			'var' => 'poDeliveryType'
			),
			505 => array(
			'var' => 'priceTime'
			),
			506 => array(
			'var' => 'extOrderNum'
			),
			507 => array(
			'var' => 'deliveryWarehouseCode'
			),
			508 => array(
			'var' => 'deliveryWarehouseName'
			),
			
			);
			
		}
		
		if (is_array($vals)){
			
			
			if (isset($vals['dbNo'])){
				
				$this->dbNo = $vals['dbNo'];
			}
			
			
			if (isset($vals['itemSize'])){
				
				$this->itemSize = $vals['itemSize'];
			}
			
			
			if (isset($vals['vendorGoodsNo'])){
				
				$this->vendorGoodsNo = $vals['vendorGoodsNo'];
			}
			
			
			if (isset($vals['poDeliveryType'])){
				
				$this->poDeliveryType = $vals['poDeliveryType'];
			}
			
			
			if (isset($vals['priceTime'])){
				
				$this->priceTime = $vals['priceTime'];
			}
			
			
			if (isset($vals['extOrderNum'])){
				
				$this->extOrderNum = $vals['extOrderNum'];
			}
			
			
			if (isset($vals['deliveryWarehouseCode'])){
				
				$this->deliveryWarehouseCode = $vals['deliveryWarehouseCode'];
			}
			
			
			if (isset($vals['deliveryWarehouseName'])){
				
				$this->deliveryWarehouseName = $vals['deliveryWarehouseName'];
			}
			
			
		}
		
	}
	
	
	public function getName(){
		
		return 'BaseBillAndDisCountExtSource';
	}
	
	public function read($input){
		
		$input->readStructBegin();
		while(true){
			
			$schemeField = $input->readFieldBegin();
			if ($schemeField == null) break;
			$needSkip = true;
			
			
			if ("dbNo" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->dbNo);
				
			}
			
			
			
			
			if ("itemSize" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->itemSize);
				
			}
			
			
			
			
			if ("vendorGoodsNo" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->vendorGoodsNo);
				
			}
			
			
			
			
			if ("poDeliveryType" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->poDeliveryType);
				
			}
			
			
			
			
			if ("priceTime" == $schemeField){
				
				$needSkip = false;
				$input->readI64($this->priceTime);
				
			}
			
			
			
			
			if ("extOrderNum" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->extOrderNum);
				
			}
			
			
			
			
			if ("deliveryWarehouseCode" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->deliveryWarehouseCode);
				
			}
			
			
			
			
			if ("deliveryWarehouseName" == $schemeField){
				
				$needSkip = false;
				$input->readString($this->deliveryWarehouseName);
				
			}
			
			
			
			if($needSkip){
				
				\Osp\Protocol\ProtocolUtil::skip($input);
			}
			
			$input->readFieldEnd();
		}
		
		$input->readStructEnd();
		
		
		
	}
	
	public function write($output){
		
		$xfer = 0;
		$xfer += $output->writeStructBegin();
		
		if($this->dbNo !== null) {
			
			$xfer += $output->writeFieldBegin('dbNo');
			$xfer += $output->writeString($this->dbNo);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->itemSize !== null) {
			
			$xfer += $output->writeFieldBegin('itemSize');
			$xfer += $output->writeString($this->itemSize);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->vendorGoodsNo !== null) {
			
			$xfer += $output->writeFieldBegin('vendorGoodsNo');
			$xfer += $output->writeString($this->vendorGoodsNo);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->poDeliveryType !== null) {
			
			$xfer += $output->writeFieldBegin('poDeliveryType');
			$xfer += $output->writeString($this->poDeliveryType);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->priceTime !== null) {
			
			$xfer += $output->writeFieldBegin('priceTime');
			$xfer += $output->writeI64($this->priceTime);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->extOrderNum !== null) {
			
			$xfer += $output->writeFieldBegin('extOrderNum');
			$xfer += $output->writeString($this->extOrderNum);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->deliveryWarehouseCode !== null) {
			
			$xfer += $output->writeFieldBegin('deliveryWarehouseCode');
			$xfer += $output->writeString($this->deliveryWarehouseCode);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		if($this->deliveryWarehouseName !== null) {
			
			$xfer += $output->writeFieldBegin('deliveryWarehouseName');
			$xfer += $output->writeString($this->deliveryWarehouseName);
			
			$xfer += $output->writeFieldEnd();
		}
		
		
		$xfer += $output->writeFieldStop();
		$xfer += $output->writeStructEnd();
		return $xfer;
	}
	
}

?>