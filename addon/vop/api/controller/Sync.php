<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\vop\api\controller;

use addon\vop\model\Goods as GoodsModel;
use addon\vop\model\Vop;
use app\api\controller\BaseApi;


//vop/api/Sync/brandStart2

class Sync extends BaseApi
{
    /**
     * 每天凌晨更改状态，开始同步在售和预售
     */
    public function brandStart()
    {
        set_time_limit(0);
        model('sync_vop')->update(['status' => 0, 'page' => 0],['type' => 1]);
        //model('sync_vop')->update(['status' => 0, 'page' => 0],['type' => 2]);
        return $this->response($this->success());
    }


    /**
     *预售
     */
    public function brandStart2()
    {
        set_time_limit(0);
        model('sync_vop')->update(['status' => 0, 'page' => 0],['type' => 2]);
        return $this->response($this->success());
    }



    /**
     * 在售品牌列表保存goods_brand
     */
    public function brandList()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getBrandList();
        return $this->response($res);
    }

    /**
     * 预告品牌列表保存goods_brand
     */
    public function preBrandList()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getPreBrandList();
        return $this->response($res);
    }

    /**
     * 商品列表
     */
    public function goodsList()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getGoodsList();
        return $this->response($res);
    }


    /**
     * 预告商品列表
     */
    public function preGoodsList()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getPreGoodsList();
        return $this->response($res);
    }

    /**
     * 地址库
     */
    public function address()
    {
        $model = new Vop();
        $res = $model->address();
        dump($res);die;
    }


    public function category()
    {
        $model = new GoodsModel();
        $res = $model->categoryDeal();
        return $this->response($res);
    }

    //更新是否发货
    public function orderDeliveryStatus()
    {
        $model = new GoodsModel();
        $res = $model->orderDeliveryStatus();
        return $this->response($res);
    }

    //退货申请处理
    public function orderRefundStatus()
    {
        $model = new GoodsModel();
        $res = $model->orderRefundStatus();
        return $this->response($res);
    }

    public function getCarrierList(){
        $model = new Vop();
        $res = $model->getCarrierList();
        foreach ($res as $key => $value) {
            $add = [
                'cust_no' => $value['custNo'],
                'cust_name' => $value['custName'],
                'cust_code' => $value['custCode'],
            ];
            model('vop_carrier')->add($add);
        }

        return $this->response($res);
    }


    public function priceDeal()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->priceDeal();
        return $this->response($res);
    }



    /**
     * 商品分类统计列表(唯品给了所有分类的excel)
     */
    /*    public function category()
        {
            $model = new GoodsModel();
            $res = $model->category();
            return $this->response($res);
        }*/



    /*    public function getToken()
        {
            $code = isset($this->params['code']) ? $this->params['code'] : '';
            $ip = request()->ip();

            if (empty($code) || empty($ip)) {
                return $this->response($this->error('', '参数缺失'));
            }

            $model = new GoodsModel();
            $data = [
                'code' => $code,
                'ip' => $ip
            ];
            $res = $model->getAccessToken($data);
            return $this->response($res);
        }*/

    /*************************************************************新加方法**********************************************************/

    /**
     *退货申请处理  (判断维代购 是否接收到货物 )
     */
    public function orderRefundStatusTheGoods()
    {
        $model = new GoodsModel();
        $res = $model->orderRefundStatusTheGoods();
        return $this->response($res);
    }


    /**
     *唯品收到货物 同意后执行  退款维权结束
     */
    public function orderRefundFinish()
    {
        $model = new GoodsModel();
        $res = $model->orderRefundFinish();
        return $this->response($res);
    }



    /**
     *唯品收到下架
     */
    public function detectionGoodsOnline()
    {
        $model = new GoodsModel();
        $res = $model->detectionGoodsOnline();
        return $this->response($res);
    }





    /*************************************************************永久保留*7777**********************************************************/






    /*************************************************************临时调试***********************************************************/


    /**
     * 商品详情
     */
    public function getGoodsDetail()
    {
        $goodFullId = isset($this->params['vop_goods_id']) ? $this->params['vop_goods_id'] : 0;
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getGoodsDetail($goodFullId);
        var_dump("<pre>");
        var_dump($res);
        exit;
        return $this->response($res);
    }




    /**
     * 商品列表
     */
    public function goodsList2()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getGoodsList2();
        return $this->response($res);
    }











    //退货申请处理进程  详情
    public function getOrderReturnProgress()
    {
        $model = new Vop();
        $res = $model->getOrderReturnProgress();
        return $this->response($res);
    }




    //更新是否发货2
    public function orderDeliveryStatus2()
    {
        $model = new GoodsModel();
        $res = $model->orderDeliveryStatus2();
        return $this->response($res);
    }




    //退货申请处理  详情
    public function orderRefundStatus2()
    {
        $model = new GoodsModel();
        $res = $model->orderRefundStatus2();
        return $this->response($res);
    }



    /**
     * 预售品牌数据（快速查询数据可以删除此方法）
     * @return array
     */
    public function preBrandList2()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getPreBrandList2();
        return $this->response($res);
    }



    /**
     * 补充商品分类
     */
    public function replenishCategory()
    {
        $model = new GoodsModel();
        $res = $model->replenishCategory();
        dump($res);die;
    }







    /**
     * 定时删除重复商品
     */
    public function modifyIsDelete()
    {
        $model = new GoodsModel();
        $res = $model->modifyIsDelete();
        dump($res);die;
    }







}