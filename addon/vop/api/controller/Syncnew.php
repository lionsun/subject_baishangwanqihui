<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\vop\api\controller;

use addon\vop\model\Goodsnew as GoodsModel;
use addon\vop\model\Vop;
use app\api\controller\BaseApi;


//预售商品数据处理

class Syncnew extends BaseApi
{
    /**
     * 每天凌晨更改状态，开始同步在售和预售
     */
    public function brandStart()
    {
        set_time_limit(0);
        model('sync_vop')->update(['status' => 0, 'page' => 0],['type' => 1]);
        model('sync_vop')->update(['status' => 0, 'page' => 0],['type' => 2]);
        return $this->response($this->success());
    }


    
    /**
     * 预告品牌列表保存
     * goods_brand
     * vop_ad_pre
     */
    public function preBrandList()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getPreBrandList();
        return $this->response($res);
    }




    /**
     * 预告商品列表
     */
    public function preGoodsList()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $res = $model->getPreGoodsList();
        return $this->response($res);
    }




}