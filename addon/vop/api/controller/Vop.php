<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\vop\api\controller;

use addon\vop\model\Goods;
use app\api\controller\BaseApi;


class Vop extends BaseApi
{
    /**
     * 获取唯品商品库存
     */
    public function getVopGoodsInfo()
    {
        $sku_id = isset($this->params['sku_id']) ? $this->params['sku_id'] : 0;
        if (empty($sku_id)) {
            return $this->response($this->error('', '参数缺失'));
        }

        $model = new Goods();
        $res = $model->getVopGoodsInfo($sku_id);
        return $this->response($res);

    }

    public function carrierList(){
        $model = new Goods();
        $res = $model->carrierList();
        return $this->response($res);
    }
}