<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\vop\api\controller;

use app\api\controller\BaseApi;

class Test extends BaseApi
{
    public function goods()
    {
        require_once 'addon/vop/src/com/vip/wpc/ospservice/vop/WpcVopOspServiceClient.php';

        $ctx = null;
        try {
            //1、获取服务客户端
            $service = \com\vip\wpc\ospservice\vop\WpcVopOspServiceClient::getService();

            //2、设置系统级调用参数，只需在程序开始调用前设置一次即可
            $ctx = \Osp\Context\InvocationContextFactory::getInstance ();
            $ctx->setAppKey("1bca9769");//替换为你的appKey
            $ctx->setAppSecret("F0CD2F97B8B546AD4096FE8775430256");//替换为你的appSecret
            //$ctx->setAccessToken("accessToken");//替换为你的accessToken，通过Oauth认证时必填
            //$ctx->setAppURL("http://sandbox.vipapis.com/");//沙箱环境
            $ctx->setAppURL("https://gw.vipapis.com/");//正式环境


            //3、调用API及返回
            $request1=new \com\vip\wpc\ospservice\vop\request\WpcGoodsListRequest();
            $request1->vopChannelId="1bca9769";
            //$request1->timestamp=time();
            $request1->userNumber="88544117";
            $request1->page=1;
            $request1->pageSize=1;
            $request1->adId = '48089';

            $rtn = $service->getGoodsList($request1);
            $rtn = object_to_array($rtn);
            dump($rtn);die;
        } catch (\Osp\Exception\OspException $e) {
            //4、捕获异常
            var_dump($ctx->getSign());//获取最近一次调用的sign值
        }

    }

    public function brand()
    {
        require_once 'addon/vop/src/com/vip/wpc/ospservice/vop/WpcVopOspServiceClient.php';

        $ctx = null;
        try {
            //1、获取服务客户端
            $service = \com\vip\wpc\ospservice\vop\WpcVopOspServiceClient::getService();

            //2、设置系统级调用参数，只需在程序开始调用前设置一次即可
            $ctx = \Osp\Context\InvocationContextFactory::getInstance ();
            $ctx->setAppKey("1bca9769");//替换为你的appKey
            $ctx->setAppSecret("F0CD2F97B8B546AD4096FE8775430256");//替换为你的appSecret
            //$ctx->setAccessToken("accessToken");//替换为你的accessToken，通过Oauth认证时必填
            //$ctx->setAppURL("http://sandbox.vipapis.com/");//沙箱环境
            $ctx->setAppURL("https://gw.vipapis.com/");//正式环境


            //3、调用API及返回
            $request1=new \com\vip\wpc\ospservice\vop\request\WpcBrandListRequest();
            $request1->vopChannelId="1bca9769";
            //$request1->timestamp=time();
            $request1->userNumber="88544117";
            $request1->page=1;
            $request1->pageSize=20;

            $rtn = $service->getBrandList($request1);
            $rtn = object_to_array($rtn);
            dump($rtn);die;
        } catch (\Osp\Exception\OspException $e) {
            //4、捕获异常
            var_dump($ctx->getSign());//获取最近一次调用的sign值
        }

    }

    public function preBrand()
    {
        require_once 'addon/vop/src/com/vip/wpc/ospservice/vop/WpcVopOspServiceClient.php';

        $ctx = null;
        try {
            //1、获取服务客户端
            $service = \com\vip\wpc\ospservice\vop\WpcVopOspServiceClient::getService();

            //2、设置系统级调用参数，只需在程序开始调用前设置一次即可
            $ctx = \Osp\Context\InvocationContextFactory::getInstance ();
            $ctx->setAppKey("1bca9769");//替换为你的appKey
            $ctx->setAppSecret("F0CD2F97B8B546AD4096FE8775430256");//替换为你的appSecret
            //$ctx->setAccessToken("accessToken");//替换为你的accessToken，通过Oauth认证时必填
            //$ctx->setAppURL("http://sandbox.vipapis.com/");//沙箱环境
            $ctx->setAppURL("https://gw.vipapis.com/");//正式环境


            //3、调用API及返回
            $request1=new \com\vip\wpc\ospservice\vop\request\WpcPreBrandListRequest();
            $request1->vopChannelId="1bca9769";
            //$request1->timestamp=time();
            $request1->userNumber="88544117";
            $request1->page=1;
            $request1->pageSize=5;

            $rtn = $service->getPreBrandList($request1);
            $rtn = object_to_array($rtn);
            dump($rtn);die;
        } catch (\Osp\Exception\OspException $e) {
            //4、捕获异常
            var_dump($ctx->getSign());//获取最近一次调用的sign值
        }

    }


	public function address()
	{
        require_once 'addon/vop/src/vipapis/address/AddressServiceClient.php';

        $ctx = null;
        try {
            //1、获取服务客户端
            $service = \vipapis\address\AddressServiceClient::getService();
            //2、设置系统级调用参数，只需在程序开始调用前设置一次即可
            $ctx = \Osp\Context\InvocationContextFactory::getInstance ();

            $ctx->setAppKey("1bca9769");//替换为你的appKey
            $ctx->setAppSecret("F0CD2F97B8B546AD4096FE8775430256");//替换为你的appSecret
            //$ctx->setAccessToken("accessToken");//替换为你的accessToken，通过Oauth认证时必填
            //$ctx->setAppURL("http://sandbox.vipapis.com/");//沙箱环境
            $ctx->setAppURL("https://gw.vipapis.com/");//正式环境
            //$ctx->setTimeOut(30)//超时时间，可选，默认30秒

            //3、调用API及返回
            $rtn = $service->getProvinceWarehouse(\vipapis\address\Is_Show_GAT::SHOW_ALL);
            $rtn = object_to_array($rtn);
            dump($rtn);die;
        } catch (\Osp\Exception\OspException $e) {
            //4、捕获异常
            var_dump($ctx->getSign());//获取最近一次调用的sign值
        }

	}

}