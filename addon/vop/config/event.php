<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //商品下架
        'AdIdToOffGoods' => [
            'addon\vop\event\AdIdToOffGoods',
        ],
        //商品上架
        'AdIdToOnGoods' => [
            'addon\vop\event\AdIdToOnGoods',
        ],
        //支付唯品订单
        'OrderPay' => [
            'addon\vop\event\OrderPay',
        ],
        //        'orderRefundApplyVop' => [
        //            'addon\vop\event\VopOrderRefundApply',
        //        ],
        'VopOrderRefundApply' => [
            'addon\vop\event\VopOrderRefundApply',
        ],
        'VopRefundDelivery' => [
            'addon\vop\event\VopRefundDelivery',
        ],
    ],

    'subscribe' => [
    ],
];
