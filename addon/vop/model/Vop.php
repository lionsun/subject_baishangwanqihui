<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\vop\model;

use app\model\BaseModel;
use think\facade\Log;

/**
 * 唯品Api
 */
class Vop extends BaseModel
{
    public $appKey = '1bca9769';
    public $appSecret = 'F0CD2F97B8B546AD4096FE8775430256';
    public $accessToken = '';
    public $uri = 'http://www.bswqh.com';//回调地址
    public $userNumber = '88544117';//代购编号
    public $service = null;

    public function __construct()
    {
        require_once 'addon/vop/src/com/vip/wpc/ospservice/vop/WpcVopOspServiceClient.php';

        //1、获取服务客户端
        $this->service = \com\vip\wpc\ospservice\vop\WpcVopOspServiceClient::getService();

        //2、设置系统级调用参数，只需在程序开始调用前设置一次即可
        $ctx = \Osp\Context\InvocationContextFactory::getInstance ();
        $ctx->setAppKey($this->appKey);//替换为你的appKey
        $ctx->setAppSecret($this->appSecret);//替换为你的appSecret
        //$ctx->setAccessToken("accessToken");//替换为你的accessToken，通过Oauth认证时必填
        //$ctx->setAppURL("http://sandbox.vipapis.com/");//沙箱环境
        $ctx->setAppURL("https://gw.vipapis.com/");//正式环境
        //$ctx->setTimeOut(30)//超时时间，可选，默认30秒
    }

    /*****************************************************商品*********************************************************/

    /**
     * 品牌列表
     * @param $page
     * @param $page_size
     * @return array|mixed|null
     */
    public function getBrandList($page, $page_size)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcBrandListRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->page = $page;
        $request->pageSize = $page_size;

        $rtn =$this->service->getBrandList($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }

    /**
     * 预告品牌列表
     * @param $page
     * @param $page_size
     * @return array|mixed|null
     */
    public function getPreBrandList($page, $page_size)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcPreBrandListRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->page = $page;
        $request->pageSize = $page_size;

        $rtn =$this->service->getPreBrandList($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }

    /**
     * 品牌商品列表
     * @param $page
     * @param $page_size
     * @param $ad_id
     * @return array|mixed|null
     */
    public function getGoodsList($page, $page_size , $ad_id)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcGoodsListRequest();
        //$request->timestamp = time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->page = $page;
        $request->pageSize = $page_size;
        $request->adId = $ad_id;

        $rtn =$this->service->getGoodsList($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }


    /**
     * 预告商品列表
     * @param $page
     * @param $page_size
     * @param $ad_id
     * @return array|mixed|null
     */
    public function getPreGoodsList($page, $page_size, $ad_id)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcPreGoodsListRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->page = $page;
        $request->pageSize = $page_size;
        $request->adId = $ad_id;

        $rtn =$this->service->getPreGoodsList($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }


    /**
     * 商品详情
     * @param $goodFullId
     * @return array|mixed|null
     */
    public function getGoodsDetail($goodFullId)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcGoodsDetailRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->goodFullIds = $goodFullId;

        $rtn =$this->service->getGoodsDetail($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }

    /**
     * 查询尺码状态
     * @param $sizeId
     * @return array|mixed|null
     */
    public function getSizeStatus($sizeId)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcSizeStatusRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->sizeIds = $sizeId;

        $rtn =$this->service->getSizeStatus($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }



    /**
     * 查询商品上下架
     * @param $goodFullId
     * @return array|mixed|null
     */
    public function getGoodsOnline($goodFullId)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcOnlineGoodsRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->goodFullIds = $goodFullId;

        $rtn =$this->service->getGoodsOnline($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }

    /**
     * 商品库存
     * @param $goodFullId
     * @return array|mixed|null
     */
    public function getGoodsStock($goodFullId)
    {
        $request=new \com\vip\wpc\ospservice\vop\request\WpcGoodsStockRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;
        $request->goodFullIds = $goodFullId;

        $rtn =$this->service->getGoodsStock($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }

    /*****************************************************订单*********************************************************/

    /**
     * 创建订单
     * @param $order_id
     * @return array|mixed|null
     */
    public function orderCreate($order_id)
    {
        $order_info = model('order')->getInfo(['order_id' => $order_id, 'source' => 2],'order_no,source,name,mobile,province_id,city_id,district_id,address,buyer_ip');
        if (empty($order_info)) {
            return $this->error('','未找到订单数据');
        }

        $order_goods = model('order_goods')->getList(['order_id' => $order_id],'sku_no,num');
        if (empty($order_goods)) {
            return $this->error('','未找到订单商品数据');
        }
        $sizeInfo = [];
        foreach ($order_goods as $key => $value) {
            $sizeInfo[$value['sku_no']] = $value['num'];
        }

        $area_ids = [$order_info['province_id'],$order_info['city_id'],$order_info['district_id']];
        $area_info = model('area')->getList([['id','in',$area_ids]],'name');
        if (empty($area_info)) {
            return $this->error('','地区未找到');
        }

        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderCreateRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            //$request->sizeInfo = '{"6917924778591519937":1}';
            $request->sizeInfo = json_encode($sizeInfo);
            $request->provinceName = isset($area_info[0])?$area_info[0]:'';
            $request->cityName = isset($area_info[1])?$area_info[1]:'';
            $request->areaName = isset($area_info[2])?$area_info[2]:'';
            $request->address = $order_info['address'];
            $request->consignee = $order_info['name'];
            $request->mobile = $order_info['mobile'];
            $request->traceId = 1;//调用方的事务id ??
            $request->clientIp = $order_info['buyer_ip'];//用户真实ip
            $request->channelOrderSn = $order_info['order_no'];//渠道订单号

            $rtn = $this->service->orderCreate($request);
            $rtn = object_to_array($rtn);
            if (isset($rtn['orders'])) {
                $vop_order_no = $rtn['orders'][0]['orderSn'];
                model('order')->update(['vop_order_no' => $vop_order_no],['order_id' => $order_id]);
            }
            Log::INFO('【创建唯品订单】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【创建唯品订单'.$order_id.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');
            return $this->error('',$e->getReturnMessage());
        }

    }




    /**
     * 申请代扣
     * @param $order_id
     * @return array
     */
    public function applyPayment($order_id)
    {
        $order_info = model('order')->getInfo(['order_id' => $order_id, 'source' => 2],'vop_order_no,buyer_ip,source');
        if(empty($order_info)){
            return $this->error('','不属于唯品订单数据');
        }
        //return $this->success(['代扣唯品订单']);
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcVopOrderPaymentRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->clientIp = $order_info['buyer_ip'];//用户真实ip
            $request->orderSn = $order_info['vop_order_no'];//订单编号

            $rtn =$this->service->applyPayment($request);


            $rtn = object_to_array($rtn);
            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$rtn;
            $new_data['method']="applyPayment";
            $new_data['method_text']="代扣唯品订单申请代扣";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../applyPaymentLOG.txt');



            if ($rtn['applySuccess'] == false) {
                Log::ERROR('【代扣唯品订单申请代扣】'.$order_id);
                Log::ERROR('【代扣唯品订单申请失败申请代扣】'.$rtn['failReason']);
                return $this->error('',$rtn['failReason']);
            }
            Log::INFO('【代扣唯品订单申请代扣】'.json_encode($rtn));
            return $this->success($rtn);
         } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【代扣唯品订单申请代扣'.$order_id.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');


            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$e->getReturnMessage();
            $new_data['method']="applyPayment";
            $new_data['method_text']="代扣唯品订单申请代扣";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../applyPaymentLOG.txt');



            return $this->error('',$e->getReturnMessage());
        }
    }






    /**
     * 取消订单
     * @param $order_id
     * @return array
     */
    public function orderCancel($order_id)
    {
        $order_info = model('order')->getInfo(['order_id' => $order_id,'source' => 2],'vop_order_no,buyer_ip');
        if (empty($order_info)) {
            return $this->error('','未找到订单数据');
        }
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcVopOrderCancelRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->clientIp = $order_info['buyer_ip'];//用户真实ip
            $request->orderSn = $order_info['vop_order_no'];//订单编号

            $rtn =$this->service->orderCancel($request);
            $rtn = object_to_array($rtn);



            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$rtn;
            $new_data['method']="orderCancel";
            $new_data['method_text']="取消唯品订单";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderCancelLOG.txt');






            Log::INFO('【取消唯品订单】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【取消唯品订单'.$order_id.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');



            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$e->getReturnMessage();
            $new_data['method']="orderCancel";
            $new_data['method_text']="取消唯品订单";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderCancelLOG.txt');



            return $this->error('',$e->getReturnMessage());
        }
    }

    /**
     * 申请退货单
     * @return array
     */
    public function orderReturnCreate($params)
    {
        $order_goods_info = model('order_goods')->getInfo(['order_goods_id' => $params['order_goods_id'],'source' => 2]);
        if (empty($order_goods_info)) {
            return $this->error('','未找到订单数据');
        }

        $order_info = model('order')->getInfo(['order_id' => $order_goods_info['order_id']]);

        $sizeInfo = [
            [
                'sizeId' => $order_goods_info['sku_no'],
                'num' => $order_goods_info['num'],
                'reasonId' => $params['reason_id'],
            ],
        ];

        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderReturnCreateRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->clientIp = $order_info['buyer_ip'];//用户真实ip
            $request->orderSn = $order_info['vop_order_no'];//订单编号
            $request->sizeInfo = json_encode($sizeInfo);

            $rtn =$this->service->orderReturnCreate($request);
            $rtn = object_to_array($rtn);


            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$rtn;
            $new_data['method']="orderReturnCreate";
            $new_data['method_text']="唯品订单申请退货";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderReturnCreateLOG.txt');




            Log::INFO('【唯品订单申请退货】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {


            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$e->getReturnMessage();
            $new_data['method']="orderReturnCreate";
            $new_data['method_text']="唯品订单申请退货";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderReturnCreateLOG.txt');



            Log::ERROR('【唯品订单申请退货'.$params['order_goods_id'].'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');
            return $this->error('',$e->getReturnMessage());
        }
    }


    /**
     * 可选退货原因
     * @param $order_goods_id
     * @return array
     */
    public function getReturnOrderCreate($order_goods_id)
    {
        $order_goods_info = model('order_goods')->getInfo(['order_goods_id' => $order_goods_id,'source' => 2],'order_id');
        if (empty($order_goods_info)) {
            return $this->error('','未找到订单数据');
        }
        $order_info = model('order')->getInfo(['order_id' => $order_goods_info['order_id']],'vop_order_no');
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderReturnRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->orderSn = $order_info['vop_order_no'];//订单编号

            $rtn =$this->service->getReturnOrderCreate($request);
            $rtn = object_to_array($rtn);
            Log::INFO('【唯品可选退货原因】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【唯品可选退货原因'.$order_goods_id.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');
            return $this->error('',$e->getReturnMessage());
        }
    }

    public function getOrderReturnDetail($vop_order_no)
    {
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderReturnRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->orderSn = $vop_order_no;//订单编号

            $rtn =$this->service->getOrderReturnDetail($request);


            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$rtn;
            $new_data['method']="getOrderReturnDetail";
            $new_data['method_text']="唯品退货详情信息";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../getOrderReturnDetailLOG.txt');



            $rtn = object_to_array($rtn);
            Log::INFO('【唯品退货详情信息】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【唯品退货详情信息'.$vop_order_no.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');



            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$e->getReturnMessage();
            $new_data['method']="getOrderReturnDetail";
            $new_data['method_text']="唯品退货详情信息";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../getOrderReturnDetailLOG.txt');


            return $this->error('',$e->getReturnMessage());
        }
    }



    /**
     * 填写回寄物流单号    //21030421392635
     * @return array
     */
    public function updateReturnTransportNo($params)
    {

        $order_goods_info = model('order_goods')->getInfo(['order_goods_id' => $params['order_goods_id']],'order_id,source');
        if (empty($order_goods_info) || $order_goods_info['source'] != 2) {
            return $this->error('','未找到订单数据');
        }
        $order_info = model('order')->getInfo(['order_id' => $order_goods_info['order_id']],'vop_order_no');


        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderReturnUpdateTransportNoRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->orderSn = $order_info['vop_order_no'];//订单编号
            $request->carriersCode = $params['vop_refund_cust_no'];
            $request->transportNo = $params['refund_delivery_no'];//运单号
            $request->remark = '';

            $rtn =$this->service->updateReturnTransportNo($request);
            $rtn = object_to_array($rtn);



            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$rtn;
            $new_data['data']=$params;
            $new_data['method']="updateReturnTransportNo";
            $new_data['method_text']="填写回寄物流单号";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../updateReturnTransportNoLOG.txt');





            Log::INFO('【唯品填写回寄物流单号信息】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {



            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$e->getReturnMessage();
            $new_data['method']="updateReturnTransportNo";
            $new_data['method_text']="唯品退货详情信息";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../updateReturnTransportNoLOG.txt');



            Log::ERROR('【唯品填写回寄物流单号'.$order_info['vop_order_no'].'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');
            return $this->error('',$e->getReturnMessage());
        }
    }






    /**
     * 订单状态信息
     * @param $vop_order_no
     * @return array
     */
    public function getOrderInfoList($vop_order_no)
    {
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderInfoRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->orderSnList = $vop_order_no;//订单编号
            $request->page = 1;
            $request->pageSize = 20;
            $rtn =$this->service->getOrderInfoList($request);
            $rtn = object_to_array($rtn);


            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$rtn;
            $new_data['method']="getOrderInfoList";
            $new_data['method_text']="唯品订单状态信息返回";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../getOrderInfoListLOG.txt');


            Log::INFO('【唯品订单状态信息返回】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【唯品订单状态信息'.$vop_order_no.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');




            $new_data['time_new']=date("Y-m-d H:i:s.u");
            $new_data['res_data']=$e->getReturnMessage();
            $new_data['method']="getOrderInfoList";
            $new_data['method_text']="唯品订单状态信息返回";
            error_log(print_r($new_data,1),3,dirname(__FILE__).'../../getOrderInfoListLOG.txt');




            return $this->error('',$e->getReturnMessage());
        }
    }

    /**
     * 物流轨迹
     * @param $vop_order_no
     * @return array
     */
    public function getOrderTrack($vop_order_no)
    {
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderTrackRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->orderSn = $vop_order_no;//订单编号

            $rtn =$this->service->getOrderTrack($request);
            $rtn = object_to_array($rtn);
            Log::INFO('【唯品物流轨迹返回】'.json_encode($rtn));
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【唯品物流轨迹'.$vop_order_no.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');
            return $this->error('',$e->getReturnMessage());
        }
    }


    /**
     * 获取承运商列表(快递)
     * @return array|mixed|null
     */
    public function getCarrierList()
    {
        $request=new \com\vip\wpc\ospservice\common\request\WpcVopRequest();
        //$request->timestamp=time();
        $request->vopChannelId = $this->appKey;
        $request->userNumber = $this->userNumber;

        $rtn =$this->service->getCarrierList($request);
        $rtn = object_to_array($rtn);
        return $rtn;
    }


    public function address()
    {
        require_once 'addon/vop/src/vipapis/address/AddressServiceClient.php';
        $adress_service = \vipapis\address\AddressServiceClient::getService();
        $rtn = $adress_service->getProvinceWarehouse(\vipapis\address\Is_Show_GAT::SHOW_ALL);
        $rtn = object_to_array($rtn);
        return $rtn;
    }







    /*************************************************************快速调试********************************************************************/

    /**
     * 查询退货进度
     * @return array|mixed|null
     */
    public function getOrderReturnProgress($order_goods_id)
    {
        $order_goods_info = model('order_goods')->getInfo(['order_goods_id' => $order_goods_id,'source' => 2],'order_id');
        if (empty($order_goods_info)) {
            return $this->error('','未找到订单数据');
        }
        $order_info = model('order')->getInfo(['order_id' => $order_goods_info['order_id']],'vop_order_no');
        try {
            $request=new \com\vip\wpc\ospservice\vop\request\WpcOrderReturnRequest();
            //$request->timestamp=time();
            $request->vopChannelId = $this->appKey;
            $request->userNumber = $this->userNumber;
            $request->orderSn = $order_info['vop_order_no'];//订单编号
            $rtn =$this->service->getReturnOrderCreate($request);
            $rtn = object_to_array($rtn);
            return $this->success($rtn);
        } catch (\Osp\Exception\OspException $e) {
            Log::ERROR('【唯品可选退货原因'.$order_goods_id.'返回'.$e->getReturnCode().'--'.$e->getReturnMessage().'】');
            return $this->error('',$e->getReturnMessage());
        }
        
    }











}