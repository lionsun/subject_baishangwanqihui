<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\vop\model;

use app\model\BaseModel;
use addon\vop\model\Vop as VopModel;
use app\model\goods\Goods as GoodsModel;
use app\model\order\Order as OrderModel;
use app\model\order\OrderRefund as OrderRefundModel;
use app\model\system\Cron;
use think\facade\Log;
use think\facade\Db;

/**
 * 商品
 */
class Goods extends BaseModel
{
    /**
     * 唯品在售品牌数据
     * @return array
     */
    public function getBrandList()
    {
        $record = model('sync_vop')->getInfo(['type' => 1]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }
        $model = new VopModel();
        $page_size = 20;
        model("goods_brand")->startTrans();
        try {
            if (empty($record)) {
                $return = $model->getBrandList(1, $page_size);
                model('sync_vop')->add([
                    'total' => $return['totalNum'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 1
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                $return = $model->getBrandList($page, $page_size);
                model('sync_vop')->update(['page' => $page, 'update_time' => time(), 'total' => $return['totalNum']], ['type' => 1]);
            }

            if (empty($return['brandList'])) {
                model('sync_vop')->update(['status' => 1], ['type' => 1]);
                model('sync_vop')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }
            $cron = new Cron();
            foreach ($return['brandList'] as $key => $value) {
                $band_info = model('goods_brand')->getInfo(['brand_name' => $value['brandName']], 'brand_id');
                if (empty($band_info)) {
                    $add_brand = [
                        'brand_name' => $value['brandName'],
                        'image_url' => (string)$value['brandImage'],
                        'create_time' => time(),
                        'source' => 2,
                    ];
                    model('goods_brand')->add($add_brand);
                }

                $ad_info = model('vop_ad')->getInfo(['ad_id' => $value['adId']]);
                if (empty($ad_info)) {
                    //记录ad_id和各个时间
                    model('vop_ad')->add([
                        'ad_id' => $value['adId'],
                        'sell_time_from' => strtotime($value['sellTimeFrom']),
                        'sell_time_to' => strtotime($value['sellTimeTo']),
                        'pre_time' => strtotime($value['preTime']),
                    ]);
                    //添加定时任务：上架，下架
                    if (time() < strtotime($value['sellTimeFrom'])) {
                        $cron->addCron(1, 0, "唯品商品上架", "AdIdToOnGoods", strtotime($value['sellTimeFrom']), $value['adId']);
                        $cron->addCron(1, 0, "唯品商品下架", "AdIdToOffGoods", strtotime($value['sellTimeTo']), $value['adId']);
                    } elseif (strtotime($value['sellTimeFrom']) <= time() &&  time() <= strtotime($value['sellTimeTo'])) {
                        $cron->addCron(1, 0, "唯品商品下架", "AdIdToOffGoods", strtotime($value['sellTimeTo']), $value['adId']);
                    }
                }
            }

            model('goods_brand')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods_brand')->rollback();
            return $this->error('', $e->getMessage());
        }
    }



    /**
     * 预售品牌
     * @return array
     */
    public function getPreBrandList()
    {
        $record = model('sync_vop')->getInfo(['type' => 2]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        $model = new VopModel();

        $page_size = 20;
        model("goods_brand")->startTrans();
        try {
            if (empty($record)) {
                $return = $model->getPreBrandList(1, $page_size);
                model('sync_vop')->add([
                    'total' => $return['totalNum'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 2
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                $return = $model->getPreBrandList($page, $page_size);
                model('sync_vop')->update(['page' => $page, 'update_time' => time(), 'total' => $return['totalNum']], ['type' => 2]);
            }
            if (empty($return['brandList'])) {
                model('sync_vop')->update(['status' => 1], ['type' => 2]);
                model('sync_vop')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }
            if(!empty($return)){
                $data = array(
                    "time" => time(),
                    'relate_id' => 1,
                    "methods" => "getPreBrandList",
                    "name"=>"预售品牌数据保存",
                    "message"=>json_encode($return)
                );
                model('interface_detection')->add($data);
            }
            $cron = new Cron();
            foreach ($return['brandList'] as $key => $value) {
                $band_info = model('goods_brand')->getInfo(['brand_name' => $value['brandName']], 'brand_id');
                if (empty($band_info)) {
                    $add_brand = [
                        'brand_name' => $value['brandName'],
                        'image_url' => (string)$value['brandImage'],
                        'create_time' => time(),
                        'source' => 2,
                    ];
                    model('goods_brand')->add($add_brand);
                }

                $ad_info = model('vop_ad_pre')->getInfo(['ad_id' => $value['adId']]);
                if (empty($ad_info)) {
                    //记录ad_id和各个时间
                    model('vop_ad_pre')->add([
                        'ad_id' => $value['adId'],
                        'sell_time_from' => strtotime($value['sellTimeFrom']),
                        'sell_time_to' => strtotime($value['sellTimeTo']),
                        'pre_time' => strtotime($value['preTime']),
                    ]);
                    //添加定时任务：上架，下架
                    if (time() < strtotime($value['sellTimeFrom'])) {
                        $cron->addCron(1, 0, "唯品商品上架", "AdIdToOnGoods", strtotime($value['sellTimeFrom']), $value['adId']);
                        $cron->addCron(1, 0, "唯品商品下架", "AdIdToOffGoods", strtotime($value['sellTimeTo']), $value['adId']);
                    } elseif (strtotime($value['sellTimeFrom']) <= time() &&  time() <= strtotime($value['sellTimeTo'])){
                        $cron->addCron(1, 0, "唯品商品下架", "AdIdToOffGoods", strtotime($value['sellTimeTo']), $value['adId']);
                    }
                }

                $vop_ad_pre_log_info = model('vop_ad_pre_log')->getInfo(['ad_id' => $value['adId']]);
                if(empty($vop_ad_pre_log_info)){
                    //补丁方法
                    //记录ad_id和各个时间
                    model('vop_ad_pre_log')->add([
                        'ad_id' => $value['adId'],
                        'sell_time_from' => strtotime($value['sellTimeFrom']),
                        'sell_time_from_text' => $value['sellTimeFrom'],
                        'sell_time_to' => strtotime($value['sellTimeTo']),
                        'sell_time_to_text' =>$value['sellTimeTo'],
                        'pre_time' => strtotime($value['preTime']),
                        'pre_time_text' => $value['preTime'],
                        "create_time"=> time(),
                    ]);
                }



            }
            model('goods_brand')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods_brand')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 唯品商品数据
     * @return array
     */
    public function getGoodsList()
    {
        $record = model('sync_vop')->getInfo(['type' => 3]);
        $vop_ad_info = model('vop_ad')->getInfo([['is_sync', '=', 0]], 'ad_id');
        if ($record['status'] == 1 && empty($vop_ad_info)) {
            model('vop_ad')->update(['is_sync' => 0],[['ad_id','<>', 0]]);
            model('sync_vop')->update(['page' => 0,'status' => 0],['type' => 3]);
            return $this->error('', '商品数据已经全部导完');
        }

        $model = new VopModel();
        $page_size = 10;
        model("goods")->startTrans();
        try {
            //上一个品牌的数据导完，将status改回0，更新下一个品牌的ad_id
            if ($record['status'] == 1 && !empty($vop_ad_info)) {
                model('sync_vop')->update([
                    'ad_id' => $vop_ad_info['ad_id'],
                    'page' => 0,
                    'status' => 0,
                ], ['type' => 3]);
                $record['page'] = 0;
                $record['ad_id'] = $vop_ad_info['ad_id'];
            }

            //$vop_ad_info['ad_id'] = 51143;

            if (empty($record)) {
                model('sync_vop')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 3,
                    'ad_id' => $vop_ad_info['ad_id']
                ]);
                $page = 1;
                $ad_id = $vop_ad_info['ad_id'];
            } else {
                $page = $record['page'] + 1;
                model('sync_vop')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
                $ad_id = $record['ad_id'];
            }
            try {
                $goods_list = $model->getGoodsList($page, $page_size, $ad_id);
            } catch (\Osp\Exception\OspException $e) {
                if ($e->getReturnCode() == 600002) {
                    model('vop_ad')->delete(['ad_id' => $ad_id]);

                    //更改同步候鸟状态
                    model('sync_vop')->update(['status' => 1], ['type' => 3]);

                    model('sync_vop')->commit();
                    model('vop_ad')->commit();
                    return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
                }
            }
            if (!isset($goods_list['goods']) || empty($goods_list['goods'])) {
                //更改品牌同步状态
                model('vop_ad')->update(['is_sync' => 1],['ad_id' => $ad_id]);

                //更改同步候鸟状态
                model('sync_vop')->update(['status' => 1], ['type' => 3]);

                model('sync_vop')->commit();
                model('vop_ad')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }
            foreach ($goods_list['goods'] as $key => $value) {
                $goods_info =  model('goods')->getInfo(['vop_goods_id' => $value['goodFullId']],'goods_id');
                $goods_detail = $model->getGoodsDetail($value['goodFullId'])[0];
                $base_data = $value;
                $base_data['catIdOne'] = $goods_detail['catIdOne'];
                $base_data['catIdTwo'] = $goods_detail['catIdTwo'];
                $base_data['catIdThree'] = $goods_detail['catIdThree'];
                $base_data['ad_id'] = $ad_id;
                if (!empty($goods_info)) {
                    //编辑商品
                    $res = $this->editOneGoods($base_data);
                } else {
                    //添加商品
                    $res = $this->addOneGoods($base_data);
                }
                if ($res['code'] < 0) {
                    Log::ERROR('【唯品报错】');
                    Log::ERROR($value['goodFullId']);
                    Log::ERROR($res);
                }
            }
            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 唯品分类统计
     * @return array
     */
    /*    public function category()
        {
            $record = model('sync_vop')->getInfo(['type' => 2]);
            $brand_info = model('goods_brand_vop')->getInfo([['is_sync', '=', 0]], 'ad_id');
            if ($record['status'] == 1 && empty($brand_info)) {
                return $this->error('', '商品数据已经全部导完');
            }

            $model = new VopModel();
            $page_size = 20;

            //上一个品牌的数据导完，将status改回0，更新下一个品牌的ad_id
            if ($record['status'] == 1 && !empty($brand_info)) {
                model('sync_vop')->update(['status' => 0, 'ad_id' => $brand_info['ad_id'], 'page' => 0], ['type' => 2]);
                $record['page'] = 0 ;
                $record['ad_id'] = $brand_info['ad_id'];
            }

            model("category_vop")->startTrans();
            try {
                if (empty($record)) {
                    model('sync_vop')->add([
                        'total' => 0,
                        'page' => 1,
                        'page_size' => $page_size,
                        'type' => 2,
                        'ad_id' => $brand_info['ad_id']
                    ]);
                    $page = 1;
                    $ad_id = $brand_info['ad_id'];
                } else {
                    $page = $record['page'] + 1;
                    model('sync_vop')->update(['page' => $page, 'update_time' => time()], ['type' => 2]);
                    $ad_id = $record['ad_id'];
                }
                $goods_list = $model->getGoodsList($page, $page_size, $ad_id);
                //dump($goods_list['totalNum']);

                if (empty($goods_list['goods'])) {
                    //更改品牌同步状态
                    model('goods_brand_vop')->update(['is_sync' => 1], ['ad_id' => $ad_id]);
                    //更改同步候鸟状态
                    model('sync_vop')->update(['status' => 1], ['type' => 2]);

                    model('sync_vop')->commit();
                    model('goods_brand_vop')->commit();
                    return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
                }
                $goodFullIds = [];
                foreach ($goods_list['goods'] as $key => $value) {
                    //分类统计
                    $goodFullIds[] =  $value['goodFullId'];

                }
                //dump($goodFullIds);die;
                //分类统计
                if (!empty($goodFullIds)) {
                    $detail = $this->goodsDetail(implode(',',$goodFullIds));
                    $add = [];
                    foreach ($detail as $key => $value) {
                        $category_vop_info = model('category_vop')->getInfo(['cate_id_1' => $value['catIdOne'], 'cate_id_2' => $value['catIdTwo'], 'cate_id_3' => $value['catIdThree']]);
                        if (empty($category_vop_info)) {
                            $add[] = [
                                'cate_id_1' => $value['catIdOne'],
                                'cate_id_2' => $value['catIdTwo'],
                                'cate_id_3' => $value['catIdThree'],
                                'cate_name_1' => $value['catNameOne'],
                                'cate_name_2' => $value['catNameTwo'],
                                'cate_name_3' => $value['catNameThree'],
                            ];
                        }
                    }
                    //去重
                    //dump($add);
                    $add = $this->assoc_unique($add, 'cate_id_3');
                    //dump($add);
                    model('category_vop')->addList($add);
                }

                model('category_vop')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size]);
            } catch (\Exception $e) {
                model('category_vop')->rollback();
                return $this->error('', $e->getMessage());
            }

        }*/

    //二维数组去重
    function assoc_unique($arr, $key) {
        $tmp_arr = array();
        foreach ($arr as $k => $v) {
            if (in_array($v[$key], $tmp_arr)) {//搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true
                unset($arr[$k]);
            } else {
                $tmp_arr[] = $v[$key];
            }
        }
        sort($arr); //sort函数对数组进行排序
        return $arr;

    }

    public function getPreGoodsList()
    {
        $record = model('sync_vop')->getInfo(['type' => 4]);
        //$vop_ad_info = model('vop_ad_pre')->getInfo([['is_sync', '=', 0]], 'ad_id');
        //        if ($record['status'] == 1 && empty($vop_ad_info)) {
        //            model('vop_ad_pre')->update(['is_sync' => 0], [['ad_id', '<>', 0]]);
        //            model('sync_vop')->update(['page' => 0, 'status' => 0], ['type' => 4]);
        //            return $this->error('', '商品数据已经全部导完');
        //        }

        $vop_ad_info = model('vop_ad_pre_log')->getInfo([['is_sync', '=', 0]], 'ad_id');
        if ($record['status'] == 1 && empty($vop_ad_info)) {
            //model('vop_ad_pre_log')->update(['is_sync' => 0], [['ad_id', '<>', 0]]);
            model('sync_vop')->update(['page' => 0, 'status' => 0], ['type' => 4]);
            return $this->error('', '商品数据已经全部导完');
        }

        $new_data=[];
        $model = new VopModel();
        $page_size = 10;
        model("goods")->startTrans();
        try {
            //上一个品牌的数据导完，将status改回0，更新下一个品牌的ad_id
            if ($record['status'] == 1 && !empty($vop_ad_info)) {
                model('sync_vop')->update(['status' => 0, 'ad_id' => $vop_ad_info['ad_id'], 'page' => 0], ['type' => 4]);
                $record['ad_id'] = $vop_ad_info['ad_id'];
                $record['page'] = 0;
            }

            if (empty($record)) {
                model('sync_vop')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 4,
                    'ad_id' => $vop_ad_info['ad_id']
                ]);
                $page = 1;
                $ad_id = $vop_ad_info['ad_id'];
            } else {
                $page = $record['page'] + 1;
                model('sync_vop')->update(['page' => $page, 'update_time' => time()], ['type' => 4]);
                $ad_id = $record['ad_id'];
            }






            try {
                $goods_list = $model->getPreGoodsList($page, $page_size, $ad_id);
            } catch (\Osp\Exception\OspException $e) {
                if ($e->getReturnCode() == 600002) {
                    model('vop_ad_pre')->delete(['ad_id' => $ad_id]);


                    model('vop_ad_pre_log')->update(['is_sync' => 1], [['ad_id' ,'=',$ad_id]]);

                    //更改同步候鸟状态
                    model('sync_vop')->update(['status' => 1], ['type' => 4]);
                    model('sync_vop')->commit();
                    model('vop_ad_pre')->commit();

                    return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1, 'ad_id' => $ad_id]);
                }
            }

            if (!isset($goods_list['goods']) || empty($goods_list['goods'])) {
                //更改品牌同步状态
                model('vop_ad')->update(['is_sync' => 1],['ad_id' => $ad_id]);
                //更改同步候鸟状态
                model('sync_vop')->update(['status' => 1], ['type' => 4]);

                model('sync_vop')->commit();
                model('vop_ad_pre')->commit();

                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1, 'ad_id' => $ad_id]);

            }
            foreach ($goods_list['goods'] as $key => $value) {
                $goods_info =  model('goods')->getInfo(['vop_goods_id' => $value['goodFullId']],'goods_id');
                $goods_detail = $model->getGoodsDetail($value['goodFullId'])[0];
                $base_data = $value;
                $base_data['catIdOne'] = $goods_detail['catIdOne'];
                $base_data['catIdTwo'] = $goods_detail['catIdTwo'];
                $base_data['catIdThree'] = $goods_detail['catIdThree'];
                $base_data['ad_id'] = $ad_id;


                $new_data['time_new'] = date("Y-m-d H:i:s.u");
                $new_data['res_data'] = $base_data;
                $new_data['method'] = "getPreGoodsList";
                $new_data['method_text'] = "唯品商品数据";
                error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../getPreGoodsListLOG.txt');


                if (!empty($goods_info)) {
                    //编辑商品
                    $res = $this->editOneGoods($base_data);
                } else {
                    //添加商品
                    $res = $this->addOneGoods($base_data);
                }
                $new_data[$value['goodFullId']]=$goods_info;
                if ($res['code'] < 0) {
                    Log::ERROR('【唯品报错】');
                    Log::ERROR($value['goodFullId']);
                    Log::ERROR($res);
                }
            }
            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size,'new_data' => $new_data]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 添加商品
     * @param $base_data
     * @return array
     */
    public function addOneGoods($base_data)
    {
        //dump($base_data);die;

        //商品详情处理  循环拼接html
        $description = '';
        if (!empty($base_data['dcImageURLs'])) {
            $description = '<p>';
            foreach ($base_data['dcImageURLs'] as $key => $image) {
                $description .= '<img src="'.$image.'">';
            }
            $description .= '</p>';
        }

        //商品状态
        if ($base_data['goodOnline'] == 0) {
            $goods_state = 0; //下架
        }else if ($base_data['goodOnline'] == 1) {
            $goods_state = 1; //上架
        }else if ($base_data['goodOnline'] == 2) {
            $goods_state = 2; //预售
        }

        $goods_price = $goods_market_price = $goods_cost_price = 0;

        //分类id
        $category_id = $category_id_1 = $category_id_2 = $category_id_3 = 0;
        $category_name = '';

        //sku处理
        $sku = $base_data['sizes'];
        //$goods_spec_format组合：俩数组，一个是’颜色‘，一个是’尺码‘
        $usec = explode(" ", microtime())[0];
        $msec = round($usec * 1000);
        $spec_id = -(count($sku) - 1 + getdate()['seconds'] + $msec);
        $spec_value_id = -(abs($spec_id) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($sku) + rand(0,100);

        //截取颜色
        $position = mb_strpos($base_data['color'],"：");
        $group_1_spec_value_name = mb_substr($base_data['color'], $position + 1);

        $group_1 = [
            'spec_id' => $spec_id,
            'spec_name' => '颜色',
            'value' => [
                [
                    'spec_id' => $spec_id,
                    'spec_name' => '颜色',
                    'spec_value_id' => $spec_value_id,
                    'spec_value_name' => $group_1_spec_value_name,
                    'image' => ''
                ]
            ],
        ];

        $group_2 = [
            'spec_id' => $spec_id - 1,
            'spec_name' => '尺码',
        ];
        $group_2_value = [];
        foreach ($sku as $key => $value) {
            $spec_value_id = -(abs($spec_id) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($sku) + rand(100,200);
            $group_2_value[$key] = [
                'spec_id' => $spec_id - 1,
                'spec_name' => '尺码',
                'spec_value_id' => $spec_value_id,
                'spec_value_name' => $value['sizeName'],
            ];
        }
        $group_2['value'] = $group_2_value;
        $goods_spec_format = array_merge([$group_1], [$group_2]);

        //主图图片
        $goods_image = $base_data['goodImage'];
        $sku_images_arr = $base_data['goodBigImage'];

        //$goods_sku_data组合
        foreach ($sku as $key => $value) {
            //成本价
            $cost_price = $value['vipshopPrice'];

            //销售价
            if ($base_data['isMp'] == false) {
                $price = $value['vipshopPrice'];
            } else {
                $price = $value['suggestPrice'];
            }
            //市场价
            $market_price = round((float)$price/0.8);

            if ($key == 0) {
                $goods_price = $price;
                $goods_market_price = $market_price;
                $goods_cost_price = $cost_price;
            }

            $goods_sku_data[] = [
                'spec_name' =>  $value['sizeName'],
                'sku_no' => $value['sizeId'],
                'price' => $price,
                'market_price' => $market_price,
                'cost_price' => $cost_price,
                'stock' => 0,
                'weight' => '',
                'volume' => '',
                'max_buy' => $value['buyMaxNum'],
                'min_buy' => $value['buyMinNum'],
                'vop_commission' => $value['commission'],
                'vop_suggest_add_price' => $value['suggestAddPrice'],
                'sku_image' => $goods_image,
                'sku_images' => implode(',',$sku_images_arr),
                'sku_images_arr' => $sku_images_arr,
                'sku_spec_format' => [
                    [
                        'spec_id' => $group_1['spec_id'],
                        'spec_name' => '颜色',
                        'spec_value_id' => $group_1['value'][0]['spec_value_id'],
                        'spec_value_name' => $group_1_spec_value_name,
                        'image' => '',
                    ],
                    [
                        'spec_id' => $group_2['spec_id'],
                        'spec_name' => '尺码',
                        'spec_value_id' => $group_2['value'][$key]['spec_value_id'],
                        'spec_value_name' => $value['sizeName'],
                    ]
                ],
            ];
        }

        //分类
        $category_info = model('wp_cate_ref')->getInfo(['wp3c' => $base_data['catIdThree']],'ns1c,ns2c,ns3c,ns3n');
        if (!empty($category_info)) {
            $category_id = $category_info['ns3c'];
            $category_id_1 = $category_info['ns1c'];
            $category_id_2 = $category_info['ns2c'];
            $category_id_3 = $category_info['ns3c'];
            $category_name = $category_info['ns3n'];
        }

        //品牌id
        $brand_info = null;
        $brand_name = '';
        if (!empty($base_data['brandCnName'])) {
            $brand_name = $base_data['brandCnName'];
            $brand_info = model('goods_brand')->getInfo(['brand_name' => $base_data['brandCnName']],'brand_id');
        }elseif(!empty($base_data['brandEnName'])){
            $brand_name = $base_data['brandEnName'];
            $brand_info = model('goods_brand')->getInfo(['brand_name' => $base_data['brandEnName']],'brand_id');
        }

        $brand_id = 0;
        if (empty($brand_info) && !empty($brand_name)){
            $brand_id = model('goods_brand')->add([
                'brand_name' => $brand_name,
                'image_url' => (string)$base_data['logo'],
                'create_time' => time(),
                'source' => 2
            ]);
        }

        $data = [
            'goods_name' => $base_data['goodName'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => 15,
            'website_id' => 0,
            'category_id' => $category_id,
            'category_id_1' => $category_id_1,
            'category_id_2' => $category_id_2,
            'category_id_3' => $category_id_3,
            'category_name' => $category_name,
            'brand_id' => !empty($brand_info) ? $brand_info['brand_id'] : $brand_id,
            'brand_name' => $brand_name,
            'goods_image' => $goods_image,
            'goods_content' => $description,
            'goods_state' => $goods_state,
            'price' => $goods_price,
            'market_price' => $goods_market_price,
            'cost_price' => $goods_cost_price,
            'volume' => 0,
            'goods_stock' => 0,
            'goods_stock_alarm' => 0,
            'is_free_shipping' => 1,
            'shipping_template' => 0,
            'sort' => 0,
            'commission_rate' => 0,
            'video_url' => '',
            'supplier_id' => 0,
            'source' => 2,
            'max_buy' => !empty($sku)?$sku[0]['buyMaxNum'] : 0,
            'min_buy' => !empty($sku)?$sku[0]['buyMinNum'] : 0,
            'goods_attr_format' => '',
            'introduction' => '',
            'goods_shop_category_ids' => '',
            'unit' => '',
            'keywords' => '',
            'sku_no' => !empty($sku) ? $sku[0]['sizeId'] : '',
            'weight' => '',

            'goods_spec_format' => !empty($goods_spec_format) ? json_encode($goods_spec_format, JSON_UNESCAPED_UNICODE) : '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',

            'vop_goods_id' => $base_data['goodFullId'],
            'ad_id' => $base_data['ad_id'],  //广告id
            'is_mp' => (int)$base_data['isMp'], //是否MP商品
            'vop_sn'=> $base_data['sn'],//货号
        ];

        $goods_model = new GoodsModel();
        $res = $goods_model->addGoods($data);

        if ($res['code'] < 0) {
            return $this->error($base_data['sku']);
        }
        return $this->success();
    }

    /**
     * 编辑商品
     * @param $base_data
     * @return array
     */
    public function editOneGoods($base_data)
    {
        $goods_info = model('goods')->getInfo(['vop_goods_id' => $base_data['goodFullId']]);
        if (empty($goods_info)) {
            return $this->error('','未找到商品数据');
        }

        $goods_sku = model('goods_sku')->getList(['goods_id' => $goods_info['goods_id']]);

        //商品详情处理  循环拼接html
        $description = '';
        if (!empty($base_data['dcImageURLs'])) {
            $description = '<p>';
            foreach ($base_data['dcImageURLs'] as $key => $image) {
                $description .= '<img src="'.$image.'">';
            }
            $description .= '</p>';
        }


        //商品状态
        if ($base_data['goodOnline'] == 0) {
            $goods_state = 0; //下架
        }else if ($base_data['goodOnline'] == 1) {
            $goods_state = 1; //上架
        }else if ($base_data['goodOnline'] == 2) {
            $goods_state = 2; //预售
        }

        $old_goods_spec_format = json_decode($goods_info['goods_spec_format'],true);
        $old_group_1 = $old_goods_spec_format[0];
        $old_group_2 = $old_goods_spec_format[1];


        //sku处理
        $sku = $base_data['sizes'];
        //$goods_spec_format组合：俩数组，一个是’颜色‘，一个是’尺码‘
        //截取颜色
        $position = mb_strpos($base_data['color'],"：");
        $group_1_spec_value_name = mb_substr($base_data['color'], $position + 1);

        $group_1 = [
            'spec_id' => $old_group_1['spec_id'],
            'spec_name' => '颜色',
            'value' => [
                [
                    'spec_id' => $old_group_1['spec_id'],
                    'spec_name' => '颜色',
                    'spec_value_id' => $old_group_1['value'][0]['spec_value_id'],
                    'spec_value_name' => $group_1_spec_value_name,
                    'image' => ''
                ]
            ],
        ];


        //已经保存的所有唯品sku_no=>sku_id
        $sku_no_olds =[];
        foreach ($goods_sku as $key => $value) {
            if (!empty($value['sku_no'])) {
                $sku_no_olds[$value['sku_no']] = $value['sku_id'];
            }
        }
        //所有的规格值的spec_value_id
        $spec_value_old_ids = [];
        foreach ($old_group_2['value'] as $key => $value) {
            $spec_value_old_ids[] = $value['spec_value_id'];
        }
        $verify_state = 1;
        if (count($sku) == count($sku_no_olds)) {
            $spec_value_ids = $spec_value_old_ids;
            //增加了
        }elseif(count($sku) > count($sku_no_olds)){
            $inc_num = count($sku) - count($sku_no_olds);
            $new_spec_value_id = [];
            for($i=0;$i< $inc_num;$i++){
                $new_spec_value_id[] = -(abs($old_group_2['spec_id']) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + $i + rand(100,200);
            }
            $spec_value_ids = array_merge($spec_value_old_ids, $new_spec_value_id);
            $verify_state = 0;
        }else{
            //减少了
            $spec_value_ids = array_slice($spec_value_old_ids, 0, count($sku));
            $verify_state = 0;
        }


        $group_2 = [
            'spec_id' => $old_group_2['spec_id'],
            'spec_name' => '尺码',
        ];
        $group_2_value = [];
        foreach ($sku as $key => $value) {
            $group_2_value[$key] = [
                'spec_id' => $old_group_2['spec_id'],
                'spec_name' => '尺码',
                'spec_value_id' => $spec_value_ids[$key],
                'spec_value_name' => $value['sizeName'],
            ];
        }
        $group_2['value'] = $group_2_value;
        $goods_spec_format = array_merge([$group_1], [$group_2]);



        $goods_price = $goods_info['price'];
        $goods_market_price = $goods_info['market_price'];
        $goods_cost_price = $goods_info['cost_price'];
        $goods_image = $base_data['goodImage'];
        $sku_images_arr = $base_data['goodBigImage'];
        //用到之前的sku_id
        $use_sku_ids = [];
        foreach ($sku as $key => $value) {
            //成本价
            $cost_price = $value['vipshopPrice'];
            if ($base_data['isMp'] == false) {
                $price = $value['vipshopPrice'];
            } else {
                $premium = 0;
                if ((float)$goods_info['premium'] != 0) {
                    $premium = (float)$goods_info['premium'];
                }

                if ($premium != 0) {
                    //销售价
                    $price =  round((float)$value['suggestPrice']*(($premium-1)*$premium/10/1.13+$premium));
                } else {
                    $price = $value['suggestPrice'];
                }
            }
            //市场价
            $market_price = round((float)$price/0.8);

            if ($key == 0) {
                $goods_price = $price;
                $goods_market_price = $market_price;
                $goods_cost_price = $cost_price;
            }


            //sku有新增
            $goods_sku_info = '';
            if (!isset($sku_no_olds[$value['sizeId']])) {
                $sku_id = model('goods_sku')->add([
                    'sku_no' => $value['sizeId'],
                    'goods_id' => $goods_info['goods_id']
                ]);
            } else {
                $sku_id = $sku_no_olds[$value['sizeId']];
                array_push($use_sku_ids, $sku_id);
                $goods_sku_info = model('goods_sku')->getInfo(['sku_id' => $sku_id],'sku_image,sku_images');
            }

            $goods_sku_data[] = [
                'sku_id' => $sku_id,
                'spec_name' =>  $value['sizeName'],
                'sku_no' => $value['sizeId'],
                'price' => $price,
                'market_price' => $market_price,
                'cost_price' => $cost_price,
                'stock' => 0,
                'weight' => '',
                'volume' => '',
                'max_buy' => $value['buyMaxNum'],
                'min_buy' => $value['buyMinNum'],
                'vop_commission' => $value['commission'],
                'vop_suggest_add_price' => $value['suggestAddPrice'],
                'sku_image' => !empty($goods_sku_info) && !empty($goods_sku_info['sku_image'])? $goods_sku_info['sku_image'] : $goods_image,
                'sku_images' => !empty($goods_sku_info) && !empty($goods_sku_info['sku_images'])? $goods_sku_info['sku_images'] : implode(',',$sku_images_arr),
                'sku_images_arr' => !empty($goods_sku_info) && !empty($goods_sku_info['sku_images'])? explode(',',$goods_sku_info['sku_images']) : $sku_images_arr,
                'sku_spec_format' => [
                    [
                        'spec_id' => $group_1['spec_id'],
                        'spec_name' => '颜色',
                        'spec_value_id' => $group_1['value'][0]['spec_value_id'],
                        'spec_value_name' => $group_1_spec_value_name,
                        'image' => '',
                    ],
                    [
                        'spec_id' => $group_2['spec_id'],
                        'spec_name' => '尺码',
                        'spec_value_id' => $group_2['value'][$key]['spec_value_id'],
                        'spec_value_name' => $value['sizeName'],
                    ]
                ],
            ];
        }


        //删除多余的sku数据  用到的$use_sku_ids和之前所有sku_ids比较，看看是否有没有用到的，有就删除
        $old_sku_ids = array_column($goods_sku,'sku_id');
        $diff = array_diff($old_sku_ids, $use_sku_ids);
        if (!empty($diff)) {
            model('goods_sku')->delete([['sku_id','in',$diff]]);
        }

        //品牌id
        if (!empty($base_data['brandCnName'])) {
            $brand_info = model('goods_brand')->getInfo(['brand_name' => $base_data['brandCnName']]);
        }else{
            $brand_info = model('goods_brand')->getInfo(['brand_name' => $base_data['brandEnName']]);
        }

        $data = [
            'goods_id' => $goods_info['goods_id'],
            'goods_name' => $base_data['goodName'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => 15,
            'website_id' => 0,

            'category_id' => $goods_info['category_id'],
            'category_id_1' => $goods_info['category_id_1'],
            'category_id_2' => $goods_info['category_id_2'],
            'category_id_3' => $goods_info['category_id_3'],
            'category_name' => $goods_info['category_name'],

            'brand_id' => !empty($brand_info) ? $brand_info['brand_id'] : 0,
            'brand_name' => !empty($brand_info) ? $brand_info['brand_name'] : '',
            'goods_image' => $goods_info['goods_image'],
            'goods_content' => $description,

            'goods_state' => $goods_state,
            'price' => $goods_price,
            'market_price' => $goods_market_price,
            'cost_price' => $goods_cost_price,

            'sku_no' => !empty($sku) ? $sku[0]['sizeId'] : '',
            'weight' => '',
            'volume' => 0,
            'goods_stock' => 0,
            'goods_stock_alarm' => 0,
            'is_free_shipping' => 1,
            'shipping_template' => 0,
            'goods_spec_format' => !empty($goods_spec_format) ? json_encode($goods_spec_format, JSON_UNESCAPED_UNICODE) : '',
            'goods_attr_format' => '',
            'introduction' => '',
            'keywords' => '',
            'unit' => '',
            'sort' => 0,
            'commission_rate' => 0,
            'video_url' => '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',
            'goods_shop_category_ids' => '',
            'supplier_id' => 0,
            'source' => 2,
            'premium' => $goods_info['premium'],
            'verify_state' => $verify_state,

            'max_buy' => !empty($sku)?$sku[0]['buyMaxNum'] : 0,
            'min_buy' => !empty($sku)?$sku[0]['buyMinNum'] : 0,

            'ad_id' => $base_data['ad_id'],  //广告id
            'is_mp' => (int)$base_data['isMp'], //是否MP商品
        ];

        $goods_model = new GoodsModel();
        $res = $goods_model->editGoods($data, 1);
        if ($res['code'] < 0) {
            return $this->error();
        }
        return $this->success();


    }


    public function goodsDetail($goodFullId)
    {
        $model = new VopModel();
        $res = $model->getGoodsDetail($goodFullId);
        return $res;
    }

    //添加定时任务,根据ad_id定时上架
    public function adIdToOnGoods($ad_id)
    {
        $goods_ids = model('vop_ad_goods')->getColumn(['ad_id' => $ad_id],'goods_id');
        if (!empty($goods_ids)) {
            model('goods')->update(['goods_state' => 1, 'verify_state' => 0],[['goods_id','in',$goods_ids]]);
            model('goods_sku')->update(['goods_state' => 1, 'verify_state' => 0],[['goods_id','in',$goods_ids]]);
        }
        return $this->success();
    }

    //添加定时任务,根据ad_id定时下架
    public function adIdToOffGoods($ad_id)
    {
        $goods_ids = model('vop_ad_goods')->getColumn(['ad_id' => $ad_id],'goods_id');
        if (!empty($goods_ids)) {
            model('goods')->update(['goods_state' => 0, 'verify_state' => 0],[['goods_id','in',$goods_ids]]);
            model('goods_sku')->update(['goods_state' => 0, 'verify_state' => 0],[['goods_id','in',$goods_ids]]);
            model('vop_ad_goods')->delete(['ad_id' => $ad_id]);
            model('vop_ad')->delete(['ad_id' => $ad_id]);
        }
        return $this->success();
    }


    /**
     * 唯品商品单独获取状态和库存
     * @param $sku_id
     * @return array
     */
    public function getVopGoodsInfo($sku_id)
    {
        //库存
        $sku_info = model('goods_sku')->getInfo(['sku_id' => $sku_id, 'source' => 2],'sku_no');
        if (empty($sku_info)) {
            return $this->error('','未找到商品数据');
        }
        $vop_model = new VopModel();
        $status_return  = $vop_model->getSizeStatus($sku_info['sku_no']);

        return $this->success([
            'stock' => $status_return['list'][0]['stock'],
            'max_buy' => $status_return['list'][0]['buyMaxNum'],
            'min_buy' => $status_return['list'][0]['buyMinNum'],
            'status' => $status_return['list'][0]['goodOnline'],
        ]);
    }

    public function categoryDeal()
    {
        $record = model('sync_vop')->getInfo(['type' => 6]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("wp_cate_ref")->startTrans();
        try {
            $page_size = 20;
            if (empty($record)) {
                model('sync_vop')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 6
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_vop')->update(['page' => $page, 'update_time' => time()], ['type' => 6]);
            }
            $deal_data = model('wp_cate_ref')->pageList(['ns1c' => null], '*', 'id asc', 1, 20);

            if (empty($deal_data['list'])) {
                $res = model('sync_vop')->update(['status' => 1], ['type' => 6]);
                model('sync_vop')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }

            foreach ($deal_data['list'] as $key => $value) {
                $info = model('goods_category')->getInfo(['category_id' => $value['ns2c']],'pid');
                $pid_info = model('goods_category')->getInfo(['category_id' => $info['pid']],'category_id,category_name');
                model('wp_cate_ref')->update(['ns1c' => $pid_info['category_id'],'ns1n' => $pid_info['category_name']],['ns2c' => $value['ns2c']]);
            }

            model('wp_cate_ref')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('wp_cate_ref')->rollback();
            return $this->error('', $e->getMessage());
        }
    }





    /**
     * 定时任务，更新发货状态
     * @return array
     */
    public function orderDeliveryStatus()
    {
        $list = model('order')->getList(['order_status' => 1,'pay_status' => 1 ,'source' => 2],'order_id,vop_order_no,site_id');
        if (!empty($list)) {
            $vop_model = new VopModel();
            $order_model = new OrderModel();
            foreach ($list as $key => $value) {
                $return  = $vop_model->getOrderInfoList($value['vop_order_no']);
                //statusCode  = 5
                //订单支付后唯品会取消订单
                if ($return['code'] >= 0 && $return['data'][0]['childOrderSnList'][0]['statusCode'] == 3) {
                    $order_goods_ids = model('order_goods')->getColumn(['order_id' => $value['order_id']],'order_goods_id');
                    //发货
                    $data = array(
                        "type" => 'manual',//发货方式（手动发货）
                        "order_goods_ids" => implode(',',$order_goods_ids),
                        "express_company_id" => 0,
                        "delivery_no" => $return['data'][0]['childOrderSnList'][0]['transportNo'],
                        "order_id" => $value['order_id'],
                        "delivery_type" => 1,
                        "site_id" => $value['site_id'],
                        "template_id" => 0//电子面单模板id
                    );
                    $result = $order_model->orderGoodsDelivery($data);
                    model('express_delivery_package')->update(['express_company_name' => $return['data'][0]['childOrderSnList'][0]['transportName']],['order_id' => $value['order_id']]);
                }
            }
        }
        return $this->success();
    }

    /**
     * 定时任务更新退货处理状态
     * @return array
     */
    public function orderRefundStatus(){
        $list = model('order_goods')->getList(['refund_type' => 1,'refund_status' => 1,'source' => 2],'order_id,site_id,order_goods_id');
        if (!empty($list)) {
            $vop_model = new VopModel();
            foreach ($list as $key => $value) {
                $order_info = model('order')->getInfo(['order_id' => $value['order_id']],'vop_order_no');
                $vop_return = $vop_model->getOrderReturnDetail($order_info['vop_order_no']);


                $new_data['time_new']=date("Y-m-d H:i:s.u");
                $new_data['res_data']=$vop_return;
                $new_data['method']="orderRefundStatus";
                $new_data['method_text']="定时任务更新退货处理状态";
                error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderRefundStatusLOG.txt');


                if ($vop_return['code']==0 && $vop_return['data']['returnStatus'] == '退货已审核') {
                    $order_refund_model = new OrderRefundModel();
                    $data = array(
                        "order_goods_id" => $value['order_goods_id'],
                        'site_id' => $value['site_id'],
                        'is_vop' => 1,
                        'refund_address'=> $vop_return['data']['returnAddressInfo']['address']."-".$vop_return['data']['returnAddressInfo']['name']."--联系电话：".$vop_return['data']['returnAddressInfo']['phone'],
                    );
                    $res = $order_refund_model->orderRefundConfirm($data, ['uid' => 0,'username' => '唯品']);
                }
            }
        }
        return $this->success();

    }



    /**
     * 唯品会承运商列表
     * @return array
     */
    public function carrierList(){
        $list = model('vop_carrier')->getList([['cust_no','<>','']]);
        return $this->success($list);
    }



    public function priceDeal()
    {
        $record = model('sync_vop')->getInfo(['type' => 5]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 20;
            if (empty($record)) {
                model('sync_vop')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 5
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_vop')->update(['page' => $page, 'update_time' => time()], ['type' => 5]);
            }
            $deal_data = model('zhai_bug')->pageList([], '*', 'goods_id asc', $page, $page_size);

            if (empty($deal_data['list'])) {
                $res = model('sync_vop')->update(['status' => 1], ['type' => 5]);
                model('sync_vop')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            $mapping = [];
            foreach ($deal_data['list'] as $key => $value) {
                $mapping[$value['goods_sku']] = $value['goods_id'];
            }

            $model = new VopModel();
            $goods_skus = implode(',',array_column($deal_data['list'],'goods_sku'));
            $details = $model->getGoodsDetail($goods_skus);

            $lists = [];
            foreach ($details as $key => $detail) {
                $lists[$mapping[$detail['goodFullId']]] = $detail;
            }
            foreach ($lists as $key => $value) {
                foreach ($value['sizes'] as $k => $v) {
                    $sku = model('goods_sku')->getInfo(['goods_id' => $key, 'sku_no' => $v['sizeId']], 'sku_id');

                    //成本价
                    $cost_price = $v['vipshopPrice'];

                    //销售价
                    if ($value['isMp'] == false) {
                        $price = $v['vipshopPrice'];
                    } else {
                        $price = $v['suggestPrice'];
                    }
                    //市场价
                    $market_price = round((float)$price/0.8);

                    if ($k == 0) {
                        $goods_price = $price;
                        $goods_market_price = $market_price;
                        $goods_cost_price = $cost_price;
                    }

                    model('goods_sku')->update(['price' => $price, 'market_price' => $market_price, 'discount_price' => $price, 'vop_commission' => $v['commission'], 'vop_suggest_add_price' => $v['suggestAddPrice']], ['sku_id' => $sku['sku_id']]);
                }
                if (isset($goods_price) && isset($goods_market_price) && isset($goods_cost_price)) {
                    model('goods')->update(['price' => $goods_price, 'market_price' => $goods_market_price, 'cost_price' => $goods_cost_price], ['goods_id' => $key]);
                }
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }



    /*******************************************唯品会 补充方法 开始**********************************************************************/


    /**
     *
     *退货申请处理  (判断维代购 是否接收到货物 )
     */
    public function orderRefundStatusTheGoods()
    {
        $list = model('order_goods')->getList(['refund_status' => 5, 'source' => 2], 'order_id,site_id,order_goods_id');
        $res=[];
        if (!empty($list)) {
            $vop_model = new VopModel();
            foreach ($list as $key => $value) {
                $order_info = model('order')->getInfo(['order_id' => $value['order_id']], 'vop_order_no');
                $vop_model = new VopModel();
                $vop_return = $vop_model->getOrderReturnDetail($order_info['vop_order_no']);
                if ($vop_return['code'] == 0 && ($vop_return['data']['returnStatus'] == '1' || $vop_return['data']['returnStatus'] == '已退款')) {
                    $order_refund_model = new OrderRefundModel();
                    $data = array(
                        "order_goods_id" => $value['order_goods_id'],
                        'site_id' => $value['site_id'],
                        "is_refund_stock" => 0,
                    );
                    $new_data['time_new'] = date("Y-m-d H:i:s.u");
                    $new_data['res_data'] = $vop_return;
                    $new_data['res_data2'] = $data;
                    $new_data['method'] = "orderRefundStatusTheGoods";
                    $new_data['method_text'] = "退货申请处理(判断维代购 是否接收到货物)";
                    $res[$value['order_goods_id']]=$data;
                    error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../orderRefundStatusTheGoodsLOG.txt');
                    $res = $order_refund_model->orderRefundTakeDelivery($data, ['uid' => 0, 'username' => '唯品']);
                }
            }
        }
        return $this->success($res);
    }



    /*
     *唯品收到货物 同意后执行  退款维权结束
     */
    public function orderRefundFinish()
    {
        $list = model('order_goods')->getList(['refund_status' => 6, 'source' => 2], 'order_id,site_id,order_goods_id');
        $res=[];
        if (!empty($list)) {
            $vop_model = new VopModel();
            foreach ($list as $key => $value) {
                $order_refund_model = new OrderRefundModel();
                $data = array(
                    "order_goods_id" => $value['order_goods_id'],
                    'site_id' => $value['site_id'],
                );
                $res[$value['order_goods_id']]=$data;
                $new_data['time_new'] = date("Y-m-d H:i:s.u");
                $new_data['res_data'] = $value;
                $new_data['method'] = "orderRefundFinish";
                $new_data['method_text'] = "唯品收到货物 同意后执行  退款维权结束";
                error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../orderRefundFinishLOG.txt');
                $res = $order_refund_model->orderRefundFinish($data, ['uid' => 0, 'username' => '唯品']);
            }
        }
        return $this->success($list);
    }







    /*
     *定时执行商品上下架
     */
    public function detectionGoodsOnline()
    {

        $interface_detection_info = model('interface_detection')->getFirstData([['methods','=','detectionGoodsOnline']],'*',"relate_id desc");
        // var_dump($interface_detection_info);
        // exit;
        $goods_id=0;
        if(!empty($interface_detection_info)){
            $goods_id=$interface_detection_info['relate_id'];
        }
        $condition[] = [ 'goods_state', '=', 0 ];
        $condition[] = [ 'source', '=', 2 ];
        $condition[] = [ 'goods_id', '>', $goods_id ];
        //$condition[] = [ 'is_detection', '=', 0 ];
        $goods_list = model('goods')->pageList($condition, 'goods_state,source,vop_goods_id,goods_id', 'goods_id', 1, 200);
        $res=[];
        if (!empty($goods_list['list'])) {
            $vop_model = new VopModel();
            foreach ($goods_list['list'] as $key => $value) {
                $vop_model = new VopModel();
                $vop_return = $vop_model->getGoodsOnline($value['vop_goods_id']);
                if(!empty($vop_return['goodsList'][0])){
                    if($vop_return['goodsList'][0]['goodOnline']==0){
                        $goods_model = new GoodsModel();
                        $goods_model->modifyGoodsState($value['goods_id'], 0,15);
                        //model('goods')->update(['is_detection' => 1], [['goods_id', '=', $value['goods_id']]]);
                        $data = array(
                            "time" => time(),
                            'relate_id' => $value['goods_id'],
                            "methods" => "detectionGoodsOnline",
                            "name"=>"下架",
                            "message"=>json_encode($vop_return)
                        );
                        model('interface_detection')->add($data);
                        $res[$value['goods_id']]=$value['goods_id'];
                        $new_data['time_new'] = date("Y-m-d H:i:s.u");
                        $new_data['res_data'] = $vop_return;
                        $new_data['method'] = "detectionGoodsOnline";
                        $new_data['method_text'] = "定时执行商品上下架";
                        error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../detectionGoodsOnlineLOG.txt');
                    }else if($vop_return['goodsList'][0]['goodOnline']==1){
                        $data = array(
                            "time" => time(),
                            'relate_id' => $value['goods_id'],
                            "methods" => "detectionGoodsOnline",
                            "name"=>"上架",
                            "message"=>json_encode($vop_return)
                        );
                        $res[$value['goods_id']]=$value['goods_id'];
                        model('interface_detection')->add($data);
                        $goods_model = new GoodsModel();
                        $goods_model->modifyGoodsState($value['goods_id'], 1,15);
                    }
                }
            }
        }
        return $this->success($res);
    }




    /**
     * 补充分类
     */
    public function replenishCategory()
    {

        $condition[] = ['category_id', '=', 0];
        $condition[] = ['source', '=', 2];
        //$condition[] = ['goods_id', '=', 366912];
        $goods_list = model('goods')->pageList($condition, 'goods_state,source,vop_goods_id,goods_id', 'goods_id', 1, 100);
        if(!empty($goods_list['list'])){
            foreach ($goods_list['list'] as $k => $v) {
                $base_data = $this->getGoodsDetail($v['vop_goods_id']);
                $category_data = [];
                if (!empty($base_data)) {
                    $base_data = $base_data[0];
                    //分类
                    $category_info = model('wp_cate_ref')->getInfo(['wp3c' => $base_data['catIdThree']], 'ns1c,ns2c,ns3c,ns3n');
                    if (!empty($category_info)) {
                        $category_data = [
                            'category_id' => $category_info['ns3c'],
                            'category_id_1' => $category_info['ns1c'],
                            'category_id_2' => $category_info['ns2c'],
                            'category_id_3' => $category_info['ns3c'],
                            'category_name' => $category_info['ns3n'],
                        ];
                        model('goods')->update($category_data, [['goods_id', '=', $v['goods_id']]]);
                        model('goods_sku')->update($category_data, [['goods_id', '=', $v['goods_id']]]);
                    }else{

                        $category_vop_info = model('category_vop')->getInfo(['cate_id_1' => $base_data['catIdOne'], 'cate_id_2' => $base_data['catIdTwo'], 'cate_id_3' => $base_data['catIdThree']]);
                        if (empty($category_vop_info)) {
                            $add = [
                                'cate_id_1' => $base_data['catIdOne'],
                                'cate_id_2' => $base_data['catIdTwo'],
                                'cate_id_3' => $base_data['catIdThree'],
                                'cate_name_1' => $base_data['catNameOne'],
                                'cate_name_2' => $base_data['catNameTwo'],
                                'cate_name_3' => $base_data['catNameThree'],
                                'is_sync' => 1,
                            ];
                            model('category_vop')->add($add);
                        }
                        $category_data["empty_data"]=$v['vop_goods_id'];
                    }
                }
                var_dump("<pre>");
                var_dump($category_data);
            }
        }
        var_dump("<pre>");
        exit;
        return $this->success(1);
    }







    /**
     *退货申请处理  (判断维代购 是否接收到货物 )
     * 定向测试
    //    public function orderRefundStatusTheGoods2(){
    //        $list = model('order_goods')->getList(['order_id' => 474,'refund_status' => 6,'source' => 2],'order_id,site_id,order_goods_id');
    //        if (!empty($list)) {
    //            $vop_model = new VopModel();
    //            foreach ($list as $key => $value) {
    //                $order_info = model('order')->getInfo(['order_id' => $value['order_id']],'vop_order_no');
    //                $vop_model = new VopModel();
    //                $vop_return = $vop_model->getOrderReturnDetail($order_info['vop_order_no']);
    //                $new_data['time_new']=date("Y-m-d H:i:s.u");
    //                $new_data['res_data']=$vop_return;
    //                $new_data['method']="orderRefundStatusTheGoods";
    //                $new_data['method_text']="退货申请处理(判断维代购 是否接收到货物)";
    //                error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderRefundStatusTheGoodsLOG2.txt');
    //                if ($vop_return['code']==0 && $vop_return['data']['returnStatus'] == '已退款') {
    //                    $order_refund_model = new OrderRefundModel();
    //                    $data = array(
    //                        "order_goods_id" => $value['order_goods_id'],
    //                        'site_id' => $value['site_id'],
    //                    );
    //                    $res = $order_refund_model->orderRefundTakeDelivery($data, ['uid' => 0,'username' => '唯品']);
    //                }
    //            }
    //        }
    //        return $this->success();
    //    }
    /*******************************************唯品会 补充方法 结束**********************************************************************/












    /**************************************************************************************************************/

    /**
     * 唯品商品数据
     * @return array
     */
    public function getGoodsDetail($goodFullId)
    {
        $model = new VopModel();
        $goods_detail = $model->getGoodsDetail($goodFullId);
        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$goods_detail;
        $new_data['method']="getGoodsDetail";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../getGoodsDetailLOG.txt');
        return  $goods_detail;
    }






    /**
     * 唯品商品数据
     * @return array
     */
    public function getGoodsList2()
    {
        $record = model('sync_vop')->getInfo(['type' => 3]);
        $vop_ad_info = model('vop_ad')->getInfo([['is_sync', '=', 0]], 'ad_id');
        if ($record['status'] == 1 && empty($vop_ad_info)) {
            model('vop_ad')->update(['is_sync' => 0],[['ad_id','<>', 0]]);
            model('sync_vop')->update(['page' => 0,'status' => 0],['type' => 3]);
            return $this->error('', '商品数据已经全部导完');
        }

        $model = new VopModel();
        $page_size = 10;
        model("goods")->startTrans();
        try {
            //上一个品牌的数据导完，将status改回0，更新下一个品牌的ad_id
            if ($record['status'] == 1 && !empty($vop_ad_info)) {
                model('sync_vop')->update([
                    'ad_id' => $vop_ad_info['ad_id'],
                    'page' => 0,
                    'status' => 0,
                ], ['type' => 3]);
                $record['page'] = 0;
                $record['ad_id'] = $vop_ad_info['ad_id'];
            }

            //$vop_ad_info['ad_id'] = 51143;

            if (empty($record)) {
                model('sync_vop')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 3,
                    'ad_id' => $vop_ad_info['ad_id']
                ]);
                $page = 1;
                $ad_id = $vop_ad_info['ad_id'];
            } else {
                $page = $record['page'] + 1;
                model('sync_vop')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
                $ad_id = $record['ad_id'];
            }
            try {
                $goods_list = $model->getGoodsList($page, $page_size, $ad_id);
            } catch (\Osp\Exception\OspException $e) {
                if ($e->getReturnCode() == 600002) {
                    model('vop_ad')->delete(['ad_id' => $ad_id]);

                    //更改同步候鸟状态
                    model('sync_vop')->update(['status' => 1], ['type' => 3]);

                    model('sync_vop')->commit();
                    model('vop_ad')->commit();
                    return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
                }
            }
            if (!isset($goods_list['goods']) || empty($goods_list['goods'])) {
                //更改品牌同步状态
                model('vop_ad')->update(['is_sync' => 1],['ad_id' => $ad_id]);

                //更改同步候鸟状态
                model('sync_vop')->update(['status' => 1], ['type' => 3]);

                model('sync_vop')->commit();
                model('vop_ad')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }



            foreach ($goods_list['goods'] as $key => $value) {
                $goods_info =  model('goods')->getInfo(['vop_goods_id' => $value['goodFullId']],'goods_id');
                $goods_detail = $model->getGoodsDetail($value['goodFullId'])[0];
                $base_data = $value;
                $base_data['catIdOne'] = $goods_detail['catIdOne'];
                $base_data['catIdTwo'] = $goods_detail['catIdTwo'];
                $base_data['catIdThree'] = $goods_detail['catIdThree'];
                $base_data['ad_id'] = $ad_id;
                if (!empty($goods_info)) {
                    //编辑商品
                    $res = $this->editOneGoods($base_data);
                } else {
                    //添加商品
                    $res = $this->addOneGoods($base_data);
                }
                if ($res['code'] < 0) {
                    Log::ERROR('【唯品报错】');
                    Log::ERROR($value['goodFullId']);
                    Log::ERROR($res);
                }
            }
            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }




    /********************************************************快速调试方法*******************************************************/


    /**
     *
     * 定时任务更新退货处理状态  SELECT * FROM `ns_order` WHERE   vop_order_no='21030421392635'
     * @return array
     *
     */
    public function orderRefundStatus2(){
        $list = model('order_goods')->getList(['order_id' => 474,'refund_type' => 1,'refund_status' => 1,'source' => 2],'order_id,site_id,order_goods_id');
        if (!empty($list)) {
            $vop_model = new VopModel();
            foreach ($list as $key => $value) {
                $order_info = model('order')->getInfo(['order_id' => $value['order_id']],'vop_order_no');
                $vop_model = new VopModel();
                $vop_return = $vop_model->getOrderReturnDetail($order_info['vop_order_no']);
                $new_data['time_new']=date("Y-m-d H:i:s.u");
                $new_data['res_data']=$vop_return;
                $new_data['method']="orderRefundStatus";
                $new_data['method_text']="定时任务更新退货处理状态";
                error_log(print_r($new_data,1),3,dirname(__FILE__).'../../orderRefundStatusLOG2.txt');
                if ($vop_return['code']==0 && $vop_return['data']['returnStatus'] == '退货已审核') {
                    $order_refund_model = new OrderRefundModel();
                    $data = array(
                        "order_goods_id" => $value['order_goods_id'],
                        'site_id' => $value['site_id'],
                        'is_vop' => 1,
                        'refund_address'=> $vop_return['data']['returnAddressInfo']['address']."-".$vop_return['data']['returnAddressInfo']['name']."--联系电话：".$vop_return['data']['returnAddressInfo']['phone'],
                    );
                    $res = $order_refund_model->orderRefundConfirm2($data, ['uid' => 0,'username' => '唯品']);
                }
            }
        }
        return $this->success();
    }






    /**
     * 定时任务，更新发货状态   快速调试
     * @return array
     */
    public function orderDeliveryStatus2()
    {
        $list = model('order')->getList(['order_id' => 474,'order_status' => 1,'pay_status' => 1 ,'source' => 2],'order_id,vop_order_no,site_id');
        if (!empty($list)) {
            $vop_model = new VopModel();
            $order_model = new OrderModel();
            foreach ($list as $key => $value) {
                $return  = $vop_model->getOrderInfoList($value['vop_order_no']);
                //statusCode  = 5
                //订单支付后唯品会取消订单
                if ($return['code'] >= 0 && $return['data'][0]['childOrderSnList'][0]['statusCode'] == 3) {
                    $order_goods_ids = model('order_goods')->getColumn(['order_id' => $value['order_id']],'order_goods_id');
                    //发货
                    $data = array(
                        "type" => 'manual',//发货方式（手动发货）
                        "order_goods_ids" => implode(',',$order_goods_ids),
                        "express_company_id" => 0,
                        "delivery_no" => $return['data'][0]['childOrderSnList'][0]['transportNo'],
                        "order_id" => $value['order_id'],
                        "delivery_type" => 1,
                        "site_id" => $value['site_id'],
                        "template_id" => 0//电子面单模板id
                    );
                    $result = $order_model->orderGoodsDelivery($data);
                    model('express_delivery_package')->update(['express_company_name' => $return['data'][0]['childOrderSnList'][0]['transportName']],['order_id' => $value['order_id']]);
                }
            }
        }
        return $this->success();
    }








    /**
     * 修改删除状态  vop_goods_id
     * @param $goods_ids
     * @param $is_delete
     * @param $site_id
     */
    public function modifyIsDelete()
    {
        $res=Db::query("SELECT    goods_id ,COUNT(vop_goods_id) AS b FROM   ns_goods  WHERE source=2  GROUP BY vop_goods_id   HAVING b>1
");
        $goods_ids="";
        if(!empty($res)){
            $arr2 = array_column($res, 'goods_id');
            $goods_ids=implode(",",$arr2);

            model('goods')->update(['is_delete' => 1], [['goods_id', 'in',  $goods_ids ]]);
            model('goods_sku')->update(['is_delete' => 1], [['goods_id', 'in', $goods_ids ]]);
        }
        return $this->success($goods_ids);
    }




}