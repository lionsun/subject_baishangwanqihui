<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\present\event;

use addon\present\model\Present;
use app\model\goods\Goods;

/**
 * 同步库存
 */
class SyncStock
{

	public function handle($params)
	{
	    $sku_id = $params['sku_id'] ?? 0;
        $present_model = new Present();
	    //sku库存改变后 更新库存信息
	    if($sku_id > 0){
            $condition = array(
                ['sku_id', '=', $sku_id]
            );
            $goods_model = new Goods();
            $sku_info = $goods_model->getGoodsSkuInfo($condition, 'stock')['data'] ?? [];
            if(!empty($sku_info)){
                $result = $present_model->modifyPresentStock($condition, $sku_info['stock']);
                return $result;
            }
        }
        return error(-1, '同步库存出错');
	}
}