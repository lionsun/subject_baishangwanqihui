<?php

// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\pickpoint\model;

use app\model\BaseModel;
use com\vip\vop\cup\api\address\VipAddressInfo;
use think\facade\Cache;
use think\facade\Db;
use addon\pickpoint\model\PickpointOrder as orderModel;


/**
 * 自提点
 *
 *
 */
class Pickpoint extends BaseModel
{


    /*
     * 添加自提点
     * @param unknown $data
     */
    public function addPickpoint($data)
    {
        model('pickpoint')->startTrans();
        try {

            if (!empty($this->getPickpointInfo([['member_id', '=', $data['member_id']]])['data'])) {
                model("pickpoint")->rollback();
                return $this->error('', '管理员重复请重新选择！！');
            }
            $pickpoint_id = model('pickpoint')->add($data);
            Cache::tag("pickpoint")->clear();
            model('pickpoint')->commit();
            return $this->success($pickpoint_id);
        } catch (\Exception $e) {
            model('pickpoint')->rollback();
            return $this->error('', $e->getMessage());
        }
    }





    /**
     * 修改自提点
     * @param unknown $data
     * @return multitype:string
     */
    public function editPickpoint($data, $pickpoint_id)
    {
        $info_condition[]    = ['member_id', '=',  $data['member_id']];
        $info_condition[]    = ['pickpoint_id', '<>',  $pickpoint_id];
        $info = $this->getPickpointInfo($info_condition)['data'];
        if (!empty($info)) {
            model("pickpoint")->rollback();
            return $this->error('', '管理员重复请重新选择！！');
        }
        //更换配送单以及订单管理员
        $order_model = new orderModel();
        $order_model->updateMember($pickpoint_id,$data['member_id']);
        $res = model('pickpoint')->update($data,  [['pickpoint_id', '=', $pickpoint_id]]);
        Cache::tag("pickpoint")->clear();
        return $this->success($res);
    }





    /**
     * 删除自提点
     * @param unknown $condition
     */
    public function deletePickpoint($condition)
    {
        $res = model('pickpoint')->delete($condition);
        Cache::tag("pickpoint")->clear();
        return $this->success($res);
    }


    /**
     * 获取自提点信息 getPickpointInfo pickpoin
     * @param array $condition
     * @param string $field
     */
    public function getPickpointInfo($condition, $field = '*')
    {
        $res = model('pickpoint')->getInfo($condition, $field);
        return $this->success($res);
    }


    public function getAddress($longitude,$latitude){
        $result['address'] = getAddress($longitude,$latitude);
        return $this->success($result);
    }
    /**
     * 获取自提点列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getPickpointList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $data = json_encode([$condition, $field, $order, $limit]);
        $cache = Cache::get("store_getStoreList_" . $data);
        if (!empty($cache)) {
            return $this->success($cache);
        }
        $list = model('pickpoint')->getList($condition, $field, $order, '', '', '', $limit);
        Cache::tag("pickpoint")->set("pickpoint_getPickpointList_" . $data, $list);

        return $this->success($list);
    }




    /**
     * 获取自提点分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getPickpointPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('pickpoint')->rawPageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }




    /**
     * 查询自提点  带有距离
     * @param $condition
     * @param $lnglat
     */
    public function getLocationPickPointList($condition, $field, $lnglat)
    {
        $order = '';
        if ($lnglat['lat'] !== null && $lnglat['lng'] !== null) {
            $field .= ' , FORMAT(st_distance ( point ( ' . $lnglat['lng'] . ', ' . $lnglat['lat'] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) as distance ';
            $condition[] = ['', 'exp', Db::raw(' FORMAT(st_distance ( point ( ' . $lnglat['lng'] . ', ' . $lnglat['lat'] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) < 10000')];
            $order = 'distance asc';
        }

        $list = model('store')->getList($condition, $field, $order);
        return $this->success($list);
    }

    /**
     * 查询自提点  带有距离
     * @param $condition
     * @param $lnglat
     */
    public function getLocationPickPointPageList($condition, $page = 1, $page_size = PAGE_LIST_ROWS, $field, $lnglat)
    {
        $order = '';
        if ($lnglat['lat'] !== null && $lnglat['lng'] !== null) {
            $field .= ',FORMAT(st_distance ( point ( ' . $lnglat['lng'] . ', ' . $lnglat['lat'] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) as distance';
            $condition[] = ['', 'exp', Db::raw(' FORMAT(st_distance ( point ( ' . $lnglat['lng'] . ', ' . $lnglat['lat'] . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) < 10000')];
            $order = Db::raw(' st_distance ( point ( ' . $lnglat['lng'] . ', ' . $lnglat['lat'] . ' ), point ( longitude, latitude ) ) * 111195 / 1000 asc');
        }
        $list = model('store')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }


    //判断该店铺是否可以自提
    public function isPickUp($site_id){
        $shopInfo = model('shop')->getInfo(['site_id'=>$site_id],'is_pickup');
        return $shopInfo['is_pickup'];
    }
}
