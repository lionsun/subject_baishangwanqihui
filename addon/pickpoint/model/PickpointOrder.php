<?php

// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\pickpoint\model;

use app\model\BaseModel;
use app\model\order\OrderCommon as OrderCommonModel;
use think\facade\Cache;



/**
 * 自提点
 *
 *
 */
class PickpointOrder extends BaseModel
{
    const WAIT_DELIVERY = 0;
    const WAIT_PICK_UP = 1;
    const ALREADY_PICK_UP = 2;



    /**
     * 生成配送单
     *
     * @param array $site_id
     */

    public function add($data){
        $delivery_data = [
            'site_id'    => $data['site_id'],
            'pick_point_id'    => $data['delivery_pick_point_id'],
            'delivery_pick_point_name' =>  $data['delivery_pick_point_name'],
            'pick_point_member_id'    => $data['delivery_pick_point_member_id'],
            'delivery_pick_point_info'    => $data['delivery_pick_point_info'],
            'order_ids'  => $data['order_ids'],
            'delivery_no' => $this->createDeliveryOrder($data['site_id']),
            'status'    => 1,
            'create_time'   => time(),
            'delivery_time' => time()
        ];
        $delivery_id = model('pick_point_delivery')->add($delivery_data);
        if($delivery_id>0){
            return $this->success($delivery_id);
        }
    }


    /**
     * 更新配送单状态
     *
     * @param
     */
    public function updateStatus($data,$delivery_id){
        $res = model('pick_point_delivery')->update($data,  ['delivery_id' =>  $delivery_id]);
        return $res;
    }


    public function  updateMember($pickpoint_id,$member_id){
        $res = model('pick_point_delivery')->update(['pick_point_member_id'=>$member_id],  [['pick_point_id','=',$pickpoint_id]]);
        $res1 = model('order')->update(['delivery_pick_point_member_id'=>$member_id],  [['delivery_pick_point_id','=',$pickpoint_id]]);
        if($res && $res1){
            return $res;
        }else{
            return $this->error('订单转移管理员错误', '');
        }
    }


    public function createDeliveryOrder($site_id)
    {
        $time_str = date('YmdHi');
        $max_no = Cache::get('delivery' . $site_id . "_" . $time_str);
        if (!isset($max_no) || empty($max_no)) {
            $max_no = 1;
        } else {
            $max_no = $max_no + 1;
        }
        $order_no = $time_str . sprintf("%04d", $max_no);
        Cache::set('delivery' . $site_id . "_" . $time_str, $max_no);
        return $order_no;
    }


    public function getDeliveryInfo($condition)
    {
        $info= model('pick_point_delivery')->getInfo($condition);
        if (empty($info)) {
            return $this->error('', '');
        }
        $goods_condition[] = ['order_id','in',$info['order_ids']];
        $info['delivery_pick_point_array'] = json_decode($info['delivery_pick_point_info'],true);
        $info['member_info'] = model('member')->getInfo([['member_id','=',$info['pick_point_member_id']]],'username,nickname,mobile');
        return $this->success($info);
    }

    public function getDeliveryDetail($condition)
    {
        $info= model('pick_point_delivery')->getInfo($condition);
        if (empty($info)) {
            return $this->error('', '');
        }
        $goods_condition[] = ['order_id','in',$info['order_ids']];
        $info['delivery_pick_point_array'] = json_decode($info['delivery_pick_point_info'],true);
        $info['order_list'] = model('order')->getList($goods_condition);
        $info['order_goods'] = model('order_goods')->getList($goods_condition);
        $info['member_info'] = model('member')->getInfo([['member_id','=',$info['pick_point_member_id']]],'username,nickname,mobile');
        return $this->success($info);
    }



    public function orderService($condition){
        $condition[]    = ['status', '=',  self::WAIT_PICK_UP];
        $delivery_info = $this->getDeliveryInfo($condition)['data'];
        if (empty($delivery_info)) {
            return $this->error('', '配货单不存在');
        }
        $data['status'] = self::ALREADY_PICK_UP;
        $data['service_time'] = time();
        $res = model('pick_point_delivery')->update($data,  $condition);
        $take_condition = array(
            ['order_status','=',3],
            ['order_id', 'in', $delivery_info['order_ids']],
            ['order_type', '=', 5]
        );
        //修改为待提货状态
        model('order')->update(['order_status'=>5,'order_status_name'=>'待提货'],$take_condition);
        Cache::tag("pick_point_delivery")->clear();
        return $this->success($res);
    }

    /**
     * 自提点收货操作
     * @param unknown $data
     */
    public function pickPointOrderTakeDelivery($condition)
    {
        $order_model = new OrderCommonModel();
        $order_info = $order_model->getOrderDetail($condition)['data'];
        if($order_info){
            if($order_info['is_lock']==1){
                return $this->success(0,'订单维权中');
            }
        }else{
            return $this->error('订单信息错误', '');
        }
        $order_model->orderCommonTakeDelivery($condition);
        return $this->success();
    }

    /**
     *
     *
     * 获取自提点订单列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getDeliveryList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $list = model('pick_point_delivery')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }


    /**
     * 获取自提点分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getDeliveryPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('pick_point_delivery')->pageList($condition, $field, $order, $page, $page_size);
        foreach($list['list'] as $key=>$val){
            $goods_condition = [['order_id','in',$list['list'][$key]['order_ids']]];
            $order_condition = [['order_id','in',$list['list'][$key]['order_ids']],['order_status','=',1]];
            $list['list'][$key]['delivery_pick_point_array'] = json_decode($val['delivery_pick_point_info'],true);
            $list['list'][$key]['order_list'] = model('order')->getList($goods_condition);
            $list['list'][$key]['valid_order_list'] = model('order')->getList($order_condition);
            $list['list'][$key]['order_count'] = model('order')->getCount($goods_condition,'order_id');
            $list['list'][$key]['order_goods_count'] = model('order_goods')->getSum($goods_condition,'num');
            $list['list'][$key]['member_info'] = model('member')->getInfo([['member_id','=',$val['pick_point_member_id']]],'username,nickname,mobile');
            $list['list'][$key]['site_info'] = model('shop')->getInfo([['site_id','=',$list['list'][$key]['site_id']]],'site_id,site_name,username,mobile,telephone');
        }
        return $this->success($list);
    }
}
