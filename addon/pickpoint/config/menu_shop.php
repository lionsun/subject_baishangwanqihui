<?php
// +----------------------------------------------------------------------
// | 菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'PICK_POINT_ROOT_SHOP',
        'title' => '自提点',
        'url' => 'pickpoint://shop/pickpoint/lists',
        'picture' => 'addon/pickpoint/shop/view/public/img/pickpoint.png',
        'parent' => 'TOOL_ROOT',
        'is_show' => 1,
        'sort' => 1,
        'child_list' => [
            [
                'name' => 'PICK_POINT_SHOP_LISTS',
                'title' => '自提点列表',
                'url' => 'pickpoint://shop/pickpoint/lists',
                'is_show' => 1,
                'sort' => 1,
                'child_list' => [

                ]
            ],
            [
                'name'             => 'SHOP_ORDER_PICK_POINT_DELIVERY',
                'title'            => '配货列表',
                'url'              => 'pickpoint://shop/delivery/deliverylists',
                'is_show'          => 1,
                'sort'             => 2,
                'child_list'       => [
                    [
                        'name'             => 'SHOP_ORDER_PICK_POINT_DETAIL',
                        'title'            => '详情',
                        'url'              => 'pickpoint://shop/delivery/deliverydetail',
                        'is_show'          => 0,
                        'sort'             => 1,
                    ]
                ]
            ],
        ]
    ]
];
