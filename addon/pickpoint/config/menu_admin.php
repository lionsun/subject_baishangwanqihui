<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------

return [
    [
        'name' => 'PICK_POINT_ROOT_ADMIN',
        'title' => '自提点',
        'url' => 'pickpoint://admin/pickpoint/lists',
        'picture' => 'addon/pickpoint/admin/view/public/img/pickpoint.png',
        'parent' => 'TOOL_ROOT',
        'is_show' => 1,
        'sort' => 1,
        'child_list' => [
            [
                'name' => 'PICK_POINT_ROOT',
                'title' => '自提点列表',
                'url' => 'pickpoint://admin/pickpoint/lists',
                'is_show' => 1,
                'sort' => 1,
                'child_list' => [
                    [

                        'name' => 'PICK_POINT_ADD',
                        'title' => '添加自提点',
                        'url' => 'pickpoint://admin/pickpoint/add',
                        'is_show' => 0,
                    ],
                    [
                        'name' => 'PICK_POINT_EDIT',
                        'title' => '编辑自提点',
                        'url' => 'pickpoint://admin/pickpoint/edit',
                        'is_show' => 1,
                    ]
                ]
            ],
            [
                'name' => 'PICK_POINT_ORDER_LISTS',
                'title' => '配货列表',
                'url' => 'pickpoint://admin/order/lists',
                'is_show' => 1,
                'sort' => 1,
                'child_list' => [
                    [

                        'name' => 'PICK_POINT_ORDER_DETAIL',
                        'title' => '配货详情',
                        'url' => 'pickpoint://admin/order/detail',
                        'is_show' => 0,
                    ],
                ]
            ],
        ]
    ]
];
