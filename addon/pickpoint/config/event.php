<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],
    'listen'    => [
        //展示
        'ShowPromotion' => [
            'addon\pickpoint\event\ShowPromotion',
        ],
        //自提点信息
        'PickPointInfo' => [
            'addon\pickpoint\event\PickPointInfo',
        ],
        //创建配送单
        'CreateDeliveryOrder' => [
            'addon\pickpoint\event\CreateDeliveryOrder',
        ],
    ],
    'subscribe' => [
    ],
];
