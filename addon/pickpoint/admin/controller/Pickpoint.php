<?php

/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\pickpoint\admin\controller;

use app\model\member\Member as MemberModel;
use app\model\shop\ShopMember;
use app\admin\controller\BaseAdmin;
use app\model\system\Address as AddressModel;
use addon\pickpoint\model\Pickpoint as  PickpointModel;



/**
 * 自提点管理
 */
class Pickpoint extends BaseAdmin
{

    protected $model = "";


    public function __construct()
    {
        //执行父类构造函数
        parent::__construct();
        $this->model= new PickpointModel();

    }

    /**
     * 自提点列表
     */
    public function lists()
    {
        if (request()->isAjax()) {
            $page_index     = input('page', 1);
            $page_size      = input('page_size', PAGE_LIST_ROWS);
            $search_text    = input('search_keys', '');
            $condition      = [];
            if (!empty($search_text)) {
                $condition[] = ['pickpoint_name', 'LIKE', "%{$search_text}%"];
            }
            $res =  $this->model->getPickpointPageList($condition, $page_index, $page_size, 'create_time DESC');
            return $res;
        }
        return $this->fetch('pickpoint/lists');
    }




    /**
     * 添加自提点
     */
    public function add()
    {
        if (request()->isAjax()) {
            $data = array(
                "pickpoint_name" => input("pickpoint_name", ''),
                "telphone" => input("telphone", ''),
                "pickpoint_image" => input("pickpoint_image", ''),
                "status" => input("status", 0),
                "province_id" => input("province_id", 0),
                "city_id" => input("city_id", 0),
                "district_id" => input("district_id", 0),
                "community_id" => input("community_id", 0),
                "address" => input("address", ''),
                "full_address" => input("full_address", ''),
                "longitude" => input("longitude", 0),
                "latitude" => input("latitude", 0),
                "open_date" => input("open_date", ''),
                "member_name" => input("member_name", ''),
                "member_id" => input("member_id", 0),
                "create_time" =>time()
            );
            return  $this->model->addPickpoint($data);
        }
        //查询省级数据列表
        $address_model = new AddressModel();
        $list = $address_model->getAreaList([ [ "pid", "=", 0 ], [ "level", "=", 1 ] ]);
        $this->assign("province_list", $list["data"]);

        $member = new ShopMember();
        $list = $member->getShopMemberPageList([], 1, 0, 'nsm.subscribe_time desc')['data'];
        $this->assign("member_list", $list["list"]);   //username
        return $this->fetch('pickpoint/add');
    }




    /**
     * 修改自提点
     */
    public function edit()
    {
        $pickpoint_id    = input('pickpoint_id', 0);
        $condition      = [['pickpoint_id', '=', $pickpoint_id]];
        if (request()->isAjax()) {
            $data = array(
                "pickpoint_name" => input("pickpoint_name", ''),
                "telphone" => input("telphone", ''),
                "pickpoint_image" => input("pickpoint_image", ''),
                "status" => input("status", 0),
                "province_id" => input("province_id", 0),
                "city_id" => input("city_id", 0),
                "district_id" => input("district_id", 0),
                "community_id" => input("community_id", 0),
                "address" => input("address", ''),
                "full_address" => input("full_address", ''),
                "longitude" => input("longitude", 0),
                "latitude" => input("latitude", 0),
                "open_date" => input("open_date", ''),
                "member_name" => input("member_name", ''),
                "member_id" => input("member_id", 0),
                "modify_time" =>time()
            );
            $res  = $this->model->editPickpoint($data, $pickpoint_id);
            return $res;
        }
        //查询省级数据列表
        $address_model = new AddressModel();
        $list = $address_model->getAreaList([ [ "pid", "=", 0 ], [ "level", "=", 1 ] ]);
        $this->assign("province_list", $list["data"]);
        $member = new ShopMember();
        $list = $member->getShopMemberPageList([], 1, 0, 'nsm.subscribe_time desc')['data'];
        $this->assign("member_list", $list["list"]);   //username
        $this->assign("info", $this->model->getPickpointInfo($condition)["data"]);
        return $this->fetch('pickpoint/edit');
    }

    /**
     * 删除自提点
     * @return array
     */
    public function delete()
    {
        if (request()->isAjax()) {
            $pickpoint_id= input('pickpoint_id', 0);
            if (empty($pickpoint_id)) {
                return error(-1, '参数错误！');
            }
            $condition      = [['pickpoint_id', '=', $pickpoint_id]];
            $res            = $this->model->deletepickpoint($condition);
            return $res;
        }
    }


    /**
     * 获取地理位置id
     */
    public function getGeographicId()
    {
        $address_model = new AddressModel();
        $address = request()->post("address", ",,");
        $address_array = explode(",", $address);
        $province = $address_array[0];
        $city = $address_array[1];
        $district = $address_array[2];
        $subdistrict = $address_array[3];
        $province_list = $address_model->getAreaList([ "name" => $province, "level" => 1 ], "id", '');
        $province_id = !empty($province_list["data"]) ? $province_list["data"][0]["id"] : 0;
        $city_list = ($province_id > 0) && !empty($city) ? $address_model->getAreaList([ "name" => $city, "level" => 2, "pid" => $province_id ], "id", '') : [];
        $city_id = !empty($city_list["data"]) ? $city_list["data"][0]["id"] : 0;
        $district_list = !empty($district) && $city_id > 0 && $province_id > 0 ? $address_model->getAreaList([ "name" => $district, "level" => 3, "pid" => $city_id ], "id", '') : [];
        $district_id = !empty($district_list["data"]) ? $district_list["data"][0]["id"] : 0;

        $subdistrict_list = !empty($subdistrict) && $city_id > 0 && $province_id > 0 && $district_id > 0 ? $address_model->getAreaList([ "name" => $subdistrict, "level" => 4, "pid" => $district_id ], "id", '') : [];
        $subdistrict_id = !empty($subdistrict_list["data"]) ? $subdistrict_list["data"][0]["id"] : 0;
        return [ "province_id" => $province_id, "city_id" => $city_id, "district_id" => $district_id, "subdistrict_id" => $subdistrict_id ];
    }


    /*
     *  会员选择
     */
    public function memberselect(){
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = 8;
            $search_text = input('search_text', '');
            $search_text_type = input('search_text_type', 'username');//可以传username mobile email

            $condition = [];
            //下拉选择
            $condition[] = [ $search_text_type, 'like', "%" . $search_text . "%" ];
            $condition[] = [ 'is_pintuan_trader', '=', 0 ];

            $order = 'member_id desc';
            $field = 'member_id,mobile,headimg,username,reg_time';

            $member_model = new MemberModel();
            $list = $member_model->getMemberPageList($condition, $page, $page_size, $order, $field);
            return $list;
        } else {
            //回调函数
            $callback = input('callback', '');
            $this->assign('callback', $callback);
            return $this->fetch('pickpoint/member_select');
        }

    }



}
