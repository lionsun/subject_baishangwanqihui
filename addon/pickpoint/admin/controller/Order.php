<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\Pickpoint\admin\controller;

use addon\pickpoint\model\PickpointOrder;
use addon\supply\model\order\Config as ConfigModel;
use addon\supply\model\order\Order as OrderModel;
use addon\supply\model\order\OrderCommon as OrderCommonModel;
use app\admin\controller\BaseAdmin;
use think\facade\Config;

/**
 * 订单管理 控制器
 */
class Order extends BaseAdmin
{
    public function lists(){
        $order_label_list = array(
            "delivery_no" => "配送单号",
            "delivery_pick_point_name" => "自提点名",
        );
        $start_time = input("start_time", '');
        $end_time = input("end_time", '');
        $order_label = !empty($order_label_list[ input("order_label") ]) ? input("order_label") : "";
        $search_text = input("search", '');
        $status = input("status", '');
        $pick_point_order_model = new PickpointOrder();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $condition = [];
            if (!empty($start_time) && empty($end_time)) {
                $condition[] = [ "create_time", ">=", date_to_time($start_time) ];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = [ "create_time", "<=", date_to_time($end_time) ];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
            }
            if ($search_text != "") {
                $condition[] = [ $order_label, 'like', "%$search_text%" ];
            }
            if ($status != "") {
                $condition[] = [ 'status', '=', $status ];
            }

            $list = $pick_point_order_model->getDeliveryPageList($condition, $page_index, $page_size, "create_time desc");
            return $list;
        } else {
            $this->assign('order_label_list',$order_label_list);
            return $this->fetch('order/list');
        }
    }

    public function detail(){

        $order_ids = input("order_ids", '');
        $start_time = input("start_time", '');
        $end_time = input("end_time", '');
        $order_common_model = new \app\model\order\OrderCommon();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $condition = [
                [ 'order_id', "in", $order_ids],
                [ 'order_type', "=", 5],
                [ 'is_delete', '=', 0 ]
            ];
            if (!empty($start_time) && empty($end_time)) {
                $condition[] = [ "create_time", ">=", date_to_time($start_time) ];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = [ "create_time", "<=", date_to_time($end_time) ];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
            }
            $list = $order_common_model->getOrderPageList($condition, $page_index, $page_size, "create_time desc");
            return $list;
        } else {

            //订单来源 (支持端口)
            $order_from = Config::get("app_type");
            $this->assign('order_from_list', $order_from);

            $pay_type = $order_common_model->getPayType();
            $this->assign("pay_type_list", $pay_type);

            $this->assign("http_type", get_http_type());
            $this->assign("order_ids", $order_ids);
            return $this->fetch('order/detail');
        }

    }
}
