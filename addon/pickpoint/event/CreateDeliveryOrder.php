<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\pickpoint\event;
use addon\pickpoint\model\PickpointOrder as PickpointOrderModel;

/**
 *
 */
class CreateDeliveryOrder
{

    /**
     * 创建配送单
     * @param $data_order
     */
	public function handle($data_order)
	{
	    trace($data_order);
	    if($data_order['order_type']==5 && $data_order['delivery_pick_point_id']!=0){
	        //判断商家与自提点间是否已生成配货单
            $pick_point_model = new PickpointOrderModel();
            $pick_point_model->add($data_order);
        }
	}
}