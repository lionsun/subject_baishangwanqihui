<?php

/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\pickpoint\shop\controller;

use app\shop\controller\BaseShop;
use app\model\shop\Shop;
use addon\pickpoint\model\Pickpoint as  PickpointModel;



/**
 * 自提点管理
 */
class Pickpoint extends BaseShop
{

    protected $model = "";


    public function __construct()
    {
        //执行父类构造函数
        parent::__construct();
        $this->model= new PickpointModel();
    }

    public function config(){
        $model = new Shop();
        if (request()->isAjax()) {
            $is_pickup = input('is_pickup', 0);
            return $model->setIsPickUp($this->site_id,$is_pickup);
        }
        $info = $model->getShopInfo([['site_id','=',$this->site_id]]);
        $this->assign('info',$info['data']);
        return $this->fetch('pickpoint/config');
    }

    /**
     * 自提点列表
     */
    public function lists()
    {


        if (request()->isAjax()) {
            $page_index     = input('page', 1);
            $page_size      = input('page_size', PAGE_LIST_ROWS);
            $search_text    = input('search_keys', '');
            $condition      = [];
            if (!empty($search_text)) {
                $condition[] = ['pickpoint_name', 'LIKE', "%{$search_text}%"];
            }
            $res =  $this->model->getPickpointPageList($condition, $page_index, $page_size, 'create_time DESC');
            return $res;
        }
        return $this->fetch('pickpoint/lists');
    }


}
