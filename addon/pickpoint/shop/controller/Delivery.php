<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\pickpoint\shop\controller;

use app\shop\controller\BaseShop;
use app\model\express\Config as ConfigModel;
use app\model\order\OrderCommon as OrderCommonModel;
use app\model\order\Order as OrderModel;
use addon\electronicsheet\model\ExpressElectronicsheet as ExpressElectronicsheetModel;
use think\facade\Config;
use addon\electronicsheet\model\ElectronicsheetDelivery;
use addon\pickpoint\model\PickpointOrder;

/**
 * 配送
 * Class Express
 * @package app\shop\controller
 */
class Delivery extends BaseShop
{
	
	
	public function __construct()
	{
		//执行父类构造函数
		parent::__construct();
		
	}

	/**
	 * 配送方式
	 */
	public function express()
	{
		$config_model = new ConfigModel();
		$config_result = $config_model->getExpressConfig($this->site_id);
		$express_config = $config_result["data"];
		$this->assign("express_config", $express_config);
		$config_result = $config_model->getStoreConfig($this->site_id);
		$store_config = $config_result["data"];
		$this->assign("store_config", $store_config);
		$config_result = $config_model->getLocalDeliveryConfig($this->site_id);
		$local_delivery_config = $config_result["data"];
		$this->assign("local_delivery_config", $local_delivery_config);
        $config_result = $config_model->getPickPointConfig($this->site_id);
        $pick_point_config = $config_result["data"];
        $this->assign("pick_point_config", $pick_point_config);
		return $this->fetch("delivery/delivery");
	}


	

    /**
     * 自提点配置开关
     * @return \multitype
     */
    public function modifyPickPointStatus()
    {
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            $is_use = input("is_use", 0);
            $data = array();
            $result = $config_model->setPickPointConfig($data, $is_use, $this->site_id);
            return $result;
        }
    }

    /**
     * 批量发货
     */
    public function batchDelivery()
    {
        if (request()->isAjax()) {

            $order_model = new OrderModel();
            $data = array(
                "type" => input('type','manual'),//发货方式（手动发货、电子面单）
                "express_company_id" => input("express_company_id", 0),//物流公司
                "delivery_type" => input("delivery_type", 0),//是否需要物流
                "site_id" => $this->site_id,
                "template_id" => input('template_id',0),//电子面单模板id
            );

            $order_list = input('order_list','');
            $delivery_id = input('delivery_id',0);
            $data['delivery_id'] = $delivery_id;
            $result = $order_model->orderBatchDelivery($data,$order_list);
            return $result;
        }
    }


    /**
     * 获取电子面单模板列表
     */
    public function getExpressElectronicsheetList()
    {
        //电子面单插件
        $addon_is_exit = addon_is_exit('electronicsheet');
        if($addon_is_exit == 1){

            //获取电子面单模板
            $electronicsheet_model = new ExpressElectronicsheetModel();
            $condition[] = [ 'site_id', '=', $this->site_id ];

            $electronicsheet_list = $electronicsheet_model->getExpressElectronicsheetList($condition, '', 'is_default desc');
            return $electronicsheet_list;

        }else{
            return success(0,'success',[]);
        }
    }

    /**
     * 打印电子面单
     */
    public function printElectronicsheet()
    {
        if (request()->isAjax()) {

            $addon_is_exit = addon_is_exit('electronicsheet');
            if($addon_is_exit != 1){
                return [
                    'code' => -1001,
                    'message' => '电子面单插件不存在',
                    'data' => ''
                ];
            }

            $order_model = new OrderModel();
            $data = array(
                "type" => 'electronicsheet',//电子面单
                "express_company_id" => 0,//物流公司
                "delivery_type" => 1,
                "site_id" => $this->site_id,
                "template_id" => input('template_id',0),//电子面单模板id
                "is_delivery" => input('is_delivery', 0),//是否发货
                'order_id' => input('order_id'),//订单id
                'order_goods_ids' => '',
                'delivery_no' => ''
            );

            //电子面单
            $electronicsheet_model = new ElectronicsheetDelivery();
            $result = $electronicsheet_model->delivery($data);
            if($result['code'] >= 0){

                if($data['is_delivery'] == 1){//发货

                    $data['delivery_no'] = $result['data']['Order']['LogisticCode'];
                    $res = $order_model->orderGoodsDelivery($data, 2);
                    if($res['code'] < 0){
                        return $res;
                    }
                }

                return $result;
            }else{
                return $result;
            }

        }

    }


    public function deliverylists(){
        $order_label_list = array(
            "delivery_no" => "配送单号",
            "delivery_pick_point_name" => "自提点名",
        );
        $start_time = input("start_time", '');
        $end_time = input("end_time", '');
        $order_label = !empty($order_label_list[ input("order_label") ]) ? input("order_label") : "";
        $search_text = input("search", '');
        $status = input("status", '');
        $pick_point_order_model = new PickpointOrder();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $condition = [
                ["site_id", "=", $this->site_id],
            ];
            if (!empty($start_time) && empty($end_time)) {
                $condition[] = [ "create_time", ">=", date_to_time($start_time) ];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = [ "create_time", "<=", date_to_time($end_time) ];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
            }
            if ($search_text != "") {
                $condition[] = [ $order_label, 'like', "%$search_text%" ];
            }
            if ($status != "") {
                $condition[] = [ 'status', '=', $status ];
            }
            $list = $pick_point_order_model->getDeliveryPageList($condition, $page_index, $page_size, "create_time desc");
            return $list;
        } else {
            $this->assign('order_label_list',$order_label_list);
            return $this->fetch('delivery/delivery_list');
        }
    }


    public function deliverydetail(){

        $order_ids = input("order_ids", '');
        $order_name = input("order_name", '');
        $pay_type = input("pay_type", '');
        $order_from = input("order_from", '');
        $start_time = input("start_time", '');
        $end_time = input("end_time", '');
        $search_text = input("search", '');
        $promotion_type = input("promotion_type", '');//订单类型
        $order_type = input("order_type", 'all');//营销类型
        $order_common_model = new OrderCommonModel();
        if (request()->isAjax()) {
            $order_ids = input("order_ids", '');
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $condition = [
                [ 'order_id', "in", $order_ids],
                [ 'order_type', "=", 5],
                [ 'site_id', "=", $this->site_id ],
                [ 'is_delete', '=', 0 ]
            ];
            //订单内容 模糊查询
            if ($order_name != "") {
                $condition[] = [ "order_name", 'like', "%$order_name%" ];
            }
            //订单来源
            if ($order_from != "") {
                $condition[] = [ "order_from", "=", $order_from ];
            }
            //订单支付
            if ($pay_type != "") {
                $condition[] = [ "pay_type", "=", $pay_type ];
            }
            //订单类型
            if ($order_type != 'all') {
                $condition[] = [ "order_type", "=", $order_type ];
            }
            //营销类型
            if ($promotion_type != "") {
                if ($promotion_type == 'empty') {
                    $condition[] = [ "promotion_type", "=", '' ];
                } else {
                    $condition[] = [ "promotion_type", "=", $promotion_type ];
                }
            }
            if (!empty($start_time) && empty($end_time)) {
                $condition[] = [ "create_time", ">=", date_to_time($start_time) ];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = [ "create_time", "<=", date_to_time($end_time) ];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
            }
            $list = $order_common_model->getOrderPageList($condition, $page_index, $page_size, "create_time desc");
            return $list;
        } else {

            //订单来源 (支持端口)
            $order_from = Config::get("app_type");
            $this->assign('order_from_list', $order_from);

            $pay_type = $order_common_model->getPayType();
            $this->assign("pay_type_list", $pay_type);

            $this->assign("http_type", get_http_type());
            $this->assign("order_ids", $order_ids);
            return $this->fetch('delivery/delivery_detail');
        }

    }

}