<?php

namespace addon\pickpoint\api\controller;


use app\api\controller\BaseApi;
use addon\pickpoint\model\Pickpoint as PickpointModel;
use think\facade\Db;

class pickpoint extends BaseApi{

    /**
     * 自提点分页列表
     */
    public function lists(){
        $page      = isset($this->params['page'])      ? $this->params['page']      : 1;
        $page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
        $longitude = isset($this->params['longitude']) ? $this->params['longitude'] : null;   //经度
        $latitude  = isset($this->params['latitude'])  ? $this->params['latitude']  : null;   //纬度
        $keywords  = isset($this->params['keywords']) ? $this->params['keywords']      : '';
        $model = new PickpointModel();
        $condition[] = ['status','=',1];
        if($keywords!=''){
            $condition[] = ['pickpoint_name','like','%'.$keywords.'%'];
        }
        $field = '*';
        if($latitude !== null && $longitude !== null){
            $condition[] = ['', 'exp', Db::raw(' FORMAT(st_distance ( point ( ' . $longitude . ', ' . $latitude . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) < 10000')];
            $param = ',';
            $distance = get_distance_field($longitude, $latitude, 'longitude', 'latitude');
            $sort = 'asc';
            $field.=$param.$distance.'as distance';
            $order_by = $distance.' '.$sort;
        }else{
            $order_by ='pickpoint_id asc';
        }
        $pageList = $model->getPickpointPageList($condition, $page, $page_size, $order_by, $field);
        return $this->response($pageList);
    }


    /**
     * 订单详情
     */
    public function detail(){
        $pickpoint_id = isset($this->params['pickpoint_id']) ? $this->params['pickpoint_id'] : 0;
        $longitude = isset($this->params['longitude']) ? $this->params['longitude'] : null;   //经度
        $latitude  = isset($this->params['latitude'])  ? $this->params['latitude']  : null;   //纬度
        $model = new PickpointModel();
        $field = '*';
        $condition[] = ['status','=',1];
        $condition[] = ['pickpoint_id','=',$pickpoint_id];
        if($latitude !== null && $longitude !== null){
            $field .= ' , FORMAT(st_distance ( point ( ' . $longitude . ', ' . $latitude . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) as distance';
            $condition[] = ['', 'exp', Db::raw(' FORMAT(st_distance ( point ( ' . $longitude . ', ' . $latitude . ' ), point ( longitude, latitude ) ) * 111195 / 1000, 2) < 10000')];
        }
        $detail = $model->getPickpointInfo($condition, $field);
        return $this->response($detail);
    }


    /**
     * 获取当前所在地址
     */
    public function getLocalAddress(){
        $longitude = isset($this->params['longitude']) ? $this->params['longitude'] : '117.20783';   //经度
        $latitude  = isset($this->params['latitude'])  ? $this->params['latitude']  : '39.118851';   //纬度
        if($latitude != '' && $longitude != '') {
            $model = new PickpointModel();
            $result = $model->getAddress($longitude,$latitude);
            return  $this->response($result);
        }else{
            return  $this->response($this->error('','缺少参数'));
        }
    }



}
