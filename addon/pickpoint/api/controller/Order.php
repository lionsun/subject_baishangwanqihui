<?php

namespace addon\pickpoint\api\controller;


use app\api\controller\BaseApi;
use addon\pickpoint\model\PickpointOrder as PickpointOrderModel;
use app\model\order\OrderCommon as OrderCommonModel;

class order extends BaseApi{


    /**
     * 自提点  订单分页列表(管理员)
     */

    public function lists(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $status    = isset($this->params['status'])    ? $this->params['status']    : '';
        $page      = isset($this->params['page'])      ? $this->params['page']      : 1;
        $page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
        //自提点管理员id
        $member_id = $this->member_id;
        $pick_point_order_model = new PickpointOrderModel();
        $condition[] = ['pick_point_member_id','=',$member_id];
        if($status !== ''){
            $condition[] = ['status','=',$status];
        }
        $order = 'create_time desc';
        $pagelist = $pick_point_order_model->getDeliveryPageList($condition,$page,$page_size,$order);
        return $this->response($pagelist);
    }


    /**
     * 自提点  订单详情(管理员)
     */
    public function detail(){
        $delivery_id      = isset($this->params['delivery_id']) ? $this->params['delivery_id'] : 0;
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = $this->member_id;
        $pick_point_order_model = new PickpointOrderModel();
        $condition[] = ['pick_point_member_id','=',$member_id];
        $condition[] = ['delivery_id','=',$delivery_id];
        $detail = $pick_point_order_model->getDeliveryDetail($condition);
        return $this->response($detail);
    }


    /**
     * 自提点  配送单订单列表
     */
    public function orderlist(){
        $delivery_id      = isset($this->params['delivery_id']) ? $this->params['delivery_id'] : 0;
        $page      = isset($this->params['page ']) ? $this->params['page '] : 0;
        $page_size      = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
        $keywords  = isset($this->params['keywords'])    ? $this->params['keywords']      : '';
        $order_id  = isset($this->params['order_id'])    ? $this->params['order_id']      : '';
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = $this->member_id;
        $pick_point_order_model = new PickpointOrderModel();
        $condition[] = ['pick_point_member_id','=',$member_id];
        if($order_id == "" || $order_id == 0 ){
            $condition[] = ['delivery_id','=',$delivery_id];
        }
        if($delivery_id==0 && $order_id !='' && $order_id == 0){
            $delivery_id = $pick_point_order_model->getDeliveryInfo([['order_ids','like','%'.$order_id.',%']])['data']['delivery_id'];
            $condition[] = ['delivery_id','=',$delivery_id];
        }
        $info = $pick_point_order_model->getDeliveryInfo($condition)['data'];
        if($keywords!==''){
            $order_condition[] = ['access_code|order_no','like','%'.$keywords.'%'];
        }
        $order = 'order_id desc';
        if(!empty($info)){
            if($order_id !='' && $order_id != 0){
                $order_condition[] = ['order_id','=',$order_id];
            }else{
                $order_condition[] = ['order_id','in',$info['order_ids']];
            }
            $order_common_model = new OrderCommonModel();
            $order_list = $order_common_model->getOrderPageList($order_condition,$page,$page_size,$order);
            return $this->response($order_list);
        }else{
            return $this->response($this->error('','信息不存在'));
        }
    }


    /**
    * 自提点  配送单订单列表
    */
    public function ordergoods(){
        $delivery_id      = isset($this->params['delivery_id']) ? $this->params['delivery_id'] : 0;
        $page      = isset($this->params['page ']) ? $this->params['page '] : 0;
        $page_size      = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = $this->member_id;
        $pick_point_order_model = new PickpointOrderModel();
        $condition[] = ['pick_point_member_id','=',$member_id];
        $condition[] = ['delivery_id','=',$delivery_id];
        $info = $pick_point_order_model->getDeliveryInfo($condition)['data'];
        if(!empty($info)){
            $order_condition[] = ['order_id','in',$info['order_ids']];
            $order_common_model = new OrderCommonModel();
            $order_list = $order_common_model->getOrderGoodsList($order_condition,$page,$page_size);
            return $this->response($order_list);
        }else{
            return $this->response($this->error('','信息不存在'));
        }
    }

    /**
     * 自提点  订单详情(管理员操作订单已送达)
     */
    public function orderService(){
        $delivery_id = isset($this->params['delivery_id']) ? $this->params['delivery_id'] : 0;
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = $this->member_id;
        $pick_point_order_model = new PickpointOrderModel();
        $condition[] = ['pick_point_member_id','=',$member_id];
        $condition[] = ['delivery_id','=',$delivery_id];
        $res = $pick_point_order_model->orderService($condition);
        return $this->response($res);
    }

    /**
     * 自提点  订单详情(管理员操作订单已提货)
     */
     public function optionOrderComplete(){
         $token = $this->checkToken();
         if ($token['code'] < 0) return $this->response($token);
         $member_id = $this->member_id;
         $pick_point_order_model = new PickpointOrderModel();
         $order_id      = isset($this->params['order_id']) ? $this->params['order_id'] : 0;
         $access_code   = isset($this->params['access_code']) ? $this->params['access_code'] : '';
         $condition[] = ['delivery_pick_point_member_id','=',$member_id];
         $condition[] = ['order_id','=',$order_id];
         $condition[] = ['order_type','=',5];
         if($access_code!=''){
             $condition[] = ['access_code','=',$access_code];
         }
         $res = $pick_point_order_model->pickPointOrderTakeDelivery($condition);
         return $this->response($res);
     }

}
