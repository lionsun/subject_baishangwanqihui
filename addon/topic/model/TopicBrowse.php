<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\topic\model;

use app\model\BaseModel;

/**
 * 专题活动浏览历史
 */
class TopicBrowse extends BaseModel
{
	
	/**
	 * 添加专题活动浏览
	 * @param unknown $data
	 * @return multitype:string
	 */
	public function addTopicBrowse($data)
	{
        $star_time = strtotime(date('Y-m-d'));
        $end_time = strtotime(date('Y-m-d ' . '23:59:59'));
        $condition[] = ['type', '=', '1'];
        $condition[] = ['topic_goods_id', '=', $data['topic_id']];
        $condition[] = ['topic_id', '=', $data['topic_id']];
        $condition[] = ['member_id', '=', $data['member_id']];
        $condition[] = ['consumer_ip', '=', $data['consumer_ip']];
        $condition[] = ['create_time', '>', $star_time];
        $condition[] = ['create_time', '<', $end_time];
        //判断UV PV
        $info = model("promotion_topic_browse")->getInfo($condition);
        if($info){
           $data['type'] = 2;
        }
        $res = model("promotion_topic_browse")->add($data);

		return $this->success($res);
	}

    /**
     * 专题活动浏览总数
     * @param unknown $id
     * @param unknown $start_time
     * @param unknown $end_time
     * @return multitype:string
     */
    public function getTopicBrowseSum($id, $start_time, $end_time){
        $condition[] = ['topic_id', '=', $id];
        if (!empty($start_time) && empty($end_time)) {
            $condition[] = ["create_time", ">=", date_to_time($start_time)];
        } elseif (empty($start_time) && !empty($end_time)) {
            $condition[] = ["create_time", "<=", date_to_time($end_time)];
        } elseif (!empty($start_time) && !empty($end_time)) {
            $condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
        }

        $res = model("promotion_topic_browse")->getList($condition, 'COUNT(distinct consumer_ip) as ip, SUM(CASE WHEN type = 1 THEN 1 ELSE 0 END) AS uv, SUM(CASE WHEN type = 2 THEN 1 ELSE 0 END) AS pv', '', '', '', '', '');

        return $res;
    }

    /**
     * 获取专题活动商品浏览总数
     * @param unknown $id
     * @param unknown $start_time
     * @param unknown $end_time
     * @return multitype:string
     */
    public function getTopicGoodsViewCount($topic_id, $sku_id, $start_time, $end_time){
        $condition[] = ['type', '=', 2];
        $condition[] = ['topic_id', '=', $topic_id];
        $condition[] = ['topic_goods_id', '=', $sku_id];
        $condition[] = ['create_time', '>', $start_time];
        $condition[] = ['create_time', '<', $end_time];

        $res = model("promotion_topic_browse")->getCount($condition, 'id');

        return $res;
    }

}