<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\topic\shop\controller;

use app\shop\controller\BaseShop;
use addon\topic\model\TopicGoods as TopicGoodsModel;
use addon\topic\model\Topic as TopicModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\goods\Goods as GoodsModel;

/**
 * 专题活动
 * @author Administrator
 *
 */
class Goods extends BaseShop
{
	/**
     * 商品选择组件
     * @return \multitype
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $category_id = input('category_id', "");// 商品分类id
            $topic_id = input('topic_id', 0);
            $select_id = input('select_id', '');
            $search_type = input('search_type', 'all');
            
            //营销活动开始时间 结束时间 当前商品

            $alias = 'gs';
            $join = [
            	['shop s', 's.site_id = gs.site_id', 'inner'],
            ];

            $condition[] = ['gs.site_id', '=', $this->site_id];
            $condition[] = [ 's.shop_status', '=', 1];
            $condition[] = [ 'gs.goods_state', '=', 1 ];
			$condition[] = [ 'gs.verify_state', '=', 1 ];
			$condition[] = [ 'gs.is_delete', '=', 0 ];


            if (!empty($goods_name)) {
                $condition[] = ['gs.sku_name', 'like', '%' . $goods_name . '%'];
            }
            if (!empty($category_id)) {
                $condition[] = ['gs.category_id_1|gs.category_id_2|gs.category_id_3', 'like', [$category_id, '%' . $category_id . ',%', '%' . $category_id, '%,' . $category_id . ',%'], 'or'];
            }
            if($search_type == 'checked'){
            	$condition[] = ['gs.sku_id', 'in', $select_id];
            }

            $order = 'gs.create_time desc';

            $goods_model = new GoodsModel();
            $field = 'gs.goods_id,gs.sku_id,gs.sku_name,gs.goods_class_name,gs.sku_image,gs.price,gs.market_price,gs.stock,gs.create_time,gs.is_virtual,gs.source';
            $goods_list = $goods_model->getGoodsSkuPageList($condition, $page, $page_size, $order, $field, $alias, $join);
            
            
            $topic_model = new TopicModel();
            $topic_goods_model = new TopicGoodsModel();
            $topic_info = $topic_model->getTopicInfo([['topic_id', '=', $topic_id]])['data'];
            $time_info = ['start_time' => $topic_info['start_time'], 'end_time' => $topic_info['end_time']];
            foreach($goods_list['data']['list'] as $key=>$val){
            	//检测相同时间段专题活动
            	$show_info = [];
            	$alias = 'tpg';
            	$join = [
            		['promotion_topic pt', 'pt.topic_id = tpg.topic_id', 'inner']
            	];
            	$field = 'pt.start_time, pt.end_time, pt.topic_name';
            	$topic_goods_list = $topic_goods_model->getTopicGoodsList([['sku_id', '=', $val['sku_id']]], $field, '', $alias, $join)['data'];
            	foreach($topic_goods_list as $k=>$v){
            		if(checkSameTime($time_info, $v)){
	                    $show_info[] = [
	                        'name' => $v['topic_name'],
	                        'start_time' => $v['start_time'],
	                        'end_time' => $v['end_time'],
	                    ];
	                }
            	}
            	$goods_list['data']['list'][$key]['same_time_topic'] = $show_info;
            }

            return $goods_list;
        } else {

            //专题id
            $topic_id = input('topic_id', '');
            $this->assign('topic_id', $topic_id);

            //商品分类
            $goods_category_model = new GoodsCategoryModel();
            $condition = [];
            $condition[] = ['category_id', 'in', $this->getShopGoodsCategoryIdArr()];
            $category_list = $goods_category_model->getCategoryList($condition, 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);

            return $this->fetch("goods/goods_select");
        }
    }
}