<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\topic\shop\controller;

use app\shop\controller\BaseShop;
use addon\topic\model\Topic as TopicModel;
use addon\topic\model\TopicGoods as TopicGoodsModel;
use addon\topic\model\TopicBrowse as TopicBrowseModel;
use app\model\order\OrderCommon as OrderCommonModel;
use app\model\system\PageRetention as PageRerentionModel;
use app\model\web\WebSite as WebsiteModel;

/**
 * 专题活动
 * @author Administrator
 *
 */
class Topic extends BaseShop
{
	/**
	 * 专题活动列表
	 */
	public function lists()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$topic_name = input('topic_name', '');
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');

			$condition = [];
			$condition[] = [ 'topic_name', 'like', '%' . $topic_name . '%' ];
            if (!empty($start_time) && empty($end_time)) {
                $condition[] = ["start_time", ">=", date_to_time($start_time)];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = ["end_time", "<=", date_to_time($end_time)];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'start_time', '>=', date_to_time($start_time)];
                $condition[] = [ 'end_time', '<=', date_to_time($end_time)];
            }

			$order = 'modify_time desc,create_time desc';
			$field = '*';
			
			$topic_model = new TopicModel();
			$topic_browse_model = new TopicBrowseModel();
			$res = $topic_model->getTopicPageList($condition, $page, $page_size, $order, $field);
            $website_model = new WebsiteModel();
            $website_info = $website_model->getWebSite([['site_id', '=', 0]], '*');
			foreach($res['data']['list'] as $key=>$val){
                $res['data']['list'][$key]['status_name'] = $topic_model->getTopicStatusName($val['status']);
                $res['data']['list'][$key]['view_url'] = $website_info['data']['web_domain'] . '/diy?view_name=TOPIC_ID_' . $val['topic_id'];
                $topic_browse = $topic_browse_model->getTopicBrowseSum($val['topic_id'], $start_time, $end_time);
                $res['data']['list'][$key]['ip'] = $topic_browse[0]['ip'] ?: 0;
                $res['data']['list'][$key]['uv'] = $topic_browse[0]['uv'] ?: 0;
                $res['data']['list'][$key]['pv'] = $topic_browse[0]['pv'] ?: 0;
            }
			return $res;
		} else {
			$this->forthMenu();
			return $this->fetch("topic/lists");
		}
	}
	
	/**
	 * 专题活动商品列表
	 */
	public function goods()
	{
		$topic_id = input("topic_id", 0);
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$sku_name = input('sku_name', '');
			
			$condition = [];
			$condition[] = [ 'nptg.topic_id', '=', $topic_id ];
			$condition[] = [ 'nptg.site_id', '=', $this->site_id ];
			$condition[] = [ 'ngs.sku_name', 'like', '%' . $sku_name . '%' ];
			$order = 'id desc';
			$field = '*';
			$topic_goods_model = new TopicGoodsModel();

            $alias = 'nptg';
            $join = [
                [ 'goods_sku ngs', 'nptg.sku_id = ngs.sku_id', 'inner' ],
                [ 'promotion_topic npt', 'nptg.topic_id = npt.topic_id', 'inner' ],
            ];

			$res = $topic_goods_model->getTopicGoodsPageList($condition, $page, $page_size, $order, $field, $alias, $join);
	        $topic_browse_model = new TopicBrowseModel();
            $order_common_model = new OrderCommonModel();
            $page_rerention_model = new PageRerentionModel();

            foreach($res['data']['list'] as $k=>$v){
                $res['data']['list'][$k]['rerention_time'] = $page_rerention_model->getRetentionTime($v['sku_id'], $v['start_time'], $v['end_time']);
                $res['data']['list'][$k]['view_count'] = $topic_browse_model->getTopicGoodsViewCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);
                $res['data']['list'][$k]['order_count'] = $order_common_model->getTopicGoodsOrderCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);

                if($res['data']['list'][$k]['view_count'] == 0 || $res['data']['list'][$k]['order_count'] == 0){
                    $res['data']['list'][$k]['conversionRate'] = 0;
                }else{
                    $res['data']['list'][$k]['conversionRate'] = bcdiv($res['data']['list'][$k]['view_count'], $res['data']['list'][$k]['order_count'], 2) * 100;
                }
                $res['data']['list'][$k]['rerention_time'] = bcdiv($res['data']['list'][$k]['rerention_time'], 3600, 2);
            }
			return $res;
		} else {
			$topic_model = new TopicModel();
			
			$topic_info = $topic_model->getTopicInfo([ 'topic_id' => $topic_id ], '*');
			$this->assign("topic_id", $topic_id);
			$this->assign("topic_info", $topic_info['data']);
			return $this->fetch("topic/goods");
		}
	}
	
	/**
	 * 添加专题活动商品
	 */
	public function addTopicGoods()
	{
		if (request()->isAjax()) {
			$topic_id = input("topic_id", 0);
			$sku_ids = input("sku_ids", '');
			$topic_goods_model = new TopicGoodsModel();
			$res = $topic_goods_model->addTopicGoods($topic_id, $this->site_id, $sku_ids);
			return $res;
		}
	}
	
	/**
	 * 编辑专题活动商品
	 */
	public function editTopicGoods()
	{
		if (request()->isAjax()) {
			$topic_id = input("topic_id", 0);
			$sku_id = input("sku_id", 0);
			$price = input("price", 0);
			$topic_goods_model = new TopicGoodsModel();
			$res = $topic_goods_model->editTopicGoods($topic_id, $this->site_id, $sku_id, $price);
			return $res;
		}
	}
	
	/**
	 * 删除专题活动商品
	 */
	public function deleteTopicGoods()
	{
		if (request()->isAjax()) {
			$topic_id = input("topic_id", 0);
			$sku_id = input("sku_id", 0);
			$topic_goods_model = new TopicGoodsModel();
			$res = $topic_goods_model->deleteTopicGoods($topic_id, $this->site_id, $sku_id);
			return $res;
		}
	}
	
	/**
	 * 专题活动商品列表
	 */
	public function goodslist()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$sku_name = input('sku_name', '');
			
			$condition = [];
			$condition[] = [ 'nptg.site_id', '=', $this->site_id ];
			$condition[] = [ 'ngs.sku_name', 'like', '%' . $sku_name . '%' ];
			$order = '';
			$field = '*';
			$topic_goods_model = new TopicGoodsModel();

            $alias = 'nptg';
            $join = [
                [ 'goods_sku ngs', 'nptg.sku_id = ngs.sku_id', 'inner' ],
                [ 'promotion_topic npt', 'nptg.topic_id = npt.topic_id', 'inner' ],
            ];

			$res = $topic_goods_model->getTopicGoodsPageList($condition, $page, $page_size, $order, $field, $alias, $join);
			return $res;
		} else {
			$this->forthMenu();
			return $this->fetch("topic/goodslist");
		}
	}

    /**
     * 营销活动导出
     */
    public function exportTopic(){
        $start_time = input('start_time', 0);
        $end_time = input('end_time', 0);
        $topic_name = input('topic_name', '');
        if (!empty($start_time) && empty($end_time)) {
            $condition[] = ["start_time", ">=", date_to_time($start_time)];
            $goods_condition[] = ["nptg.start_time", ">=", date_to_time($start_time)];
        } elseif (empty($start_time) && !empty($end_time)) {
            $condition[] = ["end_time", "<=", date_to_time($end_time)];
            $goods_condition[] = ["nptg.end_time", "<=", date_to_time($end_time)];
        } elseif (!empty($start_time) && !empty($end_time)) {
            $condition[] = [ 'start_time', '>=', date_to_time($start_time)];
            $condition[] = [ 'end_time', '<=', date_to_time($end_time)];
            $goods_condition[] = [ 'nptg.start_time', '>=', date_to_time($start_time)];
            $goods_condition[] = [ 'nptg.end_time', '<=', date_to_time($end_time)];
        }
        $condition[] = [ 'topic_name', 'like', '%' . $topic_name . '%' ];
        $goods_condition[] = [ 'npt.topic_name', 'like', '%' . $topic_name . '%' ];
        $goods_condition[] = [ 'nptg.site_id', '=', $this->site_id ];


        //专题活动列表
        $topic_model = new TopicModel();
        $topic_browse_model = new TopicBrowseModel();
        $topic_list = $topic_model->getExportTopicList($condition);
        foreach($topic_list as $key=>$val){
            $topic_browse = $topic_browse_model->getTopicBrowseSum($val['topic_id'], date('Y-m-d H:i:s', $val['start_time']), date('Y-m-d H:i:s', $val['end_time']));
            $topic_list[$key]['ip'] = $topic_browse[0]['ip'] ?: 0;
            $topic_list[$key]['uv'] = $topic_browse[0]['uv'] ?: 0;
            $topic_list[$key]['pv'] = $topic_browse[0]['pv'] ?: 0;
        }

        //活动商品列表
        $topic_goods_model = new TopicGoodsModel();
        $order_common_model = new OrderCommonModel();
        $topic_goods_list = $topic_goods_model->getExportTopicGoodsList($goods_condition);
        foreach($topic_goods_list as $k=>$v){
            $topic_goods_list[$k]['view_count'] = $topic_browse_model->getTopicGoodsViewCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);
            $topic_goods_list[$k]['order_count'] = $order_common_model->getTopicGoodsOrderCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);

            if($topic_goods_list[$k]['view_count'] == 0 || $topic_goods_list[$k]['order_count'] == 0){
                $topic_goods_list[$k]['conversionRate'] = 0;
            }else{
                $topic_goods_list[$k]['conversionRate'] = bcdiv($topic_goods_list[$k]['view_count'], $topic_goods_list[$k]['order_count'], 2) * 100;
            }
        }

        $header_arr = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
        );
        //处理数据
        $excelTableData = [];
        $excelTableData[] = $topic_list;
        $excelTableData[] = $topic_goods_list;

        //接收需要展示的字段
        $topic_field = ["topic_id", "topic_name", "start_time", "end_time", "status", "ip", "uv", "pv"];
        $goods_field = ["topic_id", "start_time", "end_time", "sku_id", "goods_name", "retention_time", "view_count", "order_count", "conversionRate"];

        $topic_field_name = [
            "topic_id" => '专题活动编号',
            "topic_name" => '专题活动名称',
            "start_time" => '活动开始时间',
            "end_time" => '活动结束时间',
            "status" => '活动状态',
            "ip" => 'IP数',
            "uv" => 'UV数',
            "pv" => 'PV数'
        ];
        $goods_field_name = [
            "topic_id" => '专题活动编码',
            "start_time" => '活动开始时间',
            "end_time" => '活动结束时间',
            "sku_id" => 'SKU编码',
            "goods_name" => '商品名称',
            "retention_time" => '留存时间',
            "view_count" => '访问量',
            "order_count" => '成交量',
            "conversionRate" => '商品转化率'
        ];
        $sheetName = ['专题活动统计-活动信息', '专题活动统计-商品信息'];

        // 实例化excel
        $phpExcel = new \PHPExcel();
        if(empty($excelTableData)){
            $phpExcel->setactivesheetindex(0);
            $phpExcel->getActiveSheet()->setTitle($sheetName[0]);
            $phpExcel->getActiveSheet()->setCellValue('A1', '该时间段没有产生数据,请重新选择时间');
            $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);//设置是否加粗
        }

        foreach($excelTableData as $k=>$v){
            //多个Sheet工作簿
            if($k !== 0) $phpExcel->createSheet();
            $phpExcel->setactivesheetindex($k);
            $phpExcel->getActiveSheet($k)->setTitle($sheetName[$k]);
            $phpExcel->getActiveSheet($k)->freezePane('A2');//冻结窗口

            //活动统计信息
            if($k == 0){
                $count = count($topic_field);
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $topic_field_name[$topic_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    if(!$v) continue;
                    foreach($v as $key=>$val){ //内容行
                        $value = $val[$topic_field[$i]];

                        //内容长度  标题内容取最大值
                        $title_len = strlen($topic_field_name[$topic_field[$i]]);
                        $value_len = strlen($value);
                        $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;

                        //插入行位置
                        $keys = $key+2;
                        if($topic_field[$i] == 'start_time'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        }elseif($topic_field[$i] == 'end_time'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        }else{
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                        }
                        if($strlen < 20) $strlen = 20;
                        $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                        $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . $keys)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    }
                }
            }

            //活动商品信息
            if($k == 1){
                $count = count($goods_field);
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $goods_field_name[$goods_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    if (!$v) continue;
                    foreach ($v as $key => $val) {//内容行
                        if (!$val) continue;
                        $value = $val[$goods_field[$i]];

                        //内容长度 取最大值
                        $title_len = strlen($goods_field_name[$goods_field[$i]]);
                        $value_len = strlen($value);
                        $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;
                        //插入行位置
                        $keys = $key + 2;
                        if ($goods_field[$i] == 'start_time') {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        } elseif ($goods_field[$i] == 'end_time') {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        } elseif ($goods_field[$i] == 'conversionRate') {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value . '%');
                        } elseif ($goods_field[$i] == 'retention_time') {
                            $value = bcdiv($val[$goods_field[$i]], 3600, 2);
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value . '小时');
                        } else {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                        }
                        if ($strlen < 20) $strlen = 20;
                        $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                        $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . $keys)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    }
                }
            }
        }

        // 设置第一个sheet为工作的sheet
        $phpExcel->setActiveSheetIndex(0);

        // 保存Excel 2007格式文件，保存路径为当前路径，名字为export.xlsx
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $file = '专题活动统计.xlsx';
        $objWriter->save($file);

        header("Content-type:application/octet-stream");

        $filename = basename($file);
        header("Content-Disposition:attachment;filename = " . $filename);
        header("Accept-ranges:bytes");
        header("Accept-length:" . filesize($file));
        readfile($file);
        unlink($file);
        exit;
    }
}