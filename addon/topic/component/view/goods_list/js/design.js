/**
 * 空的验证组件，后续如果增加业务，则更改组件
 */
var goodsListHtml = '<div class="topic-goods-list-edit layui-form">';

		goodsListHtml += '<div class="layui-form-item">';
			goodsListHtml += '<label class="layui-form-label sm">专题活动</label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<div v-bind:class="{  \'layui-unselect layui-form-select\' : true, \'layui-form-selected\' : activityIsShow }" v-on:click="activityIsShow=!activityIsShow;">';
					goodsListHtml += '<div class="layui-select-title">';
						goodsListHtml += '<input type="text"  v-model="activityList[activityIndex].topic_name" readonly="readonly" class="layui-input layui-unselect">';
						goodsListHtml += '<i class="layui-edge"></i>';
					goodsListHtml += '</div>';
						goodsListHtml += '<dl class="layui-anim layui-anim-upbit">';
						goodsListHtml += '<dd v-for="(item,index) in activityList" v-bind:value="item.topic_id" v-bind:class="{ \'layui-this\' : (index==activityIndex) }" v-on:click.stop="data.activityId=item.topic_id;activityIsShow=false;activityIndex=index;">{{item.topic_name}} {{ item.topic_id != 0 ? ("("+ item.status_name +")") : "" }}</dd>';
					goodsListHtml += '</dl>';
				goodsListHtml += '</div>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';

		goodsListHtml += '<div class="layui-form-item" v-if="activityIndex != 0">';
			goodsListHtml += '<label class="layui-form-label sm">商品来源</label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<template v-for="(item,index) in goodsSources" v-bind:k="index">';
					goodsListHtml += '<div v-on:click="data.sources=item.value" v-bind:class="{ \'layui-unselect layui-form-radio\' : true,\'layui-form-radioed\' : (data.sources==item.value) }"><i class="layui-anim layui-icon">&#xe643;</i><div>{{item.text}}</div></div>';
				goodsListHtml += '</template>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';
		
		goodsListHtml += '<div class="layui-form-item" v-if="activityIndex != 0 && isLoad && data.sources == \'category\'">';
			goodsListHtml += '<label class="layui-form-label sm">一级分类</label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<div v-bind:class="{  \'layui-unselect layui-form-select\' : true, \'layui-form-selected\' : isShow }" v-on:click="isShow=!isShow;">';
					goodsListHtml += '<div class="layui-select-title">';
						goodsListHtml += '<input type="text"  v-model="categoryList[selectIndex].category_name" readonly="readonly" class="layui-input layui-unselect">';
						goodsListHtml += '<i class="layui-edge"></i>';
					goodsListHtml += '</div>';
					goodsListHtml += '<dl class="layui-anim layui-anim-upbit">';
						goodsListHtml += '<dd v-for="(item,index) in categoryList" v-bind:value="item.category_id" v-bind:class="{ \'layui-this\' : (data.categoryId==item.category_id) }" v-on:click.stop="data.categoryId=item.category_id;isShow=false;selectIndex=index;getGoodsCategory(2,item.category_id)">{{item.category_name}}</dd>';
					goodsListHtml += '</dl>';
				goodsListHtml += '</div>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';

		goodsListHtml += '<div class="layui-form-item" v-if="activityIndex != 0 && twoIsLoad && data.sources == \'category\'">';
			goodsListHtml += '<label class="layui-form-label sm">二级分类</label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<div v-bind:class="{  \'layui-unselect layui-form-select\' : true, \'layui-form-selected\' : twoIsShow }" v-on:click="twoIsShow=!twoIsShow;">';
					goodsListHtml += '<div class="layui-select-title">';
						goodsListHtml += '<input type="text" placeholder="请选择" v-bind:value="twoCategoryList[twoSelectIndex].category_name" readonly="readonly" class="layui-input layui-unselect">';
						goodsListHtml += '<i class="layui-edge"></i>';
					goodsListHtml += '</div>';
					goodsListHtml += '<dl class="layui-anim layui-anim-upbit">';
						goodsListHtml += '<dd v-for="(item,index) in twoCategoryList" v-bind:value="item.category_id" v-bind:class="{ \'layui-this\' : (data.twoCategoryId==item.category_id) }" v-on:click.stop="data.twoCategoryId=item.category_id;twoIsShow=false;twoSelectIndex=index;">{{item.category_name}}</dd>';
					goodsListHtml += '</dl>';
				goodsListHtml += '</div>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';
		
		goodsListHtml += '<div class="layui-form-item" v-if="activityIndex != 0 && isLoad && data.sources == \'diy\'">';
			goodsListHtml += '<label class="layui-form-label sm">手动选择</label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<a href="#" class="ns-input-text ns-text-color" v-on:click="addGoods">选择</a>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';
		
		goodsListHtml += '<div class="layui-form-item" v-if="activityIndex != 0 && data.sources != \'diy\'">';
			goodsListHtml += '<label class="layui-form-label sm">商品数量</label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<input type="number" class="layui-input goods-account" v-on:keyup="shopNum" v-model="data.goodsCount" placeholder="0表示不限数量"/>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';
		
		goodsListHtml += '<div class="layui-form-item" v-if="activityIndex != 0 && data.sources != \'diy\'">';
			goodsListHtml += '<label class="layui-form-label sm"></label>';
			goodsListHtml += '<div class="layui-input-block">';
				goodsListHtml += '<template v-for="(item,index) in goodsCount" v-bind:k="index">';
					goodsListHtml += '<div v-on:click="data.goodsCount=item.value" v-bind:class="{ \'layui-unselect layui-form-radio\' : true,\'layui-form-radioed\' : (data.goodsCount !== \'\' && data.goodsCount==item.value) }"><i class="layui-anim layui-icon">&#xe643;</i><div>{{item.key}}</div></div>';
				goodsListHtml += '</template>';
			goodsListHtml += '</div>';
		goodsListHtml += '</div>';
		
	goodsListHtml += '</div>';
var select_goods_list = []; //配合商品选择器使用
Vue.component("topic-goods-list", {
	template: goodsListHtml,
	data: function () {
		return {
			data: this.$parent.data,
			//优惠券活动相关
			activityIsShow:false,
			activityList:[
				{
					topic_id : 0,
					topic_name : '请选择',
				}
			],
			activityIndex:0,
			goodsSources: [
				{
					text: "默认",
					value: "default"
				},
				{
					text: "商品分类",
					value: "category"
				},
				{
					text : "手动选择",
					value : "diy"
				}
			],
			categoryList: [],
			twoCategoryList:[],
			isLoad: false,
			twoIsLoad:false,
			isShow: false,
			twoIsShow: false,
			selectIndex: 0,//当前选中的下标
			twoSelectIndex:0,//二级选中下标
			goodsCount: [
				{key: 10, value: 10},
				{key: 20, value: 20},
				{key: 30, value: 30},
				{key: 40, value: 40},
				{key: '不限', value: 0},
			]
		}
	},
	created:function() {
		if(!this.$parent.data.verify) this.$parent.data.verify = [];
		this.$parent.data.verify.push(this.verify);//加载验证方法
		this.getActivityList();
		this.getGoodsCategory(1,0);
		this.getGoodsCategory(2,this.$parent.data.categoryId);
	},
	methods: {
		shopNum: function(){
			if (this.$parent.data.goodsCount.length > 0 && this.$parent.data.goodsCount < 0) {
				layer.msg("商品数量不能小于0");
				this.$parent.data.goodsCount = 0;
			}
		},
		verify : function () {
			var res = { code : true, message : "" };
			if(this.$parent.data.goodsCount.length===0) {
				res.code = false;
				res.message = "请输入商品数量";
			}
			if (this.$parent.data.goodsCount < 0) {
				res.code = false;
				res.message = "商品数量不能小于0";
			}
			if (this.$parent.data.goodsCount > 50){
				res.code = false;
				res.message = "商品数量最多为50";
			}
			return res;
		},
		//获取活动列表
		getActivityList(){
			var self = this;
			$.ajax({
				url : ns.url('topic://' + module+"/topic/lists"),
				dataType: 'JSON',
				type: 'post',
				data: {},
				success: function(res) {
					if(res.data){
						self.activityList = res.data.list;
						self.activityList.unshift({
							topic_id : 0,
							topic_name : '请选择',
						});
						for(let i in self.activityList){
							if(self.activityList[i].topic_id == self.data.activityId){
								self.activityIndex = i;
							}
						}
					}
				}
			})
		},
		getGoodsCategory(level,pid){
			var self = this;
			$.ajax({
				url : ns.url(module+"/goodscategory/getCategoryList"),
				dataType: 'JSON',
				type: 'post',
				data: {'level':level, 'pid':pid},
				success: function(res) {
					if(res.data){
						let categorylist = res.data;
						categoryList = res.data;
						categoryList.splice(0,0,{ category_name : "请选择", category_id : "" });
						categoryList.push({});
						categoryList.pop();
						if(level == 1){
							self.categoryList = categorylist;
							self.isLoad = true;
							for(var i=0;i<self.categoryList.length;i++){
								if(self.categoryList[i].category_id == self.data.categoryId){
									self.selectIndex = i;
									break;
								}
							}
						}else{
							self.twoCategoryList = categorylist;
							self.twoIsLoad = true;
							for(var i=0;i<self.twoCategoryList.length;i++){
								if(self.twoCategoryList[i].category_id == self.data.twoCategoryId){
									self.twoSelectIndex = i;
									break;
								}
							}
						}
					}
				}
			})
		},
		addGoods: function(){
			var self = this;
			goodsSelect(function (res) {
				self.$parent.data.goodsId = [];
				self.$parent.data.skuId = [];
				for (var i=0; i<res.length; i++) {
					self.$parent.data.goodsId.push(res[i].goods_id);
					self.$parent.data.skuId.push(res[i].sku_id);
				}
			}, '', {
				url:ns.url("topic://admin/goodssku/goodsSelect", {mode: "spu", disabled:0, promotion: "all", activity_id : self.data.activityId, select_id:self.$parent.data.skuId.toString()}),
			});
		}
	}
});

var goodsListStyleHtml = '<div class="layui-form-item">';
		goodsListStyleHtml += '<label class="layui-form-label sm">选择风格</label>';
		goodsListStyleHtml += '<div class="layui-input-block">';
			goodsListStyleHtml += '<div class="ns-input-text ns-text-color selected-style" v-on:click="selectGoodsStyle">选择</div>';
		goodsListStyleHtml += '</div>';
	goodsListStyleHtml += '</div>';

Vue.component("topic-goods-list-style", {
	template: goodsListStyleHtml,
	data: function() {
		return {
			data: this.$parent.data,
		}
	},
	created:function() {
		if(!this.$parent.data.verify) this.$parent.data.verify = [];
		this.$parent.data.verify.push(this.verify);//加载验证方法
	},
	methods: {
		verify: function () {
			var res = { code: true, message: "" };
			return res;
		},
		selectGoodsStyle: function() {
			var self = this;
			layer.open({
				type: 1,
				title: '风格选择',
				area:['930px','630px'],
				btn: ['确定', '返回'],
				content: $(".draggable-element[data-index='" + self.data.index + "'] .edit-attribute .goods-list-style").html(),
				success: function(layero, index) {
					$(".layui-layer-content input[name='style']").val(self.data.style);
					$("body").on("click", ".layui-layer-content .style-list-con-goods .style-li-goods", function () {
						$(this).addClass("selected ns-border-color").siblings().removeClass("selected ns-border-color");
						$(".layui-layer-content input[name='style']").val($(this).index() + 1);
					});
				},
				yes: function (index, layero) {
					self.data.style = $(".layui-layer-content input[name='style']").val();
					layer.closeAll()
				}
			});
		},
	}
});

// 是否启用更多按钮设置
var moreBtnHtml = '<div class="layui-form-item">';
	moreBtnHtml +=	 '<label class="layui-form-label sm">{{data.label}}</label>';
	moreBtnHtml +=	 '<div class="layui-input-block">';
	moreBtnHtml +=		 '<template v-for="(item,index) in list" v-bind:k="index">';
	moreBtnHtml +=			 '<div v-on:click="parent[data.field]=item.value" v-bind:class="{ \'layui-unselect layui-form-radio\' : true,\'layui-form-radioed\' : (parent[data.field]==item.value) }"><i class="layui-anim layui-icon">&#xe643;</i><div>{{item.label}}</div></div>';
	moreBtnHtml +=		 '</template>';
	moreBtnHtml +=	 '</div>';
	moreBtnHtml += '</div>';

Vue.component("topic-goods-list-more-btn", {
	props: {
		data: {
			type: Object,
			default: function () {
				return {
					field: "isShowCart",
					label: "是否启用"
				};
			}
		}
	},
	created: function () {
		if(!this.$parent.data.verify) this.$parent.data.verify = [];
		this.$parent.data.verify.push(this.verify);//加载验证方法
		if (this.data.label == undefined) this.data.label = "启用";
		if (this.data.field == undefined) this.data.field = "isShowCart";
	},
	watch: {
		data: function (val, oldVal) {
			if (val.field == undefined) val.field = oldVal.field;
			if (val.label == undefined) val.label = "启用";
		},
	},
	template: moreBtnHtml,
	data: function () {
		return {
			list: [
				{label: "是", value: 1},
				{label: "否", value: 0},
			],
			parent: this.$parent.data,
		};
	},
	methods: {
		verify : function () {
			var res = { code : true, message : "" };
			return res;
		},
	},
});

// 购物车按钮
var cartStyleHtml = '<div class="layui-form-item">';
		cartStyleHtml += '<label class="layui-form-label sm">选择风格</label>';
		cartStyleHtml += '<div class="layui-input-block">';
			cartStyleHtml += '<div class="ns-input-text ns-text-color selected-style" v-on:click="selectTestStyle">选择</div>';
		cartStyleHtml += '</div>';
	cartStyleHtml += '</div>';

Vue.component("cart-style", {
	template: cartStyleHtml,
	data: function() {
		return {
			data: this.$parent.data,
		}
	},
	created:function() {
		if(!this.$parent.data.verify) this.$parent.data.verify = [];
		this.$parent.data.verify.push(this.verify);//加载验证方法
	},
	methods: {
		verify: function () {
			var res = { code: true, message: "" };
			return res;
		},
		selectTestStyle: function() {
			var self = this;
			layer.open({
				type: 1,
				title: '风格选择',
				area:['930px','630px'],
				btn: ['确定', '返回'],
				content: $(".draggable-element[data-index='" + self.data.index + "'] .edit-attribute .cart-list-style").html(),
				success: function(layero, index) {
					$(".layui-layer-content input[name='cart_style']").val(self.data.style);
					$("body").on("click", ".layui-layer-content .cart-list-con .cart-li", function () {
						$(this).addClass("selected ns-border-color").siblings().removeClass("selected ns-border-color");
						$(".layui-layer-content input[name='cart_style']").val($(this).index() + 1);
					});
				},
				yes: function (index, layero) {
					self.data.cartStyle = $(".layui-layer-content input[name='cart_style']").val();
					layer.closeAll()
				}
			});
		},
	}
});

// 多选
var showContentHtml = '<div class="layui-form-item goods-show-box">';
	showContentHtml +=	'<div class="layui-input-inline">';
		showContentHtml +=		'<div v-on:click="changeStatus(\'isShowGoodName\')" class="layui-unselect layui-form-checkbox" v-bind:class="{\'layui-form-checked\': (data.isShowGoodName == 1)}" lay-skin="primary"><span>商品名称</span><i class="layui-icon layui-icon-ok"></i></div>';
		showContentHtml +=		'<div v-on:click="changeStatus(\'isShowGoodSubTitle\')" class="layui-unselect layui-form-checkbox" v-bind:class="{\'layui-form-checked\': (data.isShowGoodSubTitle == 1)}" lay-skin="primary"><span>副标题</span><i class="layui-icon layui-icon-ok"></i></div>';
		showContentHtml +=		'<div v-on:click="changeStatus(\'isShowMarketPrice\')" class="layui-unselect layui-form-checkbox" v-bind:class="{\'layui-form-checked\': (data.isShowMarketPrice == 1)}" lay-skin="primary"><span>划线市场价</span><i class="layui-icon layui-icon-ok"></i></div>';
		showContentHtml +=		'<div v-on:click="changeStatus(\'isShowGoodSaleNum\')" class="layui-unselect layui-form-checkbox" v-bind:class="{\'layui-form-checked\': (data.isShowGoodSaleNum == 1)}" lay-skin="primary"><span>商品销量</span><i class="layui-icon layui-icon-ok"></i></div>';
	showContentHtml +=	'</div>';
	showContentHtml += '</div>';

Vue.component("show-content", {
	template: showContentHtml,
	data: function () {
		return {
			data: this.$parent.data,
			isShowGoodName: this.$parent.data.isShowGoodName,
			isShowMarketPrice: this.$parent.data.isShowMarketPrice,
		};
	},
	created: function () {
		if(!this.$parent.data.verify) this.$parent.data.verify = [];
		this.$parent.data.verify.push(this.verify);//加载验证方法
	},
	methods: {
		verify :function () {
			var res = { code: true, message: "" };
			return res;
		},
		changeStatus: function(field) {
			this.$parent.data[field] = this.$parent.data[field] ? 0 : 1;
		}
	}
});