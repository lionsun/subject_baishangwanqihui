<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\topic\admin\controller;

use addon\topic\model\Topic as TopicModel;
use addon\topic\model\TopicGoods as TopicGoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\web\AdvPosition;
use app\model\web\Adv as AdvModel;
use app\model\web\Pc as PcModel;
use app\model\web\WebSite as WebsiteModel;
use addon\topic\model\TopicBrowse as TopicBrowseModel;
use app\model\order\OrderCommon as OrderCommonModel;
use app\model\system\PageRetention as PageRerentionModel;


/**
 * 专题控制器
 */
class Topic extends BaseAdmin
{

    private $pc_model;
	/**
	 * 专题列表
	 */
	public function lists()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$topic_name = input('topic_name', '');
            $start_time = input('start_time', '');
            $end_time = input('end_time', '');

			$condition = [];
			$condition[] = [ 'topic_name', 'like', '%' . $topic_name . '%' ];
            if (!empty($start_time) && empty($end_time)) {
                $condition[] = ["start_time", ">=", date_to_time($start_time)];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = ["end_time", "<=", date_to_time($end_time)];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'start_time', '>=', date_to_time($start_time)];
                $condition[] = [ 'end_time', '<=', date_to_time($end_time)];
            }
			$order = 'modify_time desc,create_time desc';
			$field = '*';
			
			$topic_model = new TopicModel();
            $topic_browse_model = new TopicBrowseModel();
			$res = $topic_model->getTopicPageList($condition, $page, $page_size, $order, $field);
            $website_model = new WebsiteModel();
            $website_info = $website_model->getWebSite([['site_id', '=', 0]], '*');
            foreach($res['data']['list'] as $key=>$val){
                $res['data']['list'][$key]['status_name'] = $topic_model->getTopicStatusName($val['status']);
                $res['data']['list'][$key]['view_url'] = $website_info['data']['web_domain'] . '/diy?view_name=TOPIC_ID_' . $val['topic_id'];
                $topic_browse = $topic_browse_model->getTopicBrowseSum($val['topic_id'], $start_time, $end_time);
                $res['data']['list'][$key]['ip'] = $topic_browse[0]['ip'] ?: 0;
                $res['data']['list'][$key]['uv'] = $topic_browse[0]['uv'] ?: 0;
                $res['data']['list'][$key]['pv'] = $topic_browse[0]['pv'] ?: 0;
            }
			return $res;
		} else {
			$this->forthMenu();
			return $this->fetch("topic/lists");
		}
	}
	
	/**
	 * 添加专题活动
	 */
	public function add()
	{
		if (request()->isAjax()) {
			$topic_name = input("topic_name", '');
			$start_time = input("start_time", 0);
			$end_time = input("end_time", 0);
			$remark = input("remark", '');
			$topic_adv = input("topic_adv", '');
			$bg_color = input("bg_color", '#ffffff');
			$topic_model = new TopicModel();
			$data = array(
				"topic_name" => $topic_name,
				"start_time" => $start_time,
				"end_time" => $end_time,
				"remark" => $remark,
				"topic_adv" => $topic_adv,
				'bg_color' => $bg_color
			);
			$res = $topic_model->addTopic($data);
			return $res;
		} else {
			return $this->fetch("topic/add");
		}
	}
	
	/**
	 * 编辑专题活动
	 */
	public function edit()
	{
		$topic_id = input("topic_id", '');
		$topic_model = new TopicModel();
		if (request()->isAjax()) {
			$topic_name = input("topic_name", '');
			$start_time = input("start_time", 0);
			$end_time = input("end_time", 0);
			$remark = input("remark", '');
			$topic_adv = input("topic_adv", '');
			$bg_color = input("bg_color", '#ffffff');
			
			$data = array(
				"topic_name" => $topic_name,
				"start_time" => $start_time,
				"end_time" => $end_time,
				"remark" => $remark,
				"topic_adv" => $topic_adv,
				"topic_id" => $topic_id,
				'bg_color' => $bg_color
			);
			$res = $topic_model->editTopic($data);
			return $res;
		} else {
			$condition = array(
				[ "topic_id", "=", $topic_id ]
			);
			$topic_info_result = $topic_model->getTopicInfo($condition);
			$this->assign("info", $topic_info_result["data"]);
			return $this->fetch("topic/edit");
		}
	}
	
	/**
	 * 删除专题活动
	 */
	public function delete()
	{
		$topic_id = input("topic_id", '');
		$topic_model = new TopicModel();
		$res = $topic_model->deleteTopic($topic_id);
		return $res;
	}
	
	/**
	 * 专题活动商品列表
	 */
	public function goods()
	{
		$topic_id = input("topic_id", 0);
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
            $sku_name = input('sku_name', '');

			$condition = [];
			$condition[] = [ 'nptg.topic_id', '=', $topic_id ];
			$condition[] = [ 'ngs.sku_name', 'like', '%' . $sku_name . '%' ];
			$order = '';
			$field = '';
			$topic_goods_model = new TopicGoodsModel();
            $alias = 'nptg';
            $join = [
                [ 'goods_sku ngs', 'nptg.sku_id = ngs.sku_id', 'inner' ],
                [ 'promotion_topic npt', 'nptg.topic_id = npt.topic_id', 'inner' ]
            ];
			$res = $topic_goods_model->getTopicGoodsPageList($condition, $page, $page_size, $order, $field, $alias, $join);
            $topic_browse_model = new TopicBrowseModel();
            $order_common_model = new OrderCommonModel();
            $page_rerention_model = new PageRerentionModel();

            foreach($res['data']['list'] as $k=>$v){
                $res['data']['list'][$k]['rerention_time'] = $page_rerention_model->getRetentionTime($v['sku_id'], $v['start_time'], $v['end_time']);
                $res['data']['list'][$k]['view_count'] = $topic_browse_model->getTopicGoodsViewCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);
                $res['data']['list'][$k]['order_count'] = $order_common_model->getTopicGoodsOrderCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);

                if($res['data']['list'][$k]['view_count'] == 0 || $res['data']['list'][$k]['order_count'] == 0){
                    $res['data']['list'][$k]['conversionRate'] = 0;
                }else{
                    $res['data']['list'][$k]['conversionRate'] = bcdiv($res['data']['list'][$k]['view_count'], $res['data']['list'][$k]['order_count'], 2) * 100;
                }
                $res['data']['list'][$k]['rerention_time'] = bcdiv($res['data']['list'][$k]['rerention_time'], 3600, 2);
            }
			return $res;
		} else {
			$topic_model = new TopicModel();
			
			$topic_info = $topic_model->getTopicInfo([ 'topic_id' => $topic_id ], '*');
			$this->assign("topic_id", $topic_id);
			$this->assign("topic_info", $topic_info['data']);
			return $this->fetch("topic/goods");
		}
	}
	
	/**
	 * 专题活动商品列表
	 */
	public function goodslist()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$sku_name = input('sku_name', '');
			
			$condition = [];
			$condition[] = [ 'ngs.sku_name', 'like', '%' . $sku_name . '%' ];
			$order = '';
			$field = '*';
			$topic_goods_model = new TopicGoodsModel();
            $alias = 'nptg';
            $join = [
                [ 'goods_sku ngs', 'nptg.sku_id = ngs.sku_id', 'inner' ],
                [ 'promotion_topic npt', 'nptg.topic_id = npt.topic_id', 'inner' ],
            ];
            $res = $topic_goods_model->getTopicGoodsPageList($condition, $page, $page_size, $order, $field, $alias, $join);
			return $res;
		} else {
			$this->forthMenu();
			return $this->fetch("topic/goodslist");
		}
	}
	
	/**
	 * 删除专题活动商品
	 */
	public function deleteTopicGoods()
	{
		if (request()->isAjax()) {
			$topic_id = input("topic_id", 0);
			$sku_id = input("sku_id", 0);
			$site_id = input("site_id", 0);
			$topic_goods_model = new TopicGoodsModel();
			$res = $topic_goods_model->deleteTopicGoods($topic_id, $site_id, $sku_id);
			return $res;
		}
	}


    /**
     * 活动页面设置
     */
    public function setting(){
        $topic_id = input("topic_id", 0);
        $keyword = "TOPIC_ID_".$topic_id;
        $adv_position = new AdvPosition();
        $ap_info = $adv_position->getAdvPositionInfo([ [ 'keyword', '=', $keyword ] ]);
        if (request()->isAjax()) {
            if($ap_info[ 'data' ]){
                $data = [
                    'ap_name' => input('ap_name', ''),
                    'keyword' => input('keyword', $keyword),
                    'ap_intro' => input('ap_intro', ''),
                    'ap_height' => input('ap_height', 0),
                    'ap_width' => input('ap_width', 0),
                    'default_content' => input('default_content', ''),
                    'ap_background_color' => input('ap_background_color', ''),
                    'type' => input('type', ''),
                ];
                return $adv_position->editAdvPosition($data, [ [ 'keyword', '=', $keyword ] ]);
            }else{
                $data = [
                    'ap_name' => input('ap_name', ''),
                    'keyword' => input('keyword', $keyword),
                    'ap_intro' => input('ap_intro', ''),
                    'ap_height' => input('ap_height', 0),
                    'ap_width' => input('ap_width', 0),
                    'default_content' => input('default_content', ''),
                    'ap_background_color' => input('ap_background_color', ''),
                    'type' => input('type', ''),
                ];
                return $adv_position->addAdvPosition($data);
            }
        } else {
            $this->assign('keyword',$keyword);
            $this->assign('topic_id',$topic_id);
            if($ap_info[ 'data' ]){
                $this->assign('ap_id', $ap_info[ 'data' ]['ap_id']);
            }else{
                $this->assign('ap_id', 0);
            }
            $this->assign('info', $ap_info[ 'data' ]);
            $adv_position = new AdvPosition();
            $position_list = $adv_position->getAdvPositionList([], '*', 'ap_id desc');
            $this->assign('position_list', $position_list['data']);
            return $this->fetch("topic/setting");
        }
    }

    //广告位
    public function advlists(){
        $adv = new AdvModel();
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', '');
            $ap_id = input('ap_id', '');

            $condition = [];
            if (!empty($search_text)) {
                $condition[] = [ 'a.adv_title', 'like', '%' . $search_text . '%' ];
            }
            if ($ap_id !== '') {
                $condition[] = [ 'a.ap_id', '=', $ap_id ];
            }

            if ($ap_id == 0) {
                $condition[] = [ 'a.ap_id', '=', $ap_id ];
            }else{
                return $adv->getAdvPageList($condition, $page, $page_size);
            }
        }
    }


    /**
     * 添加广告
     */
    public function addAdv()
    {
        $adv = new AdvModel();
        $ap_id = input('ap_id', 0);
        if (request()->isAjax()) {
            $data = [
                'ap_id' => input('ap_id', 0),
                'adv_title' => input('adv_title', ''),
                'adv_url' => input('adv_url', ''),
                'adv_image' => input('adv_image', ''),
                'slide_sort' => input('slide_sort', 0),
                'price' => input('price', 0),
                'type' => input('type', 1),
                'background' => input('background', ''),
            ];
            return $adv->addAdv($data);
        } else {
            $pc_model = new PcModel();
            $pc_link = $pc_model->getLink();
            $this->assign("pc_link", $pc_link);
            $this->assign("ap_id", $ap_id);
            $adv_position = new AdvPosition();
            $adv_position_list = $adv_position->getAdvPositionList();
            $this->assign('adv_position_list', $adv_position_list[ 'data' ]);
            return $this->fetch("topic/add_adv");
        }
    }


    /**
     * 编辑广告
     */
    public function editAdv()
    {
        $adv_id = input('adv_id', '');
        $adv = new AdvModel();
        if (request()->isAjax()) {
            $data = [
                'ap_id' => input('ap_id', 0),
                'adv_title' => input('adv_title', ''),
                'adv_url' => input('adv_url', ''),
                'adv_image' => input('adv_image', ''),
                'slide_sort' => input('slide_sort', 0),
                'price' => input('price', 0),
                'background' => input('background', ''),
            ];
            return $adv->editAdv($data, [ [ 'adv_id', '=', $adv_id ] ]);
        } else {
            $pc_model = new PcModel();
            $pc_link = $pc_model->getLink();
            $this->assign("pc_link", $pc_link);

            $adv_position = new AdvPosition();
            $adv_position_list = $adv_position->getAdvPositionList();
            $this->assign('adv_position_list', $adv_position_list[ 'data' ]);
            $adv_info = $adv->getAdvInfo($adv_id);
            $this->assign('adv_info', $adv_info[ 'data' ]);

            // 得到当前广告图类型
            $type = 1;// 1 pc、2 wap
            foreach ($adv_position_list[ 'data' ] as $k => $v) {
                if ($v[ 'ap_id' ] == $adv_info[ 'data' ][ 'ap_id' ]) {
                    $type = $v[ 'type' ];
                    break;
                }
            }
            $this->assign("type", $type);

            return $this->fetch("topic/edit_adv");
        }
    }


    /**
     * 编辑楼层
     * @return mixed
     */
    public function editFloor()
    {
        $pc_model = new PcModel();
        if (request()->isAjax()) {
            $id = input("id", 0);
            $data = [
                'block_id' => input("block_id", 0), //楼层模板关联id
                'title' => input("title", ''), // 楼层标题
                'value' => input("value", ''),
                'state' => input("state", 0),// 状态（0：禁用，1：启用）
                'sort' => input("sort", 0), //排序号
                'view_name' => input('view_name', ''),//自定义页面名称
            ];
            if ($id == 0) {
                $res = $pc_model->addFloor($data);
            } else {
                $res = $pc_model->editFloor($data, [['id', '=', $id]]);
            }
            return $res;
        } else {
            $id = input("id", 0);
            $this->assign("id", $id);

            //专题活动id
            $topic_id = input('topic_id', 0);
            $this->assign('topic_id', $topic_id);

            $view_name = input('view_name', '');
            if (!empty($id)) {
                $floor_info = $pc_model->getFloorDetail($id);
                $floor_info = $floor_info['data'];
                $this->assign("floor_info", $floor_info);

                $floor_value = json_decode($floor_info['value'], true);
                $this->assign('floor_value', $floor_value);

                $view_name = $floor_info['view_name'];
            } else {
                $this->assign('floor_value', null);
            }
            $this->assign('view_name', $view_name);


            $floor_block_list = $pc_model->getFloorBlockList();
            $floor_block_list = $floor_block_list['data'];
            $this->assign("floor_block_list", $floor_block_list);

            $pc_link = $pc_model->getLink();
            $this->assign("pc_link", $pc_link);

            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryTree();
            $category_list = $category_list['data'];
            $this->assign("category_list", $category_list);


            $diy_view_info = $pc_model->getPcSiteDiyViewInfo([['name', '=', $view_name]]);
            $this->assign('diy_view_info', $diy_view_info['data']);

            return $this->fetch('topic/edit_floor');
        }
    }
    /**
     * 营销活动导出
     */
    public function exportTopic(){
        $start_time = input('start_time', 0);
        $end_time = input('end_time', 0);
        $topic_name = input('topic_name', '');
        if (!empty($start_time) && empty($end_time)) {
            $condition[] = ["start_time", ">=", date_to_time($start_time)];
            $goods_condition[] = ["nptg.start_time", ">=", date_to_time($start_time)];
        } elseif (empty($start_time) && !empty($end_time)) {
            $condition[] = ["end_time", "<=", date_to_time($end_time)];
            $goods_condition[] = ["nptg.end_time", "<=", date_to_time($end_time)];
        } elseif (!empty($start_time) && !empty($end_time)) {
            $condition[] = [ 'start_time', '>=', date_to_time($start_time)];
            $condition[] = [ 'end_time', '<=', date_to_time($end_time)];
            $goods_condition[] = [ 'nptg.start_time', '>=', date_to_time($start_time)];
            $goods_condition[] = [ 'nptg.end_time', '<=', date_to_time($end_time)];
        }
        $condition[] = [ 'topic_name', 'like', '%' . $topic_name . '%' ];
        $goods_condition[] = [ 'npt.topic_name', 'like', '%' . $topic_name . '%' ];


        //专题活动列表
        $topic_model = new TopicModel();
        $topic_browse_model = new TopicBrowseModel();
        $topic_list = $topic_model->getExportTopicList($condition);
        foreach($topic_list as $key=>$val){
            $topic_browse = $topic_browse_model->getTopicBrowseSum($val['topic_id'], date('Y-m-d H:i:s', $val['start_time']), date('Y-m-d H:i:s', $val['end_time']));
            $topic_list[$key]['ip'] = $topic_browse[0]['ip'] ?: 0;
            $topic_list[$key]['uv'] = $topic_browse[0]['uv'] ?: 0;
            $topic_list[$key]['pv'] = $topic_browse[0]['pv'] ?: 0;
        }

        //活动商品列表
        $topic_goods_model = new TopicGoodsModel();
        $order_common_model = new OrderCommonModel();
        $topic_goods_list = $topic_goods_model->getExportTopicGoodsList($goods_condition);
        foreach($topic_goods_list as $k=>$v){
            $topic_goods_list[$k]['view_count'] = $topic_browse_model->getTopicGoodsViewCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);
            $topic_goods_list[$k]['order_count'] = $order_common_model->getTopicGoodsOrderCount($v['topic_id'], $v['sku_id'], $v['start_time'], $v['end_time']);

            if($topic_goods_list[$k]['view_count'] == 0 || $topic_goods_list[$k]['order_count'] == 0){
                $topic_goods_list[$k]['conversionRate'] = 0;
            }else{
                $topic_goods_list[$k]['conversionRate'] = bcdiv($topic_goods_list[$k]['view_count'], $topic_goods_list[$k]['order_count'], 2) * 100;
            }
        }

        $header_arr = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
        );
        //处理数据
        $excelTableData = [];
        $excelTableData[] = $topic_list;
        $excelTableData[] = $topic_goods_list;

        //接收需要展示的字段
        $topic_field = ["topic_id", "topic_name", "start_time", "end_time", "status", "ip", "uv", "pv"];
        $goods_field = ["topic_id", "start_time", "end_time", "sku_id", "goods_name", "retention_time", "view_count", "order_count", "conversionRate"];

        $topic_field_name = [
            "topic_id" => '专题活动编号',
            "topic_name" => '专题活动名称',
            "start_time" => '活动开始时间',
            "end_time" => '活动结束时间',
            "status" => '活动状态',
            "ip" => 'IP数',
            "uv" => 'UV数',
            "pv" => 'PV数'
        ];
        $goods_field_name = [
            "topic_id" => '专题活动编码',
            "start_time" => '活动开始时间',
            "end_time" => '活动结束时间',
            "sku_id" => 'SKU编码',
            "goods_name" => '商品名称',
            "retention_time" => '留存时间',
            "view_count" => '访问量',
            "order_count" => '成交量',
            "conversionRate" => '商品转化率'
        ];
        $sheetName = ['专题活动统计-活动信息', '专题活动统计-商品信息'];

        // 实例化excel
        $phpExcel = new \PHPExcel();
        if(empty($excelTableData)){
            $phpExcel->setactivesheetindex(0);
            $phpExcel->getActiveSheet()->setTitle($sheetName[0]);
            $phpExcel->getActiveSheet()->setCellValue('A1', '该时间段没有产生数据,请重新选择时间');
            $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);//设置是否加粗
        }

        foreach($excelTableData as $k=>$v){
            //多个Sheet工作簿
            if($k !== 0) $phpExcel->createSheet();
            $phpExcel->setactivesheetindex($k);
            $phpExcel->getActiveSheet($k)->setTitle($sheetName[$k]);
            $phpExcel->getActiveSheet($k)->freezePane('A2');//冻结窗口

            //活动统计信息
            if($k == 0){
                $count = count($topic_field);
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $topic_field_name[$topic_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    if(!$v) continue;
                    foreach($v as $key=>$val){ //内容行
                        $value = $val[$topic_field[$i]];

                        //内容长度  标题内容取最大值
                        $title_len = strlen($topic_field_name[$topic_field[$i]]);
                        $value_len = strlen($value);
                        $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;

                        //插入行位置
                        $keys = $key+2;
                        if($topic_field[$i] == 'start_time'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        }elseif($topic_field[$i] == 'end_time'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        }else{
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                        }
                        if($strlen < 20) $strlen = 20;
                        $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                        $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . $keys)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    }
                }
            }

            //活动商品信息
            if($k == 1){
                $count = count($goods_field);
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $goods_field_name[$goods_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    if (!$v) continue;
                    foreach ($v as $key => $val) {//内容行
                        if (!$val) continue;
                        $value = $val[$goods_field[$i]];

                        //内容长度 取最大值
                        $title_len = strlen($goods_field_name[$goods_field[$i]]);
                        $value_len = strlen($value);
                        $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;
                        //插入行位置
                        $keys = $key + 2;
                        if ($goods_field[$i] == 'start_time') {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        } elseif ($goods_field[$i] == 'end_time') {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, date('Y-m-d H:i:s', $value));
                        } elseif ($goods_field[$i] == 'conversionRate') {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value . '%');
                        } elseif ($goods_field[$i] == 'retention_time') {
                            $value = bcdiv($val[$goods_field[$i]], 3600, 2);
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value . '小时');
                        } else {
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                        }
                        if ($strlen < 20) $strlen = 20;
                        $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                        $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . $keys)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    }
                }
            }
        }

        // 设置第一个sheet为工作的sheet
        $phpExcel->setActiveSheetIndex(0);

        // 保存Excel 2007格式文件，保存路径为当前路径，名字为export.xlsx
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $file = '专题活动统计.xlsx';
        $objWriter->save($file);

        header("Content-type:application/octet-stream");

        $filename = basename($file);
        header("Content-Disposition:attachment;filename = " . $filename);
        header("Accept-ranges:bytes");
        header("Accept-length:" . filesize($file));
        readfile($file);
        unlink($file);
        exit;
    }
}