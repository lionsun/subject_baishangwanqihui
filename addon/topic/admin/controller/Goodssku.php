<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\topic\admin\controller;

use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use addon\topic\model\TopicGoods as TopicGoodsModel;
use app\model\goods\GoodsBrand as GoodsBrandModel;

/**
 * 专题活动控制器
 */
class Goodssku extends BaseAdmin
{
    /**
     * 商品选择
     * @return array|mixed|void
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $shop_name = input('shop_name', '');
            $min_price = input('min_price', 0);
            $max_price = input('max_price', 0);
            $goods_class = input('goods_class', "");// 商品类型，实物、虚拟
            $category_id = input('category_id', "");// 商品分类id
            $select_id = input('select_id', '');
            $search_type = input('search_type', 'all');
            $topic_id = input('activity_id', 0);
            $source = input('source', ''); //来源
            $brand_id = input('brand_id', ''); //品牌
            $is_need_card = input('is_need_card', ''); //海外购
            $alias = 'ag';
            $join = [
                ['goods_sku gs', 'ag.sku_id = gs.sku_id', 'left'],
                ['shop s', 'gs.site_id = s.site_id', 'left']
            ];
            $condition = [
                ['ag.topic_id', '=', $topic_id],
                ['gs.is_delete', '=', 0],
                ['gs.goods_state', '=', 1],
                ['gs.verify_state', '=', 1],
                ['s.shop_status', '=', 1],
            ];
            if (!empty($goods_name)) {
                $condition[] = ['gs.goods_name', 'like', '%' . $goods_name . '%'];
            }
            if (!empty($shop_name)) {
                $condition[] = ['gs.site_name', 'like', '%' . $shop_name . '%'];
            }
            if($search_type == 'checked'){
                $condition[] = ['gs.sku_id', 'in', $select_id];
            }
            if (!empty($category_id)) {
                $condition[] = ['gs.category_id_1|gs.category_id_2|gs.category_id_3', '=', $category_id];
            }

            if ($goods_class !== "") {
                $condition[] = ['gs.goods_class', '=', $goods_class];
            }

            if ($min_price != "" && $max_price != "") {
                $condition[] = ['gs.price', 'between', [$min_price, $max_price]];
            } elseif ($min_price != "") {
                $condition[] = ['gs.price', '<=', $min_price];
            } elseif ($max_price != "") {
                $condition[] = ['gs.price', '>=', $max_price];
            }

            if(!empty($source)){
                $condition[] = ['gs.source', '=', $source];
            }

            if(!empty($brand_id)){
                $condition[] = ['gs.brand_id', '=', $brand_id];
            }

            if($is_need_card!==''){
                if($is_need_card==1){
                    $condition[] = ['gs.trade_type', 'in', [2,3]];
                }else{
                    $condition[] = ['gs.trade_type', 'in', [0,1,4,5]];
                }
            }
            $order = 'gs.create_time desc';
            $field = 'ag.topic_price as price, ag.topic_price as discount_price, gs.sku_id,gs.goods_id,gs.sku_name,gs.sku_image,gs.price as market_price,gs.create_time,gs.is_virtual,gs.site_name';
            $topic_goods_model = new TopicGoodsModel();
            $goods_list = $topic_goods_model->getTopicGoodsPageList($condition, $page, $page_size, $order, $field,  $alias, $join);
            return $goods_list;
        } else {
            //已经选择的商品sku数据
            $select_id = input('select_id', '');
            $mode = input('mode', 'spu');
            $max_num = input('max_num', 0);
            $min_num = input('min_num', 0);
            $is_virtual = input('is_virtual', '');
            $disabled = input('disabled', 0);
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao
            $activity_id = input('activity_id', 0);//活动id

            $this->assign('select_id', $select_id);
            $this->assign('mode', $mode);
            $this->assign('max_num', $max_num);
            $this->assign('min_num', $min_num);
            $this->assign('is_virtual', $is_virtual);
            $this->assign('disabled', $disabled);
            $this->assign('promotion', $promotion);
            $this->assign('activity_id', $activity_id);

            // 营销活动
            $goods_promotion_type = event('GoodsPromotionType');
            $this->assign('promotion_type', $goods_promotion_type);

            //商品分类
            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList([], 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);

            //获取品牌;
            $goods_brand_model = new GoodsBrandModel();
            $brand_list = $goods_brand_model->getBrandList([['site_id', 'in', ("0,$this->site_id")]], "brand_id, brand_name");
            $brand_list = $brand_list[ 'data' ];
            $this->assign("brand_list", $brand_list);
            return $this->fetch("goodssku/goods_select");
        }
    }
}