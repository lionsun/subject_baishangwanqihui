<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\nthmfolddiscount\event;
use addon\nthmfolddiscount\model\Discount as DiscountModel;

/**
 * 活动展示
 */
class PromotionPrice
{

    /**
     * 活动详情
     * @param $sku_info
     */
	public function handle($sku_info)
	{
	    if(isset($sku_info['promotion_type']) && $sku_info['promotion_type'] == DiscountModel::PROMOTION_TYPE) {

	        if(isset($sku_info['promotion_info'])){
	            $discount_content = json_decode($sku_info['promotion_info']['info']['discount_content'], true);
	            $nthmfold_num = 0;
	            //最终要使用的折扣
                $use_array = [];
	            $use_nthmfold = [];
	            foreach($discount_content as $nthmfold){
	                if($sku_info['num'] >= $nthmfold['num'] && $nthmfold['num'] > $nthmfold_num){
                        $use_array[$nthmfold['num']] = $nthmfold['discount'];
                    }
                }
	            $old_price_count = 0;

                for ($i = 0; $i < $sku_info['num']; $i++) {
                    if (isset($use_array[$i+1])) {
                        $discount_arr = [
                            'num' => 1,
                            'discount' => $use_array[$i+1]
                        ];
                        $use_nthmfold[] =  $discount_arr;
                    } else {
                        $old_price_count += 1;
                    }
                }
                if($old_price_count!=0){
                    $old_price_arr = [
                        'num' => $old_price_count,
                        'discount' => 10
                    ];
                    $use_nthmfold[] = $old_price_arr;
                }

                $goods_money = 0;
	            if(!empty($use_nthmfold)){
                    $discount_price = $use_nthmfold;
                    $promotion_price = $use_nthmfold;
                    foreach ($use_nthmfold as $key=>$val){
                        $discount_price[$key]['discount_price'] = round($use_nthmfold[$key]['discount'] / 10 * $sku_info['price'],2);
                        $promotion_price[$key]['promotion_price'] = round($sku_info['price'] - $use_nthmfold[$key]['discount'] / 10 * $sku_info['discount_price'],2);
                        $goods_money += $use_nthmfold[$key]['discount'] / 10 * $sku_info['price']*$use_nthmfold[$key]['num'];
                    }
                    $price = $sku_info['discount_price'];
                    $sku_info['price'] = $price;
                    $sku_info['goods_money'] = $sku_info['price']*$sku_info['num'];
                    $sku_info['real_goods_money'] = $sku_info['goods_money'];
                    $sku_info['coupon_money'] = 0;//优惠券金额
                    $sku_info['promotion_money'] = round($sku_info['price']*$sku_info['num']-$goods_money,2);//优惠金额
                    $sku_info['discount_price'] = $price;
                    $sku_info['discount_price_data'] = $discount_price;
                    $sku_info['promotion_price'] = $sku_info['price'] - $sku_info['discount_price'];
                    $sku_info['promotion_price_data'] = $promotion_price;
                    return $sku_info;
                }
            }
        }
	}
}