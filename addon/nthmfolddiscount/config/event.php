<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //第n件m折开启
        'Opennthmfolddiscount'  => [
            'addon\nthmfolddiscount\event\OpenDiscount',
        ],
        //第n件m折关闭
        'Closenthmfolddiscount'  => [
            'addon\nthmfolddiscount\event\CloseDiscount',
        ],
        //展示活动
        'ShowPromotion' => [
            'addon\nthmfolddiscount\event\ShowPromotion',
        ],
        //检测相同时间段营销活动
        'CheckSameTimePromotion' => [
            'addon\nthmfolddiscount\event\CheckSameTimePromotion',
        ],
        //活动详情
        'PromotionInfo' => [
            'addon\nthmfolddiscount\event\PromotionInfo',
        ],
        //活动详情
        'PromotionPrice' => [
            'addon\nthmfolddiscount\event\PromotionPrice',
        ],
    ],

    'subscribe' => [
    ],
];
