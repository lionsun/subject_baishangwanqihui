<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\nthmfolddiscount\shop\controller;


use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\shop\ShopCategory;
use app\shop\controller\BaseShop;
use addon\nthmfolddiscount\model\Discount as DiscountModel;
use app\model\system\Promotion as PromotionModel;

/**
 * 第n件m折控制器
 */
class Discount extends BaseShop
{
	/**
	 * 添加活动
	 */
	public function add()
	{
		if (request()->isAjax()) {
			$data = [
				'discount_name' => input('discount_name', ''),
				'remark' => input('remark', ''),
				'start_time' => strtotime(input('start_time', '')),
				'end_time' => strtotime(input('end_time', '')),
				'site_id' => $this->site_id,
                'discount_content' => input('discount_content', ''),
                'discount_goods' => '',
			];
			
			$discount_model = new DiscountModel();
			return $discount_model->addDiscount($data);
		} else {
			return $this->fetch("discount/add");
		}
	}
	
	/**
	 * 编辑活动
	 */
	public function edit()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			$data = [
				'discount_name' => input('discount_name', ''),
				'remark' => input('remark', ''),
				'start_time' => strtotime(input('start_time', '')),
				'end_time' => strtotime(input('end_time', '')),
				'discount_id' => input('discount_id', 0),
				'site_id' => $this->site_id,
                'discount_content' => input('discount_content', ''),
			];
			return $discount_model->editDiscount($data);
			
		} else {
			$discount_id = input('discount_id', 0);
			$this->assign('discount_id', $discount_id);
			
			$discount_info = $discount_model->getDiscountInfo($discount_id, $this->site_id);
			$this->assign('discount_info', $discount_info['data']);
			return $this->fetch("discount/edit");
		}
		
	}
	
	/**
	 * 第n件m折详情
	 */
	public function detail()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			//活动商品
			$discount_id = input('discount_id', 0);
			$list = $discount_model->getDiscountGoods($discount_id);
			foreach ($list['data'] as $key => $val) {
				if ($val['price'] != 0) {
					$discount_rate = floor($val['discount_price'] / $val['price'] * 100);
				} else {
					$discount_rate = 100;
				}
				$list['data'][ $key ]['discount_rate'] = $discount_rate;
			}
			return $list;
		} else {
			$discount_id = input('discount_id', 0);
			$site_id = $this->site_id;
			$this->assign('discount_id', $discount_id);
			
			//活动详情
			$discount_info = $discount_model->getDiscountInfo($discount_id, $site_id);
			$this->assign('discount_info', $discount_info['data']);
			
			return $this->fetch("discount/detail");
		}
	}
	
	/**
	 * 活动列表
	 */
	public function lists()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$discount_name = input('discount_name', '');
			$status = input('status', '');
			
			$condition = [];
			if ($status !== "") {
				$condition[] = [ 'status', '=', $status ];
			}
			$condition[] = [ 'site_id', '=', $this->site_id ];
			$condition[] = [ 'discount_name', 'like', '%' . $discount_name . '%' ];
			$order = 'create_time desc';
			$field = 'discount_id, discount_name, start_time, end_time, status, create_time';
			
			$discount_status_arr = DiscountModel::getDiscountStatus();
			$res = $discount_model->getDiscountPageList($condition, $page, $page_size, $order, $field);
			foreach ($res['data']['list'] as $key => $val) {
				$res['data']['list'][ $key ]['status_name'] = $discount_status_arr[ $val['status'] ];
			}
			return $res;
			
		} else {
			//第n件m折状态
			$discount_status_arr = DiscountModel::getDiscountStatus();
			$this->assign('discount_status_arr', $discount_status_arr);
			
			return $this->fetch("discount/lists");
		}
	}
	
	/**
	 * 关闭活动
	 */
	public function close()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->closeDiscount($discount_id, $this->site_id);
		}
	}
	
	/**
	 * 删除活动
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->deleteDiscount($discount_id, $this->site_id);
		}
	}
	
	/**
	 * 第n件m折商品管理
	 */
	public function manage()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			//第n件m折商品列表
			$discount_id = input('discount_id', 0);
			
			$res = $discount_model->getDiscountGoods($discount_id);
			return $res;
		} else {
			$discount_id = input('discount_id', 0);
			$this->assign('discount_id', $discount_id);
			
			//活动详情
			$discount_info = $discount_model->getDiscountInfo($discount_id, $this->site_id);
			$this->assign('discount_info', $discount_info['data']);
			
			return $this->fetch("discount/manage");
		}
	}
	
	/**
	 * 添加商品
	 */
	public function addGoods()
	{
		if (request()->isAjax()) {
			$goods_ids = input('goods_ids', '');
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->addDiscountGoods($discount_id, $this->site_id, $goods_ids);
		}
	}
	
	/**
	 * 删除商品
	 */
	public function deleteGoods()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$goods_id = input('goods_id', '');
			$discount_model = new DiscountModel();
			return $discount_model->deleteDiscountGoods($discount_id, $goods_id, $this->site_id);
		}
	}

    /**
     * 商品选择组件
     * @return \multitype
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $goods_id = input('goods_id', 0);
            $is_virtual = input('is_virtual', '');// 是否虚拟类商品（0实物1.虚拟）
            $min_price = input('min_price', 0);
            $max_price = input('max_price', 0);
            $goods_class = input('goods_class', "");// 商品类型，实物、虚拟
            $category_id = input('category_id', "");// 商品分类id
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、fenxiao、bargain
            $promotion_type = input('promotion_type', "");
            $discount_id = input('discount_id', 0);//折扣id

            $condition = [
                ['is_delete', '=', 0],
                ['goods_state', '=', 1],
                ['site_id', '=', $this->site_id]
            ];


            if (!empty($goods_name)) {
                $condition[] = ['goods_name', 'like', '%' . $goods_name . '%'];
            }
            if ($is_virtual !== "") {
                $condition[] = ['is_virtual', '=', $is_virtual];
            }
            if (!empty($goods_id)) {
                $condition[] = ['goods_id', '=', $goods_id];
            }
            if (!empty($category_id)) {
                $condition[] = ['category_id', 'like', [$category_id, '%' . $category_id . ',%', '%' . $category_id, '%,' . $category_id . ',%'], 'or'];
            }

            if (!empty($promotion_type)) {
                $condition[] = ['promotion_addon', 'like', "%{$promotion_type}%"];
            }

            if ($goods_class !== "") {
                $condition[] = ['goods_class', '=', $goods_class];
            }

            if ($min_price != "" && $max_price != "") {
                $condition[] = ['price', 'between', [$min_price, $max_price]];
            } elseif ($min_price != "") {
                $condition[] = ['price', '<=', $min_price];
            } elseif ($max_price != "") {
                $condition[] = ['price', '>=', $max_price];
            }

            $order = 'create_time desc';
            $goods_model = new GoodsModel();
            $field = 'goods_id,goods_name,goods_class_name,goods_image,price,goods_stock,create_time,is_virtual';
            $goods_list = $goods_model->getGoodsPageList($condition, $page, $page_size, $order, $field);

            if (!empty($goods_list[ 'data' ][ 'list' ])) {
                $discount_model = new DiscountModel();
                $discount_info = $discount_model->getDiscountInfo($discount_id, $this->site_id)['data'];
                $promotion_model = new PromotionModel();
                foreach ($goods_list[ 'data' ][ 'list' ] as $k => $v) {
                    //sku数据
                    $goods_list[ 'data' ][ 'list' ][ $k ][ 'sku_list' ] = [];
                    //相同时间的营销活动
                    $goods_list[ 'data' ][ 'list' ][ $k ][ 'same_time_promotion'] = $promotion_model->checkSameTimePromotion([
                        'mode' => 'spu',
                        'id' => $v['goods_id'],
                        'time_info' => ['start_time' => $discount_info['start_time'], 'end_time' => $discount_info['end_time']]
                    ]);
                }
            }

            return $goods_list;
        } else {

            //已经选择的商品sku数据
            $select_id = input('select_id', '');
            $mode = input('mode', 'spu');
            $max_num = input('max_num', 0);
            $min_num = input('min_num', 0);
            $is_virtual = input('is_virtual', '');
            $disabled = input('disabled', 0);
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao
            $discount_id = input('discount_id', 0);//第n件m折id

            $this->assign('select_id', $select_id);
            $this->assign('mode', $mode);
            $this->assign('max_num', $max_num);
            $this->assign('min_num', $min_num);
            $this->assign('is_virtual', $is_virtual);
            $this->assign('disabled', $disabled);
            $this->assign('promotion', $promotion);
            $this->assign('discount_id', $discount_id);

            // 营销活动
            $goods_promotion_type = event('GoodsPromotionType');
            $this->assign('promotion_type', $goods_promotion_type);

            //商品分类
            $condition = [
                ['category_id', 'in', $this->getShopGoodsCategoryIdArr()],
            ];
            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList($condition, 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);
            return $this->fetch("discount/goods_select");
        }
    }
}