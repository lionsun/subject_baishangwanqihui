<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\nthmfolddiscount\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\nthmfolddiscount\model\Discount as DiscountModel;

/**
 * 第n件m折控制器
 */
class Discount extends BaseAdmin
{
	
	/**
	 * 第n件m折列表
	 */
	public function lists()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$discount_name = input('discount_name', '');
			$site_name = input('site_name', '');
			$status = input('status', '');
			
			$condition = [];
			if ($status !== "") {
				$condition[] = [ 'status', '=', $status ];
			}
			$condition[] = [ 'discount_name', 'like', '%' . $discount_name . '%' ];
			$condition[] = [ 'site_name', 'like', '%' . $site_name . '%' ];
			$order = 'create_time desc';
			$field = 'discount_id, discount_name, start_time, end_time, status, create_time, site_id, site_name';
			
			$discount_status_arr = DiscountModel::getDiscountStatus();
			$res = $discount_model->getDiscountPageList($condition, $page, $page_size, $order, $field);
			foreach ($res['data']['list'] as $key => $val) {
				$res['data']['list'][ $key ]['status_name'] = $discount_status_arr[ $val['status'] ];
			}
			return $res;
			
		} else {
			//第n件m折状态
			$discount_status_arr = DiscountModel::getDiscountStatus();
			$this->assign('discount_status_arr', $discount_status_arr);
			return $this->fetch("discount/lists");
		}
	}
	
	/**
	 * 第n件m折详情
	 */
	public function detail()
	{
		if (request()->isAjax()) {
			//活动商品
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			$list = $discount_model->getDiscountGoods($discount_id);
			return $list;
		} else {
			$discount_id = input('discount_id', 0);
			$site_id = input('site_id', 0);
			$this->assign('discount_id', $discount_id);
			
			//活动详情
			$discount_model = new DiscountModel();
			$discount_info = $discount_model->getDiscountInfo($discount_id, $site_id);
			$this->assign('discount_info', $discount_info['data']);
			
			return $this->fetch("discount/detail");
		}
	}
	
	/**
	 * 关闭活动
	 */
	public function close()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$site_id = input('site_id', 0);
			$this->addLog("强制关闭第n件m折id:" . $discount_id);
			$discount_model = new DiscountModel();
			return $discount_model->closeDiscount($discount_id, $site_id);
		}
	}
	
	/**
	 * 删除活动
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$site_id = input('site_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->deleteDiscount($discount_id, $site_id);
		}
	}
}