<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\nthmfolddiscount\model;

use app\model\BaseModel;
use app\model\system\Cron;
use think\facade\Db;

/**
 * 第n件m折
 */
class Discount extends BaseModel
{
    const PROMOTION_TYPE = 'nthmfolddiscount';
    const PROMOTION_TYPE_NAME = '第n件m折';

    //活动状态
    const DISCOUNT_STATUS_NOT_START = 0;
    const DISCOUNT_STATUS_IN_PROCESS = 1;
    const DISCOUNT_STATUS_ENDED = 2;
    const DISCOUNT_STATUS_CLOSED = -1;

    /**
     * 第n件m折状态
     */
    public static function getDiscountStatus()
    {
        $status = [
            self::DISCOUNT_STATUS_NOT_START  => '未开始',
            self::DISCOUNT_STATUS_IN_PROCESS => '进行中',
            self::DISCOUNT_STATUS_ENDED      => '已结束',
            self::DISCOUNT_STATUS_CLOSED     => '已关闭（手动）',
        ];
        return $status;
    }

    /**
     * 获取状态名称
     * @param $id
     * @return string
     */
    public static function getDiscountStatusName($id)
    {
        $status = self::getDiscountStatus();
        if(isset($status[$id])){
            $name = $status[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    /**
     * 添加第n件m折活动
     * @param unknown $data
     */
    public function addDiscount($data)
    {
        $data['create_time'] = time();
        $site_info           = model('shop')->getInfo([['site_id', '=', $data['site_id']]], 'site_name');
        $data['site_name']   = $site_info['site_name'];

        $cron = new Cron();
        if ($data['start_time'] <= time()) {
            $data['status'] = 1;//直接启动
            $discount_id    = model('promotion_nthmfold_discount')->add($data);
            $cron->addCron(1, 0, "第n件m折关闭", "Closenthmfolddiscount", $data['end_time'], $discount_id);
        } else {
            $discount_id = model('promotion_nthmfold_discount')->add($data);
            $cron->addCron(1, 0, "第n件m折开启", "Opennthmfolddiscount", $data['start_time'], $discount_id);
            $cron->addCron(1, 0, "第n件m折关闭", "Closenthmfolddiscount", $data['end_time'], $discount_id);
        }

        return $this->success($discount_id);
    }

    /**
     * 修改第n件m折活动
     * @param unknown $data //传输数据(针对已经开始之后活动不能修改时间)
     */
    /*public function editDiscount($data)
    {
        $discount_id   = $data['discount_id'];
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]], 'start_time,status,discount_goods');
        if ($discount_info['status'] != 1) {
            //针对未开始的活动进行设置
            $cron = new Cron();
            if ($data['start_time'] <= time()) {
                $data['status'] = 1;//直接启动
                $res            = model('promotion_nthmfold_discount')->update($data, [['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]]);
                if ($res) {
                    model('goods')->update(['start_time' => $data['start_time'], 'end_time' => $data['end_time']], [['goods_id', 'in', $discount_info['discount_goods']]]);
                    model('goods_sku')->update(['start_time' => $data['start_time'], 'end_time' => $data['end_time']], [['goods_id', 'in', $discount_info['discount_goods']]]);
                    $goods_data = [
                        'discount_name' => $data['discount_name'],
                        'start_time' => $data['start_time'],
                        'end_time' => $data['end_time'],
                    ];
                    model('promotion_nthmfold_discount_goods')->update($goods_data, [['discount_id', '=', $discount_id]]);
                    //活动商品启动
                    $this->cronOpennthmfolddiscount($discount_id);
                    $cron->deleteCron([['event', '=', 'Opennthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->deleteCron([['event', '=', 'Closenthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->addCron(1, 0, "第n件m折关闭", "Closenthmfolddiscount", $data['end_time'], $discount_id);
                }
            } else {
                $res = model('promotion_nthmfold_discount')->update($data, [['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]]);
                if ($res) {
                    model('goods')->update(['start_time' => $data['start_time'], 'end_time' => $data['end_time']], [['goods_id', 'in', $discount_info['discount_goods']]]);
                    model('goods_sku')->update(['start_time' => $data['start_time'], 'end_time' => $data['end_time']], [['goods_id', 'in', $discount_info['discount_goods']]]);
                    $goods_data = [
                        'discount_name' => $data['discount_name'],
                        'start_time' => $data['start_time'],
                        'end_time' => $data['end_time'],
                    ];
                    model('promotion_nthmfold_discount_goods')->update($goods_data, [['discount_id', '=', $discount_id]]);
                    $cron->deleteCron([['event', '=', 'Opennthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->deleteCron([['event', '=', 'Closenthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->addCron(1, 0, "第n件m折开启", "Opennthmfolddiscount", $data['start_time'], $discount_id);
                    $cron->addCron(1, 0, "第n件m折关闭", "Closenthmfolddiscount", $data['end_time'], $discount_id);
                }

            }
        } else {
            //针对已经启动的第n件m折只能修改文字信息不能修改时间
            if (isset($data['start_time'])) unset($data['start_time']);
            if (isset($data['end_time'])) unset($data['end_time']);
            $res = model('promotion_nthmfold_discount')->update($data, [['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]]);
            $goods_data = [
                'discount_name' => $data['discount_name'],
            ];
            model('promotion_nthmfold_discount_goods')->update($goods_data, [['discount_id', '=', $discount_id]]);
        }
        return $this->success($res);
    }*/
    public function editDiscount($data)
    {
        $discount_id   = $data['discount_id'];
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]], 'start_time,status,discount_goods');
        if ($discount_info['status'] != 1) {
            //针对未开始的活动进行设置
            $cron = new Cron();
            //活动已开始
            if ($data['start_time'] <= time()) {
                $data['status'] = 1;//直接启动
                $res            = model('promotion_nthmfold_discount')->update($data, [['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]]);
                if ($res) {
                    $goods_data = [
                        'discount_name' => $data['discount_name'],
                        'start_time' => $data['start_time'],
                        'end_time' => $data['end_time'],
                    ];
                    model('promotion_nthmfold_discount_goods')->update($goods_data, [['discount_id', '=', $discount_id]]);
                    //活动商品启动
                    $goods_data = [
                        'promotion_type'     => self::PROMOTION_TYPE,
                        'start_time'         => $data['start_time'],
                        'end_time'           => $data['end_time'],
                        'nthmfold_discount_id' => $discount_id,
                    ];
                    model('goods')->update($goods_data, [['goods_id', 'in', $discount_info['discount_goods']]]);
                    model('goods_sku')->update($goods_data, [['goods_id', 'in', $discount_info['discount_goods']]]);
                    $cron->deleteCron([['event', '=', 'Opennthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->deleteCron([['event', '=', 'Closenthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->addCron(1, 0, "第n件m折关闭", "Closenthmfolddiscount", $data['end_time'], $discount_id);
                }
            } else {
                //活动未开始
                $res = model('promotion_nthmfold_discount')->update($data, [['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]]);
                if ($res) {
                    $goods_data = [
                        'discount_name' => $data['discount_name'],
                        'start_time' => $data['start_time'],
                        'end_time' => $data['end_time'],
                    ];
                    model('promotion_nthmfold_discount_goods')->update($goods_data, [['discount_id', '=', $discount_id]]);
                    $cron->deleteCron([['event', '=', 'Opennthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->deleteCron([['event', '=', 'Closenthmfolddiscount'], ['relate_id', '=', $discount_id]]);
                    $cron->addCron(1, 0, "第n件m折开启", "Opennthmfolddiscount", $data['start_time'], $discount_id);
                    $cron->addCron(1, 0, "第n件m折关闭", "Closenthmfolddiscount", $data['end_time'], $discount_id);
                }

            }
        } else {
            //针对已经启动的第n件m折只能修改文字信息不能修改时间
            if (isset($data['start_time'])) unset($data['start_time']);
            if (isset($data['end_time'])) unset($data['end_time']);
            $res = model('promotion_nthmfold_discount')->update($data, [['discount_id', '=', $discount_id], ['site_id', '=', $data['site_id']]]);
            $goods_data = [
                'discount_name' => $data['discount_name'],
            ];
            model('promotion_nthmfold_discount_goods')->update($goods_data, [['discount_id', '=', $discount_id]]);
        }
        return $this->success($res);
    }

    /**
     * 手动关闭第n件m折
     * @param unknown $discount_id
     * @param unknown $site_id
     * @return multitype:string
     */
    public function closeDiscount($discount_id, $site_id)
    {
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id], ['site_id', '=', $site_id]], 'start_time,status,discount_goods');
        if (!empty($discount_info)) {
            //针对正在进行的活动
            if ($discount_info['status'] == self::DISCOUNT_STATUS_IN_PROCESS) {
                $res = model('promotion_nthmfold_discount')->update(['status' => self::DISCOUNT_STATUS_CLOSED], [['discount_id', '=', $discount_id]]);
                $data = [
                    'promotion_type'     => '',
                    'start_time'         => 0,
                    'end_time'           => 0,
                    'nthmfold_discount_id' => 0,
                ];
                model('goods')->update($data, [['goods_id', 'in', $discount_info['discount_goods']]]);
                model('goods_sku')->update($data, [['goods_id', 'in', $discount_info['discount_goods']]]);
                return $this->success($res);
            } else {
                return $this->error("", "正在进行的活动才能进行关闭操作");
            }
        } else {
            return $this->error("", "活动不存在");
        }
    }

    /**
     * 启动第n件m折事件
     * @param unknown $discount_id
     */
    public function cronOpennthmfolddiscount($discount_id)
    {
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id]], 'start_time,status,discount_goods');
        if (!empty($discount_info)) {
            if ($discount_info['start_time'] <= time() && $discount_info['status'] == self::DISCOUNT_STATUS_NOT_START) {
                $res = model('promotion_nthmfold_discount')->update(['status' => self::DISCOUNT_STATUS_IN_PROCESS], [['discount_id', '=', $discount_id]]);
                $data = [
                    'promotion_type'     => self::PROMOTION_TYPE,
                    'start_time'         => $discount_info['start_time'],
                    'end_time'           => $discount_info['end_time'],
                    'nthmfold_discount_id' => $discount_id,
                ];
                model('goods')->update($data, [['goods_id', 'in', $discount_info['discount_goods']]]);
                model('goods_sku')->update($data, [['goods_id', 'in', $discount_info['discount_goods']]]);
                return $this->success($res);
            } else {
                return $this->error("", "第n件m折活动已开启或者关闭");
            }

        } else {
            return $this->error("", "第n件m折活动不存在");
        }
    }

    /**
     * 结束第n件m折事件
     * @param unknown $discount_id
     */
    public function cronClosenthmfolddiscount($discount_id)
    {
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id]], 'start_time,status');
        if (!empty($discount_info)) {
            //针对正在进行的活动
            if ($discount_info['status'] == self::DISCOUNT_STATUS_IN_PROCESS) {
                $res = model('promotion_nthmfold_discount')->update(['status' => self::DISCOUNT_STATUS_ENDED], [['discount_id', '=', $discount_id]]);
                $data = [
                    'promotion_type'     => '',
                    'start_time'         => 0,
                    'end_time'           => 0,
                    'nthmfold_discount_id' => 0,
                ];
                model('goods')->update($data, [['nthmfold_discount_id', '=', $discount_id]]);
                model('goods_sku')->update($data, [['nthmfold_discount_id', '=', $discount_id]]);
                return $this->success($res);
            } else {
                return $this->error("", "正在进行的活动才能进行关闭操作");
            }
        } else {
            return $this->error("", "活动不存在");
        }
    }

    /**
     * 删除第n件m折活动(针对未进行中)
     * @param unknown $discount_id
     */
    public function deleteDiscount($discount_id, $site_id)
    {
        $res = model('promotion_nthmfold_discount')->delete([['discount_id', '=', $discount_id], ['site_id', '=', $site_id], ['status', '<>', 1]]);
        if ($res) {
            model('promotion_nthmfold_discount_goods')->delete([['discount_id', '=', $discount_id]]);
            $data = [
                'promotion_type'     => '',
                'start_time'         => 0,
                'end_time'           => 0,
                'nthmfold_discount_id' => 0,
            ];
            model('goods')->update($data, [['nthmfold_discount_id', '=', $discount_id]]);
            model('goods_sku')->update($data, [['nthmfold_discount_id', '=', $discount_id]]);
            return $this->success($res);
        } else {
            return $this->error('', "正在进行中或者权限不足");
        }
    }

    /**
     * 添加第n件m折商品
     * @param unknown $discount_id
     * @param unknown $goods_ids goods_id组（添加时不设置价格）
     */
    public function addDiscountGoods($discount_id, $site_id, $goods_ids)
    {
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id], ['site_id', '=', $site_id]], 'discount_id,discount_name,start_time,end_time,status,discount_goods');
        if (!empty($discount_info)) {
            $arr1 = $discount_info['discount_goods'] ? explode(',', $discount_info['discount_goods']) : [];
            $arr2 = $goods_ids ? explode(',', $goods_ids) : [];
            $goods_array = array_merge($arr1, $arr2);
            model('promotion_nthmfold_discount')->update(['discount_goods' => join(',', $goods_array)], [['site_id', '=', $site_id], ['discount_id', '=', $discount_id]]);
            //添加折扣商品
            $add_data = [];
            foreach($arr2 as $goods_id){
                $add_data[] = [
                    'discount_id' => $discount_info['discount_id'],
                    'discount_name' => $discount_info['discount_name'],
                    'goods_id' => $goods_id,
                    'start_time' => $discount_info['start_time'],
                    'end_time' => $discount_info['end_time'],
                ];
            }
            model('promotion_nthmfold_discount_goods')->addList($add_data);
            if ($discount_info['status'] == self::DISCOUNT_STATUS_IN_PROCESS) {
                $data = [
                    'promotion_type'     => self::PROMOTION_TYPE,
                    'start_time'         => $discount_info['start_time'],
                    'end_time'           => $discount_info['end_time'],
                    'nthmfold_discount_id' => $discount_id,
                ];
                model('goods')->update($data, [['goods_id', 'in', $goods_ids]]);
                model('goods_sku')->update($data, [['goods_id', 'in', $goods_ids]]);
            }
        }
        return $this->success();
    }

    /**
     * 删除第n件m折商品
     * @param unknown $discount_id
     * @param unknown $goods_id
     * @param unknown $site_id
     */
    public function deleteDiscountGoods($discount_id, $goods_id, $site_id)
    {
        $discount_info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id], ['site_id', '=', $site_id]], 'status, discount_goods');
        if (!empty($discount_info)) {
            $goods_array = explode(',', $discount_info['discount_goods']);
            $index = array_search($goods_id, $goods_array);
            unset($goods_array[$index]);
            $res = model('promotion_nthmfold_discount')->update(['discount_goods' => join(',', $goods_array)], [['discount_id', '=', $discount_id]]);
			if ($res && $discount_info['status'] == 1) {
                $data = [
                    'promotion_type'     => '',
                    'start_time'         => 0,
                    'end_time'           => 0,
                    'nthmfold_discount_id' => 0,
                ];
                model('goods')->update($data, [['goods_id', '=', $goods_id]]);
                model('goods_sku')->update($data, [['goods_id', '=', $goods_id]]);
                model('promotion_nthmfold_discount_goods')->delete([['discount_id', '=', $discount_id], ['goods_id', '=', $goods_id]]);
            }
			return $this->success($res);
		} else {
            return $this->error('', '活动不存在');
        }
    }

    /**
     * 获取第n件m折基础信息
     * @param unknown $discount_id
     * @param unknown $site_id
     * @return multitype:string
     */
    public function getDiscountInfo($discount_id, $site_id)
    {
        $info = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id], ['site_id', '=', $site_id]], 'discount_id, site_id, discount_name, status, remark, start_time, end_time, create_time, modify_time, discount_content, discount_goods');
        if(!empty($info)){
            $info['status_name'] = self::getDiscountStatusName($info['status']);
        }
        return $this->success($info);
    }

    public function getDiscountGoods($discount_id)
    {
        $info           = model('promotion_nthmfold_discount')->getInfo([['discount_id', '=', $discount_id]], 'discount_goods');
        $discount_goods = !empty($info) ? $info['discount_goods'] : '';
        $order          = $discount_goods ? Db::raw("field(goods_id, {$discount_goods})") : '';
        $list           = model('goods')->getList([['goods_id', 'in', $discount_goods]], 'nthmfold_discount_id, start_time, end_time, goods_id, price, goods_name, goods_image', $order);

        return $this->success($list);
    }

    /**
     * 获取折扣商品列表
     */
    public function getDiscountGoodsList($condition, $field = '*', $order = '')
    {
        $list = model('promotion_nthmfold_discount_goods')->getList($condition, $field, $order);
        return $this->success($list);
    }

    /**
     * 获取第n件m折列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getDiscountList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $list = model('promotion_nthmfold_discount')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

    /**
     * 获取第n件m折分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getDiscountPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('promotion_nthmfold_discount')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }
}