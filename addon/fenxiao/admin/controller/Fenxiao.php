<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\fenxiao\admin\controller;

use addon\fenxiao\model\FenxiaoAccount;
use addon\fenxiao\model\FenxiaoApply;
use addon\fenxiao\model\FenxiaoData;
use addon\fenxiao\model\FenxiaoLevel;
use addon\fenxiao\model\FenxiaoLevel as FenxiaoLevelModel;
use addon\fenxiao\model\FenxiaoOrder;
use app\admin\controller\BaseAdmin;
use addon\fenxiao\model\Fenxiao as FenxiaoModel;
use app\model\system\Menu as MenuModel;
use app\model\member\Member as MemberModel;
use addon\fenxiao\model\Config as ConfigModel;


/**
 *  分销设置
 */
class Fenxiao extends BaseAdmin
{
	/**
	 * 分销概况
	 */
	public function index()
	{
		//获取分销商账户统计
		$fenxiao_data_model = new FenxiaoData();
		$account_data = $fenxiao_data_model->getFenxiaoAccountData();
		$this->assign('account_data', $account_data);
		//累计佣金
		$fenxiao_account = number_format($account_data['account'] + $account_data['account_withdraw'], 2, '.', '');
		$this->assign('fenxiao_account', $fenxiao_account);
		//获取申请人数
		$fenxiao_apply_num = $fenxiao_data_model->getFenxiaoApplyCount();
		$this->assign('fenxiao_apply_num', $fenxiao_apply_num);
		//分销商人数
		$fenxiao_num = $fenxiao_data_model->getFenxiaoCount();
		$this->assign('fenxiao_num', $fenxiao_num);
		return $this->fetch('fenxiao/index');
	}
	
	/**
	 * 分销商列表
	 */
	public function lists()
	{
		$model = new FenxiaoModel();
		$member_model = new MemberModel();
		$order_model = model('fenxiao_order');

		if (request()->isAjax()) {
			$condition = [];
            $team_condition = [];
			$order_condition = '';
            $withdraw_condition = '';

			$fenxiao_name = input('fenxiao_name', '');
			if ($fenxiao_name) {
				$condition[] = [ 'f.fenxiao_name', 'like', '%' . $fenxiao_name . '%' ];
			}
			
			$parent_name = input('parent_name', '');
			if ($parent_name) {
				$condition[] = [ 'pf.fenxiao_name', 'like', '%' . $parent_name . '%' ];
			}
			
			$level_id = input('level_id', '');
			if ($level_id) {
				$condition[] = [ 'f.level_id', '=', $level_id ];
			}
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && $end_time) {
                $team_condition[] = [ 'reg_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
                $order_condition = " and (o.create_time between '".date_to_time($start_time)."' and '".date_to_time($end_time)."') ";
                $withdraw_condition = " and (fw.create_time between '".date_to_time($start_time)."' and '".date_to_time($end_time)."') ";
			} elseif (!$start_time && $end_time) {
                $team_condition[] = [ 'reg_time', '<=', date_to_time($end_time) ];
                $order_condition = " and o.create_time <= '".date_to_time($end_time)."' ";
                $withdraw_condition = " and fw.create_time <= '".date_to_time($end_time)."' ";
			} elseif ($start_time && !$end_time) {
                $team_condition[] = [ 'reg_time', '>=', date_to_time($start_time) ];
                $order_condition = " and o.create_time >= '".date_to_time($start_time)."' ";
                $withdraw_condition = " and fw.create_time >= '".date_to_time($start_time)."' ";
			}
			
			$status = input('status', '');
			if (!empty($status)) {
				$condition[] = [ 'f.status', '=', $status ];
			}
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getFenxiaoPageList($condition, $page, $page_size, 'f.create_time desc');

			foreach($list['data']['list'] as $k=>$v){
			    if($v['parent'] > 0){
			        if($v['grand_parent'] > 0){
                        $fenxiao_ids = $this->getChildIds($v['fenxiao_id'], 3);
                    }else{
                        $fenxiao_ids = $this->getChildIds($v['fenxiao_id'], 2);
                    }
                }else{
                    $fenxiao_ids = $this->getChildIds($v['fenxiao_id'], 1);
                }
                $each_team_condition = [];

                $each_team_condition[] = ['fenxiao_id', '=', "$v[fenxiao_id]"];
                $each_team_condition[] = ['is_fenxiao', '=', '0'];
                $new_team_condition = array_merge($team_condition, $each_team_condition);

			    $team_count_array = $member_model->getMemberCount($new_team_condition);
			    $team_count = $team_count_array['data'];

                $fenxiao_id = $v['fenxiao_id'];
                $commission = ", SUM(CASE WHEN fo.three_fenxiao_id = $fenxiao_id THEN three_commission WHEN fo.two_fenxiao_id = $fenxiao_id THEN two_commission WHEN fo.one_fenxiao_id = $fenxiao_id THEN one_commission END) as commission ";

                $order_money_info = $order_model->query("SELECT SUM(fo.real_goods_money) as order_money_count " . $commission . " FROM `ns_fenxiao_order` fo LEFT JOIN ns_order o ON o.order_id = fo.order_id WHERE (FIND_IN_SET(fo.one_fenxiao_id, '$fenxiao_ids') OR FIND_IN_SET(fo.two_fenxiao_id, '$fenxiao_ids') OR FIND_IN_SET(fo.three_fenxiao_id, '$fenxiao_ids')) " . $order_condition . " and fo.is_refund = 0 and fo.is_settlement = 1");
                $withdraw_info = $order_model->query("SELECT SUM(fw.money) as withdraw_money FROM `ns_fenxiao` f LEFT JOIN ns_fenxiao_withdraw fw ON fw.fenxiao_id = f.fenxiao_id where f.fenxiao_id = " . $fenxiao_id . $withdraw_condition);
                $order_money_count = $order_money_info[0]['order_money_count'] ?: 0;
                $order_commission_count = $order_money_info[0]['commission'] ?: 0;
                $withdraw_money_count = $withdraw_info[0]['withdraw_money'] ?: 0;

                $list['data']['list'][$k]['team_count'] = $team_count;
                $list['data']['list'][$k]['order_money_count'] = $order_money_count;
                $list['data']['list'][$k]['commission'] = $order_commission_count;
                $list['data']['list'][$k]['withdraw_money'] = $withdraw_money_count;
            }

			return $list;
		} else {
			$level_model = new FenxiaoLevel();
			$level_list = $level_model->getLevelList([ [ 'status', '=', 1 ] ], 'level_id,level_name');
			$this->assign('level_list', $level_list['data']);
			
			$config_model = new ConfigModel();
			$basics = $config_model->getFenxiaoBasicsConfig();
			$this->assign("basics_info", $basics['data']['value']);
			$this->forthMenu();
			return $this->fetch('fenxiao/lists');
		}
	}

	/**
	 *  获取子级ID
	 */
	public function getChildIds($parent, $level){
        $fenxiao_model = model('fenxiao');

        $fenxiao_ids = $parent;
        if($level == 1){
            $two_fenxiao_info = $fenxiao_model->getList([ ['parent', '=', $parent] ], 'GROUP_CONCAT(fenxiao_id) as fenxiao_id', '', '', '', 'parent');
            if(!empty($two_fenxiao_info)){
                $two_fenxiao_id = $two_fenxiao_info[0]['fenxiao_id'];
                $fenxiao_ids .= ',';
                $fenxiao_ids .= $two_fenxiao_id;
                if(!empty($two_fenxiao_id)){
                    $three_fenxiao_info = $fenxiao_model->getList([['parent', 'in', $two_fenxiao_id]], 'GROUP_CONCAT(fenxiao_id) as fenxiao_id', '', '', '', 'parent');
                    if(!empty($three_fenxiao_info)){
                        $three_fenxiao_id = implode(',', array_column($three_fenxiao_info, 'fenxiao_id'));
                        $fenxiao_ids .= ',';
                        $fenxiao_ids .= $three_fenxiao_id;
                    }
                }
            }
            return $fenxiao_ids;
        }else if($level == 2){
            $three_fenxiao_info = $fenxiao_model->getList([ ['parent', '=', $parent] ], 'GROUP_CONCAT(fenxiao_id) as fenxiao_id', '', '', '', 'parent');
            if(!empty($three_fenxiao_info)){
                $three_fenxiao_id = implode(',', array_column($three_fenxiao_info, 'fenxiao_id'));
                $fenxiao_ids .= ',';
                $fenxiao_ids .= $three_fenxiao_id;
            }
            return $fenxiao_ids;
        }else if($level == 3){
            return $fenxiao_ids;
        }
    }

	/**
	 * 详情
	 */
	public function detail()
	{
		$fenxiao_id = input('fenxiao_id', '');
		$model = new FenxiaoModel();
		$fenxiao_leve_model = new FenxiaoLevelModel();
		$condition[] = [ 'f.fenxiao_id', '=', $fenxiao_id ];
		$info = $model->getFenxiaoDetailInfo($condition);
		$fenxiao_level = $fenxiao_leve_model->getLevelInfo([ [ 'level_id', '=', $info['data']['level_id'] ] ]);
		$this->assign('status', $model->fenxiao_status_zh);
		$this->assign('level', $fenxiao_level['data']);
		$this->assign('info', $info['data']);
		
		$this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
		return $this->fetch('fenxiao/fenxiao_detail');
	}
	
	/**
	 * 分销账户信息
	 */
	public function account()
	{
		$model = new FenxiaoModel();
		$fenxiao_id = input('fenxiao_id', '');
		
		$condition[] = [ 'f.fenxiao_id', '=', $fenxiao_id ];
		$info = $model->getFenxiaoDetailInfo($condition);
		$account = $info['data']['account'] - $info['data']['account_withdraw_apply'];
		$info['data']['account'] = number_format($account, 2, '.', '');
		$this->assign('fenxiao_info', $info['data']);
		
		if (request()->isAjax()) {
			
			$account_model = new FenxiaoAccount();
			$page = input('page', 1);
			$status = input('status', '');
			
			$fenxiao_id = input('fenxiao_id', '');
			$list_condition[] = [ 'fenxiao_id', '=', $fenxiao_id ];
			if ($status) {
				if ($status == 1) {
					$list_condition[] = [ 'money', '>', 0 ];
				} else {
					$list_condition[] = [ 'money', '<', 0 ];
				}
			}
			
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if ($start_time && $end_time) {
				$list_condition[] = [ 'create_time', 'between', [ $start_time, $end_time ] ];
			} elseif (!$start_time && $end_time) {
				$list_condition[] = [ 'create_time', '<=', $end_time ];
				
			} elseif ($start_time && !$end_time) {
				$list_condition[] = [ 'create_time', '>=', $start_time ];
			}
			
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $account_model->getFenxiaoAccountPageList($list_condition, $page, $page_size);
			return $list;
		}
		$this->assign('fenxiao_id', $fenxiao_id);
		
		$this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
		return $this->fetch('fenxiao/fenxiao_account');
	}
	
	/**
	 * 订单管理
	 */
	public function order()
	{
		$model = new FenxiaoOrder();
		
		$fenxiao_id = input('fenxiao_id', '');
		if (request()->isAjax()) {
			
			$page_index = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$fenxiao_id = input('fenxiao_id', '');
			$condition[] = [ 'one_fenxiao_id|two_fenxiao_id|three_fenxiao_id', '=', $fenxiao_id ];
            $is_settlement = input('status', '');
            if(!empty($is_settlement)){
                $condition[] = [ 'fo.is_settlement', '=', $is_settlement - 1 ];
            }

			$search_text_type = input('search_text_type', "goods_name");//订单编号/店铺名称/商品名称
			$search_text = input('search_text', "");
			if (!empty($search_text)) {
				$condition[] = [ 'fo.' . $search_text_type, 'like', '%' . $search_text . '%' ];
			}
			
			//下单时间
			$start_time = input('start_time', '');
			$end_time = input('end_time', '');
			if (!empty($start_time) && empty($end_time)) {
				$condition[] = [ 'o.create_time', '>=', date_to_time($start_time) ];
			} elseif (empty($start_time) && !empty($end_time)) {
				$condition[] = [ 'o.create_time', '<=', date_to_time($end_time) ];
			} elseif (!empty($start_time) && !empty(date_to_time($end_time))) {
				$condition[] = [ 'o.create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
			}

			$list = $model->getFenxiaoOrderPageList($condition, $page_index, $page_size);
			return $list;
			
		} else {
			//订单状态
			$this->assign('fenxiao_id', $fenxiao_id);
			$this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
			return $this->fetch('fenxiao/order_lists');
		}
	}
	
	/**
	 * 订单详情
	 */
	public function orderDetail()
	{
		$fenxiao_order_model = new FenxiaoOrder();
		$fenxiao_order_id = input('fenxiao_order_id', '');
		$order_info = $fenxiao_order_model->getFenxiaoOrderDetail([ [ 'fenxiao_order_id', '=', $fenxiao_order_id ] ]);
		$this->assign('order_info', $order_info['data']);
		return $this->fetch('fenxiao/order_detail');
	}
	
	/**
	 * 冻结
	 */
	public function frozen()
	{
		$fenxiao_id = input('fenxiao_id', '');
		
		$model = new FenxiaoModel();
		
		return $model->frozen($fenxiao_id);
	}
	
	/**
	 * 恢复正常
	 */
	public function unfrozen()
	{
		$fenxiao_id = input('fenxiao_id', '');
		
		$model = new FenxiaoModel();
		
		return $model->unfrozen($fenxiao_id);
	}
	
	
	/**
	 * 分销商申请列表
	 */
	public function apply()
	{
		$model = new FenxiaoApply();
		if (request()->isAjax()) {
			
			$condition[] = [ 'status', '=', 1 ];
			
			$fenxiao_name = input('fenxiao_name', '');
			if ($fenxiao_name) {
				$condition[] = [ 'fenxiao_name', 'like', '%' . $fenxiao_name . '%' ];
			}
			$mobile = input('mobile', '');
			if ($mobile) {
				$condition[] = [ 'mobile', 'like', '%' . $mobile . '%' ];
			}
			$level_id = input('level_id', '');
			if ($level_id) {
				$condition[] = [ 'level_id', '=', $level_id ];
			}
			$create_start_time = input('create_start_time', '');
			$create_end_time = input('create_end_time', '');
			if ($create_start_time && $create_end_time) {
				$condition[] = [ 'create_time', 'between', [ strtotime($create_start_time), strtotime($create_end_time) ] ];
			} elseif (!$create_start_time && $create_end_time) {
				$condition[] = [ 'create_time', '<=', strtotime($create_end_time) ];
				
			} elseif ($create_start_time && !$create_end_time) {
				$condition[] = [ 'create_time', '>=', strtotime($create_start_time) ];
			}
			
			$rg_start_time = input('rg_start_time', '');
			$rg_end_time = input('rg_end_time', '');
			if ($rg_start_time && $rg_end_time) {
				$condition[] = [ 'reg_time', 'between', [ strtotime($rg_start_time), strtotime($rg_end_time) ] ];
			} elseif (!$rg_start_time && $rg_end_time) {
				$condition[] = [ 'reg_time', '<=', strtotime($rg_end_time) ];
				
			} elseif ($rg_start_time && !$rg_end_time) {
				$condition[] = [ 'reg_time', '>=', strtotime($rg_start_time) ];
			}
			
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$list = $model->getFenxiaoApplyPageList($condition, $page, $page_size, 'create_time desc', '*');
			return $list;
			
		} else {
			
			$level_model = new FenxiaoLevel();
			$level_list = $level_model->getLevelList([ [ 'status', '=', 1 ] ], 'level_id,level_name');
			$this->assign('level_list', $level_list['data']);
			
			$this->forthMenu();
			return $this->fetch('fenxiao/apply');
		}
	}
	
	/**
	 * 分销商申请通过
	 */
	public function applyPass()
	{
		$apply_id = input('apply_id');
		
		$model = new FenxiaoApply();
		$res = $model->pass($apply_id);
		return $res;
	}
	
	/**
	 * 分销商申请通过
	 */
	public function applyRefuse()
	{
		$apply_id = input('apply_id');
		
		$model = new FenxiaoApply();
		$res = $model->refuse($apply_id);
		return $res;
	}
	
	
	/**
	 * 四级菜单
	 * @param unknown $params
	 */
	protected function fiveMenu($params = [])
	{
		$url = strtolower($this->url);
		$menu_model = new MenuModel();
		$menu_info = $menu_model->getMenuInfo([ [ 'url', "=", $url ], [ 'level', '=', 5 ] ], 'parent');
		
		if (!empty($menu_info['data'])) {
			$menus = $menu_model->getMenuList([ [ 'app_module', "=", $this->app_module ], [ 'is_show', "=", 1 ], [ 'parent', '=', $menu_info['data']['parent'] ] ], '*', 'sort asc');
			foreach ($menus['data'] as $k => $v) {
				$menus['data'][ $k ]['parse_url'] = addon_url($menus['data'][ $k ]['url'], $params);
				if ($menus['data'][ $k ]['url'] == $url) {
					$menus['data'][ $k ]['selected'] = 1;
				} else {
					$menus['data'][ $k ]['selected'] = 0;
				}
			}
			$this->assign('forth_menu', $menus['data']);
		}
	}

    /**
     * 分销商下级团队
     */
    public function team()
    {
        $fenxiao_id = input('fenxiao_id', 0);
        $fenxiao_model = new FenxiaoModel();
        $order_model = model('fenxiao_order');

        if (request()->isAjax()) {
            $order_condition = '';

            $level = input('level', 0);
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $start_time = input('start_time', '');
            $end_time = input('end_time', '');
            if ($start_time && $end_time) {
                $order_condition = " and (o.create_time between '".date_to_time($start_time)."' and '".date_to_time($end_time)."') ";
            } elseif (!$start_time && $end_time) {
                $order_condition = " and o.create_time <= '".date_to_time($end_time)."' ";
            } elseif ($start_time && !$end_time) {
                $order_condition = " and o.create_time >= '".date_to_time($start_time)."' ";
            }

            $list = $fenxiao_model->getFenxiaoTeam($level, $fenxiao_id, $page, $page_size);

            foreach($list['data']['list'] as $k=>$v){
                $list['data']['list'][$k]['order_money_count'] = 0;
                $list['data']['list'][$k]['commission'] = 0;

                $fenxiao_id = $v['fenxiao_id'] ?: '-1';
                $commission = ", SUM(CASE WHEN fo.three_fenxiao_id = $fenxiao_id THEN three_commission WHEN fo.two_fenxiao_id = $fenxiao_id THEN two_commission WHEN fo.one_fenxiao_id = $fenxiao_id THEN one_commission END) as commission ";

                $order_money_info = $order_model->query("SELECT SUM(fo.real_goods_money) as order_money_count ".$commission." FROM `ns_fenxiao_order` fo LEFT JOIN ns_order o ON o.order_id = fo.order_id WHERE (fo.one_fenxiao_id = '$fenxiao_id' OR fo.two_fenxiao_id = '$fenxiao_id' OR fo.three_fenxiao_id = '$fenxiao_id') " . $order_condition . " and fo.is_refund = 0 and fo.is_settlement = 1");
                if(!empty($order_money_info)){
                    $order_money_count = $order_money_info[0]['order_money_count'] ?: 0;
                    $order_commission_count = $order_money_info[0]['commission'] ?: 0;
                    $list['data']['list'][$k]['order_money_count'] = $order_money_count;
                    $list['data']['list'][$k]['commission'] = $order_commission_count;
                }
                $order_money = $order_model->query("SELECT SUM(o.order_money) as order_money, count(order_id) as order_num FROM ns_order o  WHERE o.order_status > 0 and o.member_id = " . $v['member_id'] . $order_condition);
                if(!empty($order_money)){
                    $list['data']['list'][$k]['order_money'] = $order_money[0]['order_money'] ?: 0;
                    $list['data']['list'][$k]['order_num'] = $order_money[0]['order_num'] ?: 0;
                }
                $order_complete_money = $order_model->query("SELECT SUM(o.order_money) as order_complete_money, count(order_id) as order_complete_num FROM ns_order o  WHERE o.order_status = 10 and o.member_id = " . $v['member_id'] . $order_condition);
                if(!empty($order_complete_money)){
                    $list['data']['list'][$k]['order_complete_money'] = $order_complete_money[0]['order_complete_money'] ?: 0;
                    $list['data']['list'][$k]['order_complete_num'] = $order_complete_money[0]['order_complete_num'] ?: 0;
                }

            }

            return $list;
        } else {
            $this->assign('fenxiao_id', $fenxiao_id);
            $this->fiveMenu([ 'fenxiao_id' => $fenxiao_id ]);
            return $this->fetch('fenxiao/team');
        }
    }


}