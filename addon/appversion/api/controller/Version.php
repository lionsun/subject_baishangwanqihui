<?php

/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: http://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace addon\appversion\api\controller;

use app\api\controller\BaseApi;
use addon\appversion\model\Version as AppVersionModel;

/**
 * App 版本管理API
 * Class Version
 * @package addon\appversion\api\controller
 */
class Version extends BaseApi
{
    /**
     * 获取当前版本信息
     * @return false|string
     */
    public function info()
    {
        $system_type  = isset($this->params['system_type']) ? $this->params['system_type'] : '';
        $version_code = isset($this->params['version_code']) ? $this->params['version_code'] : '';

        if (empty($system_type)) {
            return $this->response($this->error('', 'REQUEST_SYSTEM_TYPE_PARAM'));
        }
        if (empty($version_code)) {
            return $this->response($this->error('', 'REQUEST_VERSION_CODE_PARAM'));
        }

        $app_version_model = new AppVersionModel();
        $res               = $app_version_model->getAppVersionInfo([
            ['system_type', '=', $system_type],
            ['version_code', '=', $version_code]
        ]);

        return $this->response($res);
    }

    /**
     * 检测当前版本是否为最新
     * @return false|string
     */
    public function isLatest()
    {
        $system_type  = isset($this->params['system_type']) ? $this->params['system_type'] : '';
        $version_code = isset($this->params['version_code']) ? $this->params['version_code'] : '';

        if (empty($system_type)) {
            return $this->response($this->error('', 'REQUEST_SYSTEM_TYPE_PARAM'));
        }
        if (empty($version_code)) {
            return $this->response($this->error('', 'REQUEST_VERSION_CODE_PARAM'));
        }

        $app_version_model = new AppVersionModel();
        $res               = $app_version_model->checkIsLatest($system_type, $version_code);

        return $this->response($res);
    }

    /**
     * 获取最新的App版本信息
     */
    public function latestInfo()
    {
        $system_type = isset($this->params['system_type']) ? $this->params['system_type'] : '';

        if (empty($system_type)) {
            return $this->response($this->error('', 'REQUEST_SYSTEM_TYPE_PARAM'));
        }

        $app_version_model = new AppVersionModel();
        $res               = $app_version_model->getLatestAppVersionInfo($system_type);

        return $this->response($res);
    }
}