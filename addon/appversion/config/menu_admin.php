<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name'             => 'APP_VERSION',
        'title'            => 'App版本管理',
        'url'              => 'appversion://admin/version/lists',
        'parent'           => 'CONFIG_SYSTEM',
        'is_show'          => 1,
        'picture'          => '',
        'picture_selected' => '',
        'sort'             => 6,
        'child_list'       => [
            [
                'name'    => 'APP_VERSION_ADD',
                'title'   => '添加版本',
                'url'     => 'appversion://admin/version/addversion',
                'parent'  => '',
                'is_show' => 0,
            ],
            [
                'name'    => 'APP_VERSION_EDIT',
                'title'   => '编辑版本',
                'url'     => 'appversion://admin/version/editversion',
                'parent'  => '',
                'is_show' => 0,
            ],
            [
                'name'    => 'APP_VERSION_DELETE',
                'title'   => '删除版本',
                'url'     => 'appversion://admin/version/deleteversion',
                'parent'  => '',
                'is_show' => 0,
            ],
        ],
    ],
];
