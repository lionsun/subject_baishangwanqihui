<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\appversion\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\appversion\model\Version as AppVersionModel;
use app\model\web\WebSite as WebsiteModel;

/**
 * APP 版本管理控制器
 * Class Version
 * @package addon\appversion\admin\controller
 */
class Version extends BaseAdmin
{
    /**
     * 版本列表
     */
    public function lists()
    {
        if (request()->isAjax()) {
            $page          = input('page', 1);
            $page_size     = input('page_size', PAGE_LIST_ROWS);
            $system_type   = input('system_type', '');
            $version_title = input('version_title', '');
            $version_code  = input('version_code', '');

            $condition = [];
            if (!empty($system_type)) {
                $condition[] = ['system_type', '=', $system_type];
            }
            if (!empty($version_title)) {
                $condition[] = ['version_title', 'like', '%' . $version_title . '%'];
            }
            if (!empty($version_code)) {
                $condition[] = ['version_code', '=', $version_code];
            }

            $order_by = 'version_id desc';
            $field    = 'version_id, system_type, version_title, version_code, download_url, create_time';

            $app_version_model = new AppVersionModel();
            $res               = $app_version_model->getAppVersionPageList($condition, $page, $page_size, $order_by, $field);
            return $res;
        } else {
            $this->assign([
                'system_type_data' => AppVersionModel::getSystemType()
            ]);

            $website_model = new WebsiteModel();
            $website_info = $website_model->getWebSite([['site_id', '=', 0]], '*');
            $this->assign('website_info', $website_info['data']);

            $download_path = "{$website_info['data']['wap_domain']}/otherpages/download/download";
            $qrcode_path = "upload/qrcode/app_down.jpg";
            if(is_file($qrcode_path)){
                unlink($qrcode_path);
            }
            $qrcode_path = getQrcode([
                'code_text' => $download_path,
                'code_out_file' => $qrcode_path,
                'logo_path' => 'public/static/img/download-logo.jpg',
                'code_size' => 26,
                'logo_size' => 0.18,
            ])['data'];
            $this->assign('qrcode_path', $qrcode_path);
            $this->assign('download_path', $download_path);

            return $this->fetch('version/lists');
        }
    }

    /**
     * 添加版本
     */
    public function addVersion()
    {
        if (request()->isAjax()) {
            $app_version_model = new AppVersionModel();
            $res               = $app_version_model->addAppVersion([
                'system_type'   => input('system_type', ''),
                'version_title' => input('version_title', ''),
                'version_code'  => input('version_code', ''),
                'download_url'  => input('download_url', ''),
                'update_log'    => input('update_log', ''),
            ]);
            return $res;
        } else {
            $this->assign([
                'system_type_data' => AppVersionModel::getSystemType()
            ]);
            return $this->fetch('version/add_version');
        }
    }

    /**
     * 编辑版本
     */
    public function editVersion()
    {
        $app_version_model = new AppVersionModel();
        if (request()->isAjax()) {
            $res = $app_version_model->editAppVersion([
                'version_id'    => input('version_id', 0),
                'system_type'   => input('system_type', ''),
                'version_title' => input('version_title', ''),
                'version_code'  => input('version_code', ''),
                'download_url'  => input('download_url', ''),
                'update_log'    => input('update_log', ''),
            ]);
            return $res;
        } else {
            $version_id = input('version_id', 0);

            $app_version_info = $app_version_model->getAppVersionInfo([['version_id', '=', $version_id]])['data'];

            $this->assign([
                'system_type_data' => AppVersionModel::getSystemType(),
                'app_version_info' => $app_version_info
            ]);
            return $this->fetch('version/edit_version');
        }
    }

    /**
     * 删除版本
     */
    public function deleteVersion()
    {
        if (request()->isAjax()) {
            $app_version_model = new AppVersionModel();
            $res               = $app_version_model->deleteAppVersion(input('version_id', 0));
            return $res;
        }
    }
}