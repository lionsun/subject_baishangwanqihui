<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com.cn
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\appversion\model;

use think\facade\Cache;
use app\model\BaseModel;

/**
 *  APP 版本管理
 * Class Version
 * @package addon\appversion\model
 */
class Version extends BaseModel
{
    /**
     * 操作系统 Android
     */
    const SYSTEM_ANDROID = 'Android';

    /**
     * 操作系统 IOS
     */
    const SYSTEM_IOS = 'IOS';

    /**
     * 缓冲标签名称
     */
    const CACHE_TAG_NAME = 'app_version';

    /**
     * 获取操作系统类型
     * @param null $id
     * @return array|array[]
     */
    public static function getSystemType($id = null)
    {
        $data   = array(
            array(
                'id'   => self::SYSTEM_ANDROID,
                'name' => self::SYSTEM_ANDROID
            ),
            array(
                'id'   => self::SYSTEM_IOS,
                'name' => self::SYSTEM_IOS
            )
        );
        $result = [];
        if (is_null($id)) {
            $result = $data;
        } else {
            foreach ($data as $val) {
                if ($id == $val['id']) {
                    $result = $val;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 添加版本
     * @param array $params
     * @return array
     */
    public function addAppVersion(array $params)
    {
        $system_type  = isset($params['system_type']) ? $params['system_type'] : '';
        $version_code = isset($params['version_code']) ? $params['version_code'] : '';

        if (empty(self::getSystemType($system_type))) {
            return $this->error('', '操作系统错误');
        }
        if (empty($version_code)) {
            return $this->error('', '版本号必传');
        }

        // 检测重复
        $check_info = $this->getAppVersionInfo([
            ['version_code', '=', $version_code],
            ['system_type', '=', $system_type],
        ], 'version_id')['data'];
        if (!empty($check_info)) {
            return $this->error('', '版本号重复');
        }

        $res = model('app_version')->add([
            'system_type'   => $system_type,
            'version_title' => $params['version_title'],
            'version_code'  => $version_code,
            'download_url'  => $params['download_url'],
            'update_log'    => $params['update_log'],
            'create_time'   => time(),
        ]);

        Cache::tag(self::CACHE_TAG_NAME)->clear();

        return ($res > 0) ? $this->success($res) : $this->error();
    }

    /**
     * 编辑版本
     * @param array $params
     * @return array
     */
    public function editAppVersion(array $params)
    {
        $version_id   = isset($params['version_id']) ? $params['version_id'] : 0;
        $system_type  = isset($params['system_type']) ? $params['system_type'] : '';
        $version_code = isset($params['version_code']) ? $params['version_code'] : '';
        $condition    = [['version_id', '=', $version_id]];

        if (empty(self::getSystemType($system_type))) {
            return $this->error('', '操作系统错误');
        }
        if (empty($version_code)) {
            return $this->error('', '版本号必传');
        }

        // 检测是否存在
        $version_info = $this->getAppVersionInfo($condition, 'version_id')['data'];
        if (empty($version_info)) {
            return $this->error('', '数据错误');
        }

        // 检测重复
        $check_info = $this->getAppVersionInfo([
            ['version_code', '=', $version_code],
            ['system_type', '=', $system_type],
            ['version_id', '<>', $version_id],
        ], 'version_id')['data'];
        if (!empty($check_info)) {
            return $this->error('', '版本号重复');
        }

        $res = model('app_version')->update([
            'system_type'   => $system_type,
            'version_title' => $params['version_title'],
            'version_code'  => $version_code,
            'download_url'  => $params['download_url'],
            'update_log'    => $params['update_log'],
            'update_time'   => time(),
        ], $condition);

        Cache::tag(self::CACHE_TAG_NAME)->clear();

        return ($res > 0) ? $this->success($res) : $this->error();
    }

    /**
     * 删除版本
     * @param $version_id
     * @return array
     */
    public function deleteAppVersion($version_id)
    {
        $condition = [['version_id', '=', $version_id]];

        $version_info = $this->getAppVersionInfo($condition, 'version_id')['data'];
        if (empty($version_info)) {
            return $this->error('', '数据错误');
        }

        $res = model('app_version')->delete($condition);

        Cache::tag(self::CACHE_TAG_NAME)->clear();

        return ($res > 0) ? $this->success($res) : $this->error();
    }

    /**
     * 获取版本信息
     * @param $condition
     * @param string $field
     * @return array
     */
    public function getAppVersionInfo($condition, $field = '*')
    {
        $params     = json_encode(func_get_args());
        $cache_name = __FUNCTION__ . '_' . $params;

        $res = Cache::get($cache_name);
        if (empty($res)) {
            $res = model('app_version')->getInfo($condition, $field);
            $res = $this->handleData($res);
            Cache::tag(self::CACHE_TAG_NAME)->set($cache_name, $res);
        }
        return $this->success($res);
    }

    /**
     * 获取版本分页列表
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     */
    public function getAppVersionPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'version_id desc', $field = '*')
    {
        $params     = json_encode(func_get_args());
        $cache_name = __FUNCTION__ . '_' . $params;

        $res = Cache::get($cache_name);
        if (empty($res)) {

            $res = model('app_version')->pageList($condition, $field, $order, $page, $page_size);
            foreach ($res['list'] as $key => $val) {
                $res['list'][$key] = $this->handleData($val);
            }

            Cache::tag(self::CACHE_TAG_NAME)->set($cache_name, $res);
        }

        return $this->success($res);
    }

    /**
     * 获取最新版App信息
     * @param $system_type
     * @param string $field
     * @return array
     */
    public function getLatestAppVersionInfo($system_type, $field = '*')
    {
        $params     = json_encode(func_get_args());
        $cache_name = __FUNCTION__ . '_' . $params;

        $res = Cache::get($cache_name);
        if (empty($res)) {
            $res = model('app_version')->getFirstData([['system_type', '=', $system_type]], $field, 'version_id desc');
            $res = $this->handleData($res);
            Cache::tag(self::CACHE_TAG_NAME)->set($cache_name, $res);
        }
        return $this->success($res);
    }

    /**
     * 检测当前版本是否为最新
     * @param $system_type
     * @param $version_code
     * @return array
     */
    public function checkIsLatest($system_type, $version_code)
    {
        $is_atest = 1;
        $info     = $this->getLatestAppVersionInfo($system_type, 'version_code')['data'];

        if (!empty($info) && ($info['version_code'] !== $version_code)) {
            $is_atest = 0;
        }
        return $this->success($is_atest);
    }

    /**
     * 处理数据
     * @param $info
     * @return mixed
     */
    private function handleData($info)
    {
        if (!empty($info)) {
            if (isset($info['download_url']) && !empty($info['download_url'])) {
                $info['real_download_url'] = img($info['download_url']);
            }
        }
        return $info;
    }
}