<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'PROMOTION_PLATFORM_MANJIAN',
        'title' => '跨店满减',
        'url' => 'platformmanjian://admin/manjian/lists',
        'parent' => 'PROMOTION_PLATFORM',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_DETAIL',
                'title' => '活动详情',
                'url' => 'platformmanjian://admin/manjian/detail',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_ADD',
                'title' => '添加活动',
                'url' => 'platformmanjian://admin/manjian/add',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_EDIT',
                'title' => '编辑活动',
                'url' => 'platformmanjian://admin/manjian/edit',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_CLOSE',
                'title' => '关闭活动',
                'url' => 'platformmanjian://admin/manjian/close',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_DELETE',
                'title' => '删除活动',
                'url' => 'platformmanjian://admin/manjian/delete',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_MANAGE',
                'title' => '商品管理',
                'url' => 'platformmanjian://admin/manjian/manage',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_GOODS_DELETE',
                'title' => '商品删除',
                'url' => 'platformmanjian://admin/manjian/deletegoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_GOODS_CHECK',
                'title' => '商品审核',
                'url' => 'platformmanjian://admin/manjian/checkgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
        ]
    ],
];
