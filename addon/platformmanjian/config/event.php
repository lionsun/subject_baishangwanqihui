<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //展示活动
        'ShowPromotion' => [
            'addon\platformmanjian\event\ShowPromotion',
        ],
        //检测相同时间段营销活动
        'CheckSameTimePromotion' => [
            'addon\platformmanjian\event\CheckSameTimePromotion',
        ],
        //活动详情
        'PromotionInfo' => [
            'addon\platformmanjian\event\PromotionInfo',
        ],
        'PromotionPrice' => [
            'addon\platformmanjian\event\PromotionPrice',
        ],
        //跨店铺满减开启
        'OpenPlatformManjian'  => [
            'addon\platformmanjian\event\OpenDiscount',
        ],
        //跨店铺满减关闭
        'ClosePlatformManjian'  => [
            'addon\platformmanjian\event\CloseDiscount',
        ],
    ],

    'subscribe' => [
    ],
];
