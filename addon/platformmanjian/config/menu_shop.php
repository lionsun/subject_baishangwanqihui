<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'PROMOTION_PLATFORM_MANJIAN',
        'title' => '跨店满减',
        'url' => 'platformmanjian://shop/manjian/lists',
        'parent' => 'PROMOTION_CENTER',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_DETAIL',
                'title' => '活动详情',
                'url' => 'platformmanjian://shop/manjian/detail',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_MANAGE',
                'title' => '商品管理',
                'url' => 'platformmanjian://shop/manjian/manage',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_GOODS_SELECT',
                'title' => '商品选择',
                'url' => 'platformmanjian://shop/manjian/selectgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_GOODS_ADD',
                'title' => '商品添加',
                'url' => 'platformmanjian://shop/manjian/addgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_PLATFORM_MANJIAN_GOODS_DELETE',
                'title' => '商品删除',
                'url' => 'platformmanjian://shop/manjian/deletegoods',
                'sort'    => 1,
                'is_show' => 0
            ],
        ]
    ],
];
