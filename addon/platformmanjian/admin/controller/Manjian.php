<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformmanjian\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\platformmanjian\model\Manjian as ManjianModel;
use app\model\goods\GoodsCategory;
use app\model\shop\ShopGroup as ShopGroupModel;
use app\model\shop\Shop as ShopModel;
use think\Validate;


/**
 * 满减控制器
 */
class Manjian extends BaseAdmin
{



    /**
     * 满减列表 manjian
     */
    public function lists()
    {
        if(request()->isAjax()){
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $manjian_name = input('manjian_name', '');
            $status = input('status','');
            $condition = [];
            $condition[] = ['manjian_name', 'like', '%'. $manjian_name .'%'];
            if($status != null){
                $condition[] = ['status', '=', $status];
            }
            $order = 'create_time desc';
            $field = 'manjian_id,manjian_name,start_time,end_time,create_time,status,manjian_type';
            $manjian_model = new ManjianModel();
            $res = $manjian_model->getManjianPageList($condition, $page, $page_size, $order, $field);
            //获取状态名称
            $manjian_status_arr = $manjian_model->getManjianStatus();
            $getManjiantypeArray = $manjian_model->getManjiantypeArray();
            foreach($res['data']['list'] as $key=>$val){
                $res['data']['list'][$key]['status_name'] = $manjian_status_arr[$val['status']];
                $res['data']['list'][$key]['manjian_type_name'] = $getManjiantypeArray[$val['manjian_type']];
            }
            return $res;
        }else{
            //满减状态
            $manjian_model = new ManjianModel();
            $manjian_status_arr = $manjian_model->getManjianStatus();
            $this->assign('manjian_status_arr', $manjian_status_arr);
            return $this->fetch("manjian/lists");
        }
    }

    /**
     * 添加活动
     */
    public function add()
    {
        if(request()->isAjax()){
            $data = [
                'manjian_name' => input('manjian_name', ''),
                'manjian_type' => input('manjian_type', ''),
                'type_id_array' => input('type_id_array', ''),
                'start_time' => strtotime(input('start_time', '')),
                'end_time' => strtotime(input('end_time', '')),
                'rule_json' => input('rule_json', ''),
                'remark' => input('remark', ''),
                'is_audit' => input('is_audit', 0),
                'sign_start_time' => strtotime(input('sign_start_time', 0)),
                'sign_end_time' => strtotime(input('sign_end_time', 0)),
            ];
            $manjian_model = new ManjianModel();
            return $manjian_model->addManjian($data);
        }else{
            $this->assign('is_install_present', addon_is_exit('present', $this->site_id));
            $this->getShopList();
            return $this->fetch("manjian/add");
        }
    }

    /**
     * 满减编辑
     */
    public function edit()
    {
        $manjian_model = new ManjianModel();
        if(request()->isAjax()){
            $data = [
                'manjian_name' => input('manjian_name', ''),
                'manjian_type' => input('manjian_type', ''),
                'type_id_array' => input('type_id_array', ''),
                'start_time' => strtotime(input('start_time', '')),
                'end_time' => strtotime(input('end_time', '')),
                'rule_json' => input('rule_json', ''),
                'remark' => input('remark', ''),
                'is_audit' => input('is_audit', 0),
                'manjian_id' => input('manjian_id', 0),
                'sign_start_time' => strtotime(input('sign_start_time', 0)),
                'sign_end_time' => strtotime(input('sign_end_time', 0)),
            ];
            return $manjian_model->editManjian($data);
        }else{
            $manjian_id = input('manjian_id', 0);
            $manjian_info= $manjian_model->getManjianInfo(['manjian_id'=>$manjian_id])['data'];
            if(!empty($manjian_info)){
                $manjian_info['rule_json']=json_decode($manjian_info['rule_json'],true);
                if($manjian_info['manjian_type']==2){
                    $manjian_info['new_type_id_array']=explode(",",$manjian_info['type_id_array']);
                }else{
                    $manjian_info['new_type_id_array']=[];
                }
                $this->assign('manjian_info', $manjian_info);
            }
            $this->assign('is_install_present', addon_is_exit('present', $this->site_id));
            $this->getShopList( $manjian_info['type_id_array']);
            return $this->fetch("manjian/edit");
        }
    }

    /**
     * 满减关闭
     */
    public function close()
    {
        if(request()->isAjax()){
            $manjian_id = input('manjian_id', 0);
            $manjian_model = new ManjianModel();
            return $manjian_model->closeManjian($manjian_id);
        }
    }

    /**
     * 满减删除
     */
    public function delete()
    {
        if(request()->isAjax()){
            $manjian_id = input('manjian_id', 0);
            $manjian_model = new ManjianModel();
            return $manjian_model->deleteManjian($manjian_id);
        }
    }

    /**
     * 满减详情
     */
    public function manage()
    {
        $manjian_model = new ManjianModel();
        if(request()->isAjax()){
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $site_name = trim(input('site_name', ''));
            $goods_name = trim(input('goods_name', ''));

            $condition = [];
            if($site_name){
                $condition[] = ['site_name', '=', $site_name];
            }
            if($goods_name){
                $condition[] = ['goods_name', '=', $goods_name];
            }

            $field = '*';
            $order = 'id desc';
            $res = $manjian_model->getManjianGoodsPageList($condition, $field, $order, $page, $page_size);
            return $res;
        }else{
            $manjian_id = input('manjian_id', 0);
            $manjian_info = $manjian_model->getManjianInfo([['manjian_id', '=', $manjian_id]]);
            $this->assign('manjian_info',$manjian_info['data']);
            return $this->fetch('manjian/detail');
        }
    }

    /**
     * 审核通过
     */
    public function goodsAuditPass()
    {
        if(request()->isAjax()) {
            $id = input('id', 0);
            $manjian_id = input('manjian_id', 0);
            $goods_id = input('goods_id', 0);
            $manjian_model = new ManjianModel();
            $res = $manjian_model->manjianGoodsAuditPass($id,$manjian_id,$goods_id);
            return $res;
        }
    }

    /**
     * 审核拒绝
     */
    public function goodsAuditRefuse()
    {
        if(request()->isAjax()) {
            $id = input('id', 0);
            $audit_remark = input('audit_remark', '');
            $manjian_model = new ManjianModel();
            $res = $manjian_model->manjianGoodsAuditRefuse($id,$audit_remark);
            return $res;
        }
    }

    /**
     * 删除商品
     */
    public function deleteGoods()
    {
        if(request()->isAjax()) {
            $goods_id = input('goods_id', 0);
            $site_id = input('site_id', 0);
            $manjian_id = input('manjian_id', 0);
            $manjian_model = new ManjianModel();
            return $manjian_model->deleteManjianGoods($manjian_id, $site_id, $goods_id);
        }
    }


    /**************************************public 公共到******************************************************/



    /**
     *  店铺列表
     */

    public function getShopList($type_id_array='')
    {
        $shop_model = new ShopModel();
        $shoplist=  $shop_model->getShopList([], 'site_id as value,site_name as  title ', 'site_id desc')['data'];
        /*if(!empty($type_id_array)){
         foreach ($shoplist  as  $k=>$v  ){
             if(in_array($v['value'],explode(",",$type_id_array))){
                 $shoplist[$k]['disabled']=true;
             }
         }
        }*/
        $this->assign('shoplist',$shoplist);
    }

    /**
     * 详情
     */
    public function detail()
    {
        if (request()->isAjax()) {
            //活动商品
            $manjian_id = input('manjian_id', 0);
            $manjian_model = new ManjianModel();
            $condition[] = ['manjian_id','=',$manjian_id];
            $list = $manjian_model->getManjianGoodsList($condition);
            return $list;
        } else {
            $manjian_id = input('manjian_id', 0);
            $this->assign('manjian_id', $manjian_id);
            $condition[] = ['manjian_id','=',$manjian_id];
            //活动详情
            $manjian_model = new ManjianModel();
            $manjian_info = $manjian_model->getManjianInfo($condition)['data'];
            $type_id_array = $manjian_info['type_id_array'];
            if($manjian_info['manjian_type']==1){
                $manjian_info['use_limit'] = '全平台';
            }else if($manjian_info['manjian_type']==2){
                $shop_model = new ShopModel();
                $shop_list = $shop_model->getShopList([['site_id','in',$type_id_array]], 'site_id as value,site_name as  title ', 'site_id desc')['data'];
                $manjian_info['use_limit']  = implode(",", array_column($shop_list,'title'));
            }else if($manjian_info['manjian_type']==3){
                $goods_category_model = new GoodsCategory();
                $goods_category_list =$goods_category_model->getCategoryTree([['category_id','in',$type_id_array]], 'category_id,category_name,level,pid', 'category_id desc')['data'];
                $manjian_info['use_limit']  = $goods_category_list;
            }
            $this->assign('manjian_info', $manjian_info);

            return $this->fetch("manjian/detail");
        }
    }



}