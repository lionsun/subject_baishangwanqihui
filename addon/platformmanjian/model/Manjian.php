<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\platformmanjian\model;

use app\model\system\Cron;
use app\model\BaseModel;
use think\facade\Cache;
use think\facade\Db;
use app\model\system\Promotion as PromotionModel;

/**
 * 满减
 */
class Manjian extends BaseModel
{
    const PROMOTION_TYPE = 'platformmanjian';
    const PROMOTION_TYPE_NAME = '跨店铺满减';

    //满减类型
    const MANJIAN_TYPE_ALL = 1;
    const MANJIAN_TYPE_SPECIAL_SHOP = 2;
    const MANJIAN_TYPE_SPECIAL_CATEGORY = 3;

    //活动状态
    const MANJIAN_STATUS_NOT_START = 0;
    const MANJIAN_STATUS_IN_PROCESS = 1;
    const MANJIAN_STATUS_ENDED = 2;
    const MANJIAN_STATUS_CLOSED = -1;

    /**
     * 获取状态
     * @return string[]
     */
    public static function getManjianStatus()
    {
        $arr = [
            self::MANJIAN_STATUS_NOT_START => '未开始',
            self::MANJIAN_STATUS_IN_PROCESS => '进行中',
            self::MANJIAN_STATUS_ENDED => '已结束',
            self::MANJIAN_STATUS_CLOSED => '已关闭',
        ];
        return $arr;
    }

    /**
     * 获取类型名称 MANJIAN_TYPE
     * @return string[]
     */
    public static function getManjiantypeArray()
    {
        $arr = [
            self::MANJIAN_TYPE_ALL => '全平台',
            self::MANJIAN_TYPE_SPECIAL_SHOP => '指定店铺',
            self::MANJIAN_TYPE_SPECIAL_CATEGORY => '指定品类',
        ];
        return $arr;
    }


    /**
     * 获取状态名称
     * @param $id
     * @return string
     */
    public static function getManjiantypeName($id)
    {
        $status = self::getManjiantypeArray();
        if(isset($status[$id])){
            $name = $status[$id];
        }else{
            $name = '';
        }
        return $name;
    }


    /**
     * 获取状态名称
     * @param $id
     * @return string
     */
    public static function getDiscountStatusName($id)
    {
        $status = self::getManjianStatus();
        if(isset($status[$id])){
            $name = $status[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    //商品状态
    const GOODS_STATUS_WAIT_AUDIT = 0;
    const GOODS_STATUS_AUDIT_PASS = 1;
    const GOODS_STATUS_AUDIT_REFUSE = 2;

    /**
     * 获取状态
     * @return string[]
     */
    public static function getGoodsStatus()
    {
        $arr = [
            self::MANJIAN_STATUS_NOT_START => '未开始',
            self::MANJIAN_STATUS_IN_PROCESS => '进行中',
            self::MANJIAN_STATUS_ENDED => '已结束',
            self::MANJIAN_STATUS_CLOSED => '已关闭',
        ];
        return $arr;
    }

    /**
     * 获取状态名称
     * @param $id
     * @return string
     */
    public static function getGoodsStatusName($id)
    {
        $status = self::getGoodsStatus();
        if(isset($status[$id])){
            $name = $status[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    /**
     * 添加满减
     * @param array $data
     */
    public function addManjian($data)
    {
        //时间检测
        if ($data[ 'end_time' ] < time()) {
            return $this->error('', '结束时间不能早于当前时间');
        }
        if ($data[ 'start_time' ] <= time()) {
            $data[ 'status' ] = self::MANJIAN_STATUS_IN_PROCESS;//直接启动
        } else {
            $data[ 'status' ] = self::MANJIAN_STATUS_NOT_START;
        }
        model('promotion_platform_manjian')->startTrans();
        try {
            $data[ 'create_time' ] = time();
            $manjian_id = model('promotion_platform_manjian')->add($data);
            $cron = new Cron();
            if ($data[ 'start_time' ] <= time()) {
                $cron->addCron(1, 0, "满减送关闭", "ClosePlatformManjian", $data[ 'end_time' ], $manjian_id);
            } else {
                $cron->addCron(1, 0, "满减送开启", "OpenPlatformManjian", $data[ 'start_time' ], $manjian_id);
                $cron->addCron(1, 0, "满减送关闭", "ClosePlatformManjian", $data[ 'end_time' ], $manjian_id);
            }

            model('promotion_platform_manjian')->commit();
            return $this->success();
        } catch ( \Exception $e ) {
            model('promotion_platform_manjian')->rollback();
            return $this->error('', $e->getMessage());
        }

    }

    /**
     * 修改满减
     * @param array $data
     */
    public function editManjian($data)
    {
        $manjian_id = $data[ 'manjian_id' ];
        /*$manjian_status = model('promotion_platform_manjian')->getInfo([['site_id', '=', $data[ 'site_id' ]], ['manjian_id', '=', $manjian_id]], 'status');
        if ($manjian_status[ 'status' ] != 0) {
            return $this->error('', '只有未开始的满减送活动才能进行修改');
        }*/
        //时间检测
        if ($data[ 'end_time' ] < time()) {
            return $this->error('', '结束时间不能早于当前时间');
        }
        if ($data[ 'start_time' ] <= time()) {
            $data[ 'status' ] = self::MANJIAN_STATUS_IN_PROCESS;//直接启动
        } else {
            $data[ 'status' ] = self::MANJIAN_STATUS_NOT_START;
        }
        model('promotion_platform_manjian')->startTrans();
        try {
            model('promotion_platform_manjian')->update($data, [['manjian_id', '=', $manjian_id]]);
            $cron = new Cron();
            $cron->deleteCron([['event', '=', 'ClosePlatformManjian'], ['relate_id', '=', $manjian_id]]);
            $cron->deleteCron([['event', '=', 'OpenPlatformManjian'], ['relate_id', '=', $manjian_id]]);
            if ($data[ 'start_time' ] <= time()) {
                $cron->addCron(1, 0, "满减送关闭", "ClosePlatformManjian", $data[ 'end_time' ], $manjian_id);
            } else {
                $cron->addCron(1, 0, "满减送开启", "OpenPlatformManjian", $data[ 'start_time' ], $manjian_id);
                $cron->addCron(1, 0, "满减送关闭", "ClosePlatformManjian", $data[ 'end_time' ], $manjian_id);
            }
            //如果不需要审核将待审核的商品都改为审核通过
            if($data['is_audit'] == 0){
                model('promotion_platform_manjian_goods')->update(['audit_status' => self::GOODS_STATUS_AUDIT_PASS], [['manjian_id', '=', $data['manjian_id'], ['audit_status', '=', self::GOODS_STATUS_WAIT_AUDIT]]]);
            }
            model('promotion_platform_manjian')->commit();
            return $this->success();
        } catch ( \Exception $e ) {
            model('promotion_platform_manjian')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 删除满减
     * @param unknown $manjian_id
     */
    public function deleteManjian($manjian_id)
    {
        $condition = [
            ['manjian_id', '=', $manjian_id],
        ];
        $res = model('promotion_platform_manjian')->delete($condition);
        if ($res) {
            model('promotion_platform_manjian_goods')->delete($condition);
            $cron = new Cron();
            $cron->deleteCron([['event', '=', 'OpenPlatformManjian'], ['relate_id', '=', $manjian_id]]);
            $cron->deleteCron([['event', '=', 'ClosePlatformManjian'], ['relate_id', '=', $manjian_id]]);
            $data = [
                'promotion_type'     => '',
                'start_time'         => 0,
                'end_time'           => 0,
                'manjian_discount_id' => 0,
            ];
            model('goods')->update($data, [['manjian_discount_id', '=', $manjian_id]]);
            model('goods_sku')->update($data, [['manjian_discount_id', '=', $manjian_id]]);
            return $this->success($res);
        } else {
            return $this->error();
        }
    }

    /**
     * 获取满减信息
     * @param array $condition
     * @param string $field
     */
    public function getManjianInfo($condition, $field = '*')
    {
        $res = model('promotion_platform_manjian')->getInfo($condition, $field);
        return $this->success($res);
    }

    /**
     * 获取满减列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getManjianList($condition = [], $field = '*', $order = 'manjian_id desc', $limit = null)
    {
        $list = model('promotion_platform_manjian')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($list);
    }

    /**
     * 获取满减分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getManjianPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('promotion_platform_manjian')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }

    /**
     * 获取满减列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getManjianGoodsList($condition = [], $field = '', $order = 'id desc', $limit = null)
    {
        $list = model('promotion_platform_manjian_goods')->getList($condition, $field, $order);
        foreach ($list as $key=>$val){
            $goods_info = model('goods')->getInfo([['goods_id','=',$val['goods_id']]],'price,goods_name,goods_image');
            $list[$key]['price'] = $goods_info['price'];
            $list[$key]['goods_name'] = $goods_info['goods_name'];
            $list[$key]['goods_image'] = $goods_info['goods_image'];
        }
        return $this->success($list);
    }

    /**
     * 获取满减商品分页列表
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getManjianGoodsPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
    {
        $list = model('promotion_platform_manjian_goods')->pageList($condition, $field, $order, $page, $page_size);
        return $this->success($list);
    }

    /**
     * 启动满减
     * @param unknown $manjian_id
     */
    public function cronOpenPlatformManjian($manjian_id)
    {
        $manjian_info = model('promotion_platform_manjian')->getInfo([['manjian_id', '=', $manjian_id]], 'start_time,status,discount_goods');
        if (!empty($manjian_info)) {
            if ($manjian_info[ 'start_time' ] <= time() && $manjian_info[ 'status' ] == self::MANJIAN_STATUS_NOT_START) {
                model('promotion_platform_manjian')->startTrans();
                try {
                    model('promotion_platform_manjian')->update(['status' => self::MANJIAN_STATUS_IN_PROCESS], [['manjian_id', '=', $manjian_id]]);
                    model('promotion_platform_manjian_goods')->update(['status' => self::MANJIAN_STATUS_IN_PROCESS], [['manjian_id', '=', $manjian_id]]);
                    model('promotion_platform_manjian')->commit();
                    $data = [
                        'promotion_type'     => self::PROMOTION_TYPE,
                        'start_time'         => $manjian_info['start_time'],
                        'end_time'           => $manjian_info['end_time'],
                        'manjian_discount_id' => 0,
                    ];
                    model('goods')->update($data, [['goods_id', 'in', $manjian_info['discount_goods']]]);
                    model('goods_sku')->update($data, [['goods_id', 'in', $manjian_info['discount_goods']]]);
                    return $this->success();
                } catch ( \Exception $e ) {
                    model('promotion_platform_manjian')->rollback();
                    return $this->error('', $e->getMessage());
                }
            } else {
                return $this->error("", "满减送活动已开启或者关闭");
            }
        } else {
            return $this->error("", "满减送活动不存在");
        }
    }

    /**
     * 结束满减 自动事件
     * @param unknown $manjian_id
     */
    public function cronClosePlatformManjian($manjian_id)
    {
        $manjian_info = model('promotion_platform_manjian')->getInfo([['manjian_id', '=', $manjian_id]], 'status');
        if (!empty($manjian_info)) {
            if ($manjian_info[ 'status' ] == self::MANJIAN_STATUS_IN_PROCESS) {
                model('promotion_platform_manjian')->startTrans();
                try {
                    model('promotion_platform_manjian')->update(['status' => self::MANJIAN_STATUS_ENDED], [['manjian_id', '=', $manjian_id]]);
                    model('promotion_platform_manjian_goods')->update(['status' => self::MANJIAN_STATUS_ENDED], [['manjian_id', '=', $manjian_id]]);
                    $data = [
                        'promotion_type'     => '',
                        'start_time'         => 0,
                        'end_time'           => 0,
                        'manjian_discount_id' => 0,
                    ];
                    model('goods')->update($data, [['manjian_discount_id', '=', $manjian_id]]);
                    model('goods_sku')->update($data, [['manjian_discount_id', '=', $manjian_id]]);
                    model('promotion_platform_manjian')->commit();
                    return $this->success();
                } catch (\Exception $e) {

                    model('promotion_platform_manjian')->rollback();
                    return $this->error('', $e->getMessage());
                }
            } else {
                return $this->error("", "跨店铺满减活动已关闭");
            }
        } else {
            return $this->error("", "跨店铺满减活动不存在");
        }
    }

    /**
     * 关闭满减 手动关闭
     * @param unknown $manjian_id
     */
    public function closeManjian($manjian_id)
    {
        $condition = array(
            ['manjian_id', '=', $manjian_id],
        );
        $manjian_info = model('promotion_platform_manjian')->getInfo($condition, 'start_time,end_time,status');
        if (!empty($manjian_info)) {
            if ($manjian_info[ 'status' ] == self::MANJIAN_STATUS_IN_PROCESS) {
                model('promotion_platform_manjian')->startTrans();
                try {
                    model('promotion_platform_manjian')->update(['status' => self::MANJIAN_STATUS_CLOSED], [['manjian_id', '=', $manjian_id]]);
                    model('promotion_platform_manjian_goods')->update(['status' => self::MANJIAN_STATUS_CLOSED], [['manjian_id', '=', $manjian_id]]);
                    $data = [
                        'promotion_type'     => '',
                        'start_time'         => 0,
                        'end_time'           => 0,
                        'manjian_discount_id' => 0,
                    ];
                    model('goods')->update($data, [['manjian_discount_id', '=', $manjian_id]]);
                    model('goods_sku')->update($data, [['manjian_discount_id', '=', $manjian_id]]);
                    model('promotion_platform_manjian')->commit();
                    return $this->success();
                } catch ( \Exception $e ) {
                    model('promotion_platform_manjian')->rollback();
                    return $this->error('', $e->getMessage());
                }
            } else {
                return $this->error("", "满减送活动已关闭");
            }
        } else {
            return $this->error("", "满减送活动不存在");
        }
    }

    /**
     * 添加满减商品
     * @param $manjian_id
     * @param $site_id
     * @param $goods_id
     */
    public function addManjianGoods($manjian_id, $site_id, $goods_ids)
    {
        $manjian_info = model('promotion_platform_manjian')->getInfo([['manjian_id', '=', $manjian_id]], '*');
        if(empty($manjian_info)) return $this->error('', '满减信息有误');

        $goods_list = model('goods')->getList([['site_id', '=', $site_id], ['goods_id', 'in', $goods_ids]]);
        if(empty($goods_list)) return $this->error('', '商品信息有误');
        $count = model('promotion_platform_manjian_goods')->getCount([['manjian_id', '=', $manjian_id], ['site_id', '=', $site_id], ['goods_id', 'in', $goods_ids]]);
        if($count > 0) return $this->error('', '该商品已加入活动中，请勿重复添加');

        foreach($goods_list as $key=>$val){
            //如果为限定品类时,需要判断商品是否在品类内
            if($manjian_info['manjian_type']==3){
                $type_id_array = $manjian_info['type_id_array'];
                if(explode($val['category_id'],$type_id_array) || explode($val['category_id_1'],$type_id_array) || explode($val['category_id_2'],$type_id_array) || explode($val['category_id_3'],$type_id_array)){

                }else{
                    unset($goods_list[$key]);
                    return $this->error('该商品不能参加此活动');
                }
            }
            //判断是否存在相同时间段营销活动
            $promotion_model = new PromotionModel();
            $res = $promotion_model->checkSameTimePromotion([
                'mode' => 'spu',
                'id' => $val['goods_id'],
                'time_info' => ['start_time' => $manjian_info['start_time'], 'end_time' => $manjian_info['end_time']]
            ]);
            if(count($res) > 0) return $this->error('存在相同时间段营销活动');
        }
        if(!empty($goods_list)){
            foreach($goods_list as $key=>$val){
                $goods_data[] = [
                    'manjian_id' => $manjian_id,
                    'site_id' => $val['site_id'],
                    'site_name' => $val['site_name'],
                    'goods_id' => $val['goods_id'],
                    'status' => $manjian_info['status'],
                    'rule_json' => $manjian_info['rule_json'],
                    'start_time' => $manjian_info['start_time'],
                    'end_time' => $manjian_info['end_time'],
                    'audit_status' => $manjian_info['is_audit'] == 1 ? self::GOODS_STATUS_WAIT_AUDIT : self::GOODS_STATUS_AUDIT_PASS,
                ];
                if($manjian_info['is_audit'] == 0){
                    $data = [
                        'promotion_type'     => self::PROMOTION_TYPE,
                        'manjian_discount_id' => $manjian_id,
                        'start_time' => $manjian_info['start_time'],
                        'end_time' => $manjian_info['end_time'],
                    ];
                    model('goods')->update($data, ['goods_id'=>$val['goods_id']]);
                    model('goods_sku')->update($data, ['goods_id'=>$val['goods_id']]);
                }
            }
            model('promotion_platform_manjian_goods')->addList($goods_data);
        }

        return $this->success();
    }

    /**
     * 删除满减商品
     * @param $manjian_id
     * @param $site_id
     * @param $goods_id
     */
    public function deleteManjianGoods($manjian_id, $site_id, $goods_id)
    {
        model('promotion_platform_manjian_goods')->delete([['manjian_id', '=', $manjian_id], ['site_id', '=', $site_id], ['goods_id', '=', $goods_id]]);
        $data = [
            'promotion_type'     => '',
            'start_time'         => 0,
            'end_time'           => 0,
            'manjian_discount_id' => 0,
        ];
        model('goods')->update($data, [['goods_id', '=', $goods_id]]);
        model('goods_sku')->update($data, [['goods_id', '=', $goods_id]]);
        return $this->success();
    }

    /**
     * 满减商品审核通过
     * @param $id
     */
    public function manjianGoodsAuditPass($id,$manjian_id,$goods_id)
    {
        $manjian_info = model('promotion_platform_manjian')->getInfo([['manjian_id', '=', $manjian_id]],'manjian_id,start_time,end_time');
        if($manjian_info){
            $data = [
                'promotion_type'     => self::PROMOTION_TYPE,
                'manjian_discount_id' => $manjian_info['manjian_id'],
                'start_time' => $manjian_info['start_time'],
                'end_time' => $manjian_info['end_time'],
            ];
            model('promotion_platform_manjian_goods')->update(['audit_status' => self::GOODS_STATUS_AUDIT_PASS], [['id', '=', $id]]);
            model('goods')->update($data, [['goods_id','=',$goods_id]]);
            model('goods_sku')->update($data, [['goods_id','=',$goods_id]]);
        }
        return $this->success();
    }

    /**
     * 满减商品审核拒绝
     * @param $id
     * @param $remark
     */
    public function manjianGoodsAuditRefuse($id, $remark)
    {
        model('promotion_platform_manjian_goods')->update(['audit_status' => self::GOODS_STATUS_AUDIT_REFUSE, 'audit_remark' => $remark], ['id'=> $id]);
        return $this->success();
    }

    public function againCheckManjianGoods($id)
    {
        model('promotion_platform_manjian_goods')->update(['audit_status' => self::GOODS_STATUS_WAIT_AUDIT], ['id'=> $id]);
        return $this->success();
    }
}