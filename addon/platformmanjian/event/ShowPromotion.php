<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformmanjian\event;

/**
 * 活动展示
 */
class ShowPromotion
{

    /**
     * 活动展示
     *
     */
	public function handle()
	{
        $data = [
            'admin' => [
                [
                    //插件名称
                    'name' => 'platformmanjian',
                    //展示分类（根据平台端设置，admin（平台营销），shop：店铺营销，member:会员营销, tool:应用工具）
                    'show_type' => 'admin',
                    //展示主题
                    'title' => '跨店铺满减',
                    //展示介绍
                    'description' => '跨店铺满减功能',
                    //展示图标
                    'icon' => 'addon/manjian/icon.png',
                    //跳转链接
                    'url' => 'platformmanjian://admin/manjian/lists',
                ]
            ],
            'shop' => [
                [
                    //插件名称
                    'name' => 'platformmanjian',
                    //展示分类（根据平台端设置，admin（平台营销），shop：店铺营销，member:会员营销, tool:应用工具）
                    'show_type' => 'admin',
                    //展示主题
                    'title' => '跨店铺满减',
                    //展示介绍
                    'description' => '跨店铺满减功能',
                    //展示图标
                    'icon' => 'addon/seckill/icon.png',
                    //跳转链接
                    'url' => 'platformmanjian://shop/manjian/lists',
                ]
            ]

        ];
        return $data;
	}
}