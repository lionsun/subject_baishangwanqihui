<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformmanjian\event;
use addon\platformmanjian\model\Manjian as ManjianModel;

/**
 * 检测相同时间段营销活动
 */
class CheckSameTimePromotion
{
	public function handle($param)
	{
        $mode = $param['mode'];
        $id = $param['id'];
        $time_info = $param['time_info'];
        if($mode == 'spu'){
            $manjian_model = new ManjianModel();
            //折扣列表  //status
            $manjian_list = $manjian_model->getManjianGoodsList([['goods_id', '=', $id],['status', '<>', -1]]);
            //是否存在相同时间折扣
            $exist_same_time_promotion = 0;
            $show_info = [];
            foreach($manjian_list['data'] as $d_item){
                if($time_info['start_time'] <= $d_item['start_time'] && $time_info['end_time'] >= $d_item['end_time']){
                    $exist_same_time_promotion = 1;
                }
                if($time_info['start_time'] >= $d_item['start_time'] && $time_info['end_time'] <= $d_item['end_time']){
                    $exist_same_time_promotion = 1;
                }
                if($time_info['start_time'] <= $d_item['start_time'] && $time_info['end_time'] >= $d_item['start_time']){
                    $exist_same_time_promotion = 1;
                }
                if($time_info['start_time'] <= $d_item['end_time'] && $time_info['end_time'] >= $d_item['end_time']){
                    $exist_same_time_promotion = 1;
                }
                $show_info[] = [
                    'name' => $d_item['manjian_name'] ?? '',
                    'start_time' => $d_item['start_time'],
                    'end_time' => $d_item['end_time'],
                ];
            }
            if($exist_same_time_promotion){
                $res = [
                    'promotion_list' => $show_info,
                    'type' => ManjianModel::PROMOTION_TYPE,
                    'type_name' => ManjianModel::PROMOTION_TYPE_NAME,
                ];
                return $res;
            }
        }
	}
}