<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\platformmanjian\event;
use addon\platformmanjian\model\Manjian as ManjianModel;

/**
 * 活动展示
 */
class PromotionInfo
{

    /**
     * 活动详情
     * @param $sku_info
     */
	public function handle($sku_info)
	{
	    if(isset($sku_info['promotion_type']) && $sku_info['promotion_type'] == ManjianModel::PROMOTION_TYPE) {
            $condition = [
                ['manjian_id', '=', $sku_info['manjian_discount_id']]
            ];
            $model = new ManjianModel();
            $manjian_info = $model->getManjianInfo($condition);
            if(!empty($manjian_info['data'])){
                $sku_info['promotion_info'] = [
                    'type' => ManjianModel::PROMOTION_TYPE,
                    'info' => $manjian_info['data'],
                    'type_name' => ManjianModel::PROMOTION_TYPE_NAME
                ];
                return $sku_info;
            }
        }
	}
}