<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\newusercoupon\event;
use addon\newusercoupon\model\CouponActivity as CouponActivityModel;

/**
 * 活动展示
 */
class PromotionInfo
{

    /**
     * 活动详情
     * @param $sku_info
     */
	public function handle($sku_info)
	{

	    if(isset($sku_info['promotion_type']) && $sku_info['promotion_type'] == CouponActivityModel::PROMOTION_TYPE) {
            $model = new CouponActivityModel();
            $activity_info = $model->getActivityInfo([['activity_id','=',$sku_info['new_user_coupon_id']]]);
            if(!empty($activity_info['data'])){
                $sku_info['promotion_info'] = [
                    'type' => CouponActivityModel::PROMOTION_TYPE,
                    'info' => $activity_info['data'],
                    'type_name' => CouponActivityModel::PROMOTION_TYPE_NAME
                ];
                return $sku_info;
            }
        }
	}
}