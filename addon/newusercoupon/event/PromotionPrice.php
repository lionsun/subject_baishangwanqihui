<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\newusercoupon\event;
use addon\newusercoupon\model\CouponActivity as CouponActivityModel;

/**
 * 活动展示
 */
class PromotionPrice
{

    /**
     * 活动详情
     * @param $sku_info
     */
	public function handle($sku_info)
	{
        if(isset($sku_info['promotion_type']) && $sku_info['promotion_type'] == CouponActivityModel::PROMOTION_TYPE) {
            if(isset($sku_info['promotion_info'])){
                $sku_info['discount_price']   = $sku_info['price'];
                $sku_info['promotion_price']  = $sku_info['price'] - $sku_info['discount_price'];
                $sku_info['goods_money']      = $sku_info['price'] * $sku_info['num'];
                $sku_info['real_goods_money'] = $sku_info['goods_money'];
                $sku_info['promotion_money']  = 0;
                return $sku_info;
            }
        }
	}
}