<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newusercoupon\admin\controller;

use addon\newusercoupon\model\CouponActivity as CouponActivityModel;
use app\admin\controller\BaseAdmin;
use addon\newusercoupon\model\CouponActivity as ActivityModel;
use addon\newusercoupon\model\Coupon as CouponModel;


/**
 * 优惠券控制器
 */
class Coupon extends BaseAdmin
{
    /**
     * 活动列表
     */
    public function lists()
    {
        if(request()->isAjax()){
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $coupon_name = input('coupon_name', '');
            $status = input('status','');
            $condition = [];
            $condition[] = ['coupon_name', 'like', '%'. $coupon_name .'%'];
            if($status != null){
                $condition[] = ['status', '=', $status];
            }
            $order = 'create_time desc';
            $field = 'activity_id,coupon_name,start_time,end_time,create_time,status,coupon_type,limit_money,money,discount,max_fetch,total_count,validity_period,page';
            $activity_model = new ActivityModel();
            $res = $activity_model->getActivityPageList($condition, $page, $page_size, $order, $field);
            foreach($res['data']['list'] as $key=>$val){
                $res['data']['list'][$key]['status_name'] = ActivityModel::getActivityStatusName($val['status']);
                $res['data']['list'][$key]['coupon_type_name'] = ActivityModel::getCouponTypeName($val['coupon_type']);
                $res['data']['list'][$key]['day_time'] = (int)($res['data']['list'][$key]['validity_period'] / (24 * 60 * 60));
                $res['data']['list'][$key]['hour_time'] = (int)(($res['data']['list'][$key]['validity_period'] / 60 % (24 * 60)) / 60);
                $res['data']['list'][$key]['second_time'] = (int)(($res['data']['list'][$key]['validity_period'] / 60 % (24 * 60)) % 60);
            }
            return $res;
        }else{
            //活动状态
            $activity_status_arr = ActivityModel::getActivityStatus();
            $this->assign('activity_status_arr', $activity_status_arr);

            $coupon_type_arr = ActivityModel::getCouponType();
            $this->assign('coupon_type_arr', $coupon_type_arr);
            return $this->fetch("coupon/lists");
        }
    }

    /**
     * 添加活动
     */
    public function add()
    {
        if(request()->isAjax()){
            $data = [
                'coupon_name' => input('coupon_name', ''),
                'coupon_type' => input('coupon_type', ActivityModel::COUPON_TYPE_MANJIAN),
                'start_time' => strtotime(input('start_time', '')),
                'end_time' => strtotime(input('end_time', '')),
                'limit_money' => input('limit_money', 0),
                'money' => input('money', 0),
                'discount' => input('discount', 0),
                'validity_period' => input('validity_period', 0),
                'total_count' => input('total_count', 0),
                'max_fetch' => input('max_fetch', 0),
                'platform_rate' => input('platform_rate', 0),
                'shop_rate' => input('shop_rate', 0),
                'image' => input('image', '')
            ];
            $activity_model = new ActivityModel();
            return $activity_model->addActivity($data);
        }else{
            $coupon_type_arr = ActivityModel::getCouponType();
            $this->assign('coupon_type_arr', $coupon_type_arr);
            return $this->fetch("coupon/add");
        }
    }

    /**
     * 满减编辑
     */
    public function edit()
    {
        $activity_model = new ActivityModel();
        if(request()->isAjax()){
            $data = [
                'coupon_name' => input('coupon_name', ''),
                'coupon_type' => input('coupon_type', ActivityModel::COUPON_TYPE_MANJIAN),
                'start_time' => strtotime(input('start_time', '')),
                'end_time' => strtotime(input('end_time', '')),
                'limit_money' => input('limit_money', 0),
                'money' => input('money', 0),
                'discount' => input('discount', 0),
                'validity_period' => input('validity_period', 0),
                'total_count' => input('total_count', 0),
                'max_fetch' => input('max_fetch', 0),
                'platform_rate' => input('platform_rate', 0),
                'shop_rate' => input('shop_rate', 0),
                'activity_id' => input('activity_id', 0),
                'image' => input('image', '')
            ];
            return $activity_model->editActivity($data);
        }else{

            $activity_id = input('activity_id', 0);
            $activity_info= $activity_model->getActivityInfo(['activity_id'=>$activity_id])['data'];
            $this->assign('activity_info', $activity_info);

            $coupon_type_arr = ActivityModel::getCouponType();
            $this->assign('coupon_type_arr', $coupon_type_arr);

            return $this->fetch("coupon/edit");
        }
    }

    /**
     * 满减关闭
     */
    public function close()
    {
        if(request()->isAjax()){
            $activity_id = input('activity_id', 0);
            $activity_model = new ActivityModel();
            return $activity_model->closeActivity($activity_id);
        }
    }

    /**
     * 满减删除
     */
    public function delete()
    {
        if(request()->isAjax()){
            $activity_id = input('activity_id', 0);
            $activity_model = new ActivityModel();
            return $activity_model->deleteActivity($activity_id);
        }
    }

    /**
     * 商品管理
     */
    public function manage()
    {
        $activity_model = new CouponActivityModel();
        if (request()->isAjax()) {
            //商品列表
            $activity_id = input('activity_id', 0);

            $res = $activity_model->getActivityGoodsList(['activity_id'=>$activity_id]);
            return $res;
        } else {
            $activity_id = input('activity_id', 0);
            $this->assign('activity_id', $activity_id);
            $coupon_type_status_arr = $activity_model->getCouponTypeStatus();
            //活动详情
            $activity_info = $activity_model->getActivityInfo(['activity_id'=>$activity_id], $this->site_id)['data'];
            if(!empty($activity_info)){
                $activity_info['status_name'] = $coupon_type_status_arr[$activity_info['status']];;
                $activity_info['day_time']    = (int)($activity_info['validity_period'] / (24 * 60 * 60));
                $activity_info['hour_time']   = (int)(($activity_info['validity_period'] / 60 % (24 * 60)) / 60);
                $activity_info['second_time'] = (int)(($activity_info['validity_period'] / 60 % (24 * 60)) % 60);
            }
            $this->assign('activity_info', $activity_info);

            return $this->fetch("coupon/manage");
        }
    }

    /**
     * 删除商品
     */
    public function deleteGoods()
    {
        if (request()->isAjax()) {
            $actiivity_id = input('activity_id', 0);
            $goods_id = input('goods_id', '');
            $discount_model = new CouponActivityModel();
            return $discount_model->deleteActivityGoods($actiivity_id, $goods_id);
        }
    }
    /**
     * 限时折扣详情
     */
    public function detail()
    {
        if (request()->isAjax()) {
            //活动商品
            $activity_id = input('activity_id', 0);
            $activity_model = new ActivityModel();
            $condition[] = ['activity_id','=',$activity_id];
            $list = $activity_model->getActivityGoodsList($condition);
            return $list;
        } else {
            $activity_id = input('activity_id', 0);
            $this->assign('activity_id', $activity_id);
            $condition[] = ['activity_id','=',$activity_id];
            //活动详情
            $activity_model = new ActivityModel();
            $activity_info = $activity_model->getActivityInfo($condition)['data'];
            if(!empty($activity_info)){
                $activity_info['day_time']    = (int)($activity_info['validity_period'] / (24 * 60 * 60));
                $activity_info['hour_time']   = (int)(($activity_info['validity_period'] / 60 % (24 * 60)) / 60);
                $activity_info['second_time'] = (int)(($activity_info['validity_period'] / 60 % (24 * 60)) % 60);
            }
            $this->assign('coupon_type_info', $activity_info);

            return $this->fetch("coupon/detail");
        }
    }

    /**
     * 优惠券领取记录
     * */
    public function receive()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $activity_id = input('activity_id', 0);
            $status = input('status', '');
            $condition = [];
            $condition[] = ['nuc.activity_id', '=', $activity_id];
            if($status!=''){
                $condition[] = ['nuc.status', '=', $status];
            }
            $alias = 'nuc';
            $join = [
                [ 'member m', 'nuc.member_id = m.member_id', 'inner' ]
            ];
            $field = "nuc.*,m.member_id,m.nickname";
            $coupon_model = new CouponModel();
            $res = $coupon_model->getCouponPageList($condition, $page, $page_size,'fetch_time desc',$field, $alias, $join);
            return $res;
        } else {
            $activity_id = input('activity_id', 0);
            $this->assign('activity_id', $activity_id);
            return $this->fetch("coupon/receive");
        }
    }


    public function modifyPage(){
        $activity_id = input('activity_id', 0);
        $page = input('page', '');
        $activity_model = new CouponActivityModel();
        return $activity_model->modifyPage($activity_id,$page);
    }
}