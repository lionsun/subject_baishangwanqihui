<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newusercoupon\shop\controller;


use addon\newusercoupon\model\Manjian as ManjianModel;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\shop\ShopCategory;
use app\shop\controller\BaseShop;
use think\facade\Db;

/**
 * 满额折控制器
 */
class Manjian extends BaseShop
{
    /**
     * 满减列表
     */
    public function lists()
    {
        if(request()->isAjax()){
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $manjian_name = input('manjian_name', '');
            $status = input('status','');
            $condition = [];
            $manjian_type_all = ManjianModel::MANJIAN_TYPE_ALL;
            $manjian_type_special_shop = ManjianModel::MANJIAN_TYPE_SPECIAL_SHOP;
            $manjian_type_special_category = ManjianModel::MANJIAN_TYPE_SPECIAL_CATEGORY;
            $condition[] = ["", "exp", Db::raw(" manjian_type = {$manjian_type_all} or manjian_type = {$manjian_type_special_category} or (manjian_type = {$manjian_type_special_shop} and find_in_set({$this->site_id},type_id_array))")];
            $condition[] = ['manjian_name', 'like', '%'. $manjian_name .'%'];
            if($status != null){
                $condition[] = ['status', '=', $status];
            }
            $order = 'create_time desc';
            $field = 'manjian_id,manjian_name,start_time,end_time,create_time,status,manjian_type';

            $manjian_model = new ManjianModel();
            $res = $manjian_model->getManjianPageList($condition, $page, $page_size, $order, $field);
            //获取状态名称
            $manjian_status_arr = $manjian_model->getManjianStatus();
            $getManjiantypeArray = $manjian_model->getManjiantypeArray();
            foreach($res['data']['list'] as $key=>$val){
                $res['data']['list'][$key]['status_name'] = $manjian_status_arr[$val['status']];
                $res['data']['list'][$key]['manjian_type_name'] = $getManjiantypeArray[$val['manjian_type']];
            }
            return $res;

        }else{
            //满减状态
            $manjian_model = new ManjianModel();
            $manjian_status_arr = $manjian_model->getManjianStatus();
            $this->assign('manjian_status_arr', $manjian_status_arr);

            return $this->fetch("manjian/lists");
        }
    }

    /**
     * 满减管理
     */
    public function manage()
    {
        $manjian_model = new ManjianModel();
        if(request()->isAjax()){
            $site_name = trim(input('site_name', ''));
            $goods_name = trim(input('goods_name', ''));
            $manjian_id = input('manjian_id', 0);
            $condition = [];
            $condition[] = ['manjian_id', '=', $manjian_id];
            if($site_name){
                $condition[] = ['site_name', '=', $site_name];
            }
            if($goods_name){
                $condition[] = ['goods_name', '=', $goods_name];
            }
            $field = '*';
            //获取状态名称
            $manjian_status_arr = $manjian_model->getManjianStatus();
            $res = $manjian_model->getManjianGoodsList($condition, $field);
            foreach($res['data'] as $key=>$val){
                $res['data'][$key]['status_name'] = $manjian_status_arr[$val['status']];
            }
            return $res;
        }else{
            $manjian_id = input('manjian_id', 0);
            $manjian_info = $manjian_model->getManjianInfo([['manjian_id', '=', $manjian_id]]);
            $this->assign('manjian_info',$manjian_info['data']);
            return $this->fetch('manjian/manage');
        }
    }

    /**
     * 添加商品
     */
    public function addGoods()
    {
        if(request()->isAjax()) {
            $goods_ids = input('goods_ids', 0);
            $site_id = $this->site_id;
            $manjian_id = input('manjian_id', 0);
            $manjian_model = new ManjianModel();
            return $manjian_model->addManjianGoods($manjian_id, $site_id, $goods_ids);
        }
    }

    /**
     * 删除商品
     */
    public function deleteGoods()
    {
        if(request()->isAjax()) {
            $goods_id = input('goods_ids', 0);
            $site_id = $this->site_id;
            $manjian_id = input('manjian_id', 0);
            $manjian_model = new ManjianModel();
            return $manjian_model->deleteManjianGoods($manjian_id, $site_id, $goods_id);
        }
    }

    public function againCheck()
    {
        if(request()->isAjax()) {
            $id = input('id', 0);
            $manjian_model = new ManjianModel();
            return $manjian_model->againCheckManjianGoods($id);
        }
    }


    /**
     * 商品选择组件
     * @return \multitype
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $goods_id = input('goods_id', 0);
            $is_virtual = input('is_virtual', '');// 是否虚拟类商品（0实物1.虚拟）
            $min_price = input('min_price', 0);
            $max_price = input('max_price', 0);
            $goods_class = input('goods_class', "");// 商品类型，实物、虚拟
            $category_id = input('category_id', "");// 商品分类id
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、fenxiao、bargain
            $promotion_type = input('promotion_type', "");
            $manjian_id = input('manjian_id', 0);//折扣id

            $condition = [
                ['is_delete', '=', 0],
                ['goods_state', '=', 1],
                ['site_id', '=', $this->site_id]
            ];


            if (!empty($goods_name)) {
                $condition[] = ['goods_name', 'like', '%' . $goods_name . '%'];
            }
            if ($is_virtual !== "") {
                $condition[] = ['is_virtual', '=', $is_virtual];
            }
            if (!empty($goods_id)) {
                $condition[] = ['goods_id', '=', $goods_id];
            }
            if (!empty($category_id)) {
                $condition[] = ['category_id', 'like', [$category_id, '%' . $category_id . ',%', '%' . $category_id, '%,' . $category_id . ',%'], 'or'];
            }

            if (!empty($promotion_type)) {
                $condition[] = ['promotion_addon', 'like', "%{$promotion_type}%"];
            }

            if ($goods_class !== "") {
                $condition[] = ['goods_class', '=', $goods_class];
            }

            if ($min_price != "" && $max_price != "") {
                $condition[] = ['price', 'between', [$min_price, $max_price]];
            } elseif ($min_price != "") {
                $condition[] = ['price', '<=', $min_price];
            } elseif ($max_price != "") {
                $condition[] = ['price', '>=', $max_price];
            }

            $order = 'create_time desc';
            $goods_model = new GoodsModel();
            $field = 'goods_id,goods_name,goods_class_name,goods_image,price,goods_stock,create_time,is_virtual';
            $goods_list = $goods_model->getGoodsPageList($condition, $page, $page_size, $order, $field);

            if (!empty($goods_list[ 'data' ][ 'list' ])) {
                foreach ($goods_list[ 'data' ][ 'list' ] as $k => $v) {
                    //sku数据
                    $goods_list[ 'data' ][ 'list' ][ $k ][ 'sku_list' ] = [];
                    //相同时间的营销活动
                    $goods_list[ 'data' ][ 'list' ][ $k ][ 'same_time_promotion'] = 0;
                }
            }

            return $goods_list;
        } else {

            //已经选择的商品sku数据
            $select_id = input('select_id', '');
            $mode = input('mode', 'spu');
            $max_num = input('max_num', 0);
            $min_num = input('min_num', 0);
            $is_virtual = input('is_virtual', '');
            $disabled = input('disabled', 0);
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao
            $manjian_id = input('manjian_id', 0);//第n件m折id

            $this->assign('select_id', $select_id);
            $this->assign('mode', $mode);
            $this->assign('max_num', $max_num);
            $this->assign('min_num', $min_num);
            $this->assign('is_virtual', $is_virtual);
            $this->assign('disabled', $disabled);
            $this->assign('promotion', $promotion);
            $this->assign('manjian_id', $manjian_id);

            // 营销活动
            $goods_promotion_type = event('GoodsPromotionType');
            $this->assign('promotion_type', $goods_promotion_type);

            //商品分类
            $shop_category_model = new ShopCategory();
            $category_info = $shop_category_model->getCategoryInfo([['category_id', '=', $this->shop_info['category_id']]], '*');
            $goods_category_ids = $category_info['data']['goods_category_ids'];
            $condition = [
                ['category_id', 'in', $goods_category_ids],
            ];
            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList($condition, 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);
            return $this->fetch("manjian/goods_select");
        }
    }
}