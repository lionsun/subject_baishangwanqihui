<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\newusercoupon\shop\controller;

use addon\newusercoupon\model\CouponActivity;
use app\shop\controller\BaseShop;
use addon\newusercoupon\model\CouponActivity as CouponActivityModel;
use addon\newusercoupon\model\Coupon as CouponModel;

/**
 * 优惠券
 * @author Administrator
 *
 */
class Coupon extends BaseShop
{
    /**
     * 添加活动
     */
    public function add()
    {

        if (request()->isAjax()) {
            $data = [
                'site_id' => $this->site_id,
                'site_name' => $this->shop_info['site_name'],
                'coupon_name' => input('coupon_name', ''),//优惠券名称
                'type' => input('type'),//优惠券类型
                'goods_type' => input('goods_type', 1),
                'goods_ids' => input('goods_ids', ''),
                'money' => input('money', 0),//优惠券面额
                'discount' => input('discount', 0),//优惠券折扣
                'discount_limit' => input('discount_limit', 0),//最多优惠
                'count' => input('count', ''),//发放数量
                'max_fetch' => input('max_fetch', ''),//最大领取数量
                'at_least' => input('at_least', ''),//满多少元可以使用
                'end_time' => strtotime(input('end_time', '')),//活动结束时间
                'image' => input('image', ''),//优惠券图片
                'validity_type' => input('validity_type', ''),//有效期类型 0固定时间 1领取之日起
                'fixed_term' => input('fixed_term', ''),//领取之日起N天内有效
                'is_show' => input('is_show', 0),//是否允许直接领取 1:是 0：否 允许直接领取，用户才可以在手机端和PC端进行领取，否则只能以活动的形式发放。
            ];

            $coupon_type_model = new CouponActivityModel();
            return $coupon_type_model->addActivity($data);
        } else {

            return $this->fetch("coupon/add");
        }
    }

    /**
     * 编辑活动
     */
    public function edit()
    {
        if (request()->isAjax()) {
            $data = [
                'activity_id' => input('activity_id', ''),
                'coupon_name' => input('coupon_name', ''),//优惠券名称
                'type' => input('type'),//优惠券类型
                'goods_type' => input('goods_type', 1),
                'goods_ids' => input('goods_ids', ''),
                'money' => input('money', 0),//优惠券面额
                'discount' => input('discount', 0),//优惠券折扣
                'discount_limit' => input('discount_limit', 0),//最多优惠
                'count' => input('count', ''),//发放数量
                'max_fetch' => input('max_fetch', ''),//最大领取数量
                'at_least' => input('at_least', ''),//满多少元可以使用
                'end_time' => strtotime(input('end_time', '')),//活动结束时间
                'image' => input('image', ''),//优惠券图片
                'validity_type' => input('validity_type', ''),//有效期类型 0固定时间 1领取之日起
                'fixed_term' => input('fixed_term', ''),//领取之日起N天内有效
                'is_show' => input('is_show', 0),//是否允许直接领取 1:是 0：否 允许直接领取，用户才可以在手机端和PC端进行领取，否则只能以活动的形式发放。
            ];

            $coupon_type_model = new CouponActivityModel();
            return $coupon_type_model->editActivity($data);
        } else {
            $coupon_type_id = input('coupon_type_id', 0);
            $this->assign('coupon_type_id', $coupon_type_id);

            $coupon_type_model = new CouponActivityModel();
            $coupon_type_info = $coupon_type_model->getActivityInfo($coupon_type_id, $this->site_id);

            $this->assign('coupon_type_info', $coupon_type_info['data']);

            return $this->fetch("coupon/edit");
        }
    }

    /**
     * 活动详情
     */
    public function detail()
    {
        $coupon_type_id = input('activity_id', 0);
        $coupon_type_model = new CouponActivityModel();
        $condition = ['activity_id' => $coupon_type_id];
        $coupon_type_info = $coupon_type_model->getActivityInfo($condition,$this->site_id)['data'];
        if(!empty($coupon_type_info)){
            $coupon_type_info['day_time']    = (int)($coupon_type_info['validity_period'] / 60 / (24 * 60));
            $coupon_type_info['hour_time']   = (int)(($coupon_type_info['validity_period'] / 60 % (24 * 60)) / 60);
            $coupon_type_info['second_time'] = (int)(($coupon_type_info['validity_period'] / 60 % (24 * 60)) % 60);
        }
        $this->assign('coupon_type_info', $coupon_type_info);

        return $this->fetch("coupon/detail");
    }

    /**
     * 活动列表
     */
    public function lists()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $coupon_name = input('coupon_name', '');
            $status = input('status', '');

            $condition = [];
            if ($status !== "") {
                $condition[] = ['status', '=', $status];
            }
            $coupon_type = input('coupon_type');
            if ($coupon_type) {
                $condition[] = ['coupon_type', '=', $coupon_type];
            }
            
            $condition[] = ['coupon_name', 'like', '%' . $coupon_name . '%'];
            $order = 'create_time desc';
            $field = '*';

            $coupon_type_model = new CouponActivityModel();
            $res = $coupon_type_model->getActivityPageList($condition, $page, $page_size, $order, $field);


            //获取优惠券状态
            $coupon_type_status_arr = $coupon_type_model->getCouponTypeStatus();
            foreach ($res['data']['list'] as $key => $val) {
                $res['data']['list'][$key]['status_name'] = $coupon_type_status_arr[$val['status']];
                $res['data']['list'][$key]['coupon_type_name'] = CouponActivityModel::getCouponTypeName($val['coupon_type']);
                $res['data']['list'][$key]['day_time'] = (int)($res['data']['list'][$key]['validity_period'] / (24 * 60 * 60));
                $res['data']['list'][$key]['hour_time'] = (int)(($res['data']['list'][$key]['validity_period'] / 60 % (24 * 60)) / 60);
                $res['data']['list'][$key]['second_time'] = (int)(($res['data']['list'][$key]['validity_period'] /60 % (24 * 60)) % 60);
            }
            return $res;

        } else {
            //优惠券状态
            $coupon_type_model = new CouponActivityModel();
            $coupon_type_status_arr = $coupon_type_model->getCouponTypeStatus();
            $this->assign('coupon_type_status_arr', $coupon_type_status_arr);

            return $this->fetch("coupon/lists");
        }
    }


    /**
     * 优惠券领取记录
     * */
    public function receive()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $activity_id = input('activity_id', 0);
            $status = input('status', '');
            $condition = [];
            if($status!=''){
                $condition[] = ['nuc.status', '=', $status];
            }
            $condition[] = ['nuc.activity_id', '=', $activity_id];
            $alias = 'nuc';
            $join = [
                [ 'member m', 'nuc.member_id = m.member_id', 'inner' ]
            ];
            $field = "nuc.*,m.member_id,m.nickname";
            $coupon_model = new CouponModel();
            $res = $coupon_model->getCouponPageList($condition, $page, $page_size,'fetch_time desc',$field, $alias, $join);
            return $res;
        } else {
            $activity_id = input('activity_id', 0);
            $this->assign('activity_id', $activity_id);
            return $this->fetch("coupon/receive");
        }
    }

    /**
     * 商品管理
     */
    public function manage()
    {
        $activity_model = new CouponActivityModel();
        if (request()->isAjax()) {
            //商品列表
            $activity_id = input('activity_id', 0);

            $res = $activity_model->getActivityGoodsList(['activity_id'=>$activity_id]);
            return $res;
        } else {
            $activity_id = input('activity_id', 0);
            $this->assign('activity_id', $activity_id);
            $coupon_type_status_arr = $activity_model->getCouponTypeStatus();
            //活动详情
            $activity_info = $activity_model->getActivityInfo(['activity_id'=>$activity_id], $this->site_id)['data'];
            if(!empty($activity_info)){
                $activity_info['status_name'] = $coupon_type_status_arr[$activity_info['status']];;
                $activity_info['day_time']    = (int)($activity_info['validity_period'] / (24 * 60 * 60));
                $activity_info['hour_time']   = (int)(($activity_info['validity_period'] / 60 % (24 * 60)) / 60);
                $activity_info['second_time'] = (int)(($activity_info['validity_period'] / 60 % (24 * 60)) % 60);
            }
            $this->assign('activity_info', $activity_info);

            return $this->fetch("coupon/manage");
        }
    }

    /**
     * 添加商品
     */
    public function addGoods(){
        if (request()->isAjax()) {
            $goods_ids = input('goods_ids', '');
            $activity_id = input('activity_id', 0);
            $discount_model = new CouponActivityModel();
            return $discount_model->addActivityGoods($activity_id, $this->site_id, $goods_ids);
        }
    }

    /**
     * 删除商品
     */
    public function deleteGoods()
    {
        if (request()->isAjax()) {
            $actiivity_id = input('activity_id', 0);
            $goods_id = input('goods_id', '');
            $discount_model = new CouponActivityModel();
            return $discount_model->deleteActivityGoods($actiivity_id, $goods_id);
        }
    }
}