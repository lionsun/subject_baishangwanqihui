<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newusercoupon\model;

use app\model\BaseModel;
use app\model\system\Cron;

/**
 * 活动活动
 */
class CouponActivity extends BaseModel
{
    const PROMOTION_TYPE = 'newusercoupon';
    const PROMOTION_TYPE_NAME = '新人专享优惠券';
    /**
     * 活动状态
     */
    const ACTIVITY_STATUS_NOT_START = 0;
    const ACTIVITY_STATUS_IN_PROCESS = 1;
    const ACTIVITY_STATUS_ENDED = 2;
    const ACTIVITY_STATUS_CLOSED = -1;
	
	public static function getActivityStatus()
	{
        $arr = [
            self::ACTIVITY_STATUS_NOT_START => '未开始',
            self::ACTIVITY_STATUS_IN_PROCESS => '进行中',
            self::ACTIVITY_STATUS_ENDED => '已结束',
            self::ACTIVITY_STATUS_CLOSED => '已关闭',
        ];
        return $arr;
	}

    public static function getCouponTypeStatus()
    {
        $arr = [
            self::ACTIVITY_STATUS_NOT_START => '未开始',
            self::ACTIVITY_STATUS_IN_PROCESS => '进行中',
            self::ACTIVITY_STATUS_ENDED => '已结束',
            self::ACTIVITY_STATUS_CLOSED => '已关闭',
        ];
        return $arr;
    }

    public static function getActivityStatusName($id)
    {
        $status = self::getActivityStatus();
        if(isset($status[$id])){
            $name = $status[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    /**
     * 优惠券类型
     */
    const COUPON_TYPE_MANJIAN = 1;
    const COUPON_TYPE_MANZHE = 2;
    const COUPON_TYPE_HONGBAO = 3;

    public static function getCouponType()
    {
        $arr = [
            self::ACTIVITY_STATUS_NOT_START => '未开始',
            self::ACTIVITY_STATUS_IN_PROCESS => '进行中',
            self::ACTIVITY_STATUS_ENDED => '已结束',
            self::ACTIVITY_STATUS_CLOSED => '已关闭',
        ];
        return $arr;
    }

    public static function getCouponTypeName($id)
    {
        $arr = self::getCouponType();
        if(isset($arr[$id])){
            $name = $arr[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    public static function getCouponName(){
        $arr = [
            self::COUPON_TYPE_MANJIAN => '满减券',
            self::COUPON_TYPE_MANZHE => '满折券',
            self::COUPON_TYPE_HONGBAO => '红包券',
        ];
        return $arr;
    }

    public static function getCouponNameArr($id){
        $arr = self::getCouponName();
        if(isset($arr[$id])){
            $name = $arr[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    /**
     * 添加优惠券活动
     * @param $param
     * @return array
     */
	public function addActivity($param)
	{
        try{
            if($param['end_time'] < time()) return $this->error('', '结束时间不能早于当前时间');
            if($param['start_time'] >= $param['end_time']) return $this->error('', '结束时间不能早于开始时间');

            $data = [
                'coupon_name' => $param['coupon_name'],
                'coupon_type' => $param['coupon_type'],
                'limit_money' => $param['limit_money'],
                'money' => $param['money'],
                'discount' => $param['discount'],
                'validity_period' => $param['validity_period'],
                'total_count' => $param['total_count'],
                'max_fetch' => $param['max_fetch'],
                'platform_rate' => $param['platform_rate'],
                'shop_rate' => $param['shop_rate'],
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
                'create_time' => time(),
                'image' => $param['image'],
                'status' => $param['start_time'] > time() ? self::ACTIVITY_STATUS_NOT_START : self::ACTIVITY_STATUS_IN_PROCESS,
            ];
            $activity_id = model('promotion_newuser_coupon_activity')->add($data);

            $cron_model = new Cron();
            if($data['status'] == self::ACTIVITY_STATUS_NOT_START){
                $cron_model->addCron(1, 0, "新人优惠券活动开启", "StartNewUserCouponActivity", $data[ 'end_time' ], $activity_id);
            }
            $cron_model->addCron(1, 0, "新人优惠券活动结束", "EndNewUserCouponActivity", $data[ 'end_time' ], $activity_id);

            return $this->success($activity_id);
        }catch(\Exception $e){
            return $this->error('', $e->getMessage());
        }
	}

    /**
     * 编辑活动
     * @param $param
     * @return array
     */
	public function editActivity($param)
	{
        try{
            $activity_id = $param['activity_id'];
            $activity_info = model('promotion_newuser_coupon_activity')->getInfo([['activity_id', '=', $activity_id]]);
            if(empty($activity_info)) return $this->error('', '活动信息有误');
            if($activity_info['status'] != self::ACTIVITY_STATUS_NOT_START) return $this->error('', '只有未开始的活动可以编辑');

            if($param['end_time'] < time()) return $this->error('', '结束时间不能早于当前时间');
            if($param['start_time'] >= $param['end_time']) return $this->error('', '结束时间不能早于开始时间');

            $data = [
                'coupon_name' => $param['coupon_name'],
                'coupon_type' => $param['coupon_type'],
                'limit_money' => $param['limit_money'],
                'money' => $param['money'],
                'discount' => $param['discount'],
                'validity_period' => $param['validity_period'],
                'total_count' => $param['total_count'],
                'max_fetch' => $param['max_fetch'],
                'platform_rate' => $param['platform_rate'],
                'shop_rate' => $param['shop_rate'],
                'start_time' => $param['start_time'],
                'end_time' => $param['end_time'],
                'modify_time' => time(),
                'image' => $param['image'],
                'status' => $param['start_time'] > time() ? self::ACTIVITY_STATUS_NOT_START : self::ACTIVITY_STATUS_IN_PROCESS,
            ];
            model('promotion_newuser_coupon_activity')->update($data, [['activity_id', '=', $activity_id]]);

            $cron_model = new Cron();
            $cron = new Cron();
            $cron->deleteCron([['event', '=', 'StartNewUserCouponActivity'], ['relate_id', '=', $activity_id]]);
            $cron->deleteCron([['event', '=', 'EndNewUserCouponActivity'], ['relate_id', '=', $activity_id]]);
            if($data['status'] == self::ACTIVITY_STATUS_NOT_START){
                $cron_model->addCron(1, 0, "新人优惠券活动开启", "StartNewUserCouponActivity", $data[ 'end_time' ], $activity_id);
            }
            $cron_model->addCron(1, 0, "新人优惠券活动结束", "EndNewUserCouponActivity", $data[ 'end_time' ], $activity_id);

            return $this->success($activity_id);
        }catch(\Exception $e){
            return $this->error('', $e->getMessage());
        }
	}

    /**
     * 关闭活动
     * @param $activity_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
	public function closeActivity($activity_id)
	{
        $activity_info = model('promotion_newuser_coupon_activity')->getInfo([['activity_id', '=', $activity_id]]);
        if(empty($activity_info)) return $this->error('', '活动信息有误');
        if($activity_info['status'] != self::ACTIVITY_STATUS_IN_PROCESS) return $this->error('', '只有进行中的活动可以关闭');

        model('promotion_newuser_coupon_activity')->update(['status' => self::ACTIVITY_STATUS_CLOSED], [['activity_id', '=', $activity_id]]);
        model('promotion_newuser_coupon')->update(['status' => Coupon::COUPON_STATUS_INVALID], [['activity_id', '=', $activity_id], ['status', '=', Coupon::COUPON_STATUS_NOT_USE]]);

        $cron = new Cron();
        $cron->deleteCron([['event', '=', 'EndNewUserCouponActivity'], ['relate_id', '=', $activity_id]]);

        return $this->success($activity_id);
    }

    /**
     * 删除活动
     * @param $activity_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
	public function deleteActivity($activity_id)
	{
        $activity_info = model('promotion_newuser_coupon_activity')->getInfo([['activity_id', '=', $activity_id]]);
        if(empty($activity_info)) return $this->error('', '活动信息有误');
        if($activity_info['status'] == self::ACTIVITY_STATUS_IN_PROCESS) return $this->error('', '进行中的活动不可以删除');

        model('promotion_newuser_coupon_activity')->delete([['activity_id', '=', $activity_id]]);
        model('promotion_newuser_coupon_activity_goods')->delete([['activity_id', '=', $activity_id]]);

        return $this->success($activity_id);
	}

    /**
     * 自动开始活动
     * @param $activity_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
	public function cronStartActivity($activity_id)
    {
        $activity_info = model('promotion_newuser_coupon_activity')->getInfo([['activity_id', '=', $activity_id]]);
        if(empty($activity_info)) return $this->error('', '活动信息有误');
        if($activity_info['status'] != self::ACTIVITY_STATUS_NOT_START) return $this->error('', '活动状态不正确');

        model('promotion_newuser_coupon_activity')->update(['status' => self::ACTIVITY_STATUS_NOT_START], [['activity_id', '=', $activity_id]]);

        return $this->success($activity_id);
    }

    /**
     * 自动结束活动
     * @param $activity_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function cronEndActivity($activity_id)
    {
        $activity_info = model('promotion_newuser_coupon_activity')->getInfo([['activity_id', '=', $activity_id]]);
        if(empty($activity_info)) return $this->error('', '活动信息有误');
        if($activity_info['status'] != self::ACTIVITY_STATUS_NOT_START) return $this->error('', '活动状态不正确');

        model('promotion_newuser_coupon_activity')->update(['status' => self::ACTIVITY_STATUS_NOT_START], [['activity_id', '=', $activity_id]]);
        model('promotion_newuser_coupon')->update(['status' => Coupon::COUPON_STATUS_EXPIRED], [['activity_id', '=', $activity_id], ['status', '=', Coupon::COUPON_STATUS_NOT_USE]]);

        return $this->success($activity_id);
    }

    /**
     * 添加活动商品
     * @param $activity_id
     * @param $site_id
     * @param $goods_ids
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addActivityGoods($activity_id, $site_id, $goods_ids)
    {
        $activity_info = model('promotion_newuser_coupon_activity')->getInfo([['activity_id', '=', $activity_id]]);
        if(empty($activity_info)) return $this->error('', '活动信息有误');
        if($activity_info['status'] != self::ACTIVITY_STATUS_IN_PROCESS) return $this->error('', '活动状态不正确');

        $exist_id_arr = model('promotion_newuser_coupon_activity_goods')->getColumn([['activity_id', '=', $site_id]], 'goods_id');
        $add_id_arr = explode(',', $goods_ids);
        $add_id_arr = array_flip($add_id_arr);
        foreach($exist_id_arr as $goods_id){
            if(isset($add_id_arr[$goods_id])){
                unset($add_id_arr[$goods_id]);
            }
        }
        $add_id_arr = array_flip($add_id_arr);

        $add_data = [];
        foreach($add_id_arr as $goods_id){
            $add_data[] = [
                'activity_id' => $activity_id,
                'site_id'     => $site_id,
                'goods_id'    => $goods_id,
                'start_time'  => $activity_info['start_time'],
                'end_time'    => $activity_info['end_time']
            ];
        }
        $upd_data = [
            'promotion_type'     => self::PROMOTION_TYPE,
            'new_user_coupon_id' => $activity_id,
            'start_time'         => $activity_info['start_time'],
            'end_time'           => $activity_info['end_time']
        ];
        model('goods')->update($upd_data,[['goods_id','in',$goods_ids]]);
        model('goods_sku')->update($upd_data,[['goods_id','in',$goods_ids]]);
        model('promotion_newuser_coupon_activity_goods')->addList($add_data);

        return $this->success();
    }

    /**
     * 删除活动商品
     * @param $activity_id
     * @param $site_id
     * @param $goods_id
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function deleteActivityGoods($activity_id, $goods_id )
    {
        model('promotion_newuser_coupon_activity_goods')->delete([
            ['activity_id', '=', $activity_id],
            ['goods_id', '=', $goods_id]
        ]);
        $upd_data = [
            'promotion_type'     => '',
            'new_user_coupon_id' => 0
        ];
        model('goods')->update($upd_data,[['goods_id','=',$goods_id]]);
        model('goods_sku')->update($upd_data,[['goods_id','=',$goods_id]]);
        return $this->success();
    }

    /**
     * 获取活动信息
     * @param $condition
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
	public function getActivityInfo($condition, $site_id = 0, $field = '*')
	{
		$res = model('promotion_newuser_coupon_activity')->getInfo($condition, $field);
		if($site_id){
            $condition['site_id'] = $site_id;
        }
        $activity_goods = model('promotion_newuser_coupon_activity_goods')->getList($condition,'goods_id');
        $goods_ids = '';
        if(!empty($activity_goods)){
            foreach ($activity_goods as $val){
                $goods_ids .= $val['goods_id'].',';
            }
            $goods_ids = substr($goods_ids, 0, -1);
        }
        $goods_field = 'goods_id,goods_name,goods_stock,price,goods_image,start_time,end_time';
        $goods_list = model('goods')->getList([['goods_id', 'in', $goods_ids]], $goods_field);
        $res['goods_list'] = $goods_list;
		return $this->success($res);
	}

	/**
	 * 获取活动列表
	 * @param array $condition
	 * @param string $field
	 * @param string $order
	 * @param string $limit
	 */
	public function getActivityList($condition = [], $field = '*', $order = '', $limit = null)
	{
		$res = model('promotion_newuser_coupon_activity')->getList($condition, $field, $order, '', '', '', $limit);
		return $this->success($res);
	}
	
	/**
	 * 获取活动分页列表
	 * @param array $condition
	 * @param number $page
	 * @param string $page_size
	 * @param string $order
	 * @param string $field
	 */
	public function getActivityPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*')
	{
		$list = model('promotion_newuser_coupon_activity')->pageList($condition, $field, $order, $page, $page_size);
		return $this->success($list);
	}

    /**
     * 获取活动商品信息
     * @param $condition
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
	public function getActivityGoodsInfo($condition, $field = '*')
    {
        $info = model('promotion_newuser_coupon_activity_goods')->getInfo($condition, $field);
        return $this->success($info);
    }

    /**
     * 获取活动商品列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param null $limit
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getActivityGoodsList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $goods_list = model('promotion_newuser_coupon_activity_goods')->getList($condition, $field, $order, '', '', '', $limit);
        $goods_ids = '';
        if(!empty($goods_list)){
            foreach ($goods_list as $val){
                $goods_ids .= $val['goods_id'].',';
            }
            $goods_ids = substr($goods_ids, 0, -1);
        }
        $goods_field = 'goods_id,goods_name,goods_stock,price,goods_image,start_time,end_time';
        $list = model('goods')->getList([['goods_id', 'in', $goods_ids]], $goods_field);
        return $this->success($list);
    }

    /**
     * 获取活动商品分页列表
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getActivityGoodsPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*', $alias = '', $join = [])
    {
        $list = model('promotion_newuser_coupon_activity_goods')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return $this->success($list);
    }


    public function getActivityGoodsskuPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = '*', $alias = '', $join = [])
    {
        $list = model('promotion_newuser_coupon_activity')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return $this->success($list);
    }

    public function modifyPage($activity_id,$page){
        $result = model('promotion_newuser_coupon_activity')->update(['page'=>$page],[['activity_id','=',$activity_id]]);
        return $this->success($result);
    }
}