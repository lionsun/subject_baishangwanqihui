<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newusercoupon\model;

use app\model\BaseModel;
use app\model\system\Cron;
use think\facade\Db;
use addon\newusercoupon\model\CouponActivity;

/**
 * 优惠券管理
 */
class Coupon extends BaseModel
{
    /**
     * 优惠券状态
     */
    const COUPON_STATUS_NOT_USE = 0;
    const COUPON_STATUS_USED = 1;
    const COUPON_STATUS_EXPIRED = 2;
    const COUPON_STATUS_INVALID = -1;
	
	public static function getCouponStatus()
	{
        $arr = [
            self::COUPON_STATUS_NOT_USE => '未使用',
            self::COUPON_STATUS_USED => '已使用',
            self::COUPON_STATUS_EXPIRED => '已过期',
            self::COUPON_STATUS_INVALID => '已失效',
        ];
        return $arr;
	}

    public static function getCouponStatusName($id)
    {
        $status = self::getCouponStatus();
        if(isset($status[$id])){
            $name = $status[$id];
        }else{
            $name = '';
        }
        return $name;
    }

    /**
     * 优惠券领取
     * @param $member_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function couponFetch($member_id)
    {
        $count = model('promotion_newuser_coupon')->getCount([['member_id', '=', $member_id]]);
        if($count > 0){
            return $this->error('', '已领取新人优惠券');
        }
        $activity_list = model('promotion_newuser_coupon_activity')->getList([
            ['status', '=', CouponActivity::ACTIVITY_STATUS_IN_PROCESS],
            [Db::raw('total_count - fetch_count'), '>', 0],
        ]);
        if(empty($activity_list)){
            return $this->error('', '暂无优惠券活动');
        }
        $cron_model = new Cron();
        $curr_time = time();
        $add_data = [];
        foreach($activity_list as $activity){
            $max_fetch = $activity['max_fetch'];
            for($i=0;$i<$max_fetch;$i++){
                $data = [
                    'activity_id'   => $activity['activity_id'],
                    'coupon_type'   => $activity['coupon_type'],
                    'coupon_name'   => $activity['coupon_name'],
                    'limit_money'   => $activity['limit_money'],
                    'money'         => $activity['money'],
                    'discount'      => $activity['discount'],
                    'platform_rate' => $activity['platform_rate'],
                    'shop_rate'     => $activity['shop_rate'],
                    'member_id'     => $member_id,
                    'fetch_time'    => $curr_time,
                    'status'        => self::COUPON_STATUS_NOT_USE,
                ];
                $is_add_cron = 0;
                if($curr_time + $activity['validity_period'] > $activity['end_time']){
                    $expire_time = $activity['end_time'];
                }else{
                    $expire_time = $curr_time + $activity['validity_period'];
                    $is_add_cron = 1;
                }
                $data['expire_time'] = $expire_time;
                $coupon_id = model('promotion_newuser_coupon')->add($data);
                model('promotion_newuser_coupon_activity')->setInc(['activity_id'=> $activity['activity_id']],'fetch_count');
                //如果是活动到期失效的类型，是在活动结束的时候批量执行
                if($is_add_cron == 1){
                    $cron_model->addCron(1, 0, "到期新人优惠券", "ExpiredNewUserCoupon", $data[ 'expire_time' ], $coupon_id);
                }
                $add_data[] = $data;
            }
        }
        return $add_data;
    }

    /**
     * 自动到期优惠券
     * @param $coupon_id
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function cronExpiredCoupon($coupon_id)
    {
        model('promotion_newuser_coupon')->update(['status' => self::COUPON_STATUS_EXPIRED], [['coupon_id', '=', $coupon_id]]);
        return $this->success();
    }

    /**
     * 定时关闭
     * @return mixed
     */
    public function cronCouponEnd()
    {
        $res = model("promotion_newuser_coupon")->update([ 'status' => 3 ], [ [ 'status', '=', 1 ], [ 'expire_time', '<=', time() ] ]);
        return $res;
    }

    /**
     * 获取优惠券信息
     * @param $condition
     * @param string $field
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function getCouponInfo($condition, $field = '*')
    {
        $res = model('promotion_newuser_coupon')->getInfo($condition, $field);
        return $this->success($res);
    }

    /**
     * 获取优惠券列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param null $limit
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getCouponList($condition = [], $field = '*', $order = '', $limit = null)
    {
        $res = model('promotion_newuser_coupon')->getList($condition, $field, $order, '', '', '', $limit);
        return $this->success($res);
    }

    /**
     * 获取优惠券分页列表
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getCouponPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'fetch_time desc', $field = '*',$alias, $join)
    {
        $list = model('promotion_newuser_coupon')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
        return $this->success($list);
    }

    /**
     * 获取会员已领取优惠券优惠券数量
     * @param unknown $member_id
     * @param unknown $state
     * @return multitype:number unknown
     */
    public function getNewUserCouponNum($member_id, $state)
    {
        $condition = array(
            [ "member_id", "=", $member_id ],
//            [ "state", "=", $state ],
        );

        $num = model("promotion_newuser_coupon")->getCount($condition);
        return $this->success($num);
    }

    /**
     * 使用优惠券
     * @param $data
     */
    public function useUserNewCoupon($new_user_coupon_id, $member_id, $use_order_id)
    {
        $data = array( 'order_ids' => $use_order_id, 'use_time' => time(), 'status' => 1 );
        $condition = array(
            [ 'coupon_id', '=', $new_user_coupon_id ],
            [ 'member_id', '=', $member_id ],
            [ 'status', '=', self::COUPON_STATUS_NOT_USE ]
        );
        $result = model("promotion_newuser_coupon")->update($data, $condition);
        return $this->success($result);
    }
}