<?php

/**
 * Goodssku.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace addon\newusercoupon\api\controller;

use addon\coupon\model\CouponType;
use addon\platformcoupon\model\PlatformcouponType;
use app\model\goods\Goods;
use app\model\goods\GoodsAttribute;
use app\model\member\Member as MemberModel;
use app\api\controller\BaseApi;
use addon\newusercoupon\model\CouponActivity;

/**
 * 商品sku
 * @author Administrator
 *
 */
class Goodssku extends BaseApi
{

	/**
	 * 列表信息
	 */
	public function page()
	{
		$page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
		$page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
		$site_id = isset($this->params[ 'site_id' ]) ? $this->params[ 'site_id' ] : 0; //站点id
		$goods_id_arr = isset($this->params[ 'goods_id_arr' ]) ? $this->params[ 'goods_id_arr' ] : ''; //sku_id数组
		$keyword = isset($this->params[ 'keyword' ]) ? $this->params[ 'keyword' ] : ''; //关键词
		$category_id = isset($this->params[ 'category_id' ]) ? $this->params[ 'category_id' ] : 0; //分类
		$brand_id = isset($this->params[ 'brand_id' ]) ? $this->params[ 'brand_id' ] : 0; //品牌
		$min_price = isset($this->params[ 'min_price' ]) ? $this->params[ 'min_price' ] : 0; //价格区间，小
		$max_price = isset($this->params[ 'max_price' ]) ? $this->params[ 'max_price' ] : 0; //价格区间，大
		$is_free_shipping = isset($this->params[ 'is_free_shipping' ]) ? $this->params[ 'is_free_shipping' ] : -1; //是否免邮
		$is_own = isset($this->params[ 'is_own' ]) ? $this->params[ 'is_own' ] : ''; //是否自营
		$order = isset($this->params[ 'order' ]) ? $this->params[ 'order' ] : "create_time"; //排序（综合、销量、价格）
		$sort = isset($this->params[ 'sort' ]) ? $this->params[ 'sort' ] : "desc"; //升序、降序
		$attr = isset($this->params[ 'attr' ]) ? $this->params[ 'attr' ] : ""; //属性json
		$shop_category_id = isset($this->params[ 'shop_category_id' ]) ? $this->params[ 'shop_category_id' ] : 0;//店内分类
        $activity_id = isset($this->params[ 'activity_id' ]) ? $this->params[ 'activity_id' ] : 0; //分类
		$condition = [];

		$field = 'a.*,gs.goods_id,gs.sku_id,gs.sku_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.goods_name,gs.site_id,gs.website_id,gs.is_own,gs.is_free_shipping,gs.introduction,gs.promotion_type,g.goods_image,g.site_name,gs.goods_spec_format,gs.is_virtual,gs.ladder_discount_id,gs.source,gs.nthmfold_discount_id,gs.subscription_open_state,gs.corporate_subscription,gs.subscription_status,gs.nthmfold_discount_id,gs.manjian_discount_id,gs.new_user_coupon_id';

		$alias = 'a';
		$join = [
            [ 'goods_sku gs', 'gs.new_user_coupon_id = a.activity_id', 'inner' ],
			[ 'goods g', 'gs.sku_id = g.sku_id', 'inner' ],
		];

        //只查看处于开启状态的店铺
        $join[] = [ 'shop s', 's.site_id = gs.site_id', 'inner'];
        $condition[] = ['s.shop_status', '=', 1];

		if (!empty($site_id)) {
			$condition[] = [ 'gs.site_id', '=', $site_id ];
		}

		if (!empty($goods_id_arr)) {
			$condition[] = [ 'gs.goods_id', 'in', $goods_id_arr ];
		}

		if (!empty($keyword)) {
			$condition[] = [ 'g.goods_name|gs.sku_name|gs.keywords', 'like', '%' . $keyword . '%' ];
		}

		if (!empty($category_id)) {
			$condition[] = [ 'gs.category_id|gs.category_id_1|gs.category_id_2|gs.category_id_3', '=', $category_id ];
		}

		if (!empty($brand_id)) {
			$condition[] = [ 'gs.brand_id', '=', $brand_id ];
		}

		if(!empty($activity_id)){
            $condition[] = [ 'gs.new_user_coupon_id', '=', $activity_id ];
        }

		if ($min_price != "" && $max_price != "") {
			$condition[] = [ 'gs.discount_price', 'between', [ $min_price, $max_price ] ];
		} elseif ($min_price != "") {
			$condition[] = [ 'gs.discount_price', '>=', $min_price ];
		} elseif ($max_price != "") {
			$condition[] = [ 'gs.discount_price', '<=', $max_price ];
		}

		if (isset($is_free_shipping) && !empty($is_free_shipping) && $is_free_shipping > -1) {
			$condition[] = [ 'gs.is_free_shipping', '=', $is_free_shipping ];
		}

		if ($is_own !== '') {
			$condition[] = [ 'gs.is_own', '=', $is_own ];
		}

		// 非法参数进行过滤
		if ($sort != "desc" && $sort != "asc") {
			$sort = "";
		}

		// 非法参数进行过滤
		if ($order != '') {
			if ($order != "sale_num" && $order != "discount_price") {
				$order = 'gs.create_time';
			} else {
				$order = 'gs.' . $order;
			}
			$order_by = $order . ' ' . $sort;
		} else {
			$order_by = 'gs.sort desc,gs.create_time desc';
		}

		//拿到商品属性，查询sku_id
		if (!empty($attr)) {
			$attr = json_decode($attr, true);
			$attr_id = [];
			$attr_value_id = [];
			foreach ($attr as $k => $v) {
				$attr_id[] = $v[ 'attr_id' ];
				$attr_value_id[] = $v[ 'attr_value_id' ];
			}
			$goods_attribute = new GoodsAttribute();
			$attribute_condition = [
				[ 'attr_id', 'in', implode(",", $attr_id) ],
				[ 'attr_value_id', 'in', implode(",", $attr_value_id) ],
                [ 'app_module', '=', 'shop']
			];
			$attribute_list = $goods_attribute->getAttributeIndexList($attribute_condition, 'sku_id');
			$attribute_list = $attribute_list[ 'data' ];
			if (!empty($attribute_list)) {
				$sku_id = [];
				foreach ($attribute_list as $k => $v) {
					$sku_id[] = $v[ 'sku_id' ];
				}
				$condition[] = [
					[ 'gs.sku_id', 'in', implode(",", $sku_id) ]
				];
			}
		}

		$condition[] = [ 'gs.goods_state', '=', 1 ];
		$condition[] = [ 'gs.verify_state', '=', 1 ];
		$condition[] = [ 'gs.is_delete', '=', 0 ];


		// 优惠券
		$coupon = isset($this->params[ 'coupon_type' ]) ? $this->params[ 'coupon_type' ] : 0; //优惠券
		if ($coupon > 0) {
			$coupon_type = new CouponType();
			$coupon_type_info = $coupon_type->getInfo([
				[ 'coupon_type_id', '=', $coupon ],
				[ 'site_id', '=', $site_id ],
				[ 'goods_type', '=', 2 ]
			], 'goods_ids');
			$coupon_type_info = $coupon_type_info[ 'data' ];
			if (isset($coupon_type_info[ 'goods_ids' ]) && !empty($coupon_type_info[ 'goods_ids' ])) {
				$condition[] = [ 'g.goods_id', 'in', explode(',', trim($coupon_type_info[ 'goods_ids' ], ',')) ];
			}
		}
		//平台优惠券
		$platform_coupon = isset($this->params[ 'platform_coupon_type' ]) ? $this->params[ 'platform_coupon_type' ] : 0; //平台优惠券
		if ($platform_coupon > 0) {
			$platform_coupon_type = new PlatformcouponType();
			$platform_coupon_type_info = $platform_coupon_type->getInfo([
				[ 'platformcoupon_type_id', '=', $platform_coupon ],
				[ 'use_scenario', '=', 2 ]
			], 'group_ids');
			$platform_coupon_type_info = $platform_coupon_type_info[ 'data' ];
			if (!empty($platform_coupon_type_info)) {
				$condition[] = [ 's.group_id', 'in', explode(',', trim($platform_coupon_type_info[ 'group_ids' ], ',')) ];
			}
		}
		//店内分类
		if ($shop_category_id > 0) {
			$condition[] = [ 'gs.goods_shop_category_ids', 'like', [ $shop_category_id, '%' . $shop_category_id . ',%', '%' . $shop_category_id, '%,' . $shop_category_id . ',%' ], 'or' ];
		}

		$goods = new Goods();
		$couponActivity = new CouponActivity();
		$list = $couponActivity->getActivityGoodsskuPageList($condition, $page, $page_size, $order_by, $field, $alias, $join);
        foreach($list['data']['list'] as $key=>$val){
            $list['data']['list'][$key] = $goods->getGoodsPromotionInfo($val);
            $token = $this->checkToken();
            if($token['code'] >= 0){
                $field = 'member_id,member_type,member_level,member_level_name';
                //登录状态显示登录后的价格
                $member_model = new MemberModel();
                $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$field)['data'];
                if ($memberInfo['member_type']==0){//个人用户
                    $member_level = $memberInfo['member_level'];

                }
            }
        }

		return $this->response($list);
	}
}
