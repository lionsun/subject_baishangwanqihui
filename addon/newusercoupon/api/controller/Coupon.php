<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\newusercoupon\api\controller;

use app\api\controller\BaseApi;
use addon\newusercoupon\model\Coupon as CouponModel;

/**
 * 新人专享优惠券
 */
class Coupon extends BaseApi
{
    //新人专享优惠券列表
    public function page(){
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);
        $status       = isset($this->params[ 'status' ]) ? $this->params[ 'status' ] : '';
        $page         = isset($this->params[ 'page' ])   ? $this->params[ 'page' ] : 1;
        $page_size    = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
        $condition[]  = ['c.member_id','=',$this->member_id];
        if($status!=''){
            $condition[]  = ['c.status','=',$status];
        }
        $alias = 'c';
        $join = [
            ['promotion_newuser_coupon_activity a','a.activity_id = c.activity_id','inner']
        ];
        $field = 'c.*,a.activity_id,a.page';
        $coupon_model = new CouponModel();
        $list         = $coupon_model->getCouponPageList($condition,$page,$page_size,'',$field,$alias,$join);
        return $this->response($list);
    }

    //新人专享优惠券信息
    public function info(){
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);
        $coupon_id    = isset($this->params[ 'coupon_id' ]) ? $this->params[ 'coupon_id' ] : 0;
        $coupon_model = new CouponModel();
        $info         = $coupon_model->getCouponInfo([['coupon_id','=',$coupon_id]]);
        return $this->response($info);
    }

}