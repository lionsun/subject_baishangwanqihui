<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'NEW_USER_COUPON',
        'title' => '新人专享',
        'url' => 'newusercoupon://shop/coupon/lists',
        'parent' => 'PROMOTION_CENTER',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'NEW_USER_COUPON_DETAIL',
                'title' => '优惠券详情',
                'url' => 'newusercoupon://shop/coupon/detail',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_MANAGE',
                'title' => '商品管理',
                'url' => 'newusercoupon://shop/coupon/manage',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_SELECT',
                'title' => '商品选择',
                'url' => 'newusercoupon://shop/coupon/selectgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_ADD',
                'title' => '商品添加',
                'url' => 'newusercoupon://shop/coupon/addgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_DELETE',
                'title' => '商品删除',
                'url' => 'newusercoupon://shop/coupon/deletegoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_RECEIVE',
                'title' => '领取记录',
                'url' => 'newusercoupon://shop/coupon/receive',
                'sort'    => 1,
                'is_show' => 0
            ],
        ]
    ],
];
