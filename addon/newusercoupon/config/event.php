<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //展示活动
        'ShowPromotion' => [
            'addon\newusercoupon\event\ShowPromotion',
        ],
        //检测相同时间段营销活动
        'CheckSameTimePromotion' => [
            'addon\newusercoupon\event\CheckSameTimePromotion',
        ],
        //活动详情
        'PromotionInfo' => [
            'addon\newusercoupon\event\PromotionInfo',
        ],
        'PromotionPrice' => [
            'addon\newusercoupon\event\PromotionPrice',
        ],
        //新人优惠券活动开启
        'OpenPlatformManjian'  => [
            'addon\newusercoupon\event\OpenActivity',
        ],
        //新人优惠券活动关闭
        'ClosePlatformManjian'  => [
            'addon\newusercoupon\event\EndNewUserCouponActivity',
        ],
        //新人优惠券自动过期开启
        'ExpiredNewUserCoupon'  => [
            'addon\newusercoupon\event\ExpiredNewUserCoupon',
        ],
        //活动页面
        'PcNavLink' => [
            'addon\newusercoupon\event\PcNavLink',
        ],
    ],

    'subscribe' => [
    ],
];
