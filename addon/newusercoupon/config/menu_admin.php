<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'NEW_USER_COUPON',
        'title' => '新人专享',
        'url' => 'newusercoupon://admin/coupon/lists',
        'parent' => 'PROMOTION_PLATFORM',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'NEW_USER_COUPON_DETAIL',
                'title' => '优惠券详情',
                'url' => 'newusercoupon://admin/coupon/detail',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_ADD',
                'title' => '添加优惠券',
                'url' => 'newusercoupon://admin/coupon/add',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_EDIT',
                'title' => '编辑优惠券',
                'url' => 'newusercoupon://admin/coupon/edit',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_CLOSE',
                'title' => '关闭优惠券',
                'url' => 'newusercoupon://admin/coupon/close',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_DELETE',
                'title' => '删除优惠券',
                'url' => 'newusercoupon://admin/coupon/delete',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_MANAGE',
                'title' => '商品管理',
                'url' => 'newusercoupon://admin/coupon/manage',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_DELETE',
                'title' => '商品删除',
                'url' => 'newusercoupon://admin/coupon/deletegoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_CHECK',
                'title' => '商品审核',
                'url' => 'newusercoupon://admin/coupon/checkgoods',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'NEW_USER_COUPON_GOODS_RECEIVE',
                'title' => '领取记录',
                'url' => 'newusercoupon://admin/coupon/receive',
                'sort'    => 1,
                'is_show' => 0
            ],
        ]
    ],
];
