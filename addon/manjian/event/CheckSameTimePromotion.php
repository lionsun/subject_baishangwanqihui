<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\manjian\event;
use addon\manjian\model\Manjian as ManjianModel;

/**
 * 检测相同时间段营销活动
 */
class CheckSameTimePromotion
{

	public function handle($param)
	{
        $mode = $param['mode'];
        $id = $param['id'];
        $time_info = $param['time_info'];
        if($mode == 'spu'){
            $model = new ManjianModel();
            $manjian_list = $model->getManjianGoodsList([['goods_id', '=', $id]]);
            //是否存在相同时间折扣
            $exist_same_time_promotion = 0;
            $show_info = [];
            foreach($manjian_list['data'] as $val){
                if($time_info['start_time'] <= $val['start_time'] && $time_info['end_time'] >= $val['end_time']){
                    $exist_same_time_promotion = 1;
                }
                if($time_info['start_time'] >= $val['start_time'] && $time_info['end_time'] <= $val['end_time']){
                    $exist_same_time_promotion = 1;
                }
                if($time_info['start_time'] <= $val['start_time'] && $time_info['end_time'] >= $val['start_time']){
                    $exist_same_time_promotion = 1;
                }
                if($time_info['start_time'] <= $val['end_time'] && $time_info['end_time'] >= $val['end_time']){
                    $exist_same_time_promotion = 1;
                }
                $manjian_info = $model->getManjianInfo([['manjian_id', '=', $val['manjian_id']]], 'manjian_name')['data'];
                $show_info[] = [
                    'name' => $manjian_info['manjian_name'],
                    'start_time' => $val['start_time'],
                    'end_time' => $val['end_time'],
                ];
            }
            if($exist_same_time_promotion){
                $res = [
                    'promotion_list' => $show_info,
                    'type' => ManjianModel::PROMOTION_TYPE,
                    'type_name' => ManjianModel::PROMOTION_TYPE_NAME,
                ];
                return $res;
            }
        }
	}
}