<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'VIRTUAL_SALES',
        'title' => '操作',
        'url' => 'virtualsales://admin/virtualsales/lists',
        'parent' => 'TOOL_ROOT',
        'is_show' => 1,
        'picture' => 'addon/virtualsales/icon.png',
        'sort' => 200,
        /*
        'child_list' => [
            [
                'name' => 'PROMOTION_VIRTUALSALES_DETAIL',
                'title' => '虚拟销量',
                'url' => 'virtualsales://admin/virtualsales/detail',
                'picture' => 'addon/virtualsales/icon.png',
                'sort'    => 1,
                'is_show' => 1
            ],

        ]
        */
    ],
];
