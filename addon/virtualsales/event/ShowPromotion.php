<?php

namespace addon\virtualsales\event;

/**
 * 活动展示
 */
class ShowPromotion
{

    /**
     * 活动展示
     * 
     * @return multitype:number unknown
     */
	public function handle()
	{
        $data = [
            'admin' => [
                [
                    //插件名称
                    'name' => 'virtualsales',
                    //店铺端展示分类  shop:营销活动   member:互动营销
                    'show_type' => 'tool',
                    //展示主题
                    'title' => '虚拟销售',
                    //展示介绍
                    'description' => '虚拟销售',
                    //展示图标
                    'icon' => 'addon/virtualsales/icon.png',
                    //跳转链接
                    'url' => 'virtualsales://admin/virtualsales/lists',
                ]
            ]
        ];
	    return $data;
	}
}