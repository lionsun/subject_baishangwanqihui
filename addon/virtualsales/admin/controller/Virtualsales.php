<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\virtualsales\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\platformcoupon\model\PlatformcouponType as PlatformcouponTypeModel;
use addon\platformcoupon\model\Platformcoupon as PlatformcouponModel;
use app\model\shop\ShopGroup as ShopGroupModel;

/**
 * 优惠券
 * @author Administrator
 *
 */
class Virtualsales extends BaseAdmin
{
    public function lists()
    {
        return $this->fetch("virtualsales/lists");
    }

    public function detail()
    {
        return $this->fetch("virtualsales/detail");
    }

}