<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\houniao\api\controller;

use app\api\controller\BaseApi;
use addon\houniao\model\Goods as GoodsModel;


class Sync extends BaseApi
{
    /**
     * 同步品牌
     * @return false|string
     */
	public function syncHouniaoBrand()
	{
        set_time_limit(0);
	    $model = new GoodsModel();
        $return = $model->syncHouniaoBrand();
        return $this->response($return);
	}

    /**
     * 同步地区
     * @return false|string
     */
    public function syncHouniaoAddress()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncHouniaoAddress();
        return $this->response($return);
    }



    
    /**
     * 同步快递
     * @return false|string
     */
    public function syncHouniaoExpress()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncHouniaoExpress();
        return $this->response($return);
    }

    /**
     * 同步分类
     * @return false|string
     */
    public function syncHouniaoCategory()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncHouniaoCategory();
        return $this->response($return);
    }

    /**
     * 同步商品
     * @return false|string
     */
    public function syncHouniaoGoods()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncHouniaoGoods();
        return $this->response($return);
    }

    /**
     * 同步失败的商品
     * @return false|string
     */
    public function fixFail()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncfixFail();
        return $this->response($return);
    }

    /*========================================================================================*/

    /**
     * 补全商品的品牌
     * @return false|string
     */
    public function goodsBrand()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->goodsBrand();
        return $this->response($return);
    }

    /**
     * 补全商品中的分类
     */
    public function goodsAddCategory()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->goodsAddCategory();
        return $this->response($return);
    }


    public function addGoodsBug(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->addGoodsBug();
        return $this->response($return);
    }

    //所有商品的库存更新一边
    public function allStock(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->allStock();
        return $this->response($return);
    }

    public function zeroSku(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->zeroSku();
        return $this->response($return);
    }






    /****************************************text************************************************************/


    /**
     * 同步商品2   按照商品分类独立添加 更新数据
     * @return false|string
     */
    public function syncHouniaoGoods2()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncHouniaoGoods2();
        return $this->response($return);
    }






    /**
     * 同步分类
     * @return false|string
     */
    public function syncHouniaoCategory2()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncHouniaoCategory2();
        return $this->response($return);
    }




     //执行查重
     public function modifyIsDelete()
     {
         $model = new GoodsModel();
         $res = $model->modifyIsDelete();
         dump($res);die;
     }
     
     
     //查看商品详情
    public function getGoodsDetailBySku()
    {
        $model = new GoodsModel();
        $res = $model->getGoodsDetailBySku();
        dump($res);die;
    }





}