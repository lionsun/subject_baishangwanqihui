<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\houniao\api\controller;

use addon\houniao\model\Goods;
use app\api\controller\BaseApi;
use app\model\order\Order as OrderModel;
use think\facade\Log;

/**
 * 提供候鸟回调接口
 */
class Notify extends BaseApi
{
    /**
     *  商品新增 ---新增通知商品，商品不一定是上架状态
     */
    public function addGoods()
    {



        $notify_param = request()->post();
        $params = array_keys($notify_param);


        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="商品新增";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../addGoodsLOG.txt');


        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (isset($goods_data['data']['goodsSku']) && !empty($goods_data['data']['goodsSku'])) {
                    $model = new Goods();
                    $res = $model->addGoods($goods_data['data']['goodsSku']);
                    if ($res['code'] < 0) {
                        $info = model('fix_fail')->getInfo(['goods_sku' => $goods_data['data']['goodsSku']]);
                        if (empty($info)) {
                            model('fix_fail')->add(['goods_sku' => $goods_data['data']['goodsSku']]);
                        }

                    }

                }
            }
        }
        return 'success';
    }

    /**
     *  商品更新 ---更新通知是更新基本信息
     */
    public function modifyGoods()
    {
        $notify_param = request()->post();
        $params = array_keys($notify_param);


        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="商品更新";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../modifyGoodsLOG.txt');


        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (isset($goods_data['data']['goodsSku']) && !empty($goods_data['data']['goodsSku'])) {
                    $model = new Goods();
                    $res = $model->editGoods($goods_data['data']['goodsSku']);
                    if ($res['code'] < 0) {
                        $info = model('fix_fail')->getInfo(['goods_sku' => $goods_data['data']['goodsSku']]);
                        if (empty($info)) {
                            model('fix_fail')->add(['goods_sku' => $goods_data['data']['goodsSku']]);
                        }
                    }
                }
            }
        }
        return 'success';
    }

    /**
     *  商品上架 --- 上架通知，务必更新基本信息和价格库存
     */
    public function onGoods()
    {
        $notify_param = request()->post();
        $params = array_keys($notify_param);



        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="商品上架";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../onGoodsLOG.txt');



        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (isset($goods_data['data']['goodsSku']) && !empty($goods_data['data']['goodsSku'])) {
                    $model = new Goods();
                    $res = $model->onGoods($goods_data['data']['goodsSku']);
                    if ($res['code'] < 0) {
                        $info = model('fix_fail')->getInfo(['goods_sku' => $goods_data['data']['goodsSku']]);
                        if (empty($info)) {
                            model('fix_fail')->add(['goods_sku' => $goods_data['data']['goodsSku']]);
                        }
                    }
                }
            }
        }
        return 'success';
    }

    /**
     *  商品下架 --- 下架通知是下架整个商品
     */
    public function OffGoods()
    {


        $notify_param = request()->post();
        $params = array_keys($notify_param);

        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="商品下架";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../OffGoodsLOG.txt');






        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (isset($goods_data['data']['goodsSku']) && !empty($goods_data['data']['goodsSku'])) {
                    $model = new Goods();
                    $res = $model->offGoods($goods_data['data']['goodsSku']);
                    if ($res['code'] < 0) {
                        $info = model('fix_fail')->getInfo(['goods_sku' => $goods_data['data']['goodsSku']]);
                        if (empty($info)) {
                            model('fix_fail')->add(['goods_sku' => $goods_data['data']['goodsSku']]);
                        }
                    }
                }
            }
        }
        return 'success';
    }

    /**
     *  调整价格 --- 价格通知会有规格新增或删除，也就是下架规格
     */
    public function adjustPrice()
    {
        $notify_param = request()->post();
        $params = array_keys($notify_param);


        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="调整价格";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../adjustPriceLOG.txt');


        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (isset($goods_data['data']['goodsSku']) && !empty($goods_data['data']['goodsSku'])) {
                    $model = new Goods();
                    $res = $model->adjustPrice($goods_data['data']['goodsSku']);
                    if ($res['code'] < 0) {
                        $info = model('fix_fail')->getInfo(['goods_sku' => $goods_data['data']['goodsSku']]);
                        if (empty($info)) {
                            model('fix_fail')->add(['goods_sku' => $goods_data['data']['goodsSku']]);
                        }
                    }
                }
            }
        }
        return 'success';
    }

    /**
     *  调整库存 --- 库存通知只会在库存少于50的情况下通知
     */
    public function adjustStock()
    {
        $notify_param = request()->post();
        $params = array_keys($notify_param);



        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="调整库存";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../adjustStockLOG.txt');



        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (isset($goods_data['data']['goodsSku']) && !empty($goods_data['data']['goodsSku'])) {
                    $model = new Goods();
                    $res = $model->adjustStock($goods_data['data']['goodsSku']);
                    if ($res['code'] < 0) {
                        $info = model('fix_fail')->getInfo(['goods_sku' => $goods_data['data']['goodsSku']]);
                        if (empty($info)) {
                            model('fix_fail')->add(['goods_sku' => $goods_data['data']['goodsSku']]);
                        }
                    }
                }
            }
        }
        return 'success';
    }




    /**
     *  订单发货
     */
    public function delivery()
    {
        $notify_param = request()->post();
        Log::INFO('【订单发货】');
        Log::INFO($notify_param);
        $params = array_keys($notify_param);


        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$params;
        $new_data['method_text']="订单发货";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../deliveryLOG.txt');



        foreach ($params as $key => $value) {
            $param = json_decode($value, true);
            $goods_data = json_decode($param['requestData'],true);
            if (!empty($goods_data['data'])) {
                if (empty($goods_data['data']['cusOrderNo'])) {
                    return 'error';
                }
                $order_info = model('order')->getInfo(['out_trade_no' => $goods_data['data']['cusOrderNo']],'order_id,site_id');
                //查物流公司
                $express_company_id = model('express_company')->getInfo(['express_no_kd100' => $goods_data['data']['expressCode']],'company_id');
                //查goods_id
                $goods_info = model('goods')->getInfo(['houniao_goods_sku_no' => $goods_data['data']['goodsSku']],'goods_id');
                //通过goods_id查所有sku_no
                $goods_sku = model('goods_sku')->getColumn(['goods_id' => $goods_info['goods_id']],'sku_no');
                //通过sku_no查购买了哪些商品的order_goods_id
                $order_goods_ids = model('order_goods')->getColumn([['order_id','=',$order_info['order_id']],['sku_no','in',$goods_sku]],'order_goods_id');

                $data = array(
                    "type" => 'manual',//发货方式（手动发货）
                    "order_goods_ids" => implode(',',$order_goods_ids),
                    "express_company_id" => !empty($express_company_id) ? $express_company_id['company_id'] : 0,
                    "delivery_no" => $goods_data['data']['expressNo'],
                    "order_id" => $order_info['order_id'],
                    "delivery_type" => 1,
                    "site_id" => $order_info['site_id'],
                    "template_id" => 0//电子面单模板id
                );
                $order_model = new OrderModel();
                $result = $order_model->orderGoodsDelivery($data);
                if ($result['code'] < 0) {
                    Log::ERROR('【订单发货】');
                    Log::ERROR($goods_data['data']['cusOrderNo']);
                    Log::ERROR($result);
                }
            }
        }
        return 'success';
    }

    public function adjustPrice11()
    {
        $model = new Goods();
        $res = $model->adjustPrice('1111');
    }
}