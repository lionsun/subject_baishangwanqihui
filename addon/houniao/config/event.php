<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //候鸟支付余额不足
        'HouniaoBalance' => [
            'addon\houniao\event\HouniaoBalance'
        ],
        //订单支付异步执行
        'OrderPay' => [
            'addon\houniao\event\CreateHouniaoOrder'
        ],
    ],

    'subscribe' => [
    ],
];
