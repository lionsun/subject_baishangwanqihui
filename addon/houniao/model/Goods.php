<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\houniao\model;

use app\model\BaseModel;
use app\model\goods\Goods as GoodsModel;
use think\facade\Log;
use think\facade\Db;

/**
 * 商品
 */
class Goods extends BaseModel
{

    /**
     * 同步品牌  type=0
     * @return array
     */
    public function syncHouniaoBrand()
    {
        $model = new Houniao();
        $return = $model->getBrands();
        if ($return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }
        $record = model('sync_houniao')->getInfo(['type' => 0]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods_brand_houniao")->startTrans();
        try {
            $page_size = 300;
            if (empty($record)) {
                $count = count($return['data']);
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 0
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 0]);
            }

            $deal_data = array_slice($return['data'], $page_size * ($page - 1), $page_size);
            if (empty($deal_data)) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 0]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            foreach ($deal_data as $key => $value) {
                $add_list[] = [
                    'brand_name' => $value['brandName'],
                    'image_url' => $value['brandImg'],
                    'create_time' => time(),
                    'source' => 1,
                    'hn_brand_id' => $value['brandId']
                ];
            }
            if (!empty($add_list)) {
                $res = model('goods_brand_houniao')->addList($add_list);
                if (empty($res)) {
                    return $this->error('', $page . '失败' . $res);
                }
            }

            model('goods_brand_houniao')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods_brand_houniao')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 同步地址   type=1
     * @return array
     */
    public function syncHouniaoAddress()
    {
        $model = new Houniao();
        $return = $model->getRegions();
        if ($return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }
        $record = model('sync_houniao')->getInfo(['type' => 1]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }
        model("area")->startTrans();
        try {
            $page_size = 5000;
            if (empty($record)) {
                $count = count($return['regions']);
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 1
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 1]);
            }

            $deal_data = array_slice($return['regions'], $page_size * ($page - 1), $page_size);
            if (empty($deal_data)) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 1]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            //候鸟有的地区id
            $area_ids = array_column($deal_data,'areaId');
            //本身我们有的地区id
            $exit_area_ids = model('area')->getColumn([['id','in',$area_ids]],'id');
            //差集即候鸟多出来的地区
            $diff = array_diff($area_ids, $exit_area_ids);

            foreach ($deal_data as $key => $value) {
                if (!empty($diff) && in_array($value['areaId'],$diff)) {
                    $add_list[] = [
                        'id' => $value['areaId'],
                        'pid' => $value['pid'],
                        'name' => $value['areaName'],
                        'shortname' => $value['areaName'],
                        'longitude' => '',
                        'latitude' => '',
                        'level' => $value['areaLevel'],
                        'status' => 1,
                        'source' => 1,
                    ];
                }
            }
            if (!empty($add_list)) {
                $res = model('area')->addList($add_list);
                if (empty($res)) {
                    return $this->error('', $page . '失败' . $res);
                }
            }

            model('area')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('area')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    /**
     * 同步快递  type=2
     * @return array
     */
    public function syncHouniaoExpress()
    {
        $model = new Houniao();
        $return = $model->getExpress();
        if ($return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }
        $record = model('sync_houniao')->getInfo(['type' => 2]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("express_company")->startTrans();
        try {
            $page_size = 300;
            if (empty($record)) {
                $count = count($return['express']);
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 2
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 2]);
            }

            $deal_data = array_slice($return['express'], $page_size * ($page - 1), $page_size);
            if (empty($deal_data)) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 2]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }

            foreach ($deal_data as $key => $value) {
                $add_list[] = [
                    'company_name' => $value['name'],
                    'express_no_kd100' => $value['code'],
                    'create_time' => time()
                ];
            }

            if (!empty($add_list)) {
                $res = model('express_company')->addList($add_list);
                if (empty($res)) {
                    return $this->error('', $page . '失败' . $res);
                }
            }

            model('express_company')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('express_company')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 同步分类  type=3
     * @return array
     */
    public function syncHouniaoCategory()
    {
        $model = new Houniao();
        $return = $model->getCategory();
        if ($return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }

        $record = model('sync_houniao')->getInfo(['type' => 3]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("category_hn")->startTrans();
        try {
            $page_size = 2;
            if (empty($record)) {
                $count = count($return['data']);
                $page = 1;
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => $page,
                    'page_size' => $page_size,
                    'type' => 3,
                    'update_time' => time()
                ]);
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
            }

            $deal_data = array_slice($return['data'], $page_size * ($page - 1), $page_size);
            if (empty($deal_data)) {
                model('sync_houniao')->update(['status' => 1], ['type' => 3]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }

            foreach ($deal_data as $key => $value) {
                $res = $this->structureData($value, 0, 1);
                if ($res['code'] < 0) {
                    return $this->error('', '错误');
                }
            }
            model('category_hn')->commit();

            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('category_hn')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 分类递归操作
     * @param $data
     * @param $pid
     * @param $level
     */
    public function structureData($data, $pid, $level)
    {
        if ($level > 3) {
            return $this->success();
        }
        //父级分类信息
        if ($pid == 0) {
            $category_full_name = $data['catName'];
            $category = [
                'category_id_1' => 0,
                'category_id_2' => 0,
                'category_id_3' => 0
            ];
        } else {
            $pid_info = model('category_hn')->getInfo(['category_id' => $pid]);
            if (empty($pid_info)) {
                return $this->error('', '根据pid没有查到父级分类信息95行');
            }
            $category_full_name = $pid_info['category_full_name'] . '/' . $data['catName'];
            $category = [
                'category_id_1' => $pid_info['category_id_1'],
                'category_id_2' => $pid_info['category_id_2'],
                'category_id_3' => $pid_info['category_id_3'],
            ];
        }

        $id = model('category_hn')->add([
            'category_name' => $data['catName'],
            'pid' => $pid,
            'level' => $level,
            'category_id_1' => $category['category_id_1'],
            'category_id_2' => $category['category_id_2'],
            'category_id_3' => $category['category_id_3'],
            'category_full_name' => $category_full_name,
            'source' => 1,
            'hn_category_id' => $data['catId']
        ]);
        if (empty($id)) {
            return $this->error('', '添加分类出错124行');
        }
        model('category_hn')->update(['category_id_' . $level => $id], ['category_id' => $id]);

        if (!empty($data['children'])) {
            foreach ($data['children'] as $key => $value) {
                $this->structureData($value, $id, $level + 1);
            }
        }
    }

    /**
     * 同步商品  type=4
     * @return array
     */
    public function syncHouniaoGoods()
    {
        $record = model('sync_houniao')->getInfo(['type' => 4]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }
        $model = new Houniao();
        $page_size = 150;
        model("goods")->startTrans();
        try {
            if (empty($record)) {
                $return = $model->goodsList(1, $page_size);
                model('sync_houniao')->add([
                    'total' => $return['data']['total'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 4
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 4]);
                $return = $model->goodsList($page, $page_size);
            }
            if ($return['code'] != 0) {
                return $this->error('', 'HOUNIAO_RETURN_ERROR');
            }
            if (empty($return['data']['goodsList'])) {
                model('sync_houniao')->update(['status' => 1], ['type' => 4]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }

            foreach ($return['data']['goodsList'] as $key => $value) {
                //HN1075520003  每
                //HN1075510083  女
                //if ($value['sku'] == 'HN10755601228') {
                $res = $this->addOneGoods($value);
                if ($res['code'] < 0) {
                    return $res;
                }
                //}
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 添加商品
     * @param $base_data
     * @return array
     */
    public function addOneGoods($base_data)
    {
        //商品详情处理  循环将图片上传
        $description = htmlspecialchars_decode($base_data['description']);

        if (!empty($base_data['descriptionImages'])) {
            foreach ($base_data['descriptionImages'] as $k => $v) {
                $description = str_replace('/' . $v, $base_data['picLibUrl'] . $v, $description);
            }
        }
        $goods_spec_format = $goods_sku_data = [];
        $stock = $goods_price = $goods_market_price = $goods_cost_price = 0;
        //通过查询价格接口获取规格数据
        $model = new Houniao();
        $price_return = $model->getPrice($base_data['sku']);

        $sku = [];
        if ($price_return['code'] == 0 && !empty($price_return['data'])) {
            $sku = $price_return['data'];

            //俩数组，一个是’规格/保质期‘，一个是’包装‘
            if ($sku[0]['specGroupName'] == '基准') {
                $group_1_spec_name = '规格';
            }elseif (!empty($sku[0]['startExpDate'])) {
                $group_1_spec_name = '保质期';
            }else{
                $group_1_spec_name = '';
            }

            if (empty($group_1_spec_name)) {
                return $this->error('','group_1_spec_name_error');
            }
            $usec = (float)explode(" ", microtime())[0];
            $msec = round($usec * 1000);
            $spec_id = -(count($sku) - 1 + getdate()['seconds'] + $msec);

            $group_1 = [
                'spec_id' => $spec_id,
                'spec_name' => $group_1_spec_name,
                'value' => [],
            ];

            $spec_all = [];
            foreach ($sku as $key => $spec_group) {
                $spec_value_id = -(abs($spec_id) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($spec_group['specDetail']) + rand(0,100);
                $group_1['value'][] = [
                    'spec_id' => $group_1['spec_id'],
                    'spec_name' => $group_1['spec_name'],
                    'spec_value_id' => $spec_value_id,
                    'spec_value_name' => $spec_group['specGroupName'],
                    'image' => ''
                ];

                foreach ($spec_group['specDetail'] as $kk => $vv) {
                    //以规格为键的数组
                    $sku[$key]['item'][$vv['specName']] = $vv;
                    //规格的全部组合名
                    if (!in_array($vv['specName'], $spec_all)) {
                        array_push($spec_all, $vv['specName']);
                    }
                }
            }

            $group_2 = [
                'spec_id' => $spec_id - 1,
                'spec_name' => !empty($sku[0]['goodsUnit']) ? $sku[0]['goodsUnit'].'装' : '规格',
                'value' => [],
            ];

            if (!empty($spec_all)) {
                foreach ($spec_all as $key => $specName) {
                    $spec_value_id = -(abs($spec_id) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($sku[0]['specDetail']) + rand(100,200);

                    $group_2['value'][] = [
                        'spec_id' => $group_2['spec_id'],
                        'spec_name' => $group_2['spec_name'],
                        'spec_value_id' => $spec_value_id,
                        'spec_value_name' => $specName,
                    ];
                }
            }

            $goods_spec_format = array_merge([$group_1], [$group_2]);
            
            foreach ($sku as $key => $value) {
                //生成的id组合
                $sku[$key]['value'] = $group_2['value'];
                foreach ($group_1['value'] as $k => $v) {
                    //外层添加group_1的数据
                    if ($value['specGroupName'] == $v['spec_value_name']) {
                        $sku[$key]['spec_id'] = $v['spec_id'];
                        $sku[$key]['spec_name'] = $v['spec_name'];
                        $sku[$key]['spec_value_id'] = $v['spec_value_id'];
                        $sku[$key]['spec_value_name'] = $v['spec_value_name'];
                        $sku[$key]['image'] = $v['image'];
                        break;
                    }
                }
            }

            foreach ($sku as $key => $value) {
                foreach ($value['value'] as $k => $v) {
                    $item = [
                        "specName" => $v['spec_value_name'],
                        "specPrice" => "",
                        "specNum" => "",
                        "weight" => "",
                        "stock" => "",
                        "deliverMoney" => "",
                        "specSku" => "",
                    ];
                    //里面添加group_2的数据，有则组合，没有设默认值
                    if (isset($sku[$key]['item'][$v['spec_value_name']])) {
                        $sku[$key]['value'][$k] = array_merge($sku[$key]['value'][$k], $sku[$key]['item'][$v['spec_value_name']]);
                    } else {
                        $sku[$key]['value'][$k] = array_merge($sku[$key]['value'][$k], $item);
                    }
                }
            }

            foreach ($sku as $key => $value) {
                //不配送地区
                $no_delivery_area = [];
                foreach ($value['no_delivery_area'] as $k => $v) {
                    foreach ($v['cities'] as $kk => $vv) {
                        $no_delivery_area[] = $vv['area_id'];
                    }
                }

                //花运费地区
                $delivery_money_area = $this->deliverMoneyArea();
                if (!empty($no_delivery_area)) {
                    $delivery_money_area = array_diff($delivery_money_area, $no_delivery_area);
                }

                foreach ($value['value'] as $k => $v) {
                    $stock += intval($v['stock']);
                    $price = $market_price = 0;
                    if ((float)$v['specPrice'] != 0) {
                        //销售价
                        $price = round((float)$v['specPrice']*((1.3-1)*0.13/1.13+1.3));
                        //市场价
                        $market_price = round((float)$price/0.8);
                    }

                    if ($k == 0) {
                        $goods_cost_price = $v['specPrice'];
                        $goods_market_price = $market_price;
                        $goods_price = $price;
                    }

                    $goods_sku_data[] = [
                        'spec_name' =>  $v['specName'],
                        'sku_no' => $v['specSku'],
                        'price' => $price,
                        'market_price' => $market_price,
                        'cost_price' => $v['specPrice'],
                        'stock' => $v['stock'],
                        'weight' => (float)$v['weight']/1000,
                        'volume' => '',
                        'start_exp_time' => !empty($value['startExpDate']) ? strtotime($value['startExpDate']) : 0,
                        'end_exp_time' => !empty($value['endExpDate']) ? strtotime($value['endExpDate']) : 0,
                        'delivery_money_area' => implode(',',$delivery_money_area),//花运费地区
                        'delivery_money' => $v['deliverMoney'], //运费
                        'no_delivery_area' => implode(',',$no_delivery_area), //不配送地区
                        'sku_image' => $base_data['picLibUrl'] . $base_data['original'],
                        'sku_images' => $base_data['picLibUrl'] . $base_data['original'],
                        'sku_images_arr' => [$base_data['picLibUrl'] . $base_data['original'],],
                        'sku_spec_format' => [
                            [
                                'spec_id' => $value['spec_id'],
                                'spec_name' => $value['spec_name'],
                                'spec_value_id' => $value['spec_value_id'],
                                'spec_value_name' => $value['spec_value_name'],
                                'image' => $value['image'],
                            ],
                            [
                                'spec_id' => $v['spec_id'],
                                'spec_name' => $v['spec_name'],
                                'spec_value_id' => $v['spec_value_id'],
                                'spec_value_name' => $v['spec_value_name'],
                            ]
                        ],
                    ];
                }
            }
//            dump($goods_spec_format);
//            dump($goods_sku_data);die;
        }

        $category_id_arr = explode('_',$base_data['categoryIdPath']);

        $category_id_1_hn = isset($category_id_arr[0]) ?  -$category_id_arr[0] : 0;
        $category_id_2_hn = isset($category_id_arr[1]) ?  -$category_id_arr[1] : 0;
        $category_id_3_hn = isset($category_id_arr[2]) ?  -$category_id_arr[2] : 0;

        //初始
        $category_id_1 = $category_id_1_hn;
        $category_id_2 = $category_id_2_hn;
        $category_id_3 = $category_id_3_hn;
        $category_id = -end($category_id_arr);
        $category_name = end($base_data['category']);
        $site_id = 0;

        $cate_ref_hn = model('cate_ref_hn2')->getInfo(['cate1' => $category_id_1_hn, 'cate2' => $category_id_2_hn, 'cate3' => $category_id_3_hn]);
        if (!empty($cate_ref_hn)) {
            $category_id_1 = $cate_ref_hn['ns_cate_1'];
            $category_id_2 = $cate_ref_hn['ns_cate_2'];
            $category_id_3 = $cate_ref_hn['ns_cate_3'];
            $category_name = $cate_ref_hn['ns_cate_name'];
            if (!empty($cate_ref_hn['ns_cate_1'])) {
                $category_id = $cate_ref_hn['ns_cate_1'];
            }
            if (!empty($cate_ref_hn['ns_cate_2'])) {
                $category_id = $cate_ref_hn['ns_cate_2'];
            }
            if (!empty($cate_ref_hn['ns_cate_3'])) {
                $category_id = $cate_ref_hn['ns_cate_3'];
            }
            $site_id = $cate_ref_hn['site_id'];
        } else {
            $cate_ref_hm_hn = model('cate_ref_hm_hn')->getInfo(['hm_cate1' => abs($category_id_1_hn),'hm_cate2' => abs($category_id_2_hn),'hm_cate3' => abs($category_id_3_hn)],'hn_cata1,hn_cata2,hn_cata3,hn_cata3_lbl,shop_name');
            if (!empty($cate_ref_hm_hn)) {
                $category_id_1 = $cate_ref_hm_hn['hn_cata1'];
                $category_id_2 = $cate_ref_hm_hn['hn_cata2'];
                $category_id_3 = $cate_ref_hm_hn['hn_cata3'];
                $category_name = $cate_ref_hm_hn['hn_cata3_lbl'];
                if (!empty($cate_ref_hm_hn['hn_cata1'])) {
                    $category_id = $cate_ref_hm_hn['hn_cata1'];
                }
                if (!empty($cate_ref_hm_hn['hn_cata2'])) {
                    $category_id = $cate_ref_hm_hn['hn_cata2'];
                }
                if (!empty($cate_ref_hm_hn['hn_cata3'])) {
                    $category_id = $cate_ref_hm_hn['hn_cata3'];
                }
                $shop_info = model('shop')->getInfo(['site_name' => $cate_ref_hm_hn['shop_name']],'site_id');
                if (!empty($shop_info)) {
                    $site_id = $shop_info['site_id'];
                }else{
                    $site_id = $this->shopCategory($cate_ref_hm_hn['hn_cata3']);
                }
            }
        }



        $brand = model('goods_brand')->getInfo(['brand_name' => $base_data['brand']],'brand_id');

        //库存为0，直接下架
        $goods_state = $base_data['isSale'];
        if ($stock == 0) {
            $goods_state = 0;
        }

        $data = [
            'goods_name' => $base_data['goodsName'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => $site_id,
            'website_id' => 0,
            'category_id' => $category_id,
            'category_id_1' => $category_id_1,
            'category_id_2' => $category_id_2,
            'category_id_3' => $category_id_3,
            'category_name' => $category_name,
            'brand_id' => !empty($brand) ? $brand['brand_id'] : 0,
            'brand_name' => $base_data['brand'],
            'goods_image' => $base_data['picLibUrl'] . $base_data['original'],
            'goods_content' => $description,
            'goods_state' => $goods_state,
            'price' => $goods_price,
            'market_price' => $goods_market_price,
            'cost_price' => $goods_cost_price,
            'sku_no' => !empty($sku) ? $sku[0]['specDetail'][0]['specSku'] : '',
            'weight' => !empty($sku) ? (float)$sku[0]['specDetail'][0]['weight']/1000 : '',
            'volume' => 0,
            'goods_stock' => $stock,
            'goods_stock_alarm' => 0,
            'is_free_shipping' => 1,
            'shipping_template' => 0,
            'goods_spec_format' => !empty($goods_spec_format) ? json_encode($goods_spec_format, JSON_UNESCAPED_UNICODE) : '',
            'goods_attr_format' => '',
            'introduction' => '',
            'keywords' => $base_data['goodsSeoKeywords'],
            'unit' => $base_data['goodsUnit'],
            'sort' => 0,
            'commission_rate' => 0,
            'video_url' => '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',
            'goods_shop_category_ids' => '',
            'supplier_id' => 0,
            'source' => 1,
            'houniao_goods_sku_no' => $base_data['sku'],
            'trade_type' => $this->getTradeType($base_data['tradeType']),

            'max_buy' => 0,
            'min_buy' => 0
        ];

        $goods_model = new GoodsModel();
        $res = $goods_model->addGoods($data);
        if ($res['code'] < 0) {
            return $this->error($base_data['sku']);
        }
        return $this->success();
    }


    /**
     * 添加商品
     * @param $goodsSku
     * @return array
     */
    public function addGoods($goodsSku)
    {
        $model = new Houniao();
        $detail_return = $model->getGoodsDetailBySku($goodsSku);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }

        //候鸟下架的直接删除
        $info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku],'goods_id');
        if ($detail_return['data']['goodsDetail']['isSale'] == 0) {
            if (!empty($info)) {
                model('goods')->delete(['goods_id' => $info['goods_id']]);
                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
            }
            return $this->success();
        }

        if (!empty($info)) {
            $sku_info = model('goods_sku')->getInfo(['goods_id' =>$info['goods_id'] ]);
            if (!empty($sku_info)) {
                //更新商品
                $res = $this->editOneGoods($goodsSku, $detail_return['data']['goodsDetail']);
                return $res;
            }else{
                //候鸟下架商品查不到价格接口，导致没有保存到sku的，删除之前的goods重新添加
                model('goods')->delete(['goods_id' => $info['goods_id'] ]);
            }
        }

        $res = $this->addOneGoods($detail_return['data']['goodsDetail']);
        return $res;
    }

    /**
     * 更新商品
     * @param $goodsSku
     * @return array
     */
    public function editGoods($goodsSku)
    {
        $model = new Houniao();
        $detail_return = $model->getGoodsDetailBySku($goodsSku);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }

        $info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku],'goods_id');
        if (empty($info)) {
            return $this->error('', '未找到商品数据');
        }

        //候鸟下架的直接删除
        if ($detail_return['data']['goodsDetail']['isSale'] == 0) {
            if (!empty($info)) {
                model('goods')->delete(['goods_id' => $info['goods_id']]);
                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
            }
            return $this->success();
        }

        //更新商品
        $res = $this->editOneGoods($goodsSku, $detail_return['data']['goodsDetail']);
        return $res;
    }


    /**
     * 编辑商品
     * @param $goodsSku
     * @param $base_data
     * @return array
     */
    public function editOneGoods($goodsSku, $base_data)
    {
//        if ($base_data['isSale'] == 0) {
//            //数据库里存在下架的，直接删除
//            $info = model('goods')->getInfo(['houniao_goods_sku_no' => $base_data['sku']],'goods_id');
//            if (!empty($info)) {
//                model('goods')->delete(['goods_id' => $info['goods_id']]);
//                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
//            }
//            return $this->success();
//        }

        $goods_info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku]);
        if (empty($goods_info)) {
            return $this->error('','未找到商品数据');
        }

        $goods_sku = model('goods_sku')->getList(['goods_id' => $goods_info['goods_id']]);

        //已经保存的所有候鸟sku
        $sku_no_item =[];
        foreach ($goods_sku as $key => $value) {
            if (!empty($value['sku_no'])) {
                $sku_no_item[$value['sku_no']] = $value['sku_id'];
            }
        }

        //商品详情处理
        $description = htmlspecialchars_decode($base_data['description']);
        if (!empty($base_data['descriptionImages'])) {
            foreach ($base_data['descriptionImages'] as $k => $v) {
                $description = str_replace('/' . $v, $base_data['picLibUrl'] . $v, $description);
            }
        }

        $goods_spec_format = $goods_sku_data = [];
        $stock = 0;
        $goods_price = $goods_info['price'];
        $goods_market_price = $goods_info['market_price'];
        $goods_cost_price = $goods_info['cost_price'];

        //通过查询价格接口获取规格数据
        $model = new Houniao();
        $price_return = $model->getPrice($base_data['sku']);
        $verify_state = 1;
        if ($price_return['code'] == 0 && !empty($price_return['data'])) {
            $sku = $price_return['data'];

            //TODO 测试
            //unset($sku[1]);
            //dump($sku);die;

            //俩数组，一个是’规格/保质期‘，一个是’包装‘
            if ($sku[0]['specGroupName'] == '基准') {
                $group_1_spec_name = '规格';
            }elseif (!empty($sku[0]['startExpDate'])) {
                $group_1_spec_name = '保质期';
            }else{
                $group_1_spec_name = '';
            }

            if (empty($group_1_spec_name)) {
                return $this->error('','group_1_spec_name_error');
            }

            $old_goods_spec_format = json_decode($goods_info['goods_spec_format'],true);
            if (empty($old_goods_spec_format)){
                $usec = explode(" ", microtime())[0];
                $msec = round($usec * 1000);
                $spec_id = -(count($sku) - 1 + getdate()['seconds'] + $msec);

                $old_group_1 = [
                    'spec_id' => $spec_id,
                    'value' => []
                ];

                $old_group_2 = [
                    'spec_id' => $spec_id - 1,
                    'value' => [],
                ];
            } else {
                $old_group_1 = $old_goods_spec_format[0];
                $old_group_2 = $old_goods_spec_format[1];
            }


            /*=================================$group_1================================================*/
            //组1应该都不会变


            $spec_id = $old_group_1['spec_id'];
            $group_1 = [
                'spec_id' => $old_group_1['spec_id'],
                'spec_name' => $group_1_spec_name,
            ];
            //规格值
            $spec_value_item = [];
            foreach ($old_group_1['value'] as $key => $value) {
                $spec_value_item[] = $value['spec_value_id'];
            }

            //个数没变
            $spec_value_ids = $spec_value_item;
            if (count($sku) == count($old_group_1['value'])) {
                $spec_value_ids = $spec_value_item;
                //增加了
            }elseif(count($sku) > count($old_group_1['value'])){
                $inc_num = count($sku) - count($old_group_1['value']);
                for($i=0;$i< $inc_num;$i++){
                    $new_spec_value_id[] = -(abs($spec_id) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + $i + rand(0,10);
                }
                $spec_value_ids = array_merge($spec_value_ids, $new_spec_value_id);
                //减少了
            }else{
                $spec_value_ids = array_slice($spec_value_item, 0, count($sku));
            }

            $spec_all = [];
            foreach ($sku as $key => $spec_group) {
                $group_1['value'][] = [
                    'spec_id' => $group_1['spec_id'],
                    'spec_name' => $group_1['spec_name'],
                    'spec_value_id' => $spec_value_ids[$key],
                    'spec_value_name' => $spec_group['specGroupName'],
                    'image' => ''
                ];
                foreach ($spec_group['specDetail'] as $kk => $vv) {
                    $sku[$key]['item'][$vv['specName']] = $vv;

                    //规格的全部组合名
                    if (!in_array($vv['specName'], $spec_all)) {
                        array_push($spec_all, $vv['specName']);
                    }
                }
            }

            /*=================================$group_2================================================*/
            //组二有可能增加，减少
            $group_2 = [
                'spec_id' => $old_group_2['spec_id'],
                'spec_name' => !empty($sku[0]['goodsUnit']) ? $sku[0]['goodsUnit'].'装' : '规格',
                'value' => [],
            ];

            $spec_value_item = [];
            foreach ($old_group_2['value'] as $key => $value) {
                $spec_value_item[] = $value['spec_value_id'];
            }

            //个数没变
            $spec_value_ids = $spec_value_item;
            if (count($spec_all) == count($old_group_2['value'])) {
                $spec_value_ids = $spec_value_item;
                //增加了
            }elseif(count($spec_all) > count($old_group_2['value'])){
                $inc_num = count($spec_all) - count($old_group_2['value']);
                $new_spec_value_id = [];
                for($i=0;$i< $inc_num;$i++){
                    $new_spec_value_id[] = -(abs($old_group_2['spec_id']) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + $i + rand(100,200);
                }
                $spec_value_ids = array_merge($spec_value_ids, $new_spec_value_id);
                $verify_state = 0;
                Log::INFO('【sku增加了】'.$goodsSku);
                //减少了
            }else{
                $spec_value_ids = array_slice($spec_value_item, 0, count($spec_all));
                $verify_state = 0;
                Log::INFO('【sku减少了】'.$goodsSku);
            }

            if (!empty($spec_all)) {
                foreach ($spec_all as $key => $specName) {
                    $group_2['value'][] = [
                        'spec_id' => $group_2['spec_id'],
                        'spec_name' => $group_2['spec_name'],
                        'spec_value_id' => $spec_value_ids[$key],
                        'spec_value_name' => $specName,
                    ];
                }
            }

            /*===========================只要更新group_1  group_2======================================================*/
            $goods_spec_format = array_merge([$group_1], [$group_2]);

            foreach ($sku as $key => $value) {
                $sku[$key]['value'] = $group_2['value'];
                foreach ($group_1['value'] as $k => $v) {
                    if ($value['specGroupName'] == $v['spec_value_name']) {
                        $sku[$key]['spec_id'] = $v['spec_id'];
                        $sku[$key]['spec_name'] = $v['spec_name'];
                        $sku[$key]['spec_value_id'] = $v['spec_value_id'];
                        $sku[$key]['spec_value_name'] = $v['spec_value_name'];
                        $sku[$key]['image'] = $v['image'];
                        break;
                    }
                }
            }

            foreach ($sku as $key => $value) {
                foreach ($value['value'] as $k => $v) {
                    $item = [
                        "specName" => $v['spec_value_name'],
                        "specPrice" => "",
                        "specNum" => "",
                        "weight" => "",
                        "stock" => "",
                        "deliverMoney" => "",
                        "specSku" => "",
                    ];
                    if (isset($sku[$key]['item'][$v['spec_value_name']])) {
                        $sku[$key]['value'][$k] = array_merge($sku[$key]['value'][$k], $sku[$key]['item'][$v['spec_value_name']]);
                    } else {
                        $sku[$key]['value'][$k] = array_merge($sku[$key]['value'][$k], $item);
                    }
                }
            }
            //用到之前的sku_id
            $use_sku_ids = [];

            foreach ($sku as $key => $value) {
                //不配送地区
                $no_delivery_area = [];
                foreach ($value['no_delivery_area'] as $k => $v) {
                    foreach ($v['cities'] as $kk => $vv) {
                        $no_delivery_area[] = $vv['area_id'];
                    }
                }

                //花运费地区
                $delivery_money_area = $this->deliverMoneyArea();
                if (!empty($no_delivery_area)) {
                    $delivery_money_area = array_diff($delivery_money_area, $no_delivery_area);
                }

                foreach ($value['value'] as $k => $v) {
                    $stock += intval($v['stock']);

                    $premium = 1.3;
                    if ((float)$goods_info['premium'] != 0) {
                        $premium = (float)$goods_info['premium'];
                    }
                    //销售价
                    $price = $market_price = 0;
                    if ((float)$v['specPrice'] != 0) {
                        //销售价
                        $price =  round((float)$v['specPrice']*(($premium-1)*$premium/10/1.13+$premium));
                        //市场价
                        $market_price = round((float)$price/0.8);
                    }

                    if ($k == 0) {
                        $goods_cost_price = $v['specPrice'];
                        $goods_market_price = $market_price;
                        $goods_price = $price;
                    }

                    //候鸟那边sku有新增
                    $goods_sku_info = '';
                    if (!isset($sku_no_item[$v['specSku']])) {
                        $sku_id = model('goods_sku')->add([
                            'sku_no' => $v['specSku'],
                            'goods_id' => $goods_info['goods_id']
                        ]);
                    } else {
                        $sku_id = $sku_no_item[$v['specSku']];
                        array_push($use_sku_ids, $sku_id);
                        $goods_sku_info = model('goods_sku')->getInfo(['sku_id' => $sku_id],'sku_image,sku_images');
                    }

                    $goods_sku_data[] = [
                        'sku_id' => $sku_id,
                        'spec_name' =>  $v['specName'],
                        'sku_no' => $v['specSku'],
                        'price' => $price,
                        'market_price' => $market_price,
                        'cost_price' => $v['specPrice'],
                        'stock' => $v['stock'],
                        'weight' => (float)$v['weight']/1000,
                        'volume' => '',
                        'start_exp_time' => !empty($value['startExpDate']) ? strtotime($value['startExpDate']) : 0,
                        'end_exp_time' => !empty($value['endExpDate']) ? strtotime($value['endExpDate']) : 0,
                        'delivery_money_area' => implode(',',$delivery_money_area),//花运费地区
                        'delivery_money' => $v['deliverMoney'], //运费
                        'no_delivery_area' => implode(',',$no_delivery_area), //不配送地区
                        'sku_image' => !empty($goods_sku_info) && !empty($goods_sku_info['sku_image'])? $goods_sku_info['sku_image'] : $base_data['picLibUrl'] . $base_data['original'],
                        'sku_images' => !empty($goods_sku_info) && !empty($goods_sku_info['sku_images'])? $goods_sku_info['sku_images'] : $base_data['picLibUrl'] . $base_data['original'],
                        'sku_images_arr' => !empty($goods_sku_info) && !empty($goods_sku_info['sku_images'])? explode(',',$goods_sku_info['sku_images']) : [$base_data['picLibUrl'] . $base_data['original'],],
                        'sku_spec_format' => [
                            [
                                'spec_id' => $value['spec_id'],
                                'spec_name' => $value['spec_name'],
                                'spec_value_id' => $value['spec_value_id'],
                                'spec_value_name' => $value['spec_value_name'],
                                'image' => !empty($goods_sku_info)? $goods_sku_info['sku_image'] : $base_data['picLibUrl'] . $base_data['original'],
                            ],
                            [
                                'spec_id' => $v['spec_id'],
                                'spec_name' => $v['spec_name'],
                                'spec_value_id' => $v['spec_value_id'],
                                'spec_value_name' => $v['spec_value_name'],
                            ]
                        ],
                    ];
                }
            }

            //删除多余的sku数据  用到的$use_sku_ids和之前所有sku_ids比较，看看是否有没有用到的，有就删除
            $old_sku_ids = array_column($goods_sku,'sku_id');
            $diff = array_diff($old_sku_ids, $use_sku_ids);
            if (!empty($diff)) {
                model('goods_sku')->delete([['sku_id','in',$diff]]);
            }
        }
        //中间表  导数据的时候用来着 编辑不修改分类
        //$category_info = model('cate_ref_hm_hn')->getInfo(['hm_cate3' => $base_data['categoryId']],'hn_cata1,hn_cata2,hn_cata3,hn_cata3_lbl');

        $brand = model('goods_brand')->getInfo(['brand_name' => $base_data['brand']]);

        //小于等于库存预警，下架
        $goods_state = $base_data['isSale'];
        if ($stock <= (int)$goods_info['goods_stock_alarm']) {
            $goods_state = 0;
        }
        // dump($goods_spec_format);
        // dump($goods_sku_data);die;



        $category_id_arr = explode('_',$base_data['categoryIdPath']);
        $hn_category_id = end($category_id_arr);

        $data = [
            'goods_id' => $goods_info['goods_id'],
            'goods_name' => $base_data['goodsName'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => $this->shopCategory($hn_category_id),
            'website_id' => 0,

//            'category_id' => !empty($category_info) ? $category_info['hn_cata3'] : 0,
//            'category_id_1' => !empty($category_info) ? $category_info['hn_cata1'] : 0,
//            'category_id_2' => !empty($category_info) ? $category_info['hn_cata2'] : 0,
//            'category_id_3' => !empty($category_info) ? $category_info['hn_cata3'] : 0,
//            'category_name' => !empty($category_info) ? $category_info['hn_cata3_lbl'] : '',

            'category_id' => $goods_info['category_id'],
            'category_id_1' => $goods_info['category_id_1'],
            'category_id_2' => $goods_info['category_id_2'],
            'category_id_3' => $goods_info['category_id_3'],
            'category_name' => $goods_info['category_name'],

            'brand_id' => !empty($brand) ? $brand['brand_id'] : 0,
            'brand_name' => $base_data['brand'],
            'goods_image' => $goods_info['goods_image'],
            'goods_content' => $description,
            'goods_state' => $goods_state,
            'price' => $goods_price,
            'market_price' => $goods_market_price,
            'cost_price' => $goods_cost_price,
            'sku_no' => !empty($sku) ? $sku[0]['specDetail'][0]['specSku'] : '',
            'weight' => !empty($sku) ? (float)$sku[0]['specDetail'][0]['weight']/1000 : '',
            'volume' => 0,
            'goods_stock' => $stock,
            'goods_stock_alarm' => 0,
            'is_free_shipping' => 1,
            'shipping_template' => 0,
            'goods_spec_format' => !empty($goods_spec_format) ? json_encode($goods_spec_format, JSON_UNESCAPED_UNICODE) : '',
            'goods_attr_format' => '',
            'introduction' => '',
            'keywords' => $base_data['goodsSeoKeywords'],
            'unit' => $base_data['goodsUnit'],
            'sort' => 0,
            'commission_rate' => 0,
            'video_url' => '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',
            'goods_shop_category_ids' => '',
            'supplier_id' => 0,
            'source' => 1,
            'houniao_goods_sku_no' => $base_data['sku'],
            'trade_type' => $this->getTradeType($base_data['tradeType']),
            'premium' => $goods_info['premium'],
            'verify_state' => $verify_state,

            'max_buy' => 0,
            'min_buy' => 0
        ];

        $goods_model = new GoodsModel();
        $res = $goods_model->editGoods($data, 1);
        if ($res['code'] < 0) {
            return $this->error();
        }
        return $this->success();
    }


    /**
     * 上架商品
     * @param $goodsSku
     * @return array
     */
    public function onGoods($goodsSku)
    {
        $model = new Houniao();
        $detail_return = $model->getGoodsDetailBySku($goodsSku);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }

        //候鸟下架的直接删除
        $info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku],'goods_id');
        if ($detail_return['data']['goodsDetail']['isSale'] == 0) {
            if (!empty($info)) {
                model('goods')->delete(['goods_id' => $info['goods_id']]);
                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
            }
            return $this->success();
        }

        if (!empty($info)) {
            //更新商品
            $res = $this->editOneGoods($goodsSku, $detail_return['data']['goodsDetail']);
        } else {
            $res = $this->addOneGoods($detail_return['data']['goodsDetail']);
        }

        return $res;
    }

    /**
     * 下架商品
     * @param $goodsSku
     * @return array
     */
    public function offGoods($goodsSku)
    {
        //删除商品
        $goods_info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku], 'goods_id');
        if (!empty($goods_info)) {
            $res = model('goods')->delete(['goods_id' => $goods_info['goods_id']]);
            $res = model('goods_sku')->delete(['goods_id' => $goods_info['goods_id']]);
        }
        return $this->success();
    }

    /**
     * 调整价格
     * @param $goodsSku
     * @return array
     */
    public function adjustPrice($goodsSku)
    {
        $model = new Houniao();
        $detail_return = $model->getGoodsDetailBySku($goodsSku);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }

        //候鸟下架的直接删除
        $info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku],'goods_id');
        if (empty($info)) {
            return $this->error('', '未找到商品数据');
        }

        if ($detail_return['data']['goodsDetail']['isSale'] == 0) {
            if (!empty($info)) {
                model('goods')->delete(['goods_id' => $info['goods_id']]);
                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
            }
            return $this->success();
        }

        $res = $this->editOneGoods($goodsSku, $detail_return['data']['goodsDetail']);
        return $res;


//        $model = new Houniao();
//        $detail_return = $model->getPrice($goodsSku);
//        if ($detail_return['code'] != 0) {
//            return $this->error('', 'HOUNIAO_RETURN_ERROR');
//        }
//
//        //候鸟所有sku_no
//        $houniao_sku_no = [];
//        foreach ($detail_return['data'] as $key => $specGroup) {
//            foreach ($specGroup['specDetail'] as $k => $spec) {
//                $houniao_sku_no[] =  $spec['specSku'];
//            }
//        }
//        //已经保存的所有sku_no
//        $goods_info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku]);
//        $goods_sku_list = model('goods_sku')->getList(['goods_id' => $goods_info['goods_id']]);
//        $sku_no = [];
//        foreach ($goods_sku_list as $key => $value) {
//            if (!empty($value['sku_no'])) {
//                $sku_no[] = $value['sku_no'];
//            }
//        }
//
//
//        //差集
//        if (count($houniao_sku_no) > count($sku_no)) {
//            //候鸟增加了规格
//            $detail_return = $model->getGoodsDetailBySku($goodsSku);
//            if ($detail_return['code'] != 0) {
//                return $this->error('', 'HOUNIAO_RETURN_ERROR');
//            }
//            //更新商品
//            $res = $this->editOneGoods($goodsSku, $detail_return['data']['goodsDetail']);
//            return $this->success($res);
//        } else{
//            $premium = 1.3;
//            if ((float)$goods_info['premium'] != 0) {
//                $premium = (float)$goods_info['premium'];
//            }
//
//            //候鸟减少了规格，这边不做处理，只是将库存改为0
//            $goods_update_data = [];
//            $stock = 0;
//            foreach ($detail_return['data'] as $key => $specGroup) {
//                foreach ($specGroup['specDetail'] as $k => $spec) {
//                    $stock += $spec['stock'];
//
//                    //销售价
//                    $price = round((float)$spec['specPrice']*(($premium-1)*$premium/10/1.13+$premium));
//                    //市场价
//                    $market_price = round((float)$price/0.8);
//
//                    $spec_update_price = [
//                        'price' => $price,
//                        'market_price' => $market_price,
//                        'cost_price' => $spec['specPrice'],
//                        'stock' => $spec['stock']
//                    ];
//                    model('goods_sku')->update($spec_update_price,['sku_no' => $spec['specSku']]);
//
//                    if ($key == 0 && $k == 0) {
//                        $goods_update_data['price'] = $price;
//                        $goods_update_data['market_price'] = $market_price;
//                        $goods_update_data['cost_price'] = $spec['specPrice'];
//                    }
//
//                    $spec_no[] = $spec['specSku'];
//                }
//            }
//            $goods_update_data['goods_stock'] = $stock;
//            model('goods')->update($goods_update_data, ['houniao_goods_sku_no' => $goodsSku]);
//
//            //不在候鸟sku里面的，库存都改为0
//            if (!empty($spec_no)) {
//                model('goods_sku')->update(['stock' => 0],[['goods_id','=',$goods_info['goods_id']],['sku_no','not in',$spec_no]]);
//            }
//
//        }
//
//        return $this->success();
    }

    /**
     * 调整库存
     * @param $goodsSku
     * @return array
     */
    public function adjustStock($goodsSku)
    {
        $model = new Goods();
        $model->editGoods($goodsSku);


        $model = new Houniao();
        $detail_return = $model->getPrice($goodsSku);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }
        if (!empty($detail_return['data'])) {

            $stock = 0;
            $spec_sku_arr = [];
            foreach ($detail_return['data'] as $key => $specGroup) {
                foreach ($specGroup['specDetail'] as $k => $spec) {
                    $stock += intval($spec['stock']);
                    model('goods_sku')->update(['stock' => $spec['stock']],['sku_no' => $spec['specSku']]);
                    $spec_sku_arr[] = $spec['specSku'];
                }
            }
            $info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku],'goods_stock_alarm,goods_id,goods_name,goods_stock');
            if (empty($info)) {
                return $this->success();
            }

            //不在候鸟接口里面的，表示候鸟那边已经删除
            if (!empty($spec_sku_arr)) {
                model('goods_sku')->update(['stock' => 0],[['goods_id','=',$info['goods_id']],['sku_no','not in',$spec_sku_arr]]);
            }

            $goods_update_data = [
                'goods_stock' => $stock,
            ];
            //小于警戒值，下架
            if ($stock <= (int)$info['goods_stock_alarm']) {
                $goods_update_data['goods_state'] = 0;
                $add_goods_dynamic = [
                    'goods_id' => $info['goods_id'],
                    'goods_name' => $info['goods_name'],
                    'goods_state' => 0,
                    'value' => $stock,
                    'remark' => '小于库存预警，下架',
                    'status' => 0,
                    'create_time' => time()
                ];
                model('goods_dynamic')->add($add_goods_dynamic);
                model('goods_sku')->update(['goods_state' => 0],['goods_id' => $info['goods_id']]);
            }
            model('goods')->update($goods_update_data, ['houniao_goods_sku_no' => $goodsSku]);
        }

        return $this->success();
    }

    /**
     * 获取贸易类型
     * @param $name
     * @return int
     */
    private function getTradeType($name)
    {
        switch ($name) {
            case '保税直供':
                $tradeType = 1;
                break;
            case '完税进口':
                $tradeType = 2;
                break;
            case '国内贸易':
                $tradeType = 3;
                break;
            case '香港直邮':
                $tradeType = 4;
                break;
            case '海外直邮':
                $tradeType = 5;
                break;
            default:
                $tradeType = 0;
        }
        return $tradeType;
    }


    /**
     * 店铺分类对应
     * @param $category_id
     * @return int|string
     */
    public function shopCategory($category_id)
    {
        $info = model('category_hn')->getInfo(['level' => 3, 'hn_category_id' => $category_id],'site_id');
        $site_id = 0;
        if (!empty($info)) {
            $site_id = $info['site_id'];
        }
        return $site_id;


        //平台店铺id  => 候鸟三级分类id
        $arr = [
            '2' => [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 956, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 954, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 948, 246, 248, 249, 250, 253, 254, 255, 584, 847, 588, 589, 590, 591, 592, 593, 594, 596, 597, 598, 599, 600, 601, 602, 604, 605, 606, 607, 608, 609, 610, 611, 850, 957, 613, 614, 615, 617, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 632, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 645, 646, 647, 1105, 1106, 1107, 829, 1033, 1035, 1037, 258, 259, 260, 261, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 293, 294, 295, 296, 297, 298, 300, 301, 302, 303, 304, 305, 306, 751, 752, 753,754, 755, 756, 757, 759, 760, 761, 762, 763, 764, 765, 766, 767, 769, 770, 771, 772, 773, 774, 776, 777, 778, 779, 780, 781, 782, 783, 785, 786, 967,1338,],  //百商万企汇自营超市
            '3' => [31, 32, 37, 38, 39, 40, 41, 43, 44, 45, 46, 48, 49, 50, 51, 52, 53, 54,55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 840, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 78, 79, 80, 81, 82, 83, 84, 85, 87, 88, 90, 91, 92, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 691, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 969,],  //百商万企汇母婴自营店
            '4' => [94, 95, 668, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1132, 1133, 1134, 922, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1129, 1130, 1131, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 975, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 972, 973, 349, 350, 351, 352, 353, 354, 355, 356, 357, 974, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 977, 978, 979, 980, 981, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 976, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 982],  //百商万企汇服饰自营店
            '5' => [111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 1027, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170,171,984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1012, 1013, 1014, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 394],  //百商万企汇家电自营店
            '6' => [174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,839,843,844,845,846,195,196,197,198,199,200,201,202,797,949,34,35,36,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,222,223,224,225,226,227,228,229,230,832,841,842,232,233,234,235,236,237,238,239,240,241,242,243,799,800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815],  //百商万企汇大健康自营店
            '7' => [309, 310, 311, 312, 313, 314, 315, 316, 319, 320, 321, 941, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1038, 1039, 1040, 1041, 1042, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 943, 944, 945, 946, 1051, 942, 1046, 1047, 1048, 1049, 1050, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1080, 1081, 1082, 939, 1083, 1084, 935, 936, 937, 938, 1043, 1044, 1045, 940, 1093, 1095, 947],  //百商万企汇钟表首饰自营店
            '8' => [360,361,362,363,364,365,366,367,830,1104,1031],  //百商万企汇办公用品自营店
            '9' => [370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 952, 971,],  //百商万企汇户外运动自营店
            '10' => [384, 385, 386, 387, 388, 390, 391, 392, 393, 395, 669, 397, 398, 399, 400, 401, 402, 403, 404, 405, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 648, 418, 419, 420, 421, 422, 423, 425, 426, 427, 428, 429, 430, 431, 432, 650, 651, 652, 653, 654, 655, 656, 657, 659, 660, 661, 662, 663, 664, 665, 666, 667, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 685, 686, 687, 688, 689, 690, 788, 789, 790, 791, 792, 793, 794, 795, 796, 834, 835, 836, 837, 838, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748,1336,1335,1169,],  //百商万企汇家居家装自营店
            '11' => [856,857,859,862,865,866,868,871,891,892,893,894,895,896,897,898,899,900,901,902,903,904,905,906,907,908,909,1029,551,],  //百商万企汇宠物生活自营店
            '12' => [694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 706, 707, 708, 709, 710, 711, 712, 713, 715, 716, 717, 718, 719, 720, 721, 831, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 1025,],  //百商万企汇手机数码自营店
            '13' => [951, 921, 953, 955, 950, 919, 920, 1258, 1259, 1215, 1229, 1231, 1307, 1332, ],  //百商万企汇汽车用品自营店
        ];
        if (!empty($category_id)) {
            foreach ($arr as $key => $value) {
                if (in_array($category_id, $value)) {
                    return $key;
                }
            }
        }
        return 0;
    }

    /**
     * 需要运费地区
     */
    public function deliverMoneyArea()
    {
        //内蒙古自治区、西藏自治区、甘肃省、青海省、宁夏回族自治区、新疆维吾尔自治区
        $pid = [150000,540000,620000,630000,640000,650000];
        //二级
        $ids = model('area')->getColumn([['pid','in',$pid]],'id');
        $idss = [];
        if (!empty($ids)) {
            //三级
            $idss = model('area')->getColumn([['pid','in',$ids]],'id');
        }
        return array_merge($pid, $ids, $idss);
    }

    /**
     * 同步半夜失败的
     * @return array
     */
    public function syncfixFail()
    {
        $deal_data = model('fix_fail')->pageList([],'*','id asc',1,5);
        if (empty($deal_data['list'])) {
            return $this->error('', '无失败数据');
        }
        $ids = [];
        foreach ($deal_data['list'] as $key => $value) {
            $model = new Goods();
            $res = $model->onGoods($value['goods_sku']);
            if ($res['code'] >= 0) {
                $ids[] = $value['id'];
            }
        }
        model('fix_fail')->delete([['id','in',$ids]]);
        return $this->success();
    }




    /**
     * new
     * 商品分类调整
     * @return array
     */
    public function goodsAdjustCate()
    {
        $list = model('goods')->getList(['category_id' => 0,'source' => 1],'houniao_goods_sku_no,goods_id');


        $record = model('sync_houniao')->getInfo(['type' => 6]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }


        model("goods")->startTrans();
        try {
            $page_size = 500;
            if (empty($record)) {
                $count = count($list);
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 6
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 6]);
            }

            $deal_data = array_slice($list, 0, $page_size);
            if (empty($deal_data)) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 6]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }


            foreach ($deal_data as $key => $value) {
                //分类取负数
                $houniao = new Houniao();
                $goods_detail = $houniao->getGoodsDetailBySku($value['houniao_goods_sku_no']);
                if ($goods_detail['code'] == 0) {
                    $categoryIdPath = $goods_detail['data']['goodsDetail']['categoryIdPath'];
                    $category_id_arr = explode('_',$categoryIdPath);

                    $update_cate = [
                        'category_id' => - $goods_detail['data']['goodsDetail']['categoryId'],
                        'category_id_1' => isset($category_id_arr[0]) ? -$category_id_arr[0] : 0,
                        'category_id_2' => isset($category_id_arr[1]) ? -$category_id_arr[1] : 0,
                        'category_id_3' => isset($category_id_arr[2]) ? -$category_id_arr[2] : 0
                    ];
                    model('goods')->update($update_cate,['goods_id' => $value['goods_id']]);
                }
            }
            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    public function goodsBrand()
    {
        $list = model('goods')->getList(['source' => 1,'brand_id' => 0],'goods_id,brand_name');

        $record = model('sync_houniao')->getInfo(['type' => 12]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 100;
            if (empty($record)) {
                $count = count($list);
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 12
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 12]);
            }

            $deal_data = array_slice($list, 0, $page_size);
            if (empty($deal_data)) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 12]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }

            foreach ($deal_data as $key => $value) {
                $brand_info = model('goods_brand')->getInfo(['brand_name' => $value['brand_name']]);
                if (!empty($brand_info)) {
                    $brand_id = $brand_info['brand_id'];
                } else {
                    $info = model('goods_brand_houniao')->getInfo(['brand_name' => $value['brand_name']]);
                    $add_list = [
                        'brand_name' => $info['brand_name'],
                        'image_url' => $info['image_url'],
                        'source' => 1,
                        'create_time' => time(),
                        'hn_brand_id' => $info['hn_brand_id']
                    ];
                    $brand_id = model('goods_brand')->add($add_list);
                }
                if ($brand_id) {
                    model('goods')->update(['brand_id' => $brand_id],['goods_id'=> $value['goods_id']]);
                    model('goods_sku')->update(['brand_id' => $brand_id],['goods_id'=> $value['goods_id']]);
                }
            }
            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    public function goodsAddCategory()
    {
        $list = model('goods')->getList([['source','=',1],['category_id_1','<',0],['category_id_3','>',0]],'goods_id,category_id_1,category_id_2,category_id_3');

        $record = model('sync_houniao')->getInfo(['type' => 1]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 100;
            if (empty($record)) {
                $count = count($list);
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 1
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 1]);
            }

            $deal_data = array_slice($list, 0, $page_size);
            if (empty($deal_data)) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 1]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }

            foreach ($deal_data as $key => $value) {
                $info = model('goods_category')->getInfo(['category_id_3' => $value['category_id_3']]);
                if (empty($info)) {
                    Log::ERROR('【商品更新分类】');
                    Log::ERROR($value['goods_id']);
                    continue;
                }

                $update = [
                    'category_id_3' => $info['category_id_3'],
                    'category_id_2' => $info['category_id_2'],
                    'category_id_1' => $info['category_id_1'],
                    'category_name' => $info['category_name'],
                ];

                model('goods')->update($update,['goods_id' => $value['goods_id']]);
                model('goods_sku')->update($update,['goods_id' => $value['goods_id']]);
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    public function  addGoodsBug()
    {
        $record = model('sync_houniao')->getInfo(['type' => 5]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 20;
            if (empty($record)) {
                model('sync_houniao')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 5
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 5]);
            }
            $deal_data = model('zhai_bug')->pageList([], '*', 'id asc', $page, $page_size);

            if (empty($deal_data['list'])) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 5]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            $model = new Goods();
            foreach ($deal_data['list'] as $key => $value) {
                $res = $model->onGoods($value['goods_sku']);
                if ($res['code'] < 0) {
                    model('fix_fail')->add(['goods_sku' => $value['goods_sku']]);
                    //return $this->error($value['goods_sku'],'编辑候鸟商品出错');
                }
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    //所有库存跑一边
    public function allStock()
    {
        $record = model('sync_houniao')->getInfo(['type' => 3]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 110;
            if (empty($record)) {
                model('sync_houniao')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 3
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
            }
            $deal_data = model('goods')->pageList(['source' => 1], 'goods_id,houniao_goods_sku_no', 'goods_id asc', $page, $page_size);

            if (empty($deal_data['list'])) {
                $res = model('sync_houniao')->update(['status' => 1], ['type' => 3]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            $model = new Goods();
            foreach ($deal_data['list'] as $key => $value) {
                $model->adjustAllStock($value['goods_id'],$value['houniao_goods_sku_no']);
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    public function adjustAllStock($goods_id, $goodsSku)
    {
        $model = new Houniao();
        $detail_return = $model->getPrice($goodsSku);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }
        if (!empty($detail_return['data'])) {
            $stock = 0;
            $spec_sku_arr = [];
            foreach ($detail_return['data'] as $key => $specGroup) {
                foreach ($specGroup['specDetail'] as $k => $spec) {
                    $stock += $spec['stock'];
                    //model('goods_sku')->update(['stock' => $spec['stock']],['sku_no' => $spec['specSku']]);
                    //$spec_sku_arr[] = $spec['specSku'];
                }
            }
            //$info = model('goods')->getInfo(['houniao_goods_sku_no' => $goodsSku],'goods_stock_alarm,goods_id,goods_name,goods_stock');

            //不在候鸟接口里面的，表示候鸟那边已经删除
            // if (!empty($spec_sku_arr)) {
            //     model('goods_sku')->update(['stock' => 0],[['goods_id','=',$info['goods_id']],['sku_no','not in',$spec_sku_arr]]);
            // }

            $goods_update_data = [
                'goods_stock' => $stock,
            ];
            //小于警戒值，下架
            //if ($stock <= (int)$info['goods_stock_alarm']) {
            //$goods_update_data['goods_state'] = 0;
            // $add_goods_dynamic = [
            //     'goods_id' => $info['goods_id'],
            //     'goods_name' => $info['goods_name'],
            //     'goods_state' => 0,
            //     'value' => $stock,
            //     'remark' => '小于库存预警，下架',
            //     'status' => 0,
            //     'create_time' => time()
            // ];
            // model('goods_dynamic')->add($add_goods_dynamic);
            //model('goods_sku')->update(['goods_state' => 0],['goods_id' => $info['goods_id']]);
            //}
            model('goods')->update($goods_update_data, ['goods_id' => $goods_id]);
        }

        return $this->success();
    }

    public function zeroSku(){
        $list = model('zhai_bug')->pageList([],'*','id asc',1,100);
        foreach ($list['list'] as $key => $value) {
            model('goods_sku')->update(['stock' => 0],['goods_id' => $value['id']]);
        }
        return $this->success();
    }


    /**********************************************text****************************************************************/


    /**
     * 同步商品  type=4     按照商品分类独立添加更新数据
     * @return array
     */
    public function syncHouniaoGoods2()
    {
        $record = model('sync_houniao')->getInfo(['type' => 4]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }
        $model = new Houniao();
        $page_size = 150;
        model("goods")->startTrans();
        try {
            if (empty($record)) {
                //$return = $model->goodsList(1, $page_size);
                $return = $model->getGoodsListByCategoryId(32);
                model('sync_houniao')->add([
                    'total' => $return['data']['total'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 4
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 4]);
                $return = $model->goodsList($page, $page_size);
            }
            if ($return['code'] != 0) {
                return $this->error('', 'HOUNIAO_RETURN_ERROR');
            }
            if (empty($return['data']['goodsList'])) {
                model('sync_houniao')->update(['status' => 1], ['type' => 4]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }

            foreach ($return['data']['goodsList'] as $key => $value) {
                //HN1075520003  每
                //HN1075510083  女
                //if ($value['sku'] == 'HN10755601228') {
                $res = $this->addOneGoods($value);
                if ($res['code'] < 0) {
                    return $res;
                }
                //}
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }






    /**
     * 同步分类  type=3
     * @return array
     */
    public function syncHouniaoCategory2()
    {
        $model = new Houniao();
        $return = $model->getCategory();
        var_dump("<pre>");
        var_dump($return);
        exit;
        if ($return['code'] != 0) {
            return $this->error('', 'HOUNIAO_RETURN_ERROR');
        }

        $record = model('sync_houniao')->getInfo(['type' => 3]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("category_hn")->startTrans();
        try {
            $page_size = 2;
            if (empty($record)) {
                $count = count($return['data']);
                $page = 1;
                model('sync_houniao')->add([
                    'total' => $count,
                    'page' => $page,
                    'page_size' => $page_size,
                    'type' => 3,
                    'update_time' => time()
                ]);
            } else {
                $page = $record['page'] + 1;
                model('sync_houniao')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
            }

            $deal_data = array_slice($return['data'], $page_size * ($page - 1), $page_size);
            if (empty($deal_data)) {
                model('sync_houniao')->update(['status' => 1], ['type' => 3]);
                model('sync_houniao')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }

            foreach ($deal_data as $key => $value) {
                $res = $this->structureData($value, 0, 1);
                if ($res['code'] < 0) {
                    return $this->error('', '错误');
                }
            }
            model('category_hn')->commit();

            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('category_hn')->rollback();
            return $this->error('', $e->getMessage());
        }
    }



    /**********************************************************text************************************************************************/


      /**
    * 修改删除状态
    * @param $goods_ids
    * @param $is_delete
    * @param $site_id
    */
   public function modifyIsDelete()
   {
       $res=Db::query("SELECT    goods_id ,COUNT(houniao_goods_sku_no) AS b FROM   ns_goods  WHERE source=1  GROUP BY houniao_goods_sku_no   HAVING b>1
");
       $goods_ids="";
       if(!empty($res)){
           $arr2 = array_column($res, 'goods_id');
           $goods_ids=implode(",",$arr2);
           
           model('goods')->update(['is_delete' => 1], [['goods_id', 'in',  $goods_ids ]]);
           model('goods_sku')->update(['is_delete' => 1], [['goods_id', 'in', $goods_ids ]]);
       }
       return $this->success($goods_ids);
   }
   
   
   
   
/**
 * 添加商品 tradeType
 * @param $goodsSku
 * @return array
 */
public function getGoodsDetailBySku()
{
    
    $goodsSku="HN1075572060";
    $model = new Houniao();
    $detail_return = $model->getGoodsDetailBySku($goodsSku);
    var_dump("<pre>");
    var_dump($detail_return);
    exit;
    if ($detail_return['code'] != 0) {
        return $this->error('', 'HOUNIAO_RETURN_ERROR');
    }
    return $res;
}


      /**
    * 修改删除状态
    * @param $goods_ids
    * @param $is_delete
    * @param $site_id
    */
   public function modifyIsDelete2()
   {
       $res=Db::query("SELECT    goods_id FROM   ns_goods  WHERE source=1 and  site_id=0
");
       $goods_ids="";
       
       if(!empty($res)){
           $arr2 = array_column($res, 'goods_id');
           $goods_ids=implode(",",$arr2);
           model('goods')->update(['is_delete' => 1], [['goods_id', 'in',  $goods_ids ]]);
           model('goods_sku')->update(['is_delete' => 1], [['goods_id', 'in', $goods_ids ]]);
       }
       return $this->success($goods_ids);
   }






}