<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\houniao\model;

use app\model\BaseModel;
use app\model\system\Cron;
use think\facade\Log;

/**
 * 候鸟Api
 */
class Houniao extends BaseModel
{
    //接口文档  http://openapi.houniao.hk/

    // 测试环境URL
//    private $clientId    = '5604';
//
//    private $clientToken = 'df6f1e9a3d49720e19e2f3817ffe4066';
//
//    private $clientPayKey = 'df6f1e9a3d49720e19e2f3817ffe4066';
//
//    private $url = 'http://120.76.221.129:8001/api/route';



    // 生产环境
    private $clientId    = '20073';

    private $clientToken = 'njWCZbr1Vj0eSBDPxSxgXxitkWhBhyHV';

    private $clientPayKey = 'SWK6iVP2K8OYQWdNk12h615dm7elb5DG';

    private $url = 'https://www.houniao.hk/api/route';

    /*****************************************************地址接口*********************************************************/
    /**
     * 获取所有省市区
     * @return array
     */
    public function getRegions() {
        // 组装提交参数(数组)
        $params['method']      = 'base.getRegions';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }


    /*****************************************************商品接口*********************************************************/
    /**
     * 获取商品库存
     * @return array
     */
    public function getStock($goodsSku) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.getStock';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['goodsSku']    = $goodsSku;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     * 批量获取商品库存
     * @return array
     */
    public function getStocks($goodsSku) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.getStocks';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['goodsSku']    = $goodsSku;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     * 获取商品价格
     * @return array
     */
    public function getPrice($goodsSku) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.getPrice';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['goodsSku']    = $goodsSku;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     * 批量获取商品价格
     * @return array
     */
    public function getPrices($goodsSku) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.getPrices';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['goodsSku']    = $goodsSku;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     * 获取商品列表
     * @return array
     */
    public function goodsList($page = 1, $limit = 100) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.goodsList';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['page']        = $page;
        $params['isSale']        = 1;
        if ($limit != 0) {
            $params['limit'] = $limit;
        }

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     * 获取分类
     * @return array
     */
    public function getCategory(){
        // 组装提交参数(数组)
        $params['method']      = 'base.getCategory';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     * 获取品牌
     * @return array
     */
    public function getBrands(){
        // 组装提交参数(数组)
        $params['method']      = 'base.getBrands';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }


    /**
     * 通过分类ID获取商品列表
     * @return array
     */
    public function getGoodsListByCategoryId($categoryId) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.getGoodsListByCategoryId';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['page']        = 1;
        $params['categoryId']  = $categoryId;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }


    /**
     * 获取商品详情
     * @return array
     */
    public function getGoodsDetailBySku($goodsSku) {
        // 组装提交参数(数组)
        $params['method']      = 'goods.getGoodsDetailBySku';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['goodsSku']    = $goodsSku;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }


    public function subscribe($goodsSku)
    {
        // 组装提交参数(数组)
        $params['method']      = 'goods.subscribe';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['goodsSku']    = $goodsSku;

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }



    /*****************************************************订单接口*********************************************************/
    /**
     * 创建订单
     * @param $param
     * @return array|mixed
     */
    public function createOrder($param) {


        $new_data['time_new'] = date("Y-m-d H:i:s.u");
        $new_data['res_data'] = $param;
        error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../orderRefundStatusTheGoodsLOG.txt');



        //只有候鸟商品才走
        if (empty($param) || !isset($param['source']) || $param['source'] == 0) {
            return $this->error('','未找到订单数据或不属于候鸟订单');
        }

        $order_info = $param;
        $order_goods = model('order_goods')->getList(['order_id' => $order_info['order_id'],'source' => 1]);
        if (empty($order_goods)) {
            return $this->error('','未找到订单候鸟商品数据');
        }

        // 组装提交参数(数组 - 四维数组)
        $params['method']      = 'order.addOrder';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        $goods = [];
        foreach ($order_goods as $key => $value) {
            $goods[] = [
                'specSku' => $value['sku_no'],
                'goodsNum' => $value['num'],
            ];
        }
        $orders = [
            [
                'cusOrderNo'     => $order_info['out_trade_no'],
                'IDCard'         => $order_info['id_card'],
                'province'       => $order_info['province_id'],
                'city'           => $order_info['city_id'],
                'district'       => $order_info['district_id'],
                'areaId'         => $order_info['district_id'],
                'userAddress'    => $order_info['full_address'],
                'userName'       => empty($order_info['real_name'])?$order_info['name']:$order_info['real_name'],
                'userPhone'      => $order_info['mobile'],
                'deliverMoney'   => $order_info['delivery_money'],
                'orderRemarks'   => $order_info['buyer_message'],
                'goods'          => $goods,
            ],
        ];




        $params['orders'] = json_encode($orders);

        // 发起请求
        $result = $this->execute($params);
        if ($result['code'] == 0 && empty($result['errorOrders'])) {
            //记录候鸟订单号
            model('order')->update(['houniao_order_no' => $result['successOrders'][0]['orderItems'][0]['orderNo']],['order_id' => $order_info['order_id']]);

            //创建订单成功后，调用支付
            $return  = $this->queryBalance();
            Log::ERROR('【候鸟余额】'.$param['order_id']);
            Log::ERROR($return);
            $houniao_money = 0.00;
            if ($return['code'] == 0) {
                $houniao_money = $return['data']['money'];
            }
            $cron = new Cron();
            if ($order_info['order_money'] > $houniao_money) {
                //候鸟余额不够
                $execute_time = strtotime(date("Y-m-d 00:00:00", strtotime('+5 minute')));
                $cron->addCron(2, 1, "候鸟余额不足", "HouniaoBalance", $execute_time, $param['order_id']);
            }else{
                Log::ERROR('【候鸟支付参数】'.$result['successOrders'][0]['orderItems'][0]['orderNo']);
                $res = $this->payBalance($result['successOrders'][0]['orderItems'][0]['orderNo']);
                Log::ERROR('【候鸟支付返回】'.$param['order_id']);
                Log::ERROR($res);
                if ($res['code'] != 0) {
                    $execute_time = strtotime(date("Y-m-d 00:00:00", strtotime('+15 minute')));
                    $cron->addCron(2, 1, "候鸟余额支付错误", "HouniaoBalance", $execute_time, $param['order_id']);
                }
            }
        }
        if (!empty($result['errorOrders'])) {
            Log::ERROR('【创建订单参数】');
            Log::ERROR($orders);
            Log::ERROR('【创建订单返回】'.$param['order_id']);
            Log::ERROR($result);
            Log::ERROR('【候鸟创建订单出错】'.$param['order_id']);
        }
        return $result;
    }

    /**
     * 候鸟余额循环支付
     * @param $order_id
     */
    public function HouniaoBalance($order_id){
        $order_info = model('order')->getInfo(['order_id' => $order_id]);
        $return  = $this->queryBalance();
        $houniao_money = 0.00;
        if ($return['code'] == 0) {
            $houniao_money = $return['data']['money'];
        }
        if ($order_info['order_money'] <= $houniao_money) {
            $return = $this->payBalance($order_info['houniao_order_no']);
            if ($return['code'] == 0) {
                //删除定时任务
                $cron = new Cron();
                $cron->deleteCron([ [ 'event', '=', 'HouniaoBalance' ], [ 'relate_id', '=', $order_id ] ]);
            }
        }
    }


    /**
     * 取消订单
     * @param $param
     * @return array|mixed
     */
    public function cancel($param) {
        //只有候鸟商品才走
        $order_info = model('order')->getInfo(['order_id' => $param['order_id'],'source' => 1]);
        if (empty($order_info)) {
            return $this->error('','未找到订单数据');
        }

        // 组装提交参数(数组 - 四维数组)
        $params['method']      = 'order.cancel';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        $params['cusOrderNo'] = $order_info['out_trade_no'];
        $params['exceptReason'] = ''; //取消原因

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }

    /**
     *  查询订单状态
     * @param $param
     * @return array|mixed
     */
    public function queryOrderStatus($param) {
        $order_info = model('order')->getInfo(['order_id' => $param['order_id']]);
        if (empty($order_info)) {
            return $this->error('','未找到订单数据');
        }

        // 组装提交参数(数组 - 四维数组)
        $params['method']      = 'order.queryOrderStatus';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['cusOrderNo'] = $order_info['out_trade_no'];

        // 发起请求
        $result = $this->execute($params);
        return $result;
    }





    /*****************************************************快递物流接口*********************************************************/
    /**
     * 获取订单物流信息
     * @return array
     */
    public function getOrderExpress() {
        // 组装提交参数(数组)
        $params['method']      = 'order.getExpress';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;
        $params['cusOrderNo']  = '';

        // 发起请求
        $result = $this->execute($params);

        return $result;
    }

    /**
     * 获取快递
     * @return mixed
     */
    public function getExpress() {
        // 组装提交参数(数组)
        $params['method']      = 'base.getExpress';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        // 发起请求
        $result = $this->execute($params);

        return $result;
    }

    public function queryBalance()
    {
        // 组装提交参数(数组)
        $params['method']      = 'capitals.queryBalance';
        $params['clientId']    = $this->clientId;
        $params['clientToken'] = $this->clientToken;

        // 发起请求
        $result = $this->execute($params);

        return $result;
    }

    public function payBalance($order_no)
    {
        // 组装提交参数(数组)
        $params['service']      = 'pay.balance';
        $params['version']      = '1.0';
        $params['client_id']      = $this->clientId;
        $params['order_no']      = $order_no;
        $params['time']      = time();

        $sign = $this->getSignedQuery($params);
        $params['sign'] = $sign;
        Log::ERROR('【候鸟支付参数】');
        Log::ERROR($params);
        $data = $this->arrayToXml($params);
        Log::ERROR($data);
        // 发起请求
        $result = $this->payExecute($data);
        Log::ERROR('【候鸟支付返回】');
        Log::ERROR($data);
        return $result;
    }

    /**
     * 获取签名后的请求字符串
     *
     * @param array $params 参数数组
     * @return string
     */
    private function getSignedQuery($params) {

        ksort($params);
        $query = str_replace('+', '%20', http_build_query($params));
        $str = $query . '&key=' . $this->clientPayKey;
        $str = strtoupper(md5($str));
        return $str;
    }

    /**
     * 数组转xml
     * @param $arr
     * @return string
     */
    public function arrayToXml($arr){
        $xml = "<xml>";
        foreach ($arr as $key=>$val){
            if(is_array($val)){
                $xml.="<".$key.">".$this->arrayToXml($val)."</".$key.">";
            }else{
                if (is_numeric($val)){
                    $xml.="<".$key.">".$val."</".$key.">";
                }else{
                    $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
                }
            }
        }
        $xml.="</xml>";
        return $xml;
    }

    /**
     * @Author     : Jason
     * @Description: CURL请求
     * @LastModify : Jason
     * @param $params
     * @return mixed
     */
    public function execute($params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);
        return $response;
    }


    public function payExecute($params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, 'https://www.houniao.hk/pay/gateway');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);
        return $response;
    }

}