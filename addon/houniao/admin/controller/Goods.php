<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\houniao\admin\controller;

use addon\houniao\model\Goods as GoodsModel;
use app\admin\controller\BaseAdmin;

/**
 * 商品
 */
class Goods extends BaseAdmin
{
    /**
     * 同步候鸟分类
     * @return mixed
     */
    public function syncHouniaoCategory()
    {
        if (request()->isAjax()) {
            $model = new GoodsModel();
            return $model->syncHouniaoCategory();
        }
    }

    /**
     * 同步候鸟品牌
     * @return mixed
     */
    public function syncHouniaoBrand()
    {
        if (request()->isAjax()) {
            $model = new GoodsModel();
            return $model->syncHouniaoBrand();
        }
    }

}