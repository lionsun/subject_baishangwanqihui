<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\ladderdiscount\shop\controller;


use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\shop\ShopCategory;
use app\shop\controller\BaseShop;
use addon\ladderdiscount\model\Discount as DiscountModel;
use app\model\system\Promotion as PromotionModel;

/**
 * 满额折控制器
 */
class Discount extends BaseShop
{
	/**
	 * 添加活动
	 */
	public function add()
	{
		if (request()->isAjax()) {
			$data = [
				'discount_name' => input('discount_name', ''),
				'remark' => input('remark', ''),
				'start_time' => strtotime(input('start_time', '')),
				'end_time' => strtotime(input('end_time', '')),
				'site_id' => $this->site_id,
                'discount_content' => input('discount_content', ''),
                'discount_goods' => '',
			];
			
			$discount_model = new DiscountModel();
			return $discount_model->addDiscount($data);
		} else {
			return $this->fetch("discount/add");
		}
	}
	
	/**
	 * 编辑活动
	 */
	public function edit()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			$data = [
				'discount_name' => input('discount_name', ''),
				'remark' => input('remark', ''),
				'start_time' => strtotime(input('start_time', '')),
				'end_time' => strtotime(input('end_time', '')),
				'discount_id' => input('discount_id', 0),
				'site_id' => $this->site_id,
                'discount_content' => input('discount_content', ''),
			];
			
			return $discount_model->editDiscount($data);
			
		} else {
			$discount_id = input('discount_id', 0);
			$this->assign('discount_id', $discount_id);
			
			$discount_info = $discount_model->getDiscountInfo($discount_id, $this->site_id);
			$this->assign('discount_info', $discount_info['data']);
			
			return $this->fetch("discount/edit");
		}
		
	}
	
	/**
	 * 满额折详情
	 */
	public function detail()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			//活动商品
			$discount_id = input('discount_id', 0);
			$list = $discount_model->getNewDiscountGoods($discount_id);
			/*foreach ($list['data'] as $key => $val) {
				if ($val['price'] != 0) {
				    $discount_info = $discount_model->getDiscountInfo($discount_id,$this->site_id);
				    if(isset($discount_info["data"]["discount_content"]) && !empty($discount_info["data"]["discount_content"])){
				        $discount_content = json_decode($discount_info["data"]["discount_content"],true);
                        $discount_rate = $discount_content[0]['discount'] * 10;
                        $list['data'][ $key ]['discount_price'] = sprintf("%.2f",($val["price"] * $discount_rate / 100));
                    }else{
                        $discount_rate = 100;
                        $list['data'][ $key ]['discount_price'] = $val['price'];
                    }
				} else {
					$discount_rate = 100;
                    $list['data'][ $key ]['discount_price'] = $val['price'];
				}
				$list['data'][ $key ]['discount_rate'] = $discount_rate;
			}*/

			return $list;
		} else {
			$discount_id = input('discount_id', 0);
			$site_id = $this->site_id;
			$this->assign('discount_id', $discount_id);
			
			//活动详情
			$discount_info = $discount_model->getDiscountInfo($discount_id, $site_id);
			$this->assign('discount_info', $discount_info['data']);
			
			return $this->fetch("discount/detail");
		}
	}
	
	/**
	 * 活动列表
	 */
	public function lists()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$discount_name = input('discount_name', '');
			$status = input('status', '');
			
			$condition = [];
			if ($status !== "") {
				$condition[] = [ 'status', '=', $status ];
			}
			$condition[] = [ 'site_id', '=', $this->site_id ];
			$condition[] = [ 'discount_name', 'like', '%' . $discount_name . '%' ];
			$order = 'create_time desc';
			$field = 'discount_id, discount_name, start_time, end_time, status, create_time';
			
			$discount_status_arr = DiscountModel::getDiscountStatus();
			$res = $discount_model->getDiscountPageList($condition, $page, $page_size, $order, $field);
			foreach ($res['data']['list'] as $key => $val) {
				$res['data']['list'][ $key ]['status_name'] = $discount_status_arr[ $val['status'] ];
			}
			return $res;
			
		} else {
			//满额折状态
			$discount_status_arr = DiscountModel::getDiscountStatus();
			$this->assign('discount_status_arr', $discount_status_arr);
			
			return $this->fetch("discount/lists");
		}
	}
	
	/**
	 * 关闭活动
	 */
	public function close()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->closeDiscount($discount_id, $this->site_id);
		}
	}
	
	/**
	 * 删除活动
	 */
	public function delete()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->deleteDiscount($discount_id, $this->site_id);
		}
	}
	
	/**
	 * 满额折商品管理
	 */
	public function manage()
	{
		$discount_model = new DiscountModel();
		if (request()->isAjax()) {
			//满额折商品列表
			$discount_id = input('discount_id', 0);
			
			$res = $discount_model->getDiscountGoods($discount_id);
			return $res;
		} else {
			$discount_id = input('discount_id', 0);
			$this->assign('discount_id', $discount_id);
			
			//活动详情
			$discount_info = $discount_model->getDiscountInfo($discount_id, $this->site_id);
			$this->assign('discount_info', $discount_info['data']);
			
			return $this->fetch("discount/manage");
		}
	}
	
	/**
	 * 添加商品
	 */
	public function addGoods()
	{
		if (request()->isAjax()) {
			$goods_ids = input('goods_ids', '');
			$discount_id = input('discount_id', 0);
			$discount_model = new DiscountModel();
			return $discount_model->addDiscountGoods($discount_id, $this->site_id, $goods_ids);
		}
	}
	
	/**
	 * 删除商品
	 */
	public function deleteGoods()
	{
		if (request()->isAjax()) {
			$discount_id = input('discount_id', 0);
			$goods_id = input('goods_id', '');
			$discount_model = new DiscountModel();
			return $discount_model->deleteDiscountGoods($discount_id, $goods_id, $this->site_id);
		}
	}
}