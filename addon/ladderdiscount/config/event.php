<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //满额折开启
        'OpenLadderDiscount'  => [
            'addon\ladderdiscount\event\OpenDiscount',
        ],
        //满额折关闭
        'CloseLadderDiscount'  => [
            'addon\ladderdiscount\event\CloseDiscount',
        ],
        //展示活动
        'ShowPromotion' => [
            'addon\ladderdiscount\event\ShowPromotion',
        ],
        //检测相同时间段营销活动
        'CheckSameTimePromotion' => [
            'addon\ladderdiscount\event\CheckSameTimePromotion',
        ],
        //活动详情
        'PromotionInfo' => [
            'addon\ladderdiscount\event\PromotionInfo',
        ],
        //活动详情
        'PromotionPrice' => [
            'addon\ladderdiscount\event\PromotionPrice',
        ],
    ],

    'subscribe' => [
    ],
];
