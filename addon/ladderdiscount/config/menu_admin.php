<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'PROMOTION_LADDER_DISCOUNT',
        'title' => '满额折',
        'url' => 'ladderdiscount://admin/discount/lists',
        'parent' => 'PROMOTION_SHOP',
        'is_show' => 0,
        'is_control' => 0,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 101,
        'child_list' => [
            [
                'name' => 'PROMOTION_LADDER_DISCOUNT_CLOSE',
                'title' => '关闭活动',
                'url' => 'ladderdiscount://admin/discount/close',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_LADDER_DISCOUNT_DELETE',
                'title' => '删除活动',
                'url' => 'ladderdiscount://admin/discount/delete',
                'sort'    => 1,
                'is_show' => 0
            ],
            [
                'name' => 'PROMOTION_LADDER_DISCOUNT_DETAIL',
                'title' => '活动详情',
                'url' => 'ladderdiscount://admin/discount/detail',
                'sort'    => 1,
                'is_show' => 0
            ],
        
        ]
    ],
];
