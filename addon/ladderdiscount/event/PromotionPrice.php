<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\ladderdiscount\event;
use addon\ladderdiscount\model\Discount as DiscountModel;

/**
 * 活动展示
 */
class PromotionPrice
{

    /**
     * 活动详情
     * @param $sku_info
     */
	public function handle($sku_info)
	{
	    if(isset($sku_info['promotion_type']) && $sku_info['promotion_type'] == DiscountModel::PROMOTION_TYPE) {

	        if(isset($sku_info['promotion_info'])){
	            $discount_content = json_decode($sku_info['promotion_info']['info']['discount_content'], true);
	            $ladder_num = 0;
	            //最终要使用的折扣
	            $use_ladder = null;
	            foreach($discount_content as $ladder){
	                if($sku_info['num'] >= $ladder['num'] && $ladder['num'] > $ladder_num){
	                    $ladder_num = $ladder['num'];
                        $use_ladder = $ladder;
                    }
                }
	            if(!empty($use_ladder)){
                    $sku_info['discount_price'] = round($use_ladder['discount'] / 10 * $sku_info['price'], 2);
                    $sku_info['goods_money'] = $sku_info['price'] * $sku_info['num'];
                    $sku_info['real_goods_money'] = $sku_info['goods_money'];
                    $sku_info['promotion_money'] = ($sku_info['price'] - $sku_info['discount_price']) * $sku_info['num'];
                    return $sku_info;
	            }
            }
        }
	}
}