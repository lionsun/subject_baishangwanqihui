<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\zhongminonline\api\controller;

use addon\zhongminonline\model\Goods as GoodsModel;
use app\api\controller\BaseApi;
use addon\zhongminonline\model\zhongminonline;
use think\facade\Log;


class Test extends BaseApi
{
	public function getPrice()
	{


        $method = getMethodName(__METHOD__);
        $return = (new Zhongminzaixian())->$method('HN1075550548');

	    $a=array (
            'code' => 0,
            'successOrders' =>
                array (
                    0 =>
                        array (
                            'cusOrderNo' => '16099222114547',
                            'orderNos' =>
                                array (
                                    0 => '2021010649472',
                                ),
                            'orderItems' =>
                                array (
                                    0 =>
                                        array (
                                            'orderNo' => '2021010649472',
                                            'orderStatus' => -2,
                                            'isPay' => false,
                                            'goodsMoney' => 41,
                                            'deliverMoney' => '0.00',
                                            'discountMoney' => 0,
                                            'totalMoney' => 41,
                                            'invoiceMoney' => 0,
                                            'realTotalMoney' => 41,
                                            'goods_items' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'goodsSku' => 'HN10755010213',
                                                            'specSku' => 'HN10755010213_000000_1',
                                                            'goodsPrice' => '41.00',
                                                            'goodsSpecNum' => '1',
                                                            'goodsNum' => 1,
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
            'errorOrders' =>
                array (
                ),
        );
	    dump($a['successOrders'][0]['orderItems'][0]['orderNo']);die;





        return $this->response($return);

	}


	public function cancel()
    {
        $model = new Zhongminzaixian();
        $res = $model->cancel(['order_id' => 1]);
        dump($res);die;
    }

    public function execute($params) {



        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_URL, 'https://www.houniao.hk/pay/gateway');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response, true);
        return $response;
    }


    public function arrayToXml($arr){
        $xml = "<xml>";
        foreach ($arr as $key=>$val){
            if(is_array($val)){
                $xml.="<".$key.">".$this->arrayToXml($val)."</".$key.">";
            }else{
                if (is_numeric($val)){
                    $xml.="<".$key.">".$val."</".$key.">";
                }else{
                    //$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
                    $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
                }
            }
        }
        $xml.="</xml>";
        return $xml;
    }


    //更新缺少数据
    public function addDecGoods(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncZhongminonlineUpdateGoods();
        return $this->response($return);
    }

}