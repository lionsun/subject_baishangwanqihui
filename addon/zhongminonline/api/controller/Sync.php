<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\zhongminonline\api\controller;

use app\api\controller\BaseApi;
use addon\zhongminonline\model\Goods as GoodsModel;
use addon\zhongminonline\model\VirtualGoods as VirtualGoodsModel;


class Sync extends BaseApi
{
    /**
     * 同步品牌
     * @return false|string
     */
	public function syncZhongminonlineBrand()
	{
        set_time_limit(0);
	    $model = new GoodsModel();
        $return = $model->syncZhongminonlineBrand();
        return $this->response($return);
	}

    /**
     * 同步分类
     * @return false|string
     */
    public function syncZhongminonlineCategory()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncZhongminonlineCategory();
        return $this->response($return);
    }

    /**
     * 同步商品
     * @return false|string
     */
    public function syncZhongminonlineGoods()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncZhongminonlineGoods();
        return $this->response($return);
    }


    /**
     * 同步更新商品
     * @return false|string
     */
    public function syncZhongminonlineUpdateGoods()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncZhongminonlineUpdateGoods();
        return $this->response($return);
    }

    /**
     * 同步失败的商品
     * @return false|string
     */
    public function fixFail()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->syncfixFail();
        return $this->response($return);
    }

    /*========================================================================================*/

    /**
     * 补全商品的品牌
     * @return false|string
     */
    public function goodsBrand()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->goodsBrand();
        return $this->response($return);
    }

    /**
     * 补全商品中的分类
     */
    public function goodsAddCategory()
    {
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->goodsAddCategory();
        return $this->response($return);
    }


    public function addGoodsBug(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->addGoodsBug();
        return $this->response($return);
    }

    //所有商品的库存更新一边
    public function allStock(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->allStock();
        return $this->response($return);
    }

    public function zeroSku(){
        set_time_limit(0);
        $model = new GoodsModel();
        $return = $model->zeroSku();
        return $this->response($return);
    }






    /****************************************text************************************************************/


     //执行查重
     public function modifyIsDelete()
     {
         $model = new GoodsModel();
         $res = $model->modifyIsDelete();
         dump($res);die;
     }




    /****************************************虚拟商品测试************************************************************/

     //获取虚拟商品列表
    public function virtualGoodsList(){
        $model = new VirtualGoodsModel();
        $res = $model->syncVirtualGoodsList();
        var_dump($res);
    }

    //获取虚拟商品简单信息
    public function getVirtualGoodsInfo(){
        $model = new VirtualGoodsModel();
        $goods_id = '1001225';
        $res = $model->getGoodsInfo($goods_id);
        var_dump($res);
    }

    //获取虚拟商品信息
    public function getVirtualGoodsDetail(){
        $model = new VirtualGoodsModel();
        $goods_id = '1001225';
        $res = $model->getGoodsDetail($goods_id);
        var_dump($res);
    }

    //话费充值下单接口
    public function phoneRecharge(){
        $goods_id = '1001225';
        $param = [
            'customerOrderNo' => time().rand(0,1000),
            'goodsId'         => $goods_id,
            'account'         => '18635596744',
            'num'             => 1,
            'buyerIp'         => '127.0.0.1',
            'notify_url'      => 'www.baidu.com',
        ];
        $model = new VirtualGoodsModel();
        $res = $model->phoneRecharge($param);
        var_dump($res);
    }


    //订单状态查询
    public function orderQuery(){
        $param = [
            'customerOrderNo' => '1619581811127',
        ];
        $model = new VirtualGoodsModel();
        $res = $model->orderQuery($param);
        var_dump($res);
    }


    public function queryBalance(){
        $model = new VirtualGoodsModel();
        $res = $model->queryBalance();
        var_dump($res);
    }





}