<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\zhongminonline\api\controller;

use addon\zhongminonline\model\Goods;
use app\api\controller\BaseApi;
use app\model\order\Order as OrderModel;
use think\facade\Log;

/**
 * 提供中闽在线回调接口
 */
class Notify extends BaseApi
{


    /**
     *  订单创建  zhongminonline/api/Notify/createOrder
     */
    public function createOrder()
    {
        return    (new  \addon\zhongminonline\model\Zhongminonline)->createOrder(["order_id"=>$this->params['order_id']]);
    }





    /**
     *发起售后（直接退款模式）
     */
    public function orderRefundStatus1()
    {
        $res =  (new  \addon\zhongminonline\model\Zhongminonline)->orderRefundStatus1();
        return $this->response($res);
    }



    /**
     *发起售后（直接退货第一次处理）(查询是否通过售后)
     */
    public function orderRefundStatus()
    {
        $res =  (new  \addon\zhongminonline\model\Zhongminonline)->orderRefundStatus();
        return $this->response($res);
    }




    /**
     *发起售后（直接退货第2次处理） (查询售后是否收到货物)  zhongminonline/api/Notify/orderRefundTakeDelivery
     */
    public function orderRefundTakeDelivery()
    {
        $res =  (new  \addon\zhongminonline\model\Zhongminonline)->orderRefundTakeDelivery();
        return $this->response($res);
    }



    /**
     *发起售后（直接退货第3次处理） (查询售后是否完成)
     */
    public function orderRefundTransfer()
    {
        $res =  (new  \addon\zhongminonline\model\Zhongminonline)->orderRefundTransfer();
        return $this->response($res);
    }


    public function notify(){
        $notify_param = request()->post();
        $new_data['time_new']=date("Y-m-d H:i:s.u");
        $new_data['res_data']=$notify_param;
        $new_data['method_text']="数据";
        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../ALL_LOG.txt');
//        $param = json_decode($notify_param, true);
//        if($param['code']==200){
//            if($param['data']['type']=='GOODS'){
//                if (empty($param['data']['sku_id']) && empty($param['data']['good_id'])) {
//                    return 'error';
//                }
//                switch($param['data']['event']){
//                    case 'AGENT_PRICE':  //价格调整
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="价格调整";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../AGENT_PRICE_LOG.txt');
//                        $model = new Goods();
//                        $res = $model->adjustPriceByGoodsId($param['data']['goods_id']);
//                        if ($res['code'] < 0) {
//                            $info = model('zmonline_fail')->getInfo(['zhongmin_goods_id' => $param['data']['goods_id']]);
//                            if (empty($info)) {
//                                model('zmonline_fail')->add(['zhongmin_goods_id' => $param['data']['goods_id']]);
//                            }
//                        }
//                    case 'SOLD_OUT':    //商品下架
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="商品下架";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../SOLD_OUT_LOG.txt');
//                        $model = new Goods();
//                        $res = $model->offGoods($param['data']['goods_id']);
//                        if ($res['code'] < 0) {
//                            $info = model('zmonline_fail')->getInfo(['zhongmin_goods_id' => $param['data']['goods_id']]);
//                            if (empty($info)) {
//                                model('zmonline_fail')->add(['zhongmin_goods_id' => $param['data']['goods_id']]);
//                            }
//                        }
//                    case 'PUTAWAY':     //商品上架
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="商品上架";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../PUTAWAY_LOG.txt');
//                        $model = new Goods();
//                        $res = $model->onGoods($param['data']['goods_id']);
//                        if ($res['code'] < 0) {
//                            $info = model('zmonline_fail')->getInfo(['zhongmin_goods_id' => $param['data']['goods_id']]);
//                            if (empty($info)) {
//                                model('zmonline_fail')->add(['zhongmin_goods_id' => $param['data']['goods_id']]);
//                            }
//                        }
//                }
//            }elseif($param['data']['type']=='ORDER'){
//                if (empty($param['data']['order_no'])) {
//                    return 'error';
//                }
//                switch($param['data']['event']){
//                    case 'CANCELED':  //订单取消
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="订单取消";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../CANCELED_LOG.txt');
//                        $order_info = model('order')->getInfo(['out_trade_no' => $param['data']['order_no']],'order_id,site_id');
//                        //订单完成后，系统自动完成订单（并进行结算操作）
//                        $order_model = new OrderModel();
//                        $result = $order_model->orderClose($order_info['order_id']);
//                        if ($result['code'] < 0) {
//                            Log::ERROR('【订单关闭】');
//                            Log::ERROR($param['data']['order_no']);
//                            Log::ERROR($result);
//                        }
//
//                    case 'WAIT_SEND': //订单待发货
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="订单待发货";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../WAIT_SEND_LOG.txt');
//                    case 'REVOKE':    //订单驳回
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="订单驳回";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../REVOKE_LOG.txt');
//                        //驳回订单后,需要退款
//                        $order_info = model('order')->getInfo(['out_trade_no' => $param['data']['order_no']],'order_id,site_id');
//                        //订单关闭
//                        $order_model = new OrderModel();
//                        $result = $order_model->orderClose($order_info['order_id']);
//                        if ($result['code'] < 0) {
//                            Log::ERROR('【订单驳回】');
//                            Log::ERROR($param['data']['order_no']);
//                            Log::ERROR($result);
//                        }
//
//                    case 'DELIVER':  //订单发货
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="订单发货";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../DELIVER_LOG.txt');
//                        //对订单进行发货操作
//                        $order_info = model('order')->getInfo(['out_trade_no' => $param['data']['order_no']],'order_id,site_id');
//                        //查goods_id
//                        $goods_info = model('goods')->getInfo(['zhongmin_goods_id' => $param['data']['goods_id']],'goods_id');
//                        //通过goods_id查所有sku_id
//                        $goods_sku = model('goods_sku')->getColumn(['goods_id' => $goods_info['goods_id']],'zhongmin_goods_sku_id');
//                        //通过sku_id查购买了哪些商品的order_goods_id
//                        $order_goods_ids = model('order_goods')->getColumn([['order_id','=',$order_info['order_id']],['zhongmin_goods_sku_id','in',$goods_sku]],'order_goods_id');
//
//                        $data = array(
//                            "type" => 'manual',//发货方式（手动发货）
//                            "order_goods_ids" => implode(',',$order_goods_ids),
//                            "express_company_id" => 0,
//                            "delivery_no" => 0,
//                            "order_id" => $order_info['order_id'],
//                            "delivery_type" => 1,
//                            "site_id" => $order_info['site_id'],
//                            "template_id" => 0//电子面单模板id
//                        );
//                        $order_model = new OrderModel();
//                        $result = $order_model->orderGoodsDelivery($data);
//                        if ($result['code'] < 0) {
//                            Log::ERROR('【订单发货】');
//                            Log::ERROR($param['data']['order_no']);
//                            Log::ERROR($result);
//                        }
//                    case 'CLOSED':   //订单关闭
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="订单关闭";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../CLOSED_LOG.txt');
//                        $order_info = model('order')->getInfo(['out_trade_no' => $param['data']['order_no']],'order_id,site_id');
//                        //订单关闭
//                        $order_model = new OrderModel();
//                        $result = $order_model->orderClose($order_info['order_id']);
//                        if ($result['code'] < 0) {
//                            Log::ERROR('【订单关闭】');
//                            Log::ERROR($param['data']['order_no']);
//                            Log::ERROR($result);
//                        }
//                    case 'FINISHED':  //订单完成
//                        $new_data['time_new']=date("Y-m-d H:i:s.u");
//                        $new_data['res_data']=$param;
//                        $new_data['method_text']="订单完成";
//                        error_log(print_r($new_data,1),3,dirname(__FILE__).'../../FINISHED_LOG.txt');
//                        $order_info = model('order')->getInfo(['out_trade_no' => $param['data']['order_no']],'order_id,site_id');
//                        //订单完成后，系统自动完成订单（并进行结算操作）
//                        $order_model = new OrderModel();
//                        $result = $order_model->orderComplete($order_info['order_id']);
//                        if ($result['code'] < 0) {
//                            Log::ERROR('【订单完成】');
//                            Log::ERROR($param['data']['order_no']);
//                            Log::ERROR($result);
//                        }
//                }
//            }else{
//                Log::ERROR('【数据错误】');
//                Log::ERROR($param);
//            }
//        }
        return 'SUCCESS';
    }

}