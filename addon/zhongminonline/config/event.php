<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //中闽在线支付余额不足
        'ZhongminonlineBalance' => [
            'addon\zhongminonline\event\ZhongminonlineBalance'
        ],
        //订单支付异步执行
        'OrderPay' => [
            'addon\zhongminonline\event\CreateZhongminonlineOrder'
        ],
        //发起售后
        'ZhongminonlineOrderRefundApply' => [
            'addon\zhongminonline\event\ZhongminonlineOrderRefundApply',
        ],
        //中闽填写回寄物流单号
        'ZhongminRefundDelivery' => [
            'addon\zhongminonline\event\ZhongminRefundDelivery',
        ],
    ],

    'subscribe' => [
    ],
];
