<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\zhongminonline\model;

use app\model\BaseModel;
use app\model\goods\VirtualGoods as VirtualGoodsModel;
use think\facade\Log;
use think\facade\Db;

/**
 * 商品
 */
class VirtualGoods extends BaseModel
{



    //虚拟商品列表   type=104
    public function syncVirtualGoodsList()
    {
        $record = model('sync_zhongminonline')->getInfo(['type' => 104]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }
        $model = new Virtual();
        $page_size = 40;
        model("goods")->startTrans();
        try {
            if (empty($record)) {
                $return = $model->goodsList(1,$page_size);
                model('sync_zhongminonline')->add([
                    'total' => $return['data']['count'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 104
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 104]);
                $return = $model->goodsList($page, $page_size);
            }
            if ($return['code'] != 200) {
                return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
            }
            if (empty($return['data']['goods_list'])) {
                model('sync_zhongminonline')->update(['status' => 1], ['type' => 104]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }
            foreach ($return['data']['goods_list'] as $key => $value) {
                $res = $this->addOneGoods($return['data']['goods_list'][$key]);
                if ($res['code'] < 0) {
                    return $res;
                }
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }

    }


    public function addOneGoods($goods_info){
        $site_id = 24;
        $category_id = $category_id_1 = $category_id_2 = $category_id_3 = 0;
        $category_name = '';
        $goods_sku_data[0] = [
            'sku_id'          => $goods_info['id'],
            'sku_name'        => $goods_info['name'],
            'spec_name'       => '',
            'sku_no'          => $goods_info['id'],
            'website_id'      => 0,
            'sku_spec_format' => '',
            'price'           => $goods_info['parvalue'],
            'market_price'    => $goods_info['parvalue'],
            'cost_price'      => $goods_info['purchase_price'] ,
            'stock'           => $goods_info['stock'],
            'sku_image'       => $goods_info['logo'],
            'sku_images'      => $goods_info['logo']
        ];
        $data = [
            'goods_name' => $goods_info['name'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => $site_id,
            'category_id' => $category_id,
            'category_id_1' => $category_id_1,
            'category_id_2' => $category_id_2,
            'category_id_3' => $category_id_3,
            'category_name' => $category_name,
            'brand_id' => 0,
            'brand_name' => '',
            'goods_image' => $goods_info['logo'],
            'goods_content' => $goods_info['description'],
            'goods_state' => 1,
            'price' => $goods_info['parvalue'],
            'market_price' => $goods_info['parvalue'],
            'cost_price' => $goods_info['purchase_price'],
            'sku_no' => '',
            'virtual_indate' => 0,
            'goods_stock' => $goods_info['stock'],
            'goods_stock_alarm' => 0 ,
            'goods_spec_format' => '',
            'goods_attr_format' => '',
            'introduction' => $goods_info['description'],
            'keywords' => '',
            'unit' => '',
            'sort' => 0,
            'commission_rate' => '',
            'video_url' => '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',
            'goods_shop_category_ids' => '',
            'supplier_id' => 0,
            'max_buy' => 0,
            'min_buy' => 0,
            'zhongmin_goods_type' => $goods_info['type'],
            'source' => 3,   //中闽虚拟商品
            'zhongmin_goods_id' => $goods_info['id'],
        ];
        $goods_model = new VirtualGoodsModel();
        $res = $goods_model->addGoods($data);
        if ($res['code'] < 0) {
            return $this->error($goods_info['id']);
        }
    }


    public function getGoodsInfo($goods_id){
        $model = new Virtual();
        $return = $model->goodsInfo($goods_id);
        if ($return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        return $this->success($return);
    }

    public function getGoodsDetail($goods_id){
        $model = new Virtual();
        $return = $model->goodsDetail($goods_id);
        if ($return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        return $this->success($return);
    }

    public function phoneRecharge($param){
        $model = new Virtual();
        $return = $model->cardRecharge($param);
        if ($return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        return $this->success($return);
    }

    public function orderQuery($param){
        $model = new Virtual();
        $return = $model->orderQuery($param);
        if ($return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        return $this->success($return);
    }

    public function queryBalance(){
        $model = new Virtual();
        $return = $model->queryBalance();
        if ($return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        return $this->success($return);
    }






        






}