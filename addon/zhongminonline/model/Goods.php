<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\zhongminonline\model;

use app\model\BaseModel;
use app\model\goods\Goods as GoodsModel;
use think\facade\Log;
use think\facade\Db;

/**
 * 商品
 */
class Goods extends BaseModel
{

    public  $delevery_time = [
        'TIME24' => '24小时内发货',
        'TIME48' => '48小时内发货'
    ];


    public  $servicePromise = [
        'FREE_DELIVERY' => '支持七天无理由退货',
        'PROMISE10' => '假一赔十',
        'NO_FREE_DELIVERY' => '不支持七天无理由退货'
    ];


    /**
     * 同步品牌  type=0
     * @return array
     */
    public function syncZhongminonlineBrand()
    {
        $model = new Zhongminonline();
        $record = model('sync_zhongminonline')->getInfo(['type' => 0]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods_brand_zhongminonline")->startTrans();
        try {
            $page_size = 300;
            if (empty($record)) {
                $page = 1;
                $return = $model->getBrands($page,$page_size);
                if ($return['code'] != 200) {
                    return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
                }
                $count = count($return['data']);
                model('sync_zhongminonline')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 0
                ]);
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 0]);
                $return = $model->getBrands($page, $page_size);
            }
            $deal_data = $return['data'];
            if (empty($deal_data)) {
                $res = model('sync_zhongminonline')->update(['status' => 1], ['type' => 0]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            foreach ($deal_data['list'] as $key => $value) {
                $add_list[] = [
                    'brand_name' => $value['name'],
                    'image_url' => $value['icon'],
                    'create_time' => time(),
                    'source' => 3,
                    'zm_brand_id' => $value['id'],
                    'description' => $value['description']
                ];
            }
            if (!empty($add_list)) {
                $res = model('goods_brand_zhongminonline')->addList($add_list);
                if (empty($res)) {
                    return $this->error('', $page . '失败' . $res);
                }
            }

            model('goods_brand_zhongminonline')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods_brand_zhongminonline')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 同步分类  type=3
     * @return array
     */
    public function syncZhongminonlineCategory()
    {
        $model = new Zhongminonline();
        $return = $model->categoryList();
        if ($return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }

        $record = model('sync_zhongminonline')->getInfo(['type' => 3]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("category_zmonline")->startTrans();
        try {
            $page_size = 2;
            if (empty($record)) {
                $count = count($return['data']);
                $page = 1;
                model('sync_zhongminonline')->add([
                    'total' => $count,
                    'page' => $page,
                    'page_size' => $page_size,
                    'type' => 3,
                    'update_time' => time()
                ]);
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
            }

            $deal_data = array_slice($return['data'], $page_size * ($page - 1), $page_size);
            if (empty($deal_data)) {
                model('sync_zhongminonline')->update(['status' => 1], ['type' => 3]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }

            foreach ($deal_data as $key => $value) {
                $res = $this->structureData($value, 0, 1);
                if ($res['code'] < 0) {
                    return $this->error('', '错误');
                }
            }
            model('category_zmonline')->commit();

            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('category_zmonline')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 分类递归操作
     * @param $data
     * @param $pid
     * @param $level
     */
    public function structureData($data, $pid, $level)
    {
        if ($level > 3) {
            return $this->success();
        }
        //父级分类信息
        if ($pid == 0) {
            $category_full_name = $data['name'];
            $category = [
                'category_id_1' => 0,
                'category_id_2' => 0,
                'category_id_3' => 0
            ];
        } else {
            $pid_info = model('category_zmonline')->getInfo(['category_id' => $pid]);
            if (empty($pid_info)) {
                return $this->error('', '根据pid没有查到父级分类信息95行');
            }
            $category_full_name = $pid_info['category_full_name'] . '/' . $data['name'];
            $category = [
                'category_id_1' => $pid_info['category_id_1'],
                'category_id_2' => $pid_info['category_id_2'],
                'category_id_3' => $pid_info['category_id_3'],
            ];
        }

        $id = model('category_zmonline')->add([
            'category_name' => $data['name'],
            'pid' => $pid,
            'level' => $level,
            'category_id_1' => $category['category_id_1'],
            'category_id_2' => $category['category_id_2'],
            'category_id_3' => $category['category_id_3'],
            'category_full_name' => $category_full_name,
            'source' => 1,
            'zmonline_category_id' => $data['id']
        ]);
        if (empty($id)) {
            return $this->error('', '添加分类出错124行');
        }
        model('category_zmonline')->update(['category_id_' . $level => $id], ['category_id' => $id]);

        if (!empty($data['children'])) {
            foreach ($data['children'] as $key => $value) {
                $this->structureData($value, $id, $level + 1);
            }
        }
    }

    /**
     * 同步商品  type=4
     * @return array
     */
    public function syncZhongminonlineGoods()
    {
        $record = model('sync_zhongminonline')->getInfo(['type' => 4]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }
        $model = new zhongminonline();
        $page_size = 40;
        model("goods")->startTrans();
        try {
            if (empty($record)) {
                $return = $model->goodsList(1, $page_size);
                model('sync_zhongminonline')->add([
                    'total' => $return['data']['total'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 4
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 4]);
                $return = $model->goodsList($page, $page_size);
            }
            if ($return['code'] != 200) {
                return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
            }
            if (empty($return['data']['list'])) {
                model('sync_zhongminonline')->update(['status' => 1], ['type' => 4]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }
            foreach ($return['data']['list'] as $key => $value) {
                $res = $this->addOneGoods($return['data']['list'][$key]);
                if ($res['code'] < 0) {
                    return $res;
                }
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 同步商品  type=4
     * @return array
     */
    public function syncZhongminonlineUpdateGoods()
    {
        $record = model('sync_zhongminonline')->getInfo(['type' => 100]);  //更新商品
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已更新过');
        }
        $model = new zhongminonline();
        $page_size = 400;
        model("goods")->startTrans();
        try {
            if (empty($record)) {
                $return = $model->goodsList(1, $page_size);
                model('sync_zhongminonline')->add([
                    'total' => $return['data']['total'],
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 100
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 4]);
                $return = $model->goodsList($page, $page_size);
            }
            if ($return['code'] != 200) {
                return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
            }
            if (empty($return['data']['list'])) {
                model('sync_zhongminonline')->update(['status' => 1], ['type' => 4]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => 1]);
            }
            foreach ($return['data']['list'] as $key => $value) {
                $goods_model = new GoodsModel();
                $isexist = $goods_model->getGoodsInfo(['zhongmin_goods_id'=>$return['data']['list'][$key]['id']]);
                if($isexist['data']){
                    //只更新商品详情图片
                    $res = $this->editGoodsDetailImage($return['data']['list'][$key]['id'],$return['data']['list'][$key]);
                    //$res = $this->editOneGoods($return['data']['list'][$key]['id'],$return['data']['list'][$key]);
                }else{
                    $res = $this->addOneGoods($return['data']['list'][$key]);
                }
                if ($res['code'] < 0) {
                    return $res;
                }
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    /**
     * 添加商品
     * @param $base_data
     * @return array
     */
    public function addOneGoods($base_data)
    {
        //商品详情处理  循环将图片上传
        $description = htmlspecialchars_decode($base_data['detailContent']);
        $detailImage = '';
        if (!empty($base_data['detailImages'])) {
            foreach ($base_data['detailImages'] as $k => $v) {
                $detailImage .= "<p><img src=".$v."></p>";
            }
        }
        $description = $detailImage.$description;
        $goods_spec_format = $goods_sku_data = [];
        $stock = $goods_price = $goods_market_price = $goods_cost_price = 0;
        if(!empty($base_data['skuList'])){
            $sku = $base_data['skuList'];
            $group = [];
            $specList = [];
            if(!empty($sku[0]['specList'])){
                foreach($sku[0]['specList'] as $k=>$v){
                    $usec = (float)explode(" ", microtime())[0];
                    $msec = round($usec * 1000);
                    $spec_id = -(count($sku) - 1 + getdate()['seconds'] + $msec);
                    $group[$k]['spec_id'] = $spec_id-$k;
                    $group[$k]['spec_name'] = $v['name'];
                    $group[$k]['value'] = [];
                }
                foreach($sku as $key=>$val){
                    $specList = array_merge($specList,$val['specList']);
                }
                $specList = array_unique_fb($specList);
                foreach($specList as $key=>$val){
                    foreach($group as $k=>$v){
                        if($val['name'] == $group[$k]['spec_name']){
                            $spec_value_id = -(abs($v['spec_id']) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($sku[0]['specList']) + rand(100,200);
                            $group[$k]['value'][] = [
                                'spec_id' => $v['spec_id'],
                                'spec_name' => $v['spec_name'],
                                'spec_value_id' => $spec_value_id,
                                'spec_value_name' => $val['value'],
                                'image' =>  $val['image'],
                            ];
                        }
                    }
                }
                $goods_spec_format = $group;
                foreach($sku as $k => $v){
                    $sku[$k]['specDetail'] = [];
                    foreach($v['specList'] as $kk=>$vv){
                        foreach($group as $group_k => $group_v){
                            if($vv['name'] == $group_v['spec_name']){
                                foreach($group_v['value'] as $group_v_k => $group_v_v){
                                    if($vv['value'] == $group_v_v['spec_value_name']){
                                        $sku[$k]['specList'][$kk]['spec_id'] = $group[0]['spec_id'];
                                        $sku[$k]['specList'][$kk]['spec_name'] =$group[0]['spec_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_id'] = $group_v_v['spec_id'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_name'] = $group_v_v['spec_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_value_id'] = $group_v_v['spec_value_id'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_value_name'] = $group_v_v['spec_value_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['image'] = $vv['image'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($sku as $k => $v) {
            $stock += intval($v['stock']);
            $price = $market_price = 0;
            if ((float)$v['agentPrice'] != 0) {
                //销售价
                $price = round((float)$v['agentPrice']*((1.3-1)*0.13/1.13+1.3));
                //市场价
                $market_price = round((float)$v['marketPrice']);
            }

            if ($k == 0) {
                $goods_cost_price = $v['agentPrice'];
                $goods_market_price = $market_price;
                $goods_price = $price;
            }
            $goods_sku_data[] = [
                'spec_name' =>  isset($v['specList'][0]['spec_name'])?$v['specList'][0]['spec_name']:'',
                'zhongmin_goods_sku_id' => $v['skuId'],
                'zhongmin_goods_id' => $base_data['id'],
                'sku_no' => $v['code'],
                'price' => $price,
                'market_price' => $market_price,
                'cost_price' => $v['agentPrice'],
                'stock' => $v['stock'],
                'weight' => (float)$v['weight']/1000,
                'volume' => '',
                'start_exp_time' => 0,
                'end_exp_time' =>  0,
                'delivery_money_area' => '',//花运费地区
                'delivery_money' => 0, //运费
                'no_delivery_area' => '', //不配送地区
                'sku_image' => $base_data['coverImage'],
                'sku_images' => implode(',',$base_data['photos']),
                'sku_images_arr' => $base_data['photos'],
                'sku_spec_format' => isset($v['specDetail']['value'])?$v['specDetail']['value']:''
            ];
        }
        $category_id_arr = explode(',',$base_data['categoryIds']);

        $site_id = 24;
        $category_id = 0;
        $category_id_1 = isset($category_id_arr[0]) ? $category_id_arr[0]:0;
        $category_id_2 = isset($category_id_arr[1]) ? $category_id_arr[1]:0;
        $category_id_3 = isset($category_id_arr[2]) ? $category_id_arr[2]:0;
        if($category_id_1!=0){
            $category_id = $category_id_1;
        }
        if($category_id_2!=0){
            $category_id = $category_id_2;
        }
        if($category_id_3!=0){
            $category_id = $category_id_3;
        }
        $category_info = model('category_zmonline')->getInfo(['zmonline_category_id'=>$category_id]);
        if($category_info){
            $category_id_1 = $category_info['ns_category_id_1'];
            $category_id_2 = $category_info['ns_category_id_2'];
            $category_id_3 = $category_info['ns_category_id_3'];
            $category_name = $category_info['category_name'];
            if (!empty($category_info['ns_category_id_1'])) {
                $category_id = $category_info['ns_category_id_1'];
            }
            if (!empty($category_info['ns_category_id_2'])) {
                $category_id = $category_info['ns_category_id_2'];
            }
            if (!empty($category_info['ns_category_id_3'])) {
                $category_id = $category_info['ns_category_id_3'];
            }
        }
        $data = [
            'goods_name' => $base_data['name'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => $site_id,
            'website_id' => 0,
            'category_id' => $category_id,
            'category_id_1' => $category_id_1,
            'category_id_2' => $category_id_2,
            'category_id_3' => $category_id_3,
            'category_name' => $category_name,
            'brand_id'    => $base_data['brandId'],
            'brand_name'  => $base_data['brandName'],
            'goods_image' => implode(',',$base_data['photos']),
            'goods_content' => $description,
            'goods_state' => 1,
            'price' => $goods_price,
            'market_price' => $goods_market_price,
            'cost_price' => $goods_cost_price,
            'sku_no' => !empty($sku) ? $sku[0]['code'] : '',
            'weight' => !empty($sku) ? $sku[0]['weight'] : '',
            'volume' => 0,
            'goods_stock' => $stock,
            'goods_stock_alarm' => 0,
            'is_free_shipping' => 1,
            'shipping_template' => 0,
            'goods_spec_format' => !empty($goods_spec_format) ? json_encode($goods_spec_format, JSON_UNESCAPED_UNICODE) : '',
            'goods_attr_format' => '',
            'introduction' => '',
            'keywords' => $base_data['sellingPoint'],
            'unit' => '',
            'sort' => 0,
            'commission_rate' => 0,
            'video_url' => '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',
            'goods_shop_category_ids' => '',
            'supplier_id' => 0,
            'source' => 3,
            'zhongmin_goods_id' => $base_data['id'],
            'zhongmin_goods_sku_id' => $base_data['skuList'][0]['skuId'],
            'trade_type' => 0,
            'max_buy' => 0,
            'min_buy' => 0,
            'deliver_time' => $this->delevery_time[$base_data['deliverTime']],
            'service_promise' => json_encode($base_data['servicePromise']),
        ];
        $goods_model = new GoodsModel();
        $res = $goods_model->addGoods($data);
        if ($res['code'] < 0) {
            return $this->error($base_data['id']);
        }
        return $this->success();
    }


    /**
     * 添加商品
     * @param $goodsSku
     * @return array
     */
    public function addGoods($goods_id)
    {
        $model = new Zhongminonline();
        $detail_return = $model->getGoodsDetailByGoodsId($goods_id);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        //中闽在线下架的直接删除
        $info = model('goods')->getInfo(['zhongmin_goods_id' => $goods_id],'goods_id');
//        if ($detail_return['data']['goodsDetail']['isSale'] == 0) {
//            if (!empty($info)) {
//                model('goods')->delete(['goods_id' => $info['goods_id']]);
//                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
//            }
//            return $this->success();
//        }

        if (!empty($info)) {
            $sku_info = model('goods_sku')->getInfo(['goods_id' =>$info['goods_id'] ]);
            if (!empty($sku_info)) {
                //更新商品
                $res = $this->editOneGoods($goods_id, $detail_return['data']['list'][0]);
                return $res;
            }else{
                //中闽在线下架商品查不到价格接口，导致没有保存到sku的，删除之前的goods重新添加
                model('goods')->delete(['goods_id' => $info['goods_id'] ]);
            }
        }

        $res = $this->addOneGoods($detail_return['data']['list'][0]);
        return $res;
    }

    /**
     * 更新商品
     * @param $goodsSku
     * @return array
     */
    public function editGoods($goods_id)
    {
        $model = new Zhongminonline();
        $detail_return = $model->getGoodsDetailByGoodsId($goods_id);
        if ($detail_return['code'] != 0) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }

        $info = model('goods')->getInfo(['zhongmin_goods_id' => $goods_id],'goods_id');
        if (empty($info)) {
            return $this->error('', '未找到商品数据');
        }

        //中闽在线下架的直接删除
//        if ($detail_return['data']['goodsDetail']['isSale'] == 0) {
//            if (!empty($info)) {
//                model('goods')->delete(['goods_id' => $info['goods_id']]);
//                model('goods_sku')->delete(['goods_id' => $info['goods_id']]);
//            }
//            return $this->success();
//        }

        //更新商品
        $res = $this->editOneGoods($goods_id, $detail_return['data']['list'][0]);
        return $res;
    }


    public function editGoodsDetailImage($goods_id, $base_data){
        //商品详情处理
        model('goods')->startTrans();
        $description = htmlspecialchars_decode($base_data['detailContent']);
        $detailImage = '';
        if (!empty($base_data['detailImages'])) {
            foreach ($base_data['detailImages'] as $k => $v) {
                $detailImage .= "<p><img src=".$v."></p>";
            }
        }
        $description = $detailImage.$description;
        //更新表数据
        $data['goods_content'] = $description;
        $condition = ['zhongmin_goods_id' => $goods_id];
        model('goods')->update($data,$condition);
        model('goods_sku')->update($data,$condition);
        model('goods')->commit();
        return $this->success($goods_id);
    }
    /**
     * 编辑商品
     * @param $goodsSku
     * @param $base_data
     * @return array
     */
    public function editOneGoods($goods_id, $base_data)
    {
        $goods_info = model('goods')->getInfo(['zhongmin_goods_id' => $goods_id]);
        if (empty($goods_info)) {
            return $this->error('','未找到商品数据');
        }

        $goods_sku = model('goods_sku')->getList(['goods_id' => $goods_info['goods_id']]);

        //已经保存的所有中闽在线sku
        $sku_no_item =[];
        foreach ($goods_sku as $key => $value) {
            if (!empty($value['zhongmin_goods_sku_id'])) {
                $sku_no_item[$value['zhongmin_goods_sku_id']] = $value['zhongmin_goods_sku_id'];
            }
        }
        //商品详情处理
        $description = htmlspecialchars_decode($base_data['detailContent']);
        $detailImage = '';
        if (!empty($base_data['detailImages'])) {
            foreach ($base_data['detailImages'] as $k => $v) {
                $detailImage .= "<p><img src=".$v."></p>";
            }
        }
        $description = $detailImage.$description;

        $goods_spec_format = $goods_sku_data = [];
        $stock = $goods_price = $goods_market_price = $goods_cost_price = 0;
        if(!empty($base_data['skuList'])){
            $sku = $base_data['skuList'];
            $group = [];
            $specList = [];
            if(!empty($sku[0]['specList'])){
                foreach($sku[0]['specList'] as $k=>$v){
                    $usec = (float)explode(" ", microtime())[0];
                    $msec = round($usec * 1000);
                    $spec_id = -(count($sku) - 1 + getdate()['seconds'] + $msec);
                    $group[$k]['spec_id'] = $spec_id-$k;
                    $group[$k]['spec_name'] = $v['name'];
                    $group[$k]['value'] = [];
                }
                foreach($sku as $key=>$val){
                    $specList = array_merge($specList,$val['specList']);
                }
                $specList = array_unique_fb($specList);
                foreach($specList as $key=>$val){
                    foreach($group as $k=>$v){
                        if($val['name'] == $group[$k]['spec_name']){
                            $spec_value_id = -(abs($v['spec_id']) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($sku[0]['specList']) + rand(100,200);
                            $group[$k]['value'][] = [
                                'spec_id' => $v['spec_id'],
                                'spec_name' => $v['spec_name'],
                                'spec_value_id' => $spec_value_id,
                                'spec_value_name' => $val['value'],
                                'image' =>  $val['image'],
                            ];
                        }
                    }
                }
                $goods_spec_format = $group;
                foreach($sku as $k => $v){
                    $sku[$k]['specDetail'] = [];
                    foreach($v['specList'] as $kk=>$vv){
                        foreach($group as $group_k => $group_v){
                            if($vv['name'] == $group_v['spec_name']){
                                foreach($group_v['value'] as $group_v_k => $group_v_v){
                                    if($vv['value'] == $group_v_v['spec_value_name']){
                                        $sku[$k]['specList'][$kk]['spec_id'] = $group[0]['spec_id'];
                                        $sku[$k]['specList'][$kk]['spec_name'] =$group[0]['spec_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_id'] = $group_v_v['spec_id'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_name'] = $group_v_v['spec_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_value_id'] = $group_v_v['spec_value_id'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_value_name'] = $group_v_v['spec_value_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['image'] = $vv['image'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($sku as $k => $v) {
            $stock += intval($v['stock']);
            $price = $market_price = 0;
            if ((float)$v['agentPrice'] != 0) {
                //销售价
                $price = round((float)$v['agentPrice']*((1.3-1)*0.13/1.13+1.3));
                //市场价
                $market_price = round((float)$v['marketPrice']);
            }

            if ($k == 0) {
                $goods_cost_price = $v['agentPrice'];
                $goods_market_price = $market_price;
                $goods_price = $price;
            }
            $goods_sku_data[] = [
                'spec_name' =>  isset($v['specList'][0]['spec_name'])?$v['specList'][0]['spec_name']:'',
                'zhongmin_goods_sku_id' => $v['skuId'],
                'zhongmin_goods_id' => $base_data['id'],
                'sku_no' => $v['code'],
                'price' => $price,
                'market_price' => $market_price,
                'cost_price' => $v['agentPrice'],
                'stock' => $v['stock'],
                'weight' => (float)$v['weight']/1000,
                'volume' => '',
                'start_exp_time' => 0,
                'end_exp_time' =>  0,
                'delivery_money_area' => '',//花运费地区
                'delivery_money' => 0, //运费
                'no_delivery_area' => '', //不配送地区
                'sku_image' => $base_data['coverImage'],
                'sku_images' => implode(',',$base_data['photos']),
                'sku_images_arr' => $base_data['photos'],
                'sku_spec_format' => isset($v['specDetail']['value'])?$v['specDetail']['value']:''
            ];
        }
        if(!empty($base_data['skuList'])){
            $sku = $base_data['skuList'];
            $group = [];
            $specList = [];
            if(!empty($sku[0]['specList'])){
                foreach($sku[0]['specList'] as $k=>$v){
                    $usec = (float)explode(" ", microtime())[0];
                    $msec = round($usec * 1000);
                    $spec_id = -(count($sku) - 1 + getdate()['seconds'] + $msec);
                    $group[$k]['spec_id'] = $spec_id-$k;
                    $group[$k]['spec_name'] = $v['name'];
                    $group[$k]['value'] = [];
                }
                foreach($sku as $key=>$val){
                    $specList = array_merge($specList,$val['specList']);
                }
                $specList = array_unique_fb($specList);
                foreach($specList as $key=>$val){
                    foreach($group as $k=>$v){
                        if($val['name'] == $group[$k]['spec_name']){
                            $spec_value_id = -(abs($v['spec_id']) + getdate()['seconds'] + round(explode(" ", microtime())[0] * 1000)) + count($sku[0]['specList']) + rand(100,200);
                            $group[$k]['value'][] = [
                                'spec_id' => $v['spec_id'],
                                'spec_name' => $v['spec_name'],
                                'spec_value_id' => $spec_value_id,
                                'spec_value_name' => $val['value'],
                                'image' =>  $val['image'],
                            ];
                        }
                    }
                }
                $goods_spec_format = $group;
                foreach($sku as $k => $v){
                    $sku[$k]['specDetail'] = [];
                    foreach($v['specList'] as $kk=>$vv){
                        foreach($group as $group_k => $group_v){
                            if($vv['name'] == $group_v['spec_name']){
                                foreach($group_v['value'] as $group_v_k => $group_v_v){
                                    if($vv['value'] == $group_v_v['spec_value_name']){
                                        $sku[$k]['specList'][$kk]['spec_id'] = $group[0]['spec_id'];
                                        $sku[$k]['specList'][$kk]['spec_name'] =$group[0]['spec_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_id'] = $group_v_v['spec_id'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_name'] = $group_v_v['spec_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_value_id'] = $group_v_v['spec_value_id'];
                                        $sku[$k]['specDetail']['value'][$kk]['spec_value_name'] = $group_v_v['spec_value_name'];
                                        $sku[$k]['specDetail']['value'][$kk]['image'] = $vv['image'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($sku as $k => $v) {
            $stock += intval($v['stock']);
            $price = $market_price = 0;
            if ((float)$v['agentPrice'] != 0) {
                //销售价
                $price = round((float)$v['agentPrice']*((1.3-1)*0.13/1.13+1.3));
                //市场价
                $market_price = round((float)$v['marketPrice']);
            }

            if ($k == 0) {
                $goods_cost_price = $v['agentPrice'];
                $goods_market_price = $market_price;
                $goods_price = $price;
            }
            $goods_sku_data[] = [
                'spec_name' =>  isset($v['specList'][0]['spec_name'])?$v['specList'][0]['spec_name']:'',
                'zhongmin_goods_sku_id' => $v['skuId'],
                'zhongmin_goods_id' => $base_data['id'],
                'sku_no' => $v['code'],
                'price' => $price,
                'market_price' => $market_price,
                'cost_price' => $v['agentPrice'],
                'stock' => $v['stock'],
                'weight' => (float)$v['weight']/1000,
                'volume' => '',
                'start_exp_time' => 0,
                'end_exp_time' =>  0,
                'delivery_money_area' => '',//花运费地区
                'delivery_money' => 0, //运费
                'no_delivery_area' => '', //不配送地区
                'sku_image' => $base_data['coverImage'],
                'sku_images' => implode(',',$base_data['photos']),
                'sku_images_arr' => $base_data['photos'],
                'sku_spec_format' => isset($v['specDetail']['value'])?$v['specDetail']['value']:''
            ];
        }
        $category_id_arr = explode(',',$base_data['categoryIds']);

        $site_id = 24;
        $category_id = 0;
        $category_id_1 = isset($category_id_arr[0]) ? $category_id_arr[0]:0;
        $category_id_2 = isset($category_id_arr[1]) ? $category_id_arr[1]:0;
        $category_id_3 = isset($category_id_arr[2]) ? $category_id_arr[2]:0;
        if($category_id_1!=0){
            $category_id = $category_id_1;
        }
        if($category_id_2!=0){
            $category_id = $category_id_2;
        }
        if($category_id_3!=0){
            $category_id = $category_id_3;
        }
        $category_info = model('category_zmonline')->getInfo(['zmonline_category_id'=>$category_id]);
        if($category_info){
            $category_id_1 = $category_info['ns_category_id_1'];
            $category_id_2 = $category_info['ns_category_id_2'];
            $category_id_3 = $category_info['ns_category_id_3'];
            $category_name = $category_info['category_name'];
            if (!empty($category_info['ns_category_id_1'])) {
                $category_id = $category_info['ns_category_id_1'];
            }
            if (!empty($category_info['ns_category_id_2'])) {
                $category_id = $category_info['ns_category_id_2'];
            }
            if (!empty($category_info['ns_category_id_3'])) {
                $category_id = $category_info['ns_category_id_3'];
            }
        }
        $data = [
            'goods_id' => $goods_info['goods_id'],
            'goods_name' => $base_data['name'],
            'goods_attr_class' => 0,
            'goods_attr_name' => '',
            'site_id' => $site_id,
            'website_id' => 0,
            'category_id' => $category_id,
            'category_id_1' => $category_id_1,
            'category_id_2' => $category_id_2,
            'category_id_3' => $category_id_3,
            'category_name' => $category_name,
            'brand_id'    => $base_data['brandId'],
            'brand_name'  => $base_data['brandName'],
            'goods_image' => implode(',',$base_data['photos']),
            'goods_content' => $description,
            'goods_state' => 1,
            'price' => $goods_price,
            'market_price' => $goods_market_price,
            'cost_price' => $goods_cost_price,
            'sku_no' => !empty($sku) ? $sku[0]['code'] : '',
            'weight' => !empty($sku) ? $sku[0]['weight'] : '',
            'volume' => 0,
            'goods_stock' => $stock,
            'goods_stock_alarm' => 0,
            'is_free_shipping' => 1,
            'shipping_template' => 0,
            'goods_spec_format' => !empty($goods_spec_format) ? json_encode($goods_spec_format, JSON_UNESCAPED_UNICODE) : '',
            'goods_attr_format' => '',
            'introduction' => '',
            'keywords' => $base_data['sellingPoint'],
            'unit' => '',
            'sort' => 0,
            'commission_rate' => 0,
            'video_url' => '',
            'goods_sku_data' => !empty($goods_sku_data) ? json_encode($goods_sku_data, JSON_UNESCAPED_UNICODE) : '',
            'goods_shop_category_ids' => '',
            'supplier_id' => 0,
            'source' => 3,
            'zhongmin_goods_id' => $base_data['id'],
            'zhongmin_goods_sku_id' => $base_data['skuList'][0]['skuId'],
            'trade_type' => 0,
            'max_buy' => 0,
            'min_buy' => 0,
            'deliver_time' => $this->delevery_time[$base_data['deliverTime']],
            'service_promise' => json_encode($base_data['servicePromise']),
        ];
        $goods_model = new GoodsModel();
        $res = $goods_model->editGoods($data, 1);
        if ($res['code'] < 0) {
            return $this->error();
        }
        return $this->success();
    }


    /**
     * 上架商品
     * @param $goodsSku
     * @return array
     */
    public function onGoods($goods_id)
    {
        $model = new Zhongminonline();
        $detail_return = $model->getGoodsDetailByGoodsId($goods_id);
        if ($detail_return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }
        if(!isset($detail_return['data']['list'][0]) || empty($detail_return['data']['list'][0])){
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }else{
            $goods_detail = $detail_return['data']['list'][0];
        }
        //中闽在线下架的直接删除
//        $info = model('goods')->getInfo(['zhongmin_goods_id' => $goods_id],'goods_id');
//        if (!empty($info)) {
//            //更新商品
//            $res = $this->editOneGoods($goods_id, $goods_detail);
//            var_dump($res);die;
//        } else {
            $res = $this->addOneGoods($goods_detail);
//        }

        return $res;
    }
    /**
     * 下架商品
     * @param $goodsSku
     * @return array
     */
    public function offGoods($goods_id)
    {
        //删除商品
        $goods_info = model('goods')->getInfo(['zhongmin_goods_id' => $goods_id], 'goods_id');
        if (!empty($goods_info)) {
            $res = model('goods')->update(['goods_state'=> 0],['goods_id' => $goods_info['goods_id']]);
            $res = model('goods_sku')->update(['goods_state'=> 0],['goods_id' => $goods_info['goods_id']]);
        }
        return $this->success();
    }

    /**
     * 根据goods_id调整价格
     * @param $goodsSku
     * @return array
     */
    public function adjustPriceByGoodsId($goods_id)
    {
        $model = new Zhongminonline();
        $detail_return = $model->getGoodsDetailByGoodsId($goods_id);
        if ($detail_return['code'] != 200) {
            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
        }

        //中闽在线下架的直接删除
        $info = model('goods')->getInfo(['zhongmin_goods_id' => $goods_id],'goods_id');
        if (empty($info)) {
            return $this->error('', '未找到商品数据');
        }
        $res = $this->editOneGoods($goods_id, $detail_return['data']['list'][0]);
        return $res;
    }


    /**
     * 店铺分类对应
     * @param $category_id
     * @return int|string
     */
    public function shopCategory($category_id)
    {
        $info = model('category_zmonline')->getInfo(['level' => 3, 'zmonline_category_id' => $category_id],'site_id');
        $site_id = 0;
        if (!empty($info)) {
            $site_id = $info['site_id'];
        }
        return $site_id;


        //平台店铺id  => 中闽在线三级分类id
        $arr = [
            '2' => [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 956, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 954, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 948, 246, 248, 249, 250, 253, 254, 255, 584, 847, 588, 589, 590, 591, 592, 593, 594, 596, 597, 598, 599, 600, 601, 602, 604, 605, 606, 607, 608, 609, 610, 611, 850, 957, 613, 614, 615, 617, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 632, 634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 645, 646, 647, 1105, 1106, 1107, 829, 1033, 1035, 1037, 258, 259, 260, 261, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 293, 294, 295, 296, 297, 298, 300, 301, 302, 303, 304, 305, 306, 751, 752, 753,754, 755, 756, 757, 759, 760, 761, 762, 763, 764, 765, 766, 767, 769, 770, 771, 772, 773, 774, 776, 777, 778, 779, 780, 781, 782, 783, 785, 786, 967,1338,],  //百商万企汇自营超市
            '3' => [31, 32, 37, 38, 39, 40, 41, 43, 44, 45, 46, 48, 49, 50, 51, 52, 53, 54,55, 56, 58, 59, 60, 61, 62, 63, 64, 65, 840, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 78, 79, 80, 81, 82, 83, 84, 85, 87, 88, 90, 91, 92, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 691, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 574, 575, 576, 577, 578, 969,],  //百商万企汇母婴自营店
            '4' => [94, 95, 668, 1120, 1121, 1122, 1123, 1124, 1125, 1126, 1127, 1128, 1132, 1133, 1134, 922, 1108, 1109, 1110, 1111, 1112, 1113, 1114, 1115, 1116, 1117, 1118, 1119, 1129, 1130, 1131, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 975, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 972, 973, 349, 350, 351, 352, 353, 354, 355, 356, 357, 974, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 977, 978, 979, 980, 981, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 976, 498, 499, 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519, 521, 522, 523, 524, 525, 526, 527, 528, 529, 530, 531, 532, 533, 534, 535, 536, 537, 538, 539, 540, 541, 542, 543, 544, 545, 546, 547, 982],  //百商万企汇服饰自营店
            '5' => [111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 1027, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170,171,984, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 996, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1012, 1013, 1014, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 394],  //百商万企汇家电自营店
            '6' => [174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,839,843,844,845,846,195,196,197,198,199,200,201,202,797,949,34,35,36,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,222,223,224,225,226,227,228,229,230,832,841,842,232,233,234,235,236,237,238,239,240,241,242,243,799,800,801,802,803,804,805,806,807,808,809,810,811,812,813,814,815],  //百商万企汇大健康自营店
            '7' => [309, 310, 311, 312, 313, 314, 315, 316, 319, 320, 321, 941, 1085, 1086, 1087, 1088, 1089, 1090, 1091, 1092, 1071, 1072, 1073, 1074, 1075, 1076, 1077, 1078, 1038, 1039, 1040, 1041, 1042, 1052, 1053, 1054, 1055, 1056, 1057, 1058, 943, 944, 945, 946, 1051, 942, 1046, 1047, 1048, 1049, 1050, 1059, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1070, 1080, 1081, 1082, 939, 1083, 1084, 935, 936, 937, 938, 1043, 1044, 1045, 940, 1093, 1095, 947],  //百商万企汇钟表首饰自营店
            '8' => [360,361,362,363,364,365,366,367,830,1104,1031],  //百商万企汇办公用品自营店
            '9' => [370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 952, 971,],  //百商万企汇户外运动自营店
            '10' => [384, 385, 386, 387, 388, 390, 391, 392, 393, 395, 669, 397, 398, 399, 400, 401, 402, 403, 404, 405, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 648, 418, 419, 420, 421, 422, 423, 425, 426, 427, 428, 429, 430, 431, 432, 650, 651, 652, 653, 654, 655, 656, 657, 659, 660, 661, 662, 663, 664, 665, 666, 667, 673, 674, 675, 676, 677, 678, 679, 680, 681, 682, 683, 685, 686, 687, 688, 689, 690, 788, 789, 790, 791, 792, 793, 794, 795, 796, 834, 835, 836, 837, 838, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 735, 736, 737, 738, 739, 740, 741, 742, 743, 744, 745, 746, 747, 748,1336,1335,1169,],  //百商万企汇家居家装自营店
            '11' => [856,857,859,862,865,866,868,871,891,892,893,894,895,896,897,898,899,900,901,902,903,904,905,906,907,908,909,1029,551,],  //百商万企汇宠物生活自营店
            '12' => [694, 695, 696, 697, 698, 699, 700, 701, 702, 703, 704, 706, 707, 708, 709, 710, 711, 712, 713, 715, 716, 717, 718, 719, 720, 721, 831, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 1025,],  //百商万企汇手机数码自营店
            '13' => [951, 921, 953, 955, 950, 919, 920, 1258, 1259, 1215, 1229, 1231, 1307, 1332, ],  //百商万企汇汽车用品自营店
        ];
        if (!empty($category_id)) {
            foreach ($arr as $key => $value) {
                if (in_array($category_id, $value)) {
                    return $key;
                }
            }
        }
        return 0;
    }

    /**
     * 需要运费地区
     */
    public function deliverMoneyArea()
    {
        //内蒙古自治区、西藏自治区、甘肃省、青海省、宁夏回族自治区、新疆维吾尔自治区
        $pid = [150000,540000,620000,630000,640000,650000];
        //二级
        $ids = model('area')->getColumn([['pid','in',$pid]],'id');
        $idss = [];
        if (!empty($ids)) {
            //三级
            $idss = model('area')->getColumn([['pid','in',$ids]],'id');
        }
        return array_merge($pid, $ids, $idss);
    }

    /**
     * 同步半夜失败的
     * @return array
     */
    public function syncfixFail()
    {
        $deal_data = model('fix_fail')->pageList([],'*','id asc',1,5);
        if (empty($deal_data['list'])) {
            return $this->error('', '无失败数据');
        }
        $ids = [];
        foreach ($deal_data['list'] as $key => $value) {
            $model = new Goods();
            $res = $model->onGoods($value['goods_sku']);
            if ($res['code'] >= 0) {
                $ids[] = $value['id'];
            }
        }
        model('fix_fail')->delete([['id','in',$ids]]);
        return $this->success();
    }



    public function goodsBrand()
    {
        $list = model('goods')->getList(['source' => 3,'brand_id' => 0],'goods_id,brand_name');

        $record = model('sync_zhongminonline')->getInfo(['type' => 12]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 100;
            if (empty($record)) {
                $count = count($list);
                model('sync_zhongminonline')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 12
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 12]);
            }

            $deal_data = array_slice($list, 0, $page_size);
            if (empty($deal_data)) {
                $res = model('sync_zhongminonline')->update(['status' => 1], ['type' => 12]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }

            foreach ($deal_data as $key => $value) {
                $brand_info = model('goods_brand')->getInfo(['brand_name' => $value['brand_name']]);
                if (!empty($brand_info)) {
                    $brand_id = $brand_info['brand_id'];
                } else {
                    $info = model('goods_brand_zhongminonline')->getInfo(['brand_name' => $value['brand_name']]);
                    $add_list = [
                        'brand_name' => $info['brand_name'],
                        'image_url' => $info['image_url'],
                        'source' => 1,
                        'create_time' => time(),
                        'hn_brand_id' => $info['hn_brand_id']
                    ];
                    $brand_id = model('goods_brand')->add($add_list);
                }
                if ($brand_id) {
                    model('goods')->update(['brand_id' => $brand_id],['goods_id'=> $value['goods_id']]);
                    model('goods_sku')->update(['brand_id' => $brand_id],['goods_id'=> $value['goods_id']]);
                }
            }
            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }

    public function goodsAddCategory()
    {
        $list = model('goods')->getList([['source','=',1],['category_id_1','<',0],['category_id_3','>',0]],'goods_id,category_id_1,category_id_2,category_id_3');

        $record = model('sync_zhongminonline')->getInfo(['type' => 1]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 100;
            if (empty($record)) {
                $count = count($list);
                model('sync_zhongminonline')->add([
                    'total' => $count,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 1
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 1]);
            }

            $deal_data = array_slice($list, 0, $page_size);
            if (empty($deal_data)) {
                $res = model('sync_zhongminonline')->update(['status' => 1], ['type' => 1]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }

            foreach ($deal_data as $key => $value) {
                $info = model('goods_category')->getInfo(['category_id_3' => $value['category_id_3']]);
                if (empty($info)) {
                    Log::ERROR('【商品更新分类】');
                    Log::ERROR($value['goods_id']);
                    continue;
                }

                $update = [
                    'category_id_3' => $info['category_id_3'],
                    'category_id_2' => $info['category_id_2'],
                    'category_id_1' => $info['category_id_1'],
                    'category_name' => $info['category_name'],
                ];

                model('goods')->update($update,['goods_id' => $value['goods_id']]);
                model('goods_sku')->update($update,['goods_id' => $value['goods_id']]);
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    public function  addGoodsBug()
    {
        $record = model('sync_zhongminonline')->getInfo(['type' => 5]);
        if ($record['status'] == 1) {
            return $this->error('', '该类数据之前已导过');
        }

        model("goods")->startTrans();
        try {
            $page_size = 20;
            if (empty($record)) {
                model('sync_zhongminonline')->add([
                    'total' => 0,
                    'page' => 1,
                    'page_size' => $page_size,
                    'type' => 5
                ]);
                $page = 1;
            } else {
                $page = $record['page'] + 1;
                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 5]);
            }
            $deal_data = model('zhai_bug')->pageList([], '*', 'id asc', $page, $page_size);

            if (empty($deal_data['list'])) {
                $res = model('sync_zhongminonline')->update(['status' => 1], ['type' => 5]);
                model('sync_zhongminonline')->commit();
                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
            }
            $model = new Goods();
            foreach ($deal_data['list'] as $key => $value) {
                $res = $model->onGoods($value['goods_sku']);
                if ($res['code'] < 0) {
                    model('fix_fail')->add(['goods_sku' => $value['goods_sku']]);
                }
            }

            model('goods')->commit();
            return $this->success(['page' => $page, 'page_size' => $page_size]);
        } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error('', $e->getMessage());
        }
    }


    //所有库存跑一边
//    public function allStock()
//    {
//        $record = model('sync_zhongminonline')->getInfo(['type' => 3]);
//        if ($record['status'] == 1) {
//            return $this->error('', '该类数据之前已导过');
//        }
//
//        model("goods")->startTrans();
//        try {
//            $page_size = 110;
//            if (empty($record)) {
//                model('sync_zhongminonline')->add([
//                    'total' => 0,
//                    'page' => 1,
//                    'page_size' => $page_size,
//                    'type' => 3
//                ]);
//                $page = 1;
//            } else {
//                $page = $record['page'] + 1;
//                model('sync_zhongminonline')->update(['page' => $page, 'update_time' => time()], ['type' => 3]);
//            }
//            $deal_data = model('goods')->pageList(['source' => 1], 'goods_id,zhongminonline_goods_sku_no', 'goods_id asc', $page, $page_size);
//
//            if (empty($deal_data['list'])) {
//                $res = model('sync_zhongminonline')->update(['status' => 1], ['type' => 3]);
//                model('sync_zhongminonline')->commit();
//                return $this->success(['page' => $page, 'page_size' => $page_size, 'status' => $res]);
//            }
//            $model = new Goods();
//            foreach ($deal_data['list'] as $key => $value) {
//                $model->adjustAllStock($value['goods_id'],$value['zhongminonline_goods_sku_no']);
//            }
//
//            model('goods')->commit();
//            return $this->success(['page' => $page, 'page_size' => $page_size]);
//        } catch (\Exception $e) {
//            model('goods')->rollback();
//            return $this->error('', $e->getMessage());
//        }
//    }
//
//    public function adjustAllStock($goods_id, $goodsSku)
//    {
//        $model = new Zhongminonline();
//        $detail_return = $model->getPrice($goodsSku);
//        if ($detail_return['code'] != 0) {
//            return $this->error('', 'ZHONGMINONLINE_RETURN_ERROR');
//        }
//        if (!empty($detail_return['data'])) {
//            $stock = 0;
//            foreach ($detail_return['data'] as $key => $specGroup) {
//                foreach ($specGroup['specDetail'] as $k => $spec) {
//                    $stock += $spec['stock'];
//                }
//            }
//            $goods_update_data = [
//                'goods_stock' => $stock,
//            ];
//            model('goods')->update($goods_update_data, ['goods_id' => $goods_id]);
//        }
//
//        return $this->success();
//    }


    /**
    * 修改删除状态
    * @param $goods_ids
    * @param $is_delete
    * @param $site_id
    */
   public function modifyIsDelete()
   {
       $res=Db::query("SELECT    goods_id ,COUNT(zhongminonline_goods_sku_no) AS b FROM   ns_goods  WHERE source=1  GROUP BY zhongminonline_goods_sku_no   HAVING b>1
");
       $goods_ids="";
       if(!empty($res)){
           $arr2 = array_column($res, 'goods_id');
           $goods_ids=implode(",",$arr2);
           
           model('goods')->update(['is_delete' => 1], [['goods_id', 'in',  $goods_ids ]]);
           model('goods_sku')->update(['is_delete' => 1], [['goods_id', 'in', $goods_ids ]]);
       }
       return $this->success($goods_ids);
   }


   public function virtualGoodsList(){
       
   }





}