<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\zhongminonline\model;


use app\model\BaseModel;
use app\model\order\OrderRefund as OrderRefundModel;
use think\facade\Log;

/**
 * 中闽在线Api
 */
class zhongminonline extends BaseModel
{
    //接口文档  https://www.showdoc.cc/838635587788728?page_id=4566962470576632

    //测试环境
    //    private $appkey = '1801079';
    //
    //    private $appsecret = '954126B2ADA0FF45E044AAFF6743F561';
    //
    //    private $url = 'http://gongxiao.iwankeji.com';


    // 生产环境
    private $appkey = '1802380';

    private $appsecret = '4419C075C93DA82AB1F97A88510AB0BA';

    private $url = 'http://gx.zhongminzaixian.com';

    /*****************************************************公共接口*********************************************************/

    /**
     * 生成签名
     * @return array
     */

    public function getSign($data)
    {
        $str = '';
        ksort($data);
        foreach ($data as $key => $val) {
            $str .= $key . '=' . $val . '&';
        }
        $last_str = strtolower(substr($str, 0, -1)) . $this->appsecret;
        $sign = md5($last_str);
        return $sign;
    }

    /**
     * 获取品牌
     * @return array
     */
    public function getBrands($page = 1, $page_size = 10, $brand_id = '', $brand_name = '')
    {
        // 组装提交参数(数组)
        $param_url = '/api/agent/brandList';
        $last_url = $this->url . $param_url;
        $common_data = [
            'appkey' => $this->appkey
        ];
        $service_data = [
            'id' => $brand_id,
            'name' => $brand_name,
            'page' => $page,
            'pageSize' => $page_size
        ];
        $data = array_merge($common_data, $service_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        $result = $this->execute($data, $last_url);
        return $result;
    }



    /*****************************************************商品接口*********************************************************/


    /**
     * 获取所有商品分类列表
     * @return array
     */
    public function categoryList()
    {
        $param_url = '/api/agent/categoryList';
        $last_url = $this->url . $param_url;
        $common_data = [
            'appkey' => $this->appkey
        ];
        $sign = $this->getSign($common_data);
        $common_data['sign'] = $sign;
        $result = $this->execute($common_data, $last_url);
        return $result;
    }


    /**
     * 获取商品列表
     * @return array
     */
    public function goodsList($page = 1, $page_size = 10, $goodsIds = '', $goodsName = '')
    {
        $param_url = '/api/agent/goodsList';
        $last_url = $this->url . $param_url;
        $common_data = [
            'appkey' => $this->appkey
        ];
        $service_data = [
            'goodsIds' => $goodsIds,
            'goodsName' => $goodsName,
            'page' => $page,
            'pageSize' => $page_size
        ];
        $data = array_merge($common_data, $service_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /*****************************************************订单创建*********************************************************/


    /**
     * 创建订单
     * @param $param
     * @return array|mixed
     */
    public function createOrder($param)
    {

        return $this->error('', '未找到订单数据或不属于中闽在线订单11');



        $param = model('order')->getInfo(['order_id' => $param['order_id']]);
        //只有中闽在线商品才走
        if (empty($param) || !isset($param['source']) || $param['source'] != 3) {
            return $this->error('', '未找到订单数据或不属于中闽在线订单');
        }
        $order_info = $param;
        $order_goods = model('order_goods')->getList(['order_id' => $order_info['order_id'], 'source' => 3]);
        if (empty($order_goods)) {
            return $this->error('', '未找到订单中闽在线商品数据');
        }
        $param_url = '/api/agent/createOrder';
        $last_url = $this->url . $param_url;
        $common_data = [
            'appkey' => $this->appkey
        ];
        foreach ($order_goods as $key => $value) {
            $full_address = explode("-", $order_info['full_address']);
            $zhongminonline_goodsId = model('goods')->getInfo(['goods_id' => $value['goods_id']], "zhongmin_goods_id")["zhongmin_goods_id"];
            $zhongminonline_skuId = model('goods_sku')->getInfo(['sku_id' => $value['sku_id']], "zhongmin_goods_sku_id")["zhongmin_goods_sku_id"];
            $orders_data = [];
            $orders_data = [
                'customerOrderNo' => $order_info['out_trade_no'],
                'goodsId' => $zhongminonline_goodsId,
                'skuId' => $zhongminonline_skuId,
                'num' => $value['num'],
                'receiveProvince' => $full_address[0],
                'receiveCity' => $full_address[1],
                'receiveCounty' => $full_address[2],
                'receiveAddress' => $order_info['full_address'] . "-" . $order_info['address'],
                'receiveName' => empty($order_info['real_name']) ? $order_info['name'] : $order_info['real_name'],
                'receiveMobile' => $order_info['mobile'],
                'remark' => $order_info['buyer_message'],
            ];
            $data = array_merge($common_data, $orders_data);
            $sign = $this->getSign($data);
            $data['sign'] = $sign;


            $result = $this->execute($data, $last_url);
            $new_data['time_new'] = date("Y-m-d H:i:s.u");
            $new_data['res_data'] = $result;
            $new_data['orders_data'] = $orders_data;
            $new_data['orders_data'] = "createOrder";
            $new_data['method_text'] = "中闽订单生成";
            error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../createOrderlog.txt');
            if (!empty($result['code']) && $result['code'] = 200) {
                model('order_goods')->update(['zhongmin_order_no' => $result['data']['orderNo']], [['order_goods_id', "=", $value['order_goods_id']]]);
            }
        }
        return 1;
    }

    /**
     * 取消订单
     * @param $param
     * @return array|mixed
     */
    public function cancel($param)
    {
        //只有中闽在线商品才走
        $order_info = model('order')->getInfo(['order_id' => $param['order_id'], 'source' => 3]);
        if (empty($order_info)) {
            return $this->error('', '未找到订单数据');
        }
        $param_url = '/api/agent/cancelOrder';
        $last_url = $this->url . $param_url;
        $common_data = [
            'appkey' => $this->appkey
        ];
        $orders_data = [
            [
                'orderNo' => $order_info['out_trade_no']
            ],
        ];
        $data = array_merge($common_data, $orders_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;

        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /**
     * 订单列表
     *  * @param $param array
     * orderNo            string    订单编号
     * customerOrderNo    string    客户订单编号
     * goodsIds            string    商品编号,多个用逗号隔开
     * goodsName        string    商品名称关键词
     * startTime        string    下单时间段查询,开始时间
     * endTime            string    下单时间段查询,结束时间
     * logisticsNo        string    快递单号
     * receiveName        string    收件人姓名
     * receiveMobile    string    收件人手机号
     * orderStatus        string    订单状态 [WAIT_CONFIRM 待审核][WAIT_SEND 待发货][DELIVERED 已发货][FINISHED 已完成][CANCELED 已取消][REJECT 已拒绝]
     * page                int        查询页码
     * pageSize            int        查询页行数
     * @return array|mixed
     */
    public function orderList($param)
    {
        $param_url = '/api/agent/orderList';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = $param;
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;

        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /**
     * 查询订单详情
     * @param $param
     * @return array|mixed
     */
    public function orderDetail($param)
    {
        $param_url = '/api/agent/orderDetail';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'orderNo' => $param['order_id']
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;

        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }










    /*****************************************************快递物流接口*********************************************************/
    /**
     * 获取订单物流信息
     * @param $param
     * @return array
     */
    public function logisticsDetail($param)
    {
        $param_url = '/api/agent/logisticsDetail';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'logisticsNo' => $param['logisticsNo']
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }

    /**
     * 获取快递运费(对应参数)
     * @param $param
     * @return mixed
     */
    public function calcFreightFee($param)
    {
        $param_url = '/api/agent/calcFreightFee';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'goodsId' => $param['goodsId'],
            'skuCode' => $param['skuCode'],
            'num' => $param['num'],
            'province' => $param['province'],
            'city' => $param['city'],
            'county' => $param['county'],
            'address' => $param['address'],
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }



    /*****************************************************订单售后模块*********************************************************/


    /**
     * 发起售后      调试ok
     * @param $param
     * @return mixed
     */
    public function submitAfterSale($param)
    {
        $param_url = '/api/agent/submitAfterSale';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'orderNo' => $param['zhongmin_order_no'],
            'type' => $param['type'],
            'refundReason' => $param['refund_reason'],
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /*
     * 直接退款 查询反馈接果
     */
    public function orderRefundStatus1()
    {
        $list = model('order_goods')->getList(['refund_status' => 1, "refund_type"=>1,'source' => 3], 'zhongmin_order_no,order_id,site_id,order_goods_id,zhongmin_order_no');
        $data=[];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $res= $this->afterSaleDetail($value);
                if($res['code']==200){
                    if($res['code']['refundStatus']==1){
                        $order_refund_model = new OrderRefundModel();
                        $data = array(
                            "order_goods_id" => $value['order_goods_id'],
                            'site_id' => 0
                        );
                        //同意退款
                        $res = $order_refund_model->orderRefundConfirm($data, [ "uid"=>0,"username"=>"中闽系统"]);
                        //退款完成 执行退款操作
                        $res = $order_refund_model->orderRefundFinish($data, [ "uid"=>0,"username"=>"中闽系统"]);
                    }
                }
            }
        }
        return $this->success($data);
    }




    /**
     * 更新退货处理状态  (同意退货后第一步 接收退货地址)
     * @return array
     */
    public function orderRefundStatus(){
        $list = model('order_goods')->getList(['refund_status' => 1, "refund_type"=>2,'source' => 3], 'zhongmin_order_no,order_id,site_id,order_goods_id,zhongmin_order_no');
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $res= $this->afterSaleDetail($value);
                if($res['code']==200){
                    if($res['code']['refundStatus']==1){
                        $order_refund_model = new OrderRefundModel();
                        $data = array(
                            "order_goods_id" => $value['order_goods_id'],
                            'site_id' => $value['site_id'],
                            'refund_address'=> $res['code']['afterSaleAddress'],
                        );
                        $res = $order_refund_model->orderRefundConfirm($data, ['uid' => 0,'username' => '中闽系统']);
                    }
                }
            }
        }
        return $this->success();
    }




    /**
     * 更新退货处理状态  (同意退货后第2步 返回退货地址)
     * @return array
     */
    public function ZhongminRefundDelivery($params)
    {
        $order_goods_info = model('order_goods')->getInfo(['order_goods_id' => $params['order_goods_id']],'order_id,source,zhongmin_order_no');
        if (empty($order_goods_info) || $order_goods_info['source'] != 3) {
            return $this->error('','未找到订单数据');
        }
        $params['zhongmin_order_no']=$order_goods_info['zhongmin_order_no'];
        $res= $this->refundLogistics($params);
        return $this->success($res);
    }




    /**
     * 更新退货处理状态  (售后收到东西 业务处理)
     * @return array
     */
    public function orderRefundTakeDelivery()
    {
        $list = model('order_goods')->getList(['refund_status' => 5, "refund_type"=>2,'source' => 3], 'zhongmin_order_no,order_id,site_id,order_goods_id,zhongmin_order_no');
        $res = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $res= $this->afterSaleDetail($value);
                $new_data["value"] = $value;
                $new_data['time_new'] = date("Y-m-d H:i:s.u");
                $new_data['res_data'] = $res;
                $new_data['method'] = "orderRefundTakeDelivery";
                $new_data['method_text'] = "售后收到东西 业务处理 同意后";
                error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../orderRefundTakeDeliveryLOG.txt');
                if($res['code']==200){
                    if($res['code']['refundStatus']==1){
                        $order_refund_model = new OrderRefundModel();
                        $data = array(
                            "order_goods_id" => $value['order_goods_id'],
                            'site_id' => $value['site_id'],
                            'refund_address'=> $res['code']['afterSaleAddress'],
                        );
                        $res = $order_refund_model->orderRefundTakeDelivery($data, ['uid' => 0,'username' => '中闽系统']);
                    }
                }
            }
        }
        return $this->success($res);
    }




    /*
     *中闽收到货物 同意后执行
     */
    public function orderRefundTransfer()
    {
        $list = model('order_goods')->getList(['refund_status' => 6, "refund_type"=>2,'source' => 3], 'zhongmin_order_no,order_id,site_id,order_goods_id,zhongmin_order_no');
        $res = [];
        if (!empty($list)) {
            foreach ($list as $key => $value) {
                $res= $this->afterSaleDetail($value);
                $new_data["value"] = $value;
                $new_data['time_new'] = date("Y-m-d H:i:s.u");
                $new_data['res_data'] = $res;
                $new_data['method'] = "orderRefundTransfer";
                $new_data['method_text'] = "中闽收到货物 同意后执行  退款维权结束";
                error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../orderRefundTransferLOG.txt');
                if($res['code']==200){
                    if($res['code']['refundStatus']==1){
                        $order_refund_model = new OrderRefundModel();
                        $data = array(
                            "order_goods_id" => $value['order_goods_id'],
                            'site_id' => $value['site_id'],
                        );
                        $res = $order_refund_model->orderRefundFinish($data, ['uid' => 0,'username' => '中闽系统']);
                    }
                }
            }
        }
        return $this->success($res);
    }






    /**
     * 售后详情
     * @param $param
     * @return mixed
     */
    public function afterSaleDetail($param)
    {
        $param_url = '/api/agent/afterSaleDetail';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'orderNo' => $param['zhongmin_order_no']
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }






    /**
     * 售后寄回物流
     * @param $param
     * @return mixed
     */
    public function refundLogistics($param)
    {
        $param_url = '/api/agent/refundLogistics';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'orderNo' => $param['zhongmin_order_no'],
            'logisticsCompany' => $param['refund_delivery_name'],
            'logisticsNo' => $param['refund_delivery_no']
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }



    /**
     * 取消售后
     * @param $param
     * @return mixed
     */

    public function cancelAfterSale($param)
    {
        $param_url = '/api/agent/cancelAfterSale';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'appkey' => $this->appkey
        ];
        //业务参数
        $custom_data = [
            'orderNo' => $param['orderNo'],
            'logisticsCompany' => $param['logisticsCompany'],
            'logisticsNo' => $param['logisticsNo']
        ];
        $data = array_merge($common_data, $custom_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }

    /**
     * 数组转xml
     * @param $arr
     * @return string
     */
    public function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $xml .= "<" . $key . ">" . $this->arrayToXml($val) . "</" . $key . ">";
            } else {
                if (is_numeric($val)) {
                    $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
                } else {
                    $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
                }
            }
        }
        $xml .= "</xml>";
        return $xml;
    }


    /**
     * @Author     : Jason
     * @Description: CURL请求
     * @LastModify : Jason
     * @param $params
     * @return mixed
     */
    public function execute($params, $url)
    {
        $ch = curl_init();
        $this_header = array(
            "content-type: application/x-www-form-urlencoded;charset=UTF-8"
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $result = curl_exec($ch);
        error_log(print_r($result, 1), 3, dirname(__FILE__) . '../../executelog.txt');
        if ($result) return json_decode($result, true);


    }


    //    public function payExecute($params)
    //    {
    //        $ch = curl_init();
    //        curl_setopt($ch, CURLOPT_POST, true);
    //        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    //        curl_setopt($ch, CURLOPT_URL, 'https://www.houniao.hk1111/pay/gateway');
    //        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    //        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    //        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
    //        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    //        $response = curl_exec($ch);
    //        curl_close($ch);
    //        $response = json_decode($response, true);
    //        return $response;
    //    }


    public function getGoodsDetailByGoodsId($goods_id){
        $param_url = '/api/agent/goodsList';
        $last_url = $this->url.$param_url;
        $common_data = [
            'appkey' => $this->appkey
        ];
        $service_data = [
            'goodsIds'  => $goods_id,
            'goodsName' => '',
            'page'      => 1,
            'pageSize'  => 10
        ];
        $data = array_merge($common_data,$service_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        $result = $this->execute($data,$last_url);
        return $result;
    }

}