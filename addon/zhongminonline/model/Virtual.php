<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\zhongminonline\model;
use app\model\BaseModel;
use app\model\order\OrderRefund as OrderRefundModel;
use think\facade\Log;

/**
 * 中闽虚拟商品Api
 */
class Virtual extends BaseModel
{

    //接口文档  http://www.962699.com/support/orderApi/1

    // 测试环境
//    private $customerId = '80011012';
//
//    private $appkey = 'E36BC06C19CFF6751CBFBCE3DBB33465';
//
//    private $url = 'http://test.962699.com';

    //正式环境
    private $customerId = '80011064';

    private $appkey = '4B4111A785736BAE3841FDD8BE487D11';

    private $url = 'http://www.962699.com';

    /*****************************************************公共接口*********************************************************/

    /**
     * 生成签名
     * @return array
     */

    public function getSign($data)
    {
        $str = '';
        ksort($data);
        foreach ($data as $key => $val) {
            $str .= $key . '=' . $val . '&';
        }
        $last_str = strtolower(substr($str, 0, -1)) . $this->appkey;
        $sign = md5($last_str);
        return $sign;
    }


    /*****************************************************商品接口*********************************************************/


    /**
     * 获取商品列表
     * @return array
     */
    public function goodsList($page = 1, $page_size = 10, $goodsIds = '')
    {
        $param_url = '/api/goods/list';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];
        $service_data = [
            'goodsId' => $goodsIds,
            'page' => $page,
            'pageSize' => $page_size
        ];
        $data = array_merge($common_data, $service_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /**
     * 获取商品详情
     * @return array
     */
    public function goodsInfo($goodsIds = '')
    {
        $param_url = '/api/goods/info';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];
        $service_data = [
            'goodsId' => $goodsIds,
        ];
        $data = array_merge($common_data, $service_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /**
     * 获取商品详情
     * @return array
     */
    public function goodsDetail($goodsIds = '')
    {
        $param_url = '/api/goods/detail';
        $last_url = '';
        $last_url .= $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];
        $service_data = [
            'goodsId' => $goodsIds,
        ];
        $data = array_merge($common_data, $service_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        $result = $this->execute($data, $last_url);
        return $result;
    }


    /*****************************************************虚拟充值接口*********************************************************/


    /**
     * 创建订单
     * @param $param
     * @return array|mixed
     */
    public function createVirtualOrder($param)
    {
        $param = model('order')->getInfo(['order_id' => $param['order_id']]);
        //只有中闽在线虚拟商品才走
        if (empty($param) || !isset($param['source']) || $param['source'] != 3 || $param['order_type'] != 4) {
            return $this->error('', '未找到订单数据或不属于中闽在线虚拟订单');
        }
        $order_info = $param;
        $order_goods = model('order_goods')->getList(['order_id' => $order_info['order_id'], 'source' => 4]);
        if (empty($order_goods)) {
            return $this->error('', '未找到订单中闽在线虚拟商品数据');
        }
        $common_data = [
            'customerId' => $this->customerId
        ];
        foreach ($order_goods as $key => $value) {
            $zhongminonline_goodsId = model('goods')->getInfo(['goods_id' => $value['goods_id']], "zhongmin_goods_id")["zhongmin_goods_id"];
            $zhongminonline_goodsType = model('goods')->getInfo(['goods_id' => $value['goods_id']], "zhongmin_goods_type")["zhongmin_goods_type"];
            if($zhongminonline_goodsType==1){
                $param_url = '/api/order/create';
            }elseif($zhongminonline_goodsType==2){
                $param_url = '/api/order/card';
            }elseif($zhongminonline_goodsType==3){
                $param_url = '/api/order/phone';
            }
            $last_url = $this->url . $param_url;
            $orders_data = [
                'customerOrderNo' => $order_info['out_trade_no'],
                'goodsId' => $zhongminonline_goodsId,
                'num' => $value['num'],
                'account' => $order_info['mobile'],
                'buyerIp' => $param['buyerIp'],
                'notifyURL' => 'www.baidu.com'
            ];
            $data = array_merge($common_data, $orders_data);
            $sign = $this->getSign($data);
            $data['sign'] = $sign;
            $result = $this->execute($data, $last_url);
            $new_data['time_new'] = date("Y-m-d H:i:s.u");
            $new_data['res_data'] = $result;
            $new_data['orders_data'] = $orders_data;
            $new_data['orders_data'] = "createVirtualOrder";
            $new_data['method_text'] = "中闽虚拟订单生成";
            error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '../../createVirtualOrderlog.txt');
            if (!empty($result['code']) && $result['code'] = 200) {
                model('order_goods')->update(['zhongmin_order_no' => $result['data']['orderNo']], [['order_goods_id', "=", $value['order_goods_id']]]);
            }
        }
        return 1;
    }



    /**
     * 话费下单接口
     * @param $param
     * @return array|mixed
     */
    public function phoneRecharge($param)
    {

        $param_url = '/api/order/phone';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];

        $orders_data = [
            'customerOrderNo' => $param['customerOrderNo'],
            'goodsId' => $param['goodsId'],
            'account' => $param['account'],
            'num' => $param['num'],
            'buyerIp' => $param['buyerIp'],
            'notifyURL' => $param['notify_url'],
        ];
        $data = array_merge($common_data, $orders_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;


        $result = $this->execute($data, $last_url);

        return $result;
    }


    /**
     * 卡密下单接口
     * @param $param
     * @return array|mixed
     */
    public function cardRecharge($param)
    {

        $param_url = '/api/order/card';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];

        $orders_data = [
            'customerOrderNo' => $param['customerOrderNo'],
            'goodsId' => $param['goodsId'],
            'num' => $param['num'],
            'buyerIp' => $param['buyerIp'],
            'notifyURL' => $param['notify_url'],
        ];
        $data = array_merge($common_data, $orders_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;


        $result = $this->execute($data, $last_url);

        return $result;
    }


    /**
     * 直充下单接口
     * @param $param
     * @return array|mixed
     */
    public function createRecharge($param)
    {

        $param_url = '/api/order/create';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];

        $orders_data = [
            'customerOrderNo' => $param['customerOrderNo'],
            'goodsId' => $param['goodsId'],
            'account' => $param['account'],
            'num' => $param['num'],
            'buyerIp' => $param['buyerIp'],
            'notifyURL' => $param['notify_url'],
        ];
        $data = array_merge($common_data, $orders_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;


        $result = $this->execute($data, $last_url);
        return $result;
    }






    /**
     * 订单查询接口
     * @param $param
     * @return array|mixed
     */
    public function orderQuery($param)
    {

        $param_url = '/api/order/query';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];

        $orders_data = [
            'customerOrderNo' => $param['customerOrderNo'],
        ];
        $data = array_merge($common_data, $orders_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;


        $result = $this->execute($data, $last_url);
        return $result;
    }

    /**
     * 拿到余额接口
     * @param $param
     * @return array|mixed
     */
    public function queryBalance()
    {
        $param_url = '/api/customer/balance';
        $last_url = $this->url . $param_url;
        //公共参数
        $common_data = [
            'customerId' => $this->customerId
        ];
        $data = $common_data;
        $sign = $this->getSign($data);
        $data['sign'] = $sign;
        // 发起请求
        $result = $this->execute($data, $last_url);
        return $result;
    }

    /**
     * 手机号归属地查询接口
     * @param $param
     * @return array|mixed
     */
    public function mobileInfo($param){
        $param_url = '/api/other/mobileInfo';
        $last_url = $this->url . $param_url;
        $common_data = [
            'customerId' => $this->customerId
        ];

        $orders_data = [
            'mobile' => $param['mobile'],
        ];
        $data = array_merge($common_data, $orders_data);
        $sign = $this->getSign($data);
        $data['sign'] = $sign;


        $result = $this->execute($data, $last_url);
        return $result;
    }


    /**
     * @Author     : Jason
     * @Description: CURL请求
     * @LastModify : Jason
     * @param $params
     * @return mixed
     */
    public function execute($params, $url)
    {
        $ch = curl_init();
        $this_header = array(
            "content-type: application/x-www-form-urlencoded;charset=UTF-8"
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $result = curl_exec($ch);
        error_log(print_r($result, 1), 3, dirname(__FILE__) . '../../executelog.txt');
        if ($result) return json_decode($result, true);


    }










}