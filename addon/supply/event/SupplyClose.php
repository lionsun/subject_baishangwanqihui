<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\supply\event;

use addon\supply\model\goods\Goods;

/**
 * 供应商关闭
 * @author Administrator
 *
 */
class SupplyClose
{
    public function handle($data)
    {
        $site_id = $data["site_id"];
        //将供应商下的商品全部下架
        $goods_model = new Goods();
        $goods_result = $goods_model->lockup([["site_id", "=", $site_id]], "供应商关闭");
        return $goods_result;
    }
}
