<?php

namespace addon\devtools\event;

/**
 * 活动展示
 */
class ShowPromotion
{

    /**
     * 活动展示
     * 
     * @return multitype:number unknown
     */
	public function handle()
	{
        $data = [
            'admin' => [
                [
                    //插件名称
                    'name' => 'devtools',
                    //店铺端展示分类  shop:营销活动   member:互动营销
                    'show_type' => 'tool',
                    //展示主题
                    'title' => '开发工具',
                    //展示介绍
                    'description' => '开发工具',
                    //展示图标
                    'icon' => 'addon/devtools/icon.png',
                    //跳转链接
                    'url' => 'devtools://admin/devtools/ExportGoodsCategory',
                ]
            ]
        ];
	    return $data;
	}
}