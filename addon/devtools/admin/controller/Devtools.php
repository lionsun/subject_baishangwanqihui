<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\devtools\admin\controller;

use app\admin\controller\BaseAdmin;
use addon\platformcoupon\model\PlatformcouponType as PlatformcouponTypeModel;
use addon\platformcoupon\model\Platformcoupon as PlatformcouponModel;
use app\model\shop\ShopGroup as ShopGroupModel;

/**
 * 优惠券
 * @author Administrator
 *
 */
class Devtools extends BaseAdmin
{
    public function lists()
    {
        return $this->fetch("devtools/lists");
    }

    public function ExportGoodsCategory()
    {
        return $this->fetch("devtools/ExportGoodsCategory");
    }


    public function detail()
    {
        return $this->fetch("devtools/edit");
    }

    public function exportGoods()
    {
        return $this->fetch("devtools/ExportGoods");
    }


}