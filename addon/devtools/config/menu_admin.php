<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'DEVTOOLS_EXPORT_GOODS_CATEGORY',
        'title' => '导出商品分类',
        'url' => 'devtools://admin/devtools/ExportGoodsCategory',
        'parent' => 'TOOL_ROOT',
        'is_show' => 1,
        'picture' => 'addon/devtools/icon.png',
        'sort' => 100,
        'child_list' => [
        ]

    ],
    [
        'name' => 'DEVTOOLS_EXPORT_GOODS',
        'title' => '导出商品',
        'url' => 'devtools://admin/devtools/ExportGoods',
        'parent' => 'TOOL_ROOT',
        'is_show' => 1,
        'picture' => 'addon/devtools/icon.png',
        'sort' => 110,
        'child_list' => [
        ]

    ],
    [
        'name' => 'DEVTOOLS_SCRIPT',
        'title' => '脚本',
        'url' => 'devtools://admin/devtools/lists',
        'parent' => 'TOOL_ROOT',
        'is_show' => 1,
        'picture' => 'addon/devtools/icon.png',
        'sort' => 900,
        'child_list' => [
            [
                'name' => 'DEVTOOLS_SCRIPT_EDIT',
                'title' => '编辑脚本',
                'url' => 'devtools://admin/devtools/detail',
                'picture' => 'addon/devtools/icon.png',
                'sort'    => 1,
                'is_show' => 0
            ],

        ]

    ],
];
