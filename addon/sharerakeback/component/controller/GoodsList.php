<?php

namespace addon\sharerakeback\component\controller;

use app\component\controller\BaseDiyView;

/**
 * 商品列表·组件
 */
class GoodsList extends BaseDiyView
{
	/**
	 * 后台编辑界面
	 */
	public function design()
	{
		return $this->fetch("goods_list/design.html");
	}
}