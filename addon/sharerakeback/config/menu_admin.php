<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'SHARE_RAKE_BACK',
        'title' => '分享返佣',
        'url' => 'sharerakeback://admin/discount/lists',
        'parent' => 'PROMOTION_SHOP',
        'is_show' => 0,
        'is_control' => 1,
        'is_icon' => 0,
        'sort' => 100,
        'child_list' => [
            [
                'name' =>  'PROMOTION_MEMBER_PRICE',
                'title' => '会员价信息',
                'url' => 'sharerakeback://admin/discount/lists',
                'is_show' => 1,
                'is_control' => 1,
                'is_icon' => 0,
                'sort' => 1,
                'child_list' => [
                    [
                        'name' => 'SHARE_RAKE_BACK_GOODS_DETAIL',
                        'title' => '详细信息',
                        'url' => 'sharerakeback://admin/discount/detail',
                        'is_show' => 1,
                        'is_control' => 1,
                        'is_icon' => 0,
                        'sort' => 102,
                    ],
                ],
            ],
            [
                'name' => 'SET_SHARE_RAKE_BACK_CONFIG',
                'title' => '设置分享返佣限制',
                'url' => 'sharerakeback://admin/discount/config',
                'sort'    => 5,
                'is_show' => 1,
                'is_control' => 1,
                'is_icon' => 0,
            ],
        ]
    ],

];
