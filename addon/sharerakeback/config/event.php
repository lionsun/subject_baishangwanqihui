<?php
// 事件定义文件
return [
    'bind'      => [
        
    ],

    'listen'    => [
        //展示活动
        'ShowPromotion' => [
            'addon\sharerakeback\event\ShowPromotion',
        ],
        //会员价设置
        'MemberPriceInfo' => [
            'addon\sharerakeback\event\MemberPriceInfo',
        ],
        //订单完成
        'OrderComplete' => [
            'addon\sharerakeback\event\OrderComplete',
        ],
    ],

    'subscribe' => [
    ],
];
