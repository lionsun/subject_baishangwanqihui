<?php
// +----------------------------------------------------------------------
// | 平台端菜单设置
// +----------------------------------------------------------------------
return [
    [
        'name' => 'SHARE_RAKE_BACK',
        'title' => '分享返佣',
        'url' => 'sharerakeback://shop/discount/lists',
        'parent' => 'PROMOTION_CENTER',
        'is_show' => 0,
        'is_control' => 1,
        'is_icon' => 0,
        'picture' => '',
        'picture_select' => '',
        'sort' => 100,
        'child_list' => [
            [
                'name' => 'SHARE_RAKE_BACK_GOODS_LIST',
                'title' => '分享返佣管理',
                'url' => 'sharerakeback://shop/discount/lists',
                'parent' => 'PROMOTION_CENTER',
                'is_show' => 1,
                'is_control' => 1,
                'is_icon' => 0,
                'picture' => '',
                'picture_select' => '',
                'sort' => 100,
            ],
            [
                'name' => 'SHARE_RAKE_BACK_SHOP_CONFIG',
                'title' => '设置分享返佣',
                'url' => 'sharerakeback://shop/discount/config',
                'parent' => 'PROMOTION_CENTER',
                'is_show' => 0,
                'sort' => 1,
            ],
        ]
    ],
];
