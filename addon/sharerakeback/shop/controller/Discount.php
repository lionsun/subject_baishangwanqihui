<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\sharerakeback\shop\controller;


use addon\sharerakeback\model\ShareRakeBackGoodsSku as ShareRakeBackGoodsSkuModel;
use addon\sharerakeback\model\ShareRakeBackGoods as ShareRakeBackGoodsModel;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\goods\GoodsShopCategory as GoodsShopCategoryModel;
use app\model\member\MemberLevel as MemberLevelModel;
use app\shop\controller\BaseShop;
use addon\sharerakeback\model\Discount as DiscountModel;
use addon\sharerakeback\model\Config as ConfigModel;
use think\facade\Db;

/**
 * 会员价控制器
 */
class Discount extends BaseShop
{
	
	/**
	 * 会员价商品列表
	 */
	public function lists()
	{
        $goods_model = new GoodsModel();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', "");
            $start_sale = input('start_sale', 0);
            $end_sale = input('end_sale', 0);
            $is_share_rake_back = input('is_share_rake_back', '');
            $goods_shop_category_ids = input('goods_shop_category_ids', '');
            $condition = [ [ 'is_delete', '=', 0 ], [ 'site_id', '=', $this->site_id ], [ 'verify_state', '=', 1 ] ];
            if (!empty($search_text)) {
                $condition[] = [ 'goods_name', 'like', '%' . $search_text . '%' ];
            }
            if ($is_share_rake_back !== "") {
                $condition[] = [ 'is_share_rake_back', '=', $is_share_rake_back ];
            }
            if (!empty($start_sale)) $condition[] = [ 'sale_num', '>=', $start_sale ];
            if (!empty($end_sale)) $condition[] = [ 'sale_num', '<=', $end_sale ];
            if (!empty($goods_shop_category_ids)) $condition[] = [ 'goods_shop_category_ids', 'like', [ $goods_shop_category_ids, '%' . $goods_shop_category_ids . ',%', '%' . $goods_shop_category_ids, '%,' . $goods_shop_category_ids . ',%' ], 'or' ];
            $res = $goods_model->getGoodsPageList($condition, $page_index, $page_size,"create_time desc","goods_id,goods_name,site_id,site_name,goods_image,is_own,goods_state,verify_state,price,goods_stock,create_time,sale_num,is_virtual,goods_class,is_fenxiao,fenxiao_type,sku_id,max_buy,min_buy,source,cost_price,subscription_open_state,corporate_subscription,subscription_status,subscription_check_reason,brand_name,goods_stock_alarm,category_name,modify_time,share_rake_rate,is_share_rake_back");
            return $res;
        } else {
            //获取店内分类
            $model = new ConfigModel();
            $config = $model->getConfig();
            $goods_shop_category_model = new GoodsShopCategoryModel();
            $goods_shop_category_list = $goods_shop_category_model->getShopCategoryTree([ [ 'site_id', "=", $this->site_id ] ], 'category_id,category_name,pid,level');
            $goods_shop_category_list = $goods_shop_category_list['data'];
            $this->assign("goods_shop_category_list", $goods_shop_category_list);
            $this->assign("config", $config['data']);
            return $this->fetch("discount/lists");
        }
	}

    /**
     * 添加商品会员价
     */
    public function add()
    {
        if (request()->isAjax()) {
            //获取商品信息
            $goods_id = input('goods_id', 0);
            $is_set_member_price = input('is_set_member_price', 0);
            $member_price_set_content = input('member_price_set_content', '');
            $condition = [
                'site_id'  => $this->site_id,
                'goods_id' => $goods_id
            ];
            $member_price_data = [
                'is_set_member_price'       => $is_set_member_price,
                'member_price_set_content'  => $member_price_set_content,
            ];
            $discount_model = new DiscountModel();
            return $discount_model->addGoods($member_price_data,$condition);
        } else {
            $member_level_model = new MemberLevelModel();
            $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',1]])['data'];
            $this->assign("member_level_list", $member_level_list);
            return $this->fetch("discount/add");
        }
    }

    /**
     * 编辑商品会员价
     */
    public function edit(){
        if (request()->isAjax()) {
            //获取商品信息
            $goods_id = input('goods_id', 0);
            $is_set_member_price = input('is_set_member_price', 0);
            $member_price_set_content = input('member_price_set_content', '');
            $condition = [
                'site_id'  => $this->site_id,
                'goods_id' => $goods_id
            ];
            $member_price_data = [
                'is_set_member_price'       => $is_set_member_price,
                'member_price_set_content'  => $member_price_set_content,
            ];
            $discount_model = new DiscountModel();
            return $discount_model->editGoods($member_price_data,$condition);
        } else {
            $goods_id = input('goods_id', 0);
            $discount_model = new DiscountModel();
            $goods_model = new GoodsModel();
            $member_price = $discount_model->getGoodsInfo([['goods_id','=',$goods_id]])['data'];
            $member_price['goods_image'] = $goods_model->getGoodsDetail($goods_id)['data']['goods_image'];
            $member_price['sku_list'] = $goods_model->getGoodsSkuList([['goods_id','=',$goods_id]])['data'];
            $this->assign("detail", $member_price);
            return $this->fetch("discount/edit");
        }
    }

    /**
     * 关闭会员价
     */
    public function closeMemberPrice(){
        $goods_id = input("goods_id", 0);
        $data = [
            'is_set_member_price' => 0,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->memberprice($condition,$data);
        return $res;
    }
    
    /**
     * 开启会员价
     */
    public function openMemberPrice(){
        $goods_id = input("goods_id", 0);
        $data = [
            'is_set_member_price' => 1,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->memberprice($condition,$data);
        return $res;
    }

	
	/**
	 * 删除商品
	 */
	public function deleteGoods()
	{
		if (request()->isAjax()) {
			$goods_id = input('goods_id', '');
			$discount_model = new DiscountModel();
			return $discount_model->deleteGoods($goods_id);
		}
	}

    /**
     * 商品选择组件
     * @return \multitype
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $goods_id = input('goods_id', 0);
            $is_virtual = input('is_virtual', '');// 是否虚拟类商品（0实物1.虚拟）
            $min_price = input('min_price', 0);
            $max_price = input('max_price', 0);
            $goods_class = input('goods_class', "");// 商品类型，实物、虚拟
            $category_id = input('category_id', "");// 商品分类id
            $promotion_type = input('promotion_type', "");

            $condition = [
                ['is_delete', '=', 0],
                ['goods_state', '=', 1],
                ['site_id', '=', $this->site_id],
                ['is_set_member_price', '=', 0]
            ];


            if (!empty($goods_name)) {
                $condition[] = ['goods_name', 'like', '%' . $goods_name . '%'];
            }
            if ($is_virtual !== "") {
                $condition[] = ['is_virtual', '=', $is_virtual];
            }
            if (!empty($goods_id)) {
                $condition[] = ['goods_id', '=', $goods_id];
            }
            if (!empty($category_id)) {
                $condition[] = ['category_id', 'like', [$category_id, '%' . $category_id . ',%', '%' . $category_id, '%,' . $category_id . ',%'], 'or'];
            }

            if (!empty($promotion_type)) {
                $condition[] = ['promotion_addon', 'like', "%{$promotion_type}%"];
            }


            if ($goods_class !== "") {
                $condition[] = ['goods_class', '=', $goods_class];
            }

            if ($min_price != "" && $max_price != "") {
                $condition[] = ['price', 'between', [$min_price, $max_price]];
            } elseif ($min_price != "") {
                $condition[] = ['price', '<=', $min_price];
            } elseif ($max_price != "") {
                $condition[] = ['price', '>=', $max_price];
            }

            $order = 'create_time desc';
            $goods_model = new GoodsModel();
            $field = 'goods_id,goods_name,goods_class_name,goods_image,price,goods_stock,create_time,is_virtual,cost_price';
            $goods_list = $goods_model->getGoodsPageList($condition, $page, $page_size, $order, $field);
            return $goods_list;
        } else {

            //已经选择的商品sku数据
            $select_id = input('select_id', '');
            $mode = input('mode', 'spu');
            $max_num = input('max_num', 0);
            $min_num = input('min_num', 0);
            $is_virtual = input('is_virtual', '');
            $disabled = input('disabled', 0);
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao
            $discount_id = input('discount_id', 0);//会员价id

            $this->assign('select_id', $select_id);
            $this->assign('mode', $mode);
            $this->assign('max_num', $max_num);
            $this->assign('min_num', $min_num);
            $this->assign('is_virtual', $is_virtual);
            $this->assign('disabled', $disabled);
            $this->assign('promotion', $promotion);
            $this->assign('discount_id', $discount_id);

            // 营销活动
            $goods_promotion_type = event('GoodsPromotionType');
            $this->assign('promotion_type', $goods_promotion_type);

            //商品分类
            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList([], 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);
            return $this->fetch("discount/goods_select");
        }
    }

    /**
     * 是否开启充值
     * @return mixed
     */
    public function setConfig()
    {
        $model = new ConfigModel();
        $is_use = input('is_use', 0);
        $data = [];
        return $model->setShopConfig($data, $is_use,$this->site_id);
    }

    /**
     * 修改商品分享返佣状态
     */
    public function modify()
    {
        if (request()->isAjax()) {
            $fenxiao_goods_model = new ShareRakeBackGoodsModel();
            $goods_id = input('goods_id');
            $is_share_rake_back = input('is_share_rake_back', 0);
            return $fenxiao_goods_model->modifyGoodsShareRakeBackStatus($goods_id, $is_share_rake_back ? 0 : 1, $this->site_id);
        }
    }

    public function config()
    {
        $share_rake_back_goods_sku_model = new ShareRakeBackGoodsSkuModel();
        if (request()->isAjax()) {
            Db::startTrans();
            try {
                $goods_id = input('goods_id');
                $shares_skus = input('shares', []);
                $is_share_rake_back = input('is_share_rake_back', 0);
                $share_rake_rate = input('share_rake_rate', 0);
                $goods_data = [
                    'is_share_rake_back' => $is_share_rake_back,
                    'share_rake_rate'     => $share_rake_rate
                ];

                $shares_goods_sku_data = [];
                foreach ($shares_skus as $level_id => $level_data) {
                    foreach ($level_data['sku_id'] as $key => $sku_id) {
                        $share_rake_back_sku = [
                            'goods_id' => $goods_id,
                            'level_id' => $level_id,
                            'sku_id' => $sku_id,
                            'rate' => $level_data['rate'][ $key ],
                            'member_price' => $level_data['member_price'][ $key ],
                        ];
                        $shares_goods_sku_data[] = $share_rake_back_sku;
                    }
                }
                $share_rake_back_goods_sku_model->deleteSku([ 'goods_id' => $goods_id ]);
                $share_rake_back_goods_sku_model->addSkuList($shares_goods_sku_data);

                $share_rake_back_goods_model = new ShareRakeBackGoodsModel();
                $re = $share_rake_back_goods_model->editGoodsShareRakeBack($goods_data, [ [ 'goods_id', '=', $goods_id ], [ 'site_id', '=', $this->site_id ] ]);
                Db::commit();
                return $re;
            } catch (\Exception $e) {
                Db::rollback();
                return error(-1, $e->getMessage());
            }
        }else{
            $goods_id = input('goods_id');
            $goods_model = new GoodsModel();
            $share_rake_back_goods_sku_model = new ShareRakeBackGoodsSkuModel();
            $member_level_model = new MemberLevelModel();
            $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',1]])['data'];
            $goods_info = $goods_model->getGoodsDetail($goods_id);
            $share_rake_back_skus = $share_rake_back_goods_sku_model->getSkuList([ 'goods_id' => $goods_id ]);
            $skus = [];
            foreach ($share_rake_back_skus['data'] as $share_rake_back_skus) {
                $skus[ $share_rake_back_skus['level_id'] . '_' . $share_rake_back_skus['sku_id'] ] = $share_rake_back_skus;
            }
            $goods_info['data']['share_skus'] = $skus;
            $goods_info['data']['goods_image'] =
                !empty($goods_info['data']['goods_image'])?explode(',', $goods_info['data']['goods_image'])[0]:'';
            $this->assign("member_level_list", $member_level_list);
            $this->assign('goods_info', $goods_info['data']);

            return $this->fetch("discount/config");
        }
    }
}