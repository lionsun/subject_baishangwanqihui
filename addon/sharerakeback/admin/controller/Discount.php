<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\sharerakeback\admin\controller;

use addon\sharerakeback\model\ShareRakeBackGoodsSku as ShareRakeBackGoodsSkuModel;
use app\admin\controller\BaseAdmin;
use addon\sharerakeback\model\Discount as DiscountModel;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsBrand;
use app\model\member\MemberLevel as MemberLevelModel;
use addon\sharerakeback\model\Config as ConfigModel;

/**
 * 会员价控制器
 */
class Discount extends BaseAdmin
{
	
	/**
	 * 会员价列表
	 */
    public function lists()
    {
        $model = new GoodsModel();

        if (request()->isAjax()) {

            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $condition = [ [ 'is_share_rake_back', '=', 1 ] ];
            $search_text_type = input('search_text_type', "goods_name");//店铺名称或者商品名称
            $search_text = input('search_text', "");
            if (!empty($search_text)) {
                $condition[] = [ $search_text_type, 'like', '%' . $search_text . '%' ];
            }

            $goods_class = input('goods_class', "");//商品种类
            if ($goods_class !== "") {
                $condition[] = [ 'goods_class', '=', $goods_class ];
            }

            $goods_state = input('goods_state', "");//商品状态
            if ($goods_state !== '') {
                $condition[] = [ 'goods_state', '=', $goods_state ];
            }

            $category_id = input('category_id', "");//分类ID
            if (!empty($category_id)) {
                $condition[] = [ 'category_id|category_id_1|category_id_2|category_id_3', '=', $category_id ];
            }

            $brand_id = input('goods_brand', '');//商品品牌
            if (!empty($brand_id)) {
                $condition[] = [ 'brand_id', '=', $brand_id ];
            }
            $list = $model->getGoodsPageList($condition, $page_index, $page_size);
            return $list;
        } else {
            $this->forthMenu([]);
            //商品品牌
            $goods_brand_model = new GoodsBrand();
            $list = $goods_brand_model->getBrandList('', 'brand_id,brand_name', 'sort asc');
            $this->assign('goods_brand', $list['data']);
            return $this->fetch('discount/lists');
        }
    }

    public function checkMemberPrice(){
        $goods_id = (int)input('goods_id', '');
        $is_set_member_price = (int)input('is_set_member_price', 0);
        $member_price_set_content= input('member_price_set_content', '');
        $condition = ['goods_id' => $goods_id];
        $data = [
            'is_set_member_price'      => $is_set_member_price,
            'member_price_set_content' => $member_price_set_content,
        ];
        $discount_model = new DiscountModel();
        return $discount_model->checkmemberprice($data,$condition);
    }

    public function closeMemberPrice(){
        $goods_id = input("goods_id", 0);
        $data = [
            'is_set_member_price' => 0,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->memberprice($condition,$data);
        return $res;
    }

    public function openMemberPrice(){
        $goods_id = input("goods_id", 0);
        $data = [
            'is_set_member_price' => 1,
        ];
        $condition = ['goods_id' => $goods_id];
        $model = new DiscountModel();
        $res = $model->memberprice($condition,$data);
        return $res;
    }

    /**
     * 删除商品
     */
    public function deleteGoods()
    {
        if (request()->isAjax()) {
            $goods_id = input('goods_id', '');
            $discount_model = new DiscountModel();
            return $discount_model->deleteGoods($goods_id);
        }
    }


    /**
     * 分享返佣限制设置
     */
    public function config(){
        $member_level_model = new MemberLevelModel();
        $config_model = new ConfigModel();
        if (request()->isAjax()) {
            //设置代理商审核设置
            $min_user_level = input('min_user_level', '');
            $data = [
                'min_user_level'  => $min_user_level
            ];
            return $config_model->setConfig($data);
        }else{
            $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',1]], 'level_id, level_name', 'growth asc');
            $config_info = $config_model->getConfig()['data']['value'];
            $this->assign('config_info', $config_info);
            $this->assign('member_level_list', $member_level_list['data']);
            //四级菜单
            $this->forthMenu([]);

            return $this->fetch('discount/config');
        }
    }


    public function detail()
    {
        $goods_id = input('goods_id');
        $goods_model = new GoodsModel();
        $share_rake_back_sku_model = new ShareRakeBackGoodsSkuModel();
        $member_level_model = new MemberLevelModel();
        $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',1]])['data'];
        $goods_info = $goods_model->getGoodsDetail($goods_id);
        $share_rake_back_skus = $share_rake_back_sku_model->getSkuList([ 'goods_id' => $goods_id ]);
        $skus = [];
        foreach ($share_rake_back_skus['data'] as $share_rake_back_skus) {
            $skus[ $share_rake_back_skus['level_id'] . '_' . $share_rake_back_skus['sku_id'] ] = $share_rake_back_skus;
        }
        $goods_info['data']['share_skus'] = $skus;
        $this->assign('member_level_list', $member_level_list);
        $this->assign('goods_info', $goods_info['data']);
        return $this->fetch('discount/detail');
    }



}