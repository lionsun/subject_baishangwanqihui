<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\sharerakeback\model;

use app\model\BaseModel;
use app\model\goods\Goods as GoodsModel;
use app\model\member\MemberLevel as MemberLevelModel;

/**
 * 分享返佣商品
 */
class ShareRakeBackGoods extends BaseModel
{
	
	/**
	 * @return array
	 */
	public function editGoodsShareRakeBack($data, $condition)
	{
		$re = model('goods')->update($data, $condition);
		return $this->success($re);
	}


    /**
     * 修改分享返佣状态
     * @param $goods_ids
     * @param $is_fenxiao
     * @param $site_id
     * @return array
     */
	public function modifyGoodsShareRakeBackStatus($goods_id, $is_share_rake_back, $site_id)
	{
	    $share_rake_back_goods_sku = model('share_rake_back_goods_sku')->getList([ [ 'goods_id', '=', $goods_id ] ]);
        model('goods')->startTrans();
	    try {
            if (empty($share_rake_back_goods_sku)) {
                $member_level_model = new MemberLevelModel();
                $member_level_list = $member_level_model->getMemberLevelList([['level_type','=',1]])['data'];
                $goods_model = new GoodsModel();
                $goods_info = $goods_model->getGoodsDetail($goods_id);
                $share_rake_back_goods_sku_data = [];
                foreach ($member_level_list as $level) {
                    foreach ($goods_info['data']['sku_data'] as $sku) {
                        $share_rake_back_sku = [
                            'goods_id' => $goods_id,
                            'level_id' => $level['level_id'],
                            'sku_id' => $sku['sku_id'],
                            'rate' => $level['one_rate'],
                            'member_price' => 0,
                        ];
                        $share_rake_back_goods_sku_data[] = $share_rake_back_sku;
                    }
                }
                model('share_rake_back_goods_sku')->addList($share_rake_back_goods_sku_data);
            }

            model('goods')->update([ 'is_share_rake_back' => $is_share_rake_back ], [ [ 'goods_id', '=', $goods_id ], [ 'site_id', '=', $site_id ] ]);
            model('goods')->commit();
            return $this->success(1);
	    } catch (\Exception $e) {
            model('goods')->rollback();
            return $this->error($e->getMessage());
        }
	}
}