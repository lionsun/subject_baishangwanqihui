<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\sharerakeback\model;

use app\model\BaseModel;


/**
 * 分享返佣
 */
class ShareRakeBackGoodsSku extends BaseModel
{
	/**
	 * 添加分享返佣商品
	 * @param $data
	 * @return array
	 */
	public function addSku($data)
	{
		$res = model('share_rake_back_goods_sku')->add($data);
		return $this->success($res);
	}
	
	
	/**
	 * 编辑分享返佣商品
	 * @param $data
	 * @param array $condition
	 * @return array
	 */
	public function editSku($data, $condition = [])
	{
		$data['update_time'] = time();
		$res = model('share_rake_back_goods_sku')->update($data, $condition);
		
		return $this->success($res);
	}
	
	
	/**
	 * 删除分享返佣商品
	 * @param array $condition
	 * @return array
	 */
	public function deleteSku($condition = [])
	{
		$res = model('share_rake_back_goods_sku')->delete($condition);
		return $this->success($res);
	}


    /**
     * 获取分享返佣商品信息
     * @param array $condition
     * @return array
     */
    public function getShareRakeBackGoodsSkuInfo($condition=[],$field='*'){
        $list = model('share_rake_back_goods_sku')->getInfo($condition, $field);
        return $this->success($list);
    }
	
	
	/**
	 * 获取分享返佣sku列表
	 * @param array $condition
	 * @param string $field
	 * @param string $order
	 * @param string $limit
	 */
	public function getSkuList($condition = [], $field = '*', $order = '', $limit = null)
	{
		$list = model('share_rake_back_goods_sku')->getList($condition, $field, $order, '', '', '', $limit);
		return $this->success($list);
	}
	
	/**
	 * 获取分享返佣商品分页列表
	 * @param array $condition
	 * @param int $page
	 * @param int $page_size
	 * @param string $order
	 * @param string $field
	 * @param string $order
	 * @return array
	 */
	public function getShareRakeBackGoodsSkuPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'fgs.goods_sku_id desc', $field = 'fgs.goods_sku_id,fgs.goods_id,fgs.sku_id,fgs.level_id,fgs.one_rate,fgs.one_money,fgs.two_rate,fgs.two_money,fgs.three_rate,fgs.three_money,gs.sku_name,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.site_id', $alias = '', $join = '')
	{
		$list = model('share_rake_back_goods_sku')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
		return $this->success($list);
	}
	
	
	/**
	 * 批量添加分享返佣商品
	 * @param $data
	 */
	public function addSkuList($data)
	{
		$re = model('share_rake_back_goods_sku')->addList($data);
		return $this->success($re);
	}



}