<?php
// +---------------------------------------------------------------------+
// | NiuCloud | [ WE CAN DO IT JUST NiuCloud ]                |
// +---------------------------------------------------------------------+
// | Copy right 2019-2029 www.niucloud.com                          |
// +---------------------------------------------------------------------+
// | Author | NiuCloud <niucloud@outlook.com>                       |
// +---------------------------------------------------------------------+
// | Repository | https://github.com/niucloud/framework.git          |
// +---------------------------------------------------------------------+

namespace addon\sharerakeback\model;

use app\model\BaseModel;
use app\model\system\Config as ConfigModel;

/**
 * 设置
 */
class Config extends BaseModel
{
    /**
     * 设置分享返佣限制
     * @return array
     */
    public function setConfig($data)
    {
        $config = new ConfigModel();
        $res = $config->setConfig($data, '分享返佣限制设置', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'SHARE_RAKE_BACK_CONFIG' ] ]);
        return $res;
    }

    /**
     * 获取分享返佣限制设置信息
     * @return
     */
    public function getConfig()
    {
        $config = new ConfigModel();
        $res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'SHARE_RAKE_BACK_CONFIG' ] ]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'min_user_level'  => 0,
            ];
        }
        $member_level_info = model('member_level')->getInfo([['level_id', '=', $res['data']['value']['min_user_level']]]);
        $res['data']['value']['min_user_level_name'] = !empty($member_level_info) ? $member_level_info['level_name'] : '';
        $res['data']['value']['min_user_level_growth'] = !empty($member_level_info) ? $member_level_info['growth'] : 0;
        return $res;
    }
}