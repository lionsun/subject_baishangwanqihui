<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\sharerakeback\api\controller;

use app\api\controller\BaseApi;
use app\model\member\MemberAccount;
use app\model\order\Order as OrderModel;

/**
 * 会员价
 */
class Rebate extends BaseApi
{
	/**
	 * 分享返佣佣金显示
	 */
	public function info()
	{
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);
        $info = [];
        $order_model = new OrderModel();
        //全部佣金
        $condition = ['share_member_id' => $this->member_id ];
        $info['all_rabate'] = $order_model->getSumByField($condition,'share_rake_back_money');
        //已结算佣金
        $info['settlement_rabate'] = $order_model->getSumByField(['share_member_id'=>$this->member_id,'is_settlement'=> 1],'share_rake_back_money');
        //结算中佣金
        $info['settleing_rabate'] = $order_model->getSumByField(['share_member_id'=>$this->member_id,'is_settlement'=> 0],'share_rake_back_money');
        return $this->response($this->success($info));
	}

    /**
     * 返佣订单
     */
    public function order_list(){
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);
        $status = isset($this->params[ 'status' ]) ? $this->params[ 'status' ] : '';
        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
        $order_model = new OrderModel();
        //全部佣金
        $condition[] = ['share_member_id','=', $this->member_id ];
        if($status!==''){
            $condition[] = ['is_settlement','=', $status ];
        }
        $field = 'order_id,order_no,order_money,order_status_name,name,is_settlement,share_rake_back_money,create_time';
        $order_list = $order_model->getOrderPageList($condition,$page,$page_size,'order_id desc',$field);
        return $this->response($order_list);
    }



    /**
     * 返佣记录
     */
    public function rebate_log(){
        $token = $this->checkToken();
        if ($token[ 'code' ] < 0) return $this->response($token);
        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
        $member_account_model = new MemberAccount();
        $condition[] = ['member_id','=', $this->member_id];
        $condition[] = ['from_type','=', 'share_back'];
        $order = 'create_time desc';
        $field = '*';
        $log_list = $member_account_model->getMemberAccountPageList($condition,$page,$page_size,$order,$field);
        return $this->response($log_list);
    }
}