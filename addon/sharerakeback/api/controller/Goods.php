<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace addon\sharerakeback\api\controller;

use app\api\controller\BaseApi;
use app\model\goods\Goods as GoodsModel;
use app\model\member\Member as MemberModel;
use app\model\shop\Shop as ShopModel;

/**
 * 会员价
 */
class Goods extends BaseApi
{
	/**
	 * 基础信息
	 */
	public function info()
	{
		$sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
        $share_member_id = isset($this->params[ 'share_member_id' ]) ? $this->params[ 'share_member_id' ] : 0;
		if (empty($sku_id)) {
			return $this->response($this->error('', 'REQUEST_SKU_ID'));
		}
		$goods = new GoodsModel();
		$field = "sku_id,goods_id,sku_name,sku_spec_format,price,cost_price,market_price,discount_price,promotion_type,start_time,end_time,stock,click_num,sale_num,collect_num,sku_image,sku_images,goods_id,site_id,goods_content,goods_state,verify_state,is_virtual,is_free_shipping,goods_spec_format,goods_attr_format,introduction,unit,video_url,evaluate,category_id,category_id_1,category_id_2,category_id_3,category_name,max_buy,min_buy,source,ladder_discount_id,nthmfold_discount_id,cost_price,manjian_discount_id";
		$info = $goods->getGoodsSkuInfo([ [ 'sku_id', '=', $sku_id ] ], $field);
        $member_field = 'member_id,member_type,member_level,member_level_name';
        //登录状态显示登录后的价格
        $member_model = new MemberModel();
        $memberInfo = $member_model->getMemberInfo(['member_id' => $share_member_id],$member_field)['data'];
        if ($memberInfo['member_type']==0){//个人用户
            $member_level = $memberInfo['member_level'];
            //获取会员价
            $info['data']['member_level'] = $member_level;
            $info['data'] = $goods->getMemberPriceInfo($info['data']);
        }
		return $this->response($info);
	}


    /**
     * 会员价详情
     */
    public function detail(){
        $sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
        $share_member_id = isset($this->params[ 'share_member_id' ]) ? $this->params[ 'share_member_id' ] : 0;
        if (empty($sku_id)) {
            return $this->response($this->error('', 'REQUEST_SKU_ID'));
        }
		
		$res = [];
		
        $goods_model = new GoodsModel();
        $goods_sku_detail = $goods_model->getGoodsSkuDetail($sku_id);
        $goods_sku_detail = $goods_sku_detail[ 'data' ];
        $field = 'member_id,member_type,member_level,member_level_name';
        //登录状态显示登录后的价格
        $member_model = new MemberModel();
        $memberInfo = $member_model->getMemberInfo(['member_id' => $share_member_id],$field)['data'];
        if ($memberInfo['member_type']==0){//个人用户
            $member_level = $memberInfo['member_level'];
            $goods_sku_detail['member_level'] = $member_level;
            $goods_sku_detail = $goods_model->getMemberPriceInfo($goods_sku_detail);
        }
        $res[ 'goods_sku_detail' ] = $goods_sku_detail;
		if (empty($goods_sku_detail)) return $this->response($this->error($res));
		$goods_info = $goods_model->getGoodsInfo([['goods_id', '=', $goods_sku_detail['goods_id']]])['data'] ?? [];
		if (empty($goods_info)) return $this->response($this->error([], '找不到商品'));
		$res[ 'goods_info' ] = $goods_info;
		
		if ($goods_sku_detail['max_buy'] > 0){
		    if($this->member_id > 0){
		        $res[ 'goods_sku_detail' ]['purchased_num'] = $goods_model->getGoodsPurchasedNum($goods_sku_detail['goods_id'], $this->member_id);
		    }
		}
		//店铺信息
		$shop_model = new ShopModel();
		$shop_info = $shop_model->getShopInfo([ [ 'site_id', '=', $goods_sku_detail[ 'site_id' ] ] ], 'site_id,site_name,is_own,logo,avatar,banner,seo_description,qq,ww,telephone,shop_desccredit,shop_servicecredit,shop_deliverycredit,shop_baozh,shop_baozhopen,shop_baozhrmb,shop_qtian,shop_zhping,shop_erxiaoshi,shop_tuihuo,shop_shiyong,shop_shiti,shop_xiaoxie,shop_sales,sub_num');
		
		$shop_info = $shop_info[ 'data' ];
		$res[ 'shop_info' ] = $shop_info;
		
		return $this->response($this->success($res));
    }
}