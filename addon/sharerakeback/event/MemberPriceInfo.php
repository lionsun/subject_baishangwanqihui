<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\sharerakeback\event;


use addon\sharerakeback\model\Config as ConfigModel;
use addon\sharerakeback\model\ShareRakeBackGoodsSku as ShareRakeBackGoodsSkuModel;
use app\model\goods\Goods as GoodsModel;
use app\model\member\MemberLevel as MemberLevelModel;

/**
 * 会员价计算
 */
class MemberPriceInfo
{

    /**
     * 会员价计算
     * 
     * @return multitype:number unknown
     */
	public function handle($sku_info)
	{
	    //如果有营销活动则不考虑会员价
	    if(empty($sku_info['promotion_type'])){

            $config_model = new ConfigModel();
            $config_info = $config_model->getConfig()['data'];

            //判断该商品是否开启返佣以及返佣比例是多少
            $goods_model = new GoodsModel();
            $goods_info = $goods_model->getGoodsInfo(['goods_id'=>$sku_info['goods_id']],'is_share_rake_back,share_rake_rate')['data'];

            if($goods_info['is_share_rake_back'] == 1){
                //分享分佣配置
                $sku_info['share_rake_bake_config'] = $config_info;

                //获取会员价
                $share_model = new ShareRakeBackGoodsSkuModel();
                $member_price_info = $share_model->getShareRakeBackGoodsSkuInfo([['sku_id','=',$sku_info['sku_id']],['level_id','=',$sku_info['member_level']]],'member_price')['data'];


                //用户等级 成长值高则等级高
                $member_level_info = model('member_level')->getInfo([['level_id', '=', $sku_info['member_level']]]);

                //能否分享
                if($member_level_info['growth'] >= $config_info['value']['min_user_level_growth']){
                    $sku_info['is_share'] =  1;
                    $sku_info['member_price'] = $member_price_info['member_price'];
                    $sku_info['share_price'] = sprintf('%.2f',($sku_info['price']-$member_price_info['member_price'])*$goods_info['share_rake_rate']/100);
                    $sku_info['share_member_price'] = sprintf('%.2f',$sku_info['member_price']+$sku_info['share_price']);
                }else{
                    $sku_info['is_share'] =  0;
                    $sku_info['member_price'] = $member_price_info['member_price'];
                    $sku_info['share_price'] = 0;
                    $sku_info['share_member_price'] = $sku_info['price'];
                }

                return $sku_info;
            }
        }
	}
}