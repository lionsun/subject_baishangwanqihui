<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */
declare (strict_types = 1);

namespace addon\sharerakeback\event;

use app\model\member\MemberAccount;

/**
 * 会员价计算
 */
class OrderComplete
{

    /**
     * 会员价计算
     * 
     * @return multitype:number unknown
     */
	public function handle($condition)
	{
        $order_info = model('order')->getInfo($condition, 'share_member_id, share_rake_back_money');
        trace([
            '返佣信息1' => $order_info
        ]);
        if($order_info['share_member_id']>0 && $order_info['share_rake_back_money']>0){
            trace([
                '返佣信息22' => $order_info['share_rake_back_money']
            ]);
            $member_account_model = new MemberAccount();
            $member_account_model->addMemberAccount($order_info["share_member_id"], "balance_money", $order_info['share_rake_back_money'], "share_back", "分享返佣", "分享返佣可提现余额:" . $order_info['share_rake_back_money']);
        }
	}
}