<?php

/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsBrand as GoodsBrandModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\web\Pc as PcModel;
use app\model\shop\Config as ShopConfigModel;

/**
 * Pc端接口
 * @author Administrator
 *
 */
class Pc extends BaseApi
{
	/**
	 * 获取热门搜索关键词
	 */
	public function hotSearchWords()
	{
		$pc_model = new PcModel();
		$info = $pc_model->getHotSearchWords();
		return $this->response($this->success($info[ 'data' ][ 'value' ]));
	}

	/**
	 * 获取默认搜索关键词
	 */
	public function defaultSearchWords()
	{
		$pc_model = new PcModel();
		$info = $pc_model->getDefaultSearchWords();
		return $this->response($this->success($info[ 'data' ][ 'value' ]));
	}

	/**
	 * 获取首页浮层
	 */
	public function floatLayer()
	{
		$pc_model = new PcModel();
		$info = $pc_model->getFloatLayer();
		return $this->response($this->success($info[ 'data' ][ 'value' ]));
	}

	/**
	 * 楼层列表
	 *
	 * @return string
	 */
	public function floors()
	{
	    $view_name = input('view_name', 'NS_PC_INDEX');//自定义页面名称

		$pc_model = new PcModel();
		$condition = [
		    [ 'view_name', '=', $view_name],
			[ 'state', '=', 1 ]
		];
		$list = $pc_model->getFloorList($condition, 'pf.title,pf.value,fb.name as block_name,fb.title as block_title, pf.block_id');
		if (!empty($list[ 'data' ])) {
			foreach ($list[ 'data' ] as $k => $v) {
				$v = $pc_model->dealWithFloorInfo($v);
				$v['value'] = json_decode($v['value']);
				$list[ 'data' ][$k] = $v;
			}
		}
		return $this->response($list);
	}

	/**
	 * 获取导航
	 */
	public function navList()
	{
		$pc_model = new PcModel();
		$data = $pc_model->getNavPageList([ [ 'is_show', '=', 1 ] ], 1, 0, 'sort');
		return $this->response($data);
	}

	/**
	 * 获取友情链接
	 */
	public function friendlyLink()
	{
		$pc_model = new PcModel();
		$data = $pc_model->getlinkPageList([ [ 'is_show', '=', 1 ] ], 1, 0, 'link_sort');
		return $this->response($data);
	}

    /**
     * 商城功能
     */
	public function shopFunction()
    {
        $config_model = new ShopConfigModel();
        $config_info = $config_model->getPcShopFunctionConfig();
        return $this->response($this->success($config_info['data']['value']));
    }

    /**
     * 自定义页面信息
     */
    public function diyViewInfo()
    {
        $view_name = input('view_name', '');//自定义页面名称
        $pc_model = new PcModel();
        $info = $pc_model->getPcSiteDiyViewInfo([['name', '=', $view_name]]);
        return $this->response($info);
    }
}
