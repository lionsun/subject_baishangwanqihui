<?php

/**
 * Goodssku.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use addon\coupon\model\CouponType;
use addon\platformcoupon\model\PlatformcouponType;
use app\model\goods\Goods;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsAttribute;
use app\model\shop\Shop as ShopModel;
use app\model\member\Member as MemberModel;
use addon\vop\model\Goods as VopGoodsModel;
use addon\newfenxiao\model\FenxiaoGoods;
use addon\newfenxiao\model\FenxiaoGoodsSku;

/**
 * 商品sku
 * @author Administrator
 *
 */
class Goodssku extends BaseApi
{

	/**
	 * 基础信息
	 */
	public function info()
	{
		$sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
		if (empty($sku_id)) {
			return $this->response($this->error('', 'REQUEST_SKU_ID'));
		}
		$goods = new Goods();
		$field = 'sku_id,goods_id,sku_name,site_id,sku_spec_format,price,market_price,discount_price,promotion_type,start_time,end_time,stock,sku_image,sku_images,goods_spec_format,ladder_discount_id,source,nthmfold_discount_id,source,manjian_discount_id,new_user_coupon_id';
		$info = $goods->getGoodsSkuInfo([ [ 'sku_id', '=', $sku_id ] ], $field);
        if(!empty($info)){
            $info['data'] = $goods->getGoodsPromotionInfo($info['data']);
            $token = $this->checkToken();
            if($token['code'] >= 0){
                $field = 'member_id,member_type,member_level,member_level_name';
                //登录状态显示登录后的价格
                $member_model = new MemberModel();
                $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$field)['data'];
                if ($memberInfo['member_type']==0){//个人用户
                    $member_level = $memberInfo['member_level'];
                    //获取会员价
                    $info['data']['member_level'] = $member_level;
                    $info['data'] = $goods->getMemberPriceInfo($info['data']);
                }
            }
            if ($info['data']['source'] == 2) {
                $vop_goods_model = new VopGoodsModel();
                $data = $vop_goods_model->getVopGoodsInfo($sku_id);
                $info['data']['goods_state'] = $data['data']['status'];
                $info['data']['stock'] = $data['data']['stock'];
                $info['data']['max_buy'] = $data['data']['max_buy'];
                $info['data']['min_buy'] = $data['data']['min_buy'];
            }
        }
        //同煤买菜
        $fenxiao_member = isset($this->params[ 'fenxiao_member' ]) ? $this->params[ 'fenxiao_member' ] : 0;
        if(!empty($fenxiao_member)){

            $fenxiao_goods = new FenxiaoGoods();
            $fenxiao_goods_info = $fenxiao_goods->getMemberGoodsSku($sku_id, $fenxiao_member);
            $info['data']['price'] = $fenxiao_goods_info['data']['sale_price'];
        }
		return $this->response($info);
	}

	/**
	 * 详情信息
	 */
	public function detail()
	{
		$sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
		if (empty($sku_id)) {
			return $this->response($this->error('', 'REQUEST_SKU_ID'));
		}

		$res = [];

        $goods_model = new Goods();
		$goods_sku_detail = $goods_model->getGoodsSkuDetail($sku_id);
		$goods_sku_detail = $goods_sku_detail[ 'data' ];
        $goods_sku_detail['goods_content'] = dealWithEditorContent($goods_sku_detail['goods_content']);
        if(!empty($goods_sku_detail)){
            $goods_sku_detail = $goods_model->getGoodsPromotionInfo($goods_sku_detail);
        }
        if ($goods_sku_detail['source'] == 2) {
            $vop_goods_model = new VopGoodsModel();
            $data = $vop_goods_model->getVopGoodsInfo($sku_id);
            $goods_sku_detail['goods_state'] = $data['data']['status'];
            $goods_sku_detail['stock'] = $data['data']['stock'];
        }

		$res[ 'goods_sku_detail' ] = $goods_sku_detail;

		if (empty($goods_sku_detail)) return $this->response($this->error($res));
        $goods_info = $goods_model->getGoodsInfo([['goods_id', '=', $goods_sku_detail['goods_id']]],'*,CASE WHEN unix_timestamp(now()) > live_start_time AND unix_timestamp(now()) < live_end_time AND is_live_goods = 1 THEN \'正在进行\' WHEN unix_timestamp(now()) < live_start_time AND is_live_goods = 1 THEN \'即将开始\' WHEN unix_timestamp(now()) > live_end_time AND is_live_goods = 1 THEN \'已经结束\' ELSE \'未参与直播\' END AS live_status')['data'] ?? [];
        $token = $this->checkToken();
        if($token['code'] >= 0){
            $field = 'member_id,member_type,member_level,member_level_name';
            //登录状态显示登录后的价格
            $member_model = new MemberModel();
            $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$field)['data'];
            if ($memberInfo['member_type'] == 0){//个人用户
                $member_level = $memberInfo['member_level'];
                $goods_sku_detail['member_level'] = $member_level;
                $goods_sku_detail = $goods_model->getMemberPriceInfo($goods_sku_detail);
            }
        }
        if (empty($goods_info)) return $this->response($this->error([], '找不到商品'));
        $res[ 'goods_info' ] = $goods_info;
        $res[ 'goods_sku_detail' ] = $goods_sku_detail;
        if ($goods_sku_detail['max_buy'] > 0){
            if($this->member_id > 0){
                $res[ 'goods_sku_detail' ]['purchased_num'] = $goods_model->getGoodsPurchasedNum($goods_sku_detail['goods_id'], $this->member_id);
            }
        }
		//店铺信息
		$shop_model = new ShopModel();
		$shop_info = $shop_model->getShopInfo([ [ 'site_id', '=', $goods_sku_detail[ 'site_id' ] ] ], 'site_id,site_name,is_own,logo,avatar,banner,seo_description,qq,ww,telephone,shop_desccredit,shop_servicecredit,shop_deliverycredit,shop_baozh,shop_baozhopen,shop_baozhrmb,shop_qtian,shop_zhping,shop_erxiaoshi,shop_tuihuo,shop_shiyong,shop_shiti,shop_xiaoxie,shop_sales,sub_num');

		$shop_info = $shop_info[ 'data' ];
		$res[ 'shop_info' ] = $shop_info;

        //同煤买菜
        $fenxiao_member = isset($this->params[ 'fenxiao_member' ]) ? $this->params[ 'fenxiao_member' ] : 0;
        if(!empty($fenxiao_member)){

            $fenxiao_goods = new FenxiaoGoods();
            $member_goods_sku_info = $fenxiao_goods->getMemberGoodsSku($sku_id, $fenxiao_member);

            $res[ 'goods_sku_detail' ]['price'] = $member_goods_sku_info['data']['sale_price'];

        }

        return $this->response($this->success($res));
	}

	public function lists()
	{
		$sku_ids = isset($this->params[ 'sku_ids' ]) ? $this->params[ 'sku_ids' ] : '';
		$field = 'goods_id,sku_id,sku_name,price,market_price,discount_price,stock,sale_num,sku_image,goods_name,site_id,website_id,is_own,is_free_shipping,introduction,promotion_type,is_virtual,ladder_discount_id,nthmfold_discount_id,source,subscription_open_state,corporate_subscription,subscription_status,manjian_discount_id,new_user_coupon_id';
		$goods = new Goods();
		$condition = [
			[ 'sku_id', 'in', $sku_ids ]
		];
		$order_by = 'sort desc,create_time desc';
		$list = $goods->getGoodsSkuList($condition, $field, $order_by);
		foreach($list['data'] as $key=>$val){
		    $list['data'][$key]['promotion_info'] = $goods->getGoodsPromotionInfo($val);
            $token = $this->checkToken();
            if($token['code'] >= 0){
                $field = 'member_id,member_type,member_level,member_level_name';
                //登录状态显示登录后的价格
                $member_model = new MemberModel();
                $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$field)['data'];
                if ($memberInfo['member_type']==0){//个人用户
                    $member_level = $memberInfo['member_level'];
                    $list['data'][$key]['member_level'] = $member_level;
                    $list['data'][$key] = $goods->getMemberPriceInfo($list['data'][$key]);
                }
            }
        }
		return $this->response($list);
	}

	/**
	 * 列表信息
	 */
	public function pageOld()
	{
	    //检测token
        $token = $this->checkToken();

		$page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
		$page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
		$site_id = isset($this->params[ 'site_id' ]) ? $this->params[ 'site_id' ] : 0; //站点id
		$goods_id_arr = isset($this->params[ 'goods_id_arr' ]) ? $this->params[ 'goods_id_arr' ] : ''; //sku_id数组
		$keyword = isset($this->params[ 'keyword' ]) ? $this->params[ 'keyword' ] : ''; //关键词
		$category_id = isset($this->params[ 'category_id' ]) ? $this->params[ 'category_id' ] : 0; //分类
		$brand_id = isset($this->params[ 'brand_id' ]) ? $this->params[ 'brand_id' ] : 0; //品牌
		$min_price = isset($this->params[ 'min_price' ]) ? $this->params[ 'min_price' ] : 0; //价格区间，小
		$max_price = isset($this->params[ 'max_price' ]) ? $this->params[ 'max_price' ] : 0; //价格区间，大
		$is_free_shipping = isset($this->params[ 'is_free_shipping' ]) ? $this->params[ 'is_free_shipping' ] : -1; //是否免邮
		$is_own = isset($this->params[ 'is_own' ]) ? $this->params[ 'is_own' ] : ''; //是否自营
		$order = isset($this->params[ 'order' ]) ? $this->params[ 'order' ] : "create_time"; //排序（综合、销量、价格）
		$sort = isset($this->params[ 'sort' ]) ? $this->params[ 'sort' ] : "desc"; //升序、降序
		$attr = isset($this->params[ 'attr' ]) ? $this->params[ 'attr' ] : ""; //属性json
		$shop_category_id = isset($this->params[ 'shop_category_id' ]) ? $this->params[ 'shop_category_id' ] : 0;//店内分类


		$field = 'gs.goods_id,gs.sku_id,gs.sku_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.goods_name,gs.site_id,gs.website_id,gs.is_own,gs.is_free_shipping,gs.introduction,gs.promotion_type,g.goods_image,g.site_name,gs.goods_spec_format,gs.is_virtual,gs.ladder_discount_id,gs.source,gs.nthmfold_discount_id,gs.subscription_open_state,gs.corporate_subscription,gs.subscription_status,gs.nthmfold_discount_id,gs.manjian_discount_id,gs.new_user_coupon_id';

		$alias = 'gs';
		$join = [
			[ 'goods g', 'gs.sku_id = g.sku_id', 'inner' ],
		];

        $condition = [];
        $condition[] = [ 'gs.is_delete', '=', 0 ];
        $condition[] = [ 'gs.goods_state', '=', 1 ];
        $condition[] = [ 'gs.verify_state', '=', 1 ];

		if (!empty($site_id)) {
			$condition[] = [ 'gs.site_id', '=', $site_id ];
		}

		if (!empty($goods_id_arr)) {
			$condition[] = [ 'gs.goods_id', 'in', $goods_id_arr ];
		}

		if (!empty($keyword)) {
			//$condition[] = [ 'g.goods_name|gs.sku_name|gs.keywords', 'like', '%' . $keyword . '%' ];
			$condition[] = [ 'g.goods_name|gs.sku_name|gs.keywords', 'regexp', $keyword ];
		}

		if (!empty($category_id)) {
			$condition[] = [ 'gs.category_id|gs.category_id_1|gs.category_id_2|gs.category_id_3', '=', $category_id ];
		}

		if (!empty($brand_id)) {
			$condition[] = [ 'gs.brand_id', '=', $brand_id ];
		}

		if ($min_price != "" && $max_price != "") {
			$condition[] = [ 'gs.discount_price', 'between', [ $min_price, $max_price ] ];
		} elseif ($min_price != "") {
			$condition[] = [ 'gs.discount_price', '>=', $min_price ];
		} elseif ($max_price != "") {
			$condition[] = [ 'gs.discount_price', '<=', $max_price ];
		}

		if (isset($is_free_shipping) && !empty($is_free_shipping) && $is_free_shipping > -1) {
			$condition[] = [ 'gs.is_free_shipping', '=', $is_free_shipping ];
		}

		if ($is_own !== '') {
			$condition[] = [ 'gs.is_own', '=', $is_own ];
		}

		// 非法参数进行过滤
		if ($sort != "desc" && $sort != "asc") {
			$sort = "";
		}

		// 非法参数进行过滤
		if ($order != '') {
			if ($order != "sale_num" && $order != "discount_price") {
				$order = 'gs.create_time';
			} else {
				$order = 'gs.' . $order;
			}
			$order_by = $order . ' ' . $sort;
		} else {
			//$order_by = 'gs.sort desc,gs.create_time desc';
			$order_by = 'gs.sku_id desc';
		}

		//拿到商品属性，查询sku_id
		if (!empty($attr)) {
			$attr = json_decode($attr, true);
			$attr_id = [];
			$attr_value_id = [];
			foreach ($attr as $k => $v) {
				$attr_id[] = $v[ 'attr_id' ];
				$attr_value_id[] = $v[ 'attr_value_id' ];
			}
			$goods_attribute = new GoodsAttribute();
			$attribute_condition = [
				[ 'attr_id', 'in', implode(",", $attr_id) ],
				[ 'attr_value_id', 'in', implode(",", $attr_value_id) ],
                [ 'app_module', '=', 'shop']
			];
			$attribute_list = $goods_attribute->getAttributeIndexList($attribute_condition, 'sku_id');
			$attribute_list = $attribute_list[ 'data' ];
			if (!empty($attribute_list)) {
				$sku_id = [];
				foreach ($attribute_list as $k => $v) {
					$sku_id[] = $v[ 'sku_id' ];
				}
				$condition[] = [
					[ 'gs.sku_id', 'in', implode(",", $sku_id) ]
				];
			}
		}

		// 优惠券
		$coupon = isset($this->params[ 'coupon_type' ]) ? $this->params[ 'coupon_type' ] : 0; //优惠券
		if ($coupon > 0) {
			$coupon_type = new CouponType();
			$coupon_type_info = $coupon_type->getInfo([
				[ 'coupon_type_id', '=', $coupon ],
				[ 'site_id', '=', $site_id ],
				[ 'goods_type', '=', 2 ]
			], 'goods_ids');
			$coupon_type_info = $coupon_type_info[ 'data' ];
			if (isset($coupon_type_info[ 'goods_ids' ]) && !empty($coupon_type_info[ 'goods_ids' ])) {
				$condition[] = [ 'gs.goods_id', 'in', explode(',', trim($coupon_type_info[ 'goods_ids' ], ',')) ];
			}
		}
		//平台优惠券
		$platform_coupon = isset($this->params[ 'platform_coupon_type' ]) ? $this->params[ 'platform_coupon_type' ] : 0; //平台优惠券
		if ($platform_coupon > 0) {
			$platform_coupon_type = new PlatformcouponType();
			$platform_coupon_type_info = $platform_coupon_type->getInfo([
				[ 'platformcoupon_type_id', '=', $platform_coupon ],
				[ 'use_scenario', '=', 2 ]
			], 'group_ids');
			$platform_coupon_type_info = $platform_coupon_type_info[ 'data' ];
			$shop_model = new ShopModel();
			$shop_id_arr = $shop_model->getShopColumn([['group_id', 'in', $platform_coupon_type_info[ 'group_ids' ]]], 'site_id')['data'];
            if (!empty($platform_coupon_type_info)) {
                $condition[] = [ 'gs.site_id', 'in', $shop_id_arr ];
            }
		}
        //店内分类
		if ($shop_category_id > 0) {
			$condition[] = [ 'gs.goods_shop_category_ids', 'like', [ $shop_category_id, '%' . $shop_category_id . ',%', '%' . $shop_category_id, '%,' . $shop_category_id . ',%' ], 'or' ];
		}

		$goods = new Goods();
		$list = $goods->getGoodsSkuPageList($condition, $page, $page_size, $order_by, $field, $alias, $join);
        foreach($list['data']['list'] as $key=>$val){
            $list['data']['list'][$key] = $goods->getGoodsPromotionInfo($val);
            if($token['code'] >= 0){
                $member_field = 'member_id,member_type,member_level,member_level_name';
                //登录状态显示登录后的价格
                $member_model = new MemberModel();
                $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$member_field)['data'];
                if ($memberInfo['member_type']==0){//个人用户
                    $member_level = $memberInfo['member_level'];
                    $list['data']['list'][$key]['member_level'] = $member_level;
                    $list['data']['list'][$key] = $goods->getMemberPriceInfo($list['data']['list'][$key]);
                }
            }
        }
        //查看查询条件
        $list['condition'] = $condition;
		return $this->response($list);
	}


    /**
     * 列表信息
     */
    public function page()
    {
        //检测token
        $token = $this->checkToken();

        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
        $site_id = isset($this->params[ 'site_id' ]) ? $this->params[ 'site_id' ] : 0; //站点id
        $goods_id_arr = isset($this->params[ 'goods_id_arr' ]) ? $this->params[ 'goods_id_arr' ] : ''; //sku_id数组
        $keyword = isset($this->params[ 'keyword' ]) ? $this->params[ 'keyword' ] : ''; //关键词
        $category_id = isset($this->params[ 'category_id' ]) ? $this->params[ 'category_id' ] : 0; //分类
        $brand_id = isset($this->params[ 'brand_id' ]) ? $this->params[ 'brand_id' ] : 0; //品牌
        $min_price = isset($this->params[ 'min_price' ]) ? $this->params[ 'min_price' ] : 0; //价格区间，小
        $max_price = isset($this->params[ 'max_price' ]) ? $this->params[ 'max_price' ] : 0; //价格区间，大
        $is_free_shipping = isset($this->params[ 'is_free_shipping' ]) ? $this->params[ 'is_free_shipping' ] : -1; //是否免邮
        $is_own = isset($this->params[ 'is_own' ]) ? $this->params[ 'is_own' ] : ''; //是否自营
        $order = isset($this->params[ 'order' ]) ? $this->params[ 'order' ] : "create_time"; //排序（综合、销量、价格）
        $sort = isset($this->params[ 'sort' ]) ? $this->params[ 'sort' ] : "desc"; //升序、降序
        $attr = isset($this->params[ 'attr' ]) ? $this->params[ 'attr' ] : ""; //属性json
        $shop_category_id = isset($this->params[ 'shop_category_id' ]) ? $this->params[ 'shop_category_id' ] : 0;//店内分类

        $field = 'g.goods_id,g.sku_id,g.goods_name,g.goods_image as sku_image,g.price,gs.market_price,gs.discount_price,g.goods_stock,g.sale_num,g.site_id,g.website_id,g.is_own,g.is_free_shipping,g.introduction,g.promotion_type,g.site_name,g.goods_spec_format,g.is_virtual,g.ladder_discount_id,g.source,g.nthmfold_discount_id,g.subscription_open_state,g.corporate_subscription,g.subscription_status,g.nthmfold_discount_id,g.manjian_discount_id,g.new_user_coupon_id,g.is_live_goods,g.live_start_time,g.live_end_time,g.platform_name,g.live_link,CASE WHEN unix_timestamp(now()) > g.live_start_time AND unix_timestamp(now()) < g.live_end_time AND g.is_live_goods = 1 THEN \'正在进行\' WHEN unix_timestamp(now()) < g.live_start_time AND g.is_live_goods = 1 THEN \'即将开始\' WHEN unix_timestamp(now()) > g.live_end_time AND g.is_live_goods = 1 THEN \'已经结束\' ELSE \'未参与直播\' END AS live_status,CASE WHEN unix_timestamp(now()) > g.live_start_time AND unix_timestamp(now()) < g.live_end_time THEN 1 ELSE 0 END AS is_live';
        $alias = 'g';
        $join = [
            ['goods_sku gs','g.sku_id = gs.sku_id','inner'],
            ['shop s','g.site_id = s.site_id','inner']
        ];
        $condition = [];
        $condition[] = [ 'g.is_delete', '=', 0 ];
        $condition[] = [ 'g.goods_state', '=', 1 ];
        $condition[] = [ 'gs.goods_state', '=', 1 ];
        $condition[] = [ 'g.verify_state', '=', 1 ];
        $condition[] = [ 'gs.verify_state', '=', 1 ];

        if (!empty($site_id)) {
            $condition[] = [ 'g.site_id', '=', $site_id ];
        }

        if (!empty($goods_id_arr)) {
            $condition[] = [ 'g.goods_id', 'in', $goods_id_arr ];
        }

        if (!empty($keyword)) {
            $condition[] = [ 'g.goods_name', 'like', '%' . $keyword . '%' ];
            //$condition[] = [ '', 'exp', \think\facade\Db::raw("instr(goods_name, '{$keyword}')") ];
        }

        if (!empty($category_id)) {
            $condition[] = [ 'g.category_id|g.category_id_1|g.category_id_2|g.category_id_3', '=', $category_id ];
        }

        if (!empty($brand_id)) {
            $condition[] = [ 'g.brand_id', '=', $brand_id ];
        }

        if ($min_price != "" && $max_price != "") {
            $condition[] = [ 'gs.discount_price', 'between', [ $min_price, $max_price ] ];
        } elseif ($min_price != "") {
            $condition[] = [ 'gs.discount_price', '>=', $min_price ];
        } elseif ($max_price != "") {
            $condition[] = [ 'gs.discount_price', '<=', $max_price ];
        }

        if (isset($is_free_shipping) && !empty($is_free_shipping) && $is_free_shipping > -1) {
            $condition[] = [ 'g.is_free_shipping', '=', $is_free_shipping ];
        }

        if ($is_own !== '') {
            $condition[] = [ 'g.is_own', '=', $is_own ];
        }

        // 非法参数进行过滤
        if ($sort != "desc" && $sort != "asc") {
            $sort = "";
        }

        // 非法参数进行过滤
        if ($order != '') {
            if ($order != "sale_num" && $order != "discount_price") {
                $order = 'g.sku_id';
            }
            $order_by = $order . ' ' . $sort;
        } else {
            //$order_by = 'gs.sort desc,gs.create_time desc';
            $order_by = 'g.sku_id desc';
        }

        //拿到商品属性，查询sku_id
        if (!empty($attr)) {
            $attr = json_decode($attr, true);
            $attr_id = [];
            $attr_value_id = [];
            foreach ($attr as $k => $v) {
                $attr_id[] = $v[ 'attr_id' ];
                $attr_value_id[] = $v[ 'attr_value_id' ];
            }
            $goods_attribute = new GoodsAttribute();
            $attribute_condition = [
                [ 'attr_id', 'in', implode(",", $attr_id) ],
                [ 'attr_value_id', 'in', implode(",", $attr_value_id) ],
                [ 'app_module', '=', 'shop']
            ];
            $attribute_list = $goods_attribute->getAttributeIndexList($attribute_condition, 'sku_id');
            $attribute_list = $attribute_list[ 'data' ];
            if (!empty($attribute_list)) {
                $sku_id = [];
                foreach ($attribute_list as $k => $v) {
                    $sku_id[] = $v[ 'sku_id' ];
                }
                $condition[] = [
                    [ 'g.sku_id', 'in', implode(",", $sku_id) ]
                ];
            }
        }

        // 优惠券
        $coupon = isset($this->params[ 'coupon_type' ]) ? $this->params[ 'coupon_type' ] : 0; //优惠券
        if ($coupon > 0) {
            $coupon_type = new CouponType();
            $coupon_type_info = $coupon_type->getInfo([
                [ 'coupon_type_id', '=', $coupon ],
                [ 'site_id', '=', $site_id ],
                [ 'goods_type', '=', 2 ]
            ], 'goods_ids');
            $coupon_type_info = $coupon_type_info[ 'data' ];
            if (isset($coupon_type_info[ 'goods_ids' ]) && !empty($coupon_type_info[ 'goods_ids' ])) {
                $condition[] = [ 'g.goods_id', 'in', explode(',', trim($coupon_type_info[ 'goods_ids' ], ',')) ];
            }
        }

        //平台优惠券
        $platform_coupon = isset($this->params[ 'platform_coupon_type' ]) ? $this->params[ 'platform_coupon_type' ] : 0; //平台优惠券
        if ($platform_coupon > 0) {
            $platform_coupon_type = new PlatformcouponType();
            $platform_coupon_type_info = $platform_coupon_type->getInfo([
                [ 'platformcoupon_type_id', '=', $platform_coupon ],
                [ 'use_scenario', '=', 2 ]
            ], 'group_ids');
            $platform_coupon_type_info = $platform_coupon_type_info[ 'data' ];
            $shop_model = new ShopModel();
            $shop_id_arr = $shop_model->getShopColumn([['group_id', 'in', $platform_coupon_type_info[ 'group_ids' ]]], 'site_id')['data'];
            if (!empty($platform_coupon_type_info)) {
                $condition[] = [ 'g.site_id', 'in', $shop_id_arr ];
            }
        }
        //店内分类
        if ($shop_category_id > 0) {
            $condition[] = [ 'g.goods_shop_category_ids', 'like', [ $shop_category_id, '%' . $shop_category_id . ',%', '%' . $shop_category_id, '%,' . $shop_category_id . ',%' ], 'or' ];
        }

        $goods = new Goods();
        $list = $goods->getGoodsPageList($condition, $page, $page_size, $order_by, $field,$alias,$join);
        //$sql = \think\facade\Db::name('goods')->getLastSql();

        foreach($list['data']['list'] as $key=>$val){
            $list['data']['list'][$key] = $goods->getGoodsPromotionInfo($val);
            if($token['code'] >= 0){
                $member_field = 'member_id,member_type,member_level,member_level_name';
                //登录状态显示登录后的价格
                $member_model = new MemberModel();
                $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$member_field)['data'];
                if ($memberInfo['member_type']==0){//个人用户
                    $member_level = $memberInfo['member_level'];
                    $list['data']['list'][$key]['member_level'] = $member_level;
                    $list['data']['list'][$key] = $goods->getMemberPriceInfo($list['data']['list'][$key]);
                }
            }
        }
        //查看查询条件
        //$list['condition'] = $condition;
        //$list['sql'] = $sql;
        //$list['order'] = $order_by;
        return $this->response($list);
    }

    /**
     * 列表信息
     */
    public function pageO()
    {
        //检测token
        $token = $this->checkToken();

        $page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
        $page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
        $site_id = isset($this->params[ 'site_id' ]) ? $this->params[ 'site_id' ] : 0; //站点id
        $goods_id_arr = isset($this->params[ 'goods_id_arr' ]) ? $this->params[ 'goods_id_arr' ] : ''; //sku_id数组
        $keyword = isset($this->params[ 'keyword' ]) ? $this->params[ 'keyword' ] : ''; //关键词
        $category_id = isset($this->params[ 'category_id' ]) ? $this->params[ 'category_id' ] : 0; //分类
        $brand_id = isset($this->params[ 'brand_id' ]) ? $this->params[ 'brand_id' ] : 0; //品牌
        $min_price = isset($this->params[ 'min_price' ]) ? $this->params[ 'min_price' ] : 0; //价格区间，小
        $max_price = isset($this->params[ 'max_price' ]) ? $this->params[ 'max_price' ] : 0; //价格区间，大
        $is_free_shipping = isset($this->params[ 'is_free_shipping' ]) ? $this->params[ 'is_free_shipping' ] : -1; //是否免邮
        $is_own = isset($this->params[ 'is_own' ]) ? $this->params[ 'is_own' ] : ''; //是否自营
        $order = isset($this->params[ 'order' ]) ? $this->params[ 'order' ] : "create_time"; //排序（综合、销量、价格）
        $sort = isset($this->params[ 'sort' ]) ? $this->params[ 'sort' ] : "desc"; //升序、降序
        $attr = isset($this->params[ 'attr' ]) ? $this->params[ 'attr' ] : ""; //属性json
        $shop_category_id = isset($this->params[ 'shop_category_id' ]) ? $this->params[ 'shop_category_id' ] : 0;//店内分类

        $field = 'goods_id,sku_id,sku_name,price,market_price,discount_price,stock,sale_num,sku_image,goods_name,site_id,website_id,is_own,is_free_shipping,introduction,promotion_type,sku_image as goods_image,site_name,goods_spec_format,is_virtual,ladder_discount_id,source,nthmfold_discount_id,subscription_open_state,corporate_subscription,subscription_status,nthmfold_discount_id,manjian_discount_id,new_user_coupon_id';

        $condition = [];
        $condition[] = [ 'is_delete', '=', 0 ];
        $condition[] = [ 'goods_state', '=', 1 ];
        $condition[] = [ 'verify_state', '=', 1 ];

        if (!empty($site_id)) {
            $condition[] = [ 'site_id', '=', $site_id ];
        }

        if (!empty($goods_id_arr)) {
            $condition[] = [ 'goods_id', 'in', $goods_id_arr ];
        }

        if (!empty($keyword)) {
            $condition[] = [ 'sku_name', 'like', '%' . $keyword . '%' ];
            //$condition[] = [ '', 'exp', \think\facade\Db::raw("instr(sku_name, '{$keyword}')") ];
        }

        if (!empty($category_id)) {
            $condition[] = [ 'category_id|category_id_1|category_id_2|category_id_3', '=', $category_id ];
        }

        if (!empty($brand_id)) {
            $condition[] = [ 'brand_id', '=', $brand_id ];
        }

        if ($min_price != "" && $max_price != "") {
            $condition[] = [ 'discount_price', 'between', [ $min_price, $max_price ] ];
        } elseif ($min_price != "") {
            $condition[] = [ 'discount_price', '>=', $min_price ];
        } elseif ($max_price != "") {
            $condition[] = [ 'discount_price', '<=', $max_price ];
        }

        if (isset($is_free_shipping) && !empty($is_free_shipping) && $is_free_shipping > -1) {
            $condition[] = [ 'is_free_shipping', '=', $is_free_shipping ];
        }

        if ($is_own !== '') {
            $condition[] = [ 'is_own', '=', $is_own ];
        }

        // 非法参数进行过滤
        if ($sort != "desc" && $sort != "asc") {
            $sort = "";
        }

        // 非法参数进行过滤
        if ($order != '') {
            if ($order != "sale_num" && $order != "discount_price") {
                $order = 'sku_id';
            }
            $order_by = $order . ' ' . $sort;
        } else {
            //$order_by = 'gs.sort desc,gs.create_time desc';
            $order_by = 'sku_id desc';
        }

        //拿到商品属性，查询sku_id
        if (!empty($attr)) {
            $attr = json_decode($attr, true);
            $attr_id = [];
            $attr_value_id = [];
            foreach ($attr as $k => $v) {
                $attr_id[] = $v[ 'attr_id' ];
                $attr_value_id[] = $v[ 'attr_value_id' ];
            }
            $goods_attribute = new GoodsAttribute();
            $attribute_condition = [
                [ 'attr_id', 'in', implode(",", $attr_id) ],
                [ 'attr_value_id', 'in', implode(",", $attr_value_id) ],
                [ 'app_module', '=', 'shop']
            ];
            $attribute_list = $goods_attribute->getAttributeIndexList($attribute_condition, 'sku_id');
            $attribute_list = $attribute_list[ 'data' ];
            if (!empty($attribute_list)) {
                $sku_id = [];
                foreach ($attribute_list as $k => $v) {
                    $sku_id[] = $v[ 'sku_id' ];
                }
                $condition[] = [
                    [ 'sku_id', 'in', implode(",", $sku_id) ]
                ];
            }
        }

        // 优惠券
        $coupon = isset($this->params[ 'coupon_type' ]) ? $this->params[ 'coupon_type' ] : 0; //优惠券
        if ($coupon > 0) {
            $coupon_type = new CouponType();
            $coupon_type_info = $coupon_type->getInfo([
                [ 'coupon_type_id', '=', $coupon ],
                [ 'site_id', '=', $site_id ],
                [ 'goods_type', '=', 2 ]
            ], 'goods_ids');
            $coupon_type_info = $coupon_type_info[ 'data' ];
            if (isset($coupon_type_info[ 'goods_ids' ]) && !empty($coupon_type_info[ 'goods_ids' ])) {
                $condition[] = [ 'goods_id', 'in', explode(',', trim($coupon_type_info[ 'goods_ids' ], ',')) ];
            }
        }

        //平台优惠券
        $platform_coupon = isset($this->params[ 'platform_coupon_type' ]) ? $this->params[ 'platform_coupon_type' ] : 0; //平台优惠券
        if ($platform_coupon > 0) {
            $platform_coupon_type = new PlatformcouponType();
            $platform_coupon_type_info = $platform_coupon_type->getInfo([
                [ 'platformcoupon_type_id', '=', $platform_coupon ],
                [ 'use_scenario', '=', 2 ]
            ], 'group_ids');
            $platform_coupon_type_info = $platform_coupon_type_info[ 'data' ];
            $shop_model = new ShopModel();
            $shop_id_arr = $shop_model->getShopColumn([['group_id', 'in', $platform_coupon_type_info[ 'group_ids' ]]], 'site_id')['data'];
            if (!empty($platform_coupon_type_info)) {
                $condition[] = [ 'site_id', 'in', $shop_id_arr ];
            }
        }
        //店内分类
        if ($shop_category_id > 0) {
            $condition[] = [ 'goods_shop_category_ids', 'like', [ $shop_category_id, '%' . $shop_category_id . ',%', '%' . $shop_category_id, '%,' . $shop_category_id . ',%' ], 'or' ];
        }

        $goods = new Goods();
        $list = $goods->getGoodsSkuPageList($condition, $page, $page_size, $order_by, $field);
        //$sql = \think\facade\Db::name('goods')->getLastSql();

        foreach($list['data']['list'] as $key=>$val){
            $list['data']['list'][$key] = $goods->getGoodsPromotionInfo($val);
            if($token['code'] >= 0){
                $member_field = 'member_id,member_type,member_level,member_level_name';
                //登录状态显示登录后的价格
                $member_model = new MemberModel();
                $memberInfo = $member_model->getMemberInfo(['member_id' => $this->member_id],$member_field)['data'];
                if ($memberInfo['member_type']==0){//个人用户
                    $member_level = $memberInfo['member_level'];
                    $list['data']['list'][$key]['member_level'] = $member_level;
                    $list['data']['list'][$key] = $goods->getMemberPriceInfo($list['data']['list'][$key]);
                }
            }
        }
        //查看查询条件
        //$list['condition'] = $condition;
        //$list['sql'] = $sql;
        //$list['order'] = $order_by;
        return $this->response($list);
    }

	/**
	 * 商品推荐
	 * @return string
	 */
	public function recommend()
	{
		$page = isset($this->params[ 'page' ]) ? $this->params[ 'page' ] : 1;
		$page_size = isset($this->params[ 'page_size' ]) ? $this->params[ 'page_size' ] : PAGE_LIST_ROWS;
		$condition = [
            [ 'g.is_delete', '=', 0 ],
			[ 'g.goods_state', '=', 1 ],
			[ 'g.verify_state', '=', 1 ],
		];
		$goods = new Goods();
		$field = 'gs.goods_id,gs.sku_id,gs.sku_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.goods_name,gs.site_id,gs.website_id,gs.is_own,gs.is_free_shipping,gs.introduction,gs.promotion_type,g.goods_image,gs.ladder_discount_id,gs.source,gs.nthmfold_discount_id,gs.manjian_discount_id,gs.new_user_coupon_id';
		$alias = 'gs';
		$join = [
			[ 'goods g', 'gs.sku_id = g.sku_id', 'left' ]
		];
		//$order_by = 'gs.sort desc,gs.create_time desc';
        $order_by = 'gs.sku_id desc';
        //只查看处于开启状态的店铺
        //$join[] = [ 'shop s', 's.site_id = gs.site_id', 'inner'];
        //$condition[] = ['s.shop_status', '=', 1];

		$list = $goods->getGoodsSkuList($condition, $field, $order_by, 5, $alias, $join);
        $list['data']['list'] = $list['data'];
		return $this->response($list);
	}

	/**
	 * 商品二维码
	 * return
	 */
	public function goodsQrcode()
	{
		$sku_id = isset($this->params[ 'sku_id' ]) ? $this->params[ 'sku_id' ] : 0;
		if (empty($sku_id)) {
			return $this->response($this->error('', 'REQUEST_SKU_ID'));
		}
		$goods_model = new GoodsModel();
		$goods_sku_info = $goods_model->getGoodsSkuInfo([ [ 'sku_id', '=', $sku_id ] ], 'sku_id,goods_name');
		$goods_sku_info = $goods_sku_info[ 'data' ];
		$res = $goods_model->qrcode($goods_sku_info[ 'sku_id' ], $goods_sku_info[ 'goods_name' ]);
		return $this->response($res);
	}
}
