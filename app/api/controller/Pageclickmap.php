<?php

namespace app\api\controller;

use app\model\system\PageClickmap as ClickmapModel;

/**
 * 页面点击地图数据接口
 * @author wangjx
 */
class PageClickmap extends BaseApi
{
	/**
	 * 添加点击地图
	 * @return mixed
	 */
	public function addClickmap(){
        if (request()->isAjax()) {
            $data['x'] = isset($this->params['x']) ? $this->params['x'] : '';
            $data['y'] = isset($this->params['y']) ? $this->params['y'] : '';
            $data['page_name'] = "";
            $data['location'] = $_SERVER['HTTP_REFERER'];
            $data['type'] = 1;
            $data['click_num'] = 1;
            $data['create_time'] = time();

            $clickmap = new ClickmapModel();
            $info = $clickmap->addClickmap($data);
            return $this->response($info);
        }
    }

    /**
     * 通过页面链接获取点击地图
     * @return mixed
     */
    public function getClickmapByUrl(){
        if (request()->isAjax()) {
            $url = isset($this->params['url_']) ? $this->params['url_'] : 0;
            $start_time = isset($this->params['start_time']) ? $this->params['start_time'] : '';
            $end_time = isset($this->params['end_time']) ? $this->params['end_time'] : '';

            $clickmap_model = new ClickmapModel();
            $info = $clickmap_model->getClickmapByUrl($url, $start_time, $end_time);
            return $this->response($info);
        }
    }
}