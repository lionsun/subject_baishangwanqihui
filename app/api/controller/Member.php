<?php

/**
 * Member.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use addon\coupon\model\MemberCoupon;
use addon\platformcoupon\model\MemberPlatformcoupon;
use addon\newusercoupon\model\Coupon as NewUserCoupon;
use app\model\member\Member as MemberModel;
use app\model\member\MemberBankAccount as MemberBankAccountModel;
use app\model\member\MemberLevel;
use app\model\member\MemberLevel as MemberLevelModel;
use app\model\member\Register as RegisterModel;
use app\model\message\Message;
use think\facade\Cache;
use addon\pickpoint\model\Pickpoint as Pickpoint;

class Member extends BaseApi
{
    /**
     * 基础信息
     */
    public function detail()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo(
            [['member_id', '=', $this->member_id]],
            'member_id,source_member,username,nickname,mobile,email,status,headimg,member_level,member_level_name,member_label,member_label_name,sex,location,birthday,point,balance,balance_money,growth,sign_days_series,member_type,check_status,reason,business_id,invite_code,fenxiao_level'
        );
        $member_level_model = new MemberLevelModel();
        $info['data']['discount'] = $member_level_model->getMemberLevelInfo([['level_id','=',$info['data']['member_level']]])['data']['discount'];

        if (!empty($info['data'])) {
            $info['data']['password'] = empty($info['data']['password']) ? 0 : 1;
            $pick_point_model = new Pickpoint();
            $pick_point_info = $pick_point_model->getPickpointInfo([['member_id','=',$this->member_id]]);
            if($info['data']['member_type']==1){
                $cert_info = $member_model->getMemberCertInfo([['member_id', '=', $this->member_id]], '*');
                if($cert_info['data']){
                    $info['data'] = array_merge($info['data'],$cert_info['data']);
                }else{
                    $info['data']['cert_id'] = 0;
                }
            }
            if($pick_point_info['data']){
                $info['data']['is_pickpoint'] = 1;
            }else{
                $info['data']['is_pickpoint'] = 0;
            }
        }

        return $this->response($info);
    }

    /**
     * 基础信息
     */
    public function info()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo([['member_id', '=', $token['data']['member_id']]], 'member_id,source_member,username,nickname,mobile,email,password,status,headimg,member_level,member_level_name,member_label,member_label_name,qq,qq_openid,wx_openid,wx_unionid,ali_openid,baidu_openid,toutiao_openid,douyin_openid,realname,sex,location,birthday,point,balance,balance_money,growth,sign_days_series,invite_code');
        if (!empty($info['data'])) {
            $info['data']['password'] = empty($info['data']['password']) ? 0 : 1;
        }

        return $this->response($info);
    }

    /**
     * 商业用户基础信息
     */
    public function userInfo()
    {
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;

        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo([['member_id', '=', $member_id]], 'member_id,source_member,username,nickname,mobile,email,password,status,headimg,member_level,member_level_name,member_label,member_label_name,qq,qq_openid,wx_openid,wx_unionid,ali_openid,baidu_openid,toutiao_openid,douyin_openid,realname,sex,location,birthday,point,balance,balance_money,growth,sign_days_series');
        return $this->response($info);
    }

    /**
     * 修改会员头像
     * @return string
     */
    public function modifyheadimg()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $headimg = isset($this->params['headimg']) ? $this->params['headimg'] : '';
        $member_model = new MemberModel();
        $res = $member_model->editMember(['headimg' => $headimg], [['member_id', '=', $token['data']['member_id']]]);
        event('ModifyMemberHeadimg', ['member_id' => $this->member_id]);
        return $this->response($res);
    }

    /**
     * 修改昵称
     * @return string
     */
    public function modifynickname()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $nickname = isset($this->params['nickname']) ? $this->params['nickname'] : '';
        $member_model = new MemberModel();
        $res = $member_model->editMember(['nickname' => $nickname], [['member_id', '=', $token['data']['member_id']]]);
        return $this->response($res);
    }

    /**
     * 修改手机号
     * @return string
     */
    public function modifymobile()
    {
        try{
            $token = $this->checkToken();
            if ($token['code'] < 0) return $this->response($token);

            // 校验验证码
            $captcha = new Captcha();
            $check_res = $captcha->checkCaptcha(false);
            if ($check_res['code'] < 0) return $this->response($check_res);

            $register = new RegisterModel();
            $exist = $register->mobileExist($this->params['mobile']);
            if ($exist) {
                return $this->response($this->error("", "手机号已存在"));
            } else {
                $key = $this->params['key'];
                $verify_data = Cache::get($key);
                if ($verify_data["mobile"] == $this->params["mobile"] && $verify_data["code"] == $this->params["code"]) {
                    $mobile = isset($this->params['mobile']) ? $this->params['mobile'] : '';
                    $member_model = new MemberModel();
                    $res = $member_model->editMember(['mobile' => $mobile], [['member_id', '=', $token['data']['member_id']]]);
                } else {
                    $res = $this->error("", "验证码不正确");
                }
                return $this->response($res);
            }
        }catch(\Exception $e){
            dump($e->getMessage() . $e->getLine());
        }

    }

    /**
     * 修改邮箱
     * @return string
     */
    public function modifyemail()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        // 校验验证码
        $captcha = new Captcha();
        $check_res = $captcha->checkCaptcha(false);
        if ($check_res['code'] < 0) return $this->response($check_res);
        $register = new RegisterModel();
        $exist = $register->emailExist($this->params['email']);
        if ($exist) {
            return $this->response($this->error("", "邮箱已存在"));
        } else {
            $key = $this->params['key'];
            $verify_data = Cache::get($key);
            if ($verify_data["email"] == $this->params["email"] && $verify_data["code"] == $this->params["code"]) {
                $email = isset($this->params['email']) ? $this->params['email'] : '';
                $member_model = new MemberModel();
                $res = $member_model->editMember(['email' => $email], [['member_id', '=', $token['data']['member_id']]]);
            } else {
                $res = $this->error("", "验证码不正确");
            }
            return $this->response($res);
        }
    }

    /**
     * 修改密码
     * @return string
     */
    public function modifypassword()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $old_password = isset($this->params['old_password']) ? $this->params['old_password'] : '';
        $new_password = isset($this->params['new_password']) ? $this->params['new_password'] : '';

        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo([['member_id', '=', $token['data']['member_id']]], 'password');
        // 未设置密码时设置密码需验证身份
        if (empty($info['data']['password'])) {
            $key = $this->params['key'] ?? '';
            $code = $this->params['code'] ?? '';
            $verify_data = Cache::get($key);
            if (empty($verify_data) || $verify_data["code"] != $code) {
                return $this->response($this->error("", "手机验证码不正确"));
            }
        }
        $res = $member_model->modifyMemberPassword($token['data']['member_id'], $old_password, $new_password);

        return $this->response($res);
    }


    /**
     * 绑定短信验证码
     */
    public function bindmobliecode()
    {
        // 校验验证码
        $captcha = new Captcha();
        $check_res = $captcha->checkCaptcha(false);
        if ($check_res['code'] < 0) return $this->response($check_res);

        $mobile = $this->params['mobile']; //注册手机号
        $register = new RegisterModel();
        $exist = $register->mobileExist($mobile);
        if ($exist) {
            return $this->response($this->error("", "当前手机号已存在"));
        } else {
            //检测环境类型 开发和测试环境默认6666
            if(in_array(config('app.env_type'), ['dev', 'test'])){
                $code = '6666';
                $res = [
                    'code' => 0,
                    'data' => '',
                    'message' => '发送成功',
                ];
            }else{
                $code = str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT); // 生成4位随机数，左侧补0
                $message_model = new Message();
                $res = $message_model->sendMessage(['member_id' => $this->member_id, "mobile" => $mobile, "code" => $code, "support_type" => ["sms"], "keywords" => "MEMBER_BIND"]);
            }

            if ($res["code"] >= 0) {
                //将验证码存入缓存
                $key = 'bind_mobile_code_' . md5(uniqid(null, true));
                Cache::tag("bind_mobile_code")->set($key, ['mobile' => $mobile, 'code' => $code], 600);
                return $this->response($this->success(["key" => $key]));
            } else {
                return $this->response($res);
            }
        }
    }

    /**
     * 邮箱绑定验证码
     */
    public function bingemailcode()
    {
        // 校验验证码
        $captcha = new Captcha();
        $check_res = $captcha->checkCaptcha(false);
        if ($check_res['code'] < 0) return $this->response($check_res);

        $email = $this->params['email']; //注册邮箱号
        $register = new RegisterModel();
        $exist = $register->emailExist($email);
        if ($exist) {
            return $this->response($this->error("", "当前邮箱已存在"));
        } else {
            $code = str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT); // 生成4位随机数，左侧补0
            $message_model = new Message();
            $res = $message_model->sendMessage(["email" => $email, "code" => $code, "support_type" => ["email"], "keywords" => "MEMBER_BIND"]);
            if ($res["code"] >= 0) {
                //将验证码存入缓存
                $key = 'bind_email_code_' . md5(uniqid(null, true));
                Cache::tag("bind_email_code")->set($key, ['email' => $email, 'code' => $code], 600);
                return $this->response($this->success(["key" => $key]));
            } else {
                return $this->response($res);
            }
        }
    }

    /**
     * 设置密码时获取验证码
     */
    public function pwdmobliecode()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        // 校验验证码
        $captcha = new Captcha();
        $check_res = $captcha->checkCaptcha(false);
        if ($check_res['code'] < 0) return $this->response($check_res);

        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo([['member_id', '=', $token['data']['member_id']]], 'mobile');
        if (empty($info['data'])) return $this->response($this->error([], '未获取到会员信息！'));
        if (empty($info['data']['mobile'])) return $this->response($this->error([], '会员信息尚未绑定手机号！'));

        $mobile = $info['data']['mobile'];

        $code = str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT); // 生成4位随机数，左侧补0
        $message_model = new Message();
        $res = $message_model->sendMessage(["mobile" => $mobile, "code" => $code, "support_type" => ["sms"], "keywords" => "SET_PASSWORD"]);
        if (isset($res["code"]) && $res["code"] >= 0) {
            //将验证码存入缓存
            $key = 'password_mobile_code_' . md5(uniqid(null, true));
            Cache::tag("password_mobile_code_")->set($key, ['mobile' => $mobile, 'code' => $code], 600);
            return $this->response($this->success(["key" => $key, 'code' => $code]));
        } else {
            return $this->response($this->error('', '发送失败'));
        }
    }

    /**
     * 验证邮箱
     * @return string
     */
    public function checkemail()
    {
        $email = isset($this->params['email']) ? $this->params['email'] : '';
        if (empty($email)) {
            return $this->response($this->error('', 'REQUEST_EMAIL'));
        }
        $member_model = new MemberModel();
        $condition = [
            ['email', '=', $email]
        ];
        $res = $member_model->getMemberCount($condition);
        if ($res['data'] > 0) {
            return $this->response($this->error('', '当前邮箱已存在'));
        }
        return $this->response($this->success());
    }

    /**
     * 验证手机号
     * @return string
     */
    public function checkmobile()
    {
        $mobile = isset($this->params['mobile']) ? $this->params['mobile'] : '';
        if (empty($mobile)) {
            return $this->response($this->error('', 'REQUEST_MOBILE'));
        }
        $member_model = new MemberModel();
        $condition = [
            ['mobile', '=', $mobile]
        ];
        $res = $member_model->getMemberCount($condition);
        if ($res['data'] > 0) {
            return $this->response($this->error('', '当前手机号已存在'));
        }
        return $this->response($this->success());
    }

    /**
     * 修改支付密码
     * @return string
     */
    public function modifypaypassword()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $key = $this->params['key'] ?? '';
        $code = $this->params['code'] ?? '';
        $password = isset($this->params['password']) ? trim($this->params['password']) : '';
        if (empty($password)) return $this->response($this->error('', '支付密码不可为空'));

        $verify_data = Cache::get($key);
        if ($verify_data["code"] == $this->params["code"]) {
            $member_model = new MemberModel();
            $res = $member_model->modifyMemberPayPassword($token['data']['member_id'], $password);
        } else {
            $res = $this->error("", "验证码不正确");
        }
        return $this->response($res);
    }

    /**
     * 检测会员是否设置支付密码
     */
    public function issetpayaassword()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $member_model = new MemberModel();
        $res = $member_model->memberIsSetPayPassword($this->member_id);
        return $this->response($res);
    }

    /**
     * 检测支付密码是否正确
     */
    public function checkpaypassword()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $password = isset($this->params['pay_password']) ? trim($this->params['pay_password']) : '';
        if (empty($password)) return $this->response($this->error('', '支付密码不可为空'));

        $member_model = new MemberModel();
        $res = $member_model->checkPayPassword($this->member_id, $password);
        return $this->response($res);
    }


    /**
     * 修改支付密码发送手机验证码
     */
    public function paypwdcode()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        //检测环境类型 开发和测试环境默认6666
        if(in_array(config('app.env_type'), ['dev', 'test'])){
            $code = '6666';
            $res = [
                'code' => 0,
                'data' => '',
                'message' => '发送成功',
            ];
        }else{
            $code = str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT); // 生成4位随机数，左侧补0
            $message_model = new Message();
            $res = $message_model->sendMessage(["member_id" => $this->member_id, "code" => $code, "support_type" => ["sms"], "keywords" => "MEMBER_PAY_PASSWORD"]);
        }

        if ($res["code"] >= 0) {
            //将验证码存入缓存
            $key = 'pay_password_code_' . md5(uniqid(null, true));
            Cache::tag("pay_password_code")->set($key, ['member_id' => $this->member_id, 'code' => $code], 600);
            return $this->response($this->success(["key" => $key]));
        } else {
            return $this->response($res);
        }
    }

    /**
     * 验证修改支付密码动态码
     */
    public function verifypaypwdcode()
    {
        $key = isset($this->params['key']) ? trim($this->params['key']) : '';

        $verify_data = Cache::get($key);
        if ($verify_data["code"] == $this->params["code"]) {
            $res = $this->success([]);
        } else {
            $res = $this->error("", "验证码不正确");
        }
        return $this->response($res);
    }

    /**
     * 通过token得到会员id
     */
    public function id()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        return $this->response($this->success($this->member_id));
    }

    /**
     * 账户奖励规则说明
     * @return false|string
     */
    public function accountrule()
    {
        //积分
        $point = event('MemberAccountRule', ['account' => 'point']);

        //余额
        $balance = event('MemberAccountRule', ['account' => 'balance']);

        //成长值
        $growth = event('MemberAccountRule', ['account' => 'growth']);

        $res = [
            'point' => $point,
            'balance' => $balance,
            'growth' => $growth
        ];

        return $this->response($this->success($res));
    }

    /**
     * 拉取会员头像
     */
    public function pullhaedimg()
    {
        $member_id = input('member_id', '');
        $member = new MemberModel();
        $member->pullHeadimg($member_id);
    }

    /**
     * 统计会员优惠券
     */
    public function couponnum(){
        //优惠券总和为  店铺优惠券和平台优惠券的总和
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $state = $this->params['state'] ?? 1;
        $coupon_model = new MemberCoupon();
        $coupon_result = $coupon_model->getMemberCouponNum($token['data']['member_id'], $state);
        $coupon_num = $coupon_result['data'];

        $platformcoupon_model = new MemberPlatformcoupon();
        $plarform_result = $platformcoupon_model->getMemberPlatformcouponNum($token['data']['member_id'], $state);
        $platform_num = $plarform_result['data'];

        $newusercoupon_model = new NewUserCoupon();
        $newusercoupon_result = $newusercoupon_model->getNewUserCouponNum($token['data']['member_id'], $state);
        $newusercoupon_num = $newusercoupon_result['data'];
        return $this->response($coupon_model->success($coupon_num+$platform_num+$newusercoupon_num));
    }


    /**
     * 企业会员提交审核资料
     */
    public function enterpriseCert(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $company_name = isset($this->params['company_name']) ? trim($this->params['company_name']) : '';
        $province_id = isset($this->params['province_id']) ? trim($this->params['province_id']) : 0;
        $city_id = isset($this->params['city_id']) ? trim($this->params['city_id']) : 0;
        $district_id = isset($this->params['district_id']) ? trim($this->params['district_id']) : 0;
        $province_name = isset($this->params['province_name']) ? trim($this->params['province_name']) : '';
        $city_name = isset($this->params['city_name']) ? trim($this->params['city_name']) : '';
        $district_name = isset($this->params['district_name']) ? trim($this->params['district_name']) : '';
        $address = isset($this->params['address']) ? trim($this->params['address']) : '';
        $contacts_name = isset($this->params['contacts_name']) ? trim($this->params['contacts_name']) : '';
        $contacts_mobile = isset($this->params['contacts_mobile']) ? trim($this->params['contacts_mobile']) : '';
        $legal_person_name = isset($this->params['legal_person_name']) ? trim($this->params['legal_person_name']) : '';
        $legal_person_mobile = isset($this->params['legal_person_mobile']) ? trim($this->params['legal_person_mobile']) : '';
        $card_no = isset($this->params['card_no']) ? trim($this->params['card_no']) : '';
        $card_electronic_front = isset($this->params['card_electronic_front']) ? trim($this->params['card_electronic_front']) : '';
        $card_electronic_back = isset($this->params['card_electronic_back']) ? trim($this->params['card_electronic_back']) : '';
        $business_licence_number = isset($this->params['business_licence_number']) ? trim($this->params['business_licence_number']) : '';
        $business_licence_number_electronic = isset($this->params['business_licence_number_electronic']) ? trim($this->params['business_licence_number_electronic']) : '';
        $company_scale = isset($this->params['company_scale']) ? trim($this->params['company_scale']) : '';
        $check_status = 0;

        $data = [
            'check_status'=>$check_status,
        ];

        $apply_data = [
            'member_id'                 =>$this->member_id,
            'company_name'              =>$company_name,
            'province_id'               =>$province_id,
            'city_id'                   =>$city_id,
            'district_id'               =>$district_id,
            'province_name'             =>$province_name,
            'city_name'                 =>$city_name,
            'district_name'             =>$district_name,
            'address'                   =>$address,
            'contacts_name'             =>$contacts_name,
            'contacts_mobile'           =>$contacts_mobile,
            'legal_person_name'         =>$legal_person_name,
            'legal_person_mobile'       =>$legal_person_mobile,
            'card_no'                   =>$card_no,
            'card_electronic_front'     =>$card_electronic_front,
            'card_electronic_back'      =>$card_electronic_back,
            'business_licence_number'   =>$business_licence_number,
            'business_licence_number_electronic' =>$business_licence_number_electronic,
            'company_scale'             =>$company_scale,
        ];
        $member_model = new MemberModel();
        $res = $member_model->editMember($data,[['member_id','=',$this->member_id]],$apply_data);
        return $this->response($res);
    }

    //商会会员添加下级会员
    public function addBusinessUser(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $username = isset($this->params['username']) ? trim($this->params['username']) : '';
        $password = isset($this->params['password']) ? trim($this->params['password']) : '';
        $nickname = isset($this->params['nickname']) ? trim($this->params['nickname']) : '';

        $realname = isset($this->params['realname']) ? trim($this->params['realname']) : '';
        $mobile = isset($this->params['mobile']) ? trim($this->params['mobile']) : '';
        $withdraw_type = isset($this->params['withdraw_type']) ? trim($this->params['withdraw_type']) : 'bank';
        $branch_bank_name = isset($this->params['branch_bank_name']) ? trim($this->params['branch_bank_name']) : '';
        $bank_account = isset($this->params['bank_account']) ? trim($this->params['bank_account']) : '';

        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo([['member_id','=',$this->member_id],['member_type','=',2]])['data'];
        if(empty($info)){
            return $this->error('您不是商户会员，不能添加会员');
        }
        $account_data = [
            'realname'         => $realname,
            'mobile'           => $mobile,
            'withdraw_type'    => $withdraw_type,
            'branch_bank_name' => $branch_bank_name,
            'bank_account'     => $bank_account,
            'is_default'       => 1,
            'create_time'      => time()
        ];

        $level_type = 2;
        $data = [
            'username' => $username,
            'password' => $password,
            'nickname' => $nickname,
            'member_type' => 2,
        ];
        $member_level = new MemberLevel();
        $member_level_info = $member_level->getMemberLevelInfo([ [ 'is_default', '=', 1 ],['level_type','=',$level_type] ], 'level_id,level_name');
        $member_level_info = $member_level_info['data'];
        $data_reg = [
            'username'          => $data['username'],
            'nickname'          => $data['nickname'], //默认昵称为用户名
            'password'          => data_md5($data['password']),
            'member_type'       => 2,
            'member_level'      => $member_level_info['level_id'],
            'member_level_name' => $member_level_info['level_name'],
            'invite_code'       => transferCodeOrId(rand(1000,9999)),
            'business_id'       => $this->member_id,
            'check_status'      => 1,
            'reg_time'          => time(),
            'login_time'        => time(),
            'last_login_time'   => time()
        ];
        $member_bank_account_model = new MemberBankAccountModel();
        $res = $member_model->addMember($data_reg);
        if($res['code']==0){
            $account_data['member_id'] = $res['data'];
            $member_bank_account_model->addMemberBankAccount($account_data);
            return $this->response($res);
        }else{
            return $this->response($res);
        }

    }


    //获取商户会员下级企业会员
    public function getChildEnterpriseUserList(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $page = isset($this->params['page']) ? trim($this->params['page']) : 1;
        $page_size = isset($this->params['page_size']) ? trim($this->params['page_size']) : PAGE_LIST_ROWS;
        $member_model = new MemberModel();
        $business = $member_model->getMemberList([['business_id','=',$this->member_id]],'member_id')['data'];
        $array = [];
        if(!empty($business)){
            foreach($business as $key=>$val){
                $array[] = $business[$key]['member_id'];
            }
            $business_id = implode(',',$array).',';
        }else{
            $business_id = '';
        }
        $condition = [];
        $condition[] = ['member_type','=',1];
        $condition[] = ['check_status','=',1];
        $condition[] = ['invite_id','in',$business_id.$this->member_id];
        $field = 'username,mobile,reg_time,member_id,member_level_name,headimg,invite_id';
        $list = $member_model->getMemberPageList($condition, $page, $page_size, $order='reg_time desc', $field);
        foreach ($list['data']['list'] as $key=>$val){
            $list['data']['list'][$key]['company_name'] = $member_model->getMemberCertInfo([['member_id','=',$list['data']['list'][$key]['member_id']]])['data']['company_name'];
            if($list['data']['list'][$key]['invite_id']!=$this->member_id){
                $list['data']['list'][$key]['invite_name'] = $member_model->getMemberInfo([['member_id','=',$list['data']['list'][$key]['invite_id']]])['data']['username'];
            }

        }
        return $this->response($list);
    }



    //获取商户会员子账号
    public function getChildAccount(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $page = isset($this->params['page']) ? trim($this->params['page']) : 1;
        $page_size = isset($this->params['page_size']) ? trim($this->params['page_size']) : PAGE_LIST_ROWS;
        $condition = [];
        $condition[] = ['member_type','=',2];
        $condition[] = ['check_status','=',1];
        $condition[] = ['business_id','=',$this->member_id];
        $field = 'username,mobile,reg_time,member_id,member_level_name,headimg,nickname,invite_code';
        $member_model = new MemberModel();
        $list = $member_model->getMemberPageList($condition, $page, $page_size, $order='reg_time desc', $field);
        return $this->response($list);
    }

    /**
     * 获取企业规模
     */
    public function getCompanyScaleSet(){
        $member_model = new MemberModel();
        $company_scale = $member_model->getCompanyScaleSet();
        return $this->response($this->success($company_scale));
    }

    /**
     * 商业用户基础信息
     */
    public function businessUserInfo()
    {
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;
        $member_model = new MemberModel();
        $info = $member_model->getMemberInfo([['member_id', '=', $member_id]], 'member_id,source_member,username,nickname,mobile,email,password,status,headimg,member_level,member_level_name,member_label,member_label_name,realname,sex,location,birthday');
        $member_bank_account_model = new MemberBankAccountModel();
        $member_bank_account = $member_bank_account_model->getMemberBankAccountInfo([['member_id', '=', $member_id]],'member_id,realname,mobile,branch_bank_name,bank_account')['data'];
        if($member_bank_account){
            $info['data'] = array_merge($info['data'],$member_bank_account);
        }
        return $this->response($info);
    }


    /**
     * 商业用户修改提交
     */
    public function editBusinessUser(){
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;
        $nickname = isset($this->params['nickname']) ? $this->params['nickname'] : '';
        $withdraw_type = isset($this->params['withdraw_type']) ? trim($this->params['withdraw_type']) : 'bank';
        $realname = isset($this->params['realname']) ? $this->params['realname'] : '';
        $mobile = isset($this->params['mobile']) ? $this->params['mobile'] : 0;
        $branch_bank_name = isset($this->params['branch_bank_name']) ? $this->params['branch_bank_name'] : '';
        $bank_account = isset($this->params['bank_account']) ? $this->params['bank_account'] : '';
        $member_bank_account_data = [
            'member_id'         => $member_id,
            'realname'          => $realname,
            'withdraw_type'     => $withdraw_type,
            'mobile'            => $mobile,
            'branch_bank_name'  => $branch_bank_name,
            'bank_account'      => $bank_account,
            'is_default'        => 1,
            'modify_time'       => time()
        ];
        $member_data = [
            'code' => '',
            'nickname' => $nickname
        ];
        $member_model = new MemberModel();
        $member_bank_account_model = new MemberBankAccountModel();
        $member_bank_account_model->editMemberBankAccount($member_bank_account_data);
        $res = $member_model->editMember($member_data, [['member_id', '=', $member_id]]);
        return $this->response($res);
    }


    /**
     * 获取代理信息
     */
    public function getAgentInfo(){
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);
        $member_id = isset($this->params['member_id']) ? $this->params['member_id'] : 0;
        $member_model = new MemberModel();
        $agent_info = $member_model->getMemberInfo([['member_id', '=', $member_id]],'member_id,nickname,headimg,username');
        $member_info = $member_model->getMemberInfo([['member_id', '=', $this->member_id]]);
        if($member_info['data']['pintuan_trader_level']==''){
            return $this->response($agent_info);
        }else{
            return $this->response($this->error('','您已经是拼团商'));
        }
    }

    /**
     * 是否有设置支付密码
     * @return false|string
     */
    public function issetPayPassword()
    {
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $member_model = new MemberModel();
        $member_info = $member_model->getMemberInfo([['member_id', '=', $this->member_id]], 'pay_password');
        $res = success(0, '操作成功', [
            'isset' => $member_info['data']['pay_password'] ? 1 : 0,
        ]);
        return $this->response($res);
    }

}
