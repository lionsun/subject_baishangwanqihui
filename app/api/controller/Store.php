<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\store\Store as StoreModel;

/**
 * 门店
 * @author Administrator
 *
 */
class Store extends BaseApi
{
	
	/**
	 * 列表信息
	 */
	public function page()
	{
		
		$page = isset($this->params['page']) ? $this->params['page'] : 1;
		$page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
		$site_id = isset($this->params['site_id']) ? $this->params['site_id'] : 0;
		if (empty($site_id)) {
			return $this->response($this->error('', 'REQUEST_SITE_ID'));
		}
		
		$store_model = new StoreModel();
		$condition = [
			[ 'site_id', "=", $site_id ],
			[ 'status', '=', 1 ],
			[ 'is_frozen', '=', 0 ]
		];
		
		$list = $store_model->getStorePageList($condition, $page, $page_size, 'create_time desc', 'store_id,store_name,telphone,store_image,site_id,site_name,address,full_address,longitude,latitude,open_date,username');
		
		return $this->response($list);
	}
	
	/**
	 * 基础信息
	 * @return false|string
	 */
	public function info()
	{
		$store_id = isset($this->params['store_id']) ? $this->params['store_id'] : 0;
		$site_id = isset($this->params['site_id']) ? $this->params['site_id'] : 0;
		if (empty($site_id)) {
			return $this->response($this->error('', 'REQUEST_SITE_ID'));
		}
		
		if (empty($store_id)) {
			return $this->response($this->error('', 'REQUEST_STORE_ID'));
		}
		$condition = [
			[ 'store_id', "=", $store_id ],
			[ 'site_id', "=", $site_id ],
			[ 'status', '=', 1 ]
		];
		$store_model = new StoreModel();
		$list = $store_model->getStoreInfo($condition, 'store_id,store_name,telphone,store_image,site_id,site_name,address,full_address,longitude,latitude,open_date,username');
		
		return $this->response($list);
	}
	
}