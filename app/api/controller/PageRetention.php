<?php

namespace app\api\controller;

use app\model\system\PageRetention as RetentionModel;

/**
 * 页面点击地图数据接口
 * @author wangjx
 */
class PageRetention extends BaseApi
{
    /**
     * 添加页面留存时间
     * @return mixed
     */
    public function addRetention(){

        $data['url'] = isset($this->params['params']['url']) ? $this->params['params']['url'] : '';
        $data['refer'] = isset($this->params['params']['refer']) ? $this->params['params']['refer'] : '';
        $data['title'] = isset($this->params['params']['title']) ? $this->params['params']['title'] : '';
        $data['time'] = isset($this->params['params']['time']) ? $this->params['params']['time'] : 0;
        $data['time_in'] = isset($this->params['params']['time_in']) ? $this->params['params']['time_in'] : 0;
        $data['time_out'] = isset($this->params['params']['time_out']) ? $this->params['params']['time_out'] : 0;
        $data['domain'] = isset($this->params['params']['domain']) ? $this->params['params']['domain'] : '';
        $data['client_width'] = isset($this->params['params']['client_width']) ? $this->params['params']['client_width'] : 0;
        $data['client_height'] = isset($this->params['params']['client_height']) ? $this->params['params']['client_height'] : 0;
        $data['client_language'] = isset($this->params['params']['client_language']) ? $this->params['params']['client_language'] : '';
        $data['page_id'] = isset($this->params['params']['goods_sku_id']) ? $this->params['params']['goods_sku_id'] : 0;
        $data['member_id'] = 0;
        $data['client_ip'] = request()->ip();
        $data['create_time'] = time();
        $token = $this->checkToken();
        if ($token['code'] >= 0) {
            $data['member_id'] = $token['data']['member_id'];
        }

        $retention = new RetentionModel();
        $info = $retention->addRetention($data);
        return $this->response($info);

    }
}