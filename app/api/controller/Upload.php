<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\upload\Upload as UploadModel;

/**
 * 上传管理
 * @author Administrator
 *
 */
class Upload extends BaseApi
{
	
	/**
	 * 头像上传
	 */
	public function headimg()
	{
		$upload_model = new UploadModel(0);
		$param = array(
			"thumb_type" => "",
			"name" => "file"
		);
		$result = $upload_model->setPath("headimg/" . date("Ymd") . '/')->image($param);
		return $this->response($result);
	}
	
	/**
	 * 评价上传
	 */
	public function evaluateimg()
	{
		$upload_model = new UploadModel(0);
		$param = array(
			"thumb_type" => "",
			"name" => "file"
		);
		$result = $upload_model->setPath("evaluate_img/" . date("Ymd") . '/')->image($param);
		return $this->response($result);
	}

	/**
	 * 聊天图片上传
	 */
	public function chatimg()
	{
		$upload_model = new UploadModel(0);
		$param = array(
			"thumb_type" => "",
			"name" => "file"
		);
		$result = $upload_model->setPath("chat_img/" . date("Ymd") . '/')->image($param);
		return $this->response($result);
	}

    /**
     * 普通图片上传
     * @return false|string
     */
	public function commonImg()
    {
        $path = $this->params['path'] ?? '';
        if(empty($path)) return $this->response($this->error('', 'path参数不能为空'));

        $upload_model = new UploadModel(0);
        $param = array(
            "thumb_type" => "",
            "name" => "file"
        );
        $result = $upload_model->setPath($path . date("Ymd") . '/')->image($param);
        return $this->response($result);
    }
	
}