<?php

namespace app\api\controller;

use addon\topic\model\TopicBrowse as TopicBrowseModel;

/**
 * 专题活动浏览历史
 * @package app\api\controller
 */
class Topicbrowse extends BaseApi
{
	/**
	 * 添加专题活动浏览
	 */
	public function add()
	{
	    $data['topic_id'] = isset($this->params['topic_id']) ? $this->params['topic_id'] : 0;
	    $data['topic_goods_id'] = isset($this->params['topic_goods_id']) && $this->params['topic_goods_id'] > 0 ? $this->params['topic_goods_id'] : 0;
        $data['type'] = 1;
        $data['member_id'] = 0;
        $data['consumer_ip'] = request()->ip();
        $data['create_time'] = time();
        $token = $this->checkToken();
        if ($token['code'] >= 0) {
            $data['member_id'] = $token['data']['member_id'];
        }

        $topic_browse_model = new TopicBrowseModel();
        $res = $topic_browse_model->addTopicBrowse($data);

        return $this->response($res);
	}
}