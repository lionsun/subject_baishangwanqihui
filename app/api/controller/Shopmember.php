<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\shop\ShopMember as ShopMemberModel;

class Shopmember extends BaseApi
{
	/**
	 * 添加店铺关注
	 */
	public function add()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$site_id = isset($this->params['site_id']) ? $this->params['site_id'] : 0;//站点id
		
		if ($site_id === '') {
			return $this->error('', 'REQUEST_SITE_ID');
		}
		
		$shop_member_model = new ShopMemberModel();
		$res = $shop_member_model->addShopMember($site_id, $token['data']['member_id']);
		return $this->response($res);
		
	}
	
	/**
	 * 取消店铺关注
	 */
	public function delete()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$site_id = isset($this->params['site_id']) ? $this->params['site_id'] : 0;//站点id
		
		if ($site_id === '') {
			return $this->error('', 'REQUEST_SITE_ID');
		}
		
		$shop_member_model = new ShopMemberModel();
		$res = $shop_member_model->deleteShopMember($site_id, $token['data']['member_id']);
		return $this->response($res);
	}
	
	/**
	 * 检测是否关注
	 */
	public function issubscribe()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$site_id = isset($this->params['site_id']) ? $this->params['site_id'] : 0;//站点id
		
		if ($site_id === '') {
			return $this->error('', 'REQUEST_SITE_ID');
		}
		
		$shop_member_model = new ShopMemberModel();
		$res = $shop_member_model->isSubscribe($site_id, $token['data']['member_id']);
		return $this->response($res);
	}

    /**
     * 获取会员店铺分页列表
     * @return false|string
     */
	public function membershoppages()
	{
		$token = $this->checkToken();
		if ($token['code'] < 0) return $this->response($token);
		
		$page = isset($this->params['page']) ? $this->params['page'] : 1;
		$page_size = isset($this->params['page_size']) ? $this->params['page_size'] : PAGE_LIST_ROWS;
		
		$shop_member_model = new ShopMemberModel();
		$condition = [
			[ 'nsm.member_id', '=', $token['data']['member_id'] ],
			[ 'nsm.is_subscribe', '=', 1 ]
		];
		$res = $shop_member_model->getMemberShopPageList($condition, $page, $page_size, 'nsm.subscribe_time');
		return $this->response($res);
		
	}
	
}