<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\api\controller;

use app\model\system\Addon as AddonModel;

/**
 * 插件管理
 * @author Administrator
 *
 */
class Addon extends BaseApi
{

	/**
	 * 列表信息
	 */
	public function lists()
	{
		$addon = new AddonModel();
		$list = $addon->getAddonList();
		return $this->response($list);
	}

	public function addonisexit()
	{
		$res = [];
		$addon_path = './addon';
		if(is_dir($addon_path)){
            $data = scandir($addon_path);
            foreach($data as $val){
                if($val != '.' && $val != '..'){
                    $res[$val] = addon_is_exit($val);
                }
            }
        }

        return $this->response($this->success($res));
	}

	/**
	 * 插件是否存在
	 */
	public function isexit()
    {
		$name = $this->params['name'] ?? '';
		$res = 0;
		if (!empty($name)) $res = addon_is_exit($name);
		return $this->response($this->success($res));
	}

}