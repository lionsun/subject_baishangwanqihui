<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\shopapi\controller;

use app\model\system\User as UserModel;

class Login extends BaseApi
{
    protected $app_module = "shop";

	/**
	 * 登录方法
	 */
	public function login()
	{
		// 校验验证码
		$captcha = new Captcha();
		$check_res = $captcha->checkCaptcha();
		if ($check_res['code'] < 0) return $this->response($check_res);

		// 登录
		$login = new UserModel();
        if (empty($this->params["username"])) return $this->response($this->error([], "商家账号不能为空!"));
		if (empty($this->params["password"])) return $this->response($this->error([], "密码不可为空!"));

		$res = $login->appLogin($this->params['username'],$this->params["password"],$this->app_module);

		//生成access_token
		if ($res['code'] >= 0) {
			$token = $this->createToken($res['data']);
			return $this->response($this->success([ 'token' => $token ,'site_id' => $res['data']['site_id']]));
		}
		return $this->response($res);
	}



}