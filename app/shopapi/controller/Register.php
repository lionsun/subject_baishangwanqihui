<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\shopapi\controller;

use app\model\system\User as UserModel;

class Register extends BaseApi
{

    protected $app_module = "shop";

	/**
	 * 用户名密码注册
	 */
	public function register()
	{
		$register = new UserModel();

        if (empty($this->params["username"])) return $this->response($this->error([], "用户名不可为空!"));
        if (empty($this->params["password"])) return $this->response($this->error([], "密码不可为空!"));
        // 校验验证码
        $captcha = new Captcha();
        $check_res = $captcha->checkCaptcha();
        if ($check_res['code'] < 0) return $this->response($check_res);

        $data['username'] = $this->params['username'];
        $data['password'] = $this->params['password'];
        $data['app_module'] = $this->app_module;
        $data['site_id'] = 0;

        $res = $register->addUser($data);

        //生成access_token
        if ($res['code'] >= 0) {
            $token = $this->createToken($res['data']);
            return $this->response($this->success([ 'token' => $token ,'site_id' => $res['data']['site_id']]));
        }
        return $this->response($res);

		
	}

	
}