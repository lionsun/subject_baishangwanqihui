<?php
/**
 * Index.php
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2015-2025 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 * @author : niuteam
 * @date : 2015.1.17
 * @version : v1.0.0.0
 */

namespace app\shopapi\controller;

use app\model\upload\Upload as UploadModel;

/**
 * 上传管理
 * @author Administrator
 *
 */
class Upload extends BaseApi
{
	
	/**
	 * 头像上传
	 */
	public function image()
	{
		$upload_model = new UploadModel(0);
		$param = array(
			"thumb_type" => "",
			"name" => "file"
		);
        $path = $this->site_id > 0 ? "common/images/".date("Ymd"). '/' : "common/images/".date("Ymd"). '/';
		$result = $upload_model->setPath($path)->image($param);
		return $this->response($result);
	}

	/**
	 * 上传 存入相册
	 */
	public function album()
	{
        $token = $this->checkToken();
        if ($token['code'] < 0) return $this->response($token);

        $upload_model  = new UploadModel($token['data']['site_id']);
        $album_id = input("album_id", 0);

        $param = array(
            "thumb_type" => ["big","mid","small"],
            "name" => "file",
            "album_id" => $album_id
        );
        $result = $upload_model->setPath("common/images/".date("Ymd"). '/')->imageToAlbum($param);

		return $this->response($result);
	}
	
}