<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;


use app\model\system\Stat as StatModel;
/**
 * 统计管理 控制器
 */
class Stat extends BaseAdmin
{
	/**
	 * 统计概况
	 */
	public function index()
	{
        if (request()->isAjax()) {
            $date_type = input('date_type', 0);

            if($date_type == 0){
                $start_time = strtotime("today");
                $time_range = date('Y-m-d',$start_time);
            }else if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
            }

            $stat_model = new StatModel();

            $shop_stat_sum = $stat_model->getShopStatSum(0, $start_time);

            $shop_stat_sum['data']['time_range'] = $time_range;

            return $shop_stat_sum;
        }else{
            return $this->fetch("stat/index");
        }
	}

    /**
     * 店铺统计报表
     * */
    public function getStatList()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 1);

            if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 6;
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 29;
            }

            $stat_model = new StatModel();

            $stat_list = $stat_model->getShopStatList(0, $start_time);

            //将时间戳作为列表的主键
            $shop_stat_list = array_column($stat_list['data'], null, 'day_time');

            $data = array();

            for ($i = 0;$i <= $day;$i++){
                $time = strtotime(date('Y-m-d',strtotime("-".($day-$i)." day")));
                $data['time'][$i] = date('Y-m-d',$time);
                if(array_key_exists($time, $shop_stat_list)){
                    $data['order_total'][$i] = $shop_stat_list[$time]['order_total'];
                    $data['shipping_total'][$i] = $shop_stat_list[$time]['shipping_total'];
                    $data['refund_total'][$i] = $shop_stat_list[$time]['refund_total'];
                    $data['order_pay_count'][$i] = $shop_stat_list[$time]['order_pay_count'];
                    $data['goods_pay_count'][$i] = $shop_stat_list[$time]['goods_pay_count'];
                    $data['shop_money'][$i] = $shop_stat_list[$time]['shop_money'];
                    $data['platform_money'][$i] = $shop_stat_list[$time]['platform_money'];
                    $data['collect_shop'][$i] = $shop_stat_list[$time]['collect_shop'];
                    $data['collect_goods'][$i] = $shop_stat_list[$time]['collect_goods'];
                    $data['visit_count'][$i] = $shop_stat_list[$time]['visit_count'];
                    $data['order_count'][$i] = $shop_stat_list[$time]['order_count'];
                    $data['goods_count'][$i] = $shop_stat_list[$time]['goods_count'];
                    $data['add_goods_count'][$i] = $shop_stat_list[$time]['add_goods_count'];
                    $data['member_count'][$i] = $shop_stat_list[$time]['member_count'];
                }else{
                    $data['order_total'][$i] = 0.00;
                    $data['shipping_total'][$i] = 0.00;
                    $data['refund_total'][$i] = 0.00;
                    $data['order_pay_count'][$i] = 0;
                    $data['goods_pay_count'][$i] = 0;
                    $data['shop_money'][$i] = 0.00;
                    $data['platform_money'][$i] = 0.00;
                    $data['collect_shop'][$i] = 0;
                    $data['collect_goods'][$i] = 0;
                    $data['visit_count'][$i] = 0;
                    $data['order_count'][$i] = 0;
                    $data['goods_count'][$i] = 0;
                    $data['add_goods_count'][$i] = 0;
                    $data['member_count'][$i] = 0;
                }
            }

            $data['time_range'] = $time_range;

            return $data;
        }
    }

    /**
     * 交易分析
     */
    public function order()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 0);

            if($date_type == 0){
                $start_time = strtotime("today");
                $time_range = date('Y-m-d',$start_time);
            }else if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
            }

            $stat_model = new StatModel();

            $shop_stat_sum = $stat_model->getShopStatSum(0, $start_time);

            $shop_stat_sum['data']['time_range'] = $time_range;

            return $shop_stat_sum;
        }else{
            return $this->fetch("stat/order");
        }
    }

    /**
     * 交易统计报表
     * */
    public function getOrderStatList()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 1);

            if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 6;
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 29;
            }

            $stat_model = new StatModel();

            $stat_list = $stat_model->getShopStatList(0, $start_time);

            //将时间戳作为列表的主键
            $shop_stat_list = array_column($stat_list['data'], null, 'day_time');

            $data = array();

            for ($i = 0;$i <= $day;$i++){
                $time = strtotime(date('Y-m-d',strtotime("-".($day-$i)." day")));
                $data['time'][$i] = date('Y-m-d',$time);
                if(array_key_exists($time, $shop_stat_list)){
                    $data['order_total'][$i] = $shop_stat_list[$time]['order_total'];
                    $data['shipping_total'][$i] = $shop_stat_list[$time]['shipping_total'];
                    $data['refund_total'][$i] = $shop_stat_list[$time]['refund_total'];
                    $data['order_pay_count'][$i] = $shop_stat_list[$time]['order_pay_count'];
                    $data['goods_pay_count'][$i] = $shop_stat_list[$time]['goods_pay_count'];
                    $data['shop_money'][$i] = $shop_stat_list[$time]['shop_money'];
                    $data['platform_money'][$i] = $shop_stat_list[$time]['platform_money'];
                    $data['collect_shop'][$i] = $shop_stat_list[$time]['collect_shop'];
                    $data['collect_goods'][$i] = $shop_stat_list[$time]['collect_goods'];
                    $data['visit_count'][$i] = $shop_stat_list[$time]['visit_count'];
                    $data['order_count'][$i] = $shop_stat_list[$time]['order_count'];
                    $data['goods_count'][$i] = $shop_stat_list[$time]['goods_count'];
                    $data['add_goods_count'][$i] = $shop_stat_list[$time]['add_goods_count'];
                    $data['member_count'][$i] = $shop_stat_list[$time]['member_count'];
                }else{
                    $data['order_total'][$i] = 0.00;
                    $data['shipping_total'][$i] = 0.00;
                    $data['refund_total'][$i] = 0.00;
                    $data['order_pay_count'][$i] = 0;
                    $data['goods_pay_count'][$i] = 0;
                    $data['shop_money'][$i] = 0.00;
                    $data['platform_money'][$i] = 0.00;
                    $data['collect_shop'][$i] = 0;
                    $data['collect_goods'][$i] = 0;
                    $data['visit_count'][$i] = 0;
                    $data['order_count'][$i] = 0;
                    $data['goods_count'][$i] = 0;
                    $data['add_goods_count'][$i] = 0;
                    $data['member_count'][$i] = 0;
                }
            }

            $data['time_range'] = $time_range;

            return $data;
        }
    }

    /**
     * 商品统计
     * @return mixed
     */
    public function goods()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 0);

            if($date_type == 0){
                $start_time = strtotime("today");
                $time_range = date('Y-m-d',$start_time);
            }else if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
            }

            $stat_model = new StatModel();

            $shop_stat_sum = $stat_model->getShopStatSum(0, $start_time);

            $shop_stat_sum['data']['time_range'] = $time_range;

            return $shop_stat_sum;
        }else{
            return $this->fetch("stat/goods");
        }
    }

    /**
     * 商品统计报表
     * */
    public function getGoodsStatList()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 1);

            if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 6;
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 29;
            }

            $stat_model = new StatModel();

            $stat_list = $stat_model->getShopStatList(0, $start_time);

            //将时间戳作为列表的主键
            $shop_stat_list = array_column($stat_list['data'], null, 'day_time');

            $data = array();

            for ($i = 0;$i <= $day;$i++){
                $time = strtotime(date('Y-m-d',strtotime("-".($day-$i)." day")));
                $data['time'][$i] = date('Y-m-d',$time);
                if(array_key_exists($time, $shop_stat_list)){
                    $data['order_total'][$i] = $shop_stat_list[$time]['order_total'];
                    $data['shipping_total'][$i] = $shop_stat_list[$time]['shipping_total'];
                    $data['refund_total'][$i] = $shop_stat_list[$time]['refund_total'];
                    $data['order_pay_count'][$i] = $shop_stat_list[$time]['order_pay_count'];
                    $data['goods_pay_count'][$i] = $shop_stat_list[$time]['goods_pay_count'];
                    $data['shop_money'][$i] = $shop_stat_list[$time]['shop_money'];
                    $data['platform_money'][$i] = $shop_stat_list[$time]['platform_money'];
                    $data['collect_shop'][$i] = $shop_stat_list[$time]['collect_shop'];
                    $data['collect_goods'][$i] = $shop_stat_list[$time]['collect_goods'];
                    $data['visit_count'][$i] = $shop_stat_list[$time]['visit_count'];
                    $data['order_count'][$i] = $shop_stat_list[$time]['order_count'];
                    $data['goods_count'][$i] = $shop_stat_list[$time]['goods_count'];
                    $data['add_goods_count'][$i] = $shop_stat_list[$time]['add_goods_count'];
                    $data['member_count'][$i] = $shop_stat_list[$time]['member_count'];
                }else{
                    $data['order_total'][$i] = 0.00;
                    $data['shipping_total'][$i] = 0.00;
                    $data['refund_total'][$i] = 0.00;
                    $data['order_pay_count'][$i] = 0;
                    $data['goods_pay_count'][$i] = 0;
                    $data['shop_money'][$i] = 0.00;
                    $data['platform_money'][$i] = 0.00;
                    $data['collect_shop'][$i] = 0;
                    $data['collect_goods'][$i] = 0;
                    $data['visit_count'][$i] = 0;
                    $data['order_count'][$i] = 0;
                    $data['goods_count'][$i] = 0;
                    $data['add_goods_count'][$i] = 0;
                    $data['member_count'][$i] = 0;
                }
            }

            $data['time_range'] = $time_range;

            return $data;
        }
    }

    /**
     * 会员统计
     * @return mixed
     */
    public function member()
    {
        if (request()->isAjax()) {
            $stat_model = new StatModel();
            $member_stat_sum = $stat_model->getMemberStatSum();

            foreach($member_stat_sum['data'] as $k=>$v){
                $member_stat_sum['data']['total'] = $v['total'] ?: 0;
                $member_stat_sum['data']['dau'] = $v['dau'] ?: 0;
                $member_stat_sum['data']['wau'] = $v['wau'] ?: 0;
                $member_stat_sum['data']['mau'] = $v['mau'] ?: 0;
            }

            return $member_stat_sum;
        }else{
            return $this->fetch("stat/member");
        }
    }

    /**
     * 会员统计报表
     * */
    public function getMemberStatList()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 1);

            if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 6;
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 29;
            }

            $stat_model = new StatModel();
            $stat_list = $stat_model->getMemberStatList($start_time);
            $stat_list_time = array_column($stat_list['data'], 'time');

            for ($i = 0;$i <= $day; $i++){
                $key = 100;
                $time = date('Y-m-d',strtotime("-".($day-$i)." day"));
                $date = strtotime(date('Y-m-d',strtotime("-".($day-$i)." day")));
                $data['time'][$i] = $time;
                $data['date'][$i] = $date;
                $data['total'][$i] = 0;
                $data['H5'][$i] = 0;
                $data['App'][$i] = 0;
                $data['Pc'][$i] = 0;
                $data['Applets'][$i] = 0;

                $key = array_search($time, $stat_list_time);
                if($key < 100 && is_numeric($key)){
                    $data['total'][$i] = $stat_list['data'][$key]['total'];
                    $data['H5'][$i] = $stat_list['data'][$key]['H5'];
                    $data['App'][$i] = $stat_list['data'][$key]['App'];
                    $data['Pc'][$i] = $stat_list['data'][$key]['Pc'];
                    $data['Applets'][$i] = $stat_list['data'][$key]['Applets'];
                }
                $key = 100;
            }
            $data['time_range'] = $time_range;

            return $data;
        }
    }

    /**
     * 新增会员统计报表
     * */
    public function getNewMemberStatList()
    {
        if (request()->isAjax()) {
            $date_type = input('date_type', 1);

            if($date_type == 1){
                $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 6;
            }else if($date_type == 2){
                $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
                $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
                $day = 29;
            }

            $stat_model = new StatModel();
            $stat_list = $stat_model->getNewMemberStatList($start_time);
            $stat_list_time = array_column($stat_list['data'], 'time');

            for ($i = 0;$i <= $day; $i++){
                $key = 100;
                $time = date('Y-m-d',strtotime("-".($day-$i)." day"));
                $date = strtotime(date('Y-m-d',strtotime("-".($day-$i)." day")));
                $data['time'][$i] = $time;
                $data['date'][$i] = $date;
                $data['total'][$i] = 0;
                $data['oned_keep'][$i] = 0;
                $data['oned_keep_rate'][$i] = 0;
                $data['threed_keep'][$i] = 0;
                $data['threed_keep_rate'][$i] = 0;
                $data['week_keep'][$i] = 0;
                $data['week_keep_rate'][$i] = 0;
                $data['month_keep'][$i] = 0;
                $data['month_keep_rate'][$i] = 0;

                $key = array_search($time, $stat_list_time);
                if($key < 100 && is_numeric($key)){
                    $data['total'][$i] = $stat_list['data'][$key]['total'];
                    $data['oned_keep'][$i] = $stat_list['data'][$key]['oned_keep'];
                    $data['oned_keep_rate'][$i] = bcdiv($data['oned_keep'][$i], $data['total'][$i], 2) * 100;
                    $data['threed_keep'][$i] = $stat_list['data'][$key]['threed_keep'];
                    $data['threed_keep_rate'][$i] = bcdiv($data['threed_keep'][$i], $data['total'][$i], 2) * 100;
                    $data['week_keep'][$i] = $stat_list['data'][$key]['week_keep'];
                    $data['week_keep_rate'][$i] = bcdiv($data['week_keep'][$i], $data['total'][$i], 2) * 100;
                    $data['month_keep'][$i] = $stat_list['data'][$key]['month_keep'];
                    $data['month_keep_rate'][$i] = bcdiv($data['month_keep'][$i], $data['total'][$i], 2) * 100;
                }
                $key = 100;
            }
            $data['time_range'] = $time_range;

            return $data;
        }
    }

    /**
     * 店铺统计
     * @return mixed
     */
    public function shop(){
        return $this->fetch("stat/shop");
    }

    /**
     * 店铺统计导出
     */
    public function exportShopStat(){
        $date_type = input('date_type', 0);

        if($date_type == 0){
            $start_time = strtotime("today");
            $time_range = date('Y-m-d',$start_time);
        }else if($date_type == 1){
            $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
            $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
        }else if($date_type == 2){
            $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
            $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
        }

        $stat_model = new StatModel();

        //店铺统计信息
        $shop_stat_sum = $stat_model->getAllShopsStatSum($start_time);
        $site_ids = array_column($shop_stat_sum['data'], 'site_id');

        //店铺交易信息
        $condition = [["o.order_status", "=", 10],["o.finish_time", '>=', $start_time]];
        $shop_order_list = $stat_model->getAllShopsOrderList($condition, $site_ids);

        $header_arr = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
        );
        //处理数据
        $excelTableData = [];
        if (!empty($shop_stat_sum['data'])) {
            $excelTableData[] = $shop_stat_sum['data'];
            $excelTableData[] = $shop_order_list['data'];
        }

        //接收需要展示的字段
        $shop_field = ["site_id", "site_name", "site_link", "shop_uv_count", "shop_pv_count", "order_total", "order_pay_count", "time_range", "conversion"];
        $order_field = ["site_id", "site_name", "order_name", "sku_id", "goods_link", "goods_money", "pay_money", "goods_num", "finish_time"];

        $shop_field_name = [
            "site_id" => '店铺编号',
            "site_name" => '店铺名称',
            "site_link" => '店铺首页链接',
            "shop_uv_count" => '店铺UV',
            "shop_pv_count" => '店铺PV',
            "order_total" => '店铺销量',
            "order_pay_count" => '店铺销售数量',
            "time_range" => '日期',
            "conversion" => '转化率'
        ];
        $order_field_name = [
            "site_id" => '店铺编号',
            "site_name" => '店铺名称',
            "order_name" => '商品名称',
            "sku_id" => 'SKU编码',
            "goods_link" => '商品链接',
            "goods_money" => '商城价格',
            "pay_money" => '成交价格',
            "goods_num" => '成交数量',
            "finish_time" => '交易时间'
        ];
        $sheetName = ['店铺统计-店铺信息', '店铺统计-交易信息'];

        // 实例化excel
        $phpExcel = new \PHPExcel();
        if(empty($excelTableData)){
            $phpExcel->setactivesheetindex(0);
            $phpExcel->getActiveSheet()->setTitle($sheetName[0]);
            $phpExcel->getActiveSheet()->setCellValue('A1', '该时间段没有产生数据,请重新选择时间');
            $phpExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);//设置是否加粗
        }

        foreach($excelTableData as $k=>$v){
            //多个Sheet工作簿
            if($k !== 0) $phpExcel->createSheet();
            $phpExcel->setactivesheetindex($k);
            $phpExcel->getActiveSheet($k)->setTitle($sheetName[$k]);
            $phpExcel->getActiveSheet($k)->freezePane('A2');//冻结窗口

            //店铺统计息
            if($k == 0){
                $count = count($shop_field);

                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $shop_field_name[$shop_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗

                    if(!$v) continue;
                    foreach($v as $key=>$val){ //内容行
                        $value = $val[$shop_field[$i]];
                        if($val['shop_pv_count'] > 0 && !empty($shop_order_list['data'][$val['site_id']])){
                            $conversion = bcdiv(count($shop_order_list['data'][$val['site_id']]), $val['shop_pv_count'], 4) * 100;
                        }else{
                            $conversion = 0;
                        }

                        //内容长度  标题内容取最大值
                        $title_len = strlen($shop_field_name[$shop_field[$i]]);
                        $value_len = strlen($value);
                        $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;

                        //插入行位置
                        $keys = $key+2;
                        if($shop_field[$i] == 'site_link'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, url("web/shop-$val[site_id]"));
                        }elseif($shop_field[$i] == 'conversion'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $conversion . '%');
                        }elseif($shop_field[$i] == 'time_range'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $time_range);
                        }else{
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                        }
                        if($strlen < 20) $strlen = 20;
                        $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                    }
                }
            }

            //店铺交易信息
            if($k == 1){
                $count = count($order_field);
                $keys = 1;
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $order_field_name[$order_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗

                    if(!$v) continue;
                    foreach($v as $key=>$val){//内容行
                        if(!$val) continue;
                        $order_count = count($val);
                        for($n = 0; $n < $order_count; $n++){
                            $goods_id = $val[$n]['goods_link'];
                            $value = $val[$n][$order_field[$i]];

                            //内容长度 取最大值
                            $title_len = strlen($order_field_name[$order_field[$i]]);
                            $value_len = strlen($value);
                            $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;
                            //插入行位置
                            $keys = $keys+1;
                            if($order_field[$i] == 'goods_link'){
                                $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, url("web/sku-$goods_id"));
                            }else{
                                $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                            }
                            if($strlen < 20) $strlen = 20;
                            $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度

                        }
                    }
                    $keys = 1;
                }
            }
        }


        // 设置第一个sheet为工作的sheet
        $phpExcel->setActiveSheetIndex(0);

        // 保存Excel 2007格式文件，保存路径为当前路径，名字为export.xlsx
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $file = '店铺统计.xlsx';
        $objWriter->save($file);

        header("Content-type:application/octet-stream");

        $filename = basename($file);
        header("Content-Disposition:attachment;filename = " . $filename);
        header("Accept-ranges:bytes");
        header("Accept-length:" . filesize($file));
        readfile($file);
        unlink($file);
        exit;
    }
}