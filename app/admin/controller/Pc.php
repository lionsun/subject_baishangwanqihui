<?php

/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsBrand as GoodsBrandModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\web\Pc as PcModel;
use app\model\shop\Config as ShopConfig;
use app\model\web\AdvPosition as AdvPositionModel;
use app\model\web\WebSite as WebsiteModel;

/**
 * Pc端 控制器
 */
class Pc extends BaseAdmin
{
	private $pc_model;

	public function __construct()
	{
		$this->pc_model = new PcModel();
		parent::__construct();
	}

	/**
	 * 网站部署
	 * @return mixed
	 */
	public function deploy()
	{
		$refresh_time = 0;
		$path = 'web/refresh.log';
		if (file_exists($path)) {
			$refresh_time = file_get_contents($path);
		}
		$this->assign("root_url", ROOT_URL);
		$this->assign('refresh_time', $refresh_time);
		return $this->fetch('pc/deploy');
	}

	/**
	 * 默认部署：无需下载，一键刷新，API接口请求地址为当前域名，编译代码存放到web文件夹中
	 */
	public function downloadCsDefault()
	{
		return $this->pc_model->downloadCsDefault();
	}

	/**
	 * 独立部署：下载编译代码包，参考开发文档进行配置
	 */
	public function downloadCsIndep()
	{
		$domain = input("domain", ROOT_URL);
		$res = $this->pc_model->downloadCsIndep($domain);
		echo $res[ 'message' ];
	}

	/**
	 * 源码下载：下载开源代码包，参考开发文档进行配置，结合业务需求进行二次开发
	 */
	public function downloadOs()
	{
		$res = $this->pc_model->downloadOs();
		echo $res[ 'message' ];
	}

	/**
	 * 热门搜索关键词
	 * @return mixed
	 */
	public function hotSearchWords()
	{
		if (request()->isAjax()) {
		    $words = input("words", []);
			$data = [
				'words' => implode(',', $words)
			];
			$res = $this->pc_model->setHotSearchWords($data);
			return $res;
		} else {
			$hot_search_words = $this->pc_model->getHotSearchWords();
			$hot_search_words = $hot_search_words[ 'data' ][ 'value' ];

			$words_array = [];
			if(!empty($hot_search_words['words'])){
                $words_array = explode(',', $hot_search_words['words']);
            }
            $hot_search_words['words_array'] = $words_array;
			$this->assign("hot_search_words", $hot_search_words);
			return $this->fetch('pc/hot_search_words');
		}
	}

	/**
	 * 默认搜索关键词
	 * @return mixed
	 */
	public function defaultSearchWords()
	{
		if (request()->isAjax()) {
			$data = [
				'words' => input("words", "")
			];
			$res = $this->pc_model->setDefaultSearchWords($data);
			return $res;
		} else {
			$default_search_words = $this->pc_model->getDefaultSearchWords();
			$default_search_words = $default_search_words[ 'data' ][ 'value' ];
			$this->assign("default_search_words", $default_search_words);
			return $this->fetch('pc/default_search_words');
		}
	}

	/**
	 * 首页浮层
	 * @return mixed
	 */
	public function floatLayer()
	{
		if (request()->isAjax()) {
			$data = [
				'title' => input("title", ""),
				'url' => input("url", ""),
				'is_show' => input("is_show", 0),
				'number' => input("number", ""),
				'img_url' => input("img_url", "")
			];
			$res = $this->pc_model->setFloatLayer($data);
			return $res;
		} else {
			$link = $this->pc_model->getLink();
			$this->assign("link", $link);
			$float_layer = $this->pc_model->getFloatLayer();
			$float_layer = $float_layer[ 'data' ][ 'value' ];
			$this->assign("float_layer", $float_layer);
			return $this->fetch('pc/float_layer');
		}
	}

	/**
	 * 导航设置
	 * @return mixed
	 */
	public function navList()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$search_text = input('search_text', '');

			$condition = [];
			$condition[] = [ 'nav_title', 'like', '%' . $search_text . '%' ];
			$order = 'create_time desc';

			$model = new PcModel();
			return $model->getNavPageList($condition, $page, $page_size, $order);
		} else {
			return $this->fetch('pc/nav_list');
		}
	}

	/**
	 * 添加导航
	 * @return mixed
	 */
	public function addNav()
	{
		$model = new PcModel();
		if (request()->isAjax()) {
			$data = [
				'nav_title' => input('nav_title', ''),
				'nav_url' => input('nav_url', ''),
				'sort' => input('sort', ''),
				'is_blank' => input('is_blank', ''),
				'nav_icon' => input('nav_icon', ''),
				'is_show' => input('is_show', ''),
				'create_time' => time(),
			];

			return $model->addNav($data);
		} else {
			$link_list = $model->getLink();
			$this->assign('link', $link_list);

			return $this->fetch('pc/add_nav');
		}
	}

	/**
	 * 编辑导航
	 * @return mixed
	 */
	public function editNav()
	{
		$model = new PcModel();
		if (request()->isAjax()) {
			$data = [
				'nav_title' => input('nav_title', ''),
				'nav_url' => input('nav_url', ''),
				'sort' => input('sort', ''),
				'is_blank' => input('is_blank', ''),
				'nav_icon' => input('nav_icon', ''),
				'is_show' => input('is_show', ''),
				'modify_time' => time(),
			];
			$id = input('id', 0);
			$condition = [ [ 'id', '=', $id ] ];

			return $model->editNav($data, $condition);
		} else {
			$link_list = $model->getLink();
			$this->assign('link', $link_list);

			$id = input('id', 0);
			$this->assign('id', $id);

			$nav_info = $model->getNavInfo($id);
			$this->assign('nav_info', $nav_info[ 'data' ]);

			return $this->fetch('pc/edit_nav');
		}
	}

	/**
	 * 删除导航
	 * @return mixed
	 */
	public function deleteNav()
	{
		if (request()->isAjax()) {
			$id = input('id', 0);
			$model = new PcModel();
			return $model->deleteNav([ [ 'id', '=', $id ] ]);
		}
	}

	/**
	 * 修改排序
	 */
	public function modifySort()
	{
		if (request()->isAjax()) {
			$sort = input('sort', 0);
			$id = input('id', 0);
			$model = new PcModel();
			return $model->modifyNavSort($sort, $id);
		}
	}

	/**
	 * 友情链接
	 * @return mixed
	 */
	public function linklist()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$search_text = input('search_text', '');

			$condition = [];
			$condition[] = [ 'link_title', 'like', '%' . $search_text . '%' ];
			$order = 'link_sort desc';

			$model = new PcModel();
			return $model->getLinkPageList($condition, $page, $page_size, $order);
		} else {
			return $this->fetch('pc/link_list');
		}
	}

	/**
	 * 添加友情链接
	 * @return mixed
	 */
	public function addLink()
	{
		$model = new PcModel();
		if (request()->isAjax()) {
			$data = [
				'link_title' => input('link_title', ''),
				'link_url' => input('link_url', ''),
				'link_pic' => input('link_pic', ''),
				'link_sort' => input('link_sort', ''),
				'is_blank' => input('is_blank', ''),
				'is_show' => input('is_show', ''),
			];

			return $model->addLink($data);
		} else {
			return $this->fetch('pc/add_link');
		}
	}

	/**
	 * 编辑友情链接
	 * @return mixed
	 */
	public function editLink()
	{
		$model = new PcModel();
		if (request()->isAjax()) {
			$data = [
				'link_title' => input('link_title', ''),
				'link_url' => input('link_url', ''),
				'link_pic' => input('link_pic', ''),
				'link_sort' => input('link_sort', ''),
				'is_blank' => input('is_blank', ''),
				'is_show' => input('is_show', ''),
			];
			$id = input('id', 0);
			$condition = [ [ 'id', '=', $id ] ];

			return $model->editLink($data, $condition);
		} else {

			$id = input('id', 0);
			$this->assign('id', $id);

			$link_info = $model->getLinkInfo($id);
			$this->assign('link_info', $link_info[ 'data' ]);

			return $this->fetch('pc/edit_link');
		}
	}

	/**
	 * 删除友情链接
	 * @return mixed
	 */
	public function deleteLink()
	{
		if (request()->isAjax()) {
			$id = input('id', 0);
			$model = new PcModel();
			return $model->deleteLink([ [ 'id', '=', $id ] ]);
		}
	}

	/**
	 * 修改排序
	 */
	public function modifyLinkSort()
	{
		if (request()->isAjax()) {
			$sort = input('sort', 0);
			$id = input('id', 0);
			return $this->pc_model->modifyLinkSort($sort, $id);
		}
	}

	/**
	 * 首页楼层
	 * @return array|mixed
	 */
	public function floor()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$search_text = input('search_text', '');
			$view_name = input('view_name', '');

			$condition = [];
			$condition[] = [ 'pf.view_name', '=', $view_name ];
			$condition[] = [ 'pf.title', 'like', '%' . $search_text . '%' ];


			$list = $this->pc_model->getFloorPageList($condition, $page, $page_size);
			return $list;
		} else {
		    $view_name = input('view_name', '');
		    $this->assign('view_name', $view_name);

		    $diy_view_info = $this->pc_model->getPcSiteDiyViewInfo([['name', '=', $view_name]]);
		    $this->assign('diy_view_info', $diy_view_info['data']);
			return $this->fetch('pc/floor');
		}
	}

	/**
	 * 修改首页楼层排序
	 */
	public function modifyFloorSort()
	{
		if (request()->isAjax()) {
			$sort = input('sort', 0);
			$id = input('id', 0);
			$condition = array (
				[ 'id', '=', $id ],
			);
			$res = $this->pc_model->modifyFloorSort($sort, $condition);
			return $res;
		}
	}

	/**
	 * 删除首页楼层
	 * @return array
	 */
	public function deleteFloor()
	{
		if (request()->isAjax()) {
			$id = input('id', 0);
			$res = $this->pc_model->deleteFloor([ [ 'id', '=', $id ] ]);
			return $res;
		}
	}

	/**
	 * 编辑楼层
	 * @return mixed
	 */
	public function editFloor()
	{
		if (request()->isAjax()) {
			$id = input("id", 0);
			$data = [
				'block_id' => input("block_id", 0), //楼层模板关联id
				'title' => input("title", ''), // 楼层标题
				'value' => input("value", ''),
				'state' => input("state", 0),// 状态（0：禁用，1：启用）
				'sort' => input("sort", 0), //排序号
                'view_name' => input('view_name', ''),//自定义页面名称
			];
			if ($id == 0) {
				$res = $this->pc_model->addFloor($data);
			} else {
				$res = $this->pc_model->editFloor($data, [ [ 'id', '=', $id ] ]);
			}
			return $res;
		} else {
			$id = input("id", 0);
			$this->assign("id", $id);

			$view_name = input('view_name', '');
			if (!empty($id)) {
				$floor_info = $this->pc_model->getFloorDetail($id);
				$floor_info = $floor_info[ 'data' ];
				$this->assign("floor_info", $floor_info);

				$floor_value = json_decode($floor_info['value'], true);
				$this->assign('floor_value', $floor_value);
				
				$view_name = $floor_info['view_name'];
			}else{
				$this->assign('floor_value', null);
			}
            $this->assign('view_name', $view_name);


			$floor_block_list = $this->pc_model->getFloorBlockList();
			$floor_block_list = $floor_block_list[ 'data' ];
			$this->assign("floor_block_list", $floor_block_list);

			$pc_link = $this->pc_model->getLink();
			$this->assign("pc_link", $pc_link);

			$goods_category_model = new GoodsCategoryModel();
			$category_list = $goods_category_model->getCategoryTree();
			$category_list = $category_list[ 'data' ];
			$this->assign("category_list", $category_list);

            

            $diy_view_info = $this->pc_model->getPcSiteDiyViewInfo([['name', '=', $view_name]]);
            $this->assign('diy_view_info', $diy_view_info['data']);

			return $this->fetch('pc/edit_floor');
		}
	}

	/**
	 * 商品选择组件
	 * @return \multitype
	 */
	public function goodsSelect()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$goods_name = input('goods_name', '');
            $shop_name = input('shop_name', '');
			$goods_ids = input('goods_ids', '');
			$is_virtual = input('is_virtual', '');// 是否虚拟类商品（0实物1.虚拟）
			$min_price = input('min_price', 0);
			$max_price = input('max_price', 0);
			$goods_class = input('goods_class', "");// 商品类型，实物、虚拟
			$category_id = input('category_id', "");// 商品分类id
			$promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、fenxiao、bargain
			$promotion_type = input('promotion_type', "");
            $select_id = input('select_id', '');
            $search_type = input('search_type', 'all');
            $source = input('source', ''); //来源
            $brand_id = input('brand_id', ''); //品牌
            $is_need_card = input('is_need_card', ''); //海外购
		
            $alias = 'gs';
            $join = [
                ['shop s', 'gs.site_id = s.site_id', 'left']
            ];
            $condition = [
                ['gs.is_delete', '=', 0],
                ['gs.goods_state', '=', 1],
                ['gs.verify_state', '=', 1],
                ['s.shop_status', '=', 1],
            ];
            if(!empty($this->site_id)) $condition[] = ['gs.site_id', '=', $this->site_id];
            if (!empty($goods_name)) {
                $condition[] = ['gs.sku_name', 'like', '%' . $goods_name . '%'];
            }
            if (!empty($shop_name)) {
                $condition[] = ['gs.site_name', 'like', '%' . $shop_name . '%'];
            }
            if ($is_virtual !== "") {
                $condition[] = ['gs.is_virtual', '=', $is_virtual];
            }
            if (!empty($goods_id)) {
                $condition[] = ['gs.goods_id', '=', $goods_id];
            }
            if($search_type == 'checked'){
                $condition[] = ['gs.sku_id', 'in', $select_id];
            }
            
            if (!empty($category_id)) {
                $condition[] = ['gs.category_id_1|gs.category_id_2|gs.category_id_3', '=', $category_id];

            }

            if (!empty($promotion_type)) {
                $condition[] = ['gs.promotion_addon', 'like', "%{$promotion_type}%"];
            }


            if ($goods_class !== "") {
                $condition[] = ['gs.goods_class', '=', $goods_class];
            }

            if ($min_price != "" && $max_price != "") {
                $condition[] = ['gs.price', 'between', [$min_price, $max_price]];
            } elseif ($min_price != "") {
                $condition[] = ['gs.price', '<=', $min_price];
            } elseif ($max_price != "") {
                $condition[] = ['gs.price', '>=', $max_price];
            }
            
            if(!empty($source)){
                    $condition[] = ['gs.source', '=', $source];
            }
            
            if(!empty($brand_id)){
                $condition[] = ['gs.brand_id', '=', $brand_id];
            }
            
            if($is_need_card!==''){
                if($is_need_card==1){
                    $condition[] = ['gs.trade_type', 'in', [2,3]];
                }else{
                    $condition[] = ['gs.trade_type', 'in', [0,1,4,5]];
                }
            }

            $order = 'gs.create_time desc';
            $goods_model = new GoodsModel();
            $field = 'gs.goods_id,gs.sku_id,gs.sku_name,gs.goods_class_name,gs.sku_image,gs.price,gs.market_price,gs.create_time,gs.is_virtual,gs.source,gs.site_name';
            $goods_list = $goods_model->getGoodsSkuPageList($condition, $page, $page_size, $order, $field, $alias, $join);
			
			return $goods_list;
		} else {

			//已经选择的商品sku数据
			$select_id = input('select_id', '');
			$mode = input('mode', 'spu');
			$max_num = input('max_num', 0);
			$min_num = input('min_num', 0);
			$is_virtual = input('is_virtual', '');
			$disabled = input('disabled', 0);
			$promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao

			$this->assign('select_id', $select_id);
			$this->assign('mode', $mode);
			$this->assign('max_num', $max_num);
			$this->assign('min_num', $min_num);
			$this->assign('is_virtual', $is_virtual);
			$this->assign('disabled', $disabled);
			$this->assign('promotion', $promotion);

			// 营销活动
			$goods_promotion_type = event('GoodsPromotionType');
			$this->assign('promotion_type', $goods_promotion_type);

            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList([], 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);
            
            //获取品牌;
            $goods_brand_model = new GoodsBrandModel();
            $brand_list = $goods_brand_model->getBrandList([['site_id', 'in', ("0,$this->site_id")]], "brand_id, brand_name");
            $brand_list = $brand_list[ 'data' ];
            $this->assign("brand_list", $brand_list);
			return $this->fetch("pc/goods_select");
		}
	}

	/**
	 * 品牌选择
	 * @return \multitype
	 */
	public function brandSelect()
	{
		if (request()->isAjax()) {
			$page = input('page', 1);
			$page_size = input('page_size', PAGE_LIST_ROWS);
			$brand_name = input('brand_name', '');
			$brand_ids = input('brand_ids', '');

			$condition = [];
			if (!empty($brand_name)) {
				$condition[] = [ 'ngb.brand_name', 'like', '%' . $brand_name . '%' ];
			}
			if (!empty($brand_ids)) {
				$condition[] = [ 'ngb.brand_id', 'in', $brand_ids ];
			}

			$goods_brand_model = new GoodsBrandModel();
			$brand_list = $goods_brand_model->getBrandPageList($condition, $page, $page_size);
			return $brand_list;
		} else {

			$select_id = input('select_id', '');
			$max_num = input('max_num', 0);
			$min_num = input('min_num', 0);
			$disabled = input('disabled', 0);

			$this->assign('select_id', $select_id);
			$this->assign('max_num', $max_num);
			$this->assign('min_num', $min_num);
			$this->assign('disabled', $disabled);
			return $this->fetch("pc/brand_select");
		}
	}

    /**
     * 商城功能设置
     */
	public function shopFunctionConfig()
    {
        $config_model = new ShopConfig();
        if(request()->isAjax()){
            $value_json = input('value_json', '');
            $value = json_decode($value_json, true);
            return $config_model->setPcShopFunctionConfig($value);
        }else{
            $config_info = $config_model->getPcShopFunctionConfig();
            $this->assign('config_info', $config_info['data']['value']);
            return $this->fetch("pc/shop_function_config");
        }
    }

    /**
     * 自定义页面列表
     */
    public function diyViewLists()
    {
        if(request()->isAjax()) {
            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $name = input('name', '');

            $condition = [];
            $condition[] = ['site_id', '=', $this->site_id];
            if(!empty($name)){
                $condition[] = ['name', '=', "%{$name}%"];
            }
            $field = '*';
            $order = 'create_time desc';

            $res = $this->pc_model->getPcSiteDiyViewPageList($condition, $field, $order, $page, $page_size);
            $website_model = new WebsiteModel();
            $website_info = $website_model->getWebSite([['site_id', '=', 0]], '*');
            $adv_position = new AdvPositionModel();
            foreach($res['data']['list'] as $key=>$val){
                $position_info = $adv_position->getAdvPositionInfo([['keyword', '=', $val['name']]], 'ap_id')['data'];
                $res['data']['list'][$key]['ap_id'] = $position_info['ap_id'];
                $res['data']['list'][$key]['view_url'] = $website_info['data']['web_domain'] . '/diy?view_name=' . $val['name'];
            }
            return $res;
        }else{
            return $this->fetch("pc/diy_view_lists");
        }
    }

    /**
     * 编辑自定义页面
     */
    public function addOrEditDiyView()
    {
        if(request()->isAjax()) {
            $name = input('name', '');
            if($name){
                $data = [
                    'site_id' => $this->site_id,
                    'title' => input('title', ''),
                    'modify_time' => time(),
                ];
                return $this->pc_model->editPcSiteDiyView($data, $name);
            }else{
                $param = [
                    'site_id' => $this->site_id,
                    'title' => input('title', ''),
                ];
                return $this->pc_model->addPcSiteDiyView($param);
            }
        }
    }

    /**
     * 删除自定义页面
     */
    public function deleteDiyView()
    {
        if(request()->isAjax()) {
            $name = input('name', '');
            return $this->pc_model->deletePcSiteDiyView($name);
        }
    }
}
