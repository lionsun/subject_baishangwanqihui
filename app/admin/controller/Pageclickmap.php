<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;


use app\model\system\PageClickmap as PageClickmapModel;
/**
 * 热力图
 */
class Pageclickmap extends BaseAdmin
{
    /**
     * 热力图列表
     */
    public function lists(){
        if(request()->isAjax()){
            $start_time = input("start_time", '');
            $end_time = input("end_time", '');
            $page_index = input('page', 1);
            $page_size = input('limit', PAGE_LIST_ROWS);

            $clickmap_model = new PageClickmapModel();

            if (!empty($start_time) && empty($end_time)) {
                $condition[] = ["create_time", ">=", date_to_time($start_time)];
            } elseif (empty($start_time) && !empty($end_time)) {
                $condition[] = ["create_time", "<=", date_to_time($end_time)];
            } elseif (!empty($start_time) && !empty($end_time)) {
                $condition[] = [ 'create_time', 'between', [ date_to_time($start_time), date_to_time($end_time) ] ];
            }
            $condition[] = ['type', '=', '0'];

            $clickmap_list = $clickmap_model->getClickmapList($condition, $page_index, $page_size);
            $clickmap_list['start_time'] = date_to_time($start_time);
            $clickmap_list['end_time'] = date_to_time($end_time);

            return $clickmap_list;
        }else{
            return $this->fetch("clickmap/lists");
        }
    }
    /**
     * 添加/编辑热力图
     */
    public function editclickmap(){
        $clickmap_model = new PageClickmapModel();

        if(request()->isAjax()){
            $data['id'] = input('id', 0);
            $data['page_name'] = input('page_name', '');
            $data['location'] = input('location', '');
            $data['type'] = 0;
            $data['create_time'] = time();
            if($data['id'] > 0){
                $data['update_time'] = time();
                $info = $clickmap_model->editClickmap($data);
            }else{
                unset($data['id']);
                $info = $clickmap_model->addClickmap($data);
            }

            return $info;
        }else{
            $id = input('id', 0);
            if($id > 0){
                $info = $clickmap_model->getClickmapInfo($id);
                $this->assign('info', $info['data']);
                $this->assign('id', $id);
            }

            return $this->fetch("clickmap/editclickmap");
        }
    }
    /**
     * 查看热力图
     */
    public function clickmap_page(){
        return $this->fetch("clickmap/page2");
    }


}