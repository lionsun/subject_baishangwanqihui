<?php

/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\admin\controller;

use app\model\goods\Config as GoodsConfigModel;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\goods\GoodsEvaluate;
use app\model\goods\GoodsExport;
use app\model\goods\GoodsShopCategory;
use app\model\goods\GoodsBrand as GoodsBrandModel;
use think\facade\Cache;

/**
 * 商品管理 控制器
 */
class Goods extends BaseAdmin
{
    /******************************* 正常商品列表及相关操作 ***************************/

    /**
     * 商品列表
     */
    public function lists()
    {
        $goods_model = new GoodsModel();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', "");
            $search_text_type = input('search_text_type', "goods_name");
            $goods_state = input('goods_state', "");
            $verify_state = input('verify_state', "");
            $category_id = input('category_id', "");
            $brand_id = input('goods_brand', '');
            $goods_attr_class = input("goods_attr_class", "");
            $site_id = input("site_id", "");
            $goods_class = input('goods_class', "");
            $source = input('source', "");
            $is_mp = input('is_mp', "");
            $sort_type = input('sort_type', "");
            $sort = input('sort', "");

            $condition = [ [ 'is_delete', '=', 0 ] ];
            if (!empty($search_text)) {
                $condition[] = [ $search_text_type, 'like', '%' . $search_text . '%' ];
            }
            if ($goods_class !== "") {
                $condition[] = [ 'goods_class', '=', $goods_class ];
            }
            if ($is_mp !== "") {
                $condition[] = ['is_mp', '=', $is_mp];
            }
            if ($goods_state !== '') {
                $condition[] = [ 'goods_state', '=', $goods_state ];
            }
            if ($verify_state !== '') {
                $condition[] = [ 'verify_state', '=', $verify_state ];
            }
            if (!empty($category_id)) {
                $condition[] = [ 'category_id|category_id_1|category_id_2|category_id_3', '=', $category_id ];
            }
            if ($brand_id) {
                $condition[] = [ 'brand_id', '=', $brand_id ];
            }
            if ($goods_attr_class) {
                $condition[] = [ 'goods_attr_class', '=', $goods_attr_class ];
            }
            if (!empty($site_id)) {
                $condition[] = [ 'site_id', '=', $site_id ];
            }
            if ($source != 'all' && $source !== '') {
                $condition[] = [ 'source', '=', $source ];
            }

            //$order = 'create_time desc';
            $order = 'goods_id desc';
            if ($sort) {
                $order = "$sort_type $sort";
            }

            $res = $goods_model->getGoodsPageList($condition, $page_index, $page_size, $order, '*,CASE WHEN unix_timestamp(now()) > live_start_time AND unix_timestamp(now()) < live_end_time AND is_live_goods = 1 THEN \'正在进行\' WHEN unix_timestamp(now()) < live_start_time AND is_live_goods = 1 THEN \'即将开始\' WHEN unix_timestamp(now()) > live_end_time AND is_live_goods = 1 THEN \'已经结束\' ELSE \'未参与\' END AS live_status');
            //            foreach($res['data']['list'] as $key=>$val){
            //                if($val['source'] == 2 && $val['goods_state'] == 2){
            //                    $res['data']['list'][$key]['pre_info'] = model('vop_ad_goods')->getInfo([['goods_id', '=', $val['goods_id']]]);
            //                }
            //            }
            if($goods_state==2){
                if(!empty($res['data']['list'])){
                    foreach ($res['data']['list']   as   $k=>$v){
                        $goods_model = new GoodsModel();
                        $resvopAdGoods = $goods_model->getVopAdGoods([['goods_id','=',$v['goods_id']]])["data"];
                        if(!empty($resvopAdGoods)){
                            $res['data']['list'][$k]['sell_time_from']=$resvopAdGoods['sell_time_from'];
                            $res['data']['list'][$k]['sell_time_to']=$resvopAdGoods['sell_time_to'];
                        }
                    }
                }
            }
            $res['condition'] = $condition;
            return $res;
        } else {
            $verify_state = $goods_model->getVerifyState();
            $arr = [];
            foreach ($verify_state as $k => $v) {
                // 过滤已审核状态
                if ($k != 1) {
                    $total = $goods_model->getGoodsTotalCount([ [ 'verify_state', '=', $k ] ]);
                    $total = $total[ 'data' ];
                    $arr[] = [
                        'state' => $k,
                        'value' => $v,
                        'count' => $total
                    ];
                }
            }
            $verify_state = $arr;
            $this->assign("verify_state", $verify_state);

            //商品来源
            $source_type = GoodsModel::getSourceType();
            $this->assign('source_type', $source_type);

            return $this->fetch('goods/lists');
        }
    }

    /**
     * 分销市场
     */
    public function fenxiaolists()
    {
        $goods_model = new GoodsModel();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', "");
            $search_text_type = input('search_text_type', "goods_name");
            $goods_state = input('goods_state', "");
            $verify_state = input('verify_state', "");
            $category_id = input('category_id', "");
            $brand_id = input('goods_brand', '');
            $goods_attr_class = input("goods_attr_class", "");
            $site_id = input("site_id", "");
            $goods_class = input('goods_class', "");
            $source = input('source', "");
            $is_set_member_price = input('is_set_member_price', "");
            $is_mp = input('is_mp', "");
            $sort_type = input('sort_type', "");
            $sort = input('sort', "");
            $is_new_fenxiao = input('is_new_fenxiao', "");

            $condition = [ [ 'is_delete', '=', 0 ] ];
            if (!empty($search_text)) {
                $condition[] = [ $search_text_type, 'like', '%' . $search_text . '%' ];
            }
            if ($goods_class !== "") {
                $condition[] = [ 'goods_class', '=', $goods_class ];
            }
            if ($is_set_member_price !== "") {
                $condition[] = ['is_set_member_price', '=', $is_set_member_price];
            }
            if ($is_mp !== "") {
                $condition[] = ['is_mp', '=', $is_mp];
            }
            if ($goods_state !== '') {
                $condition[] = [ 'goods_state', '=', $goods_state ];
            }
            if ($verify_state !== '') {
                $condition[] = [ 'verify_state', '=', $verify_state ];
            }
            if (!empty($category_id)) {
                $condition[] = [ 'category_id|category_id_1|category_id_2|category_id_3', '=', $category_id ];
            }
            if ($brand_id) {
                $condition[] = [ 'brand_id', '=', $brand_id ];
            }
            if ($goods_attr_class) {
                $condition[] = [ 'goods_attr_class', '=', $goods_attr_class ];
            }
            if (!empty($site_id)) {
                $condition[] = [ 'site_id', '=', $site_id ];
            }
            if ($source != 'all' && $source !== '') {
                $condition[] = [ 'source', '=', $source ];
            }

            if(empty($is_new_fenxiao)){
                $condition[] = [ 'is_new_fenxiao', '=', $is_new_fenxiao ];
            }else{
                $condition[] = [ 'is_new_fenxiao', '=', $is_new_fenxiao ];
            }

            $order = 'create_time desc';
            if ($sort) {
                $order = "$sort_type $sort";
            }
            trace('商品搜索条件');
            trace($condition);
            $res = $goods_model->getGoodsPageList($condition, $page_index, $page_size, $order);
            return $res;
        } else {
            $verify_state = $goods_model->getVerifyState();
            $arr = [];
            foreach ($verify_state as $k => $v) {
                // 过滤已审核状态
                if ($k != 1) {
                    $total = $goods_model->getGoodsTotalCount([ [ 'verify_state', '=', $k ] ]);
                    $total = $total[ 'data' ];
                    $arr[] = [
                        'state' => $k,
                        'value' => $v,
                        'count' => $total
                    ];
                }
            }
            $verify_state = $arr;
            $this->assign("verify_state", $verify_state);

            //商品来源
            $source_type = GoodsModel::getSourceType();
            $this->assign('source_type', $source_type);

            return $this->fetch('goods/fenxiaolists');
        }
    }

    /**
     * 商品ＳＫＵ列表
     *
     * @return void
     */
    public function skulists()
    {
        $page = input('page', 1);
        $page_size = input('page_size', '');
        $site_id = input('site_id', 0);
        $category_id = input('category_id', 0);
        $category_level = input('category_level', 0);
        $brand_id = input('brand_id', 0);
        $min_price = input('min_price', "");
        $max_price = input('max_price', "");

        $condition = [];
        $field = 'gs.goods_id,gs.sku_id,gs.sku_name,gs.sku_image,gs.introduction,gs.price,gs.stock,gs.site_id,gs.market_price,gs.source';

        $alias = 'gs';
        $join = [
            [ 'goods g', 'gs.sku_id = g.sku_id', 'inner' ]
        ];

        if (!empty($site_id)) {
            $condition[] = [ 'gs.site_id', '=', $site_id ];
        }

        if (!empty($category_id) && !empty($category_level)) {
            $condition[] = [ 'gs.category_id_' . $category_level, '=', $category_id ];
        }

        if (!empty($brand_id)) {
            $condition[] = [ 'gs.brand_id', '=', $brand_id ];
        }

        if ($min_price != "" && $max_price != "") {
            $condition[] = [ 'gs.price', 'between', [ $min_price, $max_price ] ];
        } elseif ($min_price != "") {
            $condition[] = [ 'gs.price', '>=', $min_price ];
        } elseif ($max_price != "") {
            $condition[] = [ 'gs.price', '<=', $max_price ];
        }

        $condition[] = [ 'gs.goods_state', '=', 1 ];
        $condition[] = [ 'gs.verify_state', '=', 1 ];
        $condition[] = [ 'gs.is_delete', '=', 0 ];


        $goods = new GoodsModel();
        $list = $goods->getGoodsSkuPageList($condition, $page, PAGE_LIST_ROWS, '', $field, $alias, $join);
        return $list;
    }


    /**
     * 刷新审核状态商品数量
     */
    public function refreshVerifyStateCount()
    {
        if (request()->isAjax()) {
            $goods_model = new GoodsModel();
            $verify_state = $goods_model->getVerifyState();
            $arr = [];
            foreach ($verify_state as $k => $v) {
                // 过滤已审核状态
                if ($k != 1) {
                    $total = $goods_model->getGoodsTotalCount([ [ 'verify_state', '=', $k ], [ 'is_delete', '=', 0 ] ]);
                    $total = $total[ 'data' ];
                    $arr[] = [
                        'state' => $k,
                        'value' => $v,
                        'count' => $total
                    ];
                }
            }
            $verify_state = $arr;
            return $verify_state;
        }
    }

    /**
     * 获取SKU商品列表
     * @return \multitype
     */
    public function getGoodsSkuList()
    {
        if (request()->isAjax()) {
            $goods_id = input("goods_id", 0);
            $goods_model = new GoodsModel();
            $res = $goods_model->getGoodsSkuList([ [ 'goods_id', '=', $goods_id ] ], 'goods_id,sku_id,sku_name,price,cost_price,stock,sale_num,sku_image,spec_name,source,sku_no');
            return $res;
        }
    }

    /******************************* 违规下架商品列表及相关操作 ***************************/

    /**
     * 违规下架
     */
    public function lockup()
    {
        if (request()->isAjax()) {
            $verify_state_remark = input("verify_state_remark", 0);
            $goods_ids = input("goods_ids", 0);
            $goods_model = new GoodsModel();
            $res = $goods_model->lockup([ [ 'goods_id', 'in', $goods_ids ] ], $verify_state_remark);
            $this->addLog("商品违规下架id:" . $goods_ids . "原因:" . $verify_state_remark);
            return $res;
        }
    }

    /**
     * 获取商品违规或审核失败说明
     * @return \multitype
     */
    public function getVerifyStateRemark()
    {
        if (request()->isAjax()) {
            $goods_id = input("goods_id", 0);
            $goods_model = new GoodsModel();
            $res = $goods_model->getGoodsInfo([ [ 'goods_id', '=', $goods_id ], [ 'verify_state', 'in', [ -2, 10 ] ] ], 'verify_state_remark');
            return $res;
        }
    }

    /******************************* 待审核商品列表及相关操作 ***************************/

    /**
     * 商品审核
     */
    public function verifyOn()
    {
        if (request()->isAjax()) {
            $goods_ids = input("goods_ids", 0);
            $verify_state = input("verify_state", -2);
            $verify_state_remark = input("verify_state_remark", '');
            $goods_model = new GoodsModel();
            $res = $goods_model->modifyVerifyState($goods_ids, $verify_state, $verify_state_remark);
            return $res;
        }
    }

    /**
     * 审核设置
     */
    public function verifyConfig()
    {
        if (request()->isAjax()) {

            $is_open = input("is_open", 0);
            $data = [
                'is_open' => $is_open
            ];
            $goods_config = new GoodsConfigModel();
            $res = $goods_config->setVerifyConfig($data);
            return $res;
        } else {
            $goods_config = new GoodsConfigModel();
            $goods_verify_info = $goods_config->getVerifyConfig();
            $goods_verify_info = $goods_verify_info[ 'data' ];
            $this->assign("goods_verify_info", $goods_verify_info[ 'value' ]);
            return $this->fetch('goods/verify_config');
        }
    }

    /******************************* 商品评价列表及相关操作 ***************************/

    /**
     * 商品评价
     */
    public function evaluateList()
    {
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('limit', PAGE_LIST_ROWS);
            $site_id = input("site_id", "");
            $explain_type = input('explain_type', ''); //1好评2中评3差评
            $search_text = input('search_text', "");
            $search_type = input('search_type', "sku_name");
            $condition = [];
            //评分类型
            if ($explain_type != "") {
                $condition[] = [ "explain_type", "=", $explain_type ];
            }
            if (!empty($search_text)) {
                if (!empty($search_type)) {
                    $condition[] = [ $search_type, 'like', '%' . $search_text . '%' ];
                } else {
                    $condition[] = [ 'sku_name', 'like', '%' . $search_text . '%' ];
                }
            }
            if (!empty($site_id)) {
                $condition[] = [ 'site_id', '=', $site_id ];
            }

            $evaluate_model = new GoodsEvaluate();
            $res = $evaluate_model->getEvaluatePageList($condition, $page_index, $page_size);
            return $res;
        } else {
            return $this->fetch('goods/evaluate_list');
        }
    }

    /**
     * 评价删除
     */
    public function deleteEvaluate()
    {
        if (request()->isAjax()) {
            $id = input('id', '');
            $evaluate_model = new GoodsEvaluate();
            $res = $evaluate_model->deleteEvaluate($id);
            $this->addLog("删除商品评价id:" . $id);
            return $res;
        }
    }

    /**
     * 商品推广
     * return
     */
    public function goodsUrl()
    {
        $goods_id = input('goods_id', '');
        $goods_model = new GoodsModel();
        $goods_sku_info = $goods_model->getGoodsSkuInfo([ [ 'goods_id', '=', $goods_id ] ], 'sku_id,goods_name');
        $goods_sku_info = $goods_sku_info[ 'data' ];
        $res = $goods_model->qrcode($goods_sku_info[ 'sku_id' ], $goods_sku_info[ 'goods_name' ]);
        return $res;
    }

    /**
     * 商品预览
     * return
     */
    public function goodsPreview()
    {
        $goods_id = input('goods_id', '');
        $goods_model = new GoodsModel();
        $goods_sku_info = $goods_model->getGoodsSkuInfo([ [ 'goods_id', '=', $goods_id ] ], 'sku_id,goods_name');
        $goods_sku_info = $goods_sku_info[ 'data' ];
        $res = $goods_model->qrcode($goods_sku_info[ 'sku_id' ], $goods_sku_info[ 'goods_name' ]);
        return $res;
    }

    /**
     * 商品选择组件
     * @return array|mixed|void
     */
    public function goodsSelect()
    {
        if (request()->isAjax()) {

            $page = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $goods_name = input('goods_name', '');
            $shop_name = input('shop_name', '');
            $goods_id = input('goods_id', 0);
            $is_virtual = input('is_virtual', '');// 是否虚拟类商品（0实物1.虚拟）
            $min_price = input('min_price', 0);
            $max_price = input('max_price', 0);
            $goods_class = input('goods_class', "");// 商品类型，实物、虚拟
            $category_id = input('category_id', "");// 商品分类id
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、fenxiao、bargain
            $promotion_type = input('promotion_type', "");
            $select_id = input('select_id', '');
            $search_type = input('search_type', 'all');
            $source = input('source', ''); //来源
            $brand_id = input('brand_id', ''); //品牌
            $is_need_card = input('is_need_card', ''); //海外购
            if (!empty($promotion) && addon_is_exit($promotion)) {
                $pintuan_name = input('pintuan_name', '');//拼团活动
                $goods_list = event('GoodsListPromotion', ['page' => $page, 'page_size' => $page_size, 'site_id' => $this->site_id, 'promotion' => $promotion, 'pintuan_name' => $pintuan_name, 'goods_name' => $goods_name], true);
            } else {
                $alias = 'g';
                $join = [
                    ['shop s', 'g.site_id = s.site_id', 'left']
                ];
                $condition = [
                    ['g.is_delete', '=', 0],
                    ['g.goods_state', '=', 1],
                    ['g.verify_state', '=', 1],
                    ['s.shop_status', '=', 1],
                ];
                if(!empty($this->site_id)) $condition[] = ['g.site_id', '=', $this->site_id];
                if (!empty($goods_name)) {
                    $condition[] = ['g.goods_name', 'like', '%' . $goods_name . '%'];
                }
                if (!empty($shop_name)) {
                    $condition[] = ['g.site_name', 'like', '%' . $shop_name . '%'];
                }
                if ($is_virtual !== "") {
                    $condition[] = ['g.is_virtual', '=', $is_virtual];
                }
                if (!empty($goods_id)) {
                    $condition[] = ['g.goods_id', '=', $goods_id];
                }
                if($search_type == 'checked'){
                    $condition[] = ['g.goods_id', 'in', $select_id];
                }
                if (!empty($category_id)) {
                    $condition[] = ['g.category_id_1|g.category_id_2|g.category_id_3', '=', $category_id];

                }

                if (!empty($promotion_type)) {
                    $condition[] = ['g.promotion_addon', 'like', "%{$promotion_type}%"];
                }


                if ($goods_class !== "") {
                    $condition[] = ['g.goods_class', '=', $goods_class];
                }

                if ($min_price != "" && $max_price != "") {
                    $condition[] = ['g.price', 'between', [$min_price, $max_price]];
                } elseif ($min_price != "") {
                    $condition[] = ['g.price', '<=', $min_price];
                } elseif ($max_price != "") {
                    $condition[] = ['g.price', '>=', $max_price];
                }
                if(!empty($source)){
                    $condition[] = ['g.source', '=', $source];
                }
                
                if(!empty($brand_id)){
                    $condition[] = ['g.brand_id', '=', $brand_id];
                }
                
                if($is_need_card!==''){
                    if($is_need_card==1){
                        $condition[] = ['g.trade_type', 'in', [2,3]];
                    }else{
                        $condition[] = ['g.trade_type', 'in', [0,1,4,5]];
                    }
                }

                $order = 'g.create_time desc';
                $goods_model = new GoodsModel();
                $field = 'g.goods_id,g.goods_name,g.goods_class_name,g.goods_image,g.price,g.goods_stock,g.create_time,g.is_virtual,source,g.site_name,g.brand_id';
                $goods_list = $goods_model->getGoodsPageList($condition, $page, $page_size, $order, $field, $alias, $join);

                if (!empty($goods_list[ 'data' ][ 'list' ])) {
                    foreach ($goods_list[ 'data' ][ 'list' ] as $k => $v) {
                        $goods_sku_list = $goods_model->getGoodsSkuList([['goods_id', '=', $v[ 'goods_id' ]]], 'sku_id,sku_name,price,stock,sku_image,goods_id,goods_class_name,source');
                        $goods_sku_list = $goods_sku_list[ 'data' ];
                        $goods_list[ 'data' ][ 'list' ][ $k ][ 'sku_list' ] = $goods_sku_list;
                    }

                }
            }
            return $goods_list;
        } else {

            //已经选择的商品sku数据
            $select_id = input('select_id', '');
            $mode = input('mode', 'spu');
            $max_num = input('max_num', 0);
            $min_num = input('min_num', 0);
            $is_virtual = input('is_virtual', '');
            $disabled = input('disabled', 0);
            $promotion = input('promotion', '');//营销活动标识：pintuan、groupbuy、seckill、fenxiao

            $this->assign('select_id', $select_id);
            $this->assign('mode', $mode);
            $this->assign('max_num', $max_num);
            $this->assign('min_num', $min_num);
            $this->assign('is_virtual', $is_virtual);
            $this->assign('disabled', $disabled);
            $this->assign('promotion', $promotion);

            // 营销活动
            $goods_promotion_type = event('GoodsPromotionType');
            $this->assign('promotion_type', $goods_promotion_type);

            //商品分类
            $goods_category_model = new GoodsCategoryModel();
            $category_list = $goods_category_model->getCategoryList([], 'category_id, category_name as title, pid')['data'];
            $tree = list_to_tree($category_list, 'category_id', 'pid', 'children', 0);
            $this->assign("category_list", $tree);

            //获取品牌;
            $goods_brand_model = new GoodsBrandModel();
            $brand_list = $goods_brand_model->getBrandList([['site_id', 'in', ("0,$this->site_id")]], "brand_id, brand_name");
            $brand_list = $brand_list[ 'data' ];
            $this->assign("brand_list", $brand_list);
            return $this->fetch("goods/goods_select");
        }
    }

    /**
     * 候鸟商品动态（下架）
     * @return array|mixed
     */
    public function dynamicList()
    {
        $goods_model = new GoodsModel();
        if (request()->isAjax()) {
            $page_index = input('page', 1);
            $page_size = input('page_size', PAGE_LIST_ROWS);
            $search_text = input('search_text', "");

            $condition[] = ['status' ,'=', 0];
            if (!empty($search_text)) {
                $condition[] = [ 'goods_name', 'like', '%' . $search_text . '%' ];
            }

            $order = 'id desc';
            $res = $goods_model->getDynamicPageList($condition, $page_index, $page_size, $order);
            return $res;
        } else {
            return $this->fetch("goods/dynamic_list");
        }
    }

    /**
     * 处理候鸟导致的下架商品
     */
    public function dealGoods()
    {
        if (request()->isAjax()) {
            $id = input('id', 1);
            $goods_model = new GoodsModel();
            return $goods_model->dealGoods($id);
        }
    }

    public function checkCorporateSubscription(){
        $goods_id = (int)input('goods_id', '');
        $subscription_status = (int)input('subscription_status', 0);
        $subscription_check_reason= input('subscription_check_reason', '');
        $condition = ['goods_id' => $goods_id];
        $data = [
            'subscription_status' => $subscription_status,
            'subscription_check_reason' => $subscription_check_reason,
        ];
        $goods_model = new GoodsModel();
        return $goods_model->checkCorporateSubscription($data,$condition);
    }

    /**
     * 商品导出
     */
    public function export(){
        $export_model = new GoodsExport();
        if (request()->isAjax()) {

            $condition = array(
                ['site_id', '=', $this->site_id]
            );
            $result = $export_model->getExport($condition, '*', 'create_time desc');
            foreach($result['data'] as $key=>$val){
                $result['data'][$key]['file_name'] = basename($val['path']);
            }

            return $result;
        }else{
            return $this->fetch("goods/export");
        }
    }

    /**
     * 导出商品操作
     */
    public function exportGoods()
    {
        try{
            $search_text = input('search_text', "");
            $search_text_type = input('search_text_type', "goods_name");
            $goods_state = input('goods_state', "");
            $verify_state = input('verify_state', "");
            $category_id = input('category_id', "");
            $brand_id = input('goods_brand', '');
            $goods_class = input('goods_class', "");
            $source = input('source', "");

            $condition = [['is_delete', '=', 0]];
            //条件数组的陈诉
            $condition_desc = [];

            //店铺或商品名称
            if (!empty($search_text)) {
                $condition[] = [$search_text_type, 'like', '%' . $search_text . '%'];
            }
            $search_text_type_arr = [
                'site_name' => '店铺名称',
                'goods_name' => '商品名称',
            ];
            $condition_desc[] = ['name' => $search_text_type_arr[$search_text_type] ?? '', 'value' => $search_text];

            //商品类型
            $goods_class_value = '';
            if ($goods_class !== "") {
                $condition[] = ['goods_class', '=', $goods_class];
                $goods_class_array = ['1' => '实物商品', '2' => '虚拟商品3', '3' => '卡券商品'];
                $goods_class_value = $goods_class_array[$goods_class] ?? '';
            }
            $condition_desc[] = ['name' => '商品类型', 'value' => $goods_class_value];

            //上下架状态
            if ($goods_state !== '') {
                $condition[] = ['goods_state', '=', $goods_state];
                $goods_state_array = [1=>'正常', 0 => '下架'];
                $goods_state_value = $goods_state_array[$goods_state] ?? '';
                $condition_desc[] = ['name' => '商品状态', 'value' => $goods_state_value];
            }

            // 审核状态
            if ($verify_state !== '') {
                $condition[] = ['verify_state', '=', $verify_state];
                $verify_state_array =  [1=>'已审核', 0 => '待审核', 10 => '违规下架', -1 => '审核中', -2 => '审核失败'];
                $verify_state_value = $verify_state_array[$verify_state] ?? '';
                $condition_desc[] = ['name' => '审核状态', 'value' => $verify_state_value];
            }

            //商品分类
            $category_name = '';
            if(!empty($category_id)){
                $condition[] = ['category_id_1|category_id_2|category_id_3', '=', $category_id];
                $goods_category = new GoodsCategoryModel();
                $category_info = $goods_category->getCategoryInfo([['category_id', '=', $category_id]], 'category_name')['data'];
                $category_name = $category_info['category_name'] ?? '';
            }
            $condition_desc[] = ['name' => '商品分类', 'value' => $category_name];

            //商品品牌
            $brand_name = '';
            if(!empty($brand_id)){
                $condition[] = ['brand_id', '=', $brand_id];
                $brand_model = new GoodsBrandModel();
                $brand_info = $brand_model->getBrandInfo([['brand_id', '=', $brand_id]], 'brand_name');
                $brand_name = $brand_info['brand_name'] ?? '';
            }
            $condition_desc[] = ['name' => '商品品牌', 'value' => $brand_name];

            //商品来源
            $source_type_name = '全部';
            if($source != 'all'){
                $condition[] = ['source', '=', $source];
                $source_type_name = GoodsModel::getSourceTypeName($source);
            }
            $condition_desc[] = ['name' => '商品来源', 'value' => $source_type_name];

            $relate_id = Cache::get('ExportGoodsDataId');
            $relate_id = $relate_id ? $relate_id + 1 : 1;
            Cache::set('ExportGoodsData_' . $relate_id, ['condition' => $condition, 'condition_desc' => $condition_desc]);
            $cron = new \app\model\system\Cron();
            $cron->addCron(1, 0, "导出商品", "ExportGoodsData", time(), $relate_id);

            return success(0, '已建立导出任务，请耐心等待', ['relate_id' => $relate_id]);
        }catch(\Exception $e){
            return error(-1, $e->getMessage() . $e->getFile() . $e->getLine());
        }
    }
}
