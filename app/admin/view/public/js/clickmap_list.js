/**
 * 渲染热力图列表
 */
Clickmap = function(){};

/**
 * 设置数据集
 */
Clickmap.prototype.setData = function(data, start_time, end_time){
    Clickmap.prototype.data = data;
    Clickmap.prototype.start_time = start_time;
    Clickmap.prototype.end_time = end_time;
};

/**
 * 列名数据
 */
Clickmap.prototype.cols = [
    {
        title : "<span style='margin-left:10px;' class='ns-line-hiding' title='编号'>编号</span>",
        width : "10%",
        className : "clickmap-id",
        template : function(clickmapitem){
            var h = "<span>" + clickmapitem.id + "</span>";
            return h;
        }
    },
    {
        title : "<span style='margin-left:10px;' class='ns-line-hiding' title='页面名称'>页面名称</span>",
        width : "22%",
        className : "clickmap-pagename",
        template : function(clickmapitem){
            var h = "<span>" + clickmapitem.page_name + "</span>";
            return h;
        }
    },
    {
        title : "<span style='margin-left:10px;' class='ns-line-hiding' title='页面URL'>页面URL</span>",
        width : "34%",
        className : "clickmap-location",
        template : function(clickmapitem){
            var h = "<span>" + clickmapitem.location + "</span>";
            return h;
        }
    },
    {
        title : "<span style='margin-left:10px;' class='ns-line-hiding' title='创建时间'>创建时间</span>",
        width : "17%",
        className : "clickmap-createtime",
        template : function(clickmapitem){
            var h = "<span>" + ns.time_to_date(clickmapitem.create_time) + "</span>";
            return h;
        }
    },
    {
        title : "操作",
        width : "17%",
        align : "left",
        className : "operation",
        merge : true,
        template : function(clickmapitem, clickmap_start_time, clickmap_end_time){
            var html = "<div class='ns-table-btn'>";
            if (clickmap_start_time && !clickmap_end_time) {
                html += "<a class='layui-btn ns-line-hiding' onclick='clickmaps(" + clickmapitem.id + ",\"" + clickmapitem.location + "\", " + clickmap_start_time + ",  \"\")'>查看热力图</a>";
            } else if (!clickmap_start_time && clickmap_end_time) {
                html += "<a class='layui-btn ns-line-hiding' onclick='clickmaps(" + clickmapitem.id + ",\"" + clickmapitem.location + "\", \"\", " + clickmap_end_time + ")'>查看热力图</a>";
            } else if (clickmap_start_time && clickmap_end_time) {
                html += "<a class='layui-btn ns-line-hiding' onclick='clickmaps(" + clickmapitem.id + ",\"" + clickmapitem.location + "\", " + clickmap_start_time + ", " + clickmap_end_time + ")'>查看热力图</a>";
            } else {
                html += "<a class='layui-btn ns-line-hiding' onclick='clickmaps(" + clickmapitem.id + ",\"" + clickmapitem.location + "\",  \"\",  \"\")'>查看热力图</a>";
            }
            html += "<a class='layui-btn ns-line-hiding' onclick='editClickmap(" + clickmapitem.id + ")'>编辑</a>";
            html += "</div>";
            return html;

        }
    }
];

/**
 * 渲染表头
 */
Clickmap.prototype.header = function(){
    var colgroup = "<colgroup>";
    var thead = "<thead><tr>";

    for(var i=0;i<this.cols.length;i++){
        var align = this.cols[i].align ? "text-align:" + this.cols[i].align : "";

        colgroup += "<col width='" + this.cols[i].width + "'>";
        thead += "<th style='" + align + "' class='" + (this.cols[i].className || "") + "'>";
        thead += "<div class='layui-table-cell'>" + this.cols[i].title + "</div>";
        thead += "</th>";
    }
    colgroup += "</colgroup>";
    thead += "</tr></thead>";
    return colgroup + thead;
};

/**
 * 渲染内容
 */
Clickmap.prototype.tbody = function(){

    var tbody = "<tbody>";
    for(var i=0;i<this.data.list.length;i++){
        var clickmap_start_time = this.start_time ? this.start_time : '';
        var clickmap_end_time = this.end_time ? this.end_time : '';
        var clickmapitem = this.data.list[i];
        var itemHtml = "";
        itemHtml += "<tr>";
        for(var k=0;k<this.cols.length;k++) {
            itemHtml += "<td class='" + (this.cols[k].className || "") + "' align='" + (this.cols[k].align || "") + "' style='" + (this.cols[k].style || "") + "'>";
            itemHtml += this.cols[k].template(clickmapitem, clickmap_start_time, clickmap_end_time);
            itemHtml += "</td>";
        }
        itemHtml += "</tr>";
        tbody += itemHtml;

    }

    tbody += "</tbody>";
    return tbody;
};

/**
 * 渲染表格
 */
Clickmap.prototype.fetch = function(){
    if(this.data.list.length > 0){

        return "<table class='layui-table clickmap-list-table layui-form'>" + this.header() + this.tbody() + "</table>";
    }else{
        return "<table class='layui-table clickmap-list-table layui-form'>" + this.header() + "</table>" + "<div class='clickmap-no-data-block'><ul><li><i class='layui-icon layui-icon-tabs'></i> </li><li>暂无页面</li></ul></div>";
    }
};