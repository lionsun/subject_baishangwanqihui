var floorForm, floorLayer, floorUpload, floorLaytpl, floorColorpicker, repeatFlag = false;
layui.use(['form', 'layer', 'upload', 'laytpl', 'colorpicker'], function () {
	floorForm = layui.form;
	floorLayer = layui.layer;
	floorUpload = layui.upload;
	floorLaytpl = layui.laytpl;
	floorColorpicker = layui.colorpicker;
	floorForm.render();

	if ($("#info").length > 0) {
		setTimeout(function () {
			vm.data = JSON.parse($("#info").val().toString());
			vm.blockId = parseInt($("#block_id").val().toString());
		}, 100);
	}

	//选择楼层样式
	floorForm.on('select(block_id)', function (data) {
		var value = $(data.elem).find("option:selected").attr("data-value");
		var blockId = $(data.elem).find("option:selected").attr("data-block-id");
		vm.blockId = blockId;
		if (value) {
			vm.data = JSON.parse(value);
		}
	});

	//选择营销活动类型
    floorForm.on('select(promotion_type)', function (data) {
        let promotion_type = data.value;
        $(".promotion_type").addClass('layui-hide');
        $(".promotion_type." + promotion_type).removeClass('layui-hide');
        $(".clear_data_control").click();
    });

    //获取专题活动列表
    function getTopicLists(){
    	$.ajax({
			url: ns.url("topic://admin/topic/lists"),
			data: {
				page : 1,
				page_size : 0,
			},
			dataType: 'JSON',
			type: 'POST',
			success: function (res) {
				if(res.code >= 0){
					let init_topic_id = $("select[name='topic_id']").attr('data-init');
					let list = res.data.list;
					let html = '<option value="0">请选择</option>';
					$.each(list, function(index, item){
						let selected = init_topic_id == item.topic_id ? 'selected' : '';
						html += '<option value="'+ item.topic_id +'" '+ selected +'>'+ item.topic_name +'</option>';
					})

					$("select[name='topic_id']").html(html);
					floorForm.render();
				}
			}
		});
    }
    getTopicLists();

    //选择专题活动
    floorForm.on('select(topic_id)', function (data) {
        let topic_id = data.value;
        $(".clear_data_control").click();
    });

    //获取分类数据
    function getCategoryLists(pid, level){
    	$.ajax({
			url: ns.url("admin/goodscategory/getCategoryList"),
			data: {
				pid : pid,
			},
			dataType: 'JSON',
			type: 'POST',
			success: function (res) {
				if(res.code >= 0){
					let dom = $("select[name='category_id_"+ level +"']");
					let is_init = dom.attr('data-is-init');
					let category_id = dom.attr('data-init');
					console.log(category_id);
					let list = res.data;
					let html = '<option value="-1">请选择</option>';
					$.each(list, function(index, item){
						let selected = category_id == item.category_id ? 'selected' : '';
           				html += '<option value="'+ item.category_id +'" '+ selected +'>'+ item.category_name +'</option>';
						})
					$("select[name='category_id_"+ level +"']").html(html).attr({'data-init' : -1, 'data-is-init' : 1});
					floorForm.render();
				}
			}
		});
    }

    //获取一级分类
    let is_init = $("select[name='category_id_1']").attr('data-is-init');
    if(is_init == 0){
    	getCategoryLists(0, 1);
    	let category_id_1 = $("select[name='category_id_1']").attr('data-init');
    	getCategoryLists(category_id_1, 2);
    }else{
    	getCategoryLists(0, 1);
    }
    
    //获取二级分类
    floorForm.on('select(category_id_1)', function (data) {
        let category_id = data.value;
        getCategoryLists(category_id, 2);
    });

    //选择商品来源
    floorForm.on('radio(goods_source)', function (data) {
        let goods_source = data.value;
        $(".goods_source").addClass('layui-hide');
        $(".goods_source." + goods_source).removeClass('layui-hide');
        $(".goods_source_control").click();
    });


	floorForm.verify({
		title: function (value) {
			if (value == '') {
				return '请输入楼层名称';
			}
			if (value.length > 100) {
				return '最多100个字符';
			}
		},
		block_id: function (value) {
			if (!value) return '请选择楼层模板';
		},
		topic_id: function(value) {
			let promotion_type = $("select[name='promotion_type']").val();
			if(promotion_type == 'topic' && value == '0'){
				return '请选择专题活动';
			}
		},
		category_id_1: function(value){
			let goods_source = $("input[type='radio'][name='goods_source']:checked").val();
			if(goods_source == 'category' && value == '-1'){
				return '请选择商品分类';
			}
		},
	});

	floorForm.on('submit(save)', function (data) {

		let goods_list_rule = {};
		goods_list_rule.promotion_type = data.field.promotion_type;
		goods_list_rule.topic_id = data.field.topic_id;
		goods_list_rule.goods_source = data.field.goods_source;
		goods_list_rule.category_id_1 = data.field.category_id_1;
		goods_list_rule.category_id_2 = data.field.category_id_2;


		var value = JSON.parse(JSON.stringify(vm.data));
		value.goods_list_rule = goods_list_rule;
		for (var i in value) {
			if ($.inArray(value[i].type, ['goods', 'brand', 'category']) != -1) {
				value[i].value.list = [];
			}
		}

		data.field.value = JSON.stringify(value);

		if (repeatFlag) return;
		repeatFlag = true;

		$.ajax({
			url: ns.url("admin/pc/editFloor"),
			data: data.field,
			dataType: 'JSON',
			type: 'POST',
			success: function (res) {
				floorLayer.msg(res.message);
				if (res.code == 0) {
					if(parent!=null){
						parent.addOrEditAdvCallback('add_floor_success');
					}else{
						location.href = ns.url("admin/pc/floor", {view_name : data.field.view_name});
					}
				}
				repeatFlag = false;
			}
		});
	});
});

var vm = new Vue({
	el: "#app",
	data: function () {
		return {
			data: null,
			blockId: 0
		};
	},
	created: function () {
	},
	methods: {
		img: function (url) {
			return url ? ns.img(url) : "";
		},
		/**
		 * 初始化链接下拉框
		 * @param select_tag
		 * @param link_tag
		 */
		initLink: function (select_tag, link_tag) {
			floorForm.on('select(' + select_tag + ')', function (data) {
				var title = $(data.elem).find("option:selected").text();
				if (data.value != 'diy') {
					$("input[name='" + link_tag + "']").val(JSON.stringify({
						"title": title,
						"url": data.value
					}));
				} else {
					floorLayer.prompt({
						formType: 2,
						value: $("input[name='" + link_tag + "']").val() ? JSON.parse($("input[name='" + link_tag + "']").val()).url : '',
						title: '自定义链接地址',
						area: ['450px', '100px'],
						cancel: function () {
							$("input[name='" + link_tag + "']").val("");
						}
					}, function (value, index, elem) {
						$("input[name='" + link_tag + "']").val(JSON.stringify({
							"title": title,
							"url": value
						}));
						floorLayer.close(index);
					});
				}
			});
		},
		/**
		 * 设置文本
		 * @param data 当前数据
		 * @param callback 回调
		 */
		setText: function (data, callback) {
			var self = this;
			var getTpl = $("#setTitleHtml").html();
			if (!data) data = {};
			floorLaytpl(getTpl).render(data, function (html) {
				var textLayer = floorLayer.open({
					type: 1,
					title: "编辑文本",
					content: html,
					area: ['400px', '300px'],
					success: function (layero, index) {
						floorForm.render();
						self.initLink("pc_link_text", "text_link");

						// 文字颜色
						floorColorpicker.render({
							elem: '#text_color',  //绑定元素
							color: data.color ? data.color : "",
							done: function (color) {
								$("#text_color_input").attr("value", color);
							}
						});

						floorForm.on('submit(save_text)', function (data) {
							if (data.field.text_link) data.field.text_link = JSON.parse(data.field.text_link);
							if (callback) callback({
								text: data.field.text,
								link: data.field.text_link,
								color: data.field.text_color
							});
							floorLayer.close(textLayer);
						});
					}
				});
				floorForm.render();
			});
		},
		/**
		 * 上传图片
		 * @param data 当前数据
		 * @param callback 回调
		 */
		uploadImg: function (data, callback) {
			var self = this;
			var getTpl = $("#uploadImg").html();
			if (!data) data = {};
			floorLaytpl(getTpl).render(data, function (html) {
				var textLayer = floorLayer.open({
					type: 1,
					title: "上传图片",
					content: html,
					area: ['450px', '300px'],
					success: function (layero, index) {
						floorForm.render();
						floorUpload.render({
							elem: "#upload_image",
							url: ns.url("admin/upload/upload"),
							done: function (res) {
								$("input[name='upload_image']").val(res.data.pic_path);
								$("#upload_image").html("<img src=" + ns.img(res.data.pic_path) + " >");
							}
						});
						self.initLink("pc_link_upload", "upload_link");
						floorForm.on('submit(save_upload)', function (data) {
							if (data.field.upload_link) data.field.upload_link = JSON.parse(data.field.upload_link);
							if (callback) callback({
								url: data.field.upload_image,
								link: data.field.upload_link
							});
							floorLayer.close(textLayer);
						});
					}
				});
				floorForm.render();
			});
		},
		/**
		 * 设置商品分类
		 * @param data 当前数据
		 * @param callback 回调
		 */
		setCategory: function (data, callback) {
			var self = this;
			var getTpl = $("#setCategoryHtml").html();
			if (!data) data = {};
			floorLaytpl(getTpl).render(data, function (html) {
				var textLayer = floorLayer.open({
					type: 1,
					title: "编辑商品分类",
					content: html,
					area: ['600px', '400px'],
					success: function (layero, index) {
						floorForm.render();
						floorForm.on('select(goods_category)', function (categoryData) {
							var category_name = $.trim($(categoryData.elem).find("option:selected").text());
							var category_id = $(categoryData.elem).val();
							var isAdd = true;
							for (var i = 0; i < data.list.length; i++) {
								if (data.list[i].category_id == category_id) {
									isAdd = false;
									break;
								}
							}
							if (isAdd) {
								data.list.push({
									category_id: category_id,
									category_name: category_name
								});
								floorLaytpl(getTpl).render(data, function (html) {
									$(".set-category").html(html);
									floorForm.render();
								});
							}
						});

						floorForm.on('submit(save_category)', function (data) {
							if (data.field.category_ids) data.field.category_ids = data.field.category_ids.replace(/\s+/g, "");
							if (callback) callback({
								category_ids: data.field.category_ids,
								list: data.field.category_list ? JSON.parse(data.field.category_list) : [],
							});
							floorLayer.close(textLayer);
						});
					}
				});
				floorForm.render();
			});
		},
	}
});

/**
 * 商品选择器
 * @param callback 回调函数
 * @param selectId 已选商品id
 * @param params mode：模式(spu、sku), max_num：最大数量，min_num 最小数量, is_virtual 是否虚拟 0 1, disabled: 开启禁用已选 0 1
 */
function goodsSelect(callback, selectId, params) {
	
	let promotion_type = $("select[name='promotion_type']").val();
	let topic_id = $("select[name='topic_id']").val();
	let goods_source = $("input[type='radio'][name='goods_source']:checked").val();

	if(goods_source != 'diy'){
		return false;
	}
	if(promotion_type == 'topic'){
		if(topic_id == 0){
			layer.msg('请选择专题活动');
			return false;
		}
		params.activity_id = topic_id;
	}
	//营销配置
	let promotion_config = {
		none : {
			url : "admin/pc/goodsselect",
			select_type : 'sku',
		},
		topic : {
			url : "topic://admin/goodssku/goodsselect",
			select_type : 'sku',
		},
	};

	params.select_id = selectId[promotion_config[promotion_type].select_type].toString();

	var url = ns.url(promotion_config[promotion_type].url, params);
	//iframe层-父子操作
	floorLayer.open({
			title: "商品选择",
			type: 2,
			area: ['80%', '80%'],
			fixed: false, //不固定
			btn: ['保存', '返回'],
			content: url,
			yes: function (index, layero) {
				var iframeWin = window[layero.find('iframe')[0]['name']];//得到iframe页的窗口对象，执行iframe页的方法：
				iframeWin.selectGoods(function (obj) {
					if (typeof callback == "string") {
						try {
							eval(callback + '(obj)');
							floorLayer.close(index);
						} catch (e) {
							console.error('回调函数' + callback + '未定义');
						}
					} else if (typeof callback == "function") {
						callback(obj);
						floorLayer.close(index);
					}

				});
			}
		}
	);
}

/**
 * 品牌选择器
 * @param callback 回调函数
 * @param selectId 已选商品id
 * @param params mode：模式(spu、sku), max_num：最大数量，min_num 最小数量, disabled: 开启禁用已选 0 1
 */
function brandSelect(callback, selectId, params) {
	if (selectId.length) {
		params.select_id = selectId.toString();
	}
	var url = ns.url("admin/pc/brandselect", params);
	//iframe层-父子操作
	floorLayer.open({
			title: "品牌名称",
			type: 2,
			area: ['1000px', '600px'],
			fixed: false, //不固定
			btn: ['保存', '返回'],
			content: url,
			yes: function (index, layero) {
				var iframeWin = window[layero.find('iframe')[0]['name']];//得到iframe页的窗口对象，执行iframe页的方法：
				iframeWin.selectBrands(function (obj) {
					if (typeof callback == "string") {
						try {
							eval(callback + '(obj)');
							floorLayer.close(index);
						} catch (e) {
							console.error('回调函数' + callback + '未定义');
						}
					} else if (typeof callback == "function") {
						callback(obj);
						floorLayer.close(index);
					}

				});
			}
		}
	);
}

//获取商品来源
function getGoodsSource(){
	return $("input[type='radio'][name='goods_source']:checked").val();
}