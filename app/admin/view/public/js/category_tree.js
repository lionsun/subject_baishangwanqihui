//分类树
function CategoryTree(param){
    let that = this;
    that.disabled_ids = param.disabled_ids || '';//禁用的数据
    that.selected_ids = param.selected_ids || '';//选中的数据
    that.tree_box = param.tree_box;//树容器id
    that.tree = param.tree;//layui的树组件
    that.tree_data = [];//树数据
    that.category_level = param.category_level || 3;//分类等级
    that.selected_id_arr = [];//最终操作完成后的选中数据

    that.getGoodsCategory = function(callback) {
        $.ajax({
            url: ns.url("admin/goodscategory/getGoodsCategoryTree"),
            data: {
                level : that.category_level,
            },
            dataType: 'JSON',
            type: 'POST',
            success: function (res) {
                callback(res.data);
            }
        });
    }

    that.renderTree = function(){
        that.tree.render({
            elem: '#' + that.tree_box,
            data: that.tree_data,
            showCheckbox:true,
            id:that.tree_box,
        });
        if(that.disabled_ids){
            let disabled_id_arr = that.disabled_ids.split(',');
            for(let i in disabled_id_arr){
                $("input[name='layuiTreeCheck_"+ disabled_id_arr[i] +"']").prop('disabled', true).next().addClass('layui-checkbox-disbaled layui-disabled').next().addClass('layui-disabled').append('（不可选）');
            }
        }
        if(that.selected_ids){
            let selected_id_arr = that.selected_ids.split(',');
            for(let i in selected_id_arr){
                $("input[name='layuiTreeCheck_"+ selected_id_arr[i] +"']").prop('checked', true).next().addClass('layui-form-checked');
            }
        }
    }

    function getSelectedId(data){
        for(let i in data){
            that.selected_id_arr.push(data[i].id);
            if(data[i].children){
                getSelectedId(data[i].children);
            }
        }
    }

    that.getSelectedId = function(){
        let data = that.tree.getChecked(that.tree_box);
        that.selected_id_arr = [];
        getSelectedId(data);
        return that.selected_id_arr;
    }

    that.init = function(){
        that.getGoodsCategory(function(data){
            that.tree_data = data;
            that.renderTree();
        })
    }

    that.init();
}