<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\web;


use app\model\BaseModel;
use app\model\goods\Goods as GoodsModel;
use app\model\goods\GoodsBrand as GoodsBrandModel;
use app\model\goods\GoodsCategory as GoodsCategoryModel;
use app\model\system\Api;
use app\model\system\Config as ConfigModel;
use think\facade\Cache;

/**
 * PC端
 * @author Administrator
 *
 */
class Pc extends BaseModel
{

	private $link = [
		[
			'title' => '首页',
			'url' => '/index'
		],
		[
			'title' => '登录',
			'url' => '/login'
		],
		[
			'title' => '注册',
			'url' => '/register'
		],
		[
			'title' => '找回密码',
			'url' => '/find_pass'
		],
		[
			'title' => '公告列表',
			'url' => '/cms/notice'
		],
		[
			'title' => '帮助中心',
			'url' => '/cms/help'
		],
		[
			'title' => '购物车',
			'url' => '/cart'
		],
		[
			'title' => '品牌专区',
			'url' => '/brand'
		],
		[
			'title' => '领券中心',
			'url' => '/coupon'
		],
		[
			'title' => '商品分类',
			'url' => '/category'
		],
		[
			'title' => '团购专区',
			'url' => '/promotion/groupbuy'
		],
		[
			'title' => '秒杀专区',
			'url' => '/promotion/seckill'
		],
		[
			'title' => '会员中心',
			'url' => '/member/home/index'
		]

	];

	private $not_found_file_error = "未找到源码包，请检查目录文件";

	private $floor_block_config = [
			1 => [
				'total' => 8,
				'slice_config' => [
					['start' => 0,'length' => 8],
				],
			],
			2 => [
				'total' => 20,
				'slice_config' => [
					['start' => 0,'length' => 20],
				],
			],
			3 => [
				'total' => 16,
				'slice_config' => [
					['start' => 0,'length' => 10],
					['start' => 10,'length' => 6],
				],
			],
		];

	/*************************************************网站部署******************************************/

	public function getFloorBlockConfig()
	{
		return $this->floor_block_config;
	}

	/**
	 * 默认部署：无需下载，一键刷新，API接口请求地址为当前域名，编译代码存放到web文件夹中
	 * @return array
	 */
	public function downloadCsDefault()
	{
		try {

			$path = 'public/web/cs_default';
			$web_path = 'web'; // web端生成目录
			$config_path = 'web/assets/js'; // web模板文件目录
			if (!is_dir($path) || count(scandir($path)) <= 3) {
				return $this->error('', $this->not_found_file_error);
			}

			if (is_dir($web_path)) {
				// 先将之前的文件删除
				if (count(scandir($web_path)) > 1) deleteDir($web_path);
			} else {
				// 创建web目录
				mkdir($web_path, intval('0777', 8), true);
			}

			// 将原代码包拷贝到web目录下
			recurseCopy($path, $web_path);
			$this->copyFile($config_path);
			file_put_contents($web_path . '/refresh.log', time());
			return $this->success();
		} catch (\Exception $e) {
			return $this->error('', $e->getMessage() . $e->getLine());
		}
	}

	/**
	 * 独立部署：下载编译代码包，参考开发文档进行配置
	 * @param $domain
	 * @return array
	 */
	public function downloadCsIndep($domain)
	{
		try {

			$path = 'public/web/cs_indep';
			$source_file_path = 'upload/web/cs_indep'; // web端生成目录
			$config_path = $source_file_path . '/assets/js'; // h5模板文件目录
			if (!is_dir($path) || count(scandir($path)) <= 3) {
				return $this->error('', $this->not_found_file_error);
			}

			if (is_dir($source_file_path)) {
				// 先将之前的文件删除
				if (count(scandir($source_file_path)) > 2) deleteDir($source_file_path);
			} else {
				// 创建web目录
				mkdir($source_file_path, intval('0777', 8), true);
			}

			// 将原代码包拷贝到web目录下
			recurseCopy($path, $source_file_path);
			$this->copyFile($config_path, $domain);

			// 生成压缩包
			$file_arr = getFileMap($source_file_path);

			if (!empty($file_arr)) {
				$zipname = 'web_cs_indep_' . date('YmdHi') . '.zip';
				$zip = new \ZipArchive();
				$res = $zip->open($zipname, \ZipArchive::CREATE);
				if ($res === TRUE) {
					foreach ($file_arr as $file_path => $file_name) {
						if (is_dir($file_path)) {
							$file_path = str_replace($source_file_path . '/', '', $file_path);
							$zip->addEmptyDir($file_path);
						} else {
							$zip_path = str_replace($source_file_path . '/', '', $file_path);
							$zip->addFile($file_path, $zip_path);
						}
					}
					$zip->close();

					header("Content-Type: application/zip");
					header("Content-Transfer-Encoding: Binary");
					header("Content-Length: " . filesize($zipname));
					header("Content-Disposition: attachment; filename=\"" . basename($zipname) . "\"");
					readfile($zipname);
					@unlink($zipname);
				}
			}
			return $this->success();
		} catch (\Exception $e) {
			return $this->error('', $e->getMessage() . $e->getLine());
		}
	}

	/**
	 * 源码下载：下载开源代码包，参考开发文档进行配置，结合业务需求进行二次开发
	 * @return array
	 */
	public function downloadOs()
	{
		try {
			$source_file_path = 'public/web/os';
			if (!is_dir($source_file_path) || count(scandir($source_file_path)) <= 3) {
				return $this->error('', $this->not_found_file_error);
			}
			$file_arr = getFileMap($source_file_path);

			if (!empty($file_arr)) {
				$zipname = 'web_os_' . date('YmdHi') . '.zip';
				$zip = new \ZipArchive();
				$res = $zip->open($zipname, \ZipArchive::CREATE);
				if ($res === TRUE) {
					foreach ($file_arr as $file_path => $file_name) {
						if (is_dir($file_path)) {
							$file_path = str_replace($source_file_path . '/', '', $file_path);
							$zip->addEmptyDir($file_path);
						} else {
							$zip_path = str_replace($source_file_path . '/', '', $file_path);
							$zip->addFile($file_path, $zip_path);
						}
					}
					$zip->close();

					header("Content-Type: application/zip");
					header("Content-Transfer-Encoding: Binary");
					header("Content-Length: " . filesize($zipname));
					header("Content-Disposition: attachment; filename=\"" . basename($zipname) . "\"");
					readfile($zipname);
					@unlink($zipname);
				}
			}
		} catch (\Exception $e) {
			return $this->error('', $e->getMessage() . $e->getLine());
		}
	}

	/**
	 * 替换配置信息，API请求域名地址、图片、密钥等
	 * @param $source_path
	 * @param string $domain
	 */
	private function copyFile($source_path, $domain = __ROOT__)
	{
		$files = scandir($source_path);
		foreach ($files as $path) {
			if ($path != '.' && $path != '..') {
				$temp_path = $source_path . '/' . $path;
				if (file_exists($temp_path)) {
					if (preg_match("/(app.)(\w{8})(.js.map)$/", $temp_path)) {
						$content = file_get_contents($temp_path);
						$content = $this->paramReplace($content, $domain);
						file_put_contents($temp_path, $content);
					}
					if (preg_match("/(app.)(\w{8})(.js)$/", $temp_path)) {
						$content = file_get_contents($temp_path);
						$content = $this->paramReplace($content, $domain);
						file_put_contents($temp_path, $content);
					}
				}
			}
		}
	}

	/**
	 * 参数替换
	 * @param $string
	 * @param string $domain
	 * @return string|string[]|null
	 */
	private function paramReplace($string, $domain = __ROOT__)
	{
		$api_model = new Api();
		$api_config = $api_model->getApiConfig();
		$api_config = $api_config[ 'data' ];

		$patterns = [
			'/\{\{\$baseUrl\}\}/',
			'/\{\{\$imgDomain\}\}/',
			'/\{\{\$mpKey\}\}/',
			'/\{\{\$apiSecurity\}\}/',
			'/\{\{\$publicKey\}\}/'
		];
		$replacements = [
			$domain,
			$domain,
			'',
			$api_config[ 'is_use' ] ?? 0,
			$api_config[ 'value' ][ 'public_key' ] ?? ''
		];
		$string = preg_replace($patterns, $replacements, $string);
		return $string;
	}

	/**
	 * 获取PC端固定跳转链接
	 * @return array
	 */
	public function getLink()
	{
	    //获取pc导航链接
	    $promotion_link = event('PcNavLink');
	    foreach($promotion_link as $val){
	        $this->link = array_merge($this->link, $val);
        }
		foreach ($this->link as $k => $v) {
			if ($v[ 'url' ] == '/register' && addon_is_exit('memberregister') == 0) {
				unset($this->link[ $k ]);
			} elseif ($v[ 'url' ] == '/coupon' && addon_is_exit('coupon') == 0) {
				unset($this->link[ $k ]);
			} elseif ($v[ 'url' ] == '/promotion/seckill' && addon_is_exit('seckill') == 0) {
				unset($this->link[ $k ]);
			} elseif ($v[ 'url' ] == '/promotion/groupbuy' && addon_is_exit('groupbuy') == 0) {
				unset($this->link[ $k ]);
			}
		}
		$this->link = array_values($this->link);
		return $this->link;
	}

	/**
	 * 设置热门搜索关键词
	 * @param $data
	 * @return array
	 */
	public function setHotSearchWords($data)
	{
		$config = new ConfigModel();
		$res = $config->setConfig($data, 'PC端热门搜索关键词', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PC_HOT_SEARCH_WORDS_CONFIG' ] ]);
		return $res;
	}

	/**
	 * 获取热门搜索关键词
	 * @param $data
	 * @return array
	 */
	public function getHotSearchWords()
	{
		$config = new ConfigModel();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PC_HOT_SEARCH_WORDS_CONFIG' ] ]);
		if (empty($res[ 'data' ][ 'value' ])) {
			$res[ 'data' ][ 'value' ] = [
				'words' => ''
			];
		}
		return $res;
	}

	/**
	 * 设置默认搜索关键词
	 * @param $data
	 * @return array
	 */
	public function setDefaultSearchWords($data)
	{
		$config = new ConfigModel();
		$res = $config->setConfig($data, 'PC端默认搜索关键词', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PC_DEFAULT_SEARCH_WORDS_CONFIG' ] ]);
		return $res;
	}

	/**
	 * 获取默认搜索关键词
	 * @param $data
	 * @return array
	 */
	public function getDefaultSearchWords()
	{
		$config = new ConfigModel();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PC_DEFAULT_SEARCH_WORDS_CONFIG' ] ]);
		if (empty($res[ 'data' ][ 'value' ])) {
			$res[ 'data' ][ 'value' ] = [
				'words' => '搜索 商品/店铺'
			];
		}
		return $res;
	}

	/**
	 * 设置首页浮层
	 * @param $data
	 * @return array
	 */
	public function setFloatLayer($data)
	{
		$config = new ConfigModel();
		$res = $config->setConfig($data, 'PC端首页浮层', 1, [ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PC_INDEX_FLOAT_LAYER_CONFIG' ] ]);
		return $res;
	}

	/**
	 * 获取首页浮层
	 * @param $data
	 * @return array
	 */
	public function getFloatLayer()
	{
		$config = new ConfigModel();
		$res = $config->getConfig([ [ 'site_id', '=', 0 ], [ 'app_module', '=', 'admin' ], [ 'config_key', '=', 'PC_INDEX_FLOAT_LAYER_CONFIG' ] ]);
		if (empty($res[ 'data' ][ 'value' ])) {
			$res[ 'data' ][ 'value' ] = [
				'title' => '首页浮层',
				'url' => 'https://www.niushop.com',
				'is_show' => 1,
				'number' => '3',
				'img_url' => 'upload/default/pc/index_float_layer.png'
			];
		}
		return $res;
	}

	/*****************************************导航*****************************************/
	/**
	 * 添加导航
	 * @param array $data
	 */
	public function addNav($data)
	{
		$id = model('pc_nav')->add($data);
		Cache::tag("pc_nav")->clear();
		return $this->success($id);
	}

	/**
	 * 修改导航
	 * @param array $data
	 */
	public function editNav($data, $condition)
	{
		$res = model('pc_nav')->update($data, $condition);
		Cache::tag("pc_nav")->clear();
		return $this->success($res);
	}

	/**
	 * 删除导航
	 * @param unknown $coupon_type_id
	 */
	public function deleteNav($condition)
	{
		$res = model('pc_nav')->delete($condition);
		Cache::tag("pc_nav")->clear();
		return $this->success($res);
	}

	/**
	 * 获取导航详情
	 * @param int $id
	 * @return multitype:string mixed
	 */
	public function getNavInfo($id)
	{
		$cache = Cache::get("pc_nav_getNavInfo_" . $id);
		if (!empty($cache)) {
			return $this->success($cache);
		}
		$res = model('pc_nav')->getInfo([ [ 'id', '=', $id ] ], 'id, nav_title, nav_url, sort, is_blank, create_time, modify_time, nav_icon, is_show');
		Cache::tag("pc_nav")->set("pc_nav_getNavInfo_" . $id, $res);
		return $this->success($res);
	}

	/**
	 * 获取导航详情
	 * @param int $id
	 * @return multitype:string mixed
	 */
	public function getNavInfoByCondition($condition, $field = 'id, nav_title, nav_url, sort, is_blank, create_time, modify_time, nav_icon, is_show')
	{
		$res = model('pc_nav')->getInfo($condition, $field);
		return $this->success($res);
	}

	/**
	 * 获取导航分页列表
	 * @param array $condition
	 * @param number $page
	 * @param string $page_size
	 * @param string $order
	 * @param string $field
	 */
	public function getNavPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'create_time desc', $field = 'id, nav_title, nav_url, sort, is_blank, create_time, modify_time, nav_icon, is_show')
	{
		$data = json_encode([ $condition, $field, $order, $page, $page_size ]);
		$cache = Cache::get("pc_nav_getNavPageList_" . $data);
		if (!empty($cache)) {
			return $this->success($cache);
		}
		$list = model('pc_nav')->pageList($condition, $field, $order, $page, $page_size);
		Cache::tag("pc_nav")->set("pc_nav_getNavList_" . $data, $list);
		return $this->success($list);
	}

	/**
	 * 修改排序
	 * @param int $sort
	 * @param int $id
	 */
	public function modifyNavSort($sort, $id)
	{
		$res = model('pc_nav')->update([ 'sort' => $sort ], [ [ 'id', '=', $id ] ]);
		Cache::tag('pc_nav')->clear();
		return $this->success($res);
	}

	/*****************************************友情链接*****************************************/

	/**
	 * 添加友情链接
	 * @param array $data
	 */
	public function addLink($data)
	{
		$id = model('pc_friendly_link')->add($data);
		Cache::tag("pc_friendly_link")->clear();
		return $this->success($id);
	}

	/**
	 * 修改友情链接
	 * @param array $data
	 */
	public function editLink($data, $condition)
	{
		$res = model('pc_friendly_link')->update($data, $condition);
		Cache::tag("pc_friendly_link")->clear();
		return $this->success($res);
	}

	/**
	 * 删除友情链接
	 * @param unknown $coupon_type_id
	 */
	public function deleteLink($condition)
	{
		$res = model('pc_friendly_link')->delete($condition);
		Cache::tag("pc_friendly_link")->clear();
		return $this->success($res);
	}

	/**
	 * 获取友情链接详情
	 * @param int $id
	 * @return multitype:string mixed
	 */
	public function getLinkInfo($id)
	{
		$cache = Cache::get("pc_friendly_link_info" . $id);
		if (!empty($cache)) {
			return $this->success($cache);
		}
		$res = model('pc_friendly_link')->getInfo([ [ 'id', '=', $id ] ], 'id, link_title, link_url, link_pic, link_sort, is_blank, is_show');
		Cache::tag("pc_friendly_link")->set("pc_friendly_link_info" . $id, $res);
		return $this->success($res);
	}

	/**
	 * 获取导航分页列表
	 * @param array $condition
	 * @param number $page
	 * @param string $page_size
	 * @param string $order
	 * @param string $field
	 */
	public function getLinkPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'link_sort desc', $field = 'id, link_title, link_url, link_pic, link_sort, is_blank, is_show')
	{
		$data = json_encode([ $condition, $field, $order, $page, $page_size ]);
		$cache = Cache::get("pc_friendly_linkPageList_" . $data);
		if (!empty($cache)) {
			return $this->success($cache);
		}
		$list = model('pc_friendly_link')->pageList($condition, $field, $order, $page, $page_size);
		Cache::tag("pc_friendly_link")->set("pc_friendly_linkPageList_" . $data, $list);
		return $this->success($list);
	}

	/**
	 * 修改排序
	 * @param int $sort
	 * @param int $id
	 */
	public function modifyLinkSort($sort, $id)
	{
		$res = model('pc_friendly_link')->update([ 'link_sort' => $sort ], [ [ 'id', '=', $id ] ]);
		Cache::tag('pc_friendly_link')->clear();
		return $this->success($res);
	}

	/*****************************************首页楼层*****************************************/

	/**
	 * 添加楼层模板
	 * @param $data
	 * @return array
	 */
	public function addFloorBlockList($data)
	{
		$res = model("pc_floor_block")->addList($data);
		return $this->success($res);
	}

	/**
	 * 获取PC端首页楼层模板
	 * @return array
	 */
	public function getFloorBlockList()
	{
		$list = model('pc_floor_block')->getList([], 'id,name,title,value,sort');
		return $this->success($list);
	}

	/**
	 * 添加楼层
	 * @param $data
	 * @return array
	 */
	public function addFloor($data)
	{
		$data[ 'create_time' ] = time();
		$res = model("pc_floor")->add($data);
		return $this->success($res);
	}

	/**
	 * 编辑楼层
	 * @param $data
	 * @param $condition
	 * @return array
	 */
	public function editFloor($data, $condition)
	{
		$res = model("pc_floor")->update($data, $condition);
		return $this->success($res);
	}

	/**
	 * 修改首页楼层排序
	 * @param $sort
	 * @param $condition
	 * @return array
	 */
	public function modifyFloorSort($sort, $condition)
	{
		$res = model('pc_floor')->update([ 'sort' => $sort ], $condition);
		return $this->success($res);
	}

	/**
	 * 删除首页楼层
	 * @param $condition
	 * @return array
	 */
	public function deleteFloor($condition)
	{
		$res = model('pc_floor')->delete($condition);
		return $this->success($res);
	}

	/**
	 * 获取首页楼层信息
	 * @param $condition
	 * @param $field
	 * @return array
	 */
	public function getFloorInfo($condition, $field = 'id,block_id,title,value,state,create_time,sort')
	{
		$res = model("pc_floor")->getInfo($condition, $field);
		return $this->success($res);
	}

	/**
	 * 获取首页楼层详情
	 * @param $id
	 * @return array
	 */
	public function getFloorDetail($id)
	{
		$floor_info = model("pc_floor")->getInfo([ [ 'id', '=', $id ] ], 'id,block_id,title,value,state,sort,view_name');

		$floor_info = $this->dealWithFloorInfo($floor_info);

		return $this->success($floor_info);
	}

	/**
	 * 处理楼层信息
	 */
	public function dealWithFloorInfo($floor_info)
	{
		$block_config = $this->floor_block_config;
		$block_id = $floor_info['block_id'];

		$goods_model = new GoodsModel();
		$goods_brand_model = new GoodsBrandModel();
		$goods_category_model = new GoodsCategoryModel();
		if (!empty($floor_info)) {
			$value = $floor_info[ 'value' ];
			if (!empty($value)) {
				$value = json_decode($value, true);
				if(empty($value['goods_list_rule'])){
					$value['goods_list_rule'] = [
						'promotion_type' => 'none',
						'topic_id' => 0,
						'goods_source' => 'diy',
						'category_id_1' => -1,
						'category_id_2' => -1,
					];
				}

				//商品列表规则 如果是按照分类展示 先查询全部 然后再分配列表 因为有的样式里面有不止一个商品列表
				$goods_list_rule = $value['goods_list_rule'] ?? null;
				if($goods_list_rule && $goods_list_rule['goods_source'] == 'category'){
					$category_id = $goods_list_rule['category_id_2'] == -1 ? $goods_list_rule['category_id_1'] : $goods_list_rule['category_id_2'];
					if($goods_list_rule['promotion_type'] == 'none'){
						$total_list = $this->getCommonGoodsList([
							'goods_source' => 'category', 
							'category_id' => $category_id, 
							'limit' => $block_config[$block_id]['total']
						]);

					}else if($goods_list_rule['promotion_type'] == 'topic'){
						$total_list = $this->getTopicGoodsList([
							'goods_source' => 'category', 
							'category_id' => $category_id, 
							'limit' => $block_config[$block_id]['total']
						]);
					}
				}

				$goods_list_index = 0;
				foreach ($value as $k => $v) {
					if (!empty($v[ 'type' ])) {
						if ($v[ 'type' ] == 'goods') {
							if($goods_list_rule){
								if($goods_list_rule['goods_source'] == 'category'){
									$slice_config = $block_config[$block_id]['slice_config'][$goods_list_index];
									$value[ $k ][ 'value' ][ 'list' ] = array_slice($total_list, $slice_config['start'], $slice_config['length']);
									$goods_list_index ++;
								}else if($goods_list_rule['goods_source'] == 'diy'){
									if($goods_list_rule['promotion_type'] == 'none'){
										$value[ $k ][ 'value' ][ 'list' ] = $this->getCommonGoodsList([
											'goods_source' => 'diy',
											'sku_ids' => $value[ $k ][ 'value' ]['sku_ids'],
										]);
									}else if($goods_list_rule['promotion_type'] == 'topic'){

										$value[ $k ][ 'value' ][ 'list' ] = $this->getTopicGoodsList([
											'goods_source' => 'diy',
											'sku_ids' => $value[ $k ][ 'value' ]['sku_ids'],
										]);
									}
								}		
							}else{
								$value[ $k ][ 'value' ][ 'list' ] = $this->getCommonGoodsList([
									'goods_source' => 'diy',
									'sku_ids' => $value[ $k ][ 'value' ]['sku_ids'],
								]);
							}
						} elseif ($v[ 'type' ] == 'brand') {
							// 品牌
							$condition = [
								[ 'brand_id', 'in', $v[ 'value' ][ 'brand_ids' ] ]
							];
							$list = $goods_brand_model->getBrandList($condition, 'brand_id, brand_name, brand_initial,image_url', 'sort desc,create_time desc');
							$list = $list[ 'data' ];
							$value[ $k ][ 'value' ][ 'list' ] = $list;
						} elseif ($v[ 'type' ] == 'category') {
							// 商品分类
							$condition = [
								[ 'category_id', 'in', $v[ 'value' ][ 'category_ids' ] ]
							];
							$list = $goods_category_model->getCategoryList($condition, 'category_id,category_name,short_name,image,image_adv');
							$list = $list[ 'data' ];
							$value[ $k ][ 'value' ][ 'list' ] = $list;
						}
					}
				}
				$floor_info[ 'value' ] = json_encode($value);
			}
		}
		return $floor_info;
	}

	/**
	 * 获取普通商品列表
	 */
	protected function getCommonGoodsList($param)
	{
		$goods_model = new GoodsModel();
		$alias = 'gs';
        //只查看处于开启状态的店铺
        $join = [
            [ 'shop s', 's.site_id = gs.site_id', 'inner']
        ];
        $condition = [];
        $condition[] = [ 'gs.goods_state', '=', 1 ];
        $condition[] = [ 'gs.verify_state', '=', 1 ];
        $condition[] = [ 'gs.is_delete', '=', 0 ];
        $condition[] = [ 's.shop_status', '=', 1];

        $limit = $param['limit'] ?? null;
        if($param['goods_source'] == 'category'){
        	$condition[] = [ 'gs.category_id_1|gs.category_id_2|gs.category_id_3', '=', $param['category_id'] ?? 0];
        }else if($param['goods_source'] == 'diy'){
        	$condition[] = [ 'gs.sku_id', 'in', $param['sku_ids'] ?? ''];
        }
		$field = 'gs.sku_id,gs.sku_name,gs.price,gs.market_price,gs.discount_price,gs.stock,gs.sale_num,gs.sku_image,gs.sku_image as goods_image,gs.goods_name,gs.introduction';
		$order = 'gs.sort desc,gs.create_time desc';
		$list = $goods_model->getGoodsSkuList($condition, $field, $order, null, $alias, $join);
		return $list['data'];
	}

	/**
	 * 获取专题活动商品列表
	 */
	protected function getTopicGoodsList($param)
	{
		$alias = 'tpg';
		$join = [
            [ 'shop s', 's.site_id = tpg.site_id', 'inner'],
            ['goods_sku gs', 'gs.sku_id = tpg.sku_id', 'inner'],
        ];
        $condition = [];
        $condition[] = [ 'gs.goods_state', '=', 1 ];
        $condition[] = [ 'gs.verify_state', '=', 1 ];
        $condition[] = [ 'gs.is_delete', '=', 0 ];
        $condition[] = [ 's.shop_status', '=', 1];

        $limit = $param['limit'] ?? null;
        if($param['goods_source'] == 'category'){
        	$condition[] = [ 'gs.category_id_1|gs.category_id_2|gs.category_id_3', '=', $param['category_id'] ?? 0];
        }else if($param['goods_source'] == 'diy'){
        	$condition[] = [ 'gs.sku_id', 'in', $param['sku_ids'] ?? ''];
        }

        $field = 'tpg.id, tpg.topic_price as price,tpg.topic_price as discount_price, gs.goods_id,gs.sku_name as goods_name,gs.sku_image, gs.sku_image as goods_image,gs.create_time,gs.sku_id,gs.introduction,gs.price as market_price';
		$order = 'gs.sort desc,gs.create_time desc';
		$list = model('promotion_topic_goods')->getList($condition, $field, $order, $alias, $join, '', $limit);
		return $list;
	}

	/**
	 * 获取PC端首页楼层列表
	 * @param array $condition
	 * @param string $field
	 * @param string $order
	 * @return array
	 */
	public function getFloorList($condition = [], $field = 'pf.id,pf.block_id,pf.title,pf.value,pf.state,pf.create_time,pf.sort,fb.name as block_name,fb.title as block_title', $order = 'pf.sort desc,pf.create_time desc')
	{
		$alias = 'pf';
		$join = [
			[ 'pc_floor_block fb', 'pf.block_id = fb.id', 'inner' ]
		];

		$list = model('pc_floor')->getList($condition, $field, $order, $alias, $join);
		return $this->success($list);
	}

	/**
	 * 获取PC端首页楼层分页列表
	 * @param array $condition
	 * @param int $page
	 * @param int $page_size
	 * @param string $order
	 * @param string $field
	 * @return array
	 */
	public function getFloorPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'pf.create_time desc', $field = 'pf.id,pf.block_id,pf.title,pf.value,pf.state,pf.create_time,pf.sort,fb.name as block_name,fb.title as block_title')
	{
		$alias = 'pf';
		$join = [
			[ 'pc_floor_block fb', 'pf.block_id = fb.id', 'inner' ]
		];

		$list = model('pc_floor')->pageList($condition, $field, $order, $page, $page_size, $alias, $join);
		return $this->success($list);
	}

	/************************************** PC端自定义页面 ******************************************/

    /**
     * 添加自定义页面
     * @param $data
     * @return array
     */
    public function addPcSiteDiyView($param)
    {
        //添加页面
        $data = [
            'site_id' => $param['site_id'],
            'title' => $param['title'],
            'name' => 'PC_SITE_DIY_VIEW_' . strtoupper(uniqid()),
            'create_time' => time(),
        ];
        model('pc_site_diy_view')->add($data);

        //添加广告位
        $adv_position_data = [
            'site_id' => $param['site_id'],
            'ap_name' => "自定义页面【{$param['title']}】广告位",
            'ap_intro' => "自定义页面【{$param['title']}】广告位",
            'ap_height' => 400,
            'ap_width' => 1920,
            'default_content' => '',
            'ap_background_color' => '#FFFFFF',
            'type' => 1,//电脑端
            'keyword' => $data['name'],
        ];

        $adv_position_model = new AdvPosition();
        $adv_position_model->addAdvPosition($adv_position_data);

        return $this->success();
    }

    /**
     * 编辑自定义页面
     * @param $data
     * @param $condition
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function editPcSiteDiyView($data, $name)
    {
        model('pc_site_diy_view')->update($data, [['name', '=', $name]]);

        $adv_position_model = new AdvPosition();
        $adv_position_model->editAdvPosition([
            'ap_name' => "自定义页面【{$data['title']}】广告位",
            'ap_intro' => "自定义页面【{$data['title']}】广告位",
        ], [['keyword', '=', $name]]);

        return $this->success();
    }

    /**
     * 删除自定义页面
     * @param $condition
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function deletePcSiteDiyView($name)
    {
        $ap_info = model('adv_position')->getInfo([['keyword', '=', $name]]);
        if(!empty($ap_info)){
            //删除广告
            $adv_model = new Adv();
            $adv_model->deleteAdv([['ap_id', '=', $ap_info['ap_id']]]);
            //删除广告位
            $adv_position_model = new AdvPosition();
            $adv_position_model->deleteAdvPosition([['keyword', '=', $name]]);
        }
        //删除楼层
        model('pc_floor')->delete([['view_name', '=', $name]]);
        //删除页面
        model('pc_site_diy_view')->delete([['name', '=', $name]]);

        return $this->success();
    }

    /**
     * 获取自定义页面信息
     * @param $condition
     * @param $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getPcSiteDiyViewInfo($condition, $field = '*')
    {
        $info = model('pc_site_diy_view')->getInfo($condition, $field);
        return $this->success($info);
    }

    /**
     * 获取自定义页面列表
     * @param $condition
     * @param $field
     * @param $order
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getPcSiteDiyViewList($condition, $field, $order)
    {
        $list = model('pc_site_diy_view')->getList($condition, $field, $order);
        return $this->success($list);
    }

    /**
     * 获取自定义页面分页列表
     * @param array $condition
     * @param bool $field
     * @param string $order
     * @param int $page
     * @param int $list_rows
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getPcSiteDiyViewPageList($condition = [], $field = true, $order = '', $page = 1, $list_rows = PAGE_LIST_ROWS)
    {
        $res = model('pc_site_diy_view')->pageList($condition, $field, $order, $page, $list_rows);
        return $this->success($res);
    }
}