<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\shop;

use app\model\BaseModel;
use app\model\system\Stat;
use function PHPSTORM_META\type;

/**
 * 店铺浏览记录
 */
class ShopBrowse extends BaseModel
{
    /**
     * 添加店铺浏览记录
     * @param array $data
     */
    public function addBrowse($data)
    {
        //店铺统计
        $stat = new Stat();

        $start_time = strtotime(date('Y-m-d'));
        $end_time = strtotime(date('Y-m-d 23:59:59'));
        $data['type'] = 2;

        if ($data['member_id'] > 0) {
            $res = model('shop_browse')->getInfo([ [ 'site_id', '=', $data['site_id'] ], [ 'member_id', '=', $data['member_id'] ], [ 'type', '=', 1 ], [ 'create_time', '>', $start_time ], [ 'create_time', '<', $end_time ] ], 'site_id,id');
        } else {
            $res = model('shop_browse')->getInfo([ [ 'site_id', '=', $data['site_id'] ], [ 'consumer_ip', '=', $data['consumer_ip'] ], [ 'type', '=', 1 ], [ 'create_time', '>', $start_time ], [ 'create_time', '<', $end_time ] ], 'site_id,id');
        }

        if (empty($res)) {
            $data['type'] = 1;
            $browse_id = model('shop_browse')->add($data);
            $stat->addShopStat(['shop_uv' => 1, 'site_id' => $data['site_id']]);
        }

        $browse_id = model('shop_browse')->add($data);
        $stat->addShopStat(['shop_pv' => 1, 'site_id' => $data['site_id']]);

        return $this->success($browse_id);
    }


}