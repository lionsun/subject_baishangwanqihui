<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\shop;


use app\model\system\Config as ConfigModel;
use app\model\system\Document as DocumentModel;
use app\model\BaseModel;

/**
 * 店铺商品分类
 */
class ShopGoodsCategory extends BaseModel
{
    /**
     * 获取商家商品分类列表
     * @param $condition
     * @param string $field
     * @param string $order
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getShopGoodsCategoryList($condition, $field = '*', $order = '')
    {
        $list = model('shop_goods_category')->getList($condition, $field, $order);
        return $this->success($list);
    }

    /**
     * 更新商家商品分类佣金比率
     * 修改上级会把下级的也都改掉
     * @param $site_id
     * @param $category_id
     * @param $commission_rate
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function updateShopGoodsCategoryCommission($site_id, $category_id, $commission_rate)
    {
        $category_info = model('goods_category')->getInfo([['category_id', '=', $category_id]]);
        $level = $category_info['level'];

        $ids = $this->getShopGoodsCategoryIds($site_id);
        $id_arr = array_unique(array_merge($ids['extend'], $ids['self']));
        $condition = [
            ['level', '>=', $level],
            ["category_id_{$level}", '=', $category_id],
            ['category_id', 'in', $id_arr],
        ];

        $update_id_arr = model('goods_category')->getColumn($condition, 'category_id');
        model('shop_goods_category')->delete([['category_id', 'in', $update_id_arr]]);
        $add_data = [];
        foreach($update_id_arr as $id){
            $add_data[] = [
                'site_id' => $site_id,
                'category_id' => $id,
                'commission_rate' => $commission_rate,
            ];
        }
        model('shop_goods_category')->addList($add_data);
        return $this->success();
    }

    /**
     * 获取商家分类信息
     * @param $condition
     * @param string $field
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getShopGoodsCategoryInfo($condition, $field = '*')
    {
        $info = model('shop_goods_category')->getInfo($condition, $field);
        return $this->success($info);
    }

    /**
     * 获取商家商品分类 包括主营行业规定的和自己店铺单独的
     * @param $site_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getShopGoodsCategoryIds($site_id)
    {
        $extend = [];
        $self = [];
        $shop_info = model('shop')->getInfo([['site_id', '=', $site_id]], 'category_id, self_goods_category_ids');
        if(!empty($shop_info)){
            $self = explode(',', $shop_info['self_goods_category_ids']);
            $shop_category_info = model('shop_category')->getInfo([['category_id', '=', $shop_info['category_id']]], 'goods_category_ids');
            if(!empty($shop_category_info) && !empty($shop_category_info['goods_category_ids'])){
                $extend = explode(',', $shop_category_info['goods_category_ids']);
            }
        }
        return [
            'extend' => $extend,
            'self' => $self,
        ];
    }

    /**
     * 更新店铺自己的商品分类
     * @param $site_id
     * @param $selected_id_arr
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function updateShopSelfGoodsCategory($site_id, $selected_ids)
    {
        $selected_id_arr = explode(',', $selected_ids);

        $id_arr = $this->getShopGoodsCategoryIds($site_id);
        $extend_id_arr = $id_arr['extend'];
        $old_self_id_arr = $id_arr['self'];
        //去掉继承的 剩下的是新的自己的
        foreach($selected_id_arr as $key=>$id){
            if(in_array($id, $extend_id_arr)){
                unset($selected_id_arr[$key]);
            }
        }
        $new_self_id_arr = $selected_id_arr;
        //去掉原来自己就有的，剩下的是需要删除的
        foreach($selected_id_arr as $selected_key=>$id){
            $self_key = array_search($id, $old_self_id_arr);
            if($self_key !== false){
                unset($old_self_id_arr[$self_key]);
            }
        }
        $delete_id_arr = $old_self_id_arr;

        model('shop')->update(['self_goods_category_ids' => join(',', $new_self_id_arr)], [['site_id', '=', $site_id]]);
        if(!empty($old_self_id_arr)){
            model('shop_goods_category')->delete([['site_id', '=', $site_id], ['category_id', 'in', $delete_id_arr]]);
        }
        return $this->success();
    }


}