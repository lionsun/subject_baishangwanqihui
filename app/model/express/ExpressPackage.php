<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\express;

use addon\vop\model\Vop;
use app\model\BaseModel;

/**
 * 物流配送
 */
class ExpressPackage extends BaseModel
{

    /**
     * 获取物流包裹列表
     * @param $condition
     * @param string $field
     */
    public function getExpressDeliveryPackageList($condition, $field = "*"){
        $list = model("express_delivery_package")->getList($condition, $field);
        return $this->success($list);
    }

    /**
     * 获取包裹信息
     * @param $condition
     */
    public function package($condition, $mobile = ''){
        $list_result = $this->getExpressDeliveryPackageList($condition);
        $list = $list_result["data"];

        $source = $vop_order_no = 0;
        if (!empty($list)) {
            $order_id =$list[0]['order_id'];
            $order_info = model('order')->getInfo(['order_id' => $order_id],'source,vop_order_no');
            $source = $order_info['source'];
        }


        $trace_model = new Trace();
        foreach($list as $k => $v){
            $temp_array = explode(",", $v["goods_id_array"]);
            if(!empty($temp_array)){
                foreach($temp_array as $temp_k => $temp_v){
                    $temp_str = str_replace("http://", "http//", $temp_v);
                    $temp_str = str_replace("https://", "https//", $temp_str);
                    $temp_item = explode(":", $temp_str);
                    $sku_image = str_replace("https//", "https://", $temp_item["3"]);
                    $sku_image = str_replace("http//", "http://", $sku_image);
                    $list[$k]["goods_list"][] = ["sku_name" => $temp_item["2"], "num" => $temp_item["1"], "sku_image" => $sku_image, "sku_id" => $temp_item["0"]];
                }
            }

            if ($source == 2) {

//        $result = array(
//            "success" => true,//成功与否
//            "reason" => '',//错误原因
//            "status" => '1',//物流状态:0-无轨迹,1-已揽收, 2-在途中 201-到达派件城市，3-签收,4-问题件
//            "status_name" => '已揽收',//物流状态名称:0-无轨迹,1-已揽收, 2-在途中 201-到达派件城市，3-签收,4-问题件,
//            "shipper_code" => 'SH',//快递公司编码
//            "logistic_code" => $code,//物流运单号
//            "list" => array(
//                [
//                    "datetime" => "2015-03-08 01:15:00",
//                    "remark" => "离开广州市 发往北京市（经转）",
//                ]
//            )
//        );
                $vop_model = new Vop();
                $vop_return = $vop_model->getOrderTrack($vop_order_no);
                if ($vop_return['code'] > 0) {
                    if (empty($vop_return['data']['timeline'])) {
                        $list[$k]["trace"] = ["success" => false, "reason" => '抱歉，暂无物流记录'];
                    }else{
                        $list = [];
                        foreach ($vop_return['data']['timeline'] as $key => $value){
                            $list[$key]['datetime'] =  $value['time'];
                            $list[$key]['remark'] =  $value['remark'];
                        }
                        $list[$k]["trace"] = [
                            'success' => true,
                            'reason' => '',
                            'status' => 2,
                            'status_name' => '',
                            'shipper_code' =>'',
                            'logistic_code' => $vop_return['data']['trackId'],

                            'transport' => $vop_return['data']['transport'], //唯品物流公司
                            'transport_id' => $vop_return['data']['transportId'], //唯品物流公司ID
                            'list' => $list,
                        ];
                    }
                } else {
                    $list[$k]["trace"] = ["success" => false, "reason" => $vop_return['message']];
                }

            } else {
                $trace_list = $trace_model->trace($v["delivery_no"],$v["express_company_id"], $mobile);
                $list[$k]["trace"] = $trace_list["data"];
            }
        }
        return $list;

    }

}