<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\system;

use app\model\BaseModel;

/**
 * 页面留存时间
 * @author wangjx
 *
 */
class PageRetention extends BaseModel
{
    /**
     * 获取页面留存时间
     * @param int $sku_id
     * @param int $start_time
     * @param int $end_time
     */
    public function getRetentionTime($sku_id, $start_time, $end_time){
        $condition[] = ['page_id', '=', $sku_id];
        $condition[] = ['create_time', '>', $start_time];
        $condition[] = ['create_time', '<', $end_time];
        $res = model('page_retention')->getSum($condition, 'time');
        return $res;
    }

    /**
     * 获取页面留存时间
     * @param array $data
     */
    public function addRetention($data){
        $res = model('page_retention')->add($data);
        return $this->success($res);
    }
}