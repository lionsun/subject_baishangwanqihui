<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\system;

use app\model\BaseModel;
use think\facade\Db;

/**
 * 页面点击地图
 * @author wangjx
 *
 */
class PageClickmap extends BaseModel
{
    /**
     * 获取页面点击地图列表
     * @param array $condition
     * @param int $page
     * @param int $page_size
     * @param string $order
     * @param string $field
     */
    public function getClickmapList($condition, $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'update_time desc,create_time desc', $field = '*'){
        $res = model('page_clickmap')->pageList($condition, $field, $order, $page, $page_size, '', '', '');
        return $this->success($res);
    }

    /**
     * 获取页面点击地图
     * @param int $id
     * @return multitype:string mixed
     */
    public function getClickmapInfo($id)
    {
        $res = model('page_clickmap')->getInfo([ [ 'id', '=', $id ] ], '*');
        return $this->success($res);
    }

    /**
     * 添加点击地图
     * @param array $data
     */
    public function addClickmap($data){
        try {
            $condition[] = ['page_name', '=', $data['page_name']];
            $condition[] = ['location', '=', $data['location']];
            $condition[] = ['type', '=', 0];

            $info = model('page_clickmap')->getInfo($condition);

            if($info){
                return $this->error("", "添加失败，请勿添加重复数据！");
            }

            $res = model('page_clickmap')->add($data);
            return $this->success($res);
        } catch ( \Exception $e ) {
            return $this->error("", $e->getMessage());
        }

    }

    /**
     * 修改点击地图
     * @param array $data
     */
    public function editClickmap($data){
        try {
            $condition[] = ['page_name', '=', $data['page_name']];
            $condition[] = ['location', '=', $data['location']];
            $condition[] = ['type', '=', 0];

            $info = model('page_clickmap')->getInfo($condition);

            if($info){
                return $this->error("", "编辑失败，请勿添加重复数据！");
            }

            $res = model('page_clickmap')->update($data, ['id', '=', $data['id']]);
            return $this->success($res);
        } catch ( \Exception $e ) {
            return $this->error("", $e->getMessage());
        }

    }

    /**
     * 获取页面点击地图
     * @param array $url
     * @param array $start_time
     * @param array $end_time
     */
    public function getClickmapByUrl($url, $start_time, $end_time){
        $condition[] = ['location', '=', $url];
        if (!empty($start_time) && empty($end_time)) {
            $condition[] = ["create_time", ">=", $start_time];
        } elseif (empty($start_time) && !empty($end_time)) {
            $condition[] = ["create_time", "<=", $end_time];
        } elseif (!empty($start_time) && !empty($end_time)) {
            $condition[] = [ 'create_time', 'between', [ $start_time, $end_time ] ];
        }

        $res = model('page_clickmap')->getList($condition, 'x as xxx, y as yyy, count(id) as click_sum', 'click_sum DESC', '', '', 'CONCAT(x,y)', '');
        return $this->success($res);
    }
}