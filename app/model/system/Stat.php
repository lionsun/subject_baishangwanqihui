<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\system;

use app\model\BaseModel;
use Carbon\Carbon;
use think\facade\Db;

/**
 * 统计
 * @author Administrator
 *
 */
class Stat extends BaseModel
{
    /**
     * 添加店铺统计(按照天统计)
     * @param array $data
     */
    public function addShopStat($data)
    {
        $carbon = Carbon::now();
        $condition = [
            'site_id' => $data[ 'site_id' ],
            'year' => $carbon->year,
            'month' => $carbon->month,
            'day' => $carbon->day
        ];
        $info = model("stat_shop")->getInfo($condition, 'id');
        if (empty($info)) {
            $stat_data = [
                'site_id' => $data[ 'site_id' ],
                'year' => $carbon->year,
                'month' => $carbon->month,
                'day' => $carbon->day,
                'day_time' => Carbon::today()->timestamp,
                'create_time' => time()
            ];
            if (isset($data['order_total'])) {
                $stat_data['order_total'] = $data['order_total'];
            }
            if (isset($data['shipping_total'])) {
                $stat_data['shipping_total'] = $data['shipping_total'];
            }
            if (isset($data['refund_total'])) {
                $stat_data['refund_total'] = $data['refund_total'];
            }
            if (isset($data['order_pay_count'])) {
                $stat_data['order_pay_count'] = $data['order_pay_count'];
            }
            if (isset($data['goods_pay_count'])) {
                $stat_data['goods_pay_count'] = $data['goods_pay_count'];
            }
            if (isset($data['shop_money'])) {
                $stat_data['shop_money'] = $data['shop_money'];
            }
            if (isset($data['platform_money'])) {
                $stat_data['platform_money'] = $data['platform_money'];
            }
            if (isset($data['collect_shop'])) {
                $stat_data['collect_shop'] = $data['collect_shop'];
            }
            if (isset($data['collect_goods'])) {
                $stat_data['collect_goods'] = $data['collect_goods'];
            }
            if (isset($data['visit_count'])) {
                $stat_data['visit_count'] = $data['visit_count'];
            }
            if (isset($data['order_count'])) {
                $stat_data['order_count'] = $data['order_count'];
            }
            if (isset($data['goods_count'])) {
                $stat_data['goods_count'] = $data['goods_count'];
            }
            if (isset($data['add_goods_count'])) {
                $stat_data['add_goods_count'] = $data['add_goods_count'];
            }
            if (isset($data['member_count'])) {
                $stat_data['member_count'] = $data['member_count'];
            }
            if (isset($data['shop_uv'])) {
                $stat_data['shop_uv_count'] = $data['shop_uv'];
            }
            if (isset($data['shop_pv'])) {
                $stat_data['shop_pv_count'] = $data['shop_pv'];
            }

            $res = model("stat_shop")->add($stat_data);
        } else {
            $stat_data = ['modify_time' => time()];

            if (isset($data['order_total'])) {
                $stat_data['order_total'] = Db::raw('order_total+' . $data['order_total']);
            }
            if (isset($data['shipping_total'])) {
                $stat_data['shipping_total'] = Db::raw('shipping_total+' . $data['shipping_total']);
            }
            if (isset($data['refund_total'])) {
                $stat_data['refund_total'] = Db::raw('refund_total+' . $data['refund_total']);
            }
            if (isset($data['order_pay_count'])) {
                $stat_data['order_pay_count'] = Db::raw('order_pay_count+' . $data['order_pay_count']);
            }
            if (isset($data['goods_pay_count'])) {
                $stat_data['goods_pay_count'] = Db::raw('goods_pay_count+' . $data['goods_pay_count']);
            }
            if (isset($data['shop_money'])) {
                $stat_data['shop_money'] = Db::raw('shop_money+' . $data['shop_money']);
            }
            if (isset($data['platform_money'])) {
                $stat_data['platform_money'] = Db::raw('platform_money+' . $data['platform_money']);
            }
            if (isset($data['collect_shop'])) {
                $stat_data['collect_shop'] = Db::raw('collect_shop+' . $data['collect_shop']);
            }
            if (isset($data['collect_goods'])) {
                $stat_data['collect_goods'] = Db::raw('collect_goods+' . $data['collect_goods']);
            }
            if (isset($data['visit_count'])) {
                $stat_data['visit_count'] = Db::raw('visit_count+' . $data['visit_count']);
            }
            if (isset($data['order_count'])) {
                $stat_data['order_count'] = Db::raw('order_count+' . $data['order_count']);
            }
            if (isset($data['goods_count'])) {
                $stat_data['goods_count'] = Db::raw('goods_count+' . $data['goods_count']);
            }
            if (isset($data['add_goods_count'])) {
                $stat_data['add_goods_count'] = Db::raw('add_goods_count+' . $data['add_goods_count']);
            }
            if (isset($data['member_count'])) {
                $stat_data['member_count'] = Db::raw('member_count+' . $data['member_count']);
            }
            if (isset($data['shop_uv'])) {
                $stat_data['shop_uv_count'] = Db::raw('shop_uv_count+' . $data['shop_uv']);
            }
            if (isset($data['shop_pv'])) {
                $stat_data['shop_pv_count'] = Db::raw('shop_pv_count+' . $data['shop_pv']);
            }

            $res = Db::name('stat_shop')->where($condition)->update($stat_data);
        }
        if ($data[ 'site_id' ] != 0) {
            $data[ 'site_id' ] = 0;
            $this->addShopStat($data);
        }
        return $this->success($res);

    }

    /**
     * 获取店铺统计（按照天查询）
     * @param unknown $site_id 0表示平台
     * @param unknown $year
     * @param unknown $month
     * @param unknown $day
     */
    public function getStatShop($site_id, $year, $month, $day)
    {
        $condition = [
            'site_id' => $site_id,
            'year' => $year,
            'month' => $month,
            'day' => $day
        ];
        $info = model("stat_shop")->getInfo($condition,
            'id, site_id, year, month, day, order_total, shipping_total, refund_total, order_pay_count, goods_pay_count, shop_money, platform_money, create_time, modify_time, collect_shop, collect_goods, visit_count, order_count, goods_count, add_goods_count, day_time, member_count');

        if (empty($info)) {
            $condition[ 'day_time' ] = Carbon::today()->timestamp;
            model("stat_shop")->add($condition);
            $info = model("stat_shop")->getInfo($condition,
                'id, site_id, year, month, day, order_total, shipping_total, refund_total, order_pay_count, goods_pay_count, shop_money, platform_money, create_time, modify_time, collect_shop, collect_goods, visit_count, order_count, goods_count, add_goods_count, day_time, member_count');

        }
        return $this->success($info);
    }


    /**
     * 获取店铺统计信息
     * @param unknown $site_id
     * @param unknown $start_time
     */
    public function getShopStatSum($site_id, $start_time = 0)
    {
        $condition = [
            ['site_id', '=', $site_id]
        ];
        if (!empty($start_time)) {
            $condition[] = ['day_time', '>=', $start_time];
        }
        $info = model("stat_shop")->getInfo($condition,
            'SUM(order_total) as order_total,SUM(shipping_total) as shipping_total,SUM(refund_total) as refund_total,SUM(order_pay_count) as order_pay_count,SUM(goods_pay_count) as goods_pay_count,SUM(shop_money) as shop_money,SUM(platform_money) as platform_money,SUM(collect_shop) as collect_shop,SUM(collect_goods) as collect_goods,SUM(visit_count) as visit_count,SUM(order_count) as order_count,SUM(goods_count) as goods_count,SUM(add_goods_count) as add_goods_count,SUM(member_count) as member_count');
        if ($info[ 'order_total' ] == null) {
            $info = [
                "order_total" => 0,
                "shipping_total" => 0,
                "refund_total" => 0,
                "order_pay_count" => 0,
                "goods_pay_count" => 0,
                "shop_money" => 0,
                "platform_money" => 0,
                "collect_shop" => 0,
                "collect_goods" => 0,
                "visit_count" => 0,
                "order_count" => 0,
                "goods_count" => 0,
                "add_goods_count" => 0,
                "member_count" => 0
            ];
        }
        return $this->success($info);
    }

    /**
     * 获取店铺统计列表
     * @param unknown $site_id
     * @param unknown $start_time
     */
    public function getShopStatList($site_id, $start_time)
    {
        $condition = [
            ['site_id', '=', $site_id],
            ['day_time', '>=', $start_time],
        ];
        $info = model("stat_shop")->getList($condition,
            'id, site_id, year, month, day, order_total, shipping_total, refund_total, order_pay_count, goods_pay_count, shop_money, platform_money, create_time, modify_time, collect_shop, collect_goods, visit_count, order_count, goods_count, add_goods_count, day_time, member_count');

        return $this->success($info);
    }

    /**
     * 获取导出店铺统计信息
     * @param unknown $site_id
     * @param unknown $start_time
     */
    public function getShopsStatSum($site_id, $start_time = 0)
    {
        $condition = [
            ['stat.site_id', '=', $site_id]
        ];
        if (!empty($start_time)) {
            $condition[] = ['stat.day_time', '>=', $start_time];
        }
        $join = [
            [
                'shop s',
                'stat.site_id = s.site_id',
                'inner'
            ]
        ];
        $info = model("stat_shop")->getInfo($condition, 's.site_id,s.site_name,"" as site_link,SUM(shop_uv_count) as shop_uv_count,SUM(shop_pv_count) as shop_pv_count,SUM(order_total) as order_total,SUM(order_pay_count) as order_pay_count,"" as conversion','stat', $join);

        if (!$info) {
            $info = [
                "site_id" => '',
                "site_name" => '',
                "site_link" => '',
                "shop_uv_count" => 0,
                "shop_pv_count" => 0,
                "order_total" => 0,
                "order_pay_count" => 0,
                "time_range" => '',
                "conversion" => ''
            ];
        }
        return $this->success($info);
    }

    /**
     * 获取导出店铺交易信息
     * @param array $condition
     * @return array
     */
    public function getShopsOrderList($condition = []){
        $join = [
            [
                'order o',
                'stat.site_id = o.site_id',
                'right'
            ],
            [
                'order_goods og',
                'o.order_id = og.order_id',
                'left'
            ]
        ];

        $info = model("stat_shop")->getList($condition, 'o.order_name, og.sku_id as sku_id, og.goods_id as goods_link, o.goods_money, CASE WHEN o.balance_money > 0 THEN o.balance_money WHEN o.pay_money > 0 THEN o.pay_money END AS pay_money, goods_num, FROM_UNIXTIME(o.finish_time) as finish_time', 'finish_time', 'stat', $join, 'o.order_id', '');

        return $this->success($info);
    }

    /**
     * 获取导出所有店铺统计信息
     * @param unknown $start_time
     */
    public function getAllShopsStatSum($start_time = 0)
    {
        if (!empty($start_time)) {
            $condition[] = ['stat.day_time', '>=', $start_time];
        }
        $join = [
            [
                'shop s',
                'stat.site_id = s.site_id',
                'right'
            ]
        ];
        $info = model("stat_shop")->getList($condition, 's.site_id,s.site_name,"" as site_link,SUM(shop_uv_count) as shop_uv_count,SUM(shop_pv_count) as shop_pv_count,SUM(order_total) as order_total,SUM(order_pay_count) as order_pay_count,"" as conversion, "" as time_range', '','stat', $join, 's.site_id');

        return $this->success($info);
    }

    /**
     * 获取导出所有店铺交易信息
     * @param array $condition
     * @param string $site_ids
     * @return array
     */
    public function getAllShopsOrderList($condition = [], $site_ids){
        $join = [
            [
                'order o',
                'stat.site_id = o.site_id',
                'right'
            ],
            [
                'order_goods og',
                'o.order_id = og.order_id',
                'left'
            ]
        ];
        $info = [];
        foreach($site_ids as $k=>$v){
            $condition[] = ['o.site_id', '=', $v];
            $res = model("stat_shop")->getList($condition, 'o.site_id, o.site_name, o.order_name, og.sku_id as sku_id, og.goods_id as goods_link, o.goods_money, CASE WHEN o.balance_money > 0 THEN o.balance_money WHEN o.pay_money > 0 THEN o.pay_money END AS pay_money, goods_num, FROM_UNIXTIME(o.finish_time) as finish_time', 'finish_time', 'stat', $join, 'o.order_id', '');
            if(empty($res)){
                $info[$v] = array();
            }else{
                $info[$v] = $res;
            }
            array_pop($condition);
        }

        return $this->success($info);
    }

    /**
     * 获取会员统计信息
     */
    public function getMemberStatSum(){
        $info = model('Member')->getList('', "SUM(CASE WHEN reg_time > UNIX_TIMESTAMP(FROM_UNIXTIME(UNIX_TIMESTAMP(NOW()),'%Y-%m-%d')) THEN 1 ELSE 0 END) AS new_add, SUM(CASE WHEN to_days(now()) - to_days(FROM_UNIXTIME(login_time,'%Y-%m-%d %H:%i:%s')) < 1 THEN 1 ELSE 0 END) AS dau, SUM(CASE WHEN YEARWEEK(FROM_UNIXTIME(login_time,'%Y-%m-%d')) = YEARWEEK(now()) THEN 1 ELSE 0 END) AS wau, SUM(CASE WHEN FROM_UNIXTIME(login_time,'%y%m') = date_format(curdate(),'%y%m') THEN 1 ELSE 0 END) AS mau, COUNT(member_id) AS total", '', '', '');

        return $this->success($info);
    }

    /**
     * 获取会员统计报表信息
     * @param unknown $start_time
     */
    public function getMemberStatList($start_time){
        $condition[] = ['login_time', '>', $start_time];

        $info = model('Member')->getList($condition, "UNIX_TIMESTAMP(FROM_UNIXTIME(login_time,'%Y-%m-%d')) AS date, FROM_UNIXTIME(login_time,'%Y-%m-%d') AS time, count(member_id) as total, SUM(CASE WHEN login_type = 'h5' THEN 1 ELSE 0 END) as H5, SUM(CASE WHEN login_type = 'app' THEN 1 ELSE 0 END) as App, SUM(CASE WHEN login_type = 'pc' THEN 1 ELSE 0 END) as Pc, SUM(CASE WHEN login_type = 'applets' THEN 1 ELSE 0 END) as Applets", '', '', '', "FROM_UNIXTIME(login_time, '%Y-%m-%d')");

        return $this->success($info);
    }

    /**
     * 获取新增会员统计报表信息
     * @param unknown $start_time
     */
    public function getNewMemberStatList($start_time){
        $condition[] = ['reg_time', '>', $start_time];

        $info = model('Member')->getList($condition, "UNIX_TIMESTAMP(FROM_UNIXTIME(reg_time,'%Y-%m-%d')) AS date, FROM_UNIXTIME(reg_time,'%Y-%m-%d') AS time, COUNT(member_id) AS total, SUM(CASE WHEN login_time - reg_time > 86400 THEN 1 ELSE 0 END) AS oned_keep, SUM(CASE WHEN login_time - reg_time > 259200 THEN 1 ELSE 0 END) AS threed_keep, SUM(CASE WHEN login_time - reg_time > 604800 THEN 1 ELSE 0 END) AS week_keep, SUM(CASE WHEN login_time - reg_time > 2592000 THEN 1 ELSE 0 END) AS month_keep", '', '', '', "FROM_UNIXTIME(reg_time, '%Y-%m-%d')");

        return $this->success($info);
    }
}