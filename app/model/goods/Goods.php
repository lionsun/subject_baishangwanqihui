<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\goods;

use addon\discount\model\Discount;
use app\model\BaseModel;
use app\model\order\OrderCommon;
use app\model\order\OrderRefund;
use app\model\system\Config as ConfigModel;
use app\model\web\WebSite as WebsiteModel;
use app\model\system\Stat;
use think\facade\Log;
use think\facade\Db;

/**
 * 商品
 */
class Goods extends BaseModel
{

    private $goods_class = array('id' => 1, 'name' => '实物商品');

    private $goods_state = array(
        1 => '正常',
        0 => '下架'
    );

    private $verify_state = array(
        1 => '已审核',
        0 => '待审核',
        -2 => '审核失败',
        10 => '违规下架'
    );

    public function getGoodsState()
    {
        return $this->goods_state;
    }

    public function getVerifyState()
    {
        return $this->verify_state;
    }

    /**
     * 商品来源
     */
    const SOURCE_TYPE_PLATFORM = 0;
    const SOURCE_TYPE_HOUNIAO = 1;
    const SOURCE_TYPE_WEIPIN = 2;
    const SOURCE_TYPE_ZHONGMIN = 3;

    public static function getSourceType()
    {
        $arr = [
            ['id' => self::SOURCE_TYPE_PLATFORM, 'name' => '平台'],
            ['id' => self::SOURCE_TYPE_HOUNIAO, 'name' => '候鸟'],
            ['id' => self::SOURCE_TYPE_WEIPIN, 'name' => '唯品'],
            ['id' => self::SOURCE_TYPE_ZHONGMIN, 'name' => '中闽在线'],
        ];
        return $arr;
    }

    public static function getSourceTypeName($id)
    {
        $arr = self::getSourceType();
        $name = '';
        foreach($arr as $val){
            if($val['id'] == $id){
                $name = $val['name'];
                break;
            }
        }
        return $name;
    }

    /**
     * 商品添加
     * @param $data
     */
    public function addGoods($data)
    {
        model('goods')->startTrans();
        try {

            //店铺信息
            $shop_info = model('shop')->getInfo([['site_id', '=', $data[ 'site_id' ]]], 'site_name, website_id, is_own,cert_id,shop_status');

            $goods_config = new Config();
            $goods_verify_config = $goods_config->getVerifyConfig();
            $goods_verify_config = $goods_verify_config[ 'data' ][ 'value' ];
            $verify_state = 1;
            if (!empty($goods_verify_config[ 'is_open' ])) {
                $verify_state = 0;//开启商品审核后，审核状态为：待审核
            }

            // 店铺未认证、审核中的状态下，商品需要审核
            if (empty($shop_info[ 'cert_id' ]) || $shop_info[ 'shop_status' ] == 0 || $shop_info[ 'shop_status' ] == 2) {
                $verify_state = 0;//开启商品审核后，审核状态为：待审核
            }

            $goods_image = $data[ 'goods_image' ];

            //SKU商品数据
            if (!empty($data[ 'goods_sku_data' ])) {
                $data[ 'goods_sku_data' ] = json_decode($data[ 'goods_sku_data' ], true);
                if (empty($goods_image)) {
                    $goods_image = $data[ 'goods_sku_data' ][ 0 ][ 'sku_image' ];
                }
            }
            $goods_data = array(
                'goods_image' => $goods_image,
                'goods_stock' => $data[ 'goods_stock' ],
                'price' => !empty($data[ 'goods_sku_data' ])?$data[ 'goods_sku_data' ][ 0 ][ 'price' ] : '0',
                'market_price' => !empty($data[ 'goods_sku_data' ])?$data[ 'goods_sku_data' ][ 0 ][ 'market_price' ]:'0',
                'cost_price' => !empty($data[ 'goods_sku_data' ])?$data[ 'goods_sku_data' ][ 0 ][ 'cost_price' ]:'0',
                'goods_spec_format' => $data[ 'goods_spec_format' ],
                'houniao_goods_sku_no' => isset($data['houniao_goods_sku_no']) ? $data['houniao_goods_sku_no'] : '',
                'vop_goods_id' => isset($data['vop_goods_id']) ? $data['vop_goods_id'] : '',
                'is_mp' => isset($data['is_mp']) ? $data['is_mp'] : '',
                'vop_sn' => isset($data['vop_sn']) ? $data['vop_sn'] : '',
                'ad_id' =>   isset($data['ad_id']) ? $data['ad_id'] : 0,
                'zhongmin_goods_sku_id' => isset($data['zhongmin_goods_sku_id']) ? $data['zhongmin_goods_sku_id'] : '',
                'zhongmin_goods_id' => isset($data['zhongmin_goods_id']) ? $data['zhongmin_goods_id'] : '',
            );

            //todo  商品的分佣比率是通过选择的分类来计算的
            $goods_category_model = new GoodsCategory();
            $goods_category_info = $goods_category_model->getCategoryInfo([['category_id', '=', $data[ 'category_id' ]]], 'commission_rate')['data'] ?? [];
            if (isset($data['source']) && ($data['source'] == 1 || $data['source'] == 2 || $data['source'] == 3) && empty($goods_category_info)) {
                $goods_category_info['commission_rate'] = 0;
            }elseif (empty($goods_category_info)){
                model('goods')->rollback();
                return $this->error([], '您选择的分类不存在');
            }
            $commission_rate = $goods_category_info['commission_rate'];
            $common_data = array(
                'goods_name' => $data[ 'goods_name' ],
                'goods_class' => $this->goods_class[ 'id' ],
                'goods_class_name' => $this->goods_class[ 'name' ],
                'goods_attr_class' => $data[ 'goods_attr_class' ],
                'goods_attr_name' => $data[ 'goods_attr_name' ],
                'site_id' => $data[ 'site_id' ],
                'site_name' => (string)$shop_info[ 'site_name' ],
                'website_id' => (int)$shop_info[ 'website_id' ],
                'category_id' => $data[ 'category_id' ],
                'category_id_1' => $data[ 'category_id_1' ],
                'category_id_2' => $data[ 'category_id_2' ],
                'category_id_3' => $data[ 'category_id_3' ],
                'category_name' => $data[ 'category_name' ],
                'brand_id' => $data[ 'brand_id' ],
                'brand_name' => $data[ 'brand_name' ],
                'goods_content' => $data[ 'goods_content' ],
                'is_own' => (int)$shop_info[ 'is_own' ],
                'goods_state' => $data[ 'goods_state' ],
                'goods_stock_alarm' => $data[ 'goods_stock_alarm' ],
                'is_free_shipping' => $data[ 'is_free_shipping' ],
                'shipping_template' => $data[ 'shipping_template' ],
                'goods_attr_format' => $data[ 'goods_attr_format' ],
                'introduction' => $data[ 'introduction' ],
                'keywords' => $data[ 'keywords' ],
                'unit' => $data[ 'unit' ],
                'commission_rate' => $commission_rate,
                'video_url' => $data[ 'video_url' ],
                'sort' => $data[ 'sort' ],
                'verify_state' => $verify_state,
                'goods_shop_category_ids' => $data[ 'goods_shop_category_ids' ],
                'supplier_id' => $data[ 'supplier_id' ],
                'is_live_goods' => $data['is_live_goods'],
                'live_start_time' => $data['live_start_time'],
                'live_end_time' => $data['live_end_time'],
                'platform_name' => $data['platform_name'],
                'live_link' => $data['live_link'],
                'create_time' => time(),
                'source' => isset($data['source']) ? $data['source'] : 0,
                'trade_type' => isset($data['trade_type']) ? $data['trade_type'] : 0,
                'max_buy' => $data['max_buy'],
                'min_buy' => $data['min_buy'],
                'deliver_time' => isset($data['deliver_time']) ? $data['deliver_time'] : '',
                'service_promise' => isset($data['service_promise']) ? $data['service_promise'] : '',
            );

            $goods_id = model('goods')->add(array_merge($goods_data, $common_data));
            $sku_arr = array();
            if($data[ 'goods_state' ]==2){
                $new_data['time_new'] = date("Y-m-d H:i:s.u");
                $new_data['res_data'] = $data;
                $new_data['method'] = "addGoods";
                $new_data['method_text'] = "唯品商品预售添加数据";
                error_log(print_r($new_data, 1), 3, dirname(__FILE__) . '/addGoods.txt');
            }
            //添加sku商品
            if (!empty($data[ 'goods_sku_data' ])) {
                foreach ($data[ 'goods_sku_data' ] as $item) {
                    $sku_data = array(
                        'sku_name' => $data[ 'goods_name' ] . ' ' . $item[ 'spec_name' ],
                        'spec_name' => $item[ 'spec_name' ],
                        'sku_no' => $item[ 'sku_no' ],
                        'zhongmin_goods_sku_id' => isset($item[ 'zhongmin_goods_sku_id' ]) ? $item[ 'zhongmin_goods_sku_id' ] : 0,
                        'zhongmin_goods_id' => isset($item[ 'zhongmin_goods_id' ]) ? $item[ 'zhongmin_goods_id' ] : 0,
                        'sku_spec_format' => !empty($item[ 'sku_spec_format' ]) ? json_encode($item[ 'sku_spec_format' ]) : "",
                        'price' => $item[ 'price' ],
                        'market_price' => $item[ 'market_price' ],
                        'cost_price' => $item[ 'cost_price' ],
                        'discount_price' => $item[ 'price' ],//sku折扣价（默认等于单价）
                        'stock' => $item[ 'stock' ],
                        'weight' => $item[ 'weight' ],
                        'volume' => $item[ 'volume' ],
                        'sku_image' => $item[ 'sku_image' ],
                        'sku_images' => $item[ 'sku_images' ],
                        'goods_id' => $goods_id,

                        'start_exp_time' => isset($item['start_exp_time']) ? $item['start_exp_time'] : 0,
                        'end_exp_time' => isset($item['end_exp_time']) ? $item['end_exp_time'] : 0,
                        'delivery_money_area' => isset($item['delivery_money_area']) ? $item['delivery_money_area'] : '',
                        'delivery_money' => isset($item['delivery_money']) ? $item['delivery_money'] : 0,
                        'no_delivery_area' => isset($item['no_delivery_area']) ? $item['no_delivery_area'] : '',

                        'vop_commission' => isset($item['vop_commission'])? $item['vop_commission'] : 0,
                        'vop_suggest_add_price' => isset($item['vop_suggest_add_price'])? $item['vop_suggest_add_price'] : 0,
                        'trade_type' => isset($data['trade_type']) ? $data['trade_type'] : 0,
                        'max_buy' => $data['max_buy'],
                        'min_buy' => $data['min_buy'],
                        'deliver_time' => isset($data['deliver_time']) ? $data['deliver_time'] : '',
                        'service_promise' => isset($data['service_promise']) ? $data['service_promise'] : '',
                    );

                    //唯品的限购  goods_state     //goods_state
                    if (isset($item['max_buy'])) {
                        unset($common_data['max_buy']);
                        unset($common_data['min_buy']);
                        $sku_data['max_buy'] = $item['max_buy'];
                        $sku_data['min_buy'] = $item['min_buy'];
                    }
                    $sku_arr[] = array_merge($sku_data, $common_data);
                }
                $add = model('goods_sku')->addList($sku_arr);
            }
            //唯品售卖时间
            if (isset($data['ad_id'])) {
                $ad_info = model('vop_ad')->getInfo(['ad_id' => $data['ad_id']]);
                if (!empty($ad_info)) {
                    model('vop_ad_goods')->add([
                        'ad_id' => $ad_info['ad_id'],
                        'goods_id' => $goods_id,
                        'sell_time_from' => $ad_info['sell_time_from'],
                        'sell_time_to' => $ad_info['sell_time_to'],
                        'pre_time' => $ad_info['pre_time'],
                        'create_time'=>time()
                    ]);
                }else{
                    $vop_ad_pre_log_info = model('vop_ad_pre_log')->getInfo(['ad_id' => $data['ad_id']]);
                    if(!empty($vop_ad_pre_log_info)){
                        //补丁修复    预售商品的时间
                        // 查询失败后单独查询
                        model('vop_ad_goods')->add([
                            'ad_id' => $data['ad_id'],
                            'goods_id' => $goods_id,
                            'sell_time_from' => $vop_ad_pre_log_info['sell_time_from'],
                            'sell_time_to' => $vop_ad_pre_log_info['sell_time_to'],
                            'pre_time' => $vop_ad_pre_log_info['pre_time'],
                            'create_time'=>time()
                        ]);
                    }
                }
            }
            // 赋值第一个商品sku_id
            $first_info = model('goods_sku')->getFirstData(['goods_id' => $goods_id], 'sku_id', 'sku_id asc');
            if (!empty($first_info)) {
                model('goods')->update(['sku_id' => $first_info[ 'sku_id' ]], [['goods_id', '=', $goods_id]]);
            }

            //添加商品属性关联关系
            $this->refreshGoodsAttribute($goods_id, $data[ 'goods_attr_format' ]);


            if (!empty($data[ 'goods_spec_format' ])) {
                //刷新SKU商品规格项/规格值JSON字符串
                $this->dealGoodsSkuSpecFormat($goods_id, $data[ 'goods_spec_format' ]);
            }

            //添加店铺添加统计
            //添加统计
            $stat = new Stat();
            $stat->addShopStat(['add_goods_count' => 1, 'site_id' => $data[ 'site_id' ]]);


            //添加商品 执行 价格判断
            if($common_data['source']==1){
                $condition=[];
                $condition[] = ["", 'exp', Db::raw("price='0.00' or  cost_price='0.00' or  discount_price='0.00'  ")];
                $condition[] = ['source', '=', 1];
                $condition[] = ['goods_state', '=', 1];
                $condition[] = ['goods_id', '=', $goods_id];
                $goods_list = model('goods_sku')->getList($condition, 'sku_id');
                if(!empty($goods_list)){
                    $new_data['time_new']=date("Y-m-d H:i:s.u");
                    $new_data['data']=$goods_list;
                    $new_data['text']="houniao添加商品 执行 价格判断下架";
                    error_log(print_r($new_data,1),3,dirname(__FILE__).'/houniaomodifyGoodsStatelog.txt');
                    model('goods_sku')->update(['goods_state' => 0], $condition);
                }
            }
            model('goods')->commit();
            return $this->success($goods_id);
        } catch ( \Exception $e ) {
            model('goods')->rollback();
            return $this->error($e->getMessage().$e->getLine());
        }
    }

    /**
     * 商品编辑
     * @param $data
     */
    public function editGoods($data, $allow_update = 0)
    {
        model('goods')->startTrans();
        try {
            $goods_id = $data[ 'goods_id' ];


            //店铺信息
            $shop_info = model('shop')->getInfo([['site_id', '=', $data[ 'site_id' ]]], 'site_name, website_id, is_own,cert_id,shop_status');

            $goods_config = new Config();
            $goods_verify_config = $goods_config->getVerifyConfig();
            $goods_verify_config = $goods_verify_config[ 'data' ][ 'value' ];
            $verify_state = 1;
            $verify_state_remark = '';
            if (!empty($goods_verify_config[ 'is_open' ])) {
                $verify_state = 0;//开启商品审核后，审核状态为：待审核
            }

            // 店铺未认证、审核中的状态下，商品需要审核
            if (empty($shop_info[ 'cert_id' ]) || $shop_info[ 'shop_status' ] == 2) {
                $verify_state = 0;//开启商品审核后，审核状态为：待审核
            }


            //候鸟价格调整通知，审核判断
            if (isset($data['verify_state'])) {
                $goods_info = model('goods')->getInfo(['goods_id' => $goods_id],'verify_state');
                //违规下架的按照原来的
                if ($goods_info['verify_state'] == 10) {
                    $verify_state = $goods_info['verify_state'];
                }
                //审核通过的按照传过来的
                if ($goods_info['verify_state'] == 1) {
                    $verify_state = $data['verify_state'];
                }

            }


            if (isset($data['source']) && $data['source'] != 0 && $allow_update == 0) {
                //候鸟,唯品只允许更改溢价、库存预警、图片
                $premium = 0.00;
                if ((float)$data['premium'] != 0 ) {
                    $premium = (float)$data['premium'];
                }
                //图片更改
                $goods_image = $data[ 'goods_image' ];
                if (!empty($data[ 'goods_sku_data' ])) {
                    $data[ 'goods_sku_data' ] = json_decode($data[ 'goods_sku_data' ], true);
                    $goods_image = $data[ 'goods_sku_data' ][ 0 ][ 'sku_image' ];

                    foreach ($data[ 'goods_sku_data' ] as $item) {
                        $up = [
                            'sku_image' => $item[ 'sku_image' ],
                            'sku_images' => $item[ 'sku_images' ],
                            'verify_state' => $verify_state,
                        ];
                        model('goods_sku')->update($up,['sku_id' => $item['sku_id']]);
                    }
                }

                model('goods')->update(['premium' => $premium,'goods_stock_alarm' => $data['goods_stock_alarm'],'goods_image' => $goods_image, 'verify_state' => $verify_state],['goods_id' => $goods_id]);

                if ((float)$premium == 0) {
                    if ($data['source'] == 1) {
                        $premium = 1.3;
                    }
                }

                if ((float)$premium != 0) {
                    //价格的重新计算
                    $goods_sku_list = model('goods_sku')->getList(['goods_id' => $goods_id],'cost_price,sku_id');
                    foreach ($goods_sku_list as $key => $value) {
                        if ((float)$value['cost_price'] == 0) {
                            continue;
                        }
                        //销售价
                        $price =  round((float)$value['cost_price']*(($premium-1)*$premium/10/1.13+$premium));
                        //市场价
                        $market_price = round((float)$price/0.8);
                        model('goods_sku')->update(['price' => $price, 'market_price' => $market_price, 'premium' => $premium],['sku_id' => $value['sku_id']]);
                        if ($key == 0) {
                            model('goods')->update(['price' => $price, 'market_price' => $market_price],['goods_id' => $goods_id]);
                        }
                    }
                }

                model('goods')->commit();
                return $this->success($goods_id);
            }


            $goods_image = $data[ 'goods_image' ];

            //SKU商品数据
            if (!empty($data[ 'goods_sku_data' ])) {
                $data[ 'goods_sku_data' ] = json_decode($data[ 'goods_sku_data' ], true);
                if (empty($goods_image)) {
                    $goods_image = $data[ 'goods_sku_data' ][ 0 ][ 'sku_image' ];
                }
            }

            //todo  商品的分佣比率是通过选择的分类来计算的
            $goods_category_model = new GoodsCategory();
            $goods_category_info = $goods_category_model->getCategoryInfo([['category_id', '=', $data[ 'category_id' ]]], 'commission_rate')['data'] ?? [];
            if (isset($data['source']) && ($data['source'] == 1 || $data['source'] == 2) && empty($goods_category_info)) {
                $goods_category_info['commission_rate'] = 0;
            }elseif (empty($goods_category_info)){
                model('goods')->rollback();
                return $this->error([], '您选择的分类不存在');
            }
            $commission_rate = $goods_category_info['commission_rate'];
            $goods_data = array(
                'goods_image' => $goods_image,
                'goods_stock' => $data[ 'goods_stock' ],
                'price' => $data[ 'goods_sku_data' ][ 0 ][ 'price' ],
                'market_price' => $data[ 'goods_sku_data' ][ 0 ][ 'market_price' ],
                'cost_price' => $data[ 'goods_sku_data' ][ 0 ][ 'cost_price' ],
                'goods_spec_format' => $data[ 'goods_spec_format' ],
            );

            $common_data = array(
                'goods_name' => $data[ 'goods_name' ],
                'goods_class' => $this->goods_class[ 'id' ],
                'goods_class_name' => $this->goods_class[ 'name' ],
                'goods_attr_class' => $data[ 'goods_attr_class' ],
                'goods_attr_name' => $data[ 'goods_attr_name' ],
                'site_id' => $data[ 'site_id' ],
                'site_name' => $shop_info[ 'site_name' ],
                'website_id' => $shop_info[ 'website_id' ],
                'category_id' => $data[ 'category_id' ],
                'category_id_1' => $data[ 'category_id_1' ],
                'category_id_2' => $data[ 'category_id_2' ],
                'category_id_3' => $data[ 'category_id_3' ],
                'category_name' => $data[ 'category_name' ],
                'brand_id' => $data[ 'brand_id' ],
                'brand_name' => $data[ 'brand_name' ],
                'goods_content' => $data[ 'goods_content' ],
                'is_own' => $shop_info[ 'is_own' ],
                'goods_state' => $data[ 'goods_state' ],
                'goods_stock_alarm' => $data[ 'goods_stock_alarm' ],
                'is_free_shipping' => $data[ 'is_free_shipping' ],
                'shipping_template' => $data[ 'shipping_template' ],
                'goods_attr_format' => $data[ 'goods_attr_format' ],
                'introduction' => $data[ 'introduction' ],
                'keywords' => $data[ 'keywords' ],
                'unit' => $data[ 'unit' ],
                'commission_rate' => $commission_rate,
                'video_url' => $data[ 'video_url' ],
                'sort' => $data[ 'sort' ],
                'verify_state' => $verify_state,
                'verify_state_remark' => $verify_state_remark,
                'goods_shop_category_ids' => $data[ 'goods_shop_category_ids' ],
                'supplier_id' => $data[ 'supplier_id' ],
                'is_live_goods' => $data['is_live_goods'],
                'live_start_time' => $data['live_start_time'],
                'live_end_time' => $data['live_end_time'],
                'platform_name' => $data['platform_name'],
                'live_link' => $data['live_link'],
                'modify_time' => time(),
                'premium' => isset($data['premium'])? $data['premium'] : '',
                'source' => isset($data['source'])? $data['source'] : 0,

                'max_buy' => $data['max_buy'],
                'min_buy' => $data['min_buy']
            );

            model('goods')->update(array_merge($goods_data, $common_data), [['goods_id', '=', $goods_id], ['goods_class', '=', $this->goods_class[ 'id' ]]]);


            //判断是否参与显示折扣
            $discount_model = new Discount();
            $discount_info_result = $discount_model->getDiscountGoodsInfo([['pdg.goods_id', '=', $goods_id], ['pd.status', '=', 1]]);
            $discount_info = $discount_info_result[ 'data' ];
            //修改sku商品
            foreach ($data[ 'goods_sku_data' ] as $item) {


                $sku_data = array(
                    'sku_name' => $data[ 'goods_name' ] . ' ' . $item[ 'spec_name' ],
                    'spec_name' => $item[ 'spec_name' ],
                    'sku_no' => $item[ 'sku_no' ],
                    'sku_spec_format' => !empty($item[ 'sku_spec_format' ]) ? json_encode($item[ 'sku_spec_format' ]) : "",
                    'price' => $item[ 'price' ],
                    'market_price' => $item[ 'market_price' ],
                    'cost_price' => $item[ 'cost_price' ],
                    'stock' => $item[ 'stock' ],
                    'weight' => $item[ 'weight' ],
                    'volume' => $item[ 'volume' ],
                    'sku_image' => $item[ 'sku_image' ],
                    'sku_images' => $item[ 'sku_images' ],
                    'goods_id' => $goods_id,

                    'start_exp_time' => isset($item['start_exp_time']) ? $item['start_exp_time'] : 0,
                    'end_exp_time' => isset($item['end_exp_time']) ? $item['end_exp_time'] : 0,
                    'delivery_money_area' => isset($item['delivery_money_area']) ? $item['delivery_money_area'] : '',
                    'delivery_money' => isset($item['delivery_money']) ? $item['delivery_money'] : 0,
                    'no_delivery_area' => isset($item['no_delivery_area']) ? $item['no_delivery_area'] : '',

                    'vop_commission' => isset($item['vop_commission'])? $item['vop_commission'] : 0,
                    'vop_suggest_add_price' => isset($item['vop_suggest_add_price'])? $item['vop_suggest_add_price'] : 0,

                    'max_buy' => $data['max_buy'],
                    'min_buy' => $data['min_buy']
                );

                //唯品的限购
                if (isset($item['max_buy'])) {
                    unset($common_data['max_buy']);
                    unset($common_data['min_buy']);
                    $sku_data['max_buy'] = $item['max_buy'];
                    $sku_data['min_buy'] = $item['min_buy'];
                }

                if (empty($discount_info)) {
                    $sku_data[ 'discount_price' ] = $item[ 'price' ];
                }
                model('goods_sku')->update(array_merge($sku_data, $common_data), [['sku_id', '=', $item[ 'sku_id' ]], ['goods_class', '=', $this->goods_class[ 'id' ]]]);

                //同步库存
                event('SyncStock', ['sku_id' => $item[ 'sku_id' ]]);
            }

            //唯品售卖时间
            if (isset($data['ad_id'])) {
                $ad_info = model('vop_ad')->getInfo(['ad_id' => $data['ad_id']]);
                if (!empty($ad_info)) {
                    $ad_goods_info = model('vop_ad_goods')->getInfo(['ad_id' => $data['ad_id'], 'goods_id' => $goods_id]);
                    if (empty($ad_goods_info)) {
                        model('vop_ad_goods')->add([
                            'ad_id' => $ad_info['ad_id'],
                            'goods_id' => $goods_id,
                            'sell_time_from' => $ad_info['sell_time_from'],
                            'sell_time_to' => $ad_info['sell_time_to'],
                            'pre_time' => $ad_info['pre_time'],
                        ]);
                    }
                }else{
                    $vop_ad_pre_log_info = model('vop_ad_pre_log')->getInfo(['ad_id' => $data['ad_id']]);
                    if(!empty($vop_ad_pre_log_info)){
                        //补丁修复    预售商品的时间
                        // 查询失败后单独查询
                        model('vop_ad_goods')->add([
                            'ad_id' => $data['ad_id'],
                            'goods_id' => $goods_id,
                            'sell_time_from' => $vop_ad_pre_log_info['sell_time_from'],
                            'sell_time_to' => $vop_ad_pre_log_info['sell_time_to'],
                            'pre_time' => $vop_ad_pre_log_info['pre_time'],
                            'create_time'=>time()
                        ]);
                    }
                }
            }

            // 赋值第一个商品sku_id
            $first_info = model('goods_sku')->getFirstData(['goods_id' => $goods_id], 'sku_id', 'sku_id asc');
            model('goods')->update(['sku_id' => $first_info[ 'sku_id' ]], [['goods_id', '=', $goods_id]]);

//			添加商品属性关联关系
            $this->refreshGoodsAttribute($goods_id, $data[ 'goods_attr_format' ]);

            if (!empty($data[ 'goods_spec_format' ])) {
//				刷新SKU商品规格项/规格值JSON字符串
                $this->dealGoodsSkuSpecFormat($goods_id, $data[ 'goods_spec_format' ]);
            }
            model('goods')->commit();
            return $this->success($goods_id);
        } catch ( \Exception $e ) {
            model('goods')->rollback();
            return $this->error($e->getMessage());
        }
    }

    /**
     * 修改商品状态
     * @param $goods_ids
     * @param $goods_state
     * @param $site_id
     *
     */
    public function modifyGoodsState($goods_ids, $goods_state, $site_id)
    {
        $shop_info = model('shop')->getInfo([['site_id', '=', $site_id]], 'shop_status');
        if($shop_info['shop_status'] != 1){
            return $this->error('', '店铺关闭中，不可以操作上下架');
        }
        model('goods')->update(['goods_state' => $goods_state], [['goods_id', 'in', (string)$goods_ids], ['site_id', '=', $site_id]]);
        model('goods_sku')->update(['goods_state' => $goods_state], [['goods_id', 'in', (string)$goods_ids], ['site_id', '=', $site_id]]);
        return $this->success(1);
    }

    /**
     * 修改审核状态
     * @param $goods_ids
     * @param $verify_state
     * @param $verify_state_remark
     * @return \multitype
     */
    public function modifyVerifyState($goods_ids, $verify_state, $verify_state_remark)
    {


        //         WHERE source=2 and verify_state=0 and `goods_id`<100000
        //        UPDATE `ns_goods_sku` SET `verify_state`=1    WHERE  source=2 and verify_state=0  and sku_id <500000
        //        UPDATE `ns_goods` SET `verify_state`=1    WHERE source=2 and verify_state=0
        //        UPDATE `ns_goods_sku` SET `verify_state`=1    WHERE source=2 and verify_state=0


        model('goods')->update(['verify_state' => $verify_state, 'verify_state_remark' => $verify_state_remark], [['goods_id', 'in', $goods_ids]]);
        model('goods_sku')->update(['verify_state' => $verify_state, 'verify_state_remark' => $verify_state_remark], [['goods_id', 'in', $goods_ids]]);
        return $this->success(1);
    }

    /**
     * 修改删除状态
     * @param $goods_ids
     * @param $is_delete
     * @param $site_id
     */
    public function modifyIsDelete($goods_ids, $is_delete, $site_id)
    {
        model('goods')->update(['is_delete' => $is_delete], [['goods_id', 'in', $goods_ids], ['site_id', '=', $site_id]]);
        model('goods_sku')->update(['is_delete' => $is_delete], [['goods_id', 'in', $goods_ids], ['site_id', '=', $site_id]]);
        return $this->success(1);
    }

    /**
     * 违规下架商品
     * @param $condition
     * @param $verify_state_remark
     * @return array
     */
    public function lockup($condition, $verify_state_remark)
    {
        model('goods')->update(['verify_state_remark' => $verify_state_remark, 'verify_state' => 10, 'goods_state' => 0], $condition);
        model('goods_sku')->update(['verify_state_remark' => $verify_state_remark, 'verify_state' => 10, 'goods_state' => 0], $condition);
        return $this->success(1);
    }

    /**
     * 修改商品点击量
     * @param $sku_id
     * @param $site_id
     */
    public function modifyClick($sku_id, $site_id)
    {
        model("goods_sku")->setInc([['sku_id', '=', $sku_id], ['site_id', '=', $site_id]], 'click_num', 1);
        return $this->success(1);
    }

    /**
     * 删除回收站商品
     * @param $goods_ids
     * @param $site_id
     */
    public function deleteRecycleGoods($goods_ids, $site_id)
    {
        model('goods')->delete([['goods_id', 'in', $goods_ids], ['site_id', '=', $site_id]]);
        model('goods_sku')->delete([['goods_id', 'in', $goods_ids], ['site_id', '=', $site_id]]);
        return $this->success(1);
    }

    /**
     * 获取商品信息
     * @param array $condition
     * @param string $field
     */
    public function getGoodsInfo($condition, $field = 'goods_id,goods_name,goods_class,goods_class_name,goods_attr_class,goods_attr_name,category_id,category_id_1,category_id_2,category_id_3,category_name,brand_id,brand_name,goods_image,goods_content,goods_state,verify_state,price,market_price,cost_price,goods_stock,goods_stock_alarm,is_free_shipping,shipping_template,goods_spec_format,goods_attr_format,introduction,keywords,unit,sort,commission_rate,video_url,goods_shop_category_ids,supplier_id,sale_num,evaluate,max_buy,min_buy,source,premium,subscription_open_state,corporate_subscription,subscription_status,vop_goods_id,is_mp,new_user_coupon_id')
    {
        $info = model('goods')->getInfo($condition, $field);
        return $this->success($info);
    }

    /**
     * 获取商品详情
     * @param $goods_id
     * @return \multitype
     */
    public function getGoodsDetail($goods_id)
    {
        $info = model('goods')->getInfo([['goods_id', '=', $goods_id]], "*");
        $info[ 'sku_data' ] = model('goods_sku')->getList([['goods_id', '=', $goods_id]], 'sku_id, sku_name, sku_no, sku_spec_format, price, market_price, cost_price, discount_price, stock, weight, volume,  sku_image, sku_images, sort,max_buy,min_buy,subscription_open_state,corporate_subscription,subscription_status');
        return $this->success($info);
    }

    /**
     * 商品sku 基础信息
     * @param $condition
     * @param string $field
     * @return array
     */
    public function getGoodsSkuInfo($condition, $field = "sku_id,sku_name,sku_spec_format,price,market_price,discount_price,promotion_type,start_time,end_time,stock,click_num,sale_num,collect_num,sku_image,sku_images,goods_id,site_id,goods_content,goods_state,verify_state,is_virtual,is_free_shipping,goods_spec_format,goods_attr_format,introduction,unit,video_url,max_buy,min_buy")
    {
        $info = model('goods_sku')->getInfo($condition, $field);
        return $this->success($info);
    }

    /**
     * 商品SKU 详情
     * @param $sku_id
     * @return mixed
     */
    public function getGoodsSkuDetail($sku_id)
    {
        $info = model('goods_sku')->getInfo([['sku_id', '=', $sku_id], ['is_delete', '=', 0]], "sku_id,sku_name,sku_spec_format,price,cost_price,market_price,discount_price,promotion_type,start_time,end_time,stock,click_num,sale_num,collect_num,sku_image,sku_images,goods_id,site_id,goods_content,goods_state,verify_state,is_virtual,is_free_shipping,goods_spec_format,goods_attr_format,introduction,unit,video_url,evaluate,category_id,category_id_1,category_id_2,category_id_3,category_name,max_buy,min_buy,source,ladder_discount_id,nthmfold_discount_id,subscription_open_state,corporate_subscription,subscription_status,cost_price,manjian_discount_id,discount_content,discount_state,new_user_coupon_id");
        return $this->success($info);
    }

    /**
     * 获取商品列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getGoodsList($condition = [], $field = 'goods_id,goods_class,goods_class_name,goods_attr_name,goods_name,site_id,site_name,website_id,sort,category_name,brand_name,goods_image,goods_content,is_own,goods_state,verify_state,price,market_price,cost_price,goods_stock,goods_stock_alarm,is_virtual,is_free_shipping,shipping_template,goods_spec_format,goods_attr_format,create_time,max_buy,min_buy', $order = 'create_time desc', $limit = null, $alias = 'a', $join = [])
    {
        $list = model('goods')->getList($condition, $field, $order, $alias, $join, '', $limit);
        return $this->success($list);
    }

    /**
     * 获取商品分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     */
    public function getGoodsPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = 'create_time desc', $field = 'goods_id,goods_name,site_id,site_name,goods_image,is_own,goods_state,verify_state,price,goods_stock,create_time,sale_num,is_virtual,goods_class,is_fenxiao,fenxiao_type,sku_id,max_buy,min_buy,source,cost_price,subscription_open_state,corporate_subscription,subscription_status,subscription_check_reason,brand_name,goods_stock_alarm,category_name,modify_time', $alias = '', $join = '', $group = null)
    {
        $list = model('goods')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, $group);
        return $this->success($list);
    }

    /**
     * 编辑商品库存价格等信息
     * @param $goods_sku_array
     * @return array|\multitype
     */
    public function editGoodsStock($goods_sku_array)
    {
        $goods_sku_model = new GoodsStock();
        model('goods')->startTrans();
        try {
            foreach ($goods_sku_array as $k => $v) {
                $sku_info = model("goods_sku")->getInfo([['sku_id', '=', $v[ 'sku_id' ]]], "goods_id,stock");

                if ($k == 0) {//修改商品中的价格等信息
                    $goods_data = [
                        'price' => $v[ 'price' ],
                        'market_price' => $v[ 'market_price' ],
                        'cost_price' => $v[ 'cost_price' ]
                    ];
                    model('goods')->update($goods_data, [['goods_id', '=', $sku_info[ 'goods_id' ]]]);
                }
                if ($v[ 'stock' ] > $sku_info[ 'stock' ]) {

                    $sku_stock_data = [
                        'sku_id' => $v[ 'sku_id' ],
                        'num' => $v[ 'stock' ] - $sku_info[ 'stock' ]
                    ];
                    $goods_sku_model->incStock($sku_stock_data);
                }
                if ($v[ 'stock' ] < $sku_info[ 'stock' ]) {
                    $sku_stock_data = [
                        'sku_id' => $v[ 'sku_id' ],
                        'num' => $sku_info[ 'stock' ] - $v[ 'stock' ]
                    ];
                    $goods_sku_model->decStock($sku_stock_data);
                }
                unset($v[ 'stock' ]);
                model('goods_sku')->update($v, [['sku_id', '=', $v[ 'sku_id' ]]]);

                //同步库存
                event('SyncStock', ['sku_id' => $v[ 'sku_id' ]]);
            }
            model('goods')->commit();
            return $this->success();
        } catch ( \Exception $e ) {
            model('goods')->rollback();
            return $this->error($e->getMessage());
        }
    }

    /**
     * 获取商品sku列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param string $limit
     */
    public function getGoodsSkuList($condition = [], $field = 'sku_id,sku_name,price,stock,sale_num,sku_image,goods_id,goods_name,site_id,site_name,spec_name,max_buy,min_buy,cost_price', $order = 'create_time desc', $limit = null, $alias = 'a', $join = [])
    {
        $list = model('goods_sku')->getList($condition, $field, $order, $alias, $join, '', $limit);
        return $this->success($list);
    }

    /**
     * 获取商品sku分页列表
     * @param array $condition
     * @param number $page
     * @param string $page_size
     * @param string $order
     * @param string $field
     * @param string $alias
     * @param string $join
     */
    public function getGoodsSkuPageList($condition = [], $page = 1, $page_size = PAGE_LIST_ROWS, $order = '', $field = 'site_id,goods_id,sku_id,sku_name,price,sku_image,create_time,stock,goods_state,goods_class,goods_class_name,max_buy,min_buy,ladder_discount_id,nthmfold_discount_id,subscription_open_state,corporate_subscription,subscription_status,cost_price,manjian_discount_id', $alias = '', $join = '', $group = null)
    {
        $list = model('goods_sku')->pageList($condition, $field, $order, $page, $page_size, $alias, $join, $group);
        return $this->success($list);
    }

    /**
     * 刷新商品关联属性关系
     * @param $goods_id
     * @param $goods_attr_format
     */
    public function refreshGoodsAttribute($goods_id, $goods_attr_format)
    {
        model('goods_attr_index')->delete([['goods_id', '=', $goods_id], ['app_module', '=', 'shop']]);
        if (!empty($goods_attr_format)) {

            $list = model('goods_sku')->getList([['goods_id', '=', $goods_id]], 'sku_id');
            $goods_attr_format = json_decode($goods_attr_format, true);
            $attr_data = [];
            foreach ($goods_attr_format as $k => $v) {
                foreach ($list as $ck => $cv) {
                    $item = [
                        'goods_id' => $goods_id,
                        'sku_id' => $cv[ 'sku_id' ],
                        'attr_id' => $v[ 'attr_id' ],
                        'attr_value_id' => $v[ 'attr_value_id' ],
                        'attr_class_id' => $v[ 'attr_class_id' ],
                        'app_module' => 'shop'
                    ];
                    $attr_data[] = $item;
                }

            }

            model('goods_attr_index')->addList($attr_data);
        }
    }

    /**
     * 刷新SKU商品规格项/规格值JSON字符串
     * @param int $goods_id 商品id
     * @param string $goods_spec_format 商品完整规格项/规格值json
     */
    public function dealGoodsSkuSpecFormat($goods_id, $goods_spec_format)
    {
        if (empty($goods_spec_format)) return;

        $goods_spec_format = json_decode($goods_spec_format, true);
        //根据goods_id查询sku商品列表，查询：sku_id、sku_spec_format 列
        $sku_list = model('goods_sku')->getList([['goods_id', '=', $goods_id], ['sku_spec_format', '<>', '']], 'sku_id,sku_spec_format', 'sku_id asc');
        if (!empty($sku_list)) {

//			$temp = 0;//测试性能，勿删

            //循环SKU商品列表
            foreach ($sku_list as $k => $v) {
//				$temp++;

                $sku_format = $goods_spec_format;//最终要存储的值
                $current_format = json_decode($v[ 'sku_spec_format' ], true);//当前SKU商品规格值json

                $selected_data = [];//已选规格/规格值json

                //1、找出已选规格/规格值json

                //循环完整商品规格json
                foreach ($sku_format as $sku_k => $sku_v) {
//					$temp++;


                    //循环当前SKU商品规格json
                    foreach ($current_format as $current_k => $current_v) {
//						$temp++;


                        //匹配规格项
                        if ($current_v[ 'spec_id' ] == $sku_v[ 'spec_id' ]) {

                            //循环规格值
                            foreach ($sku_v[ 'value' ] as $sku_value_k => $sku_value_v) {
//								$temp++;

                                //匹配规格值id
                                if ($current_v[ 'spec_value_id' ] == $sku_value_v[ 'spec_value_id' ]) {
                                    $sku_format[ $sku_k ][ 'value' ][ $sku_value_k ][ 'selected' ] = true;
                                    $sku_format[ $sku_k ][ 'value' ][ $sku_value_k ][ 'sku_id' ] = $v[ 'sku_id' ];
                                    $selected_data[] = $sku_format[ $sku_k ][ 'value' ][ $sku_value_k ];
                                    break;
                                }
                            }

                        }

                    }
                }

                //2、找出未选中的规格/规格值json
                foreach ($sku_format as $sku_k => $sku_v) {
//					$temp++;

                    foreach ($sku_v[ 'value' ] as $sku_value_k => $sku_value_v) {
//						$temp++;

                        if (!isset($sku_value_v[ 'selected' ])) {

                            $refer_data = [];//参考已选中的规格/规格值json
                            $refer_data[] = $sku_value_v;

//							根据已选中的规格值进行参考
                            foreach ($selected_data as $selected_k => $selected_v) {
//								$temp++;
//								排除自身，然后进行参考
                                if ($selected_v[ 'spec_id' ] != $sku_value_v[ 'spec_id' ]) {
                                    $refer_data[] = $selected_v;
                                }
                            }

                            foreach ($sku_list as $again_k => $again_v) {
//								$temp++;

                                //排除当前SKU商品
                                if ($again_v[ 'sku_id' ] != $v[ 'sku_id' ]) {

                                    $current_format_again = json_decode($again_v[ 'sku_spec_format' ], true);
                                    $count = count($current_format_again);//规格总数量
                                    $curr_count = 0;//当前匹配规格数量

                                    //循环当前SKU商品规格json
                                    foreach ($current_format_again as $current_again_k => $current_again_v) {
//										$temp++;

                                        foreach ($refer_data as $fan_k => $fan_v) {
//											$temp++;

                                            if ($current_again_v[ 'spec_value_id' ] == $fan_v[ 'spec_value_id' ] && $current_again_v[ 'spec_id' ] == $fan_v[ 'spec_id' ]) {
                                                $curr_count++;
                                            }
                                        }

                                    }

//									匹配数量跟规格总数一致表示匹配成功
                                    if ($curr_count == $count) {
                                        $sku_format[ $sku_k ][ 'value' ][ $sku_value_k ][ 'selected' ] = false;
                                        $sku_format[ $sku_k ][ 'value' ][ $sku_value_k ][ 'sku_id' ] = $again_v[ 'sku_id' ];
                                        break;
                                    }
                                }

                            }

                            //没有匹配到规格值，则禁用
                            if (!isset($sku_format[ $sku_k ][ 'value' ][ $sku_value_k ][ 'selected' ])) {
                                $sku_format[ $sku_k ][ 'value' ][ $sku_value_k ][ 'disabled' ] = false;
                            }

                        }
                    }
                }

//				var_dump($sku_format);
//				var_dump("=========");
                //修改goods_sku表表中的goods_spec_format字段，将$sku_format值传入
                model('goods_sku')->update(['goods_spec_format' => json_encode($sku_format)], [['sku_id', '=', $v[ 'sku_id' ]]]);

            }

//			var_dump("性能：" . $temp);

        }

    }

    /**
     * 商品推广二维码
     * @param $sku_id
     * @param $goods_name
     * @param string $type
     * @return array
     */
    public function qrcode($sku_id, $goods_name, $type = "create")
    {
        $data = [
            'app_type' => "all", // all为全部
            'type' => $type, // 类型 create创建 get获取
            'data' => [
                "sku_id" => $sku_id
            ],
            'page' => '/pages/goods/detail/detail',
            'qrcode_path' => 'upload/qrcode/goods',
            'qrcode_name' => "goods_qrcode_" . $sku_id,
        ];

        event('Qrcode', $data, true);
        $app_type_list = config('app_type');

        $path = [];

        //获取站点信息
        $website_model = new WebsiteModel();
        $website_info = $website_model->getWebSite([['site_id', '=', 0]], 'wap_domain');

        foreach ($app_type_list as $k => $v) {
            switch ($k) {
                case 'h5':
                    if (!empty($website_info[ 'data' ][ 'wap_domain' ])) {
                        $path[ $k ][ 'status' ] = 1;
                        $path[ $k ][ 'url' ] = $website_info[ 'data' ][ 'wap_domain' ] . $data[ 'page' ] . '?sku_id=' . $sku_id;
                        $path[ $k ][ 'img' ] = "upload/qrcode/goods/goods_qrcode_" . $sku_id . "_" . $k . ".png";
                    } else {
                        $path[ $k ][ 'status' ] = 2;
                        $path[ $k ][ 'message' ] = '未配置手机端域名';
                    }
                    break;
                case 'weapp' :
                    $config = new ConfigModel();
                    $res = $config->getConfig([['site_id', '=', 0], ['app_module', '=', 'admin'], ['config_key', '=', 'WEAPP_CONFIG']]);
                    if (!empty($res[ 'data' ])) {
                        if (empty($res[ 'data' ][ 'value' ][ 'qrcode' ])) {
                            $path[ $k ][ 'status' ] = 2;
                            $path[ $k ][ 'message' ] = '未配置微信小程序';
                        } else {
                            $path[ $k ][ 'status' ] = 1;
                            $path[ $k ][ 'img' ] = $res[ 'data' ][ 'value' ][ 'qrcode' ];
                        }

                    } else {
                        $path[ $k ][ 'status' ] = 2;
                        $path[ $k ][ 'message' ] = '未配置微信小程序';
                    }
                    break;

                case 'wechat' :
                    $config = new ConfigModel();
                    $res = $config->getConfig([['site_id', '=', 0], ['app_module', '=', 'admin'], ['config_key', '=', 'WECHAT_CONFIG']]);
                    if (!empty($res[ 'data' ])) {
                        if (empty($res[ 'data' ][ 'value' ][ 'qrcode' ])) {
                            $path[ $k ][ 'status' ] = 2;
                            $path[ $k ][ 'message' ] = '未配置微信公众号';
                        } else {
                            $path[ $k ][ 'status' ] = 1;
                            $path[ $k ][ 'img' ] = $res[ 'data' ][ 'value' ][ 'qrcode' ];
                        }
                    } else {
                        $path[ $k ][ 'status' ] = 2;
                        $path[ $k ][ 'message' ] = '未配置微信公众号';
                    }
                    break;
            }

        }

        $return = [
            'path' => $path,
            'goods_name' => $goods_name,
        ];

        return $this->success($return);
    }

    /**
     * 增加商品销量
     * @param $sku_id
     * @param $num
     */
    public function incGoodsSaleNum($sku_id, $num)
    {
        $condition = array(
            ["sku_id", "=", $sku_id]
        );
        //增加sku销量
        $res = model("goods_sku")->setInc($condition, "sale_num", $num);
        if ($res !== false) {
            $sku_info = model("goods_sku")->getInfo($condition, "goods_id");
            $res = model("goods")->setInc([["goods_id", "=", $sku_info[ "goods_id" ]]], "sale_num", $num);
            return $this->success($res);
        }

        return $this->error($res);
    }

    /**
     * 减少商品销量
     * @param $sku_id
     * @param $num
     */
    public function decGoodsSaleNum($sku_id, $num)
    {
        $condition = array(
            ["sku_id", "=", $sku_id]
        );
        //增加sku销量
        $res = model("goods_sku")->setDec($condition, "sale_num", $num);
        if ($res !== false) {
            $sku_info = model("goods_sku")->getInfo($condition, "goods_id");
            $res = model("goods")->setDec([["goods_id", "=", $sku_info[ "goods_id" ]]], "sale_num", $num);
            return $this->success($res);
        }
        return $this->error($res);
    }

    /**
     * 获取商品总数
     * @param array $condition
     * @return array
     */
    public function getGoodsTotalCount($condition = [])
    {
        $res = model('goods')->getCount($condition);
        return $this->success($res);
    }

    /**
     * 获取商品规格项总数
     * @param array $condition
     * @return array
     */
    public function getGoodsSkuCount($condition = [])
    {
        $res = model('goods_sku')->getCount($condition);
        return $this->success($res);
    }

    /**
     * 复制商品(创建商品的副本)
     * @param $goods_id
     */
    public function copyGoods($condition)
    {
        //查询商品原来的数据用于复制
        $goods_info = model('goods')->getInfo($condition, '*');
        if (empty($goods_info))
            return $this->error();

        $sku_list = model('goods_sku')->getList($condition, '*');
        if (empty($sku_list))
            return $this->error();

        model('goods')->startTrans();

        try {

            //店铺信息
            $shop_info = model('shop')->getInfo([['site_id', '=', $goods_info[ 'site_id' ]]], 'site_name, website_id, is_own,cert_id,shop_status');

            $goods_config = new Config();
            $goods_verify_config = $goods_config->getVerifyConfig();
            $goods_verify_config = $goods_verify_config[ 'data' ][ 'value' ];
            $verify_state = 1;
            if (!empty($goods_verify_config[ 'is_open' ])) {
                $verify_state = 0;//开启商品审核后，审核状态为：待审核
            }

            // 店铺未认证、审核中的状态下，商品需要审核
            if (empty($shop_info[ 'cert_id' ]) || $shop_info[ 'shop_status' ] == 0 || $shop_info[ 'shop_status' ] == 2) {
                $verify_state = 0;//开启商品审核后，审核状态为：待审核
            }

            //先创建新的商品数据(商品默认没有库存,模拟创建操作)
            $goods_data = $goods_info;
            unset($goods_data[ 'goods_id' ]);//删除原有的自增长键
            //重置部分字段, 库存都为空, 默认不上架
            $goods_data[ 'sku_id' ] = 0;
            $goods_data[ 'create_time' ] = time();
            $goods_data[ 'modify_time' ] = time();
            $goods_data[ 'discount_id' ] = 0;
            $goods_data[ 'seckill_id' ] = 0;
            $goods_data[ 'topic_id' ] = 0;
            $goods_data[ 'pintuan_id' ] = 0;
            $goods_data[ 'bargain_id' ] = 0;
            $goods_data[ 'sale_num' ] = 0;

            $goods_data[ 'evaluate' ] = 0;
            $goods_data[ 'evaluate_shaitu' ] = 0;
            $goods_data[ 'evaluate_shipin' ] = 0;
            $goods_data[ 'evaluate_zhuiping' ] = 0;
            $goods_data[ 'evaluate_haoping' ] = 0;
            $goods_data[ 'evaluate_zhongping' ] = 0;
            $goods_data[ 'evaluate_chaping' ] = 0;
            $goods_data[ 'is_fenxiao' ] = 0;
            $goods_data[ 'fenxiao_type' ] = 1;


//            $goods_data['goods_state'] = 0;
            $goods_data[ 'verify_state' ] = $verify_state;
            $goods_data[ 'verify_state_remark' ] = '';
//            $goods_data['goods_stock'] = 0;
            $goods_id = model('goods')->add($goods_data);
            $sku_arr = [];
            foreach ($sku_list as $k => $v) {
                $sku_data = $v;
                unset($sku_data[ 'sku_id' ]);
                $sku_data[ 'goods_id' ] = $goods_id;
//                $sku_data['stock'] = 0;
//                $sku_data['goods_state'] = 0;
                $sku_data[ 'verify_state' ] = $verify_state;

                $sku_data[ 'evaluate' ] = 0;
                $sku_data[ 'evaluate_shaitu' ] = 0;
                $sku_data[ 'evaluate_shipin' ] = 0;
                $sku_data[ 'evaluate_zhuiping' ] = 0;
                $sku_data[ 'evaluate_haoping' ] = 0;
                $sku_data[ 'evaluate_zhongping' ] = 0;
                $sku_data[ 'evaluate_chaping' ] = 0;

                $sku_data[ 'click_num' ] = 0;
                $sku_data[ 'sale_num' ] = 0;
                $sku_data[ 'collect_num' ] = 0;

                $sku_data[ 'discount_id' ] = 0;
                $sku_data[ 'seckill_id' ] = 0;
                $sku_data[ 'topic_id' ] = 0;
                $sku_data[ 'pintuan_id' ] = 0;
                $sku_data[ 'bargain_id' ] = 0;
                $sku_data[ 'sale_num' ] = 0;

//                $sku_data[ 'goods_state' ] = 0;
//                $sku_data[ 'verify_state' ] = 0;
                $sku_data[ 'verify_state_remark' ] = '';

                $sku_data[ 'start_time' ] = 0;
                $sku_data[ 'end_time' ] = 0;
                $sku_data[ 'promotion_type' ] = 0;
                $sku_data[ 'discount_price' ] = $v[ 'price' ];
                $sku_data[ 'start_time' ] = 0;
                $sku_arr[] = $sku_data;
            }


            model('goods_sku')->addList($sku_arr);

            // 赋值第一个商品sku_id
            $first_info = model('goods_sku')->getFirstData(['goods_id' => $goods_id], 'sku_id', 'sku_id asc');
            model('goods')->update(['sku_id' => $first_info[ 'sku_id' ]], [['goods_id', '=', $goods_id]]);

            //添加商品属性关联关系
            $this->refreshGoodsAttribute($goods_id, $goods_info[ 'goods_attr_format' ]);

            if (!empty($data[ 'goods_spec_format' ])) {
                //刷新SKU商品规格项/规格值JSON字符串
                $this->dealGoodsSkuSpecFormat($goods_id, $data[ 'goods_spec_format' ]);
            }

            //添加店铺添加统计
            //添加统计
            $stat = new Stat();
            $stat->addShopStat(['add_goods_count' => 1, 'site_id' => $goods_info[ 'site_id' ]]);
            model('goods')->commit();

            return $this->success($goods_id);
        } catch ( \Exception $e ) {
            model('goods')->rollback();
            return $this->error($e->getMessage());
        }
    }



    /**
     * 获取会员已购该商品数
     * @param $goods_id
     * @param $member_id
     */
    public function getGoodsPurchasedNum($goods_id, $member_id){
        $join = [
            ['order o', 'o.order_id = og.order_id', 'left']
        ];
        $num = model("order_goods")->getSum([
            ['og.member_id', '=', $member_id],
            ['og.goods_id', '=', $goods_id],
            ['o.order_status', '<>', OrderCommon::ORDER_CLOSE],
            ['og.refund_status', '<>', OrderRefund::REFUND_COMPLETE ]
        ], 'og.num', 'og', $join);
        return $num;
    }

    /**
     * 获取商品会员价详情
     * @param $sku_info
     * @return mixed|null
     */
    public function getMemberPriceInfo($sku_info){
        $res = event('MemberPriceInfo', $sku_info);
        $info = null;
        foreach($res as $val){
            if(!empty($val)){
                $info = $val;
                break;
            }
        }
        if(!empty($info)){
            return $info;
        }else{
            return $sku_info;
        }
    }

    /**
     * 获取商品营销活动详情
     * @param $sku_info
     * @return mixed|null
     */
    public function getGoodsPromotionInfo($sku_info)
    {
        $res = event('PromotionInfo', $sku_info);
        $info = null;
        foreach($res as $val){
            if(!empty($val)){
                $info = $val;
                break;
            }
        }
        if(!empty($info)){
            return $info;
        }else{
            return $sku_info;
        }
    }

    /**
     * @param $sku_info
     * @return mixed
     */
    public function getGoodsPromotionPrice($sku_info)
    {
        $res = event('PromotionPrice', $sku_info);
        $info = null;
        foreach($res as $val){
            if(!empty($val)){
                $info = $val;
                break;
            }
        }
        if(!empty($info)){
            return $info;
        }else{
//            $price = $sku_info['price'];
//            $sku_info['price'] = $price;
            $sku_info['goods_money'] = $sku_info['price'] * $sku_info['num'];
            $sku_info['real_goods_money'] = $sku_info['goods_money'];
            $sku_info['promotion_money'] = 0;//优惠金额
            return $sku_info;
        }
    }

    /**
     * 商品动态列表
     * @param $condition
     * @param $page
     * @param $page_size
     * @param $order
     * @return array
     */
    public function getDynamicPageList($condition, $page, $page_size, $order){
        $list = model('goods_dynamic')->pageList($condition,'*',$order, $page, $page_size);
        return $this->success($list);
    }

    public function dealGoods($id){
        $res = model('goods_dynamic')->update(['status' => 1],['id' => $id]);
        return $this->success($res);
    }

    public function getMemberPrice($site_id = 1, $app_module = 'shop'){
        $config = new ConfigModel();
        $res    = $config->getConfig([['site_id', '=', $site_id], ['app_module', '=', $app_module], ['config_key', '=', 'SETMEMBERPRICE']]);
        if (empty($res['data']['value'])) {
            $res['data']['value'] = [
                'member_price_state'           => 0,
                'member_rate'           => 10,
            ];
        }
        return $res;
    }

    public function setMemberPrice($data, $site_id = 1, $app_model = 'shop'){
        $config = new ConfigModel();
        $res    = $config->setConfig($data, '设置会员价', 1, [['site_id', '=', $site_id], ['app_module', '=', $app_model], ['config_key', '=', 'SETMEMBERPRICE']]);
        return $res;
    }

    public function setPremium($data, $site_id){
        $shop_info = model('shop')->getInfo(['site_id' => $site_id],'is_own');
        if (empty($shop_info) || $shop_info['is_own'] == 0) {
            return $this->error('','非自营店不可修改');
        }

        $goods_ids = explode(',',$data['premium_goods_ids']);
        $premium = $data['premium_price'];
        if (!empty($goods_ids) && !empty($premium)) {

            foreach ($goods_ids as $key => $goods_id) {
                $goods_update = [
                    'verify_state' => 0,
                    'premium' => $premium
                ];

                $sku_list = model('goods_sku')->getList(['goods_id' => $goods_id],'cost_price,sku_id');
                foreach ($sku_list as $k => $sku) {
                    if ((float)$sku['cost_price'] == 0) {
                        continue;
                    }
                    //销售价
                    $price =  round((float)$sku['cost_price']*(($premium-1)*$premium/10/1.13+$premium));
                    //市场价
                    $market_price = round((float)$price/0.8);

                    model('goods_sku')->update(['price' => $price, 'market_price' => $market_price, 'premium' => $premium, 'verify_state' => 0],['sku_id'=> $sku['sku_id']]);
                    if ($k == 0){
                        $goods_update['price'] = $price;
                        $goods_update['market_price'] = $market_price;
                    }
                }

                model('goods')->update($goods_update, ['goods_id' => $goods_id]);
            }
            return $this->success();
        }
        return $this->error('','参数缺失');
    }





    /**
     * 查询预售时间
     */
    public function getVopAdGoods($condition = [],$field="*")
    {
        $getInfo = model('vop_ad_goods')->getFirstData($condition,$field,"id desc");
        return $this->success($getInfo);
    }


    /**
     * 查询预售时间
     */
    public function getVopAdPre($condition = [],$field="*")
    {
        $getInfo = model('vop_ad_pre')->getInfo($condition,$field);
        return $this->success($getInfo);
    }



}