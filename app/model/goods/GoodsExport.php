<?php

/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\model\goods;


use app\model\BaseModel;
use app\model\shop\ShopCategory;

/**
 * 商品导出记录
 */
class GoodsExport extends BaseModel
{

    /**
     * 添加导出记录
     * @param $data
     * @return array
     */
    public function addExport($data)
    {
        $res = model("goods_export")->add($data);
        return $this->success($res);
    }

    /**
     * 更新导出记录
     * @param $data
     * @return array
     */
    public function editExport($data, $condition)
    {
        $res = model("goods_export")->update($data, $condition);
        return $this->success($res);
    }

    /**
     * 删除导出记录
     * @param $data
     * @return array
     */
    public function deleteExport($condition)
    {
        $res = model("goods_export")->delete($condition);
        return $this->success($res);
    }

    /**
     * 获取导出记录
     * @param $member_id
     * @return array
     */
    public function getExport($condition, $field = "*", $order = '')
    {

        $list = model("goods_export")->getList($condition, $field, $order);
        return $this->success($list);
    }

    public function exportData($condition, $condition_desc = [])
    {
        $page_index = 1;
        $page_size = 10 * 10000;
        do{
            $res = $this->exportDataExec($condition, $condition_desc, $page_index, $page_size);
            if($res['code'] >= 0){
                $page_index ++;
            }
        }while($res['code'] >= 0);

        return $res;
    }

    /**
     * 导出商品数据
     * @param $condition
     */
    protected function exportDataExec($condition, $condition_desc = [], $page_index = 1, $page_size = 10 * 10000){
        try {
            $column_condition = array_column($condition, 2, 0);
            $site_id = $column_condition[ 'site_id' ] ?? 0;

            //TODO 一次性查询太多数据会报致命性错误
            $field_dict = array(
                'goods_id' => ['name' => '商品id'],
                'goods_name' => ['name' => '商品名称'],
                'sku_id' => ['name' => '规格id'],
                'spec_name' => ['name' => '商品规格'],
                'goods_class_name' => ['name' => '商品类型'],
                'sku_no' => ['name' => '规格编号'],
                'price' => ['name' => '销售价'],
                'market_price' => ['name' => '市场价'],
                'cost_price' => ['name' => '成本价'],
                'stock' => ['name' => '库存'],
                'category_name' => ['name' => '所属分类'],
                'brand_name' => ['name' => '商品品牌'],
                'goods_state' => ['name' => '商品状态', 'dict' => [1=>'正常', 0 => '下架']],
                'verify_state' => ['name' => '审核状态', 'dict' => [1=>'已审核', 0 => '待审核', 10 => '违规下架', -1 => '审核中', -2 => '审核失败']],
                'click_num' => ['name' => '点击量'],
                'sale_num' => ['name' => '销量'],
                'collect_num' => ['name' => '收藏量'],
            );

            $field_string = implode(',', array_keys($field_dict));

            //将要导出的商品数据
            $res = model('goods_sku')->pageList($condition, $field_string, '', $page_index, $page_size);
            //没有数据则停止
            if(count($res['list']) == 0){
                return $this->error($page_index, '已没有需要导出的数据');
            }

            $list = $res['list'];

            $temp_val = [];
            $temp_key = [];
            foreach ($field_dict as $k => $v) {
                $temp_val[] = $v['name'];
                $temp_key[] = "{\$$k}";
            }

            //TODO 循环太大 占用内存太多 服务器会崩掉
            /*foreach($list as $k => $v) {
                foreach ($v as $v_k => $v_v){
                    $tron_item = $field_dict[ $v_k ] ?? [];
                    if (!empty($tron_item)) {
                        $tron_dict_item = $tron_item[ 'dict' ] ?? [];
                        if (!empty($tron_dict_item) && isset($tron_dict_item[ $v_v ])) {
                            $list[$k][ $v_k ] = $tron_dict_item[ $v_v ];
                        }
                    }
                }
            }*/

            //导出csv文件
            $file_path = __UPLOAD__ . "/common/csv/" . date("Ymd") . '/';
            $file_name = time() . '_第'. $page_index .'页_' . '.csv';
            if (! dir_mkdir($file_path)) {
                return $this->error('', '目录创建失败');
            }

            $file_name = downloadCsv($temp_val, $temp_key, $file_path . $file_name, $list);

            //创建记录
            $data = array(
                'condition' => json_encode($condition_desc),
                'status' => 1,
                'create_time' => time(),
                'path' => $file_name,
                'site_id' => $site_id
            );
            $result = $this->addExport($data);
            return $this->success($result);

        }catch (\Exception $e){
            return $this->error([], $e->getMessage(). $e->getFile() . $e->getLine());
        }

    }

}
