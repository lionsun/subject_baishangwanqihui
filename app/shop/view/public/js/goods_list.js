var laytpl, form, element, repeat_flag, table;
$(function () {
	$("body").on("click", ".contraction", function () {
		var goods_id = $(this).attr("data-goods-id");
		var open = $(this).attr("data-open");
		var tr = $(this).parent().parent().parent().parent();
		var index = tr.attr("data-index");
		var open_state = $(this).attr("data-corporate-open");
		if (open_state==0){
			$('.corporate_subscription').hide();
		}
		if (open == 1) {
			$(this).children("span").text("+");
			$(".js-sku-list-" + index).remove();
		} else {
			$(this).children("span").text("-");
			$.ajax({
				url: ns.url("shop/goods/getGoodsSkuList"),
				data: {goods_id: goods_id},
				dataType: 'JSON',
				type: 'POST',
				async: false,
				success: function (res) {
					var list = res.data;
					var sku_list = $("#skuList").html();
					var data = {
						list: list,
						index: index
					};
					laytpl(sku_list).render(data, function (html) {
						tr.after(html);
					});
					
					layer.photos({
					  	photos: '.img-wrap',
						anim: 5
					});
				}
			});
		}
		$(this).attr("data-open", (open == 0 ? 1 : 0));
	});
	
	layui.use(['form', 'laytpl', 'element'], function () {
		form = layui.form;
		repeat_flag = false; //防重复标识
		element = layui.element;
		laytpl = layui.laytpl;
		
		form.render();
		refreshTable(0);



		form.on('radio(subscription_open_state)', function(data) {
			if (data.value == 1) {
				$('.corporate_subscription').show();
			} else {
				$('.corporate_subscription').hide();
			}
		});


		//监听Tab切换，以改变地址hash值
		element.on('tab(goods_list_tab)', function () {

			var type = this.getAttribute('data-type');
			$("input[name='goods_state']").val("");
			$("input[name='verify_state']").val("");
			if (type) {
				var id = this.getAttribute('lay-id');
				$("input[name='" + type + "']").val(id);
			}
			var html = '<button class="layui-btn layui-btn-primary" lay-event="delete">批量删除</button>';

			if (type == "goods_state" && id == 1) {
				// 销售中状态：下架
				html += '<button class="layui-btn layui-btn-primary" lay-event="off_goods">下架</button>';
				html += '<button class="layui-btn layui-btn-primary" lay-event="update_premium">批量改价</button>';
				$("input[name='verify_state']").val(1);
			} else if (type == "goods_state" && id == 0) {
				// 仓库中状态：上架
				html += '<button class="layui-btn layui-btn-primary" lay-event="on_goods">上架</button>';
				$("input[name='verify_state']").val(1);
			}
			$("#batchOperation").html(html);







			// 全部、销售中、仓库中、待审核
			if (type == null || type == "goods_state" || (type == "verify_state" && id == 0)) {
				refreshTable(0,id);
			} else if (type == "verify_state") {
				// 审核失败、违规下架
				refreshTable(1);
			}
			
		});
		
		// 监听工具栏操作
		table.tool(function (obj) {
			var data = obj.data;
			switch (obj.event) {
				case 'select': //推广
					goodsUrl(data);
					break;
				case 'preview': //预览
					goodsPreview(data);
					break;
				case 'edit':
					//编辑`
					if (data.goods_class == 1) {
						location.href = ns.url("shop/goods/editgoods", {"goods_id": data.goods_id});
					} else {
						location.href = ns.url("shop/virtualgoods/editgoods", {"goods_id": data.goods_id});
					}
					break;
				case 'delete':
					//删除
					deleteGoods(data.goods_id);
					break;
				case 'off_goods':
					//下架
					offGoods(data.goods_id);
					break;
				case 'on_goods':
					//上架
					onGoods(data.goods_id);
					break;
				case 'editStock':
					editStock(data);
					break;
				// case 'corporate_subscription':
				// 	//企业认购
				// 	corporateSubscription(data);
				// 	break;
				case 'copy':
					copyGoods(data.goods_id);
					break;
				case 'update_premium':
					//更新溢价
					updatePremium(data.goods_id);
					break;
			}
		});
		
		// 提交修改后的库存
		form.on('submit(edit_stock)', function (obj) {
			var field = obj.field;
			if (repeat_flag) return false;
			repeat_flag = true;
			
			$.ajax({
				type: "POST",
				url: ns.url("shop/goods/editGoodsStock"),
				data: {
					'sku_list': field
				},
				dataType: 'JSON',
				success: function (res) {
					layer.msg(res.message);
					repeat_flag = false;
					if (res.code == 0) {
						table.reload();
						refreshVerifyStateCount();
						layer.closeAll('page');
					}
				}
			});
		});

		// 提交企业认购信息
		// form.on('submit(corporate_subscription)', function (data) {
		// 	var corporate_subscription = new Array();
		// 	if(data.field.subscription_open_state==1){
		// 		var file_length = $('.member-level-table tr').length;
		// 		for (var i=0;i<file_length;i++)
		// 		{
		// 			corporate_subscription[i] = new Array();
		// 			var level_id = $(".member-level-table").find('tr').eq(i).find($("input[name='id']")).val();
		// 			var level_name = $(".member-level-table").find('tr').eq(i).find($("input[name='level_name']")).val();
		// 			var level_rate = $(".member-level-table").find('tr').eq(i).find($("input[name='level_rate']")).val();
		// 			if (level_rate == '') {
		// 				layer.msg('请输入认购比例');
		// 				return false;
		// 			}
		// 			corporate_subscription[i]['level_id'] = level_id;
		// 			corporate_subscription[i]['level_name'] = level_name;
		// 			corporate_subscription[i]['level_rate'] = level_rate;
		// 		}
		// 		data.field.corporate_subscription = arrayToJsonString(corporate_subscription);
		// 	}else{
		// 		data.field.corporate_subscription = [];
		// 	}
		// 	console.log(data.field);
		// 	if (repeat_flag) return false;
		// 	repeat_flag = true;
		// 	$.ajax({
		// 		type: "POST",
		// 		url: ns.url("shop/goods/corporateSubscription"),
		// 		data: {
		// 			'goods_id': data.field.goods_id,
		// 			'corporate_subscription': data.field.corporate_subscription,
		// 			'subscription_open_state': data.field.subscription_open_state
		// 		},
		// 		dataType: 'JSON',
		// 		success: function (res) {
		// 			console.log(res);
		// 			layer.msg(res.message);
		// 			repeat_flag = false;
		// 			if (res.code == 0) {
		// 				location.reload();
		// 			}
		// 		}
		// 	});
		// });
		
		// 批量操作
		table.bottomToolbar(function (obj) {
			
			if (obj.data.length < 1) {
				layer.msg('请选择要操作的数据');
				return;
			}
			var id_array = new Array();
			for (i in obj.data) id_array.push(obj.data[i].goods_id);
			switch (obj.event) {
				case "delete":
					deleteGoods(id_array.toString());
					break;
				case 'off_goods':
					//下架
					offGoods(id_array.toString());
					break;
				case 'on_goods':
					//上架
					onGoods(id_array.toString());
					break;
				case 'update_premium':
					//更新溢价
					updatePremium(id_array.toString());
					break;
			}
		});
		
		// 搜索功能
		form.on('submit(search)', function (data) {
			table.reload({
				page: {
					curr: 1
				},
				where: data.field
			});
			refreshVerifyStateCount();
			return false;
		});

		// 导出功能
		form.on('submit(export)', function (data) {

			exportGoods(data.field);
			return false;
		});
		
		// 验证
		form.verify({
			int: function (value) {
				if (value < 0) {
					return '销量不能小于0!'
				}
				if (value % 1 != 0) {
					return '销量不能为小数!'
				}
			},
		})



		form.on('submit(premium_save)', function(data) {
			if (repeat_flag) return false;
			repeat_flag = true;

			$.ajax({
				type: "POST",
				url: ns.url("shop/goods/updatePremium"),
				data: data.field,
				dataType: 'JSON',
				success: function(res) {
					layer.msg(res.message);
					repeat_flag = false;

					if (res.code == 0) {
						layer.closeAll('page');
						table.reload();
					}
				}
			});
		});
		
	});
});

/**
 * 刷新表格列表
 * @param status 状态：0 在售，1 审核违规
 */
function refreshTable(status,id=0) {


	console.log(id);

	if(id==0){

		var cols = [
			[{
				type: 'checkbox',
				unresize: 'false',
				width: '3%'
			}, {
				title: '商品信息',
				unresize: 'false',
				width: '22%',
				templet: '#goods_info'
			}, {
				field: 'price',
				title: '<span style="padding-right: 15px;">价格(元)</span>',
				unresize: 'false',
				align: 'right',
				templet: function (data) {
					return '<span style="padding-right: 15px;" title="￥'+ data.price +'">￥' + data.price + '</span>';
				}
			}, {
				field: 'cost_price',
				title: '成本',
				unresize: 'false',
				templet: function(data) {
					return data.cost_price;
				}
			}, {
				field: 'goods_stock',
				title: '库存',
				unresize: 'false'
			}, {
				field: 'goods_stock_alarm',
				title: '库存预警',
				unresize: 'false'
			}, {
				field: 'sale_num',
				title: '销量',
				unresize: 'false'
			}, {
				field: 'category_name',
				title: '分类',
				unresize: 'false',
				templet: function(data) {
					return '<span title="'+ data.category_name +'">'+ data.category_name +'</span>';
				}
			}, {
				field: 'brand_name',
				title: '品牌',
				unresize: 'false',
				templet: function(data) {
					return '<span title="'+ data.brand_name +'">'+ data.brand_name +'</span>';
				}
			}, {
				title: '商品状态',
				unresize: 'false',
				templet: function (data) {
					var str = '';
					if (data.goods_state == 1 && data.verify_state == 1) {
						str = '销售中';
					} else if (data.goods_state == 0 && data.verify_state == 1) {
						str = '仓库中';
					}else if (data.verify_state === 0) {
						str = '审核中';
					}else if (data.verify_state === 2) {
						str = '预售中';
					}else if (data.verify_state === -2) {
						str = '审核失败';
					}else if (data.verify_state === 10) {
						str = '违规下架';
					}
					return str;
				}
			}, {
				title: '直播状态',
				unresize: 'false',
				templet: function (data) {
					if(data.live_status != '未参与'){
						return '<span title="直播状态：'+ data.live_status +'\n平台：'+ data.platform_name +'\n开始时间：'+ ns.time_to_date(data.live_start_time) +'\n结束时间：'+ ns.time_to_date(data.live_end_time) +'">'+ data.live_status +'</span>';
					}else{
						return '<span title="'+ data.live_status +'\n">'+ data.live_status +'</span>';
					}
				}
			}, {
				title: '创建时间',
				unresize: 'false',
				templet: function (data) {
					return '<span title="'+ ns.time_to_date(data.create_time) +'">'+ ns.time_to_date(data.create_time) +'</span>';
				}
			}, {
				title: '修改时间',
				unresize: 'false',
				templet: function (data) {
					return '<span title="'+ ns.time_to_date(data.modify_time) +'">'+ ns.time_to_date(data.modify_time) +'</span>';
				}
			}, {
				title: '操作',
				toolbar: '#action',
				unresize: 'false'
			}]
		];
	}else{

		var cols = [
			[{
				type: 'checkbox',
				unresize: 'false',
				width: '3%'
			}, {
				title: '商品信息',
				unresize: 'false',
				width: '22%',
				templet: '#goods_info'
			}, {
				field: 'price',
				title: '<span style="padding-right: 15px;">价格(元)</span>',
				unresize: 'false',
				align: 'right',
				templet: function (data) {
					return '<span style="padding-right: 15px;" title="￥'+ data.price +'">￥' + data.price + '</span>';
				}
			}, {
				field: 'cost_price',
				title: '成本',
				unresize: 'false',
				templet: function(data) {
					return data.cost_price;
				}
			}, {
				field: 'goods_stock',
				title: '库存',
				unresize: 'false'
			}, {
				field: 'category_name',
				title: '分类',
				unresize: 'false',
				templet: function(data) {
					return '<span title="'+ data.category_name +'">'+ data.category_name +'</span>';
				}
			}, {
				field: 'brand_name',
				title: '品牌',
				unresize: 'false',
				templet: function(data) {
					return '<span title="'+ data.brand_name +'">'+ data.brand_name +'</span>';
				}
			}, {
				title: '商品状态',
				unresize: 'false',
				templet: function (data) {
					var str = '';
					if (data.goods_state == 1 && data.verify_state == 1) {
						str = '销售中';
					} else if (data.goods_state == 0 && data.verify_state == 1) {
						str = '仓库中';
					}else if (data.verify_state === 0) {
						str = '审核中';
					}else if (data.verify_state === 2) {
						str = '预售中';
					}else if (data.verify_state === -2) {
						str = '审核失败';
					}else if (data.verify_state === 10) {
						str = '违规下架';
					}
					return str;
				}
			}, {
				title: '直播状态',
				unresize: 'false',
				templet: function (data) {
					if(data.live_status != '未参与'){
						return '<span title="直播状态：'+ data.live_status +'\n平台：'+ data.platform_name +'\n开始时间：'+ ns.time_to_date(data.live_start_time) +'\n结束时间：'+ ns.time_to_date(data.live_end_time) +'">'+ data.live_status +'</span>';
					}else{
						return '<span title="'+ data.live_status +'\n">'+ data.live_status +'</span>';
					}
				}
			}, {
				title: '创建时间',
				unresize: 'false',
				width: '7%',
				templet: function (data) {
					return '<span title="'+ ns.time_to_date(data.create_time) +'">'+ ns.time_to_date(data.create_time) +'</span>';
				}
			}, {
				title: '预售开始时间',
				unresize: 'false',
				width: '7%',
				templet: function (data) {
					return '<span title="'+ ns.time_to_date(data.sell_time_from) +'">'+ ns.time_to_date(data.sell_time_from) +'</span>';
				}
			}, {
				title: '预售结束时间',
				unresize: 'false',
				width: '10%',
				templet: function (data) {
					return '<span title="'+ ns.time_to_date(data.sell_time_to) +'">'+ ns.time_to_date(data.sell_time_to) +'</span>';
				}
			}, {
				title: '操作',
				toolbar: '#action',
				unresize: 'false',
				width: '15%'
			}]
		];


	}




	if (status === 1) {
		cols = [
			[{
				type: 'checkbox',
				unresize: 'false',
				width: '3%'
			}, {
				title: '商品信息',
				unresize: 'false',
				width: '25%',
				templet: '#goods_info'
			}, {
				title: '审核状态',
				unresize: 'false',
				width: '12%',
				templet: function (data) {
					var str = '';
					if (data.verify_state == 1) {
						str = '已审核';
					} else if (data.verify_state == 0) {
						str = '待审核';
					} else if (data.verify_state == 10) {
						str = '违规下架';
					} else if (data.verify_state == -1) {
						str = '审核中';
					} else if (data.verify_state == -2) {
						str = '审核失败';
					}
					return str;
				}
			}, {
				title: '创建时间',
				unresize: 'false',
				width: '25%',
				templet: function (data) {
					return ns.time_to_date(data.create_time);
				}
			}, {
				title: '原因',
				unresize: 'false',
				width: '20%',
				templet: function (data) {
					var str = '';
					if (data.verify_state == 10) {
						str = '<a href="javascript:getVerifyStateRemark(' + data.goods_id + ',10);" class="ns-text-color">查看</a>';
					} else if (data.verify_state == -2) {
						str = '<a href="javascript:getVerifyStateRemark(' + data.goods_id + ',-2);" class="ns-text-color">查看</a>';
					}
					return str;
				}
			}, {
				title: '操作',
				toolbar: '#action',
				unresize: 'false',
				width: '15%'
			}]
		];
	}
	
	table = new Table({
		elem: '#goods_list',
		url: ns.url("shop/goods/lists"),
		cols: cols,
		bottomToolbar: "#batchOperation",
		where: {
			search_text: $("input[name='search_text']").val(),
			goods_state: $("input[name='goods_state']").val(),
			verify_state: $("input[name='verify_state']").val(),
			start_sale: $("input[name='start_sale']").val(),
			end_sale: $("input[name='end_sale']").val(),
			// start_price: 0,
			// end_price: 0,
			goods_shop_category_ids: $("select[name='goods_shop_category_ids'] option:checked").val(),
			category_id: $("input[name='category_id']").val(),
			goods_class: $("select[name='goods_class'] option:checked").val(),
		}
	});
	
	refreshVerifyStateCount();
}

function add() {
	var html = $("#selectAddGoods").html();
	laytpl(html).render({}, function (html) {
		layer.open({
			type: 1,
			title: '选择商品类型',
			area: ['600px'],
			content: html
			
		});
	});
}
//
// function corporateSubscription(data){
// 	var html = $("#corporateSubscription").html();
// 	laytpl(html).render(data, function (html) {
// 		layer_corporate_subscription = layer.open({
// 			title: '设置企业认购',
// 			skin: 'layer-tips-class',
// 			type: 1,
// 			area: ['800px'],
// 			content: html,
// 		});
// 	});
// 	form.render();
// }

// 删除
function deleteGoods(goods_ids) {
	layer.confirm('删除后进入回收站，确定删除吗?', function () {
		if (repeat_flag) return;
		repeat_flag = true;
		
		$.ajax({
			url: ns.url("shop/goods/deleteGoods"),
			data: {goods_ids: goods_ids.toString()},
			dataType: 'JSON',
			type: 'POST',
			success: function (res) {
				layer.msg(res.message);
				repeat_flag = false;
				if (res.code == 0) {
					table.reload();
					refreshVerifyStateCount();
				}
			}
		});
	});
}


// 批量改价
function updatePremium(goods_ids) {
	laytpl($("#premium_content").html()).render(goods_ids, function(html) {
		layer_pass = layer.open({
			title: '批量改价',
			skin: 'layer-tips-class',
			type: 1,
			area: ['550px'],
			content: html,
		});
	});
	$('#premium_goods_ids').val(goods_ids);
}




//商品下架
function offGoods(goods_ids) {
	if (repeat_flag) return;
	repeat_flag = true;
	
	$.ajax({
		url: ns.url("shop/goods/offGoods"),
		data: {goods_state: 0, goods_ids: goods_ids.toString()},
		dataType: 'JSON',
		type: 'POST',
		success: function (res) {
			layer.msg(res.message);
			repeat_flag = false;
			if (res.code == 0) {
				table.reload();
				refreshVerifyStateCount();
			}
		}
	});
}

//商品上架
function onGoods(goods_ids) {
	
	if (repeat_flag) return;
	repeat_flag = true;
	
	$.ajax({
		url: ns.url("shop/goods/onGoods"),
		data: {goods_state: 1, goods_ids: goods_ids.toString()},
		dataType: 'JSON',
		type: 'POST',
		success: function (res) {
			layer.msg(res.message);
			repeat_flag = false;
			if (res.code == 0) {
				table.reload();
				refreshVerifyStateCount();
			}
		}
	});
}

// 编辑库存
function editStock(data) {
	$.ajax({
		type: "POST",
		url: ns.url("shop/goods/getGoodsSkuList"),
		data: {
			'goods_id': data.goods_id,
		},
		dataType: 'JSON',
		success: function (res) {
			laytpl($("#edit_stock").html()).render(res.data, function (html) {
				layer_stock = layer.open({
					title: '修改库存',
					skin: 'layer-tips-class',
					type: 1,
					area: ['1000px'],
					content: html,
				});
			});
		}
	});
	
}

// 商品推广
function goodsUrl(data) {
	$(".operation-wrap[data-goods-id='" + data.goods_id + "'] .popup-qrcode-wrap").css("display", "block");
	$('#goods_name').html(data.goods_name);
	$.ajax({
		type: "POST",
		url: ns.url("shop/goods/goodsUrl"),
		data: {
			'goods_id': data.goods_id
		},
		dataType: 'JSON',
		success: function (res) {
			if (res.data.path.h5.status == 1) {
				res.data.goods_id = data.goods_id;
				laytpl($("#goods_url").html()).render(res.data, function (html) {
					$(".operation-wrap[data-goods-id='" + data.goods_id + "'] .popup-qrcode-wrap").html(html).show();
					
					$("body").click(function (e) {
						if (!$(e.target).closest(".popup-qrcode-wrap").length) {
							$(".operation-wrap[data-goods-id='" + data.goods_id + "'] .popup-qrcode-wrap").hide();
						}
					});
				});
			} else {
				layer.msg(res.data.path.h5.message);
			}
		}
	});
	
}

// 商品预览
var isOpenGoodsPreviewPopup = false;//防止重复弹出商品预览框
function goodsPreview(data) {
	if (isOpenGoodsPreviewPopup) return;
	isOpenGoodsPreviewPopup = true;
	$.ajax({
		type: "POST",
		url: ns.url("shop/goods/goodsPreview"),
		data: {
			'goods_id': data.goods_id
		},
		dataType: 'JSON',
		success: function (res) {
			if (res.data.path.h5.status == 1) {
				res.data.goods_id = data.goods_id;
				
				laytpl($("#goods_preview").html()).render(res.data, function (html) {
					var layerIndex = layer.open({
						title: '商品预览',
						skin: 'layer-tips-class',
						type: 1,
						area: ['600px', '600px'],
						content: html,
						success: function () {
							isOpenGoodsPreviewPopup = false;
						}
					});
				});
			} else {
				layer.msg(res.data.path.h5.message);
			}
		}
	});
}

/**
 * 获取违规下架原因
 * @param goods_id
 * @param verify_state
 */
function getVerifyStateRemark(goods_id, verify_state) {
	$.ajax({
		url: ns.url("shop/goods/getVerifyStateRemark"),
		data: {goods_id: goods_id},
		dataType: 'JSON',
		type: 'POST',
		success: function (res) {
			var data = res.data;
			if (data) {
				var title = '';
				if (verify_state == -2) title = '审核失败原因';
				else title = '违规下架原因';
				layer.open({
					title: title,
					content: data.verify_state_remark,
					cancel: function (index, layero) {
						repeat_flag = false;
					},
					end: function () {
						repeat_flag = false;
					},
					yes: function (index, layero) {
						repeat_flag = false;
						layer.close(index);
					}
				});
			}
		}
	});
	
}

/**
 * 刷新审核状态商品数量
 */
function refreshVerifyStateCount() {
	$.ajax({
		url: ns.url("shop/goods/refreshVerifyStateCount"),
		type: 'POST',
		dataType: 'JSON',
		success: function (res) {
			for (var i = 0; i < res.length; i++) {
				if (res[i].count) $('div[lay-filter="goods_list_tab"] li[data-type="verify_state"][lay-id="' + res[i].state + '"] span.count').text(res[i].count).show();
				else $('div[lay-filter="goods_list_tab"] li[data-type="verify_state"][lay-id="' + res[i].state + '"] span').hide();
			}
		}
	});
}

function closeStock() {
	layer.close(layer_stock);
}

function closeCorporateSubscription() {
	layer.close(layer_corporate_subscription);
}

//复制商品
function copyGoods(goods_id){
	layer.confirm('确定复制当前商品吗?', function() {
		$.ajax({
			url: ns.url("shop/goods/copygoods"),
			data: {goods_id : goods_id},
			dataType: 'JSON',
			type: 'POST',
			success: function(res) {
				layer.msg(res.message);

				if (res.code >= 0) {
					table.reload();
					refreshVerifyStateCount();
					// location.reload();
				}
			}
		});
	}, function () {
		layer.close();
	});
}

/**
 * 导出商品
 * @param field
 */
function exportGoods(field){
	layer.confirm('确定按当前条件导出吗?', {title:'商品导出'},function() {
		$.ajax({
			url: ns.url("shop/goods/exportgoods"),
			data: field,
			dataType: 'JSON',
			type: 'POST',
			success: function(res) {
				layer.msg(res.message);

				if (res.code >= 0) {
					window.open(ns.url('shop/goods/export'));
				}
			}
		});
	}, function () {
		layer.close();
	});
}

function arrayToJsonString(o) {
	var len = o.length;
	var new_arr = new Array();
	var str = '',strone='',strs='',jsonstr='';
	for(var i = 0;i<len;i++){
		new_arr = o[i];
		for(var k in new_arr){
			strone += '"'+k+'"'+':'+'"'+new_arr[k]+'"'+',';
		}
		str = '{'+strone.substring(0,strone.length-1)+'}';
		strone='';
		strs += str+',';
		new_arr=[];
	}
	strs = '['+strs.substring(0,strs.length-1)+']';
	return strs;
}

