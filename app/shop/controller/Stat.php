<?php
/**
 * Niushop商城系统 - 团队十年电商经验汇集巨献!
 * =========================================================
 * Copy right 2019-2029 山西牛酷信息科技有限公司, 保留所有权利。
 * ----------------------------------------------
 * 官方网址: https://www.niushop.com
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用。
 * 任何企业和个人不允许对程序代码以任何形式任何目的再发布。
 * =========================================================
 */

namespace app\shop\controller;

use app\model\system\Stat as StatModel;
use think\helper\Time;


/**
 * 数据统计
 * Class Stat
 * @package app\shop\controller
 */
class Stat extends BaseShop
{
	
	
	public function __construct()
	{
		//执行父类构造函数
		parent::__construct();
		
	}
	
	/**
	 * 店铺统计
	 * @return mixed
	 */
	public function shop()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 0);
			
			if ($date_type == 0) {
				$start_time = strtotime("today");
				$time_range = date('Y-m-d', $start_time);
			} else if ($date_type == 1) {
				$start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			} else if ($date_type == 2) {
				$start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			}
			
			$stat_model = new StatModel();
			
			$shop_stat_sum = $stat_model->getShopStatSum($this->site_id, $start_time);
			
			$shop_stat_sum['data']['time_range'] = $time_range;
			
			return $shop_stat_sum;
		} else {
			return $this->fetch("stat/shop");
		}
	}
	
	/**
	 * 店铺统计报表
	 * */
	public function getShopStatList()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 1);
			
			if ($date_type == 1) {
				$start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 6;
			} else if ($date_type == 2) {
				$start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 29;
			}
			
			$stat_model = new StatModel();
			
			$stat_list = $stat_model->getShopStatList($this->site_id, $start_time);
			
			//将时间戳作为列表的主键
			$shop_stat_list = array_column($stat_list['data'], null, 'day_time');
			
			$data = array();
			
			for ($i = 0; $i <= $day; $i++) {
				$time = strtotime(date('Y-m-d', strtotime("-" . ($day - $i) . " day")));
				$data['time'][ $i ] = date('Y-m-d', $time);
				if (array_key_exists($time, $shop_stat_list)) {
					$data['order_total'][ $i ] = $shop_stat_list[ $time ]['order_total'];
					$data['shipping_total'][ $i ] = $shop_stat_list[ $time ]['shipping_total'];
					$data['refund_total'][ $i ] = $shop_stat_list[ $time ]['refund_total'];
					$data['order_pay_count'][ $i ] = $shop_stat_list[ $time ]['order_pay_count'];
					$data['goods_pay_count'][ $i ] = $shop_stat_list[ $time ]['goods_pay_count'];
					$data['shop_money'][ $i ] = $shop_stat_list[ $time ]['shop_money'];
					$data['platform_money'][ $i ] = $shop_stat_list[ $time ]['platform_money'];
					$data['collect_shop'][ $i ] = $shop_stat_list[ $time ]['collect_shop'];
					$data['collect_goods'][ $i ] = $shop_stat_list[ $time ]['collect_goods'];
					$data['visit_count'][ $i ] = $shop_stat_list[ $time ]['visit_count'];
					$data['order_count'][ $i ] = $shop_stat_list[ $time ]['order_count'];
					$data['goods_count'][ $i ] = $shop_stat_list[ $time ]['goods_count'];
					$data['add_goods_count'][ $i ] = $shop_stat_list[ $time ]['add_goods_count'];
					$data['member_count'][ $i ] = $shop_stat_list[ $time ]['member_count'];
				} else {
					$data['order_total'][ $i ] = 0.00;
					$data['shipping_total'][ $i ] = 0.00;
					$data['refund_total'][ $i ] = 0.00;
					$data['order_pay_count'][ $i ] = 0;
					$data['goods_pay_count'][ $i ] = 0;
					$data['shop_money'][ $i ] = 0.00;
					$data['platform_money'][ $i ] = 0.00;
					$data['collect_shop'][ $i ] = 0;
					$data['collect_goods'][ $i ] = 0;
					$data['visit_count'][ $i ] = 0;
					$data['order_count'][ $i ] = 0;
					$data['goods_count'][ $i ] = 0;
					$data['add_goods_count'][ $i ] = 0;
					$data['member_count'][ $i ] = 0;
				}
			}
			
			$data['time_range'] = $time_range;
			
			return $data;
		}
	}
	
	/**
	 * 商品统计
	 * @return mixed
	 */
	public function goods()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 0);
			
			if ($date_type == 0) {
				$start_time = strtotime("today");
				$time_range = date('Y-m-d', $start_time);
			} else if ($date_type == 1) {
				$start_time = strtotime("-6 day");
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			} else if ($date_type == 2) {
				$start_time = strtotime("-29 day");
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			}
			
			$stat_model = new StatModel();
			
			$shop_stat_sum = $stat_model->getShopStatSum($this->site_id, $start_time);
			
			$shop_stat_sum['data']['time_range'] = $time_range;
			
			return $shop_stat_sum;
		} else {
			return $this->fetch("stat/goods");
		}
	}
	
	/**
	 * 商品统计报表
	 * */
	public function getGoodsStatList()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 1);
			
			if ($date_type == 1) {
				$start_time = strtotime("-6 day");
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 6;
			} else if ($date_type == 2) {
				$start_time = strtotime("-29 day");
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 29;
			}
			
			$stat_model = new StatModel();
			
			$stat_list = $stat_model->getShopStatList($this->site_id, $start_time);
			
			//将时间戳作为列表的主键
			$shop_stat_list = array_column($stat_list['data'], null, 'day_time');
			
			$data = array();
			
			for ($i = 0; $i <= $day; $i++) {
				$time = strtotime(date('Y-m-d', strtotime("-" . ($day - $i) . " day")));
				$data['time'][ $i ] = date('Y-m-d', $time);
				if (array_key_exists($time, $shop_stat_list)) {
					$data['order_total'][ $i ] = $shop_stat_list[ $time ]['order_total'];
					$data['shipping_total'][ $i ] = $shop_stat_list[ $time ]['shipping_total'];
					$data['refund_total'][ $i ] = $shop_stat_list[ $time ]['refund_total'];
					$data['order_pay_count'][ $i ] = $shop_stat_list[ $time ]['order_pay_count'];
					$data['goods_pay_count'][ $i ] = $shop_stat_list[ $time ]['goods_pay_count'];
					$data['shop_money'][ $i ] = $shop_stat_list[ $time ]['shop_money'];
					$data['platform_money'][ $i ] = $shop_stat_list[ $time ]['platform_money'];
					$data['collect_shop'][ $i ] = $shop_stat_list[ $time ]['collect_shop'];
					$data['collect_goods'][ $i ] = $shop_stat_list[ $time ]['collect_goods'];
					$data['visit_count'][ $i ] = $shop_stat_list[ $time ]['visit_count'];
					$data['order_count'][ $i ] = $shop_stat_list[ $time ]['order_count'];
					$data['goods_count'][ $i ] = $shop_stat_list[ $time ]['goods_count'];
					$data['add_goods_count'][ $i ] = $shop_stat_list[ $time ]['add_goods_count'];
					$data['member_count'][ $i ] = $shop_stat_list[ $time ]['member_count'];
				} else {
					$data['order_total'][ $i ] = 0.00;
					$data['shipping_total'][ $i ] = 0.00;
					$data['refund_total'][ $i ] = 0.00;
					$data['order_pay_count'][ $i ] = 0;
					$data['goods_pay_count'][ $i ] = 0;
					$data['shop_money'][ $i ] = 0.00;
					$data['platform_money'][ $i ] = 0.00;
					$data['collect_shop'][ $i ] = 0;
					$data['collect_goods'][ $i ] = 0;
					$data['visit_count'][ $i ] = 0;
					$data['order_count'][ $i ] = 0;
					$data['goods_count'][ $i ] = 0;
					$data['add_goods_count'][ $i ] = 0;
					$data['member_count'][ $i ] = 0;
				}
			}
			
			$data['time_range'] = $time_range;
			
			return $data;
		}
	}
	
	/**
	 * 交易统计
	 * @return mixed
	 */
	public function order()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 0);
			
			if ($date_type == 0) {
				$start_time = strtotime("today");
				$time_range = date('Y-m-d', $start_time);
			} else if ($date_type == 1) {
				$start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			} else if ($date_type == 2) {
				$start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			}
			
			$stat_model = new StatModel();
			
			$shop_stat_sum = $stat_model->getShopStatSum($this->site_id, $start_time);
			
			$shop_stat_sum['data']['time_range'] = $time_range;
			
			return $shop_stat_sum;
		} else {
			return $this->fetch("stat/order");
		}
	}
	
	/**
	 * 交易统计报表
	 * */
	public function getOrderStatList()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 1);
			
			if ($date_type == 1) {
				$start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 6;
			} else if ($date_type == 2) {
				$start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 29;
			}
			
			$stat_model = new StatModel();
			
			$stat_list = $stat_model->getShopStatList($this->site_id, $start_time);
			
			//将时间戳作为列表的主键
			$shop_stat_list = array_column($stat_list['data'], null, 'day_time');
			
			$data = array();
			
			for ($i = 0; $i <= $day; $i++) {
				$time = strtotime(date('Y-m-d', strtotime("-" . ($day - $i) . " day")));
				$data['time'][ $i ] = date('Y-m-d', $time);
				if (array_key_exists($time, $shop_stat_list)) {
					$data['order_total'][ $i ] = $shop_stat_list[ $time ]['order_total'];
					$data['shipping_total'][ $i ] = $shop_stat_list[ $time ]['shipping_total'];
					$data['refund_total'][ $i ] = $shop_stat_list[ $time ]['refund_total'];
					$data['order_pay_count'][ $i ] = $shop_stat_list[ $time ]['order_pay_count'];
					$data['goods_pay_count'][ $i ] = $shop_stat_list[ $time ]['goods_pay_count'];
					$data['shop_money'][ $i ] = $shop_stat_list[ $time ]['shop_money'];
					$data['platform_money'][ $i ] = $shop_stat_list[ $time ]['platform_money'];
					$data['collect_shop'][ $i ] = $shop_stat_list[ $time ]['collect_shop'];
					$data['collect_goods'][ $i ] = $shop_stat_list[ $time ]['collect_goods'];
					$data['visit_count'][ $i ] = $shop_stat_list[ $time ]['visit_count'];
					$data['order_count'][ $i ] = $shop_stat_list[ $time ]['order_count'];
					$data['goods_count'][ $i ] = $shop_stat_list[ $time ]['goods_count'];
					$data['add_goods_count'][ $i ] = $shop_stat_list[ $time ]['add_goods_count'];
				} else {
					$data['order_total'][ $i ] = 0.00;
					$data['shipping_total'][ $i ] = 0.00;
					$data['refund_total'][ $i ] = 0.00;
					$data['order_pay_count'][ $i ] = 0;
					$data['goods_pay_count'][ $i ] = 0;
					$data['shop_money'][ $i ] = 0.00;
					$data['platform_money'][ $i ] = 0.00;
					$data['collect_shop'][ $i ] = 0;
					$data['collect_goods'][ $i ] = 0;
					$data['visit_count'][ $i ] = 0;
					$data['order_count'][ $i ] = 0;
					$data['goods_count'][ $i ] = 0;
					$data['add_goods_count'][ $i ] = 0;
				}
			}
			
			$data['time_range'] = $time_range;
			
			return $data;
		}
	}
	
	/**
	 * 访问统计
	 * @return mixed
	 */
	public function visit()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 0);
			
			if ($date_type == 0) {
				$start_time = strtotime("today");
				$time_range = date('Y-m-d', $start_time);
			} else if ($date_type == 1) {
				$start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			} else if ($date_type == 2) {
				$start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
			}
			
			$stat_model = new StatModel();
			
			$shop_stat_sum = $stat_model->getShopStatSum($this->site_id, $start_time);
			
			$shop_stat_sum['data']['time_range'] = $time_range;
			
			return $shop_stat_sum;
		} else {
			return $this->fetch("stat/visit");
		}
	}
	
	/**
	 * 访问统计报表
	 * */
	public function getVisitStatList()
	{
		if (request()->isAjax()) {
			$date_type = input('date_type', 1);
			
			if ($date_type == 1) {
				$start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 6;
			} else if ($date_type == 2) {
				$start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
				$time_range = date('Y-m-d', $start_time) . ' 至 ' . date('Y-m-d', strtotime("today"));
				$day = 29;
			}
			
			$stat_model = new StatModel();
			
			$stat_list = $stat_model->getShopStatList($this->site_id, $start_time);
			
			//将时间戳作为列表的主键
			$shop_stat_list = array_column($stat_list['data'], null, 'day_time');
			
			$data = array();

            for ($i = 0; $i <= $day; $i++) {
                $time = strtotime(date('Y-m-d', strtotime("-" . ($day - $i) . " day")));
                $data['time'][ $i ] = date('Y-m-d', $time);
                if (array_key_exists($time, $shop_stat_list)) {
                    $data['order_total'][ $i ] = $shop_stat_list[ $time ]['order_total'];
                    $data['shipping_total'][ $i ] = $shop_stat_list[ $time ]['shipping_total'];
                    $data['refund_total'][ $i ] = $shop_stat_list[ $time ]['refund_total'];
                    $data['order_pay_count'][ $i ] = $shop_stat_list[ $time ]['order_pay_count'];
                    $data['goods_pay_count'][ $i ] = $shop_stat_list[ $time ]['goods_pay_count'];
                    $data['shop_money'][ $i ] = $shop_stat_list[ $time ]['shop_money'];
                    $data['platform_money'][ $i ] = $shop_stat_list[ $time ]['platform_money'];
                    $data['collect_shop'][ $i ] = $shop_stat_list[ $time ]['collect_shop'];
                    $data['collect_goods'][ $i ] = $shop_stat_list[ $time ]['collect_goods'];
                    $data['visit_count'][ $i ] = $shop_stat_list[ $time ]['visit_count'];
                    $data['order_count'][ $i ] = $shop_stat_list[ $time ]['order_count'];
                    $data['goods_count'][ $i ] = $shop_stat_list[ $time ]['goods_count'];
                    $data['add_goods_count'][ $i ] = $shop_stat_list[ $time ]['add_goods_count'];
                } else {
                    $data['order_total'][ $i ] = 0.00;
                    $data['shipping_total'][ $i ] = 0.00;
                    $data['refund_total'][ $i ] = 0.00;
                    $data['order_pay_count'][ $i ] = 0;
                    $data['goods_pay_count'][ $i ] = 0;
                    $data['shop_money'][ $i ] = 0.00;
                    $data['platform_money'][ $i ] = 0.00;
                    $data['collect_shop'][ $i ] = 0;
                    $data['collect_goods'][ $i ] = 0;
                    $data['visit_count'][ $i ] = 0;
                    $data['order_count'][ $i ] = 0;
                    $data['goods_count'][ $i ] = 0;
                    $data['add_goods_count'][ $i ] = 0;
                }
            }
			
			$data['time_range'] = $time_range;
			
			return $data;
		}
	}

    /**
     * 店铺统计
     */
    public function shop_export(){
        return $this->fetch("stat/shop_export");
    }

    /**
     * 店铺统计导出
     */
    public function exportShopStat(){
        $date_type = input('date_type', 0);

        if($date_type == 0){
            $start_time = strtotime("today");
            $time_range = date('Y-m-d',$start_time);
        }else if($date_type == 1){
            $start_time = strtotime(date('Y-m-d', strtotime("-6 day")));
            $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
        }else if($date_type == 2){
            $start_time = strtotime(date('Y-m-d', strtotime("-29 day")));
            $time_range = date('Y-m-d',$start_time).' 至 '.date('Y-m-d',strtotime("today"));
        }

        $stat_model = new StatModel();

        //店铺统计信息
        $shop_stat_sum = $stat_model->getShopsStatSum($this->site_id, $start_time);
        $shop_stat_sum['data']['time_range'] = $time_range;
        $shop_stat_sum['data']['site_id'] = $this->site_id;

        //店铺交易信息
        $condition = [["o.site_id", "=", $this->site_id], ["o.order_status", "=", 10],["o.finish_time", '>=', $start_time]];
        $shop_order_list = $stat_model->getShopsOrderList($condition);
        if($shop_order_list['data'] && $shop_stat_sum['data']['shop_pv_count'] > 0){
            $conversion = bcdiv(count($shop_order_list['data']), $shop_stat_sum['data']['shop_pv_count'], 4) * 100;
        }else{
            $conversion = 0;
        }

        $header_arr = array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
            'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ'
        );
        //处理数据
        $excelTableData = [];
        if (!empty($shop_stat_sum['data'])) {
            $excelTableData[] = $shop_stat_sum['data'];
            $excelTableData[] = $shop_order_list['data'];
        }

        //接收需要展示的字段
        $shop_field = ["site_id", "site_name", "site_link", "shop_uv_count", "shop_pv_count", "order_total", "order_pay_count", "time_range", "conversion"];
        $order_field = ["order_name", "sku_id", "goods_link", "goods_money", "pay_money", "goods_num", "finish_time"];

        $shop_field_name = [
            "site_id" => '店铺编号',
            "site_name" => '店铺名称',
            "site_link" => '店铺首页链接',
            "shop_uv_count" => '店铺UV',
            "shop_pv_count" => '店铺PV',
            "order_total" => '店铺销量',
            "order_pay_count" => '店铺销售数量',
            "time_range" => '日期',
            "conversion" => '转化率'
        ];
        $order_field_name = [
            "order_name" => '商品名称',
            "sku_id" => 'SKU编码',
            "goods_link" => '商品链接',
            "goods_money" => '商城价格',
            "pay_money" => '成交价格',
            "goods_num" => '成交数量',
            "finish_time" => '交易时间'
        ];
        $sheetName = ['店铺统计-店铺信息', '店铺统计-交易信息'];

        // 实例化excel
        $phpExcel = new \PHPExcel();

        foreach($excelTableData as $k=>$v){
            //多个Sheet工作簿
            if($k !== 0) $phpExcel->createSheet();
            $phpExcel->setactivesheetindex($k);
            $phpExcel->getActiveSheet($k)->setTitle($sheetName[$k]);
            $phpExcel->getActiveSheet($k)->freezePane('A2');//冻结窗口

            //店铺统计息
            if($k == 0){
                $count = count($shop_field);
                $site_id = $v['site_id'];
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $shop_field_name[$shop_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗

                    //内容行
                    $value = $v[$shop_field[$i]];
                    //标题内容取最大值
                    $title_len = strlen(iconv('utf-8', 'gb2312', $shop_field_name[$shop_field[$i]]));
                    $value_len = strlen(iconv('utf-8', 'gb2312', $value));
                    $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;

                    if($shop_field[$i] == 'site_link'){
                        $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '2', url("web/shop-$site_id"));
                    }elseif($shop_field[$i] == 'conversion'){
                        $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '2', $conversion . '%');
                    }else{
                        $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '2', $value);
                    }

                    if($strlen < 20) $strlen = 20;
                    $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                }
            }

            //店铺交易信息
            if($k == 1){
                $count = count($order_field);
                for ($i = 0; $i < $count; $i++) {
                    //标题行
                    $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . '1', $order_field_name[$order_field[$i]]);
                    $phpExcel->getActiveSheet($k)->getStyle($header_arr[$i] . '1')->getFont()->setBold(true);//设置是否加粗

                    //标题长度
                    $title_len = strlen(iconv('utf-8', 'gb2312', $order_field_name[$order_field[$i]]));

                    if(!$v) continue;
                    foreach($v as $key=>$val){
                        $goods_id = $val['goods_link'];
                        //内容行
                        $value = $val[$order_field[$i]];
                        //内容长度  标题内容取最大值
                        $value_len = strlen(iconv('utf-8', 'gb2312', $value));
                        $strlen = bccomp($title_len, $value_len) ? $title_len : $value_len;
                        //插入行位置
                        $keys = $key+2;
                        if($order_field[$i] == 'goods_link'){
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, url("web/sku-$goods_id"));
                        }else{
                            $phpExcel->getActiveSheet($k)->setCellValue($header_arr[$i] . $keys, $value);
                        }
                        if($strlen < 20) $strlen = 20;
                        $phpExcel->getActiveSheet($k)->getColumnDimension($header_arr[$i])->setWidth($strlen + 5);//设置列宽度
                    }
                }
            }
        }


        // 设置第一个sheet为工作的sheet
        $phpExcel->setActiveSheetIndex(0);

        // 保存Excel 2007格式文件，保存路径为当前路径，名字为export.xlsx
        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        $file = '店铺统计.xlsx';
        $objWriter->save($file);

        header("Content-type:application/octet-stream");

        $filename = basename($file);
        header("Content-Disposition:attachment;filename = " . $filename);
        header("Accept-ranges:bytes");
        header("Accept-length:" . filesize($file));
        readfile($file);
        unlink($file);
        exit;
    }
}