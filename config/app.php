<?php
// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

use think\facade\Env;

return [
    // 应用地址
    'app_host'         => Env::get('app.host', ''),
    // 应用的命名空间
    'app_namespace'    => '',
    // 是否启用路由
    'with_route'       => true,
    // 是否启用事件
    'with_event'       => true,
    // 默认应用
    'default_app'      => 'shop',
    // 默认时区
    'default_timezone' => 'Asia/Shanghai',

    // 应用映射（自动多应用模式有效）
    'app_map'          => [],
    // 域名绑定（自动多应用模式有效）
    'domain_bind'      => [],
    // 禁止URL访问的应用列表（自动多应用模式有效）
    'deny_app_list'    => [],

    // 异常页面的模板文件
    'exception_tmpl'   => app()->getThinkPath() . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'    => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'   => true,
    //环境类型 dev 开发 test 测试 stage 预发布 prod 生产 一般使用 测试 和 生产
    'env_type'         => 'test',
    //高德地图key 分为网页 js 安卓 ios
    'geo_key' => [
        'web' => '522eede14e8eb43ef9de4ec7a45154d4',
        'web_js' => '2e23517ec9b17d396902ecabddc2c174',
        'android' => '3f108cbdff3508caafee9d3f8feea4e2',
        'ios' => 'e7653cc1cae0e5e0fb838d214f947d3d',
    ],
];
